package com.reltio.mobile;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

public class Configuration extends HttpServlet {
    public final static String CONTEXT_PATH = "/MobileUI";
    public static String getRequestUrl(HttpServletRequest req){
        return req.getHeader("X-Forwarded-Host") == null ? req.getRequestURL().toString() : req.getHeader("X-Forwarded-Host");
    }

    private static String getScheme(HttpServletRequest req){
        String scheme = req.getHeader("X-Forwarded-Proto");
        if (scheme == null) {
            scheme = req.isSecure() ? "https" : "http";
        }
        return scheme;
    }
    public static String getUIUrl(HttpServletRequest req) {
        String requestUrl = getRequestUrl(req);
        String url = "";
        String requestUri = req.getRequestURI();
        int pos = requestUrl.indexOf(requestUri);
        String scheme = getScheme(req);
        if (pos > -1) {
            url = requestUrl.substring(0, pos) + debugPath(req) + "/";
        } else if (req.getHeader("X-Forwarded-Host") != null) { // if X-Forwarded-Host is defined, it will be something like tst-01.reltio.com, so that we need to build ui url
            url = scheme + "://" + requestUrl + "/";
        }
        int schemePosition = url.indexOf(":");
        if (schemePosition != -1) {
            url = scheme + url.substring(schemePosition);
        }
        return url;
    }

    private static String debugPath(HttpServletRequest req) {
        boolean isDebug = Boolean.parseBoolean(
                req.getSession()
                        .getServletContext()
                        .getInitParameter("debug"));
        return (isDebug ? CONTEXT_PATH : "");
    }

    private String convert(Map<String, String> parameters) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        if (parameters != null) {
            Iterator<Map.Entry<String, String>> iterator = parameters.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                stringBuilder.append(entry.getKey()).append(":\"").append(entry.getValue()).append("\"");
                if (iterator.hasNext()) {
                    stringBuilder.append(",");
                }
            }
        }

        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String> params = new HashMap<>();
        params.put("ui", getUIUrl(req));
        params.put("debugPath", debugPath(req));
        params.put("version", getServletContext().getInitParameter("Version"));
        resp.setContentType("application/json");
        try (PrintWriter pw = resp.getWriter()) {
            pw.write(convert(params));
        }
    }
}
