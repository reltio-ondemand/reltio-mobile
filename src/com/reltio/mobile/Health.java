package com.reltio.mobile;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

public class Health extends HttpServlet
{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream outputStream = resp.getOutputStream();
        resp.setContentType("application/json");
        resp.addHeader("Cache-Control", "private, no-cache, no-store, must-revalidate, max-age=0");
        resp.addHeader("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");
        resp.addHeader("Pragma", "no-cache");
        outputStream.write("{\"status\": \"OK\"}".getBytes("UTF-8"));
        outputStream.close();
    }
}
