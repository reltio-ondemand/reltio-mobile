package com.reltio.mobile;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.api.Result;
import org.eclipse.jetty.proxy.ProxyServlet;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class Proxy extends ProxyServlet {

    protected HttpClient newHttpClient() {
        SslContextFactory sec = new SslContextFactory(true);
        sec.setValidateCerts(false);
        return new HttpClient(sec);
    }

    protected void sendProxyRequest(HttpServletRequest clientRequest,
                                    HttpServletResponse proxyResponse,
                                    Request proxyRequest) {
        proxyRequest.getHeaders().remove("Host");
        super.sendProxyRequest(clientRequest, proxyResponse, proxyRequest);
    }

    @Override
    protected String rewriteTarget(HttpServletRequest request) {
        String uri = request.getServletPath();
        String queryString = request.getQueryString();
        String pathInfo = request.getPathInfo();
        StringBuilder str = new StringBuilder(this.getServletContext().getInitParameter("UIPath")).append(uri);

        if (pathInfo != null)
            str.append(pathInfo);

        if (queryString != null)
            str.append('?').append(queryString);

        return str.toString();
    }
}
