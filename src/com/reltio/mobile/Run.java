package com.reltio.mobile;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;


public class Run {
    public static void main(String[] args)
            throws Exception {
        Server server = new Server(8888);
        if (args.length > 0 && "SSL".equals(args[0])) {
            System.out.println("SSL");
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(9999);
            HttpConfiguration https = new HttpConfiguration();
            https.addCustomizer(new SecureRequestCustomizer());
            SslContextFactory sslContextFactory = new SslContextFactory();
            sslContextFactory.setKeyStorePath(Run.class.getResource(
                    "/keystore.jks").toExternalForm());
            sslContextFactory.setKeyStorePassword("123456");
            sslContextFactory.setKeyManagerPassword("123456");
            ServerConnector sslConnector = new ServerConnector(server,
                    new SslConnectionFactory(sslContextFactory, "http/1.1"),
                    new HttpConnectionFactory(https));
            sslConnector.setPort(9998);
            server.setConnectors(new Connector[]{connector, sslConnector});
        }
        WebAppContext context = new WebAppContext();
        context.setServer(server);
        context.setContextPath(Configuration.CONTEXT_PATH);
        context.setResourceBase("web");
        context.setDescriptor("web/WEB-INF/web.xml");
        server.setHandler(context);
        try {
            server.start();
            //noinspection ResultOfMethodCallIgnored
            System.in.read();
            server.stop();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(100);
        }
    }
}
