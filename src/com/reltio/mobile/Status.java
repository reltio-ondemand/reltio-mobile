package com.reltio.mobile;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class Status extends HttpServlet {

    public static final String STATUS = "status";
    public void init() {
        String version = getServletContext().getInitParameter("Version");
        System.out.println("Mobile UI. Version:"+version);

    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");

        String version = getServletContext().getInitParameter("Version");
        String urlString = Configuration.getUIUrl(req);
        String uiPath = this.getServletContext().getInitParameter("UIPath");
        URL url;
        if (!("".equals(uiPath) || uiPath == null)) {
            url = new URL(uiPath + "/" + STATUS);
        } else {
            url = new URL(urlString + STATUS);

        }
        StringBuilder sb = new StringBuilder();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        String authorization = req.getHeader("Authorization");
        if (authorization != null){
            conn.setRequestProperty("Authorization", authorization);
        }


        try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch(Exception e){
            System.out.println("Can't read ui status");
        }

        try {
            JSONObject parsedJson = new JSONObject(sb.toString());
            parsedJson.put("Mobile version", version);
            if (parsedJson.has("version")) {
                parsedJson.put("UI version", parsedJson.remove("version"));
            }
            resp.getOutputStream().write(parsedJson.toString().getBytes("UTF-8"));

        } catch (JSONException e) {
            resp.setStatus(401);
        }

    }
}
