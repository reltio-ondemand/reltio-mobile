package com.reltio.mobile;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class TenantFilter implements Filter {

    private Set<String> rootContent_;
    private Date date;
    private String timestamp;
    private String etag;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        rootContent_ = new HashSet<>();
        try {
            date = new SimpleDateFormat("yyyyMMdd-hhmm").parse(filterConfig.getInitParameter("Timestamp"));
        } catch (ParseException e) {
            date = new Date();
        }
        DateFormat dateTimeInstance = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        dateTimeInstance.setTimeZone(TimeZone.getTimeZone("GMT"));
        timestamp = dateTimeInstance.format(date);
        etag = UUID.nameUUIDFromBytes(timestamp.getBytes()).toString();
        Set<String> filesSet = filterConfig.getServletContext().getResourcePaths("/");
        for (String name : filesSet) {
            rootContent_.add(name.replaceAll("/", ""));
        }
        rootContent_.addAll(getUIServices());
    }

    /**
     * Method return the set of ui services
     *
     * @return set of ui services
     */
    private Set<String> getUIServices() {
        return new HashSet<String>() {{
            add("ui");
            add("services");
            add("configuration");
            add("status");
            add("health");
        }};
    }

    private void preventCaches(HttpServletResponse resp) {
        if (resp != null) {
            resp.setHeader("ETag", etag);
            resp.setHeader("Last-Modified", timestamp);
            resp.addHeader("Cache-Control", "private, no-cache, no-store, must-revalidate, max-age=0");
            resp.addHeader("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");
            resp.addHeader("Pragma", "no-cache");
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String servletPath = req.getServletPath();
        if (servletPath != null) {
            if (servletPath.contains("WEB-INF") || servletPath.contains("META-INF")) {
                res.sendError(404);
                return;
            }
            Matcher m = Pattern.compile("/*([^/]*)/*(.*)").matcher(servletPath);
            if (m.find()) {
                String tenant = m.group(1);
                if (rootContent_.contains(tenant)) {
                    chain.doFilter(request, response);
                    return;
                } else {
                    String toReplace = "/" + tenant;
                    String newURI = servletPath.replace(toReplace, "");
                    preventCaches(res);
                    req.getRequestDispatcher(newURI).forward(req, res);
                    return;
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
