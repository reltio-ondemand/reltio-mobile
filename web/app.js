/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".

 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */
Ext.application({
    name: 'MobileUI',

    requires: [
        'Ext.MessageBox',
        'MobileUI.controller.Base'
    ],
    viewport: {
        scrollable: false
    },
    controllers: [
        'Login',
        'ViewActivator',
        'ForgotPassword',
        'PasswordRestore',
        'ErrorPage',
        'Base',
        'Feedback',
        'NewPassword',
        'SearchFacet',
        'RelationAttributes',
        'CustomViewController',
        'EditRelationAttributes',
        'RelationComplexAttributes',
        'Search',
        'SearchTypeAhead',
        'Profile',
        'ComplexAttributes',
        'AttributesFacet',
        'PlacesFacet',
        'Relations',
        'Comments',
        'AddComments',
        'RelationsTree',
        'TwoHopsConnections',
        'CreateEntity',
        'NewEditMode',
        'dialogs.RelationshipType',
        'dialogs.CreateEntity',
        'dialogs.ChooseEntity',
        'dialogs.CreateRelation',
        'dialogs.NestedAttributeEditor',
        'dialogs.ReferenceAttributeEditor',
        'dialogs.ChooseAttributeType',
        'dialogs.GlobalFilter',
        'dialogs.SavedSearches',
        'dialogs.EditSavedSearch',
        'dialogs.entities.SelectImage',
        'dialogs.UploadImage',
        'editors.EnumEditor',
        'editors.LookupEditor',
        'editors.StringEditor',
        'editors.NumberEditor',
        'editors.TypeaheadEditor',
        'editors.DateEditor',
        'editors.BoolEditor',
        'editors.BlobEditor',
        'dashboard.Dashboard'

    ],
    profiles: ['General'],

    icon: {
        '57': 'resources/icons/apple-touch-icon-57x57.png',
        '72': 'resources/icons/apple-touch-icon-72x72.png',
        '114': 'resources/icons/apple-touch-icon-114x114.png',
        '144': 'resources/icons/apple-touch-icon-144x144.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    /**
     * Fix a Sencha Touch Bug that was introduced with Google Chrome v43+
     *
     * See following Forum Thread for more Information:
     * https://www.sencha.com/forum/showthread.php?300288-Scrolling-Issues-in-latest-Google-Chrome
     */
    fixOverflowChangedIssue: function()
    {
        if (Ext.browser.is.WebKit) {
            console.info(this.$className + ': Fix a Sencha Touch Bug (TOUCH-5716 / Scrolling Issues in Google Chrome v43+)');

            Ext.override(Ext.util.SizeMonitor, {
                constructor: function (config) {
                    var namespace = Ext.util.sizemonitor;
                    return new namespace.Scroll(config);
                }
            });

            Ext.override(Ext.util.PaintMonitor, {
                constructor: function (config) {
                    return new Ext.util.paintmonitor.CssAnimation(config);
                }
            });
        }
    },
    launch: function ()
    {
        this.fixOverflowChangedIssue();
        Ext.Ajax.setUseDefaultXhrHeader(false);
        Ext.Ajax.setTimeout(60000);
        Ext.Ajax.cors = true;
        if (window.navigator.standalone) {
            window.document.body.classList.add('standalone-mobile-app')
        }
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
        //all activity will be done in profile

    },
    statusBarStyle: 'black-translucent',
    onUpdated: function ()
    {
        window.location.reload();
    }
});
