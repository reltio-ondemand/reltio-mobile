/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Crumb', {
    extend: 'Ext.Container',
    xtype: 'crumb',
    config: {
        cls: 'crumb',
        layout: 'hbox'
    },
    /**@type {Array} @private}*/
    path: null,

    /**
     * Method sets the path
     * @param {Array} path
     */
    setPath: function (path)
    {
        this.path = path;
        this.refresh();
    },

    /**
     * Method refreshes the crumb component
     * @protected
     */
    refresh: function ()
    {
        var i, pathItem;
        this.removeAll();
        if (!Ext.isEmpty(this.path))
        {
            for (i = 0; i < this.path.length; i++)
            {
                pathItem = Ext.create('Ext.Component');
                pathItem.setCls('crumb-item');
                pathItem.setHtml(
                    '<span class = "caption">' + i18n(this.path[i] || '') + '</span>'+
                    (i + 1 !== this.path.length ? '<span class="arrow"></span>' : ''));
                this.add(pathItem);
            }
        }
    }
});
