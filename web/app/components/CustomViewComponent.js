/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.CustomViewComponent', {
    extend: 'Ext.Component',
    xtype: 'customviewcomponent',
    requires: ['MobileUI.components.MCustomAction',
    'MobileUI.core.sandbox.WebWorkerSandbox'],
    mixins: {
        action: 'MobileUI.components.MCustomAction'
    },
    config: {
        parameters: null
    },
    applyParameters: function(params){
        if (params !== null){
            this.configuration = params;
            this.initAction();
        }
    }
});