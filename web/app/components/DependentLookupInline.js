/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.DependentLookupInline', {
    extend: 'MobileUI.components.StyledSelect',
    xtype: 'dependentlookupinline',
    mixins: ['MobileUI.components.MSpinner'],
    config: {
        lookupCode: null,
        attrTypeUri: null,
        editingUri: null,
        session: null,
        localizeLabel: null
    },
    needUpdate: null,
    spinnerIcon: null,
    inited: false,

    updateAttrTypeUri: function () {
        this.registerEditor();
    },

    updateLookupCode: function () {
        this.registerEditor();
    },

    updateSession: function () {
        this.registerEditor();
    },
    updateEditingUri: function(){
        this.registerEditor();
    },
    showWaitSpinner: function () {
        this.getPicker().add({
            xtype: 'component',
            cls: 'rl-spinner-item',
            html: this.getSpinnerHtml(),
            style: 'text-align: center',
            name: 'dependent-lookup-chart-spinner'
        });
    },
    sendRequest: function () {
        this.showWaitSpinner();
        var lookupManager = this.getSession().getLookupsManager();
        var parents = lookupManager.isInited() ? lookupManager.getParents(this.getLookupCode(), this.getEditingUri(), this.getAttrTypeUri()).reduce(function (prev, current) {
            var info = current.getInfo();
            var result = (info.values || []).map(function (item) {
                return {
                    type: info.dependentLookupCode,
                    codeValue: item.value
                };
            });
            return prev.concat(result);
        }, []): [];
        this.getSession().resolveLookupValue(this.getLookupCode(), parents, '', null, this.onResponse, null, this);
    },

    onResponse: function (response) {
        response = response.codeValues || {};
        var spinner = this.getPicker().down('component[name=dependent-lookup-chart-spinner]');
        if (spinner) {
            spinner.destroy();
            spinner = null;
        }
        var values = response[this.getLookupCode()];
        var me = this,
            store = me.getStore(),
            value = me.getValue();

        if (values) {
            var processLookup = function (key, value) {
                var text = value.displayName || key;

                if (text !== key) {
                    text += ' (' + key + ')';
                }

                return {
                    html: '<span style="' +
                    ' white-space:nowrap; padding-left: ' + '0em">' + text + '</span>',
                    text: text,
                    value: key
                };
            };
            var model = Object.keys(values).map(function (key) {
                return processLookup(key, values[key]);
            });
            store.clearData();
            store.add(model);
            if (me.getUsePicker()) {
                var picker = me.getPhonePicker(),
                    name = me.getName(),
                    pickerValue = {};

                pickerValue[name] = value;
                picker.setValue(pickerValue);
            } else {
                var listPanel = me.getTabletPicker(),
                    list = listPanel.down('list'),
                    index, record;
                if (value || me.getAutoSelect()) {
                    store = list.getStore();
                    index = store.find(me.getValueField(), value, null, null, null, true);
                    record = store.getAt(index);

                    if (record) {
                        list.select(record, null, true);
                    }
                }
            }
        }
    },
    showPicker: function () {
        this.sendRequest();
        var me = this;


        //check if the store is empty, if it is, return

        if (me.getReadOnly() || !this.getSession() || !this.getAttrTypeUri() || !this.getLookupCode()) {
            return;
        }

        me.isFocused = true;

        if (me.getUsePicker()) {
            var picker = me.getPhonePicker();
            if (!picker.getParent()) {
                Ext.Viewport.add(picker);
            }
            picker.show();
        } else {
            var listPanel = me.getTabletPicker();
            if (!listPanel.getParent()) {
                Ext.Viewport.add(listPanel);
            }
            listPanel.showBy(me.getComponent(), null);
        }
    },

    getPicker: function () {
        return this.getUsePicker() ? this.getPhonePicker() : this.getTabletPicker();
    },
    block: function (parents) {
        this.setDisabled(true);
        var label = Ext.isEmpty(parents) ? 'Parents' : parents.join(', ');
        this.setPlaceHolder(i18n('Please select value for') + ' ' + label);
    },
    unblock: function() {
        this.setDisabled(false);
        this.setPlaceHolder('');
    },
    registerEditor: function () {
        if (this.getSession() && this.getAttrTypeUri() && this.getLookupCode() && this.getEditingUri() && !this.inited) {
            this.inited = true;
            var manager = this.getSession().getLookupsManager();
            manager.on('dependentUpdated', function (lookups) {
                if (lookups.indexOf(this.getLookupCode()) !== -1) {
                    this.actualizeEditor();
                }
            }, this);
            manager.registerEditor(this, this.getEditingUri(), this.getLookupCode());
            this.actualizeEditor();
        }
    },
    actualizeEditor: function(){
        var lookupManager = this.getSession().getLookupsManager();
        if (!lookupManager.isInited()){
            this.unblock();
            return;
        }
        var parents = lookupManager.getParents(this.getLookupCode(), this.getEditingUri(), this.getAttrTypeUri());
        if (parents && parents.length === 1) {
            if (!parents[0].getInfo().dependentLookupCode) {
                this.unblock();
                return;
            }
        }
        var parentsValues = parents.reduce(function (prev, current) {
            var info = current.getInfo();
            return prev.concat((info.values || [])).map(function (item) {
                return item.value;
            });
        }, []);
        if(!Ext.isEmpty(parentsValues)){
            this.unblock();
        }else {
            var emptyParents = parents.reduce(function (prev, current) {
                var info = current.getInfo();
                return prev.concat(info.uri);
            }, []).map(function(uri){
                return MobileUI.core.Metadata.findEntityAttributeByUri(null, uri);
            }).filter(function(item){
                return item;
            }).map(function(item){
                return item.label;
            });
            this.block(emptyParents);
        }
    },
    updateValue: function (newValue, oldValue) {
        if (Ext.isString(newValue)) {
            var component = this.getComponent();
            if (component) {
                component.setValue(newValue);
            }

            this.syncEmptyCls();
        } else {
            this.callParent([newValue, oldValue]);
        }
    },

    applyValue: function (value) {
        var record = value,
            index, store;

        //we call this so that the options configruation gets intiailized, so that a store exists, and we can
        //find the correct value
        this.getOptions();

        store = this.getStore();

        if ((value && !value.isModel) && store) {
            index = store.find(this.getValueField(), value, null, null, null, true);

            if (index === -1) {
                index = store.find(this.getDisplayField(), value, null, null, null, true);
            }
            if (index !== -1) {
                record = store.getAt(index);
            } else {
                record = this.getLocalizeLabel() || record;
            }
        }
        return record;
    },
    destroy: function () {
        this.callParent(arguments);
    }
});
