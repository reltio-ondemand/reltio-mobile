/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Raises events:
 * - select(record) - when item selected
 * - info(record) - when info button clicked
 * - delete(record) - when delete button clicked
 * - fetch - when new data amount required (scrolled to bottom)
 * - refresh - when data refresh required (scrolled to top)
 * - pullRefresh - on pullRefresh
 *
 * To enable fetch and refresh behaviours set allowFetch/allowRefresh.
 * After data updated when event fires call fetchDone/refreshDone
 */

Ext.define('MobileUI.components.EditableList', {
    extend: 'Ext.dataview.List',
    mixins: {observable:'Ext.mixin.Observable'},
    requires: [
        'MobileUI.components.list.items.BaseListItem',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.components.editablelist.items.ListItem'
    ],
    xtype: 'elist',
    config: {
        cls: 'elist',
        scrollable: 'vertical',
        editing: false,
        defaultType: 'eDataItem',
        useComponents: true,
        useHeaders: false,
        allowFetch: false,
        allowRefresh: false,
        allowDelete: false
    },

    /** @private */
    fetching: false,

    /** @private */
    refreshing: false,

    /** @private */
    spinnerItemIndex: -1,

    /** @private */
    listScrolling: false,

    initialize: function() {
        this.callParent(arguments);
        var scroller = this.getScrollable().getScroller();
        scroller.on('scrollstart', function() { this.listScrolling = true; }, this);
        scroller.on('scroll', this.onScroll, this);
        scroller.on('scrollend', function() { this.listScrolling = false; }, this);
    },

    /**
     * @return {boolean}
     */
    isScrolling: function() {
        return this.listScrolling;
    },

    /**
     * @private
     */
    onScroll: function(scroller, x, y) {
        var atBottom = (scroller.maxPosition.y <= y) && (scroller.dragDirection.y === 1);
        if (atBottom && this.getAllowFetch() && !this.fetching) {
            this.startFetch();
        }

        var atTop = (scroller.minPosition.y >= y) && (scroller.dragDirection.y === -1);
        if (atTop && this.getAllowRefresh() && !this.refreshing) {
            this.startRefresh();
        }
    },

    /**
     * @public
     */
    startFetch: function() {
        this.fetching = true;
        this.showSpinner(true);
        this.fireEvent('fetch');
    },

    onPullRefresh: function (callback, scope)
    {
        this.fireEvent('pullRefresh');
        if (callback)
        {
            callback.apply(scope);
        }
    },

    /**
     * This method should be called after fetch event handled
     * @param {Boolean} moreDataAvailable
     */
    fetchDone: function(moreDataAvailable) {
        this.setAllowFetch(moreDataAvailable);
        this.fetching = false;
        this.showSpinner(this.isBusy());
        this.fireEvent('fetchDone');
    },

    /**
     * @private
     */
    startRefresh: function() {
        this.refreshing = true;
        this.showSpinner(true);
        this.fireEvent('fetch');
    },

    /**
     * This method should be called after refresh event handled
     */
    refreshDone: function() {
        this.refreshing = false;
        this.showSpinner(this.isBusy());
    },

    /**
     * @return {boolean} list is waiting for data
     */
    isBusy: function() {
        return this.refreshing || this.fetching;
    },

    /**
     * Method updates editing state for all items in the list
     * @param editing {Boolean} state
     */
    applyEditing: function(editing) {
        this.getViewItems().forEach(function(item) {
            item.setEditing(editing);
        });
        return editing;
    },

    /**
     * @private
     */
    showSpinner: function(visible) {
        var store = this.getStore();
        if (Ext.isEmpty(store) || store.getCount() === 0) {
            return;
        }
        if (visible) {
            if (this.spinnerItemIndex === -1) {
                this.spinnerItemIndex = store.getCount();
                store.add([{
                    type: MobileUI.components.list.items.BaseListItem.types.pending,
                    lastInGroup: true
                }]);

                var scroller = this.getScrollable().getScroller();
                setTimeout(function() {
                    scroller.scrollToEnd(500);
                }, 100);
            }
        }
        else {
            if (this.spinnerItemIndex !== -1) {
                var spinner = this.getItemAt(this.spinnerItemIndex);
                if (spinner) {
                    store.removeAt(this.spinnerItemIndex);
                }
                this.spinnerItemIndex = -1;
            }
        }
        return false;
    },

    setMasked: function(masked) {
        this.callSuper([masked ? {
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        } : null]);
    },

    onContainerResize: function() {
        this.callSuper(arguments);
        this.container.getScrollable().getScroller().refresh();
    }
});
