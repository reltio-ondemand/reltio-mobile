/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.ExpandableContainer', {
    extend: 'Ext.Container',
    xtype: 'expandablecontainer',
    config: {
        cls: 'expandable-container',
        layout: 'hbox',
        mode: 'fullscreen',
        initialHeight: 200
    },
    requires: ['Ext.mixin.Observable'],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    statics: {
        COLLAPSED: 'collapsed',
        EXPANDED: 'expanded'
    },
    /**@type {Ext.Button} @private */
    controlButton: null,
    isCollapsed: null,
    initialize: function ()
    {
        this.callParent(arguments);
        this.controlButton = Ext.create('Ext.Button');
        this.controlButton.setCls('control-button');
        this.add(this.controlButton);
        this.controlButton.addCls(MobileUI.components.ExpandableContainer.COLLAPSED);
        this.setHeight(this.getInitialHeight());
        this.isCollapsed = true;
        this.controlButton.on('tap', function ()
        {
            if (this.isCollapsed)
            {
                this.updateState(MobileUI.components.ExpandableContainer.EXPANDED);
            }
            else
            {
                this.updateState(MobileUI.components.ExpandableContainer.COLLAPSED);
            }
            this.isCollapsed = !this.isCollapsed;
        }, this);
        this.element.on({
            resize: 'onContainerResize',
            scope: this
        });
        Ext.Viewport.on('orientationchange', this.onContainerResize, this);
    },

    onContainerResize: function() {
        var c = MobileUI.components.ExpandableContainer;
        this.updateState(this.isCollapsed ? c.COLLAPSED : c.EXPANDED, true);
    },

    updateState: function (state, instant)
    {
        var top, height, availableHeight, me = this;
        if (this.controlButton && !this.controlButton.isDestroyed)
        {
            if (state === MobileUI.components.ExpandableContainer.COLLAPSED)
            {
                this.controlButton.addCls(MobileUI.components.ExpandableContainer.COLLAPSED);
                this.controlButton.removeCls(MobileUI.components.ExpandableContainer.EXPANDED);
                this.fireEvent('beforeCollapse');
                Ext.Animator.run({
                    element: this.element,
                    duration: instant ? 0 : 200,
                    easing: 'linear',
                    preserveEndState: true,
                    from: {
                        height: this.element.getHeight()
                    },
                    to: {
                        height: this.getInitialHeight()
                    },
                    onEnd: function ()
                    {
                        me.fireEvent('collapsed');
                    }
                });
            }
            else
            {
                this.controlButton.addCls(MobileUI.components.ExpandableContainer.EXPANDED);
                this.controlButton.removeCls(MobileUI.components.ExpandableContainer.COLLAPSED);
                this.fireEvent('beforeExpand');
                if (this.element.getBox())
                {
                    top = this.element.getBox().top;
                    height = Ext.getBody().getSize().height;
                    availableHeight = height - top;
                    Ext.Animator.run({
                        element: this.element,
                        duration: instant ? 0 : 200,
                        easing: 'linear',
                        preserveEndState: true,
                        from: {
                            height: this.element.getHeight()
                        },
                        to: {
                            height: availableHeight
                        },
                        onEnd: function ()
                        {
                            me.fireEvent('expanded');
                        }
                    });
                }
            }
        }
    },
    /**
     * @inheritdoc
     */
    destroy: function() {
        Ext.Viewport.un('orientationchange', 'onContainerResize', this);
        this.callParent(arguments);
    }

});
