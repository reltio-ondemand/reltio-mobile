/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.FacetProfileBand', {
    extend: 'Ext.Container',
    xtype: 'facetprofileband',
    requires: [
        'Ext.Img',
        'Ext.Component',
        'Ext.mixin.Observable'
    ],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },

    config: {
        layout: 'hbox',
        cls: 'facetprofileband',
        minHeight: 80
    },

    /**@type {Ext.Img} @private */
    avatar: null,
    /**@type {Ext.Component} @private */
    titleLine: null,
    /**@type {Ext.Component} @private */
    subtitleLine: null,
    /**@type {Ext.Component} @private */
    infoLine: null,

    /**
     * Method creates avatar
     * @private
     * @returns {Ext.Img}
     */
    createAvatar: function ()
    {
        var avatar = Ext.create('Ext.Img');
        avatar.setCls('profile-avatar');
        avatar.setWidth(45);
        avatar.setHeight(45);
        avatar.element.on('tap', function ()
        {
            this.fireEvent('avatarTap');
        }, this);
        return avatar;
    },
    setEditMode: function(isEditMode) {
        this.edit.setHidden(!isEditMode);
    },
    createEntityBlock: function (avatar, edit)
    {
        var container = Ext.create('Ext.Container');
        container.setCls('entityBlock');
        container.setWidth(50);
        container.add(avatar);
        container.add(edit);
        container.element.on('tap', function ()
        {
            this.fireEvent('blockTap');
        }, this);
        return container;
    },
    /**
     * Method creates first line text
     * @private
     * @returns {Ext.Component}
     */
    createFirstLineText: function ()
    {
        var firstLine = Ext.create('Ext.Component');
        firstLine.setCls('profile-first-line');
        return firstLine;
    },

    /**
     * Method creates second line text
     * @private
     * @returns {Ext.Component}
     */
    createSecondLineText: function ()
    {
        var secondLine = Ext.create('Ext.Component');
        secondLine.setCls('profile-second-line');
        return secondLine;
    },
    /**
     * Method creates third line text
     * @private
     * @returns {Ext.Component}
     */
    createThirdLineText: function ()
    {
        var thirdLine = Ext.create('Ext.Component');
        thirdLine.setCls('profile-third-line');
        return thirdLine;
    },

    /**
     * On init function.
     * @protected
     */
    initialize: function ()
    {
        this.avatar = this.createAvatar();
        this.favorite = Ext.create('Ext.Img', {cls: 'profile-favorite', mode: 'img', hidden:true});
        this.favorite.on('tap', this.onFavoriteTap, this);
        this.edit = Ext.create('Ext.Img', {cls: 'profile-avatar-edit', mode: 'img', hidden:true});
        this.edit.element.on('tap', function ()
        {
            this.fireEvent('avatarTap');
        }, this);
        this.entityBlock = this.createEntityBlock(this.avatar, this.edit);
        this.titleLine = this.createFirstLineText();
        this.subtitleLine = this.createSecondLineText();
        this.infoLine = this.createThirdLineText();
        this.add([this.entityBlock, {
            xtype: 'container',
            style: {
                paddingBottom: '10px'
            },
            layout: 'vbox',
            items: [
                {xtype: 'spacer'}, this.titleLine, this.subtitleLine, this.infoLine, {xtype: 'spacer'}],
            flex: 1
        }]);
        //}, this.favorite]);
    },


    /**
     * @param title {String?}
     * @param subtitle {String?}
     * @param info {String?}
     */
    setText: function (title, subtitle, info)
    {
        title = MobileUI.core.entity.EntityUtils.processLabel(title);
        this.titleLine.setHtml(Ext.util.Format.htmlEncode(title));
        this.subtitleLine.setHtml(Ext.util.Format.htmlEncode(subtitle || ''));
        this.infoLine.setHtml(Ext.util.Format.htmlEncode(info || ''));
    },

    /**
     * Method sets image url
     * @param imgUrl {String} img
     * @param alternativeImg {String} alternative img
     */
    setAvatar: function (imgUrl, alternativeImg)
    {
        this.avatar.setStyle({'background-size': 'cover'});
        this.avatar.on('error', function ()
        {
            var that = this;
            that.self.avatar.setSrc(that.img);
        }, {img: alternativeImg, self: this}, {
            single: true,
            addOnce: true
        });
        this.avatar.setSrc(imgUrl || alternativeImg);
    },

    /**
     * Method sets badge color
     * @param color {String} color
     */
    setBadgeColor: function (color)
    {
        this.entityBlock.setStyle('background-color: ' + color);
    },
    onFavoriteTap: function ()
    {
        if (!this.tapOnFavoriteDisabled)
        {
            var state = !this.isFavorite;
            this.setFavorite(state);
            this.tapOnFavoriteDisabled = true;
            this.fireEvent('favorited', state);
        }
    },

    setFavorite: function(isFavorite) {
        this.isFavorite = isFavorite;
        this.tapOnFavoriteDisabled = false;
        if (isFavorite)
        {
            this.favorite.setSrc('resources/images/reltio/profile/favorite-active.svg');
        } else
        {
            this.favorite.setSrc('resources/images/reltio/profile/favorite.svg');
        }
        this.favorite.setHidden(false);
    }
});
