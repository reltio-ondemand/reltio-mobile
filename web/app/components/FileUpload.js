/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.FileUpload', {
    extend: 'Ext.Button',
    xtype: 'fileupload',
    config: {
        cls: 'fileupload'
    },
    requires: ['Ext.mixin.Observable','MobileUI.components.MessageBox'],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    template: [
        {
            tag: 'span',
            reference: 'badgeElement',
            hidden: true
        },
        {
            tag: 'span',
            className: 'x-button-icon',
            reference: 'iconElement',
            hidden: false
        },
        {
            tag: 'span',
            reference: 'textElement',
            hidden: true
        },
        {
            tag: 'form',
            reference: 'formElement',
            hidden: false,
            children: [
                {
                    tag: 'input',
                    reference: 'fileElement',
                    type: 'file',
                    name: 'images',
                    hidden: false,
                    tabIndex: -1,
                    style: 'opacity:0;position:absolute;top:-3px;right:-3px;bottom:-3px;left:-3px;z-index:10000;width:100%'
                }
            ]
        }],

    initialize: function ()
    {
        var that = this;
        that.callParent();

        that.fileElement.dom.onchange = function ()
        {
            that.upload.apply(that, arguments);
        };

    },
    /**
     * Method uploads data to storage
     * @private
     */
    upload: function ()
    {
        var file = this.fileElement.dom.files[0];
        var mime = file.type;
        this.fireEvent('beforeUpload', {file: file.name});
        MobileUI.core.Services.uploadImages([file], 3 , function (result)
        {
            this.fileElement.dom.disabled = true;
            if (result.error){
                MobileUI.components.MessageBox.alert(i18n('Warning'), i18n('Can\'t attach image'));
                this.fireEvent('failed');
            }
            else
            {
                result = (Ext.isArray(result) && result.length > 0) ? result[0] : result;
                this.fireEvent('uploaded', {url: result, file: file, mime: mime});
            }
            this.reset();
        }, function (result)
        {
            MobileUI.components.MessageBox.alert('Warning', 'Can\'t attach image');
            this.fireEvent('failed');
            MobileUI.core.Logger.log(result);
            this.reset();
        }, this);
    },
    reset: function ()
    {
        var that = this;
        this.fileElement.dom.disabled = false;
        that.setBadgeText(null);
        if (that.formElement)
        {
            that.formElement.dom.reset();
        }
        if (that.fileElement)
        {
            that.fileElement.show();
        }
    }
});
