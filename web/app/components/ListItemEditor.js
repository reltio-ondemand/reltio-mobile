/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.ListItemEditor', {
    singleton: 'true',
    requires: [
        'MobileUI.components.TripleToggle',
        'MobileUI.view.editors.LookupEditor',
        'MobileUI.view.editors.EnumEditor',
        'MobileUI.view.editors.TypeaheadEditor',
        'Ext.field.DatePickerNative',
        'Ext.field.Number',
        'MobileUI.components.list.editors.NumberEditor',
        'MobileUI.core.util.Validator'
    ],
    /** @private @type Object */
    editors: null,

    /**
     * Method processes list item editors state
     * @param stateHolder {Object} state
     */
    processState: function (stateHolder)
    {
        var state = stateHolder && stateHolder.state;
        if (state && state.dialog)
        {
            MobileUI.core.SuiteManager.showDialog(state.dialog, true);
        }
        else {
            MobileUI.core.SuiteManager.hideDialogs(true, null, null);
        }
    },

    /**
     * @param type {String}
     * @param builder {Function}
     * @param permanent {Boolean|null}
     */
    register: function(type, builder, permanent) {
        if (!this.editors) {
            this.init();
        }
        [].concat(type).forEach(function(type) {
            this.editors[type] = {builder: builder, permanent: permanent};
        }, this);
    },
    /**
     * @private
     * @param editorInfo {Object}
     * @returns {String|null}
     */
    getEditorType: function(editorInfo)
    {
        var result = editorInfo && editorInfo.type || null;
        if (editorInfo)
        {
            if (editorInfo.dependentLookupCode){
                result = 'DependentLookup';
            }
            else if (editorInfo.lookupCode)
            {
                result = 'Lookup';
            }
            else if (editorInfo.values !== undefined)
            {
                result = 'Enum';
            }
        }
        return result;
    },
    /**
     * @param editorInfo {Object}
     * @param item {MobileUI.components.list.items.BaseListItem}
     * @param list {MobileUI.components.EditableList}
     * @param valueChangedCallback {Function?}
     * @param valueChangedContext {Object?}
     * @returns {Boolean}
     */
    create: function(editorInfo, item, list, valueChangedCallback, valueChangedContext) {
        if (MobileUI.core.entity.EntityUtils.isComplex(editorInfo)) {
            return false;
        }

        if (editorInfo.attributes) { //reference attribute change
            return false;
        }

        if (!this.editors) {
            this.init();
        }
        valueChangedCallback = valueChangedCallback || editorInfo.valueChangedCallback || function() {};
        var editor = this.editors[this.getEditorType(editorInfo)];
        (editor || this.editors.String).builder.call(
            valueChangedContext || editorInfo.valueChangedContext,
            editorInfo, item, list, valueChangedCallback);
        return true;
    },

    /** @private */
    init: function() {
        this.editors = {};

        var fieldTypeValidator = {
            'Int' : MobileUI.core.util.Validator.types.integer,
            'Integer' : MobileUI.core.util.Validator.types.integer,
            'Long': MobileUI.core.util.Validator.types.integer,
            'Number': MobileUI.core.util.Validator.types.floatingPoint,
            'Float': MobileUI.core.util.Validator.types.floatingPoint,
            'Double': MobileUI.core.util.Validator.types.floatingPoint,
            'Dollar': MobileUI.core.util.Validator.types.dollar,
            'URL': MobileUI.core.util.Validator.types.url,
            'Image URL': MobileUI.core.util.Validator.types.url
        };

        function changeHandler(context, args, processValue) {
            return function(value, canceled, label) {
                var editorInfo = args[0],
                    item = args[1],
                    list = args[2],
                    valueChangedCallback = args[3],
                    record = item.getRecord();

                if (processValue) {
                    value = processValue(value);
                }

                if (!canceled) {
                    var editor = record.get('editor');
                    record.set('invalid', false);
                    if (editor.dependentLookupCode) {
                        if (label !== null && label !== undefined){
                            record.set('localizeLabel', label);
                        }
                        var userData = record.get('userData');
                        MobileUI.core.session.Session.getLookupsManager().touchEditor(userData ? userData.editingUri : '');
                    }
                    valueChangedCallback.call(context, value, item, editorInfo, canceled);
                    record.set('label', value);
                    item.recreate();
                    list.fireEvent('modified', record);
                    list.fireEvent('recalculate', list);
                }else {
                    valueChangedCallback.call(context, value, item, editorInfo, canceled);
                }
            };
        }

        this.register(['String', 'URL', 'Image URL'], function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('stringEditor')) {
                MobileUI.core.SuiteManager.addDialog('stringEditor', Ext.create('MobileUI.view.editors.StringEditor'));
            }
            var controller = MobileUI.app.getController('editors.StringEditor');
            controller.setTitleText(record.get('property'));
            controller.setValueType(fieldTypeValidator[editorInfo.type]);
            controller.setValue(record.get('label'));
            controller.setValueChangedHandler(changeHandler(this, arguments, function(value) {
                if ((editorInfo.type === 'URL') || (editorInfo.type === 'Image URL')) {
                    if (value.indexOf('://') === -1) {
                        value = 'http://' + value;
                    }
                }
                return value;
            }));
            MobileUI.core.SuiteManager.showDialog('stringEditor');
        });

        this.register('Blob', function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('blobEditor')) {
                MobileUI.core.SuiteManager.addDialog('blobEditor', Ext.create('MobileUI.view.editors.BlobEditor'));
            }
            var controller = MobileUI.app.getController('editors.BlobEditor');
            controller.setTitleText(record.get('property'));
            controller.setValue(record.get('label'));
            controller.setValueChangedHandler(changeHandler(this, arguments));
            MobileUI.core.SuiteManager.showDialog('blobEditor');
        });

        this.register(['Int', 'Integer', 'Long', 'Number', 'Float', 'Double', 'Dollar'], function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('numberEditor')) {
                MobileUI.core.SuiteManager.addDialog('numberEditor', Ext.create('MobileUI.view.editors.NumberEditor'));
            }
            var controller = MobileUI.app.getController('editors.NumberEditor');
            controller.setTitleText(record.get('property'));
            controller.setValueType(fieldTypeValidator[editorInfo.type]);
            var value = record.get('label');
            controller.setValue(value);

            controller.setValueChangedHandler(changeHandler(this, arguments, function(value) {
                return String(value);
            }));

            MobileUI.core.SuiteManager.showDialog('numberEditor');
        });

        this.register('Date', function(editorInfo, item) {
            var record = item.getRecord(),
                val = record.get('label');
            if (Ext.isEmpty(val)) {
                val = MobileUI.core.I18n.translate(new Date(), MobileUI.core.I18n.DATE_INTERNAL);
            }

            if (!MobileUI.core.SuiteManager.hasDialog('dateEditor')) {
                MobileUI.core.SuiteManager.addDialog('dateEditor', Ext.create('MobileUI.view.editors.DateEditor'));
            }
            var controller = MobileUI.app.getController('editors.DateEditor');
            controller.setTitleText(record.get('property'));
            controller.setValue(val);

            controller.setValueChangedHandler(changeHandler(this, arguments));

            MobileUI.core.SuiteManager.showDialog('dateEditor');
        });

        this.register('Enum', function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('enumEditor')) {
                MobileUI.core.SuiteManager.addDialog('enumEditor', Ext.create('MobileUI.view.editors.EnumEditor'));
            }
            var controller = MobileUI.app.getController('editors.EnumEditor');
            controller.setTitleText(record.get('property'));
            controller.setValue(record.get('label'));
            controller.setValueChangedHandler(changeHandler(this, arguments));
            controller.setValues(editorInfo.values);

            MobileUI.core.SuiteManager.showDialog('enumEditor');
        });

        this.register('Faceted', function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('typeaheadEditor')) {
                MobileUI.core.SuiteManager.addDialog('typeaheadEditor', Ext.create('MobileUI.view.editors.TypeaheadEditor'));
            }
            var controller = MobileUI.app.getController('editors.TypeaheadEditor');
            controller.setTitleText(record.get('property'));
            controller.setValue(record.get('label'));
            controller.setValueChangedHandler(changeHandler(this, arguments));
            controller.setFieldName(editorInfo.name);

            MobileUI.core.SuiteManager.showDialog('typeaheadEditor');
        });

        this.register('Lookup', function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('lookupEditor')) {
                MobileUI.core.SuiteManager.addDialog('lookupEditor', Ext.create('MobileUI.view.editors.LookupEditor'));
            }
            var controller = MobileUI.app.getController('editors.LookupEditor');
            controller.setTitleText(record.get('property'));
            controller.setValue(record.get('label'));
            controller.setValueChangedHandler(changeHandler(this, arguments));
            controller.setInfo(editorInfo);
            MobileUI.core.SuiteManager.showDialog('lookupEditor');
        });

        this.register('DependentLookup', function(editorInfo, item) {
            var record = item.getRecord();
            var dependentLookups = Ext.create('MobileUI.components.list.editors.DependentLookupEditor');
            if (MobileUI.core.SuiteManager.hasDialog('dependentLookupEditor')) {
                MobileUI.core.SuiteManager.removeDialog('dependentLookupEditor');
            }
            MobileUI.core.SuiteManager.addDialog('dependentLookupEditor', dependentLookups);
            dependentLookups.setTitleText(record.get('property'));
            var userData = record.get('userData');
            var editingUri = userData ? userData.editingUri : '';
            dependentLookups.setInfo(editorInfo, editingUri, record.get('localizeLabel'));
            dependentLookups.setValue(record.get('label'));
            dependentLookups.setValueChangedHandler(changeHandler(this, arguments));
            MobileUI.core.SuiteManager.showDialog('dependentLookupEditor');
        });

        this.register('Boolean', function(editorInfo, item) {
            var record = item.getRecord();
            if (!MobileUI.core.SuiteManager.hasDialog('boolEditor')) {
                MobileUI.core.SuiteManager.addDialog('boolEditor', Ext.create('MobileUI.view.editors.BoolEditor'));
            }
            var controller = MobileUI.app.getController('editors.BoolEditor'),
                label = record.get('label');
            controller.setTitleText(record.get('property'));
            controller.setValue(label && (label === 'true'));

            controller.setValueChangedHandler(changeHandler(this, arguments, function(value) {
                return value === true ? 'true' : (value === false ? 'false' : '');
            }));

            MobileUI.core.SuiteManager.showDialog('boolEditor');
        }, true);
    }
});
