/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.LoadMaskEx', {
    extend: 'Ext.LoadMask',
    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },
    config: {
        allowEvents: false
    },
    xtype: 'loadmaskex',

    /**
     * Method returns template object
     * @returns {{reference: string, cls: string, children: *[]}[]}
     */
    getTemplate: function() {
        var prefix = Ext.baseCSSPrefix;

        return [
            {
                reference: 'innerElement',
                cls: prefix + 'mask-inner',
                children: [
                    {
                        reference: 'indicatorElement',
                        cls: prefix + 'loading-spinner-outer',
                        html: this.getSpinnerHtml()
                    },
                    {
                        reference: 'messageElement'
                    }
                ]
            }
        ];
    },

    onEvent: function(e) {
        if (!this.getAllowEvents())
        {
            return this.callParent(arguments, e);
        }
    }
});