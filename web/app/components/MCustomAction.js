/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.MCustomAction', {
    requires: ['MobileUI.core.sandbox.WebWorkerSandbox'],
    enableSessionEvents: true,
    processInnerHeight: function () {
        if (!this.heightIsSet && this.setHeight) {
            var el = this.element.dom;
            var childNodes = Array.prototype.slice.call(el.childNodes).filter(function (c) {
                return c.nodeType === 1 && !c.classList.contains('x-paint-monitor');
            });
            if (childNodes.length === 1) {
                var height = childNodes[0].offsetHeight;
                if (height === 0) {
                    var task = Ext.create('Ext.util.DelayedTask', function () {
                        var height = this.child.offsetHeight;
                        this.self.setHeight(height);
                        this.self.fireEvent('sizeUpdated');
                    }, {self: this, child: childNodes[0]});
                    task.delay(100);
                }
                else {
                    this.setHeight(height);
                    this.fireEvent('sizeUpdated');
                }
            }
            this.on('resize', function () {
                this.processInnerHeight();
            }, this);

        }
    },
    processInnerHtml: function (parent) {
        var uiActions = parent.getAttribute('ui-actions');
        var id = parent.getAttribute('id') || parent.getAttribute('name');
        if (uiActions) {
            uiActions.split(',').forEach(function (uiAction) {
                parent.addEventListener(uiAction, function (e) {
                    var clone = {};
                    for (var key in e) {
                        if (e.hasOwnProperty(key)) {
                            if (Ext.isObject(e[key])) {
                                clone[key] = Ext.clone(e[key]);
                            }
                            else if (Ext.isString(e[key]) || Ext.isBoolean(e[key]) || Ext.isNumber(e[key]) || Ext.isDate(e[key])) {
                                clone[key] = e[key];
                            }
                        }
                    }
                    this.sandbox.postMessage({
                        action: 'event',
                        type: 'uiAction',
                        data: {type: uiAction, id: id, event: clone}
                    });
                }.bind(this));
            }, this);

            parent.removeAttribute('ui-actions');
        }

        for (var i = 0; i < parent.childNodes.length; i++) {
            if (parent.childNodes[i].nodeType === 1) //Node.ELEMENT_NODE
            {
                this.processInnerHtml(parent.childNodes[i]);
            }
        }

    },

    createSandbox: function () {
        return Ext.create('MobileUI.core.sandbox.WebWorkerSandbox');
    },

    initAction: function () {
        this.heightIsSet = !!this.configuration.height;

        if (this.configuration.action) {
            this.files = this.configuration.action.files || [];
            this.sandbox = this.createSandbox();
            this.sandbox.initApi(this.configuration.action.permissions || [], this.configuration);
            this.sandbox.include(this.files);
            var api = this.sandbox.getApi();

            if (this.setHidden) {
                api.on('changeVisibility', function (e) {
                    this.setHidden(!e.visibility);
                }, this);
            }

            if (this.setHtml) {

                api.on('changeHtml', function (e) {
                    var data = e,
                        tag;
                    if (data.id) {
                        tag = document.getElementById(data.id);
                    }
                    else {
                        tag = this.element.dom;
                    }
                    if (tag) {
                        if (window.MutationObserver) {
                            var observer = new window.MutationObserver(function (mutations) {
                                mutations.forEach(function(mutation){
                                    if (mutation.type === 'childList'){
                                        observer.disconnect();
                                        this.processInnerHtml(tag);
                                        this.processInnerHeight();
                                    }
                                },this);
                            }.bind(this));
                            observer.observe(tag, {childList: true, subtree: true});
                            if (data.id) {
                                tag.innerHTML = data.html;
                            } else {
                                this.setHtml(data.html);
                            }
                        }
                        else {
                            if (data.id) {
                                tag.innerHTML = data.html;
                            } else {
                                this.setHtml(data.html);
                            }
                            var task = Ext.create('Ext.util.DelayedTask', function () {
                                this.processInnerHtml(tag);
                                this.processInnerHeight();
                            }, this);
                            task.delay(100);//Hate this solution, but we need to give DOM some time to process HTML
                        }
                    }
                }, this);

                this.on('painted', function () {
                    var tag = this.element.dom;
                    if (tag) {
                        this.processInnerHtml(tag);
                    }
                }, this, {single: true});
            }

            if (this.setHeight) {
                api.on('changeSize', function (e) {
                    var data = e;
                    if (data.height !== undefined) {
                        this.setHeight(data.height);
                        this.setMaxHeight(data.height);
                        this.setMinHeight(data.height);
                        this.heightIsSet = !!data.height;
                        this.fireEvent('sizeUpdated');
                    }
                }, this);
            }


            this.on('execute', function (e) {
                this.sandbox.postMessage({
                    action: 'event',
                    type: 'execute',
                    data: null
                });
                e.stop();
            }, this);

            if (this.enableSessionEvents) {
                MobileUI.core.session.Session.on('updateEntity', function () {
                    var entity = MobileUI.core.session.Session.getEntity();
                    this.sandbox.postMessage({
                        action: 'event',
                        type: 'updateEntity',
                        data: entity
                    });
                }, this);

                var searchEngine = MobileUI.core.session.Session.getSearchEngine();
                searchEngine.on('changeSearchQuery', function (e) {
                    var query = e.query;
                    this.sandbox.postMessage({
                        action: 'event',
                        type: 'changeSearchQuery',
                        data: query
                    });
                }, this);
            }
        }
        this.on('activated', function () {
            this.sandbox.postMessage({
                action: 'event',
                type: 'changeVisibility',
                data: true
            });
        }, this);
        this.on('deactivate', function () {
            this.sandbox.postMessage({
                action: 'event',
                type: 'changeVisibility',
                data: false
            });
        }, this);
    }


})
;