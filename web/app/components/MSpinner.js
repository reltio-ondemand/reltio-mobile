/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.MSpinner', {
    statics: {
        /** @private */
        spinnersCache: {}
    },

    getSpinnerHtml: function(color) {
        color = color || '#666666';
        var cached = MobileUI.components.MSpinner.spinnersCache[color];
        if (!cached) {
            cached = MobileUI.components.MSpinner.spinnersCache[color] =
            '<div class="spinner"><div class="uil-default-css" style="-webkit-transform:scale(0.16)">' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:' + color + ';-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '</div></div>';
        }
        return cached;
    }
});