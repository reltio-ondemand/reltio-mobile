/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Map', {
    extend: 'MobileUI.components.ExpandableContainer',
    xtype: 'reltiomap',
    requires: ['Ext.Map'],
    map: null,
    mapMarkers: null,
    loaded: null,
    markers: null,
    initialize: function ()
    {
        this.callParent(arguments);
        this.map = Ext.create('Ext.Map', {
            flex: 1,
            mapOptions: {
                disableDefaultUI: true
            }
        });
        this.add(this.map);
        this.mapMarkers = [];
        Ext.Viewport.on('orientationchange', this.sizeChanged, this);
        google.maps.event.addListenerOnce(this.map.getMap(), 'idle', this.onMapLoaded.bind(this));
        this.on('expanded', this.sizeChanged, this);
        this.on('collapsed', this.sizeChanged, this);
        this.setMapMasked(true);
    },
    /**
     * @private
     * On size changed listener
     */
    sizeChanged: function ()
    {
        this.map.doResize();
    },
    /**
     * @private
     * on map loaded event
     */
    onMapLoaded: function()
    {
        this.loaded = true;
        if (this.markers)
        {
            this.setMarkersInternal(this.markers);
            this.markers = null;
        }
    },
    /**
     * @overridden
     * @param state {String} state
     */
    updateState: function (state)
    {
        this.callParent(arguments, state);
        this.setMapMasked(state === MobileUI.components.ExpandableContainer.COLLAPSED);
    },
    /**
     * Method sets markers on the map
     * @param markers {Array.<{label:String, point: Object}>} markers array
     */
    setMarkers: function (markers)
    {
        if (this.loaded)
        {
            this.setMarkersInternal(markers);
        }
        else
        {
            this.markers = markers;
        }
    },
    /**
     * @private
     * Method sets markers on the map
     * @param markers
     */
    setMarkersInternal: function(markers){
        var i;
        if (Ext.isArray(markers) && !Ext.isEmpty(markers))
        {
            this.map.getMap().setCenter(markers[0].point);
            for (i = 0; i < markers.length; i++)
            {
                this.mapMarkers.push(
                    new google.maps.Marker({
                        map: this.map.getMap(),
                        position: markers[i].point,
                        title: markers[i].label,
                        flat: true
                    }));
            }
        }
    },
    /**
     * Method set/remove masked to map
     * @param masked {Boolean} is masked or not
     */
    setMapMasked: function (masked)
    {
        this.map.setMasked(masked ? {
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: false,
            allowEvents: true
        } : null);
    },
    /**
     * @inheritdoc
     */
    destroy: function() {
        Ext.Viewport.un('orientationchange', 'sizeChanged', this);
        this.callParent(arguments);
    }
});
