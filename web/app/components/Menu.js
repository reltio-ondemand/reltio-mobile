/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Menu', {
    extend: 'Ext.Menu',
    requires: [
        'MobileUI.core.Navigation',
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.controller.CreateEntity'
    ],
    xtype: 'mainmenu',
    initialize: function() {
        this.callParent();
        MobileUI.core.session.Session.addListener('inited', this.updateUserName, this);
        this.on('painted', this.onPainted, this);
        this.on('hide', function() {
            this.shown = false;
        }, this);

        var cm = MobileUI.core.Services.getConfigurationManager();
        var plugin = cm.getExtensionById('com.reltio.mobile.dashboard.General');
        var homeButtonVisibile = plugin && plugin.items;
        var settings = MobileUI.core.Services.getGeneralSettings();

        this.setItems([
            {
                xtype: 'button',
                text: i18n('User Name'),
                name: 'UserName',
                cls: 'menu-profile-button',
                icon: 'http://reltio.images.s3.amazonaws.com/api/images/defaultImage/no-photo.png',
                ui: 'mainmenu',
                handler: function() {
                    var uri = MobileUI.components.Menu.userEntityUri;
                    if (uri) {
                        Ext.Viewport.toggleMenu('left');
                        MobileUI.core.Navigation.redirectForward('profile/' +
                            MobileUI.core.util.HashUtil.encodeUri(uri));
                    }
                }
            },
            {
                xtype: 'button',
                text: i18n('Home'),
                icon: 'resources/images/reltio/menu/home.svg',
                ui: 'mainmenu',
                hidden: !homeButtonVisibile,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                    MobileUI.core.Navigation.redirectForward('dashboard/init');
                }
            },
            {
                xtype: 'button',
                text: i18n('Search'),
                icon: 'resources/images/reltio/menu/search.svg',
                ui: 'mainmenu',
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                    MobileUI.core.Navigation.redirectForward('search/init');
                }
            },
            {
                xtype: 'button',
                text: i18n('Create Profile'),
                name: 'createProfile',
                icon: 'resources/images/reltio/menu/createProfile.svg',
                ui: 'mainmenu',
                hidden: true,
                handler: this.showCreateProfile,
                scope: this
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'button',
                text: i18n('Switch to full version'),
                icon: 'resources/images/reltio/menu/fullversion.svg',
                ui: 'mainmenu',
                hidden: settings.hideFullscreenLink,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                    MobileUI.core.Navigation.switchToFull();
                }
            },
            {xtype: 'component', cls: 'menu-separator'},
            {
                xtype: 'button',
                text: i18n('Feedback'),
                icon: 'resources/images/reltio/menu/feedback.svg',
                ui: 'mainmenu',
                hidden: settings.hideSendFeedback,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                    if (!MobileUI.core.SuiteManager.hasDialog('feedback')) {
                        var feedBack = Ext.create('MobileUI.view.Feedback');
                        MobileUI.core.SuiteManager.addDialog('feedback', feedBack);
                    }
                    MobileUI.core.SuiteManager.showDialog('feedback');
                    Ext.Viewport.setMasked(null);
                }
            },
            {xtype: 'component', cls: 'menu-separator'},
            {
                xtype: 'button',
                text: i18n('Logout'),
                icon: 'resources/images/reltio/menu/logout.svg',
                ui: 'mainmenu',
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                    MobileUI.core.util.Storage.removeItem('username');
                    MobileUI.core.session.Session.logout(function(){
                        MobileUI.core.Metadata.killMetadata();
                        if (MobileUI.core.util.Util.supportSSO()){
                            MobileUI.core.util.Util.redirect(MobileUI.core.util.Util.getLogoutURL());
                        } else {
                            MobileUI.core.Navigation.redirectForward('login');
                            window.location.reload();
                        }
                    }, this);
                    
                }
            }
        ]);
        if (MobileUI.core.Metadata.isInited()) {
            this.updateCreateProfileButton();
        } else {
            MobileUI.core.Metadata.addListener('init', this.updateCreateProfileButton, this, {single: true});
        }
    },

    statics: {
        userEntityUri: null
    },

    config: {
        cls: 'mainmenu',
        docked: 'left',
        top: 0,
        left: 0,
        bottom: 0,
        zIndex: 0,
        width: 266,
        padding: window.navigator.standalone ? '25 0 0 0' : '5 0 0 0',
        hidden: true,
        scrollable: 'vertical',
        defaultType: 'button',
        layout: {
            type: 'vbox'
        },
        defaults: {
            textAlign: 'left'
        },
        items: []
    },
    /**@type {Boolean} */
    shown: false,

    /**
     * Method returns true is component is shown
     * @returns {Boolean}
     */
    isShown: function() {
        return this.shown;
    },
    /**
     * On painted listener
     * @protected
     */
    onPainted: function() {
        var mask = Ext.create('Ext.Mask');
        mask.setCls('opaquemask');
        mask.on('tap', function() {
            Ext.Viewport.setMasked(null);
            Ext.Viewport.hideMenu('left');
        }, this, {single: true});
        Ext.Viewport.setMasked(mask);
        this.shown = true;
        this.updateUserName();
    },
    /**
     * Method updates the user name in the menu
     */
    updateUserName: function() {
        this.fillUserProfile();
        MobileUI.core.session.Session.getUserProfileEntity(this.fillUserProfile, this);
    },
    updateCreateProfileButton: function(){
        var createProfileButton = this.child('button[name=createProfile]');
        var types = MobileUI.core.Metadata.getEntityTypes(function (item) { return !item['abstract']; });
        types = Object.keys(types).map(function(name) { return types[name]; });
        var md = MobileUI.core.PermissionManager.securityService().metadata;
        var canCreate =  types.some(function(item) { return md.canCreate(item); });
        var canInitiateRequest = types.some(function(item) { return md.canInitiateRequest(item); });

        if (canCreate || canInitiateRequest) {
            createProfileButton.setHidden(false);
            if (canCreate) {
                createProfileButton.setText(i18n('Create Profile'));
            }
            else {
                createProfileButton.setText(i18n('Suggest New Profile'));
            }
        }
    },
    fillUserProfile: function(entity) {
        var button = this.child('button[name=UserName]');
        if (entity) {
            if (entity.label) {
                button.setText(entity.label);
            }
            var avatar = MobileUI.core.entity.EntityUtils.getImageAttributeValue(entity, entity.defaultProfilePic),
                entityType = MobileUI.core.Metadata.getEntityType(entity.type) || '',
                alternativeAvatar = MobileUI.core.Services.getBorderlessImageUrl(
                    MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
            var img = button.element.down('.x-button-icon');
            img.setStyle({
                'background-image': 'url(' + avatar + '), url(' + alternativeAvatar + ')',
                'background-size': 'cover'
            });
            MobileUI.components.Menu.userEntityUri = entity.uri;
        }
        else {
            var userName = MobileUI.core.session.Session.getUserName();
            button.setText(userName);
            button.setIcon('http://reltio.images.s3.amazonaws.com/api/images/defaultImage/no-photo.png');
        }
    },

    showCreateProfile: function(button) {
        var listener = function (entityTypeInfo) {
            var entityType = MobileUI.core.Metadata.getEntityType(entityTypeInfo.uri);
            var attrTypes = entityType.attributes.filter(function(attrType){
                return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
            });
            var entityUri =  MobileUI.core.entity.EntityUtils.generateUri(null, 'entities');
            var entity = {
                uri: entityUri,
                type: entityType.uri,
                attributes: MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, entityUri + '/attributes')
            };
            MobileUI.core.entity.TemporaryEntity.addEntity(entity);
            if (this.shouldInitLookups) {
                MobileUI.core.session.Session.getLookupsManager().initWithEntityType(entityType.uri);
            }

            MobileUI.core.Navigation.redirectForward('edit/' + MobileUI.core.util.HashUtil.encodeUri(entity.uri));
        };

        if (Ext.os.is.Phone) {
            Ext.Viewport.toggleMenu('left');
            MobileUI.controller.CreateEntity.showChooseEntityTypeDialog(true, listener);
            Ext.Viewport.setMasked(null);
        }
        else {
            var types = MobileUI.core.Metadata.getEntityTypes(function(item) {
                return !item['abstract'] && MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(item);
            });

            types = Object.keys(types).map(function (name) {
                return types[name];
            });
            var items = MobileUI.controller.CreateEntity.filterEntityTypes(types)
                .map(function (item) {
                    return {value: item.label, uri: item.uri};
                });

            if (!this.popup || this.popup.isDestroyed) {

                this.popup = Ext.create('MobileUI.components.Popup');

                this.popup.setWidth(300);
                this.popup.setHeight(40 * items.length);
                this.popup.setHideOnOrientationChange(false);
                var popup = this.popup;
                this.popup.add(items.map(function(item) {
                    return {
                        xtype: 'button',
                        cls: 'create-button popup-menu-item-button',
                        html: item.value,
                        handler: function() {
                            popup.hide();
                            listener(item);
                            Ext.Viewport.toggleMenu('left');
                            Ext.Viewport.setMasked(null);
                        }
                    };
                }));
            }

            this.popup.showBy(button, 'cl-cl?');
            this.popup.element.setLeft('10px');
        }
    }
});
