/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.MessageBox', {
    requires: ['Ext.MessageBox'],
    statics: {
        alert: function (title, message, fn, scope) {
            var msg = Ext.create('Ext.MessageBox');
            msg.defaultAllowedConfig.showAnimation = false;
            msg.alert(title, message, fn, scope);
            msg.on('painted', function () {
                var titleTextElement = Ext.select('.x-msgbox-title  .x-innerhtml').elements[0];
                var toolbar = msg.down('toolbar');
                var paddings = 3;
                if (toolbar && titleTextElement) {
                    toolbar.setMinHeight(titleTextElement.clientHeight + paddings);
                }
            }, this, {single: true});

            return msg;
        },
        show: function (initialConfig) {
            var msg = Ext.create('Ext.MessageBox');
            msg.defaultAllowedConfig.showAnimation = false;
            msg.show(initialConfig);
            return msg;
        }
    }
});
