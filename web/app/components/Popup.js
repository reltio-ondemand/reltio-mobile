/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Popup', {
    extend: 'Ext.Panel',
    requires: ['Ext.TitleBar'],
    xtype: 'popup',
    statics: {
        POPUP_WIDTH: 320,
        POPUP_HEIGHT: 455
    },
    config: {
        cls: 'x-popup',
        layout: 'vbox',
        hideOnMaskTap: true,
        modal: true,
        width: 159,
        items: [],
        hideOnOrientationChange: true
    },

    initialize: function() {
        this.callParent();
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
    },

    /**
     * Method sets the title
     * @param title {String} title
     */
    setTitle: function (title)
    {
        var titlebar = this.down('titlebar');
        if (title)
        {
            if (!titlebar)
            {
                titlebar = this.insertFirst({
                    docked: 'top',
                    xtype: 'titlebar',
                    title: i18n('Popup'),
                    ui: 'x-popup',
                    cls: 'toolbar-inv'
                });
            }
            this.setFilledAnchor(true);
            titlebar.setTitle(title);
        }
        else
        {
            if (titlebar)
            {
                this.setFilledAnchor(false);
                this.remove(titlebar, true);
            }
        }
    },
    /**
     * Method sets the anchor filled the gray color
     * @param isFilled {Boolean} is anchor filled or not
     */
    setFilledAnchor: function (isFilled)
    {
        if (this.tipElement)
        {
            if (isFilled)
            {
                this.tipElement.addCls('x-anchor-background');
            }
            else
            {
                this.tipElement.removeCls('x-anchor-background');
            }
        }
    },
    /**
     * @override
     */
    alignTo: function (component, alignment)
    {
        this.callParent(arguments);
        this.cmp = component;
        this.alignment = alignment;
        if (alignment === 'tr-bc')
        {
            this.tipElement.setLeft(this.tipElement.getLeft() - 5);
        }
    },

    handleOrientationChange: function () {
        if (!this.getHideOnOrientationChange() && this.cmp && this.alignment)
        {
           this.alignTo(this.cmp, this.alignment);
        }
        else
        {
            this.hide();
        }
    }

});
