/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.ProfileBand', {
    extend: 'Ext.Container',
    xtype: 'profileband',
    mixins: {observable:'Ext.mixin.Observable'},

    requires: [
        'Ext.Img',
        'Ext.Component',
        'Ext.mixin.Observable'
    ],
    config: {
        layout: 'vbox',
        cls: 'profileband'
    },

    /**@type {Ext.Component} @private */
    backgroundBox: null,
    /**@type {Ext.Img} @private */
    avatar: null,
    /**@type {Ext.Component} @private */
    titleLine: null,
    /**@type {Ext.Component} @private */
    subtitleLine: null,
    /**@type {Ext.Component} @private */
    infoLine: null,
    /**@type {Ext.Component} @private */
    favorite: null,
    /**@type {Boolean} @private */
    tapOnFavoriteDisabled: null,

    /**
     * On init function.
     * @protected
     */
    initialize: function() {
        this.backgroundBox = Ext.create('Ext.Component', {cls: 'profile-background'});
        this.avatar = Ext.create('Ext.Img', {cls: 'profile-avatar'});
        this.favorite = Ext.create('Ext.Img', {cls: 'profile-favorite', mode: 'img', hidden: true});
        this.favorite.on('tap', this.onFavoriteTap, this);
        this.titleLine = Ext.create('Ext.Component', {cls: 'profile-title'});
        this.subtitleLine = Ext.create('Ext.Component', {cls: 'profile-subtitle'});
        this.infoLine = Ext.create('Ext.Component', {cls: 'profile-info'});

        //this.add([
        //    this.backgroundBox,
        //    this.favorite,
        //    this.avatar,
        //    this.titleLine,
        //    this.subtitleLine,
        //    this.infoLine
        //]);
        this.add([
            this.backgroundBox,
            //this.favorite,
            this.avatar,
            this.titleLine,
            this.subtitleLine,
            this.infoLine
        ]);
    },

    /**
     * @param title {String?}
     * @param subtitle {String?}
     * @param info {String?}
     */
    setText: function(title, subtitle, info) {
        title = MobileUI.core.entity.EntityUtils.processLabel(title);
        this.titleLine.setHtml(Ext.util.Format.htmlEncode(title));
        this.subtitleLine.setHtml(Ext.util.Format.htmlEncode(subtitle || ''));
        this.infoLine.setHtml(Ext.util.Format.htmlEncode(info || ''));
    },

    /**
     * Method sets image url
     * @param imgUrl {String} img
     * @param alternativeImg  {String} img
     */
    setAvatar: function(imgUrl, alternativeImg) {
        this.avatar.on('error', function() {
            this.avatar.setSrc(alternativeImg);
        }, this, { single: true, addOnce: true });

        this.avatar.setSrc(imgUrl || alternativeImg);
        this.avatar.setStyle({'background-size': 'cover'});
    },

    onFavoriteTap: function ()
    {
        if (!this.tapOnFavoriteDisabled)
        {
            var state = !this.isFavorite;
            this.setFavorite(state);
            this.tapOnFavoriteDisabled = true;
            this.fireEvent('favorited', state);
        }
    },

    setFavorite: function(isFavorite) {
        this.isFavorite = isFavorite;
        this.tapOnFavoriteDisabled = false;
        if (isFavorite)
        {
            this.favorite.setSrc('resources/images/reltio/profile/favorite-active.svg');
        } else
        {
            this.favorite.setSrc('resources/images/reltio/profile/favorite.svg');
        }
        this.favorite.setHidden(false);
    },

    /**
     * Method sets badge color
     * @param color {String} color
     */
    setBadgeColor: function(color) {
        this.backgroundBox.setStyle({'background-color': color});
    }
});
