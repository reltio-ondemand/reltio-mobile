/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.ProfileFacets', {
    extend: 'Ext.Container',
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.core.Navigation',
        'MobileUI.core.util.Util'
    ],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    xtype: 'profilefacets',
    config: {
        cls: 'profilefacets',
        layout: 'hbox',
        scrollable: {
            direction: 'horizontal',
            directionLock: true,
            indicators: false
        }
    },
    facets: [],

    setFacets: function (facets)
    {
        this.facets = facets || [];
        this.refresh();
    },
    
    refresh: function() {
        this.removeAll();
        
        var facets = this.facets.filter(function(item){
            return item.alwaysVisible || item.badgeText > 0;
        });
        this.add(facets.map(function(facet) {
            var component = Ext.create('Ext.Component', {
                xtype: 'component',
                width: 110,
                html: ['<div class="facet">',
                    '<div class="image-wrapper"><img src="', facet.image, '" onerror="this.onerror=null;this.src=\'',facet.defaultImage,'\';" /></div>',
                    '<div class="facet-caption">', i18n(facet.caption), '</div>',
                    '<div class="facet-badge" />', i18n(facet.badgeText), '</div>',
                    '</div>'].join('')
            });
            component.element.on('tap', function() {
                MobileUI.core.Navigation.redirectForward(facet.action);
            });
            return component;
        }));
        this.show();

        var self = this;
        window.requestAnimationFrame(function() {
            self.setHeight(self.getItems().items.reduce(function(height, item) {
                return Math.max(height,
                    parseFloat(window.getComputedStyle(item.element.dom.firstChild).height));
            }, 0));
        });
    }
});
