/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Recaptcha', {
    extend: 'Ext.Container',
    xtype: 'recaptcha',
    requires: ['Ext.mixin.Observable'],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    config: {
        border: false,
        html: '<div id="recaptcha_widget"></div>',
        listeners: {
            painted: {
                fn: function ()
                {
                    grecaptcha.render('recaptcha_widget', {
                        sitekey:'6LdHYQkTAAAAAPMxp17Y8BDFiNs0Et2qprPC4pEB',
                        callback: this.setValue.bind(this),
                        theme: 'light'
                    });
                }
            }
        }
    },
    setValue: function(value){
        this.value = value;
    },
    /**
     * Method returns the captcha value
     * @returns {String}
     */
    getValue: function ()
    {
        return this.value;
    },
    /**
     * Method focuses the element
     */
    focus: function ()
    {
        Ext.get('recaptcha_response_field').dom.focus();
    }
});