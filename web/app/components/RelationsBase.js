/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.RelationsBase', {
    extend: 'Ext.Container',
    requires: ['MobileUI.core.util.Util'],
    config: {
        cls: 'relationsfacet',
        layout: 'hbox'
    },
    /**@type {Object} @private}*/
    facet: null,

    /**
     * Method sets the facet and count
     * @param facet
     */
    setFacet: function (facet)
    {
        this.facet = facet;
        this.refresh();
    },
    /**
     * Method decreases items count
     */
    decreaseItemsCount: function ()
    {
        this.facet.total -= 1;
        this.refresh();
    },
    /**
     * Method increases items count
     */
    increaseItemsCount: function ()
    {
        if (!Ext.isNumber(this.facet.total))
        {
            this.facet.total = 0;
        }
        this.facet.total += 1;
        this.refresh();
    },
    /**
     * Method returns the item colors class
     */
    getItemClass: function ()
    {
        return 'relationsfacet-normal-item';
    },

    /**
     * Method refreshes the facets component
     * @protected
     */
    refresh: function ()
    {
        var cls = ['relationsfacet-item'];
        cls.push(this.getItemClass());
        if (this.facet !== null)
        {
            if (!Ext.isNumber(this.facet.total))
            {
                this.facet.total = 0;
            }
            this.setHidden(false);
            this.add({
                xtype: 'component',
                cls: cls,
                flex: 1,
                html: '<span class = "caption">' + (i18n(this.facet.caption) || '') + '</span> <span class ="badge">' + i18n(this.facet.total) + '</span>'
            });
        }
    }
});
