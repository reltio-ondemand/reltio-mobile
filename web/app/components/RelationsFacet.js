/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.RelationsFacet', {
    extend: 'MobileUI.components.RelationsBase',
    xtype: 'relationsfacet',
    requires: ['Ext.mixin.Observable', 'Ext.Button', 'Ext.util.DelayedTask'],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    statics: {
        FILTER_MODE: 'filter',
        NORMAL_MODE: 'normal'
    },
    /**@type {Object} @private}*/
    cancelButton: null,
    /**@type {Object} @private}*/
    filterButton: null,
    /**@type {Boolean} @private}*/
    filterEnabled: null,
    /**@type {Object} @private}*/
    filterField: null,
    /**@type {String} @private}*/
    mode: null,
    /**@type {String} @private}*/
    filter: null,

    /**
     * Method sets filter
     * @param filter {String} filter
     */
    setFilter: function (filter)
    {
        this.filter = filter;
        this.refresh();
    },

    /**
     * Method returns the item colors class
     */
    getItemClass: function ()
    {
        return 'relationsfacet-gray-item';
    },
    /**
     * Method switch editor to normal mode and fire the event
     */
    switchToNormal: function ()
    {
        this.mode = MobileUI.components.RelationsFacet.NORMAL_MODE;
        this.fireEvent('applyFilter', this.filterField.getValue());
    },
    getFilterField: function ()
    {
        if (Ext.isEmpty(this.filterField) || this.filterField.isDestroyed)
        {

            this.filterField = Ext.create('MobileUI.components.TrueSearchField');
            this.filterField.setCls('search-field');
            this.filterField.setPlaceHolder(i18n('Filter'));
            if (!Ext.os.is.Phone)
            {
                var task = Ext.create('Ext.util.DelayedTask', this.switchToNormal);
                task.setScope(this);
                this.filterField.on('blur', function ()
                {
                    this.task.delay(300);
                }, {task: task});

                this.filterField.on('clearicontap', function ()
                {
                    this.task.cancel();
                    this.self.filterField.focus();
                }, {task: task, self: this});

                this.filterField.on('action', function ()
                {
                    this.task.cancel();
                }, {task: task, self: this});
            }
            this.filterField.on('action', function ()
            {
                this.mode = MobileUI.components.RelationsFacet.NORMAL_MODE;
                this.fireEvent('applyFilter', this.filterField.getValue());
            }, this);
        }
        return this.filterField;
    },
    /**
     * Method sets the filter state
     * @param {Boolean} state
     */
    setFilterEnabled: function (state)
    {
        this.filterEnabled = state;
        this.updateIcon();
    },
    /**
     * Method updates the filter icon
     * @private
     */
    updateIcon: function ()
    {
        if (!(Ext.isEmpty(this.filterButton) || this.filterButton.isDestroyed))
        {
            this.filterButton.setIcon(this.filterEnabled === true ? 'resources/images/reltio/relations/filter-sel.svg' : 'resources/images/reltio/relations/filter.svg');
        }
    },

    getFilterComponent: function ()
    {
        var textfield;
        if (Ext.os.is.Phone)
        {
            if (Ext.isEmpty(this.filterButton) || this.filterButton.isDestroyed)
            {
                this.filterButton = Ext.create('Ext.Button');
                this.updateIcon();
                this.filterButton.setCls('reltio-relation-button');
                this.filterButton.on('tap', function ()
                {
                    this.mode = MobileUI.components.RelationsFacet.FILTER_MODE;
                    this.refresh();
                }, this);
            }
            return this.filterButton;
        }
        else
        {
            textfield = this.getFilterField();
            textfield.setWidth(250);
            return textfield;
        }
    },
    getCancelButton: function ()
    {
        if (Ext.isEmpty(this.cancelButton) || this.cancelButton.isDestroyed)
        {
            this.cancelButton = Ext.create('Ext.Button');
            this.cancelButton.setText(i18n('Close'));
            this.cancelButton.setCls('relation-cancel-button');
            this.cancelButton.on('tap', function ()
            {
                if (Ext.os.is.Phone)
                {
                    this.fireEvent('applyFilter', this.filterField.getValue());
                }
                this.mode = MobileUI.components.RelationsFacet.NORMAL_MODE;
                this.refresh();
            }, this);
        }
        return this.cancelButton;
    },
    /**
     * Method refreshes the facets component
     * @private
     */
    refresh: function ()
    {
        var textfield, filterd;
        this.removeAll();
        if (this.mode !== MobileUI.components.RelationsFacet.FILTER_MODE)
        {
            if (this.facet !== null)
            {
                filterd = this.getFilterComponent();
                this.callParent();
                this.add({
                    xtype: 'container',
                    layout: 'vbox',
                    items: [
                        {xtype: 'spacer'},
                        filterd,
                        {xtype: 'spacer'}
                    ]
                });
                if (!Ext.os.is.Phone)
                {
                    filterd.setValue(this.filter || '');
                }
            }
        }
        else
        {
            textfield = this.getFilterField();
            this.add(
                {
                    xtype: 'container',
                    layout: 'vbox',
                    items: [
                        {xtype: 'spacer'},
                        textfield,
                        {xtype: 'spacer'}
                    ],
                    flex: 1
                });
            textfield.setValue(this.filter || '');
            this.add(this.getCancelButton());
        }
    }
});
