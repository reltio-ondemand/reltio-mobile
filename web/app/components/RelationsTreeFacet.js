/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.RelationsTreeFacet', {
    extend: 'MobileUI.components.RelationsBase',
    requires: ['Ext.mixin.Observable'],
    xtype: 'relationstreefacet',
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    /**@type {Ext.Button} @private */
    navigationButton: null,
    getNavigationButton: function ()
    {
        if (Ext.isEmpty(this.navigationButton) || this.navigationButton.isDestroyed)
        {
            this.navigationButton = Ext.create('Ext.Button');
            this.navigationButton.setIcon('resources/images/reltio/tree/navigation-tree.svg');
            this.navigationButton.setCls(['reltio-relation-button', 'reltio-relation-tree-button']);
            this.navigationButton.on('tap', function ()
            {
                this.fireEvent('navigate');
            }, this);
        }
        return this.navigationButton;
    },

    /**
     * Method returns the item colors class
     */
    getItemClass: function()
    {
        return 'relationsfacet-gray-item';
    },
    /**
     * Method refreshes the facets component
     * @protected
     */
    refresh: function ()
    {
        this.removeAll();
        this.callParent();

        this.add({xtype: 'spacer'});
        this.add(this.getNavigationButton());
    }
});