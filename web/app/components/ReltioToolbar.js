/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.ReltioToolbar', {
    extend: 'Ext.Toolbar',
    xtype: 'reltiotoolbar',
    config: {
        cls: 'toolbar reltio',
        docked: 'top'
    },
    initialize: function() {
        this.callParent();
        var phoneItems = [
            {
                icon: 'resources/images/reltio/toolbar/menu.svg',
                name: 'menuBtn',
                cls: 'icon-button'
            },
            {
                icon: 'resources/images/reltio/toolbar/back.svg',
                name: 'backToolbarBtn',
                cls: 'icon-button',
                hidden: true
            },
            {
                xtype: 'spacer'
            },
            {
                icon: 'resources/images/reltio/toolbar/search.svg',
                name: 'searchBtn',
                cls: 'icon-button'
            },
            {
                icon: 'resources/images/reltio/toolbar/gf-empty.svg',
                name: 'globalFilterMenu',
                cls: 'icon-button'
            },
            {
                icon: 'resources/images/reltio/toolbar/menu2.svg',
                name: 'additionalMenu',
                cls: 'icon-button',
                hidden: true
            }
        ];
        var tabletItems = [
            {
                icon: 'resources/images/reltio/toolbar/menu.svg',
                name: 'menuBtn',
                cls: 'icon-button'
            },
            {
                icon: 'resources/images/reltio/toolbar/back.svg',
                name: 'backToolbarBtn',
                cls: 'icon-button',
                hidden: true
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'truesearchfield',
                cls: 'search-field',
                placeHolder: i18n('Search'),
                name: 'search',
                width: 250
            },
            {
                xtype: 'spacer'
            },
            {
                icon: 'resources/images/reltio/toolbar/gf-empty.svg',
                name: 'globalFilterMenu',
                cls: 'icon-button'
            },
            {
                icon: 'resources/images/reltio/toolbar/menu2.svg',
                name: 'additionalMenu',
                cls: 'icon-button',
                hidden: true
            }
        ];
        this.setItems(Ext.os.is.Phone ? phoneItems : tabletItems);
    }

});
