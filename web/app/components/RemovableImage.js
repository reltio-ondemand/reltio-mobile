/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.RemovableImage', {
    extend: 'Ext.Img',
    requires: ['Ext.mixin.Observable', 'MobileUI.components.FileUpload'],
    include: ['Ext.mixin.Observable'],
    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },
    xtype: 'removableImage',
    config: {
        cls: 'removable-image',
        ui: 'removable-image',
        width: 100,
        height: 100
    },
    removeIcon: null,
    spinnerIcon: null,
    initialize: function ()
    {
        this.removeIcon = this.element.createChild({tag: 'img'});
        this.removeIcon.dom.setAttribute('src', 'resources/images/reltio/feedback/trash.svg');
        this.removeIcon.dom.classList.add('delete-icon');
        this.removeIcon.on({
            tap: 'onTap',
            scope: this
        });
        this.spinnerIcon = this.element.createChild({
            tag: 'div',
            cls: 'spinner-icon',
            html: this.getSpinnerHtml()
        });
        this.on('load', function ()
        {
            this.spinnerIcon.destroy();
            this.spinnerIcon = null;
        }, this, {single: true});
    },
    /**
     * On tap listener
     */
    onTap: function ()
    {
        this.fireEvent('remove', this.getSrc());
        this.setHidden(true);
        this.destroy();
    },
    /**
     * @destroy
     */
    destroy: function ()
    {
        Ext.destroy(this.removeIcon);
        if (this.spinnerIcon)
        {
            Ext.destroy(this.spinnerIcon);
        }
        this.callParent();
    }

});