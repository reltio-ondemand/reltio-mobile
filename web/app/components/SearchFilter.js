/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.SearchFilter', {
    extend: 'Ext.Panel',
    requires: ['MobileUI.core.search.SearchParametersManager'],
    xtype: 'searchfilter',
    config: {
        cls: 'searchfilter',
        layout: 'hbox'
    },
    searchableTypes:[],

    initialize: function() {
        this.callParent(arguments);
        this.searchableTypes =  Object.keys(MobileUI.core.Metadata.getEntityTypes(function(item){
            return item.searchable !== false && !item['abstract'];
        }));
        this.element.on('tap', this.onTap, this);
        this.refresh();
    },

    onTap: function(event, element) {
        if (element.classList.contains('saved-search-name')) {
            var savedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch();
            MobileUI.controller.dialogs.EditSavedSearch.show(savedSearch.uri);
        }
    },

    refresh: function() {
        var keyword = MobileUI.core.search.SearchParametersManager.getKeyword(),
            params = MobileUI.core.search.SearchParametersManager.buildSearchParameters(keyword),
            savedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch(),
            searchFilter = this,
            i, filterFunction = function(item){
                return this.values.indexOf(item) === -1;
            };

        this.removeAll();
        for (i = 0; i< params.length; i++){
            if (params[i].fieldName === MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME)
            {
                var allSearchable = this.searchableTypes.filter(filterFunction, params[i]).length === 0;

                if (allSearchable)
                {
                    params.splice(i,1);
                }
            }
        }
        var items = [];

        if (savedSearch.name) {

            var user = MobileUI.core.session.Session.getUser(),
                fullName = Ext.isEmpty(user) ? '' : user.full_name,
                owned = (savedSearch.owner === fullName);

            items.push({
                xtype: 'component',
                html: '<div class="filter saved-search">' +
                '<span class="title">' + i18n('Saved Search') +':</span>' +
                '<span class="' + (owned ? 'saved-search-name' : '') + '">' + savedSearch.name + '</span>' +
                '</div>'
            });
        }

        if (params.length > 0) {
            items.push({
                xtype: 'component',
                html: MobileUI.components.SearchFilter.generateHtml(params)
            });
        }

        if (items.length > 0) {
            this.removeCls('hidden');
            this.add([
                {
                    xtype: 'container',
                    layout: 'vbox',
                    items: items,
                    flex: 1
                },
                {
                    xtype: 'button',
                    cls: 'clear-button',
                    text: i18n('Reset'),
                    handler: function() {
                        searchFilter.fireEvent('clear');
                    }
                }
            ]);
        }
        else {
            this.addCls('hidden');
        }
    },

    statics: {
        generateHtml: function(params) {
            if (!MobileUI.components.SearchFilter.filterMap) {
                MobileUI.components.SearchFilter.filterMap = {
                    'equals': i18n(':'),
                    'not equals': i18n('not:'),
                    'exists': i18n('exists'),
                    'fullText': i18n('full text:'),
                    'hasAll': i18n('has all:'),
                    'in': i18n('in:'),
                    'notIn': i18n('not in:'),
                    'missing': i18n('missing value'),
                    'range': i18n('in range:'),
                    'not range': i18n('not in range:'),
                    'startsWith': i18n('starts with:'),
                    'count.equals': i18n('count equals'),
                    'count.not.equals': i18n('count not equal'),
                    'count.less': i18n('count less'),
                    'count.less.or.equals': i18n('count less or equal'),
                    'count.greater': i18n('count greater'),
                    'count.greater.or.equals': i18n('count greater or equal'),
                    'count.range': i18n('count in range')
                };
            }
            return (params || []).map(function(param) {
                var filterWord = MobileUI.components.SearchFilter.filterMap[param.filter] || ':';
                return '<div class="filter">' +
                    (((param.title || '').length === 0) ? '' :
                        ('<span class="title' + (filterWord === ':' ? ' no-word' : '') + '">' + i18n(Ext.util.Format.htmlEncode(param.title)) + '</span>')) +

                    '<span class="filter-word">' + filterWord +
                    '</span>' +

                    param.labels.
                        map(function(label) {
                            if (Ext.isString(label))
                            {
                                label =Ext.util.Format.htmlEncode(label).replace(/&amp;ndash;/g, '&ndash;');
                            }
                            return '<span class="label">' + label + '</span>';
                        }).
                        join(', ') +
                    '</div>';
            }, this).join('');
        },

        filterMap: null
    }
});

