/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.Selectbox', {
    extend: 'Ext.Component',
    xtype: 'reltio-selectbox',
    mixins: {observable:'Ext.mixin.Observable'},
    config: {
        cls: 'reltio-selectbox',
        label: 'About',
        required: false,
        options: null
    },
    /**
     * @param {Array} options
     */
    updateOptions: function () {
        this.refresh();
    },
    initElement: function() {
        var me = this;

        me.callParent();

        me.options.on({
            scope: me,
            change: 'changeValue'
        });
    },

    changeValue: function () {
        this.fireEvent('change',this.options.dom.value);
    },
    getTemplate: function () {
        var items = [
            {
                cls: 'inner-selectbox',
                children: [
                    {
                        tag: 'div',
                        children: [
                            {
                                reference: 'label',
                                tag: 'span',
                                cls: this.getRequired() ? 'required': ''
                            },
                            {
                                reference: 'options',
                                tag: 'select'
                            },
                            {
                                reference: 'img',
                                tag: 'img',
                                cls: 'arrow',
                                src: 'resources/images/reltio/profile/arrow-right.svg'
                            }]
                    }
                ]
            }
        ];

        return items;
    },
    /**
     * Method refreshes the crumb component
     * @protected
     */
    refresh: function () {
        this.label.setHtml(this.getLabel() || '');
        if (!Ext.isEmpty(this.getOptions()) && this.options.dom.children.length === 0) {
            var options = this.getOptions().map(function (item) {
                var result = document.createElement('option');
                result.value = item.value;
                result.text = item.text;
                result.selected = item.value === this.defaultValue;
                return result;
            }, this);

            options.forEach(function (option) {
                this.options.dom.add(option);
            }, this);
        }
    },
    getValue: function () {
       return this.options.getValue();
    },
    setValue: function (value) {
        this.defaultValue = value;
        Array.prototype.slice.call(this.options.dom.children).forEach(function(option){
            option.selected = option.value === value;
        });
        this.fireEvent('change', value);
    },

    updateLabel: function () {
        this.refresh();
    }
});
