/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.StyledSelect', {
    extend: 'Ext.field.Select',
    xtype: 'styledselectfield',

    config: {
        /**
         * @cfg {String/Number} htmlDisplayField The underlying {@link Ext.data.Field#name data value name} (or numeric Array index) to bind to this
         * Select control. This resolved value is the visibly rendered value of the available selection options when `useHtml` set to `true`.
         * @accessor
         */
        htmlDisplayField: 'html',

        /**
         * @cfg {Boolean} useHtml
         * Use html tags in item list and htmlDisplayField
         */
        useHtml: false
    },

    // @private
    getPhonePicker: function() {
        var config = this.getDefaultPhonePickerConfig();

        if (!this.picker) {
            this.picker = Ext.create('Ext.picker.Picker', Ext.apply({
                slots: [
                    {
                        align: this.getPickerSlotAlign(),
                        name: this.getName(),
                        valueField: this.getValueField(),
                        displayField: this.getDisplayField(),
                        htmlField: this.getHtmlDisplayField(),
                        value: this.getValue(),
                        store: this.getStore()
                    }
                ],
                listeners: {
                    select: this.onListItemSelect,
                    change: this.onPickerChange,
                    scope: this
                }
            }, config));
        }

        return this.picker;
    },

    // @private
    getTabletPicker: function() {
        var config = this.getDefaultTabletPickerConfig();

        if (!this.listPanel) {
            this.listPanel = Ext.create('Ext.Panel', Ext.apply({
                left: 0,
                top: 0,
                modal: true,
                cls: Ext.baseCSSPrefix + 'select-overlay',
                layout: 'fit',
                hideOnMaskTap: true,
                width: Ext.os.is.Phone ? '14em' : '18em',
                height: (Ext.os.is.BlackBerry && Ext.os.version.getMajor() === 10) ? '12em' : (Ext.os.is.Phone ? '12.5em' : '22em'),
                items: {
                    xtype: 'list',
                    store: this.getStore(),
                    itemTpl: '<span class="x-list-label">{' +
                    (this.getUseHtml() ? this.getHtmlDisplayField() : (this.getDisplayField() + ':htmlEncode')) + '}</span>',
                    listeners: {
                        select: this.onListItemSelect,
                        itemtap: this.onListTap,
                        scope: this
                    }
                }
            }, config));
        }

        return this.listPanel;
    },

    applyStore: function(store) {
        if (store === true) {
            store = Ext.create('Ext.data.Store', {
                fields: [this.getValueField(), this.getDisplayField(), this.getHtmlDisplayField()],
                autoDestroy: true
            });
        }

        if (store) {
            store = Ext.data.StoreManager.lookup(store);

            store.on({
                scope: this,
                addrecords: 'onStoreDataChanged',
                removerecords: 'onStoreDataChanged',
                updaterecord: 'onStoreDataChanged',
                refresh: 'onStoreDataChanged'
            });
        }

        return store;
    },

    /**
     * @private
     */
    applyValue: function(value) {
        var record = value,
            index, store;

        //we call this so that the options configruation gets intiailized, so that a store exists, and we can
        //find the correct value
        this.getOptions();

        store = this.getStore();

        if ((value && !value.isModel) && store) {
            index = store.find(this.getValueField(), value, null, null, null, true);

            if (index === -1) {
                index = store.find(this.getDisplayField(), value, null, null, null, true);
            }

            record = store.getAt(index);
        }

        if (record && record.get(this.getValueField()) === null) {
            return null;
        }
        if (record && record.get('value') === this.getValue()) {
            return null;
        }
        return record;
    },

    onListItemSelect: function (item, record) {
        var me = this;
        if (record) {
            if (!this.preventSelection) {
                me.setValue(record);
            }
        }
    },

    onListTap: function () {
        var list = this.listPanel.down('list');
        this.preventSelection = true;
        list.deselect(list.getSelection(), true);
        this.preventSelection = false;
        this.listPanel.hide({
            type: 'fade',
            out: true,
            scope: this
        });
    }
});
