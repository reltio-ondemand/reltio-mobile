/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.TripleToggle', {
    extend: 'Ext.Component',
    xtype: 'tripletoggle',

    config: {
        cls: 'tripletoggle',
        value: null,
        defined: false,
        yes: 'Yes',
        no: 'No',
        disabled: false
    },

    /** @private */
    initialize: function() {
        this.callParent(arguments);
        this.setHtml(
            '<div class="wrapper"><div class="yes-label">{yes}</div><div class="no-label">{no}</div></div><div class="thumb"></div>'.
                replace(/\{yes\}/, this.getYes()).replace(/\{no\}/, this.getNo()));
        this.element.on('tap', this.onTap, this);
    },

    /** @private */
    onTap: function() {
        if (this.getDisabled() === true){
            return;
        }
        if (this.getValue() === true) {
            this.setValue(false);
        }
        else if (this.getValue() === false) {
            if (this.getDefined()) {
                this.setValue(true);
            }
            else {
                this.setValue(null);
            }
        }
        else {
            this.setValue(true);
        }

        this.fireEvent('change', this.getValue());
    },

    applyValue: function(value) {
        if (value === true) {
            this.removeCls('off');
            this.addCls('on');
        }
        else if (value === false) {
            this.addCls('off');
            this.removeCls('on');
        }
        else {
            this.removeCls('off');
            this.removeCls('on');
        }

        return value;
    }
});
