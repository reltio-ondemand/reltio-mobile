/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.TrueSearchField', {
    extend : 'Ext.field.Search',
    xtype  : 'truesearchfield',

    animatedPlaceHolder: null,
    animatedPlaceHolderText: '',

    initialize: function() {
        this.animatedPlaceHolder = document.createElement('div');
        this.animatedPlaceHolder.classList.add('animated-placeholder');
        this.animatedPlaceHolder.self = this;
        this.animatedPlaceHolder.addEventListener('click', this.placeHolderClick);
        this.callParent();
        if (Ext.os.is.Phone)
        {
            this.getComponent().element.dom.classList.add('phone');
        }
        this.getComponent().element.dom.appendChild(this.animatedPlaceHolder);
        this.setPlaceHolder(this.animatedPlaceHolderText);
    },

    getElementConfig : function() {
        var tpl = this.callParent();

        tpl.tag = 'form';
        tpl.onsubmit = 'return false;';
        tpl.action = '';

        return tpl;
    },

    applyPlaceHolder: function(newPlaceHolder) {
        this.animatedPlaceHolderText = newPlaceHolder;
        if (this.animatedPlaceHolder !== null) {
            this.animatedPlaceHolder.innerText = newPlaceHolder;
            var self = this;
            setTimeout(function() {
                var width = parseFloat(window.getComputedStyle(self.animatedPlaceHolder).width);
                self.animatedPlaceHolder.style.marginLeft = (9 - width * 0.5) + 'px';
            }, 100);
        }

        return '';
    },

    placeHolderClick: function(event) {
        event.currentTarget.self.focus();
    }
});
