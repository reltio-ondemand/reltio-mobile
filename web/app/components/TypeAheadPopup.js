/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.TypeAheadPopup', {
    extend: 'MobileUI.components.Popup',
    requires: ['MobileUI.components.EditableList', 'Ext.mixin.Observable'],
    mixins: ['Ext.mixin.Observable', 'MobileUI.components.list.MListHeight', 'MobileUI.components.list.MListModel'],
    /**
     * @event 'selectItem'
     * This class produces the 'selectItem' event
     */

    /**@type{Ext.LoadMask} @private*/
    mask: null,

    initialize: function ()
    {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'typeAheadResultsStore',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.setHideOnMaskTap(false);
        this.setModal(false);
        this.add({
            xtype: 'panel',
            scrollable: true,
            flex: 1,
            style: 'margin-bottom: 5px',
            name: 'listscroll'
        });
        this.down('panel[name=listscroll]').add({
            scrollable: false,
            height: 0,
            xtype: 'elist',
            store: 'typeAheadResultsStore'
        });
        this.down('elist').on('selectItem', function (record)
        {
            this.fireEvent('selectItem', record);
            this.hide();
        }, this);
    },
    /**
     * Method sets list data
     * @param data {MobileUI.components.list.model.EListModel} data
     */
    setListData: function (data)
    {
        var list = this.down('elist'),
             totalHeight;
        if (list)
        {
            list.getStore().clearData();
            list.getStore().add(data);
            totalHeight = this.updateListHeight(list);
            this.down('panel[name=listscroll]').setHeight(Math.min(totalHeight, this.getMaxHeight()));
        }
    },

    clearData: function ()
    {
        var list = this.down('elist');
        if (list)
        {
            list.getStore().clearData();
            list.refresh();
        }
    },

    /**
     * Method returns inner component
     * @params  query {String} query string
     * @returns {Ext.Component}
     */
    getInnerComponent: function (query)
    {
        return this.down(query);
    },

    /**
     * @override
     * @param component {Ext.Component} The target component to show this component by.
     */
    showBy: function ()
    {
        this.callParent(arguments);

        //We should mask all except toolbar. So, we try to find toolbar and then
        //find the child panel
        var panel, toolbar = Ext.Viewport.down('toolbar');
        if (toolbar)
        {
            panel = toolbar.getParent();
            panel = panel.down('panel');
            if (panel)
            {
                panel.setMasked(true);
                if (!this.mask)
                {
                    this.mask = panel.getMasked();
                    this.mask['on'].call(this.mask, 'tap', 'hide', this);
                }
            }
        }
    },
    /**
     * @override
     * See the parent method docs
     * @returns {*}
     */
    hide: function ()
    {
        var result = this.callParent(arguments);
        if (this.mask)
        {
            this.mask.hide();
        }
        return result;
    }
});
