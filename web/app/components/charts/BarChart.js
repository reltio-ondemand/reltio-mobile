/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.charts.BarChart', {
    extend: 'Ext.Container',
    xtype: 'reltiobarchart',
    chart: null,
    requires: ['MobileUI.components.charts.MChart'],
    mixins: {
        'baseChart': 'MobileUI.components.charts.MChart'
    },
    config: {
        cls: 'reltiobarchart'
    },
    innerComponent: null,
    startPosition: 0,
    transition: [0,0],
    observer: null,
    /** @private */
    initialize: function () {
        this.callParent(arguments);
        this.observer = Ext.create('MobileUI.core.Observer', {
            threadNames: ['painted', 'data', 'dataview', 'options'],
            listener: this.refresh,
            self: this
        });
        this.on('painted', function () {
            this.observer.checkThread('painted');
        }, this, {single: true});
        MobileUI.app.getController('ViewActivator').on('rootClicked', function () {
            if (this.chart){
                this.chart.tip.hide();
            }
        }, this);
        this.setLayout({
            type: 'hbox',
            align: 'center',
            pack: 'center'
        });
        Ext.Viewport.on('orientationchange', 'refresh', this);
    },
    getChartWidth: function () {
        var parent = this.getParent() ? this.getParent().element.getSize().width : 0;
        var element = this.element.getSize().width || 0;
        return Math.max(parent, element);
    },
    setData: function (data) {
        this.data = Ext.clone(data);
        this.observer.checkThread('data');
    },
    setDataView: function (dataview) {
        this.dataview = dataview;
        this.observer.checkThread('dataview');
    },
    setOptions: function (options) {
        this.options = options;
        this.observer.checkThread('options');
    },
    refresh: function () {
        this.removeAll();
        if (this.chart){
            this.chart.tip.hide();
        }
        this.innerComponent = Ext.create('Ext.Component');
        this.innerComponent.setCls('inner');
        this.element.on('touchmove', function (x) {
            if (!this.isDragging) {
                return;
            }
            var dif = (this.transition[0] || 0) + x.changedTouches[0].pageX - this.startPosition;
            if (this.chart.getWidth() + dif  > this.dataview.element.getSize().width && dif < 50) {
                this.chart.setTransition([dif,this.transition[1]]);
            }
        }, this);
        this.element.on('touchstart', function (x) {
            this.startPosition = x.changedTouches[0].pageX;
            this.transition = this.chart.getTransition().translate;
        }, this);
        this.element.on('dragstart', function (e) {
            this.lockScroll(e);
        }, this);

        this.add(this.innerComponent);
        this.innerComponent.on('painted', this.loadChart, this, {single: true});
    },
    lockScroll: function (e) {
        var absDeltaX = e.absDeltaX,
            absDeltaY = e.absDeltaY;
        this.isDragging = true;
        if (absDeltaX > absDeltaY) {
            e.stopPropagation();
        } else {
            this.isDragging = false;
            return;
        }
        var scrollable = this.dataview.getScrollable();
        if (scrollable) {
            scrollable = (scrollable.getScroller().getDisabled() ? this.dataview.up('panel') : this.dataview);
            scrollable.getScrollable().getScroller().setDirectionLock(true);
        }
    },
    loadChart: function () {
        var onPainted = this.onPaint.bind(this);
        if (d3 && BarChart) {
            this.chart = new BarChart(this.innerComponent.element.id, {
                height: this.options.height || 200,
                minWidth: 230,
                width: this.getChartWidth(),
                hideXAxis: this.options.hideXAxis || false,
                data: this.data
            }, onPainted);


            this.getChart().tip.setClickHandler(this.tooltipClickHandler);
        }
    },
    getChart: function() {
        return this.chart;
    },
    /**
     * @inheritdoc
     */
    destroy: function() {
        if (this.chart) {
            this.chart.tip.hide();
        }
        this.callParent(arguments);
    }
});
