/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.charts.Chart', {
    extend: 'Ext.Container',
    requires: ['MobileUI.components.charts.Legend', 'MobileUI.components.charts.PieChart', 'MobileUI.components.charts.BarChart'],
    config: {
        layout: 'vbox'
    },
    statics: {
        TYPE: {
            ONE_DIMENSION: '1',
            TWO_DIMENSIONS: '2'
        },
        MAX_WIDTH: 280
    },
    dimension: '1',
    phoneLayout: false,
    labels: null,
    initialize: function () {
        this.callParent(arguments);
        this.labels = [];
        if (Ext.os.is.Phone) {
            this.phoneLayout = true;
            this.setLayout({
                type: 'vbox',
                pack: 'shrink'
            });
        } else {
            this.setLayout({
                type: 'vbox'
            });
        }
        this.on('resize', function () {
            if (this.chart) {
                this.chart.setMaxWidth(this.element.getSize().width);
            }
        },this);
    },
    getChartTypes: function () {
        return [{
            icon: 'resources/images/reltio/components/charts/chart-pie',
            dimension: [MobileUI.components.charts.Chart.TYPE.ONE_DIMENSION],
            className: 'MobileUI.components.charts.PieChart'
        }, {
            icon: 'resources/images/reltio/components/charts/chart-column',
            dimension: [MobileUI.components.charts.Chart.TYPE.ONE_DIMENSION, MobileUI.components.charts.Chart.TYPE.TWO_DIMENSIONS],
            className: 'MobileUI.components.charts.BarChart',
            fullScreen: true
        }].filter(function (item) {
                return item.dimension.indexOf(this.dimension) !== -1 && (this.configuration.hiddenChartTypes || []).indexOf(item.className) === -1;
            }, this);
    },

    generateColor: function (label) {
        var randomColors = ['#fccde5', '#ff9acc', '#81b2d3', '#f8b6b8',
                '#bd81be', '#82c55c', '#c4e3f1', '#7f939e', '#ffee91',
                '#fec92f', '#ee4272', '#2d9ecf', '#c4c0e1', '#7accca',
                '#cdc393', '#f69785', '#f0ecc7', '#9fd6bf', '#6d9a35',
                '#afbc26', '#12959f', '#aacf99', '#056b97', '#eab710',
                '#b4d36a', '#70bede', '#f57f27', '#7a843b', '#cfd0d2',
                '#555962', '#b71137', '#ffdc0e', '#bea68c', '#5f3566'],
            n;
        if (this.dimension === MobileUI.components.charts.Chart.TYPE.TWO_DIMENSIONS) {
            n = this.labels.indexOf(label);
            if (n === -1) {
                n = this.labels.push(label) - 1;
            }
        } else {
            n = this.labels.push(label) - 1;
        }
        return randomColors[n % randomColors.length];
    },
    setConfiguration: function (configuration) {
        this.configuration = configuration || {};
        this.dimension = this.configuration.dimension || this.dimension;
        this.chartName = this.configuration.defaultChart;
        this.redraw();
    },
    setData: function (data) {
        this.processData(data);
        this.data = data;
        this.redraw();
    },
    processData: function (data) {
        if (Ext.isObject(data)) {
            Ext.Object.each(data, function (key, value) {
                this.processData(value);
            }, this);
        } else {
            data.forEach(function (item) {
                item.color = this.generateColor(item.label);
            }, this);
        }
    },
    setDataView: function (dataview) {
        this.dataview = dataview;
        this.redraw();
    },
    redraw: function () {
        if (this.configuration && this.data && this.dataview) {
            this.removeAll();
            this.showChart(this.chartName);
        }
    },
    createChart: function (chartClass) {
        this.chart = Ext.create(chartClass);
        this.chart.setDataView(this.dataview);
        this.chart.setOnPaint(this.onChartPainted);
        this.chart.setOptions(this.configuration);
        this.chart.setData(this.prepareDataForChart(this.data));
        this.chart.setMaxWidth(this.element.getSize().width);
        return this.chart;
    },
    createLegend: function (data, simple, mode) {
        this.legend = Ext.create('MobileUI.components.charts.Legend');
        this.legend.setOptions({showValues: simple});
        this.legend.setData(this.prepareDataForLegend(data), mode);
        return this.legend;
    },
    showChart: function (chartClass) {
        var chartExists = this.getChartTypes().some(function (item) {
            return item.className === chartClass;
        });
        if (chartExists) {
            this.activeChart = chartClass;
        } else {
            this.showChart(this.getChartTypes()[0].className);
            return;
        }
        this.removeAll();
        if (this.phoneLayout) {
            this.showChartForPhone(chartClass);
        } else {
            this.showChartForTablet(chartClass);
        }

    },
    showChartForTablet: function (chartClass) {
        var chartConf = this.getChartTypes().filter(function (item) {
            return item.className === chartClass;
        })[0];
        var chart = this.createChart(chartClass);
        var menu = this.initMenu('end', 'end');
        if (chartConf.fullScreen) {
            this.add(menu);
            this.add(chart);
            this.add(this.createLegend(this.data, true, MobileUI.components.charts.Legend.HORIZONTAL));
        } else {
            var baseContainer = Ext.create('Ext.Container', {
                layout: {
                    type: 'hbox'
                }
            });
            var chartContainer = Ext.create('Ext.Container', {
                layout: {
                    type: 'vbox',
                    pack: 'justify',
                    align: 'center'
                }
            });
            var legendContainer = Ext.create('Ext.Container', {
                layout: {
                    type: 'vbox',
                    pack: '',
                    align: 'stretch'
                }
            });
            chartContainer.add(chart);
            chartContainer.setFlex(1);
            var legend = this.createLegend(this.data, true, MobileUI.components.charts.Legend.VALUES_FIRST);
            legendContainer.add(this.initMenu('end', 'end'));
            legendContainer.add(legend);
            legendContainer.setFlex(1);
            baseContainer.add(chartContainer);
            baseContainer.add(legendContainer);
            this.add(baseContainer);

        }

        this.updateMenuState();
    },
    showChartForPhone: function (chartClass) {
        this.add(this.initMenu());
        var chartContainer = Ext.create('Ext.Container', {
            layout: {
                type: 'vbox',
                pack: 'shrink',
                align: 'center'
            }
        });
        chartContainer.add(this.createChart(chartClass));
        this.add(chartContainer);
        this.updateMenuState();
        this.add(this.createLegend(this.data, true));
    },
    prepareDataForLegend: function (data) {
        var result = [];
        if (this.dimension === MobileUI.components.charts.Chart.TYPE.ONE_DIMENSION && Ext.isObject(data)) {
            Object.keys(data).forEach(function (item) {
                result = result.concat(data[item]);
            });
            data = result;
        }
        return data;
    },

    prepareDataForChart: function (data) {
        var result = {};

        Object.keys(data).forEach(function (key) {
            result[key] = data[key].map(function (val) {
                return {
                    label: Ext.util.Format.htmlEncode(val.label),
                    color: val.color,
                    value: val.value
                };
            });
        });

        return result;
    },

    setOnChartPainted: function (onChartPainted) {
        this.onChartPainted = onChartPainted;
    },
    onChartPainted: function () {
        this.onChartPainted();
    },

    updateMenuState: function () {
        this.menu.removeAll();
        var items = this.getChartTypes().filter(function (item) {
            return !item.hidden;
        });
        if (items.length > 1) {
            items.forEach(function (item, index) {
                var component = Ext.create('Ext.Button', {
                    cls: 'icon-button menuItem',
                    icon: item.icon + '.svg'
                });
                if (this.activeChart && item.className === this.activeChart || !this.activeChart && index === 0) {
                    component.setIcon(item.icon + '-active.svg');
                }
                component.on('tap', function () {
                    this.self.remove(this.self.chart);
                    this.self.remove(this.self.legend);
                    this.self.showChart(this.item.className);
                }, {self: this, item: item});
                this.menu.add(component);
            }, this);
        }
    },
    initMenu: function (align, pack) {
        this.menu = Ext.create('Ext.Container',
            {
                layout: {
                    type: 'hbox',
                    align: align || 'center',
                    pack: pack || 'center'
                },
                cls: 'reltio-menu',
                height: 30
            });
        return this.menu;
    }
});