/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.charts.Legend', {
    extend: 'Ext.Container',
    config: {
        layout: 'vbox',
        cls: 'reltio-legend'
    },
    statics: {
        VALUES_FIRST: 'values_first',
        HORIZONTAL: 'horizontal'
    },
    mode: null,
    isShowValues: true,
    setOptions: function (options) {

        this.isShowValues = !!options.showValues;
    },
    setData: function (data, mode) {
        this.on('painted', function () {
            this.fireEvent('dataPainted');
        }, this, {single: true});
        this.mode = mode;
        data = this.prepareData(data);
        var multiple = data.some(function (item) {
            return Ext.isArray(item.value);
        });
        if (!this.isShowValues && multiple || this.mode === MobileUI.components.charts.Legend.HORIZONTAL) {
            this.setLayout({
                type: 'hbox',
                align: 'center',
                pack: 'center'
            });
        }
        data.forEach(function (item) {
            this.add(this.createItem(item.color, item.label, item.value, {hideValue: !this.isShowValues}));
        }, this);
    },

    prepareData: function (data) {
        var result = [];
        if (Ext.isObject(data)) {
            var tmpRes = {};
            var forEach = function (item) {
                if (!tmpRes[item.label]) {
                    tmpRes[item.label] = {
                        label: item.label,
                        color: item.color,
                        name: item.label,
                        value: []
                    };
                }
                tmpRes[item.label].value.push({
                    name: key,
                    value: item.value
                });
            };
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    data[key].forEach(forEach);
                }
            }
            Object.keys(tmpRes).forEach(function (key) {
                result.push(tmpRes[key]);
            });
        } else {
            result = data;
        }
        return result;
    },

    createItem: function (color, name, values, options) {
        var hideValue = options && options.hideValue;
        var component;
        var isHorizontal = this.mode === MobileUI.components.charts.Legend.HORIZONTAL;
        if (isHorizontal)
        {
            this.addCls(['top-line','centered']);
        }
        var legendCls = isHorizontal ? ' legend-horizontal': '';
        if (Ext.isArray(values)) {
            component = Ext.create('Ext.Container', {
                layout: 'vbox',
                cls: 'legend-complex-item' + legendCls
            });
            if (isHorizontal) {
                this.getLayout().setAlign('center');
                this.getLayout().setPack('center');
            }
            var header = Ext.create('Ext.Component');
            header.setHtml('<div class = "header"><div class="circle" style="background-color: ' + color + '"> </div>' + '<div class="name">' + Ext.util.Format.htmlEncode(name) + '</div></div>');
            component.add(header);
            if (!hideValue && !isHorizontal) {
                values.forEach(function (record, index) {
                    var item = Ext.create('Ext.Component');
                    var lastClass = index === values.length - 1 ? 'last' : '';
                    item.setHtml('<div class="record ' + lastClass + '"><div class="subname">' + Ext.util.Format.htmlEncode(record.name) + ' </div>' + '<div class="subvalue">' + i18n(+record.value) + '</div></div>');
                    component.add(item);
                });
            }
        } else {
            component = Ext.create('Ext.Component');
            component.setCls(legendCls);
            var value = hideValue ? '' : '<div class = "value">' + i18n(+values) + '</div>';
            var html;
            if (this.mode === MobileUI.components.charts.Legend.VALUES_FIRST || this.mode === MobileUI.components.charts.Legend.HORIZONTAL) {
                html = '<div class ="legend-item-first">' +
                    value +
                    '<div class="circle" style="background-color: ' + color + '"> </div>' +
                    '<div class="name">' + Ext.util.Format.htmlEncode(name) + '</div>' +
                    '</div>';
            } else {
                html = '<div class ="legend-item">' +
                    '<div class="circle" style="background-color: ' + color + '"> </div>' +
                    '<div class="name">' + Ext.util.Format.htmlEncode(name) + '</div>' +
                    value +
                    '</div>';
            }

            component.setHtml(html);
        }
        return component;
    }
});