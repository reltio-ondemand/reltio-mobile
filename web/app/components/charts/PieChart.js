/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.charts.PieChart', {
    extend: 'Ext.Component',
    xtype: 'reltiopiechart',
    donut: null,
    requires: ['MobileUI.components.charts.MChart'],
    mixins: {
        'baseChart': 'MobileUI.components.charts.MChart'
    },
    /** @private */
    initialize: function () {
        this.callParent(arguments);
        this.on('painted', function () {
            this.observer.checkThread('painted');
        }, this, {single: true});
        this.observer = Ext.create('MobileUI.core.Observer', {
            threadNames: ['painted', 'data', 'dataview', 'options'],
            listener: this.loadChart,
            self: this
        });
        MobileUI.app.getController('ViewActivator').on('rootClicked', function () {
            if (this.donut) {
                this.donut.tip.hide();
            }
        }, this);
    },
    setDataView: function (dataview) {
        this.dataview = dataview;
        this.observer.checkThread('dataview');
    },
    setOptions: function (options) {
        this.observer.checkThread('options');
        this.options = options;
    },
    prepareData: function (data) {
        var result = [];

        if (Ext.isObject(data)) {
            Object.keys(data).forEach(function (item) {
                result = result.concat(data[item]);
            });
        }
        return result;
    },
    setData: function (data) {
        this.data = this.prepareData(data);
        this.observer.checkThread('data');
    },
    loadChart: function () {
        var onPainted = this.onPaint.bind(this);
        if (d3 && d3donut) {
            this.donut = d3donut(this.element.id, {
                width: 240,
                height: 300,
                donutWidth: 49,
                subtitle: i18n(this.options.subtitle) || '',
                'data': this.data
            }, onPainted);

            this.getChart().tip.setClickHandler(this.tooltipClickHandler);
        }
    },
    getChart: function() {
        return this.donut;
    },
    /**
     * @inheritdoc
     */
    destroy: function() {
        if (this.donut) {
            this.donut.tip.hide();
        }
        this.callParent(arguments);
    }
});
