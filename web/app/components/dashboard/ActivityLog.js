/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*Ext.define('MobileUI.components.dashboard.RecentEntities', {
    extend: 'Ext.Panel',
    xtype: 'recententities',
    requires: [
        'MobileUI.components.EditableList'
    ],

    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        savedSearches: 'MobileUI.controller.MSavedSearches',
        spinner: 'MobileUI.components.MSpinner'
    },

    config: {
        cls: 'dashboard-recententities dashboard-item',
        layout: 'vbox',
        params: {}
    },

    initialize: function() {
        this.add({
                xtype: 'component',
                cls: 'rl-header',
                html: this.getParams().title || 'Recent Entities',
                name: 'recententities-title'
            },
            {
                xtype: 'component',
                cls: 'rl-spinner-item',
                html: this.getSpinnerHtml(),
                style: 'text-align: center',
                name: 'recententities-spinner'
            },
            {
                xtype: 'elist',
                scrollable: false,
                name: 'recententities-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                defaultType: 'eDataItem'
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'component',
                        cls: 'rl-text',
                        html: '',
                        name: 'recententities-status'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                    xtype: 'button',
                    cls: 'show-more-button',
                    html: 'Show all',
                    style: 'text-align: right',
                    handler: function() {
                        MobileUI.core.Navigation.redirectForward('search/saved', false);
                    }
                }]
            });
        MobileUI.core.session.Session.totalActivities(null,
            function(activities) {
                this.down('component[name=recententities-spinner]').setHidden(true);

                if (activities.length > 0) {
                    var list = this.down('elist[name=recententities-list]');
                    list.getStore().add(activities);
                    this.updateListHeight(list);
                }
                else {
                    this.insert(1, {
                        xtype: 'component',
                        cls: 'rt-text',
                        html: 'There are no logged activity',
                        style: 'margin: 15px'
                    });
                }
            },
            function(resp) {
                MobileUI.core.Logger.error(resp);
            },
            this);
    }
});*/
