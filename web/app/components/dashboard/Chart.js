/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.dashboard.Chart', {
    extend: 'Ext.Panel',
    xtype: 'dashboardchart',
    requires: [
        'MobileUI.components.charts.BarChart',
        'MobileUI.components.charts.PieChart'
    ],

    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },

    config: {
        cls: 'dashboard-chart dashboard-item',
        layout: 'vbox',
        params: {
            caption: '',
            entity: '',
            attribute: '',
            filter: '',
            count: 100,
            type: 'bar',
            options: {
                hideXAxis: false
            }
        }
    },

    chartXTypeMap: {
        bar: 'reltiobarchart',
        pie: 'reltiopiechart'
    },

    initialize: function() {
        var config = this.getParams();
        this.add([{
                xtype: 'component',
                cls: 'rl-header',
                html: i18n(config.caption || '')
            },
            {
                xtype: 'component',
                cls: 'rl-spinner-item',
                html: this.getSpinnerHtml(),
                style: 'text-align: center',
                name: 'dashboard-chart-spinner'
            },
            {
                xtype: 'container',
                scrollable: config.kind === 'bar' ? 'horizontal' : null,
                name: 'dashboard-chart-dataview',
                items: [{
                    xtype: this.chartXTypeMap[config.kind],
                    name: 'dashboard-chart-chart',
                    cls: config.kind === 'bar' ? 'dashboard-chart-chart': ''
                }]
            }]);

        var body = this.getRequestExtraParams() || {};
        body.method = this.getMethod();

        MobileUI.core.session.Session.sendRequest(
            this.getAPIRequestString(), null, body, this.onData,
            function(resp) {
                MobileUI.core.Logger.error(resp);
            }, this);
    },

    onData: function(data) {
        var spinner = this.down('component[name=dashboard-chart-spinner]');
        var chart = this.down('component[name=dashboard-chart-chart]');
        var dataView = this.down('component[name=dashboard-chart-dataview]');
        var config = this.getParams();
        var self = this;

        if (data.errorMessage) {
            MobileUI.core.Logger.error(data.errorMessage);
            return;
        }

        chart.setTooltipClickHandler(function(data) {
            self.openSearchScreen(data.label);
        });

        data = data[Object.keys(data)[0]];

        data = Object.keys(data).map(function(key) {
            return { label: key, value: data[key] };
        });

        chart.setData({first: data});

        var options = config.options || {};
        options.dimension = MobileUI.components.charts.Chart.TYPE.ONE_DIMENSION;
        chart.setOptions(options);

        chart.setDataView(dataView);

        if (dataView.getScrollable()) {
            setTimeout(function() {
                spinner.setHidden(true);
                dataView.setHeight(chart.element.dom.offsetHeight);
            }, 100);
        }
        else {
            spinner.setHidden(true);
        }
    },

    getRequestExtraParams: function() {
        var config = this.getParams();
        return [{
            fieldName: config.attribute,
            pageSize: config.count ? config.count : 100,
            orderType: 'reversedCount'
        }];
    },

    getMethod: function() {
        return 'POST';
    },

    getAPIRequestString: function() {
        var config = this.getParams();
        var filterPart = this.getFilterPart(config.filter);
        return '/entities/_facets?filter=equals(type,\'' + config.entity + '\')' +
            (filterPart ? ' and ' + filterPart : '');
    },

    getFilterPart: function(filter) {
        var strQuery = null;
        if (filter) {
            if (!Ext.isArray(filter)) {
                filter = [filter];
            }
            strQuery = filter.map(function(f) {
                var str = (f.type || 'equals') + '(' + (f.attribute || '');
                if (f.value) {
                    var value = f.value;
                    if (!Ext.isArray(value)) {
                        value = [value];
                    }

                    str += ',' + value.map(function(v) {
                            return Ext.isString(v) ? ('\'' + v + '\'') : v;
                        }).join(',');
                }
                str += ')';
                return str;
            }).join(' and ');
        }

        var globalFilter = MobileUI.core.session.Session.getSearchEngine().getGlobalFilterPart();
        if (globalFilter) {
            if (strQuery) {
                strQuery += ' and (' + globalFilter + ')';
            }
            else {
                strQuery = globalFilter;
            }
        }

        return strQuery;
    },

    openSearchScreen: function(value) {
        var md = MobileUI.core.Metadata;
        var config = this.getParams();
        var label = value;
        var attrPath = config.attribute.split('.');
        var et = md.getEntityType(config.entity);
        var facets = [{
            fieldName: MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME,
            title: i18n('Entity types'),
            filter: 'equals',
            labels: [et.label],
            values: [config.entity]
        }];

        facets.push({
            fieldName: config.attribute,
            title: attrPath[1] || attrPath[0],
            filter: 'equals',
            labels: [label],
            values: [value]
        });

        if (config.filter) {
            var filter = Ext.isArray(config.filter) ? config.filter : [config.filter];
            for (var i = 0; i < filter.length; i++) {
                var f = filter[i];
                if (f.type === 'equals') {
                    facets.push({
                        fieldName: f.attribute,
                        title: f.attribute,
                        filter: 'equals',
                        values: [f.value],
                        labels: [f.value]
                    });
                }
            }
        }
        var spm = MobileUI.core.search.SearchParametersManager;
        spm.resetSearch();
        spm.setFacets(facets);
        MobileUI.core.Navigation.redirectForward('search/init');
    }
});
