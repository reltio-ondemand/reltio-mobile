/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.dashboard.CustomViewBoard', {
    extend: 'Ext.Panel',
    xtype: 'customviewboard',
    requires: [
        'MobileUI.components.CustomViewComponent'
    ],
    config: {
        cls: 'dashboard-item',
        layout: 'vbox',
        params: {
            caption: ''
        }
    },

    initialize: function () {
        this.add([{
                xtype: 'component',
                cls: 'rl-header',
                html: i18n(this.getParams().caption || '')
            },
                {
                    xtype: 'container',
                    name: 'dashboard-customview-dataview',
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'customviewcomponent',
                            name: 'dasboard-custom'
                        }
                    ]
                }
            ]
        );
        var cmp = this.down('customviewcomponent[name=dasboard-custom]');
        cmp.on('painted', function () {
            if (this.getParams()) {
                cmp.setParameters(this.getParams());
                var dataView = this.down('component[name=dashboard-customview-dataview]');
                cmp.on('sizeUpdated',function () {
                    if (!cmp.element.dom.offsetHeight){
                        cmp.setHeight(300);
                        dataView.setHeight(300);
                    } else {
                        cmp.setHeight(cmp.element.dom.offsetHeight);
                        dataView.setHeight(cmp.element.dom.offsetHeight);
                    }
                }, this);
            }
        }, this, {single: true});

    }
});
