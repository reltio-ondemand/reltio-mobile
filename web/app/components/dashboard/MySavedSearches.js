/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.dashboard.MySavedSearches', {
    extend: 'Ext.Panel',
    xtype: 'mysavedsearches',
    requires: [
        'MobileUI.components.EditableList',
        'MobileUI.controller.dialogs.SavedSearches'
    ],

    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        savedSearches: 'MobileUI.controller.MSavedSearches',
        spinner: 'MobileUI.components.MSpinner'
    },

    config: {
        cls: 'dashboard-mysavedsearches dashboard-item savedsearch',
        layout: 'vbox',
        params: {
            caption: 'My Saved Searches',
            count: 5
        }
    },

    initialize: function() {
        var count = this.getParams().count || 5;
        this.add([{
                xtype: 'component',
                cls: 'rl-header',
                html: i18n(this.getParams().caption || 'My Saved Searches')
            },
            {
                xtype: 'component',
                cls: 'rl-spinner-item',
                html: this.getSpinnerHtml(),
                style: 'text-align: center',
                name: 'mysavedsearches-spinner'
            },
            {
                xtype: 'elist',
                scrollable: false,
                flex: 0,
                height: 0,
                name: 'mysavedsearches-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                defaultType: 'eDataItem'
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                    xtype: 'button',
                    cls: 'show-more-button',
                    html: i18n('Show all'),
                    style: 'text-align: right',
                    handler: function() {
                        MobileUI.controller.dialogs.SavedSearches.show();
                    }
                }]
            }]);
        var defaultParams = {
            countResults: true,
            findShared: true,
            sortOrder: 'ASC',
            sortBy: 'NAME',
            favoriteOnly: true
        };
        Object.keys(this.getParams() || {}).forEach(function(item){
            defaultParams[item] = null;
        });
        var options = Ext.merge(defaultParams, (this.getParams() || {}));
        MobileUI.core.session.Session.findSavedSearches(count, 0, options,
            function(savedSearches) {
                savedSearches = savedSearches.slice(0, count);
                savedSearches.forEach(function(ss) {
                    ss.disableSwipe = true;
                });

                this.down('component[name=mysavedsearches-spinner]').setHidden(true);

                if (savedSearches.length > 0) {
                    var list = this.down('elist[name=mysavedsearches-list]');
                    list.getStore().add(savedSearches);
                    this.updateListHeight(list);

                    list.on('selectItem', function(record) {
                        var userData = record.get('userData');
                        if (userData && userData.uri) {
                            this.showSavedSearch(userData.uri);
                        }
                        else {
                            this.onAdd();
                        }
                    }, this);
                }
                else {
                    this.showError();
                }
            },
            function(resp) {
                MobileUI.core.Logger.error(resp);
                this.down('component[name=mysavedsearches-spinner]').setHidden(true);
                this.showError();
            },
            this);
    },

    showError: function(){
        this.insert(1, {
            xtype: 'component',
            cls: 'rt-text',
            html: i18n('There are no saved searches created by current user'),
            style: 'margin: 15px'
        });
    }
});
