/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.dashboard.RecentEntities', {
    extend: 'Ext.Panel',
    xtype: 'recententities',
    requires: [
        'MobileUI.components.EditableList'
    ],

    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        savedSearches: 'MobileUI.controller.MSavedSearches',
        spinner: 'MobileUI.components.MSpinner'
    },

    config: {
        cls: 'dashboard-recententities dashboard-item',
        layout: 'vbox',
        params: {
            caption: 'Recent Entities',
            count: '5'
        },
        single: false,
        itemId: null
    },

    pageSize: 20,
    offset: 0,

    initialize: function() {
        var id = this.getItemId() || 'recententities';
        var isSinge = this.getSingle();

        this.add([{
                xtype: 'component',
                cls: 'rl-header',
                html: i18n(this.getParams().caption || 'Recent Entities'),
                name: 'recententities-title'
            },
            {
                xtype: 'component',
                cls: 'rl-spinner-item',
                html: this.getSpinnerHtml(),
                style: 'text-align: center',
                name: 'recententities-spinner'
            },
            {
                xtype: 'elist',
                flex: isSinge ? 1 : 0,
                scrollable: isSinge,
                height: isSinge ? '' : 0,
                name: 'recententities-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                defaultType: 'eDataItem',
                allowFetch: isSinge,
                scrollToTopOnRefresh: false
            },
            {
                xtype: 'container',
                layout: 'hbox',
                hidden: isSinge,
                items: [
                    {
                        xtype: 'component',
                        cls: 'rl-text',
                        html: '',
                        name: 'recententities-status'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'show-more-button',
                        html: i18n('Show all'),
                        style: 'text-align: right',
                        handler: function() {
                            MobileUI.core.Navigation.redirectForward('dashboard/' + id);
                        }
                    }]
            }]);

        var count = this.getParams().count || 5;


        var list = this.down('elist[name=recententities-list]');

        if (isSinge) {
            count = this.pageSize;
            list.on('fetch', this.onListFetch, this);
            this.setFlex(1);
        }

        list.on('selectItem', this.onItemSelect, this);

        MobileUI.core.session.Session.searchActivities(this.getFilter(), 0, count, null, null,
            this.onData, function(resp) {
                MobileUI.core.Logger.error(resp);
            },
            this);
    },

    getFilter: function() {
        var username = MobileUI.core.util.Storage.getItem('username');

        return [
            {fieldName: 'user', filter: 'equals', values: [username]},
            {fieldName: 'label', filter: 'startsWith', values: ['USER_PROFILE_VIEW']}
        ];
    },

    onListFetch: function() {
        var list = this.down('elist[name=recententities-list]');

        if (list) {
            this.offset += this.pageSize;
            MobileUI.core.session.Session.searchActivities(this.getFilter(), this.offset, this.pageSize, null, null,
                this.onData, function(resp) {
                    MobileUI.core.Logger.error(resp);
                },
                this);
        }
    },

    onData: function(activities) {
        if (activities.length > 0) {
            activities = activities.sort(function(a, b) {
                return Math.max(-1, Math.min(1, a.timestamp - b.timestamp));
            });

            var uris = activities.map(function(item) {
                    return [JSON.parse(item.description).uri];
                });

            MobileUI.core.session.Session.findEntitiesByUris(uris,
                function(entities) {
                    this.down('component[name=recententities-spinner]').setHidden(true);
                    var model = MobileUI.core.Services.getSearchResultBuilder().buildContent(entities);

                    var list = this.down('elist[name=recententities-list]');
                    list.getStore().add(model);
                    list.fetchDone(true);
                    if (!this.getSingle()) {
                        this.updateListHeight(list);
                    }
                },
                function(resp) {
                    MobileUI.core.Logger.error(resp);
                }, this);
        }
        else {
            var list = this.down('elist[name=recententities-list]');
            list.fetchDone(false);
            if (list.getStore().getCount() === 0) {
                this.insert(1, {
                    xtype: 'component',
                    cls: 'rt-text',
                    html: i18n('There is no logged activity'),
                    style: 'margin: 15px'
                });
                this.down('component[name=recententities-spinner]').setHidden(true);
            }
        }
    },

    onItemSelect: function(dataEvent) {
        MobileUI.core.Navigation.redirectForward('profile/' +
            MobileUI.core.util.HashUtil.encodeUri(dataEvent.getData().userData.uri));
    }
});
