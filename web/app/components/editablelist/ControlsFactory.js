/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.ControlsFactory', {
    statics: {
        attributeTypes: {
            STRING: 'String',
            TEXT: 'Text',
            PASSWORD: 'Password',
            LONG: 'Long',
            INT: 'Int',
            FLOAT: 'Float',
            DOUBLE: 'Double',
            NUMBER: 'Number',
            BOOLEAN: 'Boolean',
            DATE: 'Date',
            TIMESTAMP: 'Timestamp',
            BLOB: 'Blob',
            IMAGE: 'Image',
            URL: 'URL',
            BLOG_URL: 'Blog URL',
            IMAGE_URL: 'Image URL',
            SSN: 'SSN',
            ID_CARD: 'IdCard',
            EMAIL: 'Email',
            ENUM: 'Enum',
            SIC_CODE: 'SIC code',
            NAICS_CODE: 'NAICS code',
            NESTED: 'Nested',
            EMAIL_DOMAINS: 'email-domain',
            TICKER_SYMBOL: 'ticker-symbol',
            CIK_ID: 'cik id',
            STOCK_EXCHANGE: 'stock-exchange',
            PHONE_NUMBER: 'phone number',
            GEO_LOCATION: 'GeoLocation',
            TWITTER_ID: 'TwitterID',
            FACEBOOK_ID: 'FacebookID',
            LINKEDIN_ID: 'LinkedInID',
            DOLLAR: 'Dollar',
            LOOKUP: 'Lookup',
            DEPENDENT_LOOKUP: 'DependentLookup'
        },

        updateTextareaHeight: function (editor) {
            var textarea = editor.element.down('textarea').dom;
            textarea.style.height = '21px';

            var height = textarea.scrollHeight;
            var delta = height - textarea.offsetHeight;

            textarea.style.height = height + 'px';
            editor.setHeight(height);
            editor.fireEvent('changeHeight', delta);
        },

        /**
         * Create an UI control by it's type
         * @param attrType {String} the type of the control
         */
        createControl: function (attrType) {
            var control;
            var type = this.attributeTypes;

            switch (attrType) {
                default:
                case type.STRING:
                    control = Ext.create('Ext.field.Text', {cls: 'rl-inline-editor'});
                    break;

                case type.LONG:
                case type.INT:
                    control = Ext.create('MobileUI.components.list.editors.NumberEditor', {cls: 'rl-inline-editor'});
                    control.setStepValue(1);
                    break;

                case type.NUMBER:
                case type.FLOAT:
                case type.DOUBLE:
                    control = Ext.create('MobileUI.components.list.editors.NumberEditor', {cls: 'rl-inline-editor'});
                    break;

                case type.BLOB:
                case type.TEXT:
                    control = Ext.create('Ext.field.TextArea', {
                        cls: 'rl-inline-editor',
                        scrollable: {direction: 'vertical', directionLock: true},
                        height: '21px',
                        listeners: {
                            keyup: this.updateTextareaHeight
                        }
                    });
                    window.setTimeout(function () {
                        this.updateTextareaHeight(control);
                    }.bind(this), 0);
                    break;

                case type.BOOLEAN:
                    var useTranslated = i18n('Yes').length <= 3 && i18n('No').length <= 3;
                    control = Ext.create('MobileUI.components.TripleToggle', {
                        yes: useTranslated ? i18n('Yes') : 'Yes',
                        no: useTranslated ? i18n('No') : 'No',
                        style: {
                            'float': 'right',
                            marginRight: '1em'
                        }
                    });
                    break;

                case type.TIMESTAMP:
                    control = Ext.create('MobileUI.components.list.editors.TimestampEditor', {cls: 'rl-inline-editor'});
                    break;

                case type.DATE:
                    control = Ext.create('Ext.field.Text', {
                        cls: 'rl-inline-editor',
                        component: {type: 'date'},
                        clearIcon: false
                    });
                    break;

                case type.ENUM:
                    control = Ext.create('Ext.field.Select', {
                        cls: 'rl-inline-editor',
                        forceSelection: false,
                        autoSelect: false,
                        defaultPhonePickerConfig: {
                            cls: 'rl-inline-editor-list'
                        },
                        defaultTabletPickerConfig: {
                            cls: 'rl-inline-editor-list'
                        }
                    });
                    break;
                case type.LOOKUP:
                    control = Ext.create('MobileUI.components.StyledSelect', {
                        cls: 'rl-inline-editor',
                        forceSelection: false,
                        autoSelect: false,
                        useHtml: true,
                        defaultPhonePickerConfig: {
                            cls: 'rl-inline-editor-list'
                        },
                        defaultTabletPickerConfig: {
                            cls: 'rl-inline-editor-list'
                        }
                    });
                    break;
            }

            return control;
        }
    }
});