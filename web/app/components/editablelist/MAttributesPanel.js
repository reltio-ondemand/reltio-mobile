/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.MAttributesPanel', {

    errors: [],
    attrValues: null,

    getModelsFromSource: function (type, attrTypes, configuration) {
        var source = this.getSource(),
            uri = source.uri;

        this.attrValues = source.value;
        if (!configuration) {
            configuration = {};
        }
        if (!configuration.attributes) {
            configuration.attributes = [{
                label: type.type === 'Reference' ? null : type.label,
                content: null
            }];
        } else {
            configuration.attributes.push({
                label: 'Other',
                content: null
            });
        }

        var result = [],
            groupedAttrTypes = [];
        var isVisible = function (attrType) {
            return !attrType.hidden && (configuration.hiddenAttributes || []).indexOf(attrType.uri) === -1;
        };
        configuration.attributes.forEach(function (attr) {
            var attrTypesForMe;

            if (attr.content === null) {
                attrTypesForMe = attrTypes
                    .filter(function (attrType) {
                        return attrType.type !== 'Nested' && attrType.type !== 'Reference';
                    })
                    .filter(function (attrType) {
                        return groupedAttrTypes.indexOf(attrType) === -1;
                    });
            } else {
                attrTypesForMe = attrTypes.filter(function (attrType) {
                    return attr.content.some(function (contentItem) {
                        return attrType.uri === contentItem || attrType.name === contentItem;
                    });
                });
            }

            attrTypesForMe = attrTypesForMe.filter(isVisible);

            if (attrTypesForMe.length) {
                var me = this;
                this.createGroup(result, attr.label, attrTypesForMe, function () {
                    return attrTypesForMe.reduce(function (acc, attrType) {
                        groupedAttrTypes.push(attrType);
                        return acc.concat(me.createContent(attrType, type, uri));
                    }, []);
                }, type);
            }
        }, this);

        attrTypes
            .filter(function (attrType) {
                return attrType.type === 'Nested' || attrType.type === 'Reference';
            })
            .filter(isVisible)
            .forEach(function (attrType) {
                this.createGroup(result, attrType.label, [attrType], function () {
                    return this.createContent(attrType, type, uri);
                }, type);
            }, this);
        return this.processLookups(result);
    },

    createGroup: function (result, label, attrTypes, createContentFunc, parentType) {
        var header = this.createHeader(label, attrTypes, parentType);
        var content = createContentFunc.call(this);
        var group = header.concat(content);
        var notHeaders = group.filter(function(item){
            return item && item.type !== 'header';
        });
        if (notHeaders.length > 0){
            Array.prototype.push.apply(result, group);
            result[result.length - 1].lastInGroup = true;
        }
    },

    createHeader: function (label, attrTypes, parentType) {
        var result = [];

        if (label) {
            var header = {
                type: MobileUI.components.editablelist.items.ListItem.types.header,
                label: i18n(label),
                disableSwipe: true
            };

            if (attrTypes.length === 1) {
                var attrType = attrTypes[0];
                header.required = MobileUI.core.CardinalityChecker.isRequired(attrType);
                header.message = MobileUI.core.CardinalityChecker.getMessage(attrType);
                header.error = (this.errors || []).filter(function (err) {
                    return !err.objectUri && err.attrType === attrType;
                }, this)[0];
            }

            result.push(header);
        }

        var security = MobileUI.core.PermissionManager.securityService().metadata;
        var canUpdate = !parentType || security.canCreateOrInitiate(parentType) || security.canUpdateOrInitiate(parentType);
        attrTypes = attrTypes.filter(function (attrType) {
            return canUpdate && MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
        });

        if (attrTypes.length > 0) {
            result.push({
                type: MobileUI.components.editablelist.items.ListItem.types.addButton,
                disableSwipe: true,
                attrTypes: attrTypes
            });
        }

        return result;
    },

    createContent: function (attrType, parentType, parentUri) {
        var emptyValue = [];
        var result = [];
        if (MobileUI.core.CardinalityChecker.isRequired(attrType) && attrType.type !== 'Nested' && attrType.type !== 'Reference') {
            emptyValue = [{
                type: attrType.uri,
                uri: MobileUI.core.entity.EntityUtils.generateUri(this.parentUri, attrType.name),
                value: ''
            }];
        }
        (this.attrValues[attrType.name] || emptyValue)
            .filter(function (val) {
                return val.ov !== false;
            })
            .filter(function (val) {
                return !MobileUI.core.entity.EntityUtils.isTemporaryUri(val.uri) || MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
            })
            .forEach(function (val) {
                var isTemporary = MobileUI.core.entity.EntityUtils.isTemporaryUri(val.uri);
                var errorForMe = (this.errors || []).filter(function (err) {
                    if (err.objectUri) {
                        return err.objectUri.indexOf(val.uri) === 0;
                    } else {
                        if (err.attrType && attrType && err.attrType.uri === attrType.uri && !MobileUI.core.entity.EntityUtils.isComplex(attrType)) {
                            return err.objectParentUri === parentUri;
                        }

                        if (err.objectParentUri) {
                            return err.objectParentUri.indexOf(val.uri) === 0;
                        }
                    }
                }, this)[0];
                var editableAttribute = isTemporary ||
                    MobileUI.core.PermissionManager.securityService().metadata.canUpdateOrInitiate(attrType);
                var deleteAllowed = isTemporary ||
                    MobileUI.core.PermissionManager.securityService().metadata.canDeleteOrInitiate(attrType);
                if (isTemporary || MobileUI.core.PermissionManager.securityService().metadata.canRead(attrType)) {
                    if (attrType.type === 'Reference') {
                        var entityType = MobileUI.core.Metadata.getEntityType(attrType.referencedEntityTypeURI);
                        result.push({
                            type: MobileUI.components.editablelist.items.ListItem.types.referenceAttribute,
                            attrType: attrType,
                            attrValue: val,
                            label: val.label || i18n('<No label>'),
                            secondaryLabel: val.relationshipLabel,
                            error: errorForMe,
                            editable: editableAttribute,
                            disableSwipe: !deleteAllowed,
                            typeBadge: {
                                value: Ext.util.Format.htmlEncode(entityType.label),
                                color: entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR
                            },
                            //entity.defaultProfilePicValue
                            image: MobileUI.core.Services.getBorderlessImageUrl(MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage))
                        });
                    } else {
                        result.push({
                            type: MobileUI.components.editablelist.items.ListItem.types.attribute,
                            attrType: attrType,
                            attrValue: val,
                            editable: editableAttribute,
                            label: val.label || i18n('<No label>'),
                            disableSwipe: !deleteAllowed,
                            error: errorForMe,
                            required: MobileUI.core.CardinalityChecker.isRequired(attrType),
                            message: MobileUI.core.CardinalityChecker.getMessage(attrType)
                        });
                    }
                }
            }, this);
        return result;
    },

    onListSelectItem: function (data) {
        var record = data.record,
            attrType = record.get('attrType'),
            attrValue = record.get('attrValue'),
            editor;

        if (!attrType || record.get('disabled')) {
            return;
        }

        if (attrType.type === 'Nested') {
            editor = this.showEditor('MobileUI.components.editablelist.editors.NestedAttributeEditor', function (editor) {
                editor.init(attrType, attrValue, this.branch, this.errors);
                editor.refreshList();
            });
        } else if (attrType.type === 'Reference') {
            editor = this.showEditor('MobileUI.components.editablelist.editors.ReferenceAttributeEditor', function (editor) {
                editor.init(attrType, attrValue, this.branch, this.errors);
                editor.initState();
            });
        }

        if (editor) {
            editor.on('done', function () {
                this.refreshList();
            }, this);
            editor.on('cancel', function (attrTypes) {
                this.updateLookupsHierarchy(attrTypes, attrValue);
            }, this);
        } else if (attrType && attrType.dependentLookupCode && record.get('editable') !== false) {
            editor = this.showEditor('MobileUI.components.editablelist.editors.DependentLookupValues', function (editor) {
                editor.init(attrType, attrValue);
            });

            editor.on('done', function (value) {
                this.updateDependentLookup(attrType, attrValue.uri, value);
                MobileUI.core.session.Session.getLookupsManager().touchEditor(attrValue.uri);
                this.refreshList();
            }, this);
        }
    },

    updateDependentLookupManager: function (attrType, uri, value) {
        if (value === null) {
            value = {
                value: '',
                lookupCode: ''
            };
            MobileUI.core.session.Session.getLookupsManager().removeDependentLookupValue(attrType.uri, uri);
        } else {
            MobileUI.core.session.Session.getLookupsManager().updateLookupsHierarchy(attrType, {
                lookupCode: value.lookupCode,
                uri: uri
            });
        }

        return value;
    },

    updateDependentLookup: function (attrType, uri, value) {
        value = this.updateDependentLookupManager(attrType, uri, value);

        this.onListChangeValue({
            uri: uri,
            attrType: attrType,
            value: value
        });
    },

    onListChangeValue: function (data) {
        if (data.attrType.lookupCode && data.value === null) {
            data.value = {
                value: '',
                lookupCode: ''
            };
        }
        if (this.getDoneButton()) {
            this.getDoneButton().setDisabled(false);
        }
        this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.edit, data);
    },

    onListDeleteAttribute: function (record) {
        var attrType = record.get('attrType');
        var attrValue = record.get('attrValue');
        if (MobileUI.core.entity.EntityUtils.isComplex(attrType)) {
            var attrTypes = MobileUI.core.Metadata.getSubAttributes(attrType);
            if (MobileUI.core.entity.EntityUtils.isReference(attrType) && MobileUI.core.entity.EntityUtils.isTemporaryUri(attrValue.refEntity.objectURI)) {
                MobileUI.core.session.Session.getLookupsManager().removeValuesForAttribute(attrValue.refEntity.objectURI);
            }
            this.actualizeLookupsOnRemove(attrTypes, attrValue);
        }
        this.onListDeleteAttributeInternal(attrType, attrValue.uri);
        this.refreshList();
    },

    onListDeleteAttributeInternal: function (attrType, attrValueUri) {
        var shouldRemove = true;

        if (MobileUI.core.CardinalityChecker.isRequired(attrType)) {
            var attrValues = this.getSource().value[attrType.name];
            if (attrValues && attrValues.length === 1 && !MobileUI.core.entity.EntityUtils.isComplex(attrType)) {
                var data = {
                    attrType: attrType,
                    uri: attrValueUri,
                    value: ''
                };
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.edit, data);
                shouldRemove = false;
            }
        }

        if (shouldRemove) {
            this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.remove, {
                attrType: attrType,
                uri: attrValueUri
            });
        }

        if (attrType.dependentLookupCode) {
            MobileUI.core.session.Session.getLookupsManager().touchEditor(attrValueUri);
            this.updateDependentLookupManager(attrType, attrValueUri, null);
        }
    },

    onListAddAttribute: function (data) {
        var attrTypes = data.record.get('attrTypes');
        var editor;
        if (this.getDoneButton()) {
            this.getDoneButton().setDisabled(false);
        }
        if (attrTypes.length > 1) {
            editor = this.showEditor('MobileUI.components.editablelist.editors.ChooseAttributeType', function (editor) {
                editor.init(attrTypes);
            });

            editor.on('done', function (attrType) {
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.add, {
                    attrType: attrType,
                    uri: MobileUI.core.entity.EntityUtils.generateUri(this.parentUri, attrType.name),
                    value: ''
                });

                this.refreshList();
            }, this);
        } else {
            var attrType = attrTypes[0];
            var attrValueUri = MobileUI.core.entity.EntityUtils.generateUri(this.parentUri, attrType.name);
            var values = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrType.attributes, attrValueUri);
            var attrValue = {
                uri: attrValueUri,
                values: values
            };
            if (attrType.type === 'Nested') {
                editor = this.showEditor('MobileUI.components.editablelist.editors.NestedAttributeEditor', function (editor) {
                    editor.init(attrType, attrValue, this.branch);
                    editor.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.add, {
                        attrType: attrType,
                        uri: attrValueUri,
                        value: values

                    });
                    editor.refreshList();
                });
            } else if (attrType.type === 'Reference') {
                editor = this.showEditor('MobileUI.components.editablelist.editors.ReferenceAttributeEditor', function (editor) {
                    editor.init(attrType, attrValue, this.branch);
                    editor.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.add, {
                        attrType: attrType,
                        uri: attrValueUri,
                        value: MobileUI.core.entity.EntityUtils.generateEmptyAttribute(editor.getAttrTypes(), attrValue.uri)
                    });
                    editor.initState();
                });
            } else {
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.add, {
                    attrType: attrType,
                    uri: MobileUI.core.entity.EntityUtils.generateUri(this.parentUri, attrType.name),
                    value: ''
                });

                this.refreshList();
            }

            if (editor) {
                editor.on('done', function () {
                    this.refreshList();
                }, this);
                editor.on('cancel', function (attrTypes) {
                    this.updateLookupsHierarchy(attrTypes, attrValue);
                }, this);
            }
        }
    },

    updateLookupsHierarchy: function (attrTypes, attrValue) {
        MobileUI.core.session.Session.getLookupsManager().removeValuesForAttribute(attrValue.uri);

        attrTypes.forEach(function (subType) {
            ((attrValue.value && attrValue.value[subType.name]) || (attrValue.attributes && attrValue.attributes[subType.name]) || []).forEach(function (value) {
                if (subType.type === 'Nested' || subType.type === 'Reference') {
                    this.updateLookupsHierarchy(subType.attributes, value);
                } else if (subType.dependentLookupCode && !Ext.isEmpty(value.value)) {
                    this.updateDependentLookupManager(subType, value.uri, value);
                }
            }, this);
        }, this);

        if (attrValue.refEntity) {
            var entity = MobileUI.core.entity.TemporaryEntity.getEntity(attrValue.refEntity.objectURI);
            if (!entity) {
                return;
            }
            var entityType = MobileUI.core.Metadata.getEntityType(entity.type);

            this.updateLookupsHierarchy(entityType.attributes, entity);
        }
    },

    actualizeLookupsOnRemove: function (attrTypes, attrValue) {
        (attrTypes || []).forEach(function (subType) {
            ((attrValue.value && attrValue.value[subType.name]) || []).forEach(function (value) {
                if (subType.type === 'Nested') {
                    this.actualizeLookupsOnRemove(subType.attributes, value);
                } else if (subType.dependentLookupCode) {
                    MobileUI.core.session.Session.getLookupsManager().removeDependentLookupValue(subType.uri, value.uri);
                }
            }, this);
        }, this);
    },

    showValidationErrors: function () {
        var message = MobileUI.core.entity.EntityUtils.validationMessageNew(this.errors);
        var toolbar = this.getNotificationToolbar();
        var notification = this.getNotificationMessage();
        if (!Ext.isEmpty(message)) {
            toolbar.setHidden(false);
            notification.setHtml(message);
            notification.setCls('validation-message');
        }
    },

    flattenTree: function (tree, lookup) {
        function depth(lookup) {
            return lookup.parent ? 1 + depth(lookup.parent) : 0;
        }

        var isLeaf = !lookup.children;
        if (lookup.displayName || lookup.name) {
            var value = lookup.name;
            var text = lookup.displayName || value;

            if (text !== value) {
                text += ' (' + value + ')';
            }

            tree.push({
                html: '<span style="' + (isLeaf ? '' : 'color: #999;') +
                ' white-space:nowrap; padding-left: ' + (depth(lookup) - 1) + 'em">' + text + '</span>',
                text: text,
                value: isLeaf ? value : null
            });
        }
        if (!isLeaf) {
            lookup.children.reduce(this.flattenTree.bind(this), tree);
        }
        return tree;
    },

    getPlaceholderText: function (lookupCode, uri, attrTypeUri) {
        var placeHolder = null;
        var lookupManager = MobileUI.core.session.Session.getLookupsManager();
        if (!lookupManager.isInited()) {
            return;
        }
        var parents = lookupManager.getParents(lookupCode, uri, attrTypeUri);
        if (Ext.isEmpty(parents)) {
            return;
        }
        if (parents.length === 1) {
            if (!parents[0].getInfo().dependentLookupCode) {
                return;
            }
        }
        var systemUris = Object.keys(MobileUI.core.Metadata.getEntityTypes()).concat(Object.keys(MobileUI.core.Metadata.getRelationTypes()));
        var filteredParents = parents.filter(function (current) {
            var info = current.getInfo();
            return Ext.isEmpty(info.values) && systemUris.indexOf(info.uri) === -1;
        });
        if (!Ext.isEmpty(filteredParents)) {
            var emptyParents = filteredParents.reduce(function (prev, current) {
                var info = current.getInfo();
                return prev.concat(info.uri);
            }, []).map(function (uri) {
                return MobileUI.core.Metadata.findEntityAttributeByUri(null, uri);
            }).filter(function (item) {
                return item;
            }).map(function (item) {
                return item.label;
            });
            var label = Ext.isEmpty(emptyParents) ? 'Parents' : emptyParents.join(' and ');
            placeHolder = i18n('Please select value for') + ' ' + label;
        }


        return placeHolder;
    },

    processLookups: function (models) {
        if (models.length === 0) {
            return Promise.resolve(models);
        }
        var resolve = function (index) {
            return this.resolveModel(models[index]).then(function () {
                return index === models.length - 1 ?
                    models :
                    resolve(index + 1);
            }.bind(this));
        }.bind(this);

        return resolve(0);
    },

    getLabelForDependentLookup: function (attrValue) {
        var label = attrValue.value;
        if (attrValue.lookupCode && attrValue.lookupCode !== label) {
            label += ' (' + attrValue.lookupCode + ')';
        }

        return label;
    },

    resolveModel: function (model) {
        return new Promise(function (resolve, reject) {
            var lookupManager = MobileUI.core.session.Session.getLookupsManager();

            var dependentLookupCode = model.attrType && model.attrType.dependentLookupCode,
                lookupCode = !dependentLookupCode && model.attrType && model.attrType.lookupCode,
                asyncResolve = false,
                lookups;

            if (lookupCode) {
                lookups = MobileUI.core.Lookups.getLookups(lookupCode).tree || {};
                lookups = lookups && lookups.children || [];
                lookups = lookups.reduce(this.flattenTree.bind(this), []);

                model.values = lookups;
            }

            if (dependentLookupCode || lookupCode) {
                var uri = model.attrValue.uri,
                    autoPopulate = lookupManager.isSupportAutoPopulate(model.attrType.uri),
                    isNonTouched = !lookupManager.isTouched(uri);

                var placeholder;
                if (dependentLookupCode) {
                    model.label = this.getLabelForDependentLookup(model.attrValue);
                    placeholder = this.getPlaceholderText(dependentLookupCode, uri, model.attrType.uri);

                    if (placeholder) {
                        model.disabled = true;

                        if (model.label) {
                            model.error = {value: placeholder};
                        } else {
                            model.label = placeholder;
                        }
                    }
                }

                if (!model.attrValue.value && autoPopulate && isNonTouched && !placeholder) {
                    if (dependentLookupCode) {
                        asyncResolve = true;
                        lookupManager.getValues(dependentLookupCode, uri, model.attrType.uri, null, function (result) {
                            if (result && result.codeValues) {
                                var lookups = result.codeValues[dependentLookupCode];
                                if (lookups && Object.keys(lookups).length === 1) {
                                    var lookupCode = Object.keys(lookups)[0],
                                        value = lookups[lookupCode].displayName;

                                    model.attrValue.lookupCode = lookupCode;
                                    model.attrValue.value = value;
                                    model.label = this.getLabelForDependentLookup(model.attrValue);

                                    this.updateDependentLookup(model.attrType, uri, model.attrValue);
                                }
                            }

                            resolve(model);
                        }, reject, this);
                    } else if (lookupCode && lookups.length === 1) {
                        model.attrValue.value = lookups[0].value;

                        this.onListChangeValue({
                            uri: uri,
                            attrType: model.attrType,
                            value: lookups[0].value
                        });
                    }
                }
            }

            if (!asyncResolve) {
                resolve(model);
            }
        }.bind(this));
    }
});
