/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.MShowEditor', {
    showEditor: function (editorCls, initEditorFunc) {
        var editor = Ext.create(editorCls),
            dialogName = editorCls + new Date().getTime();

        MobileUI.core.SuiteManager.addDialog(dialogName, editor);

        MobileUI.core.SuiteManager.showDialog(dialogName);

        initEditorFunc.call(this, editor);

        editor.on('done', this.closeEditor.bind(this, dialogName));
        editor.on('cancel', this.closeEditor.bind(this, dialogName));

        return editor;
    },

    closeEditor: function (name) {
        MobileUI.core.SuiteManager.removeDialog(name);
        MobileUI.core.SuiteManager.back();
    }
});
