/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.MTransactions', {
    statics: {
        eventTypes: {
            add: 'add',
            edit: 'edit',
            remove: 'remove',
            updateLabel: 'updateLabel'
        }
    },

    transaction: null,
    branch: null,
    parentUri: null,

    initTransaction: function (parentUri, branch) {
        this.parentUri = parentUri;
        this.branch = branch.branch();
    },

    createTransaction: function (attributes) {
        this.transaction = window.uiSDK.entity.Transactions.createTransaction(attributes, this.mutator);
    },

    mutator: function (source, event) {
        var payload = event.payload;

        switch (event.type) {
            case MobileUI.components.editablelist.MTransactions.eventTypes.add:
                window.uiSDK.entity.AttributesEditor.addAttribute(source, payload.attrType, payload.uri, Ext.clone(payload.value), payload.crosswalks);
                break;
            case MobileUI.components.editablelist.MTransactions.eventTypes.edit:
                window.uiSDK.entity.AttributesEditor.editAttribute(source, payload.attrType, payload.uri, Ext.clone(payload.value), payload.crosswalks);
                break;
            case MobileUI.components.editablelist.MTransactions.eventTypes.remove:
                window.uiSDK.entity.AttributesEditor.removeAttribute(source, payload.attrType, payload.uri);
                break;
            case MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel:
                var findResult = MobileUI.core.entity.EntityUtils.findAttribute(source, payload.uri);
                findResult.attribute.label = payload.label;
                if (payload.relationshipLabel) {
                    findResult.attribute.relationshipLabel = payload.relationshipLabel;
                }
                break;
        }
    },

    executeCommandInCurrentBranch: function (type, payload) {
        this.branch.pushEvent({
            type: type,
            payload: payload
        });
    }
});
