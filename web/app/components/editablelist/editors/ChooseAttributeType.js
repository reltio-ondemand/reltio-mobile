/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.ChooseAttributeType', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'choose-attribute-type',
        layout: 'vbox',
        scrollable: null,
        cls: 'card chooseattrtype',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },

    filter: '',
    attrTypes: null,

    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancelTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Add attribute'),
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                text: '',
                cls: 'search-field',
                name: 'search-field',
                placeHolder: i18n('Search'),
                listeners: {
                    keyup: this.onSearchChange,
                    clearicontap: this.onSearchChange,
                    scope: this
                }
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'list',
                defaultType: 'reltioListItem',
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                listeners: {
                    selectItem: this.onListSelectItem,
                    scope: this
                }
            }
        ]);
    },

    getList: function () {
        return this.down('elist[name=list]');
    },

    getSearchField: function () {
        return this.down('textfield[name=search-field]');
    },

    init: function (attrTypes) {
        this.attrTypes = attrTypes;
        this.fillList();
    },

    fillList: function () {
        var list = this.getList();
        var store = list.getStore();

        store.clearData();
        store.add(this.attrTypes
            .filter(function (attr) {
                return !this.filter || attr.label.toLowerCase().indexOf(this.filter.toLowerCase()) !== -1;
            }, this)
            .map(function (attr) {
                return {
                    label: attr.label,
                    required: MobileUI.core.CardinalityChecker.isRequired(attr),
                    type: MobileUI.components.editablelist.items.ListItem.types.selectable,
                    userData: attr
                };
            }));

        list.refresh();
    },

    onListSelectItem: function (data) {
        this.fireEvent('done', data.record.get('userData'));
    },

    onSearchChange: function () {
        this.filter = this.getSearchField().getValue();
        this.fillList();
    }
});
