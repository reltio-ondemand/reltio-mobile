/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.CreateEntityCmp', {
    extend: 'Ext.Panel',
    xtype: 'entitycreator',
    requires: [
        'MobileUI.components.editablelist.MAttributesPanel',
        'MobileUI.components.editablelist.MTransactions',
        'MobileUI.components.editablelist.MShowEditor',
        'MobileUI.components.list.MListHeight'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        profileConfiguration: 'MobileUI.controller.MProfileConfiguration',
        attributesPanel: 'MobileUI.components.editablelist.MAttributesPanel',
        transactions: 'MobileUI.components.editablelist.MTransactions',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },
    config: {
        name: 'create-entity-cmp',
        scrollable: null,
        items: []
    },

    entityType: null,
    entity: null,

    initialize: function () {
        this.callParent();
        this.add([
            {

                xtype: 'elist',
                defaultType: 'reltioListItem',
                scrollable: false,
                cls: 'reltio-profile reltio-list',
                height: 0,
                itemId: 'create-entity-attributes-list',
                name: 'list',
                plugins: [{type: 'swipetodelete', useMenu: true}],
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                listeners: {
                    selectItem: this.onListSelectItem,
                    changeValue: this.onListChangeValue,
                    addAttribute: this.onListAddAttribute,
                    'delete': this.onListDeleteAttribute,
                    recalculate: this.recalculateHeightDelayed,
                    scope: this
                }
            }
        ]);
    },

    getList: function () {
        return this.down('elist[name=list]');
    },

    init: function (entityType, entityUri, refAttributeUri, errors) {
        this.entityType = entityType;
        var attributesPart = '/attributes';
        this.entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri);
        this.createTransaction(this.entity.attributes);
        this.initTransaction(this.entity.uri + attributesPart, this.transaction);
        var replaceIfExists = function (error, field) {
            var value = error[field];
            if (value && value.indexOf(refAttributeUri) > -1) {
                var index = value.indexOf(refAttributeUri);
                var endPart = value.substring(index + refAttributeUri.length);
                endPart = (endPart.length === 0 || endPart.indexOf(attributesPart) === 0) ? endPart : attributesPart + endPart;
                error[field] = value.substring(0, index) + entityUri + endPart;
            }
        };
        this.errors = errors ? errors.map(function(error){
            var anError = Ext.clone(error);
            replaceIfExists(anError, 'objectUri');
            replaceIfExists(anError, 'objectParentUri');
            return anError;
        }): null;
        this.refreshList(true);
    },

    getSource: function () {
        return {
            uri: this.entity.uri,
            value: this.branch.getCurrentProjection()
        };
    },

    refreshList: function (preventEvent) {
        var list = this.getList();
        var store = list.getStore();
        var type = Ext.clone(this.entityType);
        this.getModelsFromSource(type, this.entityType.attributes, this.getProfileConfiguration(this.entity.type)).then(function (models) {
            store.clearData();
            store.add(models);

            list.refresh();

            var task = Ext.create('Ext.util.DelayedTask', function () {
                this.updateListHeight(list);
            }, this);
            task.delay(500);
        }.bind(this));
        if (!preventEvent){
            this.fireEvent('refreshed');
        }

    },
    validate: function () {
        var source = this.getSource();

        this.errors = MobileUI.core.session.Session.validateAttributes(source.value, this.entityType.attributes, source.uri);
        if (this.errors.length) {
            this.refreshList();
            return this.errors;
        }else {
            return [];
        }
    },
    onSaveAction: function () {
            this.branch.save();
            this.entity.attributes = this.transaction.getCurrentProjection();
            this.entity.label = MobileUI.core.entity.EntityUtils.evaluateEntityLabel(this.entity, this.entityType.dataLabelPattern);
    },

    getEntity: function(){
        return this.entity;
    },

    getDoneButton: function(){
        return null;
    }
});
