/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.DependentLookupValues', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'dependent-lookup-values',
        layout: 'vbox',
        scrollable: null,
        cls: 'card lookupeditor',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },

    attrType: null,
    attrValues: null,
    lookups: [],
    selectedItem: null,
    selectedRecord: null,
    filter: null,

    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancelTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'text-button',
                        text: i18n('Done'),
                        listeners: {
                            tap: this.onDoneTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'search-field',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search'),
                listeners: {
                    keyup: this.onSearchChange,
                    clearicontap: this.onSearchChange,
                    scope: this
                }
            },
            {
                xtype: 'elist',
                defaultType: 'reltioListItem',
                flex: 1,
                cls: 'reltio-list',
                name: 'list',
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                listeners: {
                    selectItem: this.onListSelectItem,
                    scope: this
                }
            }
        ]);
    },

    getList: function () {
        return this.down('elist[name=list]');
    },

    getSearchField: function () {
        return this.down('textfield[name=search-field]');
    },

    setTitle: function(text){
        return this.down('component[name=title]').setHtml(Ext.util.Format.htmlEncode(text));
    },

    init: function (attrType, attrValue) {
        this.attrType = attrType;
        this.attrValue = attrValue;
        this.selectedItem = attrValue.lookupCode ? attrValue : null;
        this.filter = '';
        this.setTitle(i18n(attrType.label));
        this.getLookups();
    },

    getLookups: function () {
        MobileUI.core.session.Session.getLookupsManager().getValues(this.attrType.dependentLookupCode, this.attrValue.uri, this.attrType.uri, this.filter, function (response) {
            this.lookups = response.codeValues[this.attrType.dependentLookupCode];
            this.refreshList();
        }, null, this);
    },

    onDoneTap: function () {
        this.fireEvent('done', this.selectedItem);
    },

    equalsLookups: function(a,b){
      if (Ext.isString(a) && Ext.isString(b)){
          return a === b;
      }
      if (Ext.isObject(a) && Ext.isObject(b)){
          return a.lookupCode === b.lookupCode;
      }
      return false;
    },

    onListSelectItem: function (e) {
        var value = e.record.get('userData');

        if (value === null) {
            return;
        }

        if (this.selectedRecord) {
            this.selectedRecord.getRecord().set('selected', false);
            this.selectedRecord.recreate();
        }

        if (this.equalsLookups(value, this.selectedItem)) {
            this.selectedRecord = null;
            this.selectedItem = null;
        } else {
            this.selectedItem = value;
            this.selectedRecord = e.item;
            this.selectedRecord.getRecord().set('selected', true);
            this.selectedRecord.recreate();
        }
    },

    onSearchChange: function () {
        this.filter = this.getSearchField().getValue();
        this.getLookups();
    },

    refreshList: function () {
        var list = this.getList(),
            store = list.getStore();

        store.clearData();

        if (!Ext.isEmpty(this.lookups)) {
            var model = Object.keys(this.lookups)
                .map(function (key) {
                    var text = this.lookups[key].displayName || key;

                    if (text !== key) {
                        text += ' (' + key + ')';
                    }

                    return {
                        label: text,
                        userData: {lookupCode: key, value: this.lookups[key].displayName || key},
                        type: MobileUI.components.editablelist.items.ListItem.types.selectable,
                        selected: key === (this.selectedItem && this.selectedItem.lookupCode)
                    };
                }, this)
                .sort(function (a, b) {
                    if (a.label > b.label) {
                        return 1;
                    }
                    if (a.label < b.label) {
                        return -1;
                    }
                    return 0;
                });

            store.add(model);
        } else {
            store.add({
                label: i18n('No values have been found'),
                baseCls: 'rl-dependent-disabled',
                userData: null,
                type: MobileUI.components.editablelist.items.ListItem.types.selectable
            });
        }

        list.refresh();

        this.selectedRecord = list.getViewItems().filter(function (item) {
            return item.getRecord().get('selected');
        })[0];
    }
});
