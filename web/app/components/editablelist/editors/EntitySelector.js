/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.EntitySelector', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    mixins: {
        typeAhead: 'MobileUI.controller.MTypeAhead',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },
    config: {
        name: 'entity-selector',
        layout: 'vbox',
        scrollable: null,
        cls: 'card chooseentitydialog',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },

    attrValue: null,
    entityType: null,
    offset: 0,
    limit: 50,
    typeaheadValue: null,

    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancelTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'title',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'search-field',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search'),
                listeners: {
                    keyup: this.onKeyUp,
                    clearicontap: this.onClearIcon,
                    scope: this
                }
            },
            {
                xtype: 'button',
                name: 'add-new-entity-button',
                icon: 'resources/images/reltio/relations/add.svg',
                text: '',
                cls: 'add-new-entity-btn',
                listeners: {
                    tap: this.onAddNewEntityTap,
                    scope: this
                }
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'list',
                scrollToTopOnRefresh: false,
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                listeners: {
                    selectItem: this.onListSelectItem,
                    fetch: this.onListFetch,
                    scope: this
                }
            }
        ]);
    },

    getList: function () {
        return this.down('elist[name=list]');
    },

    getSearchField: function () {
        return this.down('textfield[name=search-field]');
    },

    getTitle: function () {
        return this.down('component[name=title]');
    },

    getAddNewEntityButton: function () {
        return this.down('button[name=add-new-entity-button]');
    },

    init: function (attrType, attrValue) {
        this.attrValue = attrValue;
        this.entityType = Ext.clone(MobileUI.core.Metadata.getEntityType(attrType.referencedEntityTypeURI));
        var uris = attrType.referencedAttributeURIs;
        this.entityType.attributes = (this.entityType.attributes || []).filter(function (attrType) {
            return uris.indexOf(attrType.uri) !== -1;
        });
        this.getTitle().setHtml(i18n('Add %1', Ext.util.Format.htmlEncode(this.entityType.label)));

        this.typeaheadValue = MobileUI.core.search.SearchParametersManager.getTypeAhead();

        this.on('done', this.restoreTypeAheadState);
        this.on('cancel', this.restoreTypeAheadState);
    },

    typeaheadBehaviour: function () {
        return false;
    },

    restoreTypeAheadState: function () {
        MobileUI.core.search.SearchParametersManager.setTypeAhead(this.typeaheadValue);
    },

    onTypeAheadSearch: function () {
        this.showAddNewEntity();

        //this.offset = -this.limit;
        this.getList().startFetch();
    },

    onListFetch: function () {
        //this.offset += this.limit;
        this.doSearch();
    },

    doSearch: function () {
        var search = {value: this.value};

        MobileUI.core.session.Session.chooseEntitySearch(search, this.entityType.uri.split('/')[2], this.offset, this.limit, this.dataReceived, function (e) {
            MobileUI.core.Logger.log(e);
        }, this);
    },

    onListSelectItem: function (item) {
        var uri = item.getData()['userData'].uri;

        if (uri) {
            MobileUI.core.session.Session.requestEntity(uri, function (response) {
                this.fireEvent('done', response);
            }, null, this, true);
        }
    },

    onAddNewEntityTap: function () {
        var entityUri = MobileUI.core.entity.EntityUtils.generateUri(null, 'entities');
        var entity = {
            uri: entityUri,
            type: this.entityType.uri,
            attributes: MobileUI.core.entity.EntityUtils.generateEmptyAttribute(this.entityType.attributes, entityUri + '/attributes', this.getSearchField().getValue())
        };
        MobileUI.core.entity.TemporaryEntity.addEntity(entity);
        this.fireEvent('done', entity);
    },

    hideAddNewEntity: function () {
        this.getAddNewEntityButton().removeCls('expanded');
    },

    showAddNewEntity: function () {
        var canCreate = MobileUI.core.PermissionManager.securityService().metadata.canCreate(this.entityType);
        var canInitiateRequest = MobileUI.core.PermissionManager.securityService().metadata.canInitiateRequest(this.entityType);
        if (canCreate || canInitiateRequest) {
            var button = this.getAddNewEntityButton();
            button.addCls('expanded');
            if (canCreate) {
                button.setText(i18n('Create "%1" as new %2', this.getSearchField().getValue(), this.entityType.label));
            }
            else {
                button.setText(i18n('Suggest "%1" as new %2', this.getSearchField().getValue(), this.entityType.label));
            }
        }
    }
});
