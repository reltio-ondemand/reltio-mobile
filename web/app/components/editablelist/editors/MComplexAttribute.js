/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.MComplexAttribute', {
    attrType: null,
    attrValue: null,

    init: function (attrType, attrValue, branch, errors) {
        this.attrType = attrType;
        this.attrValue = attrValue;
        this.errors = errors;
        this.getHeader().setHtml(Ext.util.Format.htmlEncode(this.getAction(attrValue, attrType)));
        this.initTransaction(attrValue.uri, branch);
    },

    getAction: function(attrValue, attrType){
        var label = i18n(attrType.label);
        return attrValue && attrValue.value ? i18n('Edit %1', label) : i18n('Create %1', label);
    },

    getList: function () {
        return this.down('elist[name=list]');
    },

    getHeader: function(){
        return this.down('component[name=title]');
    },

    getNotificationToolbar: function () {
        return this.down('component[name=notification-toolbar]');
    },

    getNotificationMessage: function () {
        return this.down('component[name=notification-message]');
    },

    getCancel: function () {
        return this.down('component[name=cancel]');
    },

    onSaveButtonTap: function () {
        this.errors = MobileUI.core.session.Session.validateAttributes(this.getSource().value, this.getAttrTypes(), this.attrValue.uri);

        if (this.errors.length) {
            this.showValidationErrors();
            this.refreshList();
        } else {
            var source = this.getSource();
            if (this.getDoneButton()) {
                this.getDoneButton().setDisabled(true);
            }
            if (this.attrType.type === 'Reference') {
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel, {
                    uri: this.attrValue.uri,
                    label: source.label,
                    relationshipLabel: MobileUI.core.entity.EntityUtils.evaluateEntityLabel({attributes: source.value}, this.attrType.relationshipLabelPattern)
                });
            } else {
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel, {
                    uri: this.attrValue.uri,
                    label: MobileUI.core.entity.EntityUtils.evaluateLabel(null, source.value, this.attrType)
                });
            }
            this.branch.save();

            this.fireEvent('done');
        }
    },

    cancel: function () {
        this.branch.remove();
        this.fireEvent('cancel', this.getAttrTypes());
    },

    onCancelButtonTap: function () {
        this.getCancel().setDisabled(true);
        this.cancel();
    },

    onCloseNotificationButtonTap: function () {
        this.getNotificationToolbar().setHidden(true);
        this.getNotificationMessage().setHtml('');
        this.getNotificationMessage().setCls('notification-message');
    },

    refreshList: function () {
        var list = this.getList();
        var store = list.getStore();

        this.getModelsFromSource(this.attrType, this.getAttrTypes()).then(function (models) {
            store.clearData();
            store.add(models);

            list.refresh();
            var task = Ext.create('Ext.util.DelayedTask', function () {
                this.updateListHeight(list);
            }, this);
            task.delay(500);
        }.bind(this));
    }
});
