/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.NestedAttributeEditor', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    config: {
        name: 'nested-attribute-editor',
        layout: 'vbox',
        scrollable: true,
        cls: 'card popupEditor',
        fullscreen: true,
        items: []
    },
    requires: [
        'MobileUI.components.list.MListHeight',
        'MobileUI.components.editablelist.MAttributesPanel',
        'MobileUI.components.editablelist.MTransactions',
        'MobileUI.components.editablelist.editors.MComplexAttribute',
        'MobileUI.components.editablelist.MShowEditor'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        attributesPanel: 'MobileUI.components.editablelist.MAttributesPanel',
        transactions: 'MobileUI.components.editablelist.MTransactions',
        complexAttribute: 'MobileUI.components.editablelist.editors.MComplexAttribute',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },

    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        name: 'cancel',
                        listeners: {
                            tap: this.onCancelButtonTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title',
                        html: '',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'doneButton',
                        text: i18n('Done'),
                        disabled: true,
                        listeners: {
                            tap: this.onSaveButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                name: 'notification-toolbar',
                hidden: true,
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                items: [
                    {
                        xtype: 'component',
                        name: 'notification-message',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        cls: 'icon-button',
                        listeners: {
                            tap: this.onCloseNotificationButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'elist',
                defaultType: 'reltioListItem',
                editing: true,
                flex: 1,
                scrollable: false,
                height: 0,
                cls: 'reltio-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true}
                ],
                name: 'list',
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                listeners: {
                    selectItem: this.onListSelectItem,
                    changeValue: this.onListChangeValue,
                    addAttribute: this.onListAddAttribute,
                    'delete': this.onListDeleteAttribute,
                    recalculate: this.recalculateHeightDelayed,
                    scope: this
                }
            }
        ]);
    },
    getDoneButton: function(){
        return this.down('component[name=doneButton]');
    },
    getSource: function () {
        var source = MobileUI.core.entity.EntityUtils.findAttribute(this.branch.getCurrentProjection(), this.attrValue.uri).attribute;
        if (source && source.value && !MobileUI.core.entity.EntityUtils.isEmptyAttribute(source.value)){
            this.getDoneButton().setDisabled(false);
        }
        return source;
    },

    getAttrTypes: function(){
        return this.attrType.attributes;
    }
});
