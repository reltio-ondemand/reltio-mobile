/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.ReferenceAttributeEditor', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    config: {
        name: 'reference-attribute-editor',
        layout: 'vbox',
        scrollable: true,
        cls: 'card popupEditor',
        fullscreen: true,
        items: []
    },
    requires: [
        'MobileUI.components.list.MListHeight',
        'MobileUI.plugins.SwipeToDelete',
        'MobileUI.components.editablelist.MAttributesPanel',
        'MobileUI.components.editablelist.MTransactions',
        'MobileUI.components.editablelist.editors.MComplexAttribute',
        'MobileUI.components.editablelist.MShowEditor',
        'MobileUI.components.editablelist.editors.CreateEntityCmp'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        attributesPanel: 'MobileUI.components.editablelist.MAttributesPanel',
        transactions: 'MobileUI.components.editablelist.MTransactions',
        complexAttribute: 'MobileUI.components.editablelist.editors.MComplexAttribute',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },

    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        name: 'cancel',
                        listeners: {
                            tap: this.onCancelButtonTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title',
                        html: '',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'refDone',
                        text: i18n('Done'),
                        listeners: {
                            tap: this.onReferenceSaveButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                name: 'notification-toolbar',
                hidden: true,
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                items: [
                    {
                        xtype: 'component',
                        name: 'notification-message',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        cls: 'icon-button',
                        listeners: {
                            tap: this.onCloseNotificationButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'elist',
                defaultType: 'reltioListItem',
                editing: true,
                scrollable: false,
                height: 0,
                name: 'entity-selector',
                cls: 'reltio-list',
                plugins: [
                    {
                        type: 'swipetodelete',
                        useMenu: true,
                        swipeRightButtons: [{
                            label: i18n('Change'),
                            ui: 'reltio-swipe-change',
                            action: 'changeRefEntity',
                            id: 'changeRefEntity',
                            width: 70
                        }],
                        filterButtons: function(){
                            var security = MobileUI.core.PermissionManager.securityService().metadata;
                            return security.canCreateOrInitiate(this.attrType) || security.canUpdate(this.attrType);
                        }.bind(this)
                    }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                style: 'margin-bottom: 0',
                listeners: {
                    changeRefEntity: this.onListChangeRefEntity,
                    scope: this
                }
            },
            {
                xtype: 'entitycreator',
                name: 'ref-entity-creator',
                listeners: {
                    calculatedHeight: function(newHeight){
                        this.getEntityCreator().setHeight(newHeight);
                    },
                    refreshed: function(){
                        this.refreshList();
                    },
                    scope: this
                },
                style: 'margin-bottom: 10px'
            },
            {
                name: 'listSeparator',
                cls: 'rl-header',
                html: i18n('Others'),
                style: 'margin-left: 15px'
            },
            {
                xtype: 'elist',
                defaultType: 'reltioListItem',
                editing: true,
                flex: 1,
                scrollable: false,
                height: 0,
                name: 'list',
                cls: 'reltio-list',
                plugins: [
                    {
                        type: 'swipetodelete',
                        useMenu: true,
                        swipeRightButtons: [ {
                            label: i18n('Delete'),
                            ui: 'reltio-swipe-delete',
                            action: 'delete',
                            id: 'delete',
                            width: 70
                        }]
                    }
                ],

                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                style: 'margin-top: 0',
                listeners: {
                    selectItem: this.onListSelectItem,
                    changeValue: this.onListChangeValue,
                    addAttribute: this.onListAddAttribute,
                    'delete': this.onListDeleteAttribute,
                    calculatedHeight: this.refreshEntityCreatorIfNecessary,
                    recalculate: this.recalculateHeightDelayed,
                    scope: this
                }
            }
        ]);
    },

    initState: function () {
        if (!this.getSource().refEntity) {
            this.showEntitySelector();
        } else {
            this.refreshList();
            this.refreshEntitySelector();
        }
    },
    getEntityCreator: function(){
        return this.down('entitycreator');
    },

    getDoneButton: function(){
        return this.down('component[name=refDone]');
    },

    initEntityCreator: function(entityUri){
        var entityType = Ext.clone(MobileUI.core.Metadata.getEntityType(this.attrType.referencedEntityTypeURI));
        var uris = this.attrType.referencedAttributeURIs;
        entityType.attributes = (entityType.attributes || []).filter(function (attrType) {
            return uris.indexOf(attrType.uri) !== -1;
        });
        this.getEntityCreator().init(entityType, entityUri, this.getSource().uri,  this.errors);
        this.refreshList();
    },
    getHeaderRecord: function () {
        var attrType = this.attrType;
        return {
            type: MobileUI.components.editablelist.items.ListItem.types.header,
            label: i18n(attrType.label),
            disableSwipe: true,
            required: MobileUI.core.CardinalityChecker.isRequired(attrType),
            message: MobileUI.core.CardinalityChecker.getMessage(attrType),
            error: (this.errors || []).filter(function (err) {
                return !err.objectUri && err.attrType === attrType;
            }, this)[0]
        };
    },

    refreshEntitySelector: function(){
        var attrValue = this.getSource();
        var isTemporary = MobileUI.core.entity.EntityUtils.isTemporaryUri(attrValue.refEntity.objectURI);
        var label = isTemporary ? i18n('New entity will be created') : attrValue.label;
        if (isTemporary) {
            this.initEntityCreator(attrValue.refEntity.objectURI);
            MobileUI.core.session.Session.getUriMapping().register(attrValue.refEntity.objectURI, attrValue.uri);
        }
        this.getEntityCreator().setHidden(!isTemporary);
        var list = this.getEntitySelectorComponent();
        var store = list.getStore();
        store.clearData();

        var model = [
            this.getHeaderRecord(),
            {
                type: MobileUI.components.editablelist.items.ListItem.types.entityInfo,
                label: label || i18n('<No label>')
            }
        ];
        store.add(model);
        list.refresh();
        var task = Ext.create('Ext.util.DelayedTask', function () {
            this.updateListHeight(list);
        }, this);
        task.delay(500);
    },
    refreshEntityCreatorIfNecessary: function(){
        if (this.getEntityCreator()) {
            this.getEntityCreator().refreshList(false);
        }
    },

    getEntitySelectorComponent: function(){
        return this.down('elist[name=entity-selector]');
    },

    getSource: function () {
        return MobileUI.core.entity.EntityUtils.findAttribute(this.branch.getCurrentProjection(), this.attrValue.uri).attribute;
    },

    getReferenceAttributeUri: function(){
        var uriParts = this.parentUri.split('/');
        return uriParts[uriParts.length - 1];
    },

    showEntitySelector: function () {
        var editor = this.showEditor('MobileUI.components.editablelist.editors.EntitySelector', function (editor) {
            editor.init(this.attrType, this.attrValue);
        });

        editor.on('done', function (entity) {
            var attrValue = this.getSource();
            attrValue.refEntity = {
                objectURI: entity.uri,
                type: entity.type
            };
            attrValue.refRelation = {
                objectURI: 'relation/' + this.getReferenceAttributeUri(),
                crosswalks: [{
                    type: 'configuration/sources/Reltio'
                }]
            };
            attrValue.label = entity.label;

            this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.edit, {
                attrType: this.attrType,
                uri: this.attrValue.uri,
                value: attrValue
            });
            this.refreshEntitySelector();
            this.refreshList();
        }, this);
        editor.on('cancel', function () {
            if (!this.getSource().refEntity) {
                this.cancel();
            }
        }, this, {delay: 500});
        return editor;
    },

    getAttrTypes: function () {
        return this.attrType.referencedAttributeURIs
            .filter(function (uri) {
                return uri.indexOf('configuration/relationTypes') === 0;
            })
            .map(function (uri) {
                return MobileUI.core.Metadata.findRelationAttributeByUri(null, uri);
            });
    },
    isEntityCreatorExists: function(){
        var entityCreator = this.getEntityCreator();
        return entityCreator && !entityCreator.isHidden();
    },
    onReferenceSaveButtonTap: function(){
        if (this.validate().length > 0){
            return;
        }
        if (this.isEntityCreatorExists()){
            var entityCreator = this.getEntityCreator();
            entityCreator.onSaveAction();
            var entity = entityCreator.getEntity();
            this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel, {
                uri: this.attrValue.uri,
                label: entity.label
            });
        }
        this.onSaveButtonTap();
    },

    validate: function(){
        var source = this.getSource();
        this.errors = MobileUI.core.session.Session.validateAttributes(source.value, this.getAttrTypes(), source.uri);
        var errors = this.isEntityCreatorExists() ? this.getEntityCreator().validate(): [];
        this.errors = errors.concat(this.errors);
        if (this.errors.length > 0) {
            this.showValidationErrors();
            this.refreshList();
            return this.errors;
        }
        return [];
    },

    onListChangeRefEntity: function () {
        this.showEntitySelector();
    }
});