/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.SelectImageEditor', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    config: {
        name: 'select-image-editor',
        layout: 'vbox',
        scrollable: true,
        cls: 'card popupEditor',
        fullscreen: true,
        items: []
    },
    requires: [
        'MobileUI.components.list.MListHeight',
        'MobileUI.components.editablelist.MShowEditor',
        'MobileUI.components.editablelist.MTransactions',
        'MobileUI.components.editablelist.editors.UploadImage'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        transaction: 'MobileUI.components.editablelist.MTransactions',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },
    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancelButtonTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title',
                        html: i18n('Select image'),
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        text: i18n('Done'),
                        listeners: {
                            tap: this.onSaveButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'elist',
                editing: true,
                scrollable: false,
                height: 0,
                name: 'new-select-image-list',
                cls: 'reltio-list',
                plugins: [
                    'swipetodelete'
                ],
                defaultType: 'reltioListItem',
                store: {
                    data: [],
                    model: 'MobileUI.components.editablelist.Model'
                },
                style: 'margin-bottom: 0',
                listeners: {
                    selectItem: this.onListSelectItem,
                    addAttribute: this.onListAddAttribute,
                    'delete': this.onListDeleteAttribute,
                    scope: this
                }
            }
        ]);
    },
    init: function (originalEntity, branch) {
        this.originalEntity = originalEntity;
        this.initTransaction(this.originalEntity.uri, branch);
        this.defaultProfilePic = originalEntity.defaultProfilePic;
        this.defaultProfilePicValue = originalEntity.defaultProfilePicValue;
    },
    getSource: function () {
        return {
            uri: this.originalEntity.uri,
            type: this.originalEntity.type,
            defaultProfilePic: this.originalEntity.defaultProfilePic,
            attributes: this.branch.getCurrentProjection(),
            crosswalks: this.originalEntity.crosswalks
        };
    },
    getList: function () {
        return this.down('elist[name=new-select-image-list]');
    },
    refreshList: function () {
        var entity = this.getSource();
        var images = MobileUI.core.entity.EntityUtils.getImageSourcesNew(entity);


        var model = images.map(function (image) {
            var info = this.extractFields(image.value);

            return {
                label: info.label,
                secondaryLabel: info.secondaryLabel,
                thirdLabel: i18n(this.fillDate(image.uri, entity), MobileUI.core.I18n.DATE_LONG),
                avatar: image.value,
                uri: image.uri,
                state: this.defaultProfilePic === image.uri ? 'selected' : '',
                type: MobileUI.components.editablelist.items.ListItem.types.imageItem
            };
        }, this);

        var list = this.getList(),
            store = list.getStore();
        store.clearData();
        store.add([{
            disableSwipe: true,
            type: MobileUI.components.editablelist.items.ListItem.types.imagesAddButton
        }]);
        store.add(model);
        list.refresh();
        this.updateListHeight(list);
    },


    extractFields: function (image) {
        if (image.indexOf('data:') === 0) {
            var regex = /^data:.+\/(.+);base64,(.*)$/,
                matches = image.match(regex),
                ext = matches[1];
            return {
                label: ext,
                secondaryLabel: ''
            };
        } else {
            return {
                label: this.extractName(image),
                secondaryLabel: this.extractFrom(image)
            };
        }
    },

    extractName: function (item) {
        var name = item,
            position = item.lastIndexOf('/');
        if (position !== -1) {
            name = item.substring(position + 1);
        }
        return name;
    },

    extractFrom: function (item) {
        var source = 'Reltio';
        if (item.indexOf(MobileUI.core.Services.getGeneralSettings().imagePath) !== 0) {
            source = MobileUI.core.util.Util.splitUri(item).host || source;
        }
        return i18n('From') + ' ' + source;
    },

    fillDate: function (imageAttributeUri, entity) {
        var crosswalk = (entity.crosswalks || []).filter(function (crosswalk) {
            return crosswalk.attributes.some(function (attributeUri) {
                return attributeUri === imageAttributeUri;
            });
        });
        var date = !Ext.isEmpty(crosswalk) ? crosswalk[0].createDate : null;
        return Ext.Date.parseDate(date, 'c');
    },
    onCancelButtonTap: function () {
        this.branch.remove();
        this.fireEvent('cancel');
    },
    onListAddAttribute: function () {
        var editor = this.showEditor('MobileUI.components.editablelist.editors.UploadImage', function (editor) {
            editor.initUploader();
        });

        editor.on('done', function (url) {
            var entityType = MobileUI.core.Metadata.getEntityType(this.originalEntity.type);
            var attrType = entityType.attributes.filter(function (attribute) {
                return MobileUI.core.entity.EntityUtils.isImageAttribute(entityType, attribute);
            })[0];
            if (attrType) {
                this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.add, {
                    attrType: attrType,
                    uri: MobileUI.core.entity.EntityUtils.generateUri(this.originalEntity.uri + '/attributes', attrType.name),
                    value: url
                });

            }
            this.refreshList();
        }, this);
    },
    onListDeleteAttribute: function (record) {
        var entityType = MobileUI.core.Metadata.getEntityType(this.originalEntity.type);
        var attrType = entityType.attributes.filter(function (attribute) {
            return MobileUI.core.entity.EntityUtils.isImageAttribute(entityType, attribute);
        })[0];
        if (attrType) {
            this.executeCommandInCurrentBranch(MobileUI.components.editablelist.MTransactions.eventTypes.remove, {
                attrType: attrType,
                uri: record.get('uri')
            });

        }
        if (record.get('uri') === this.defaultProfilePic) {
            this.defaultProfilePic = null;
            this.defaultProfilePicValue = null;
        }
        this.refreshList();


    },
    onSaveButtonTap: function () {
        this.branch.save();
        this.fireEvent('done', {defaultProfilePic: this.defaultProfilePic, defaultProfilePicValue: this.defaultProfilePicValue});
    },
    onListSelectItem: function (e) {
        var record = e.record;
        var uri = record.get('uri');
        this.defaultProfilePic = uri;
        this.defaultProfilePicValue = record.get('avatar');
        this.refreshList();
    }
});
