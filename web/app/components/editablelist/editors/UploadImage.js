/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.editors.UploadImage', {
    extend: 'MobileUI.components.editablelist.editors.BaseEditor',
    config: {
        name: 'upload-image-editor',
        layout: 'vbox',
        scrollable: true,
        cls: 'card popupEditor uploadimage property-editor',
        fullscreen: true,
        items: []
    },
    requires: [
        'MobileUI.components.editablelist.MShowEditor'
    ],
    mixins: {
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },


    initialize: function () {
        this.callParent(arguments);
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancelButtonTap,
                            scope: this
                        }
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Add image'),
                        flex: 1
                    },
                    {
                        cls: 'text-button',
                        text: i18n('Save'),
                        listeners: {
                            tap: this.onSaveButtonTap,
                            scope: this
                        }
                    }
                ]
            },
            {
                xtype: 'component',
                cls: 'label',
                html: i18n('Upload image by Url')
            },
            {
                xtype: 'textfield',
                cls: 'property-editor-field',
                name: 'urlfield',
                placeHolder: i18n('url')
            },
            {
                xtype: 'component',
                cls: 'label',
                html: i18n('or')
            },
            {
                xtype: 'fileupload',
                cls: 'save-new-entity-button',
                text: i18n('Take photo'),
                listeners: {
                    beforeUpload: this.onTakePhotoButtonTap,
                    uploaded: this.onImageUploaded,
                    failed: this.onUploadFailed,
                    scope: this
                }
            }
        ]);
    },

    initUploader: function () {
        this.getUrlText().setValue('');
    },
    getPanel: function () {
        return this;
    },
    getUrlText: function () {
        return this.down('textfield[name=urlfield]');
    },

    onTakePhotoButtonTap: function () {
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
    },

    onSaveButtonTap: function () {
        if (Ext.isEmpty(this.getUrlText().getValue())) {
            MobileUI.components.MessageBox.alert(i18n('Warning'), i18n('Please, choose an image'));
        } else {
            this.fireEvent('done', this.getUrlText().getValue());
        }
    },
    onCancelButtonTap: function () {
        this.fireEvent('cancel');
    },
    onImageUploaded: function (resp) {
        this.getPanel().setMasked(null);
        resp = Ext.isEmpty(resp) ? '' : resp.url;
        this.getUrlText().setValue(resp);
    },
    onUploadFailed: function () {
        this.getPanel().setMasked(null);
    }
});
