/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.inline.SideMenu', {
    extend: 'MobileUI.components.Popup',
    requires: ['Ext.mixin.Observable'],
    mixins: {
        observable: Ext.mixin.Observable
    },
    statics: {
        createMenu: function (buttons, disabled) {
            var popup = Ext.create('MobileUI.components.editablelist.inline.SideMenu');
            popup.setWidth(200);
            popup.setHeight(40 * buttons.length);
            popup.setHideOnOrientationChange(false);
            popup.add(buttons.map(function (button) {
                return {
                    xtype: 'button',
                    cls: 'popup-menu-item-button',
                    html: button.label,
                    disabled: disabled,
                    handler: function () {
                        popup.fireEvent('action', button.action);
                    }
                };
            }));
            return popup;
        }
    },

    initialize: function () {
        this.callParent(arguments);
    }
});