/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.ListItem', {
    extend: 'Ext.dataview.component.DataItem',
    xtype: 'reltioListItem',

    requires: [
        'MobileUI.components.editablelist.Model',
        'MobileUI.components.editablelist.inline.SideMenu',
        'Ext.XTemplate'
    ],

    mixins: {
        attribute: 'MobileUI.components.editablelist.items.MAttributeEListItem',
        header: 'MobileUI.components.editablelist.items.MHeaderEListItem',
        addButton: 'MobileUI.components.editablelist.items.MAddButtonEListItem',
        selectable: 'MobileUI.components.editablelist.items.MSelectableEListItem',
        entityInfo: 'MobileUI.components.editablelist.items.MEntityInfoEListItem',
        imagesListItem: 'MobileUI.components.editablelist.items.MImagesEListItem',
        referenceAttribute: 'MobileUI.components.editablelist.items.MReferenceAttributeEListItem'
    },

    statics: {
        types: {
            attribute: 'attribute',
            header: 'header',
            addButton: 'addButton',
            selectable: 'selectable',
            entityInfo: 'entityInfo',
            referenceAttribute: 'referenceAttribute',
            imagesAddButton: 'imagesAddButton',
            imageItem: 'imageItem'
        },
        defaultAction: {
            action: 'selectItem'
        }
    },

    initialize: function () {
        this.callParent(arguments);
        this.addCls('x-list-item');
        this.element.on('tap', this.onItemTap, this);
    },
    createSeparator: function (inSubGroup) {
        return '<div class="rl-item-separator' + (inSubGroup ? ' subgroup' : '') + '"></div>';
    },
    updateRecord: function (record) {
        if (this.built) {
            return;
        }
        this.removeAll();
        this.handlers = {};
        this.built = true;

        if (record) {
            var type = record.get('type') || 'do';
            var fn = type + 'Layout';

            var html = this[fn](record);

            if (this.sideMenu()) {
                var pos = html.lastIndexOf('</div>');
                html = html.substr(0, pos) + '<div class="rl-edit-button menu-icon handler-sideMenu"><div></div></div></div>';
                this.registerHandler('handler-sideMenu', function (event) {
                    var item = event.item;
                    var popup = MobileUI.components.editablelist.inline.SideMenu.createMenu(item.sideMenu(), record.get('disableButtons'));
                    popup.on('action', function (action) {
                        item.dataview.fireEvent(action, event.record);
                        popup.hide();
                    });
                    popup.showBy(event.element, 'cr-cl?');
                }, true);
            }

            if (!this.hideSeparator) {
                html += this.createSeparator(this.isSubGroup);
            }
            this.setHtml(html);

            fn = type + 'AfterLayout';
            if (this[fn]) {
                this[fn]();
            }
        }
        else {
            this.removeAll();
        }

        this.callParent(arguments);
    },

    recreate: function () {
        this.removeAll();
        this.built = false;
        this.updateRecord(this.getRecord());
    },

    /** @private */
    built: false,

    /** @private */
    handlers: null,

    lastTapedElement: null,

    /** @protected */
    onItemTap: function (event, element) {
        if (this.element.down('.reltio-swipe-button')) {
            return true;
        }

        event.stopPropagation();
        this.lastTapedElement = new Ext.Element(event.target);
        var foundHandler;

        while (element && !foundHandler) {
            for (var handler in this.handlers) {
                if (this.handlers.hasOwnProperty(handler)) {
                    if (element.classList.contains(handler)) {
                        foundHandler = this.handlers[handler];
                        break;
                    }
                }
            }
            if (element === this.element.dom) {
                break;
            }
            element = element.parentElement;
        }

        if (!foundHandler) {
            foundHandler = MobileUI.components.editablelist.items.ListItem.defaultAction;
        }

        var eventData = {
            record: this.getRecord(),
            element: new Ext.Element(event.target),
            item: this
        };

        if (Ext.isFunction(foundHandler.action)) {
            foundHandler.action(eventData);
        }
        else {
            this.dataview.fireEvent(foundHandler.action, eventData);
        }
    },

    registerHandler: function (elementClass, action) {
        this.handlers[elementClass] = {action: action};
    },


    sideMenu: function () {
        if (this.getRecord().get('disableSwipe')) {
            return false;
        }

        var plugins = this.dataview.getPlugins();
        var plugin;
        if (plugins) {
            for (var i = 0; i < plugins.length; i++) {
                if (plugins[i].getFilterButtons && plugins[i].getSwipeRightButtons) {
                    plugin = plugins[i];
                    break;
                }
            }
        }

        var buttons;
        if (plugin && plugin.getUseMenu()) {
            var filterButtonFn = plugin.getFilterButtons();
            var record = this.getRecord();
            buttons = plugin.getSwipeRightButtons().filter(function (button) {
                return !filterButtonFn || filterButtonFn(button.id, record);
            });
        }

        return (buttons && buttons.length > 0) ? buttons : null;
    },

    /**
     * Method converts \n in the text
     * @param text {String} text
     * @param canBeNull {Boolean?} if the me
     */
    convertText: function (text) {
        text = Ext.util.Format.htmlEncode(text);
        if (Ext.isString(text)) {
            text = text.replace(/\n/g, '<br/>').replace(/&amp;ndash;/g, '&ndash;');
        }
        return text;
    },

    getMultiline: function (record) {
        var multiline = false;

        var message = record.get('message'),
            error = record.get('error');

        if (message || error) {
            var keepErrorMessage;

            if (error) {
                keepErrorMessage =
                    error.invalidType !== MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED ||
                    (error.attrType && record.get('attrType') && error.attrType.uri !== record.get('attrType').uri);
            }

            multiline = {
                message: message,
                error: keepErrorMessage ? error.value : null
            };
        }

        return multiline;
    }
});
