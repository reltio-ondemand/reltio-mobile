/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MAddButtonEListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right rl-buttons-item text-at-left add-attribute-button">' +
            '   <div class="rl-button handler-button rl-align-center rl-hbox">' +
            '       <img class="rl-fixed-size rl-align-center-self" src="resources/images/reltio/relations/add.svg"/>' +
            '       <div class="rl-fixed-size rl-align-center-self rl-button-label">{label}</div>' +
            '   </div>' +
            '</div>'),

        fields: ['lastInGroup', 'attrTypes']
    },

    /**
     * @protected
     * Method returns the header layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    addButtonLayout: function (record) {
        this.hideSeparator = record.get('lastInGroup');

        this.registerHandler('handler-button', function(data) {
            data.item.dataview.fireEvent('addAttribute', data);
        }, true);

        return MobileUI.components.editablelist.items.MAddButtonEListItem.template.apply({
            label: i18n('Add')
        });
    }
});
