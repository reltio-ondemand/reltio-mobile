/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MAttributeEListItem', {
    requires: [
        'MobileUI.components.editablelist.ControlsFactory'
    ],

    statics: {
        templates: {
            t: new Ext.XTemplate(
                '<div class="rl-item stick-right rl-property-item handler-root {actionCls} {invalid}" style="{offset}">' +
                '   <tpl if="multiline">' +
                '      <div class="rl-property-multiline-container">' +
                '           <div class="rl-caption rl-collapsible rl-property-label {required}">{attributeLabel}</div>' +
                '           <div class="rl-cardinality rl-collapsible" ">{multiline.message}</div>' +

                '      </div>' +
                '   <tpl else>' +
                '      <div class="rl-caption rl-collapsible rl-property-label {required}">{attributeLabel}</div>' +
                '   </tpl>' +
                '   <tpl if="multiline">' +
                '       <div class="rl-multiline rl-align-center-self">' +
                '           <div class="rl-text  handler-editor {asInline} {placeHolderCls}">{label}</div>' +
                '           <div class="rl-error-text">{multiline.error}</div>' +
                '      </div>' +
                '   <tpl else>' +
                '       <div class="rl-text rl-align-center-self handler-editor {asInline} {placeHolderCls}">{label}</div>' +
                '   </tpl>' +
                '   <tpl if="editable">' +
                '       <div class="rl-edit-button {editIcon} handler-edit">' +
                '           <div></div>' +
                '       </div>' +
                '   </tpl>' +
                '</div>'
            )
        },

        fields: ['attrType', 'attrValue', 'lastInGroup', 'error', 'values', 'message', 'disabled','editable','disableSwipe']
    },

    isInline: function (attrType) {
        return !(attrType.type === 'Nested' || attrType.type === 'Reference' || attrType.dependentLookupCode || attrType.type === 'Image');
    },

    /**
     * @protected
     * Method returns attribute layout
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    attributeLayout: function (record) {
        var template = MobileUI.components.editablelist.items.MAttributeEListItem.templates.t;

        this.hideSeparator = record.get('lastInGroup');

        var attrType = record.get('attrType');
        var error = record.get('error');
        var placeholderCls = record.get('disabled') ? 'reltio-disabled-placeholder': '';
        if (!placeholderCls && !record.get('label') && error) {
            placeholderCls = 'rl-error';
        }

        var image;
        if (attrType.type === 'Image') {
            var attrValue = record.get('attrValue');
            if (attrValue.value.CdnUrlPreview) {
                var uri = attrValue.value.CdnUrlPreview[0].value;
                var img = new Image();
                img.onload = function() {
                    this.dataview.fireEvent('recalculate', this.dataview);
                }.bind(this);
                img.src = uri;

                image = '<img src="' + uri + '" style="max-width: 100px; max-height: 80px"/>';
            }
        }

        return template.apply({
            attributeLabel: Ext.util.Format.htmlEncode(attrType.label),
            label: image || Ext.util.Format.htmlEncode(record.get('label') || error && error.value),
            required: record.get('required') && 'required',
            editable: !this.isInline(attrType) && !image,
            invalid: error ? 'invalid' : null,
            asInline: !this.isInline(attrType) ? 'rl-as-inline-new' : '',
            multiline: this.getMultiline(record),
            placeHolderCls: placeholderCls
        });
    },

    attributeAfterLayout: function () {
        var me = this;
        var record = this.getRecord();
        var attrType = record.get('attrType');

        if (!this.isInline(attrType)) {
            return;
        }

        var attrValue = record.get('attrValue'),
            value = attrValue.value;

        var adjustEditor = Ext.emptyFn;
        var type = attrType.type;

        if (type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.BOOLEAN) {
            value = (value === 'true') ? true : ((value === 'false') ? false : undefined);
        } else if (type === 'String' && attrType.values) {
            type = MobileUI.components.editablelist.ControlsFactory.attributeTypes.ENUM;

            adjustEditor = function (editor) {
                editor.set('options', attrType.values.map(function (value) {
                    return {text: i18n(value), value: value};
                }));
            };
        } else if (attrType.lookupCode) {
            type = MobileUI.components.editablelist.ControlsFactory.attributeTypes.LOOKUP;
            value = attrValue.lookupCode || value;
            adjustEditor = function (editor) {
                editor.set('options', record.get('values'));
            };
        }

        var editor = MobileUI.components.editablelist.ControlsFactory.createControl(type);
        if (record.get('editable') === false) {
            editor.setDisabled(true);
        }
        adjustEditor(editor);
        editor.set('value', value);
        editor.on('change', function (editor, value) {
            if (attrType.type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.BOOLEAN) {
                value = editor; //!!!!
                if (value === null) {
                    value = '';
                } else {
                    value = value.toString();
                }
            } else if (attrType.type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.TIMESTAMP && value) {
                editor.refresh(value);
            } else if (attrType.lookupCode) {
                MobileUI.core.session.Session.getLookupsManager().touchEditor(attrValue.uri);
            } else if (attrType.type === 'URL' || attrType.type === 'Blog URL' || attrType.type === 'Image URL') {
                if (value.toLowerCase().indexOf('www.') === 0) {
                    editor.set('value', 'http://' + value);
                    return;
                }
            }

            me.dataview.fireEvent('changeValue', {
                uri: attrValue.uri,
                attrType: attrType,
                value: value
            });
        });
        editor.on('changeHeight', function (delta) {
            me.dataview.setHeight(me.dataview.getHeight() + delta);
        });

        var element = this.element.down('.handler-editor');
        element.setHtml('');
        element = element.dom || element;
        editor.setRenderTo(element);

        var error = record.get('error');

        if (error && Ext.isEmpty(value)) {
            if (editor && error.invalidType === MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED) {
                var cover = Ext.create('Ext.Panel', {
                    cls: 'rl-required-cover',
                    html: error.value
                });
                cover.setRenderTo(element);
                element.style['minHeight'] = '25px';
                if (editor) {
                    cover.element.dom.addEventListener('click', function () {
                        cover.setHidden(true);
                        if (editor.focus) {
                            editor.focus();
                        }
                    });
                }
            }
        }
    }
});
