/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MEntityInfoEListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right handler-root">' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-title">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '       <tpl if="multiline">' +
            '           <div class="rl-error-text">{multiline.error}</div>' +
            '           <div class="rl-cardinality">{multiline.cardinality}</div>' +
            '       </tpl>' +
            '   </div>' +
            '</div>'),

        fields: ['label']
    },

    /**
     * @protected
     * Method returns the entity info layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    entityInfoLayout: function (record) {
        return MobileUI.components.editablelist.items.MEntityInfoEListItem.template.apply({
            label: this.convertText(record.get('label'))
        });
    }
});
