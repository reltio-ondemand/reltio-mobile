/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MHeaderEListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item rl-header">' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-hbox">' +
            '           <div class="{required}">{label}</div>' +
            '       </div>' +
            '       <tpl if="multiline">' +
            '           <div class="rl-error-text">{multiline.error}</div>' +
            '           <div class="rl-cardinality">{multiline.message}</div>' +
            '       </tpl>' +
            '   </div>' +
            '</div>'),

        fields: ['label', 'required', 'message', 'error']
    },

    /**
     * @protected
     * Method returns the header layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    headerLayout: function (record) {
        this.hideSeparator = true;

        return MobileUI.components.editablelist.items.MHeaderEListItem.template.apply({
            label: i18n(record.get('label')),
            required: record.get('required') && 'required',
            multiline: this.getMultiline(record)
        });
    }
});
