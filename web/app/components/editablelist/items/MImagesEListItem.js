/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MImagesEListItem', {

    statics: {
        addButtonTemplate:
            new Ext.XTemplate(
                '<div class="rl-item stick-right rl-buttons-item text-at-left add-attribute-button">' +
                '   <div class="rl-button handler-button rl-align-center rl-hbox">' +
                '       <img class="rl-fixed-size rl-align-center-self" src="resources/images/reltio/relations/add.svg"/>' +
                '       <div class="rl-fixed-size rl-align-center-self rl-button-label">{label}</div>' +
                '   </div>' +
                '</div>'),

        imageTemplate: new Ext.XTemplate(
            '<div class="rl-item rl-relations-item handler-root">' +
            '   <tpl if="avatar != null">' +
            '       <div class="rl-big-avatar {state}">' +
            '           <div class = "rl-avatar-img" style="background-image: url({avatar})"></div>' +
            '           <tpl if="state">' +
            '               <div class="rl-selected-image" style="background-image: url(resources/images/reltio/profile/selectedImage.svg)"></div>' +
            '           </tpl>' +
            '       </div>' +
            '   </tpl>' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-title">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '       <div class="rl-subtext rl-collapsible">{thirdLabel}</div>' +
            '   </div>' +
            '</div>'),

        fields: ['avatar', 'state', 'label', 'secondaryLabel', 'thirdLabel', 'uri']
    },

    /**
     * @protected
     * Method returns the header layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    imagesAddButtonLayout: function (record) {
        this.hideSeparator = record.get('lastInGroup');

        this.registerHandler('handler-button', function (data) {
            data.item.dataview.fireEvent('addAttribute', data);
        }, true);

        return MobileUI.components.editablelist.items.MImagesEListItem.addButtonTemplate.apply({
            label: i18n('Add Image')
        });
    },

    imageItemLayout: function (record) {
        this.hideSeparator = record.get('lastInGroup');

        this.registerHandler('handler-button', function (data) {
            data.item.dataview.fireEvent('selectItem', data);
        }, true);

        var template = MobileUI.components.editablelist.items.MImagesEListItem.imageTemplate;

        return template.apply({
            avatar: record.get('avatar'),
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            thirdLabel: this.convertText(record.get('thirdLabel')),
            state: record.get('state')
        });
    }

});
