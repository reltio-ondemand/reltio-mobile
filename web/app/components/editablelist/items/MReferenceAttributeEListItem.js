/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MReferenceAttributeEListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right handler-root">' +
            '   <tpl if="image != null">' +
            '       <div class="rl-avatar">' +
            '           <div style="background-image: url({image})"></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-title">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '       <div class="rl-tag-list">' +
            '           <div class="rl-tag" style="background-color:{typeBadge.color}; color: {typeBadge.textColor || \'\'}">{typeBadge.value}</div>' +
            '       </div>' +
            '       <tpl if="multiline">' +
            '           <div class="rl-error-text">{multiline.error}</div>' +
            '           <div class="rl-cardinality">{multiline.message}</div>' +
            '       </tpl>' +
            '   </div>' +
            '   <div class="rl-edit-button">' +
            '       <div></div>' +
            '   </div>' +
            '</div>'),

        fields: ['label', 'secondaryLabel', 'typeBadge', 'image','editable','disableSwipe']
    },

    /**
     * @protected
     * Method returns the reference attribute layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    referenceAttributeLayout: function (record) {
        this.hideSeparator = record.get('lastInGroup');

        return MobileUI.components.editablelist.items.MReferenceAttributeEListItem.template.apply({
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            typeBadge: record.get('typeBadge'),
            image: record.get('image'),
            multiline: this.getMultiline(record)
        });
    }
});
