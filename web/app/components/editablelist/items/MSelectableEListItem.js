/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.editablelist.items.MSelectableEListItem', {
    requires: [
        'MobileUI.components.ListItemEditor'
    ],

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item {baseCls}">' +
            '   <div class="rl-score rl-align-center-self {required}">{label}</div>' +
            '   <tpl if="score || score === 0">' +
            '       <div class="rl-vbox rl-align-center rl-fixed-size">' +
            '           <div class="rl-score">{score}</div>' +
            '       </div>' +
            '   </tpl>' +
            '   <tpl if="selected">' +
            '       <div class="rl-vbox rl-align-center rl-fixed-size rl-checkmark">' +
            '       </div>' +
            '   </tpl>' +
            '</div>'),

        fields: ['label', 'score', 'selected', 'required', 'baseCls']
    },

    /**
     * @protected
     * Method returns layout
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    selectableLayout: function (record) {
        return MobileUI.components.editablelist.items.MSelectableEListItem.template.apply({
            label: i18n(this.convertText(record.get('label'))),
            score: i18n(record.get('score')),
            selected: record.get('selected'),
            required: record.get('required') ? 'required' : '',
            baseCls: record.get('baseCls') || ''
        });
    }
});
