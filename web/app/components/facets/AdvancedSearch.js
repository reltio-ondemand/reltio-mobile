/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.AdvancedSearch', {
    requires: ['MobileUI.components.facets.advanced.MetadataManager',
        'MobileUI.components.facets.AdvancedSearchFacet'],
    mixins: {observable:'Ext.mixin.Observable'},
    statics: {
        isSearchable: function (attrType) {
            var md = MobileUI.core.Metadata,
                eu = MobileUI.core.entity.EntityUtils;
            if (eu.isComplex(attrType)) {
                var attributes = md.getSubAttributes(attrType);
                return attributes.some(MobileUI.components.facets.AdvancedSearch.isSearchable);
            }
            else {
                return Boolean(attrType.searchable) === true;
            }
        },
        type: {
            TYPEAHEAD: 'Typeahead',
            STRING: 'String',
            DATE: 'Date',
            NUMBER: 'Number',
            TIMESTAMP: 'Timestamp',
            LOOKUP: 'Lookup',
            DEPENDENT_LOOKUP: 'DependentLookup',
            BOOLEAN: 'Boolean',
            ENUM: 'Enum',
            BLOB: 'Blob',
            NESTED: 'Nested',
            REFERENCE: 'Reference'
        }
    },

    getMetadataManager: function(){
        if (!this.metadataManager){
            this.metadataManager = Ext.create('MobileUI.components.facets.advanced.MetadataManager');
        }
        return this.metadataManager;
    },
    prepareEntityTypeUri: function(uri)
    {
        if (Ext.isArray(uri))
        {
            return uri[0];
        }
        return uri;
    },
    onEntityTypeChanged: function(uri, force){
        this.oldEntityTypes = this.entityUri;
        if (this.oldEntityTypes !== uri || force){
            this.entityType = uri;
            this.onModelUpdated();
        }
    },
    clearListeners: function(container){
        if (container){
            container.un('valueChanged', this.onAdvancedFilterChanged, this);
        }
    },
    checkCollapsedState: function(attributes, state){
        return Ext.isEmpty(attributes.filter(function(attribute){
           return state.some(function(item){
              return item.uri.indexOf(attribute.uri) === 0;
           });
        }));
    },

    onModelUpdated: function(){
        var searchContainer = Ext.ComponentQuery.query('container[name=searchfacetview-advanced]');
        if (!Ext.isEmpty(searchContainer)){
            this.suppressValueChanged = true;
            if (Ext.isEmpty(this.state)){
                this.state = Ext.clone(MobileUI.core.search.SearchParametersManager.getAdvancedSearchParameters());
            }
            var state = this.state.filter(function(savedItem){
                return savedItem.filter === 'equals'|| savedItem.filter ==='startsWith';
            });
            searchContainer = searchContainer[0];
            searchContainer.removeAll();
            var info = this.metadataManager.getSimpleFields(this.entityType);
            var searchable = false;
            for (var i = 0; i < info.length && !searchable; i++) {
                searchable = !info[i].nonSearchable;
            }
            if (searchable) {
                this.clearListeners(this.mainContainer);
                this.mainContainer = Ext.create('MobileUI.components.facets.AdvancedSearchFacet');
                searchContainer.add(this.mainContainer);
                searchContainer.add({
                    xtype: 'component',
                    html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
                });
                this.mainContainer.setParameters({title: i18n('Main information'), collapsed: this.checkCollapsedState(info, state), parent:'attributes'});
                this.mainContainer.refreshFacet(info, state);
                this.mainContainer.on('valueChanged', this.onAdvancedFilterChanged, this);
            }
            (this.complexContainers || []).forEach(function(item){
                this.clearListeners(item);
            },this);
            this.complexContainers = this.metadataManager.getComplexFields(this.entityType, true)
                .filter(function(item) {
                    return item.children && item.children.some(function(child) { return !child.nonSearchable; });
                })
                .map(function(item) {
                    var container = Ext.create('MobileUI.components.facets.AdvancedSearchFacet');
                    searchContainer.add(container);
                    searchContainer.add({
                        xtype: 'component',
                        html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
                    });
                    var parent = 'attributes';
                    if (item.name) {
                        parent += '.' + item.name;
                    }
                    container.setParameters({
                        title: item.caption,
                        collapsed: this.checkCollapsedState(item.children, state),
                        parent: parent
                    });
                    container.refreshFacet(item.children, state);
                    container.on('valueChanged', this.onAdvancedFilterChanged, this);
                    return container;
                }, this);
            /*this.clearListeners(this.sourceContainer);
            this.sourceContainer = Ext.create('MobileUI.components.facets.AdvancedSearchFacet');
            searchContainer.add(this.sourceContainer);
            searchContainer.add({
                xtype: 'component',
                html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
            });
            this.sourceContainer.setParameters({title: i18n('Sources'), collapsed: this.checkCollapsedState(this.metadataManager.getSourceFields(), this.state)});
            this.sourceContainer.refreshFacet(this.metadataManager.getSourceFields(), state);
            this.sourceContainer.on('valueChanged', this.onAdvancedFilterChanged, this);

            this.clearListeners(this.auditContainer);
            this.auditContainer = Ext.create('MobileUI.components.facets.AdvancedSearchFacet');
            searchContainer.add(this.auditContainer);
            searchContainer.add({
                xtype: 'component',
                html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
            });
            this.auditContainer.setParameters({title: i18n('Audit'), collapsed: this.checkCollapsedState(this.metadataManager.getAuditFields(), this.state)});
            this.auditContainer.refreshFacet(this.metadataManager.getAuditFields(), state);
            this.auditContainer.on('valueChanged', this.onAdvancedFilterChanged, this);

            this.clearListeners(this.collabarationContainer);
            this.collabarationContainer = Ext.create('MobileUI.components.facets.AdvancedSearchFacet');
            searchContainer.add(this.collabarationContainer);
            this.collabarationContainer.setParameters({title: i18n('Collaboration'), collapsed: this.checkCollapsedState(this.metadataManager.getCollaborationFields(), this.state)});
            this.collabarationContainer.refreshFacet(this.metadataManager.getCollaborationFields(), state);
            this.collabarationContainer.on('valueChanged', this.onAdvancedFilterChanged, this);
            */
            this.state = [];
            this.suppressValueChanged = false;
            this.onAdvancedFilterChanged();
        }
    },

    onAdvancedFilterChanged: function(){
        if (!this.suppressValueChanged) {
            var state =   this.complexContainers.concat(this.mainContainer /*, this.auditContainer, this.collabarationContainer, this.sourceContainer*/);
            var parameters = [];
            state.forEach(function(item){
                if (item){
                    parameters.push.apply(parameters, item.getSearchState());
                }
            });
            this.searchParameters = parameters;
            this.fireEvent('valueChanged');
        }
    },
    restoreState: function(uri){
        this.onEntityTypeChanged(uri, true);
    },

    resetParameters: function(){
        this.searchParameters = [];
    },

    getParameters: function(){
       return this.searchParameters || [];
    }

});
