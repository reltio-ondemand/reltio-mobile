/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.AdvancedSearchFacet', {
    extend: 'MobileUI.components.EditableList',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    mixins: {observable:'Ext.mixin.Observable', listhelper: 'MobileUI.components.list.MListHeight', listModelHelper:MobileUI.components.list.MListModel},
    config: {
        scrollable: false,
        plugins: [
            {type: 'swipetodelete', useMenu: true }
        ],
        height: 0,
        cls: 'simple-facet elist advanced-facet',
        store: {
            data: [],
            model: 'MobileUI.components.list.model.EListModel'
        },
        fieldName: null,
        listeners:{
            painted: function(){
                if (!this.painted){
                    this.refreshFacet();
                    this.painted = true;
                }
            },
            expand: function()
            {
                this.onExpanded();
            },
            modified: function(item){
                this.onItemUpdated(item.get('label'), item.get('editor'));
            },
            collapse: function()
            {
                this.onCollapsed();
            },
            'delete': function(item)
            {
                this.onItemDelete(item);
            }
        }
    },
    parameters: null,
    state: null,
    initialize: function(){
        this.callParent(arguments);
        this.state = {};
    },

    resetFacet: function()
    {
        this.state = {};
        this.parameters = null;
    },
    onExpanded: function()
    {
        this.parameters.collapsed = false;
        this.refreshFacet();
    },
    onCollapsed: function()
    {
        this.parameters.collapsed = true;
        this.savedState = this.searchState;
        this.refreshFacet();
    },

    getParameters: function()
    {
        return this.parameters;
    },

    refreshFacet: function(facetsJson, savedState)
    {
        var parameters, model, store, facetsDefinition;
        store = this.getStore();
        var me = this;
        if (!Ext.isEmpty(savedState)){
            this.savedState = savedState;
        }
        if (Ext.isObject(this.parameters))
        {
            parameters = this.parameters;
            if (!facetsJson)
            {
                facetsDefinition = this.state;
            } else
            {
                facetsDefinition = facetsJson;
                this.state = facetsDefinition;
            }
            model = [];
            model.push(this.createHeader(parameters.title, parameters.collapsed));
            if (!parameters.collapsed) {
                model.push.apply(model, this.generateModel(facetsDefinition, parameters.parent, 1, this.savedState || []));
            }
        }
        var translated = model
            .filter(function (item) {
                return item.editor && item.editor.dependentLookupCode;
            })
            .filter(function(item){
                return item.value;
            });
        var flow;
        if (Ext.isEmpty(translated)){
            flow = Promise.resolve();
        } else {
            var values = translated.map(function (item) {
                return {
                    type: item.editor.dependentLookupCode,
                    codeValue: item.value
                };
            });
            flow = MobileUI.core.session.Session.resolveLookupsValue(values).then(function(result){
                translated.forEach(function(modelItem){
                    var resultItem = result.filter(function(record){
                        var lookupValues = record[modelItem.editor.dependentLookupCode];
                        return lookupValues && lookupValues[modelItem.value];
                    });
                    if (!Ext.isEmpty(resultItem)){
                        var displayName = resultItem[0][modelItem.editor.dependentLookupCode][modelItem.value].displayName;
                        var label = displayName || modelItem.value;
                        if (label !== modelItem.value) {
                            label = label + ' (' + modelItem.value + ')';
                        }
                        modelItem.localizeLabel = label;
                    }
                });
            });

        }
        /*jshint es5: true */
        flow.then(function () {
            store.clearData();
            store.add(model);
            store.sync();
            me.refresh();
            me.updateListHeight(me);
            me.updateState();
            me.fireEvent('valueChanged');
        }).catch(function(e){
            MobileUI.core.Logger.info(e);
        });


    },
    generateModel: function(facetsDefinition, parent, offset, savedState){
        var key, item, model = [], record;
        offset = offset || 1;
        parent = Ext.isEmpty(parent) ? '': parent + '.';
        /*jshint loopfunc: true */
        for (key in facetsDefinition)
        {
            if (facetsDefinition.hasOwnProperty(key))
            {
                item = facetsDefinition[key];
                var name = parent + item.name;
                if (item.nonSearchable){
                    continue;
                }
                var saved = savedState.filter(function(savedItem){
                    return savedItem.uri === item.uri;
                }).map(function(item){
                    return {
                        value: Ext.isEmpty(item.values) ? '' : item.values[0],
                        label: Ext.isEmpty(item.labels) ? '' : item.labels[0]
                    };
                });
                var value = null, label = null;
                if (!Ext.isEmpty(saved)){
                    value = saved[0].value;
                    label = saved[0].label || saved[0].value;
                }

                if (!MobileUI.core.entity.EntityUtils.isComplex(item.attributeType)) {
                    record = {
                        actionLabel: false,
                        disableSwipe: !Ext.isEmpty(item.children),
                        editor: {
                            label: item.caption,
                            type: item.attributeType,
                            uri: item.uri,
                            name: name
                        },
                        info: false,
                        label: label || '',
                        offset: offset,
                        value: value,
                        filter: item.filter,
                        property: item.caption,
                        lastInGroup: false,
                        type: MobileUI.components.list.items.BaseListItem.types.property
                    };

                    if (item.faceted) {
                        record.editor.type = 'Faceted';
                    }
                    if (item.lookupCode) {
                        record.editor.lookupCode = item.lookupCode;
                        record.editor.type = 'String';
                    } else if (item.dependentLookupCode) {
                        record.editor.dependentLookupCode = item.dependentLookupCode;
                        record.editor.type = 'String';
                    }
                    if (!Ext.isEmpty(item.enumList)) {
                        record.editor.values = item.enumList;
                        record.editor.type = 'String';
                    }
                    model.push(record);
                }else {
                    record = {
                        actionLabel: false,
                        disableSwipe: !Ext.isEmpty(item.children),
                        info: false,
                        label: '',
                        offset: offset,
                        property: item.caption,
                        lastInGroup: false,
                        type: MobileUI.components.list.items.BaseListItem.types.property,
                        userData: item
                    };
                    model.push(record);
                    model.push.apply(model, this.generateModel(item.children, name, offset + 1, savedState));
                }

                if (record.editor) {
                    record.editor.valueChangedCallback = this.onItemChanged;
                    record.editor.valueChangedContext = this;
                }
                if (value) {
                    this.onItemUpdated(value, record.editor);
                }
            }
        }
        /*jshint loopfunc: false */
        return model;

    },
    onItemDelete: function(item)
    {
        var record = item.get('id'),
            store = this.getStore(),
            index = store.findExact('id', record);
        if (index !== -1)
        {
            record = store.getAt(index);
            record.set('label', '');
            store.sync();
            this.refresh();
            this.onItemUpdated(null, record.get('editor'));
        }
        this.updateListHeight(this);
    },
    findItemByUri: function(uri, state)
    {
        var result, item;
        for (var i = 0; i < state.length; i++) {
            item = state[i];
            if (!Ext.isEmpty(item.children)) {
                result = this.findItemByUri(uri, item.children);
                if (result) {
                    break;
                }
            } else if (item.uri === uri) {
                result = item;
                break;
            }
        }
        return result;
    },

    onItemChanged: function(value, item) {
        this.onItemUpdated(value, item.getRecord().get('editor'));
    },

    onItemUpdated: function(value, editor) {
        var uri = !Ext.isEmpty(editor) && editor.uri;
        var result = this.findItemByUri(uri, this.state);
        if(!Ext.isEmpty(result)){
            result.value = value;
            result.filter = 'startsWith';
            this.updateState();
            this.fireEvent('valueChanged');
        }
    },

    createHeader: function(label, collapsed){
        var obj = {type:MobileUI.components.list.items.BaseListItem.types.facetHeader,
            label: label,
            disableSwipe: true};
        if (collapsed)
        {
            obj.state = 'collapsed';
            obj.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
        } else
        {
            obj.state = 'expanded';
            obj.stateIcon = 'resources/images/reltio/tree/expanded.svg';
        }
        return obj;
    },

    setParameters: function(parameters) {
        this.parameters = parameters;
        this.refreshFacet();
    },
    updateState: function(){
        if (!this.parameters.collapsed)
        {
            this.searchState = [];
            this.getStore().each(function(record){
                var editor = record.get('editor');

                if(!Ext.isEmpty(editor)){
                    var result = this.findItemByUri(editor.uri, this.state);
                    if (!Ext.isEmpty(result) && !Ext.isEmpty(result.value)) {
                        var item = {
                            fieldName: editor.name,
                            filter: result.filter,
                            labels: [record.get('label') || result.value],
                            title: editor.label,
                            uri: editor.uri,
                            values: [result.value]
                        };
                        this.searchState.push(item);
                    }
                }
            }, this);
        }
    },
    getSearchState: function(){
      return this.searchState || [];
    },

    getState: function() {
        return this.state;
    }
});
