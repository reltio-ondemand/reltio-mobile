/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.EntityTypeSearchFacet', {
    extend: 'MobileUI.components.facets.ToggleButtonFacet',
    config: {
        store: {
            data: [],
            model: 'MobileUI.components.list.model.EListModel'
        }
    },
    statics: {
        FIELD_NAME: 'type',
        getTypesFacetFromJson: function(facetsJson)
        {
            var entityTypes = MobileUI.core.Metadata.getEntityTypes();
            var map = {}, uri, name, temp;
            for(uri in entityTypes)
            {
                if (entityTypes.hasOwnProperty(uri))
                {
                    if (MobileUI.core.PermissionManager.securityService().metadata.canRead(entityTypes[uri])) {
                        name = uri.substring(uri.lastIndexOf('/') + 1).toLowerCase();
                        map[name] = uri;
                    }
                }
            }

            temp = facetsJson[this.FIELD_NAME] || {};
            facetsJson = {};
            for (name in temp)
            {
                if (temp.hasOwnProperty(name)){
                    var key = name.toLowerCase();
                    if (map[key]) {
                        facetsJson[map[key]] = temp[name];
                    }
                }
            }
            return facetsJson;
        },

        fromValues: function(values){
            var items = MobileUI.core.util.Util.filter(MobileUI.core.Metadata.getEntityTypes(), function (item) {
                return item.searchable !== false && !item['abstract'];
            });
            var types = Object.keys(items);
            var state = {};
            types.forEach(function(item){
                var name = item.substring(item.lastIndexOf('/') + 1),
                    on = false;
                if(values.indexOf(item) !== -1)
                {
                   on = true;
                }
                state[item] = {label: name, on: on, model: item};
            });
            return state;
        }

    },
    valid: true,
    initialize: function () {
        this.callParent(arguments);
    },

    getActiveParams: function(){
        var searchableEntityTypes = [],
            labels = [];
        for(var key in this.state)
        {
            if (this.state.hasOwnProperty(key))
            {
                if (this.state[key].on)
                {
                    searchableEntityTypes.push(key);
                    labels.push(this.state[key].label);
                }
            }
        }
        return {labels: labels, values: searchableEntityTypes};
    },
    getSearchParams: function()
    {
        var params = this.getActiveParams();
        return {
            fieldName: MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME,
            title: i18n('Entity types'),
            filter: 'equals',
            labels: params.labels,
            values: params.values,
            notForState: true
        };
    },
    refreshFacet: function(facetsDefinition){
        this.suppressEvents = true;
        if (facetsDefinition)
        {
            this.state = null;
        }
        if (!this.state) {
            var fakeJson = {};
            var items = MobileUI.core.util.Util.filter(MobileUI.core.Metadata.getEntityTypes(), function (item) {
                return item.searchable !== false && !item['abstract'];
            });
            var entityTypes = Object.keys(items);

            if (!facetsDefinition)
            {
                facetsDefinition = {};
            }
            var state = facetsDefinition[MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME] || facetsDefinition || {};
            entityTypes.forEach(function (uri) {
                var name = uri.substring(uri.lastIndexOf('/') + 1);
                var label = (items[uri] && items[uri].label) ? items[uri].label : name;
                this[name] = {label: label, on: state[uri] ? state[uri].on : true, model: uri};
            }, fakeJson);
            facetsDefinition[MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME] = fakeJson;
            this.state = MobileUI.components.facets.EntityTypeSearchFacet.getTypesFacetFromJson(facetsDefinition);
        }
        this.callParent([this.state]);
        this.suppressEvents = false;
        this.fireEvent('entityChanged', this.getSelectedEntityType());
        this.fireEvent('valueChanged', this.state);
    },
    selectAllItems: function(){
        this.suppressEvents = true;
        Ext.Object.each(this.state, function (key, value) {
            this.onFacetSelected({value: true}, value.label, key);
        }, this);
        this.refreshStore();
        this.suppressEvents = false;
        this.fireEvent('valueChanged', this.state);
        this.fireEvent('entityChanged', this.getSelectedEntityType());
    },

    deselectAllItems: function () {
        this.suppressEvents = true;
        Ext.Object.each(this.state, function (key, value) {
            this.onFacetSelected({value: false}, value.label, key);
        }, this);
        this.refreshStore();
        this.suppressEvents = false;
        this.fireEvent('valueChanged', this.state);
        this.fireEvent('entityChanged', this.getSelectedEntityType());
    },
    onFacetSelected: function(item, label, model)
    {
        this.callParent(arguments);
        var valid;
        var value = item.getValue? item.getValue(): item.value;
        this.state[model].on = Boolean(value);
        var allOn = MobileUI.core.util.Util.filter(this.state, function(item){
            return item.on;
        });
        var off = MobileUI.core.util.Util.filter(this.state, function(item){
            return !item.on;
        });
        if (Object.keys(allOn).length === 0) {
            valid = false;
            this.setErrorMessage(i18n('Please select at least one entity type'));
            this.setState('select');
        } else {
            valid = true;
            this.setErrorMessage(null);
            this.setState(Object.keys(off).length === 0 ? 'deselect' : 'all');
        }
        if (this.valid !== valid)
        {
            this.valid = valid;
            this.updateListHeight(this);
        }
        if (!this.suppressEvents) {
            this.fireEvent('entityChanged', this.getSelectedEntityType());
            this.fireEvent('valueChanged', this.state);
        }
    },

    isValid: function()
    {
        return this.valid;
    },

    getSelectedEntityType: function()
    {
        var entityTypes = this.getActiveParams().values,
            entityType, i;
        if(Ext.isEmpty(entityTypes))
        {
            return null;
        }

        if(entityTypes.length === 1)
        {
            return entityTypes[0];
        }

        var md = MobileUI.core.Metadata;
        var parentStacks = [];
        for(i = 0; i < entityTypes.length; i++)
        {
            entityType = entityTypes[i];
            parentStacks[i] = [entityType];
            var parentStack = parentStacks[i];
            while(md.getEntityType(parentStack[parentStack.length - 1]))
            {
                var parent = md.getEntityType(parentStack[parentStack.length - 1]);
                if (parent.extendsTypeURI && md.getEntityType(parent.extendsTypeURI)) {
                    parentStack.push(parent.extendsTypeURI);
                } else {
                    break;
                }
            }
        }

        for(i = 0; i < parentStacks[0].length; i++)
        {
            entityType = parentStacks[0][i];
            var contains = true;
            for(var j = 1; j < parentStacks.length && contains; j++) {
                contains = contains && Ext.Array.contains(parentStacks[j], entityType);
            }
            if(contains)
            {
                return entityType;
            }
        }

        return null;
    },

    getState: function () {
        var parameters = this.getParameters();
        var activeParams = this.getActiveParams();
        return {
            labels: activeParams.labels,
            values: activeParams.values,
            collapsed: parameters.collapsed,
            fieldName: parameters.fieldName,
            filter: 'equals',
            title: parameters.title,
            state: this.state
        };
    },
    resetFacet: function()
    {
        this.callParent(arguments);
        this.state = null;
    }
});
