/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.FavoriteFacet', {
    extend: 'MobileUI.components.facets.ToggleButtonFacet',
    statics: {
        FIELD_NAME: 'favoriteFor'
    },
    config: {
        cls: 'elist favoritefacet'
    },
    state: null,
    initialize: function () {
        this.callParent(arguments);
        this.state = {};
    },
    refreshFacet: function(facetsDefinition)
    {
        if (facetsDefinition)
        {
            this.facetsDefinition = facetsDefinition;
        }
        var model=[], store;
        store = this.getStore();
        var isOn = this.facetsDefinition ? this.facetsDefinition.on : false;
        model.push(this.createItemModel('Search by Favorite profiles', null, isOn, this.onFacetSelected, this));
        store.clearData();
        store.add(model);
        store.sync();
        this.refresh();
        this.updateListHeight(this);
    },
    onFacetSelected: function(item)
    {
        this.state.on = Boolean(item.getValue());
        this.fireEvent('valueChanged', this.state);
    },

    getSearchParams: function()
    {
        var labels = this.state.on ? ['true']: null,
            values = this.state.on ? [MobileUI.core.session.Session.getUserName()]: null;
        return {fieldName: 'favoriteFor', title: 'Favorite',filter: 'equals', labels: labels, values: values, notForState: true};
    },

    setParameters: function(parameters)
    {
        this.parameters = parameters;
        this.refreshFacet();
    },
    getParameters: function()
    {
        return this.parameters;
    },
    getState: function () {
        var labels = this.state.on ? ['true']: null,
            values = this.state.on ? [MobileUI.core.session.Session.getUserName()]: null;
        return {
            labels: labels,
            values: values,
            title: 'Favorite',
            fieldName: MobileUI.components.facets.FavoriteFacet.FIELD_NAME,
            filter: 'equals',
            state: this.state
        };

    }
});