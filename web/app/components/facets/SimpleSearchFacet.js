/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.SimpleSearchFacet', {
    extend: 'MobileUI.components.EditableList',
    requires: ['MobileUI.plugins.SwipeToDelete', 'MobileUI.view.editors.TypeaheadEditor'],
    mixins: {observable:'Ext.mixin.Observable', listhelper: 'MobileUI.components.list.MListHeight', listModelHelper: MobileUI.components.list.MListModel},
    statics: {
        fromValues: function(values){

            var state = {};
            (values||[]).forEach(function(item){
                state[item] = item;
            });
            return state;
        }
    },
    config: {
        scrollable: false,
        plugins: [
            {type: 'swipetodelete', useMenu: true }
        ],
        height: 0,
        cls: 'simple-facet elist',
        store: {
            data: [],
            model: 'MobileUI.components.list.model.EListModel'
        },
        fieldName: null,
        listeners:{
            painted: function(){
                if (!this.painted){
                    this.refreshFacet();
                    this.painted = true;
                }
            },
            expand: function()
            {
                this.onExpanded();
            },
            collapse: function()
            {
                this.onCollapsed();
            },
            'delete': function(item)
            {
                this.onItemDelete(item);
            },
            selectItem: function(item)
            {
                this.onItemSelect(item);
            }
        }
    },
    parameters: null,
    state: null,
    initialize: function(){
        this.callParent(arguments);
        this.state = {};
    },

    resetFacet: function()
    {
        this.state = {};
        this.parameters = null;
    },
    onExpanded: function()
    {
        this.parameters.collapsed = false;
        this.refreshFacet();
    },
    onCollapsed: function()
    {
        this.parameters.collapsed = true;
        this.refreshFacet();
    },
    onItemSelect: function(item)
    {
        var data = item.getData().userData;
        if (data && data.isAddButton)
        {
            var controller = MobileUI.app.getController('editors.TypeaheadEditor');
            controller.setFieldName(this.getFieldName());
            controller.setValueChangedHandler(this.onFacetItemAdd, this);
            MobileUI.core.SuiteManager.on('hideDialogs', this.onFacetItemAdd, this, {single: true, delay:700});
            if (!MobileUI.core.SuiteManager.hasDialog('addItem'))
            {
                MobileUI.core.SuiteManager.addDialog('addItem', Ext.create('MobileUI.view.editors.TypeaheadEditor'));
            }

            MobileUI.core.SuiteManager.showDialog('addItem');
            controller.setTitleLabel(this.parameters.actionLabel);
        }
    },
    onFacetItemAdd: function(item, onCancel){
        if (!item || onCancel)
        {
            return;
        }
        var record = item,
            store = this.getStore();
        if (store.findExact('label', record) === -1)
        {
            store.add(this.createItemModel(record, item.model));
            store.sync();
            this.refresh();
            this.state[record] = record;
            this.fireEvent('valueChanged', this.state);
        }
        this.updateListHeight(this);
    },

    getParameters: function()
    {
        return this.parameters;
    },

    refreshFacet: function(facetsJson)
    {
        var parameters, model, store, key, item, facetsDefinition;
        store = this.getStore();

        if (Ext.isObject(this.parameters))
        {
            parameters = this.parameters;
            if (!facetsJson)
            {
                facetsDefinition = this.state;
            } else
            {
                facetsDefinition = facetsJson;
                this.state = facetsDefinition;
            }
            model = [];
            model.push(this.createHeader(parameters.title, parameters.collapsed));
            this.setFieldName(parameters.fieldName);
            if (!parameters.collapsed)
            {
                model.push({
                    label: this.parameters.actionLabel || 'Add',
                    disableSwipe: true,
                    type: MobileUI.components.list.items.BaseListItem.types.facetItem,
                    avatar: 'resources/images/reltio/relations/add-member.svg',
                    userData: {isAddButton:true}
                });

                for (key in facetsDefinition)
                {
                    if (facetsDefinition.hasOwnProperty(key))
                    {
                        item = facetsDefinition[key];
                        model.push(this.createItemModel(item));
                    }
                }
            }
        }
        store.clearData();
        store.add(model);
        store.sync();
        this.refresh();
        this.updateListHeight(this);
    },
    onItemDelete: function(item)
    {
        var record = item.get('label'),
            store = this.getStore(),
            index = store.findExact('label', record);
        if (index !== -1)
        {
            store.removeAt(index);
            store.sync();
            this.refresh();
            delete this.state[record];
            this.fireEvent('valueChanged', this.state);
        }
        this.updateListHeight(this);
    },

    createHeader: function(label, collapsed){
        var obj = {type:MobileUI.components.list.items.BaseListItem.types.facetHeader,
            label: label,
            disableSwipe: true};
        if (collapsed)
        {
            obj.state = 'collapsed';
            obj.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
        } else
        {
            obj.state = 'expanded';
            obj.stateIcon = 'resources/images/reltio/tree/expanded.svg';
        }
        return obj;
    },

    createItemModel: function(label, model)
    {
        return {
            label: label,
            avatar: 'resources/images/reltio/components/facets/tag-icon.svg',
            type: MobileUI.components.list.items.BaseListItem.types.facetItem,
            userData: model
        };
    },
    getActiveParams: function()
    {
        var values = [],
            labels = [];
        for(var key in this.state)
        {
            if (this.state.hasOwnProperty(key))
            {
                values.push(key);
                labels.push(this.state[key]);
            }
        }
        return {labels: labels, values: values};

    },
    getSearchParams: function()
    {
        var params = this.getActiveParams();

        return {fieldName: this.parameters.fieldName, title: this.parameters.title, filter: 'equals', labels: params.labels, values: params.values, notForState: true};
    },
    setParameters: function(parameters)
    {
        this.parameters = parameters;
        this.refreshFacet();
    },
    getState: function () {
        var parameters = this.getParameters();
        var params = this.getActiveParams();
        return {
            collapsed: parameters.collapsed,
            fieldName: parameters.fieldName,
            labels: params.labels,
            values: params.values,
            filter: 'equals',
            title: parameters.title,
            state: this.state
        };
    }
});
