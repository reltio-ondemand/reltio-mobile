/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.ToggleButtonFacet', {
    extend: 'MobileUI.components.EditableList',
    mixins: {
        observable: 'Ext.mixin.Observable',
        listhelper: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel'

    },
    parameters: null,
    facetsJson: null,
    config: {
        scrollable: false,
        height: 0,
        cls: 'elist togglebuttonfacet',
        store: {
            data: [],
            model: 'MobileUI.components.list.model.EListModel'
        },
        listeners: {
            painted: function () {
                if (!this.painted) {
                    this.refreshFacet();
                    this.painted = true;
                }
            },
            expand: function () {
                this.onExpanded();
            },
            collapse: function () {
                this.onCollapsed();
            },
            facetAction: function(e) {
                this.facetAction(e);
            }
        }
    },
    facetAction: function(e){
        if (e.type === 'select')
        {
            this.selectAllItems();
        }else {
            this.deselectAllItems();
        }
    },
    onExpanded: function () {
        this.parameters.collapsed = false;
        this.refreshFacet();
    },
    onCollapsed: function () {
        this.parameters.collapsed = true;
        this.refreshFacet();
    },
    refreshFacet: function (facetsDefinition) {
        this.refreshInProgress = true;
        if (facetsDefinition) {
            this.facetsDefinition = facetsDefinition;
        }
        facetsDefinition = facetsDefinition || this.facetsDefinition;
        var parameters, model, store, key, item;
        store = this.getStore();
        if (Ext.isObject(this.parameters)) {
            parameters = this.parameters;
            model = [];
            model.push(this.createHeader(parameters.title, parameters.collapsed));
            if (!parameters.collapsed) {
                for (key in facetsDefinition) {
                    if (facetsDefinition.hasOwnProperty(key)) {
                        item = facetsDefinition[key];
                        model.push(this.createItemModel(item.label, item.model, item.on, this.onFacetSelected, this));
                    }
                }
            }
        }
        store.clearData();
        store.add(model);
        store.sync();
        this.refresh();
        this.updateListHeight(this);
        this.refreshInProgress = false;
    },
    onFacetSelected: function (item, key, model) {
        var value = item.getValue? item.getValue(): item.value;
        this.setToggleState(model, value);
    },

    selectAllItems: function () {

    },

    deselectAllItems: function () {

    },
    getSearchParams: function () {

    },
    createHeader: function (label, collapsed) {
        var obj = {
            type: MobileUI.components.list.items.BaseListItem.types.facetHeader,
            label: label,
            buttons: []
        };
        if (collapsed) {
            obj.state = 'collapsed';
            obj.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
        } else {
            obj.state = 'expanded';
            obj.stateIcon = 'resources/images/reltio/tree/expanded.svg';
        }
        return obj;
    },

    setErrorMessage: function (message) {
        var store = this.getStore(),
            index, record;
        index = store.findExact('type', MobileUI.components.list.items.BaseListItem.types.facetHeader);
        if (index !== -1) {
            record = store.getAt(index);
            record.set('error', message);
            record.setDirty();
            store.sync();
        }
    },

    setToggleState: function(model, state){
        var store = this.getStore(),
            index, record;
        index = store.findExact('userData', model);
        if (index !== -1) {
            record = store.getAt(index);
            record.set('property', state);
        }
    },

    refreshStore: function(){
        this.refresh();
    },

    setState: function(state){
        var store = this.getStore(),
            index, record;
        index = store.findExact('type', MobileUI.components.list.items.BaseListItem.types.facetHeader);
        if (index !== -1) {
            record = store.getAt(index);
            record.set('buttons',[{
                action: 'select',
                label: i18n('Select All'),
                status: state === 'all' || state ==='select' ? 'active': ''
            }, {
                action: 'deselect',
                label: i18n('Deselect All'),
                status: state === 'all' || state ==='deselect' ? 'active': ''
            }]);
            record.setDirty();
            store.sync();
        }
    },

    createItemModel: function (label, model, on, changeHandler, context) {
        return {
            type: MobileUI.components.list.items.BaseListItem.types.toggle,
            label: label,
            property: on,
            userData: model,
            editor: changeHandler && function (item) {
                if (!this.refreshInProgress) {
                    changeHandler.call(context, item, label, model);
                }
            },
            scope: this,
            offset: 20
        };
    },

    setParameters: function (parameters) {
        this.parameters = parameters;
        this.refreshFacet();
    },
    getParameters: function () {
        return this.parameters;
    },
    getState: function () {

    },
    resetFacet: function () {
        this.parameters = null;
    }
});
