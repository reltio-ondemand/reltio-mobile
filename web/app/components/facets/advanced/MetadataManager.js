/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.facets.advanced.MetadataManager', {
    fields: null,
    processMetadata: function () {
        this.fields = {};
        var metadata = MobileUI.core.Metadata;
        Object.keys(metadata.getEntityTypes()).forEach(function (entityTypeKey) {
            var entityType = metadata.getEntityTypes()[entityTypeKey];
            if (MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType)) {
                this.fields[entityTypeKey] = {};
                Object.keys(entityType.attributes).forEach(function(attrKey) {
                    var attribute = entityType.attributes[attrKey];
                    if (MobileUI.core.PermissionManager.securityService().metadata.canRead(attribute)) {
                        this.createModel(attribute.label, attribute.type, attribute, null, entityTypeKey);
                    }
                }, this);
                if (entityType.analyticsAttributes) {
                    var Type = MobileUI.components.facets.AdvancedSearch.type;
                    Object.keys(entityType.analyticsAttributes).forEach(function(attrKey) {
                        var attribute = entityType.analyticsAttributes[attrKey];
                        if (MobileUI.core.PermissionManager.securityService().metadata.canRead(attribute)) {
                            var aaType = attribute.analyticsAttributes ? Type.NESTED : attribute.type;
                            this.createModel(attribute.label, aaType, attribute, null, entityTypeKey);
                        }
                    }, this);
                }
            }
        }, this);
        this.initAuditRecords();

        this.initSourceRecords();

        this.initCollaborationRecords();
    },
    initAuditRecords: function () {
        this.auditInit = [
            {caption: 'Created by', name: 'createdBy', uri: 'special/createdBy', faceted: true},
            {caption: 'Created Date', name: 'createdTime', uri: 'special/createdTime', attributeType: 'Date'},
            {caption: 'Updated Date', name: 'updatedTime', uri: 'special/updatedTime', attributeType: 'Date'},
            {caption: 'Updated by', name: 'updatedBy', uri: 'special/updatedBy', faceted: true}
        ];
    },

    initSourceRecords: function () {
        this.sourcesInit = [{
            caption: 'Source System Name',
            name: 'sourceSystems',
            uri: 'special/sourceSystems',
            faceted: true,
            attributeType: 'Enum'
        },
            {caption: 'ID Value', name: 'crosswalks.value', uri: 'special/crosswalks.value'},
            {
                caption: 'Reltio Load Date',
                name: 'crosswalks.reltioLoadDate',
                uri: 'special/source/reltioLoadDate',
                attributeType: 'Date'
            },
            {
                caption: 'Create Date',
                name: 'crosswalks.createDate',
                uri: 'special/source/createDate',
                attributeType: 'Date'
            },
            {
                caption: 'Update Date',
                name: 'crosswalks.updateDate',
                uri: 'special/source/updateDate',
                attributeType: 'Date'
            },
            {
                caption: 'Delete Date',
                name: 'crosswalks.deleteDate',
                uri: 'special/source/deleteDate',
                attributeType: 'Date'
            },
            {
                caption: 'Publish Date',
                name: 'crosswalks.sourcePublishDate',
                uri: 'special/source/publishDate',
                attributeType: 'Date'
            }
        ];
    },
    initCollaborationRecords: function () {
        this.collaborationInit = [
            {caption: 'Commenter', name: 'attributes.Commenters', uri: 'special/collaboration/commenter', faceted: true}
        ];
    },
    createModel: function (caption, type, attributeDef, parent, entityType) {
        var AS = MobileUI.components.facets.AdvancedSearch;
        if (attributeDef && attributeDef.uri && !attributeDef.hidden) {
            var record = {
                nonSearchable: !AS.isSearchable(attributeDef),
                caption: caption,
                attributeType: type,
                entityType: entityType,
                name: attributeDef.name,
                uri: attributeDef.uri,
                faceted: attributeDef.faceted,
                children: [],
                enumList: []
            };
            if (parent) {
                record.parent = parent.uri;
            }
            if (attributeDef.values) {
                record.enumList = attributeDef.values;
            }
            if (type === AS.type.NESTED) {
                var nestedAttrTypes = attributeDef.analyticsAttributes || attributeDef.attributes;
                nestedAttrTypes.forEach(function (attrTypeObj) {
                    this.createModel(attrTypeObj.label, attrTypeObj.type, attrTypeObj, record, entityType);
                }, this);
            } else if (type === AS.type.REFERENCE) {
                var referencedAttrTypes = attributeDef.referencedAttributeURIs;
                var md = MobileUI.core.Metadata;
                var refEntity = md.getEntityTypes()[attributeDef.referencedEntityTypeURI];
                referencedAttrTypes.forEach(function (uri) {
                    var attrTypeObj = null;
                    if (uri.indexOf('configuration/relationTypes') === 0) {
                        attrTypeObj = md.findRelationAttributeByUri(null, uri);
                    }
                    else {
                        attrTypeObj = md.findEntityAttributeByUri(refEntity, uri);
                    }

                    if (!attrTypeObj) {
                        MobileUI.core.Logger.info('Sanity check failed: could not find attribute uri ' + uri + ' for ref.attribute');
                        return;
                    }
                    if (attrTypeObj.hidden || attrTypeObj.searchable !== true) {
                        return;
                    }

                    this.createModel(attrTypeObj.label, attrTypeObj.type, attrTypeObj, record, entityType);
                }, this);
            } else {
                this.checkLookup(record, attributeDef);
            }
            if (parent !== null) {
                var arr = parent.children;
                arr.push(record);
                parent.children = arr;
            }
            if (!this.fields[entityType]) {
                this.fields[entityType] = {};
            }
            if (!this.fields[entityType][record.uri]) {
                this.fields[entityType][record.uri] = [];
            }
            this.fields[entityType][record.uri].push(record);
        }
    },
    checkLookup: function (record, attrType) {
        if (attrType.dependentLookupCode) {
            record.attributeType = MobileUI.components.facets.AdvancedSearch.type.DEPENDENT_LOOKUP;
            record.dependentLookupCode = attrType.dependentLookupCode;
        } else if (attrType.lookupCode) {
            record.attributeType = MobileUI.components.facets.AdvancedSearch.type.LOOKUP;
            record.lookupCode = attrType.lookupCode;
        } 
    },

    /**
     * @returns {[object]} the map uri:record
     */
    getFields: function (entityType) {
        return this.fields[entityType];
    },
    /**
     *  return array of the audit fields or empty array
     */
    getAuditFields: function () {
        return Ext.clone(this.auditInit || []);
    },


    /**
     *  return array of the source fields or empty array
     */
    getSourceFields: function () {
        return Ext.clone(this.sourcesInit || []);
    },

    /**
     *  return array of the collaboration fields or empty array
     */
    getCollaborationFields: function () {
        return Ext.clone(this.collaborationInit || []);
    },
    findBySpecialFieldsUri: function (uri) {
        var filtered = this.getAuditFields()
            .concat(this.getSourceFields())
            .concat(this.getCollaborationFields()).filter(function (item) {
                return item.uri === uri;
            });
        if (filtered.length > 0) {
            return filtered[0];
        }
        return null;
    },

    findByUri: function (uri, entityType, parentUri) {
        if (entityType) {
            if (Ext.isObject(this.fields[entityType])) {

                return this.returnRecordForParent(this.fields[entityType][uri], parentUri);
            }
            else {
                return null;
            }
        }
        for (var type in this.fields) {
            if (this.fields.hasOwnProperty(type) && Ext.isObject(this.fields[type])) {
                if (this.fields[type].hasOwnProperty(uri)) {
                    return this.returnRecordForParent(this.fields[type][uri], parentUri);
                }
            }
        }
        return null;
    },

    returnRecordForParent: function (records, parentUri) {
        if (Ext.isArray(records)) {
            if (parentUri) {
                records = records.filter(function (item) {
                    return item.parentUri === parentUri;
                });
            }
            return records[0];
        }
        return records;
    },
    getSimpleFields: function (type) {
        var result = [];
        var Model = MobileUI.components.facets.AdvancedSearch;
        for (var fld in this.fields[type]) {
            if (this.fields[type].hasOwnProperty(fld)){
                var currentFld = this.fields[type][fld];
                for (var attr in currentFld) {
                    if (currentFld.hasOwnProperty(attr)) {
                        var e = currentFld[attr];
                        if (e.attributeType !== Model.type.REFERENCE && e.attributeType !== Model.type.NESTED && !e.parent) {
                            if (!e.hidden) {
                                result.push(e);
                            }
                        }
                    }
                }
            }

        }
        return Ext.clone(result);
    },
    getComplexFields: function (type) {
        var result = [];
        var Model = MobileUI.components.facets.AdvancedSearch;
        for (var fld in this.fields[type]) {
            if (this.fields[type].hasOwnProperty(fld)){
                var currentFld = this.fields[type][fld];
                for (var attr in currentFld) {
                    if (currentFld.hasOwnProperty(attr)){
                        var e = currentFld[attr];
                        if ((e.attributeType === Model.type.REFERENCE || e.attributeType === Model.type.NESTED) && !e.parent) {
                            if (!e.hidden) {
                                result.push(e);
                            }
                        }
                    }
                }
            }
        }
        return Ext.clone(result);
    }
});
