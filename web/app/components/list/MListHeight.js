/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.MListHeight', {
    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },
    /**
     * Method updates the list height and return new height
     * @public
     * @param list {elist}
     * @return {Number}
     */
    updateListHeight: function (list)
    {

        var items = list.getViewItems(),
            itemsLn = items && items.length || 0,
            totalHeight = 0,
            item, i;
        for (i = 0; i < itemsLn; i++)
        {
            item = items[i];
            totalHeight = totalHeight + item.element.dom.offsetHeight;
        }
        if (list.element) {
            //TODO: this is a ugly check, cause sometimes height wont be recalculated
            //logic about dialog hiding needs to be rewritten
            list.setHeight(totalHeight);
        }
        this.fireEvent('calculatedHeight', totalHeight);
        return totalHeight;
    },
    recalculateHeightDelayed: function(e)
    {
        var task = Ext.create('Ext.util.DelayedTask', function() {
            this.recalculateHeight(e);
        },this);
        task.delay(500);
    },
    /**
     *
     * @param e {Object} the event which contains list
     */
    recalculateHeight: function (e)
    {
        if (!e.isDestroyed || !e.isDestroying)
        {
            this.updateListHeight(e);
        }
    }

});
