/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.MListModel', {
    createListModel: function (attributes, label, editable) {
        var model = [], ItemUtils = MobileUI.core.entity.EntityItemUtils, i = 0;
        attributes.forEach(function (attribute) {
            var item, tmpAttribute = Ext.clone(attribute);
            if (attribute.attrType.hidden === true) {
                return;
            }
            if (!attribute.attrType.activeness && attribute.attrType.type === 'String' && label) {
                tmpAttribute.value = [{
                    value: label
                }];
                label = null;
            }
            else if (!Ext.isEmpty(attribute.value) || !Ext.isEmpty(attribute.label)) {
                tmpAttribute.value = [{
                    value: Ext.isObject(attribute.value[0]) ? '' : attribute.value[0],
                    label: attribute.label
                }];
            }
            item = ItemUtils.processOneValueAttribute(tmpAttribute, attribute.attrType, i + 1 === attributes.length, editable);
            if (MobileUI.core.entity.EntityUtils.isComplex(attribute.attrType)) {
                item.info = attribute;
                item.info.parentUri = attribute.parentUri;
                item.type = 'property';
            }
            else {
                item.editor = attribute.attrType;
            }
            item.userData.parentUri = attribute.parentUri;
            item.userData.uri = attribute.uri;
            model.push(item);
        }, this);
        return model;
    },

    saveValues: function (value, store, force) {
        store.each(function (rec) {
            var data = rec.getData();
            if (!data.userData) {
                return;
            }

            var editor = data.editor, attribute,
                info = data.info,
                attrType = Ext.isObject(info) ? data.info.attrType : data.info,
                uri = data.userData.uri || data.userData.editingUri,
                val;

            if (!MobileUI.core.entity.EntityUtils.isComplex(attrType) && Ext.isEmpty(editor)) {
                return;
            }
            attrType = editor || attrType;
            if (!MobileUI.core.entity.EntityUtils.isComplex(attrType.type)) {
                var attributes = value.attributes;
                var found = null;
                if (!Ext.isEmpty(data.label) && (!Ext.isEmpty(attributes) || force)) {
                    for (var i = 0; i < attributes.length; i++) {
                        attribute = attributes[i];
                        if (attribute.uri === uri) {
                            found = attribute;
                            break;
                        }
                    }

                    if (!found && force) {
                        found = {attrType: attrType, uri: uri};
                        attributes.push(found);
                    }
                    if (found) {
                        val = data.label;
                        found.value = [val];
                    }
                }
            }
        });
    }
});