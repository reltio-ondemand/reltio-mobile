/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.editors.DependentLookupEditor', {
    extend: 'Ext.Panel',
    mixins: {
        listEditor: 'MobileUI.controller.editors.MListEditor'
    },
    xtype: 'dependentlookupeditor',
    config: {
        name: 'dependent-lookup-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card lookupeditor',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    lookupCode: null,
    attrTypeUri: null,
    editingUri: null,
    localizeLabel: null,
    needUpdate: null,
    dependentLookupCode: null,
    inited: null,
    /** @protected */
    valueChangedHandler: null,
    /** @private */
    valueChangedContext: null,
    /** @private */
    valueType: '',
    /** @private */
    oldFilter: null,

    getTitle: function () {
        return this.down('component[name=dependentlookupeditor-title]');
    },
    getCancelBtn: function () {
        return this.down('button[name=dependentlookupeditor-cancelBtn]');
    },
    getDoneBtn: function () {
        return this.down('button[name=dependentlookupeditor-doneBtn]');
    },
    getSearchField: function () {
        return this.down('textfield[name=dependentlookupeditor-search]');
    },
    getList: function () {
        return this.down('elist[name=dependentlookupeditor-list]');
    },
    getBackButton: function () {
        return this.down('component[name=dependentlookupeditor-backButton]');
    },
    initialize: function () {
        this.callParent(arguments);
        this.refresh();
    },


    refresh: function () {
        var context = this;
        Ext.create('Ext.data.Store', {
            storeId: 'dependentLookupeditorStore',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'dependentlookupeditor-cancelBtn',
                        cls: 'text-button',
                        listeners: {
                            tap: this.onCancel,
                            scope: context
                        }
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'dependentlookupeditor-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'text-button',
                        name: 'dependentlookupeditor-doneBtn',
                        text: i18n('Done'),
                        listeners: {
                            tap: this.onDone,
                            scope: context
                        }
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'dependentlookupeditor-search',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search'),
                listeners: {
                    keyup: this.onKeyUp,
                    clearicontap: this.onClearIcon,
                    scope: context
                }
            },
            {
                flex: 1,
                xtype: 'elist',
                listeners: {
                    selectItem: this.onDependentItemTap,
                    painted: this.onListPainted,
                    scope: context
                },
                name: 'dependentlookupeditor-list',
                store: 'dependentLookupeditorStore'
            }
        ]);
    },
    onDependentItemTap: function(data){
        if (data.get('userData') === 'disabled'){
            return;
        }
        if (this.selectedItem && this.selectedItem.getRecord()) {
            this.selectedItem.getRecord().set('selected', false);
            this.selectedItem.recreate();
        }

        if (this.selectedItem === data.item){
            this.value = null;
            this.label = '';
            data.set('selected', false);
            MobileUI.core.session.Session.getLookupsManager().removeDependentLookupValue(this.attrTypeUri, this.editingUri);
            data.set('localizeLabel', '');
            this.selectedItem = null;
        }else {
            this.value = this.getRecordValue(data);
            this.label = this.getLabelValue(data);
            data.set('selected', true);
            this.selectedItem = data.item;
        }
        data.item.recreate();

        if (this.getSelectImmediate) {
            this.onDone();
        }
    },

    /** @protected */
    onDone: function () {
        MobileUI.core.SuiteManager.popDialogHistory();
        MobileUI.components.ListItemEditor.processState(MobileUI.core.SuiteManager.getCurrentState());
        if (this.valueChangedHandler) {
            this.fireEvent('change', this, this.getValue());
            this.valueChangedHandler.call(this.valueChangedContext, this.getValue(), false, this.getLocalizeLabel());
        }
    },


    /** @protected */
    onCancel: function () {
        MobileUI.components.ListItemEditor.processState(MobileUI.core.SuiteManager.getCurrentState());
        if (this.valueChangedHandler) {
            this.valueChangedHandler.call(this.valueChangedContext, this.getValue(), true);
        }
    },

    setTitleText: function (text) {
        this.getTitle().setHtml(Ext.util.Format.htmlEncode(i18n(text)));
    },

    /**
     * @param handler {Function}
     * @param context {Object?}
     */
    setValueChangedHandler: function (handler, context) {
        this.valueChangedHandler = handler;
        this.valueChangedContext = context;
    },

    setValueType: function (valueType) {
        this.valueType = valueType;
    },

    onInputChanged: function () {
        if (this.getValue) {
            var value = this.getValue();
            var valid = MobileUI.core.util.Validator.isValid(value, this.valueType);
            this.setValid(valid);
        }
    },

    setValid: function (valid) {
        if (this.getInvalidMarker) {
            var marker = this.getInvalidMarker();
            if (marker) {
                marker.setHidden(valid);
                this.getDoneBtn().setHidden(!valid);
            }
        }
    },

    onClear: function () {
        var editor = this;
        setTimeout(function () {
            editor.onInputChanged();
        }, 10);
    },

    onBackButton: function () {
        MobileUI.core.SuiteManager.back();
    },
    /**
     * @param editorInfo
     * @param editingUri
     * @param localizeLabel
     */
    setInfo: function (editorInfo, editingUri, localizeLabel) {
        this.lookupCode = editorInfo.lookupCode || editorInfo.dependentLookupCode;
        this.attrTypeUri = editorInfo.uri;
        this.editingUri = editingUri;
        this.localizeLabel = localizeLabel;
        this.registerEditor();
    },
    fillList: function (filter) {
        var list = this.getList(),
            store = list.getStore();
        if (filter === this.oldFilter){
            return;
        }
        this.oldFilter = filter;

        store.clearData();
        filter = filter || '';
        var lookupManager = MobileUI.core.session.Session.getLookupsManager();
        lookupManager.getValues(this.lookupCode, this.editingUri, this.attrTypeUri, filter, this.onResponse, null, this);
    },
    onResponse: function (response) {
        response = response.codeValues || {};
        var list = this.getList(),
            values = response[this.lookupCode],
            store = list.getStore();

        if (!Ext.isEmpty(values)) {
            var processLookup = function (key, value) {
                var text = value.displayName || key;

                if (text !== key) {
                    text += ' (' + key + ')';
                }

                return {
                    label: text,
                    userData: {value: key},
                    type: MobileUI.components.list.items.BaseListItem.types.selectable,
                    selected: this.isItemSelected(key, this.getValue())
                };
            }.bind(this);
            var model = Object.keys(values).map(function (key) {
                return processLookup(key, values[key]);
            });
            store.clearData();
            store.add(model.sort(function (a, b) {
                if (a.label > b.label) {
                    return 1;
                }
                if (a.label < b.label) {
                    return -1;
                }
                return 0;
            }));
            list.refresh();
            var items = list.getViewItems(),
                i;
            this.selectedItem = null;
            for (i = 0; i < items.length; i++) {
                if (items[i].getRecord().get('selected')) {
                    this.selectedItem = items[i];
                    break;
                }
            }
        } else {
            store.clearData();
            store.add({
                label: i18n('No values have been found'),
                baseCls: 'rl-dependent-disabled',
                userData: 'disabled',
                type: MobileUI.components.list.items.BaseListItem.types.selectable
            });
            list.refresh();
        }
    },
    isItemSelected: function (item, value) {
        return item === value;
    },

    getRecordValue: function (record) {
        return record.get('userData').value;
    },

    getLabelValue: function (record) {
        return record.get('label');
    },

    registerEditor: function () {
        if (this.attrTypeUri && this.lookupCode && this.editingUri && !this.inited) {
            this.inited = true;
            var manager = MobileUI.core.session.Session.getLookupsManager();
            manager.registerEditor(this, this.editingUri, this.lookupCode);
        }
    }
});

