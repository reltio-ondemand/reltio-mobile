/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.editors.NumberEditor', {
    extend: 'Ext.field.Number',
    xtype: 'truenumbereditor',
    initialize: function () {
        this.callParent();
        this.setStepValue('any');
        var component = this.getComponent();
        component.input.on({
            scope: this,
            keypress: 'onKeyPress'
        });
    },


    getValue: function () {
        this.callParent();
        var originalValue = this.getComponent().getValue(),
            value = parseFloat(originalValue, 10);
        return (isNaN(value)) ? null : originalValue;
    },

    onKeyPress: function () {
        this.oldValue = this.getComponent().getValue();
    },

    applyValue: function (value) {
        var minValue = this.getMinValue(),
            maxValue = this.getMaxValue();

        if (Ext.isNumber(minValue) && Ext.isNumber(value)) {
            value = Math.max(value, minValue);
        }

        if (Ext.isNumber(maxValue) && Ext.isNumber(value)) {
            value = Math.min(value, maxValue);
        }
        return (isNaN(parseFloat(value))) ? '' : value;
    },

    doKeyUp: function (me, e) {
        var value = me.getValue(),
            valueValid = value !== undefined && value !== null && value !== '' ||
                //for invalid values browser return value="", need check validity too
                (!this.getComponent().input.dom.validity || !this.getComponent().input.dom.validity.valid);

        if (!this.getComponent().input.dom.validity || !this.getComponent().input.dom.validity.valid){
            this.getComponent().setValue(this.oldValue);
            valueValid = this.getComponent().input.dom.validity.valid;
        }
        this[valueValid ? 'showClearIcon' : 'hideClearIcon']();
        if (e.browserEvent.keyCode === 13) {
            me.fireAction('action', [me, e], 'doAction');
        }
    }
});
