/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.editors.TimestampEditor', {
    extend: 'Ext.Panel',
    xtype: 'timestampeditor',
    config: {
        cls: 'timestampeditor',
        layout: 'hbox',
        value: '',
        disabled: false
    },

    applyValue: function(value) {
        if (!this.internalSetValue) {
            this.refresh(value);
        }
        return value;
    },

    twoDigits: function(x) {
        if (isNaN(x)) {
            return '';
        }
        return (x < 10) ? ('0' + x) : x;
    },

    internalSetValue: false,

    changed: function(value) {
        this.internalSetValue = true;
        this.setValue(value);
        this.internalSetValue = false;

        this.fireEvent('change', this, value);
    },

    refresh: function(value) {
        this.removeAll();

        var date =  MobileUI.core.I18n.parseDate(value);
        if (!date) {
            date = new Date(0);
        }
        var useAMPM = moment.localeData()._longDateFormat.LTS.indexOf('A') !== -1;
        var hours = date.getHours();
        var isPM = hours >= 12;
        var editor = this;

        function triggerChange() {
            editor.changed(date.toISOString());
        }

        this.add([
            Ext.create('Ext.field.Text', {
                value: (value === '') ? '' : date.getFullYear() + '-' + this.twoDigits(date.getMonth() + 1) + '-' + this.twoDigits(date.getDate()),
                listeners: {
                    change: function(editor, value) {
                        var parts = value.split('-');
                        if (parts.length > 2) {
                            date.setFullYear(parseInt(parts[0], 10));
                            date.setMonth(parseInt(parts[1], 10) - 1);
                            date.setDate(parseInt(parts[2], 10));
                        } else {
                            date = new Date(0);
                        }
                        triggerChange();
                    }
                },
                disabled: this.getDisabled(),
                component: {type: 'date'},
                name: 'date',
                clearIcon: false,
                cls: 'rl-inline-editor date-part'
            })
        ]);
        if (value !== '') {
            this.add([
                Ext.create('MobileUI.components.list.editors.NumberEditor', {
                    cls: 'rl-inline-editor time-part',
                    value: useAMPM ? (hours === 0 ? 12 : (hours <= 12 ? hours : hours - 12)) : this.twoDigits(hours),
                    clearIcon: false,
                    disabled: this.getDisabled(),
                    listeners: {
                        change: function(editor, value) {
                            date.setHours(parseInt(value, 10) + ((useAMPM && isPM) ? 12 : 0));
                            triggerChange();
                        }
                    }
                }),
                Ext.create('Ext.Component', {
                    html: ':',
                    cls: 'time-separator'
                }),
                Ext.create('MobileUI.components.list.editors.NumberEditor', {
                    cls: 'rl-inline-editor time-part',
                    value: this.twoDigits(date.getMinutes()),
                    clearIcon: false,
                    disabled: this.getDisabled(),
                    listeners: {
                        change: function(editor, value) {
                            date.setMinutes(parseInt(value, 10));
                            triggerChange();
                        }
                    }
                }),
                Ext.create('Ext.Component', {
                    html: ':',
                    cls: 'time-separator'
                }),
                Ext.create('MobileUI.components.list.editors.NumberEditor', {
                    cls: 'rl-inline-editor time-part last',
                    value: this.twoDigits(date.getSeconds()),
                    clearIcon: false,
                    disabled: this.getDisabled(),
                    listeners: {
                        change: function(editor, value) {
                            date.setSeconds(parseInt(value, 10));
                            triggerChange();
                        }
                    }
                })
            ]);

            if (useAMPM) {
                this.add([
                    Ext.create('MobileUI.components.TripleToggle', {
                        value: isPM,
                        defined: true,
                        disabled: this.getDisabled(),
                        no: moment.localeData().meridiem(8),
                        yes: moment.localeData().meridiem(20),
                        listeners: {
                            change: function(value) {
                                date.setHours(hours + (value ? 12 : -12));
                                triggerChange();
                            }
                        },
                        style: {
                            'float': 'right',
                            marginRight: '1em'
                        }
                    })
                ]);
            }
        }
    }
});

