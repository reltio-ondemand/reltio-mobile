/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.BaseListItem', {
    extend: 'Ext.dataview.component.DataItem',
    xtype: 'eDataItem',

    requires: [
        'MobileUI.components.list.model.EListModel',
        'MobileUI.components.list.items.MChartsLayout',
        'MobileUI.components.list.editors.DependentLookupEditor',
        'Ext.XTemplate'
    ],

    mixins: {
        property: 'MobileUI.components.list.items.MPropertyListItem',
        comments: 'MobileUI.components.list.items.MCommentsListItem',
        relations: 'MobileUI.components.list.items.MRelationsListItem',
        twoHops: 'MobileUI.components.list.items.MTwoHopsConnectionsListItem',
        facetHeader: 'MobileUI.components.list.items.MFacetListItem',
        buttons: 'MobileUI.components.list.items.MButtonsListItem',
        selectable: 'MobileUI.components.list.items.MSelectableListItem',
        toggle: 'MobileUI.components.list.items.MToggleListItem',
        pending: 'MobileUI.components.list.items.MPendingListItem',
        custom: 'MobileUI.components.list.items.MCustomListItem',
        savedSearch: 'MobileUI.components.list.items.MSavedSearchListItem',
        entityInfo: 'MobileUI.components.list.items.MEntityInfoListItem',
        relationsTree: 'MobileUI.components.list.items.MRelationsTreeItem',
        charts: 'MobileUI.components.list.items.MChartsLayout',
        selectImage: 'MobileUI.components.list.items.MSelectImageListItem'
    },

    statics: {
        types: {
            entityInfo: 'entityInfo',
            header: 'header',
            property: 'property',
            linkProperty: 'linkProperty',
            actionProperty: 'actionProperty',
            infoProperty: 'infoProperty',
            imageLinkProperty: 'imageLinkProperty',
            facetItem: 'facetItem',
            facetHeader: 'facetHeader',
            editor: 'editor',
            buttons: 'buttons',
            pending: 'pending',
            simple: 'simple',
            savedSearch: 'savedSearch',
            comments: 'comments',
            relationsInfo: 'relationsInfo',
            twoHopsInfo: 'twoHopsInfo',
            selectable: 'selectable',
            toggle: 'toggle',
            custom: 'custom',
            relationsTree: 'relationsTree',
            selectImage: 'selectImage',
            chart: 'chart'
        }
    },

    initialize: function() {
        this.callParent(arguments);
        this.addCls('x-list-item');
        this.element.on('tap', this.onItemTap, this);
    },
    /**
     * Method copies only simple data and arrays. All objects copy by reference
     * @param data
     * @returns {*}
     */
    makeRecordCopy: function (data) {
        var result, that = this;
        var copy = function (source) {
            var to;
            if (!Ext.isObject(source) && !Ext.isArray(source)) {
                to = Ext.clone(source);
            } else if (Ext.isArray(data[key])) {
                to = that.makeRecordCopy(source);
            } else {
                to = source;
            }
            return to;
        };
        if (Ext.isObject(data)) {
            result = {};
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    result[key] = copy(data[key]);
                }
            }
        } else if (Ext.isArray(data)) {
            result = [];
            data.forEach(function (item) {
                result.push(copy(item));
            });
        }
        return result;
    },
    updateRecord: function(record) {
        if (this.built && this.currentRecord && record && record.getData  && MobileUI.core.util.Util.equals(record.getData(), this.currentRecord))
        {
            return;
        }
        this.removeAll();
        this.handlers = {};

        if (record && record.getData)
        {
            this.currentRecord =  this.makeRecordCopy(record.getData());
        }

        this.built = true;

        if (record) {
            var type = record.get('type') || 'do';
            var fn = type + 'Layout';

            var html = this[fn](record);

            if (this.sideMenu()) {
                var pos = html.lastIndexOf('</div>');
                html = html.substr(0, pos) + '<div class="rl-edit-button menu-icon handler-sideMenu"><div></div></div></div>';
            }

            if (!this.hideSeparator) {
                html += '<div class="rl-item-separator' + (this.isSubGroup ? ' subgroup' : '') + '"></div>';
            }
            this.setHtml(html);

            fn = type + 'AfterLayout';
            if (this[fn]) {
                this[fn]();
            }

            if (this.sideMenu()) {
                this.registerHandler('handler-sideMenu', function(event) {
                    var item = event.item;
                    var buttons = item.sideMenu();
                    var popup = Ext.create('MobileUI.components.Popup');
                    var disabled =  record.get('disableButtons');

                    popup.setWidth(200);
                    popup.setHeight(40 * buttons.length);
                    popup.setHideOnOrientationChange(false);
                    popup.add(buttons.map(function(button) {
                        return {
                            xtype: 'button',
                            cls: 'popup-menu-item-button',
                            html: button.label,
                            disabled: disabled,
                            handler: function() {
                                item.dataview.fireEvent(button.action, event.record);
                                popup.hide();
                            }
                        };
                    }));
                    popup.showBy(event.element, 'cr-cl?');
                }, true);
            }
        }
        else {
            this.removeAll();
        }

        this.callParent(arguments);
    },

    recreate: function() {
        this.removeAll();
        this.built = false;
        this.updateRecord(this.getRecord());
    },

    /** @private */
    built: false,

    /** @private */
    currentRecord: null,

    /** @private */
    handlers: null,

    lastTapedElement: null,

    /** @protected */
    onItemTap: function(event, element) {
        if (this.element.down('.reltio-swipe-button')) {
            return true;
        }

        event.stopPropagation();
        this.lastTapedElement = new Ext.Element(event.target);
        var foundHandler;

        while(element && !foundHandler) {
            for (var handler in this.handlers) {
                if (this.handlers.hasOwnProperty(handler)) {
                    if (element.classList.contains(handler)) {
                        foundHandler = this.handlers[handler];
                        break;
                    }
                }
            }
            if (element === this.element.dom) {
                break;
            }
            element = element.parentElement;
        }

        if (!foundHandler) {
            foundHandler = {action: 'selectItem'};
        }

        if (foundHandler.action === 'edit' && this.showEditor) {
            if (this.showEditor()) {
                return;
            }
        }

        var eventData = foundHandler.richParameter ? {
            record: this.getRecord(),
            element: new Ext.Element(event.target)
        } : this.getRecord();
        eventData.item = this;
        if (Ext.isFunction(foundHandler.action)) {
            foundHandler.action(eventData);
        }
        else {
            this.dataview.fireEvent(foundHandler.action, eventData);
        }
    },

    registerHandler: function(elementClass, action, richParameter) {
        this.handlers[elementClass] = { action: action, richParameter: richParameter};
    },

    /**
     * Method converts \n in the text
     * @param text {String} text
     * @param canBeNull {Boolean?} if the me
     */
    convertText: function(text, canBeNull)
    {
        if (!canBeNull)
        {
            if ((text === null) || (text === undefined))
            {
                text = '';
            }
        }
        text = Ext.util.Format.htmlEncode(text);
        if (Ext.isString(text))
        {
            text = text.replace(/\n/g, '<br/>').replace(/&amp;ndash;/g, '&ndash;');
        }
        return text;
    },

    i18nTag: function(tag) {
        var localized = Ext.clone(tag);
        localized.value = i18n(tag.value);
        return localized;
    },

    sideMenu: function() {
        if (this.getRecord().get('disableSwipe')) {
            return false;
        }

        var plugins = this.dataview.getPlugins();
        var plugin;
        if (plugins) {
            for (var i = 0; i < plugins.length; i++) {
                if (plugins[i].getFilterButtons && plugins[i].getSwipeRightButtons) {
                    plugin = plugins[i];
                    break;
                }
            }
        }

        var buttons;
        if (plugin && plugin.getUseMenu()) {
            var filterButtonFn = plugin.getFilterButtons();
            var record = this.getRecord();
            buttons = plugin.getSwipeRightButtons().filter(function (button) {
                return !filterButtonFn || filterButtonFn(button.id, record);
            });
        }

        return (buttons && buttons.length > 0) ? buttons : null;
    }
});
