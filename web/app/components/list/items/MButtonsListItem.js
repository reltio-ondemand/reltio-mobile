/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MButtonsListItem', {

    requires: [
        'MobileUI.components.ListItemEditor'
    ],

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right rl-buttons-item {classes}">' +
            '   <tpl for="buttons">' +
            '       <div class="rl-button handler-button rl-align-center rl-hbox" data-index="{index}">' +
            '           <tpl if="icon"><img class="rl-fixed-size rl-align-center-self" src="{icon}" /></tpl>' +
            '           <div class="rl-fixed-size rl-align-center-self rl-button-label">{label}</div>' +
            '       </div>' +
            '   </tpl>' +
            '</div>'),

        fields: [ 'lastInGroup', 'info', 'label', 'avatar' ]
    },

    /**
     * @protected
     * Method returns buttons property layout
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    buttonsLayout: function(record) {
        this.hideSeparator = record.get('lastInGroup');
        var info = record.get('info');
        this.registerHandler('handler-button', function(data) {
            var element = data.element.dom;

            data.index = Number(element.hasAttribute('data-index') ?
                element.getAttribute('data-index') :
                element.parentElement.getAttribute('data-index'));
            data.item.dataview.fireEvent('execute', data);
        }, true);

        var labels = [].concat(record.get('label'));
        var icons = [].concat(record.get('avatar'));

        return MobileUI.components.list.items.MButtonsListItem.template.apply({
            buttons: labels.map(function(label, index) {
                return {
                    label: i18n(label),
                    icon: icons[index],
                    index: index
                };
            }),
            classes: info
        });
    }
});
