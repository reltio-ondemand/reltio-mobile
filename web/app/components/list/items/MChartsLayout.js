/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MChartsLayout', {
    requires: ['MobileUI.core.util.Util', 'MobileUI.components.charts.Chart'],
    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item rl-chart-item">' +
            '<div class="chart"></div>' +
            '</div>'),

        fields: ['chartData', 'chartOptions']
    },
    chartIsInDraw: false,
    chartLayout: function () {
        return MobileUI.components.list.items.MChartsLayout.template.apply({});
    },
    chartAfterLayout: function () {
        var record = this.getRecord(),
            data = record.get('chartData'),
            options = record.get('chartOptions'),
            chart;
        if (!this.chartIsInDraw) {
            chart = Ext.create('MobileUI.components.charts.Chart', {});
            this.chartIsInDraw = true;
            chart.setOnChartPainted(function () {
                this.dataview.fireEvent('recalculate', this.dataview);
                this.chartIsInDraw = false;
            }.bind(this));

            chart.setConfiguration(options);
            chart.setData(data);
            chart.setDataView(this.dataview);

            var element = this.element.down('.chart');
            element = element.dom || element;
            chart.setRenderTo(element);
        }
    }
});
