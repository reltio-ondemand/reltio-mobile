/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MCommentsListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item rl-comment-item">' +
            '   <div class="rl-vbox">' +
            '       <div class="rl-text first-line">{date} {label}</div>' +
            '       <div class="rl-text second-line rl-collapsible">{secondLine}</div>' +
            '   </div>' +
            '   <tpl if="showPositive">' +
            '       <img src="resources/images/reltio/relations/up.svg" />' +
            '   <tpl else>' +
            '       <img src="resources/images/reltio/relations/down.svg" />' +
            '   </tpl>' +
            '</div>'),

        fields: ['date', 'label', 'secondaryLabel', 'userVote']
    },

    /**
     * @protected
     * Method returns comments info layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    commentsLayout: function (record)
    {
        var userVote = record.get('userVote');
        return MobileUI.components.list.items.MCommentsListItem.template.apply({
            date: i18n(record.get('date')),
            label: this.convertText(record.get('label')),
            secondLine: this.convertText(record.get('secondaryLabel')),
            showPositive: !(userVote >= 1 && userVote < 3)
        });
    }
});
