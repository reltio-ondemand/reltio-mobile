/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MCustomListItem', {

    requires: [
        'MobileUI.components.ListItemEditor'
    ],

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right handle-custom-item">' +
            '</div>'),

        fields: ['editor']
    },

    /**
     * @protected
     * Method returns buttons property layout
     * @return {String} html
     */
    customLayout: function() {
        return MobileUI.components.list.items.MCustomListItem.template.apply({});
    },

    customAfterLayout: function() {
        var record = this.getRecord();
        var content = record.get('editor')(record);

        var element = this.element.down('.handle-custom-item');
        element = element.dom || element;
        if (typeof(content) === 'string') {
            element.innerHTML = content;
        }
        else {
            content.setRenderTo(element);
        }
    }
});
