/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MEntityInfoListItem', {

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item stick-right handler-root">' +
            '   <tpl if="avatar != null || defaultAvatar">' +
            '       <div class="rl-avatar">' +
            '           <div style="background-image: url({avatar}), url({defaultAvatar})"></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-title">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '       <div class="rl-tag-list">' +
            '           <tpl for="tags">' +
            '               <div class="rl-tag" style="background-color:{color}; color: {textColor || \'\'}">{value}</div>' +
            '           </tpl>' +
            '       </div>' +
            '       <tpl if="multiline">' +
            '           <div class="rl-error-text">{multiline.error}</div>' +
            '           <div class="rl-cardinality">{multiline.cardinality}</div>' +
            '       </tpl>' +
            '   </div>' +
            '   <tpl if="info && !multiclick">' +
            '       <div class="rl-edit-button">' +
            '           <div></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <tpl if="info && multiclick">' +
            '       <div class="rl-info-button handler-info">' +
            '           <div></div>' +
            '       </div>' +
            '   </tpl>' +
            '</div>'),

        fields: ['label', 'secondaryLabel', 'avatar', 'defaultImage', 'tags', 'info']
    },


    /**
     * @protected
     * Method returns entity info layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    entityInfoLayout: function(record) {
        var info = record.get('info'),
            userData = record.get('userData'),
            multiclick = false;


        //if we are not in the edit mode
        if (!(userData && userData.editingUri) && userData.actionLabel){
            this.registerHandler('rl-title', 'entityTap');
            multiclick = true;
        }
        if (record.get('editor')) {
            this.showEditor = this.showEntityInfoEditor;
        }

        if (multiclick) {
            this.registerHandler('handler-info', info ? 'edit' : 'selectItem');
            this.registerHandler('handler-root', function() {});
        }
        else {
            this.registerHandler('handler-root', info ? 'edit' : 'selectItem');
        }

        return MobileUI.components.list.items.MEntityInfoListItem.template.apply({
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            avatar: record.get('avatar') || record.get('defaultImage'),
            defaultAvatar: record.get('defaultImage'),
            tags: (record.get('tags') || []).map(this.i18nTag),
            info: info,
            multiclick: multiclick,
            multiline: MobileUI.core.entity.EntityItemUtils.getMultiLineInfo(record)

        });
    },

    /**
     * @private
     * @returns {Boolean}
     */
    showEntityInfoEditor: function() {
        var record = this.getRecord();
        return MobileUI.components.ListItemEditor.create(record.get('editor'), this, this.dataview);
    }
});
