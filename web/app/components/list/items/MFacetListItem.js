/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MFacetListItem', {
    requires: ['MobileUI.core.util.Util'],
    statics: {
        templates: {
            headerPhone: new Ext.XTemplate(
                '<div class="rl-item rl-facetheader-item">' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-text">{label}</div>' +
                '       <div class="rl-actions phone rl-hbox rl-align-center">'+
                '           <tpl if="state == \'expanded\'">'+
                '               <tpl for="buttons">'+
                '                   <div class="rl-action {status}" rl-action="{action}">{label}</div>' +
                '               </tpl>' +
                '           </tpl>' +
                '       </div>' +
                '       <tpl if="error">' +
                '           <div class="rl-error">{error}</div>' +
                '       </tpl>' +
                '   </div>' +
                '   <tpl if="state || stateIcon">' +
                '           <button class="expand-button rl-fixed-size">' +
                '               <img class="twohops-item-{state}" src="{stateIcon}"</img>' +
                '           </button>' +
                '   </tpl>' +
                '</div>'),


            headerTablet: new Ext.XTemplate(
                '<div class="rl-item rl-facetheader-item">' +
                '   <div class="rl-vbox rl-align-center">'+
                '       <div class="rl-hbox rl-align-center">'+
                '           <div class="rl-text">{label}</div>' +
                '       </div>' +
                '       <tpl if="error">' +
                '           <div class="rl-error">{error}</div>' +
                '       </tpl>' +
                '   </div>' +
                '           <tpl if="state == \'expanded\'">' +
                '               <div class="rl-actions tablet rl-hbox rl-align-center-self">' +
                '                   <tpl for="buttons">' +
                '                       <div class="rl-action {status}" rl-action="{action}">{label}</div>' +
                '                   </tpl>' +
                '               </div>' +
                '           </tpl>' +
                '   <tpl if="state || stateIcon">' +
                '           <button class="expand-button rl-fixed-size">' +
                '               <img class="twohops-item-{state}" src="{stateIcon}"</img>' +
                '           </button>' +
                '   </tpl>' +
                '</div>'),

            facetItem: new Ext.XTemplate(
                '<div class="rl-item rl-facet-item">' +
                '   <tpl if="icon != null">' +
                '       <div class="rl-avatar">' +
                '           <div style="background-image: url({icon})"></div>' +
                '        </div>' +
                '   </tpl>' +
                '   <div class="rl-title rl-align-center-self">{label}</div>' +
                '   <tpl if="score != null">' +
                '      <div class="rl-fixed-size">' +
                '          <div class="rl-score rl-facet-score">{score}</div>' +
                '      </div>' +
                '   </tpl>' +
                '</div>')
        },

        fields: ['state', 'stateIcon', 'label', 'error', 'avatar', 'score', 'buttons']
    },

    /**
     * @protected
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    facetHeaderLayout: function (record) {
        this.registerHandler('expand-button', function (event) {
            var record = event.record,
                item = event.item,
                store = item.dataview.getStore(),
                index = store.indexOf(record);

            if (record.get('state') === 'collapsed') {
                item.dataview.fireEvent('expand', {
                    record: record,
                    index: index,
                    element: event.element
                });
            }
            else {
                item.dataview.fireEvent('collapse', {
                    record: record,
                    index: index,
                    element: event.element
                });
            }
        }, true);

        this.registerHandler('rl-action', function (event) {
            var record = event.record,
                item = event.item,
                element = event.element;
            if (element.hasCls('active')) {
                item.dataview.fireEvent('facetAction', {
                    type:element.getAttribute('rl-action'),
                    record: record,
                    element: event.element
                });
            }
        }, true);

        var template = Ext.os.is.Phone ?
            MobileUI.components.list.items.MFacetListItem.templates.headerPhone :
            MobileUI.components.list.items.MFacetListItem.templates.headerTablet;
        return template.apply({
            label: i18n(this.convertText(record.get('label'))),
            stateIcon: record.get('stateIcon'),
            state: record.get('state'),
            buttons: record.get('buttons'),
            error: record.get('error')
        });
    },

    /**
     * @protected
     * @param record
     * @return {String}
     */
    facetItemLayout: function(record) {
        return MobileUI.components.list.items.MFacetListItem.templates.facetItem.apply({
            label: i18n(this.convertText(record.get('label'))),
            icon: record.get('avatar'),
            score: record.get('score')
        });
    }
});
