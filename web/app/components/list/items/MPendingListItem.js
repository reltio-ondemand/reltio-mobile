/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MPendingListItem', {
    requires: ['MobileUI.core.util.Util'],
    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },
    statics: {
        template: new Ext.XTemplate('<div class="rl-item rl-align-center rl-spinner-item">' +
            '<div class="rl-vbox rl-align-center rl-fixed-size">{spinner}</div>' +
            '</div>'),
        fields: []
    },

    /**
     * @return {String} html
     */
    pendingLayout: function() {
        return MobileUI.components.list.items.MPendingListItem.template.apply({
            spinner: this.getSpinnerHtml()
        });
    }
});
