/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MPropertyListItem', {

    requires: [
        'MobileUI.components.ListItemEditor',
        'MobileUI.components.StyledSelect',
        'MobileUI.components.list.editors.TimestampEditor',
        'MobileUI.components.DependentLookupInline'
    ],

    statics: {
        templates: {
            propertyTablet: new Ext.XTemplate(
                '<div data-reltio-id= "attribute-{propertyAttrType}" class="rl-item stick-right rl-property-item handler-root {actionCls} {invalid}" style="{offset}">' +
                '   <tpl if="multiline">' +
                '      <div class="rl-property-multiline-container">' +
                '           <div class="rl-caption rl-collapsible rl-property-label {required}">{propertyLabel}</div>' +
                '           <div class="rl-cardinality rl-collapsible" ">{multiline.cardinality}</div>' +

                '      </div>' +
                '   <tpl else>' +
                '      <div class="rl-caption rl-collapsible rl-property-label {required}">{propertyLabel}</div>' +
                '   </tpl>' +
                '   <tpl if="multiline">' +
                '       <div class="rl-multiline rl-align-center-self">' +
                '           <div class="rl-text  handler-editor {asInline} {placeHolderCls}">{label}</div>' +
                '           <div class="rl-error-text">{multiline.error}</div>' +
                '      </div>' +
                '   <tpl else>' +
                '       <div class="rl-text rl-align-center-self handler-editor {asInline} {placeHolderCls}">{label}</div>' +
                '   </tpl>' +
                '   <tpl if="editable">' +
                '       <div class="rl-edit-button {editIcon} handler-edit">' +
                '           <div></div>' +
                '       </div>' +
                '   </tpl>' +
                '</div>'
            ),

            propertyPhone: new Ext.XTemplate(
                '<div data-reltio-id= "attribute-{propertyAttrType}" class="rl-item stick-right rl-property-item phone handler-root {invalid}" style="{offset}">' +
                '   <div class="rl-vbox rl-align-center {collapsed} {actionCls}">' +
                '       <div class="rl-caption rl-collapsible rl-property-label phone {required}">{propertyLabel}</div>' +
                '       <div class="rl-hbox rl-flex rl-align-center">' +
                '           <div class="rl-text handler-editor {asInline} {placeHolderCls}">{label}</div>' +
                '       </div>' +
                '   </div>' +
                '       <tpl if="editable">' +
                '           <div class="rl-edit-button {editIcon} handler-edit">' +
                '               <div></div>' +
                '          </div>' +
                '       </tpl>' +
                '</div>'),

            header: new Ext.XTemplate(
                '<div class="rl-item rl-header">' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-hbox">' +
                '           <div class="{required}">{label}</div>' +
                '       </div>' +
                '       <tpl if="multiline">' +
                '           <div class="rl-error-text">{multiline.error}</div>' +
                '           <div class="rl-cardinality">{multiline.cardinality}</div>' +
                '       </tpl>' +
                '   </div>' +
                '</div>')
        },

        fields: ['subgroup', 'groupName', 'lastInGroup', 'property', 'label', 'editor', 'info', 'localizeLabel', 'required', 'scrollToThis', 'invalid', 'error', 'cardinality'],

        inlineTypes: [
            'String', 'StringNotTokenized',
            'Long', 'Int', 'Number', 'Float',
            'Boolean',
            'Date', 'Timestamp',
            'Blob',
            'URL', 'Blog URL', 'Image URL',
            'SSN', 'Id card (Passport)', 'SIC-code', 'NAICS-code', 'Email domains', 'Ticker symbol', 'CIK id', 'Stock exchange', 'GeoLocation'
        ]
    },

    /**
     * @protected
     * Method returns property layout
     * @param {MobileUI.components.list.model.EListModel} record
     * @param {Boolean?} isLink
     * @param {Boolean?} isImage
     * @param actionCls {String} action class
     * @param forceInfoIcon {Boolean?}
     * @return {String} html
     */
    propertyLayout: function(record, isLink, isImage, actionCls, forceInfoIcon) {
        var editor = record.get('editor'),
            info = record.get('info');

        var inlineEditor = this.inlineEditor(record);
        var placeHolder = null;

        if (editor && editor.dependentLookupCode && !inlineEditor) {
            var editingUri = record.get('userData') && record.get('userData').editingUri;
            placeHolder = this.getPlaceholderText(editor.dependentLookupCode, editingUri, editor.uri);
        }

        actionCls = actionCls || '';
        if (this.inlineEditor()) {
            this.registerHandler('handler-root', function() {});
            this.registerHandler('handler-edit', function() {});
        }
        else if(!placeHolder) {
            if (forceInfoIcon) {
                this.registerHandler('handler-root', function() {});
                this.registerHandler('handler-edit', editor || info ? 'edit' : 'selectItem');
            }
            else {
                this.registerHandler('handler-root', editor || info ? 'edit' : 'selectItem');
                this.registerHandler('handler-link', 'linkClicked');
            }
        }
        if (editor && editor.dependentLookupCode && !this.inlineEditor()) {
            var lookupManager = MobileUI.core.session.Session.getLookupsManager();
            lookupManager.on('dependentUpdated', function (lookups) {
                if (lookups.indexOf(editor.dependentLookupCode) !== -1) {
                    record.set('selected', new Date());
                }
            }, this);

            var userData = record.get('userData');
            var isNonTouched = !lookupManager.isTouched(userData ? userData.editingUri : '');
            var supportsAutoPopulate = lookupManager.isSupportAutoPopulate(editor.uri);

            if (Ext.isEmpty(record.get('label')) && supportsAutoPopulate && isNonTouched && !placeHolder) {
                lookupManager.getValues(editor.dependentLookupCode, userData.editingUri, editor.uri, null, function (result) {
                    if (result && result.codeValues){
                        var codeValue = result.codeValues[editor.dependentLookupCode];
                        if (codeValue && Object.keys(codeValue).length === 1){

                            var value = Object.keys(codeValue)[0];
                            var localizeLabel = codeValue[value].displayName;
                            if (localizeLabel) {
                                record.set('localizeLabel', localizeLabel);
                            }
                            record.set('label', value || '');
                            if (editor.valueChangedCallback) {
                                editor.valueChangedCallback.apply(editor.valueChangedContext, [value || '', this, editor]);
                            }
                            lookupManager.populateValue(editor.uri, userData.editingUri, value || '');
                        }
                    }
                }, null, this);
            }
        }

        this.isSubGroup = record.get('subgroup');
        var last = record.get('lastInGroup');
        this.hideSeparator = Ext.os.is.Phone ?
            (this.isSubGroup ? !last : last) :
            (!this.isSubGroup && last);

        var propertyLabel = this.convertText(record.get('property'), true),
            label = record.get('label'),
            isValidLink,
            img,
            template = Ext.os.is.Phone ?
                MobileUI.components.list.items.MPropertyListItem.templates.propertyPhone :
                MobileUI.components.list.items.MPropertyListItem.templates.propertyTablet;

        function makeLink(url, label) {
            return '<a href="' + url + '" target="_blank" class="handler-link">' + label + '</a>';
        }

        isValidLink = MobileUI.core.util.Util.isLinkValid(label);

        if (isImage) {
            if (isLink) {
                label = MobileUI.core.util.Util.convertToLink(label);
            }
            var url = label;
            img = new Image();
            img.onload = function() {
                this.dataview.fireEvent('recalculate', this.dataview);
            }.bind(this);
            img.src = url;

            label = '<img src="' + url + '" style="max-width: 100px; max-height: 80px"/>';
            if (isLink && isValidLink) {
                label = makeLink(url, label);
            }
        }
        else if (isLink) {
            label = isValidLink ? MobileUI.core.util.Util.convertToLink(label, true) : makeLink('http://' + label, label);
        }
        else {
            label = String(this.convertText(label));
        }

        var localizeLabel = record.get('localizeLabel');
        if (localizeLabel) {
            switch (localizeLabel) {
                case 'date':
                    label = i18n(MobileUI.core.I18n.parseDate(label));
                    break;
                case 'timestamp':
                    label = i18n(MobileUI.core.I18n.parseDate(label), MobileUI.core.I18n.DATE_LONG);
                    break;
                case 'string':
                    label = i18n(label);
                    break;
                case 'number':
                    label = Number(label);
                    if (isNaN(label)) {
                        label = '';
                    }
                    else {
                        label = i18n(label, MobileUI.core.I18n.NUMBER);
                    }
                    break;
                case 'boolean':
                    var value = label;
                    if (value === 'true') {
                        label = 'yes';
                    } else if (value === 'false') {
                        label = 'no';
                    }
                    label = i18n(label);
                    break;
                case 'dollar':
                    label = Number(label);
                    if (isNaN(label)) {
                        label = '';
                    }
                    else {
                        label = i18n(label, MobileUI.core.I18n.DOLLAR_FIELD);
                    }
                    break;
                default:
                    label = localizeLabel;
                    break;
            }
        }

        label = label || placeHolder;
        if (label === '') {
            label = '&nbsp;';
        }

        var editable = editor || info;

        if (editor && !inlineEditor) {
            this.showEditor = this.showPropertyEditor;
        }
        this.placeHolder = placeHolder;
        return template.apply({
            label: label,
            placeHolderCls: placeHolder ? 'reltio-disabled-placeholder' : '',
            offset: record.get('offset') ? 'padding-left: ' + ((record.get('offset') + 1) * 20) + 'px' : '',
            actionCls: actionCls,
            asInline: (editor && (editor.dependentLookupCode || editor.lookupCode))  ? 'rl-as-inline': '',
            collapsed: !propertyLabel && propertyLabel !== null ? 'collapsed' : '',
            propertyLabel: i18n(Ext.os.is.Phone ? propertyLabel :
                (!propertyLabel && propertyLabel !== null ? '&nbsp' : propertyLabel)),
            editable: !inlineEditor && editable,
            editIcon: forceInfoIcon ? 'info-icon' : '',
            changeCallback: record.get('changeCallback'),
            required: record.get('required') && 'required',
            invalid: (record.get('invalid') || placeHolder && label !== placeHolder) && 'invalid',
            multiline: MobileUI.core.entity.EntityItemUtils.getMultiLineInfo(record),
            propertyAttrType: record.get('groupName')
        });
    },

    getPlaceholderText: function (lookupCode, uri, attrTypeUri) {
        var placeHolder = null;
        var lookupManager = MobileUI.core.session.Session.getLookupsManager();
        if (!lookupManager.isInited()) {
            return;
        }
        var parents = lookupManager.getParents(lookupCode, uri, attrTypeUri);
        if (Ext.isEmpty(parents)){
            return;
        }
        if (parents.length === 1) {
            if (!parents[0].getInfo().dependentLookupCode) {
                return;
            }
        }
        var systemUris = Object.keys(MobileUI.core.Metadata.getEntityTypes()).concat(Object.keys(MobileUI.core.Metadata.getRelationTypes()));
        var filteredParents = parents.filter(function (current) {
            var info = current.getInfo();
            return Ext.isEmpty(info.values) && systemUris.indexOf(info.uri) === -1;
        });
        if (!Ext.isEmpty(filteredParents)) {
            var emptyParents = filteredParents.reduce(function (prev, current) {
                var info = current.getInfo();
                return prev.concat(info.uri);
            }, []).map(function (uri) {
                return MobileUI.core.Metadata.findEntityAttributeByUri(null, uri);
            }).filter(function (item) {
                return item;
            }).map(function (item) {
                return item.label;
            });
            var label = Ext.isEmpty(emptyParents) ? 'Parents' : emptyParents.join(' and ');
            placeHolder = i18n('Please select value for') + ' ' + label;
        }


        return placeHolder;
    },
    /**
     * @protected
     * Method returns property layout with link
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    linkPropertyLayout: function(record) {
        return this.propertyLayout(record, true);
    },

    linkPropertyAfterLayout: function() {
        return this.propertyAfterLayout();
    },

    /**
     * @protected
     * Method returns property layout with link
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    imageLinkPropertyLayout: function(record) {
        return this.propertyLayout(record, true, true);
    },

    imageLinkPropertyAfterLayout: function() {
        return this.propertyAfterLayout();
    },

    /**
     * @protected
     * Method returns property layout with action link
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    actionPropertyLayout: function(record) {
        var actionCls = record.get('userData') ? 'action' : null;
        return this.propertyLayout(record, false, false, actionCls);
    },

    actionPropertyAfterLayout: function() {
        return this.propertyAfterLayout();
    },

    infoPropertyLayout: function(record) {
        return this.propertyLayout(record, false, false, null, true);
    },

    infoPropertyAfterLayout: function() {
        return this.propertyAfterLayout();
    },

    /**
     * @private
     * @returns {Boolean}
     */
    showPropertyEditor: function() {
        var record = this.getRecord();
        return MobileUI.components.ListItemEditor.create(record.get('editor'), this, this.dataview);
    },

    /**
     * @protected
     * Method returns the header layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    headerLayout: function(record) {
        this.hideSeparator = true;
        return MobileUI.components.list.items.MPropertyListItem.templates.header.apply({
            label: i18n(this.convertText(record.get('label'))),
            required: record.get('required') && 'required',
            multiline: MobileUI.core.entity.EntityItemUtils.getMultiLineInfo(record),
            requiredLabel: record.get('showRequired') ? i18n('Requires a value') : ''
        });
    },

    inlineEditor: function() {
        var record = this.getRecord();
        var editor = record.get('editor');
        return editor && (!editor.dependentLookupCode && MobileUI.components.list.items.MPropertyListItem.inlineTypes.indexOf(editor.type) !== -1);
    },

    propertyAfterLayout: function() {
        var record = this.getRecord();
        var item = this;
        var value = record.get('label');
        var element;
        var editorImpl;

        function updateTextareaHeight(editor) {
            var textarea = editor.element.down('textarea').dom;
            textarea.style.height = '21px';

            var height = textarea.scrollHeight;
            var delta = height - textarea.offsetHeight;

            textarea.style.height = height + 'px';
            editor.setHeight(height);
            item.dataview.setHeight(item.dataview.getHeight() + delta);
        }

        function depth(lookup) {
            return lookup.parent ? 1 + depth(lookup.parent) : 0;
        }

        function flattenTree(tree, lookup) {
            var isLeaf = !lookup.children;
            if (lookup.displayName || lookup.name) {
                var value = lookup.name;
                var text = lookup.displayName || value;

                if (text !== value) {
                    text += ' (' + value + ')';
                }

                tree.push({
                    html: '<span style="' + (isLeaf ? '' : 'color: #999;') +
                    ' white-space:nowrap; padding-left: ' + (depth(lookup) - 1) + 'em">' + text + '</span>',
                    text: text,
                    value: isLeaf ? value : null
                });
            }
            if (!isLeaf) {
                lookup.children.reduce(flattenTree, tree);
            }
            return tree;
        }

        function validate(value, type) {
            if ((value !== '') && !MobileUI.core.util.Validator.isValid(value, type)) {
                editorImpl.addCls('invalid');
            }
            else {
                editorImpl.removeCls('invalid');
            }
        }

        var valueChangedCallback = function(value) {
            var args = arguments;
            setTimeout(function() {
                record.set('label', value);
                if (editor.valueChangedCallback) {
                    editor.valueChangedCallback.apply(editor.valueChangedContext, args);
                }
            }, 200);
        };

        var editor = record.get('editor');
        if (this.inlineEditor()) {
            var Validator = MobileUI.core.util.Validator;
            var type = editor.type;
            var isLink = false;

            if ((type === 'String') && editor.values) {
                type = 'Enum';
            }
            else if (editor.dependentLookupCode) {
                type = 'DependentLookup';
            }
            else if (editor.lookupCode) {
                type = 'Lookup';
            }

            if ((type === 'URL') || (type === 'Blog URL') || (type === 'Image URL')) {
                isLink = true;
            }

            switch (type) {
                case 'String':
                case 'StringNotTokenized':
                case 'URL':
                case 'Blog URL':
                case 'Image URL':
                case 'SSN':
                case 'Id card (Passport)':
                case 'SIC-code':
                case 'NAICS-code':
                case 'Email domains':
                case 'Ticker symbol':
                case 'CIK id':
                case 'Stock exchange':
                case 'GeoLocation':
                    editorImpl = Ext.create('Ext.field.Text', {
                        cls: 'rl-inline-editor',
                        value: value,
                        listeners: {
                            change: function(editor, value) {
                                if (isLink) {
                                    if (value.toLowerCase().indexOf('www.') === 0) {
                                        value = 'http://' + value;
                                    }
                                }
                                valueChangedCallback(value, item, editor);
                            }
                        }
                    });
                    validate(value, isLink ? Validator.types.url : 'string');
                    break;

                case 'Long':
                case 'Int':
                case 'Number':
                case 'Float':
                    editorImpl = Ext.create('MobileUI.components.list.editors.NumberEditor', {
                        cls: 'rl-inline-editor',
                        value: value,
                        listeners: {
                            change: function(editor, value) {
                                value = String(value);
                                valueChangedCallback(value, item, editor);
                            }
                        }
                    });
                    validate(value, (type === 'Int' || type === 'Integer' || type === 'Long') ? Validator.types.integer :
                        ((type === 'Dollar') ? Validator.types.dollar : Validator.types.floatingPoint));
                    break;

                case 'Date':
                    editorImpl = Ext.create('Ext.field.Text', {
                        cls: 'rl-inline-editor',
                        value: value,
                        listeners: {
                            change: function(editor, value) {
                                valueChangedCallback(value, item, editor);
                            }
                        },
                        component: {type: 'date'},
                        clearIcon: false
                    });
                    break;

                case 'Timestamp':
                    editorImpl = Ext.create('MobileUI.components.list.editors.TimestampEditor', {
                        cls: 'rl-inline-editor',
                        value: value,
                        listeners: {
                            change: function(editor, value) {
                                value = String(value);
                                valueChangedCallback(value, item, editor);
                            }
                        }
                    });
                    break;

                case 'Boolean':
                    var useTranslated = i18n('Yes').length <= 3 && i18n('No').length <= 3;
                    editorImpl = Ext.create('MobileUI.components.TripleToggle', {
                        yes: useTranslated ? i18n('Yes') : 'Yes',
                        no: useTranslated ? i18n('No') : 'No',
                        value: (value === 'true') ? true : ((value === 'false') ? false : undefined),
                        listeners: {
                            change: function(value) {
                                value = value === true ? 'true' : (value === false ? 'false' : '');
                                valueChangedCallback(value, item, editor);
                            }
                        },
                        style: {
                            'float': 'right',
                            marginRight: '1em'
                        }
                    });
                    break;

                case 'Blob':
                    editorImpl = Ext.create('Ext.field.TextArea', {
                        cls: 'rl-inline-editor',
                        scrollable: {direction: 'vertical', directionLock: true},
                        height: '21px',
                        value: value,
                        listeners: {
                            change: function(editor, value) {
                                valueChangedCallback(value, item, editor);
                            },
                            keyup: updateTextareaHeight
                        }
                    });

                    setTimeout(function() {updateTextareaHeight(editorImpl);}, 0);
                    break;

                case 'Enum':
                    editorImpl = Ext.create('Ext.field.Select', {
                        cls: 'rl-inline-editor',
                        options: editor.values.map(function(value) {
                            return {text: i18n(value), value: value};
                        }),
                        value: value || null,
                        forceSelection: false,
                        autoSelect: false,
                        listeners: {
                            change: function(select, value) {
                                if (value !== null) {
                                    valueChangedCallback(value, item, editor);
                                }
                            }
                        },
                        defaultPhonePickerConfig: {
                            cls: 'rl-inline-editor-list'
                        },
                        defaultTabletPickerConfig: {
                            cls: 'rl-inline-editor-list'
                        }
                    });
                    break;
                case 'Lookup':
                    var lookups = MobileUI.core.Lookups.getLookups(editor.lookupCode).tree || {};
                    lookups = lookups && lookups.children || [];
                    lookups = lookups.reduce(flattenTree, []);
                    var lookupManager = MobileUI.core.session.Session.getLookupsManager();
                    editorImpl = Ext.create('MobileUI.components.StyledSelect', {
                        cls: 'rl-inline-editor',
                        options: lookups,
                        value: value || null,
                        forceSelection: false,
                        autoSelect: false,
                        useHtml: true,
                        listeners: {
                            change: function(select, value) {
                                if (item && !item.isDestroyed) {
                                    valueChangedCallback(value || '', item, editor);
                                    var userData = item.getRecord().get('userData');
                                    MobileUI.core.session.Session.getLookupsManager().touchEditor(userData ? userData.editingUri : '');
                                }
                            }
                        },
                        defaultPhonePickerConfig: {
                            cls: 'rl-inline-editor-list'
                        },
                        defaultTabletPickerConfig: {
                            cls: 'rl-inline-editor-list'
                        }
                    });
                    var userData = item.getRecord().get('userData');
                    var supportsAutoPopulate = lookupManager.isSupportAutoPopulate(editor.uri);
                    var isNonTouched = !lookupManager.isTouched(userData ? userData.editingUri : '');
                    if (editor.valueChangedCallback && Ext.isEmpty(value) && lookups.length === 1 && supportsAutoPopulate && isNonTouched){
                        editorImpl.setValue(lookups[0].value);
                    }
                    break;

                case 'DependentLookup':
                    editorImpl = Ext.create('MobileUI.components.DependentLookupInline', {
                        cls: 'rl-inline-editor',
                        lookupCode: editor.dependentLookupCode,
                        attrTypeUri: editor.uri,
                        options: [],
                        localizeLabel: record.get('localizeLabel') || null,
                        value: value || null,
                        forceSelection: false,
                        editingUri: record.get('userData').editingUri || editor.uri,
                        session: MobileUI.core.session.Session,
                        autoSelect: false,
                        useHtml: true,
                        listeners: {
                            change: function(select, value) {
                                var innerRecord = select.getRecord();
                                if (innerRecord) {
                                    var localizeLabel = innerRecord.get(select.getDisplayField());
                                    if (localizeLabel) {
                                        record.set('localizeLabel', localizeLabel);
                                    }
                                }
                                valueChangedCallback(value || '', item, editor);
                            }
                        },
                        defaultPhonePickerConfig: {
                            cls: 'rl-inline-editor-list'
                        },
                        defaultTabletPickerConfig: {
                            cls: 'rl-inline-editor-list'
                        }
                    });
                    break;
            }

            element = this.element.down('.handler-editor');
            element.setHtml('');
            element = element.dom || element;
            editorImpl.setRenderTo(element);
        }
        var showRequired = false;
        if (!element) {
            element = this.element.down('.handler-editor');
            element = element.dom || element;
        }

        if (MobileUI.core.CardinalityChecker.isRequired(record.get('editor')) && MobileUI.core.session.Session.getValidator().collectHeaderErrors(record.get('editor').uri) && !this.placeHolder) {
            var showCover = (editor && editor.type === 'String') ? (value || '').trim().length === 0 : !value;
            if (showCover) {
                var cover = Ext.create('Ext.Panel', {
                    cls: 'rl-required-cover',
                    html: i18n('Requires a value')
                });
                cover.setRenderTo(element);
                element.style['minHeight'] = '25px';
                if (editorImpl) {
                    cover.element.dom.addEventListener('click', function () {
                        cover.setHidden(true);
                        editorImpl.focus();
                    });
                }
                showRequired = true;
                if (record.get('scrollToThis')){
                    var scrollTop = element.offsetParent.offsetTop + element.offsetTop;
                    this.dataview.fireEvent('scrollRequired', scrollTop);
                }
            }
        }
        if (!showRequired){
            element = this.element.down('.rl-required-cover');
            if (element && element.dom){
                element.dom.parentNode.removeChild(element.dom);
            }
        }
    }
});
