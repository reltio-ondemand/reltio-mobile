/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MRelationsListItem', {
    requires: ['MobileUI.core.util.Util'],
    initialize: function ()
    {
        this.callParent(arguments);

        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/up-active.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/up.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/down-active.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/down.svg');
    },

    statics: {
        templates: {
            tablet: new Ext.XTemplate(
                '<div class="rl-item rl-relations-item handler-root {suggested}">' +
                '   <tpl if="avatar != null || defaultAvatar">' +
                '       <div class="rl-avatar">' +
                '           <div style="background-image: url({avatar}), url({defaultAvatar})"></div>' +
                '       </div>' +
                '   </tpl>' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-title">{label}</div>' +
                '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
                '       <tpl if="tags">' +
                '          <div class="rl-tag-list">' +
                '               <tpl for="tags">' +
                '                   <div class="rl-tag" style="background-color:{color}; color: {textColor || \'\'}">{value}</div>' +
                '               </tpl>' +
                '          </div>' +
                '       </tpl>' +
                '   </div>' +
                '   <button class="{disabled} positive-button rl-fixed-size rl-align-center-self">' +
                '       <img src="{positiveButtonIcon}" />' +
                '       <div>{positiveValue}</div>' +
                '   </button>' +
                '   <button class="{disabled} negative-button rl-fixed-size rl-align-center-self">' +
                '       <img src="{negativeButtonIcon}" />' +
                '       <div>{negativeValue}</div>' +
                '   </button>' +
                '   <div class="score rl-fixed-size rl-align-center-self">{score}</div>' +
                '   <button class="{hideRating} comment-button rl-fixed-size rl-align-center-self">' +
                '       <img src="resources/images/reltio/relations/comment.svg"/>' +
                '   </button>' +
                '   <tpl if="!suggested">' +
                '       <div class="rl-info-button handler-info">' +
                '           <div></div>' +
                '       </div>' +
                '   </tpl>' +
                '</div>'),

            phone: new Ext.XTemplate(
                '<div class="rl-item rl-relations-item handler-root {suggested}">' +
                '   <tpl if="avatar != null || defaultAvatar">' +
                '       <div class="rl-avatar">' +
                '           <div style="background-image: url({avatar}), url({defaultAvatar})"></div>' +
                '       </div>' +
                '   </tpl>' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-title">{label}</div>' +
                '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
                '           <div class="rl-hbox">' +
                '               <button class="{disabled} positive-button phone rl-fixed-size">' +
                '                   <img src="{positiveButtonIcon}" />' +
                '                   <div>{positiveValue}</div>' +
                '               </button>' +
                '               <button class="{disabled} negative-button phone rl-fixed-size">' +
                '                   <img src="{negativeButtonIcon}" />' +
                '                   <div>{negativeValue}</div>' +
                '               </button>' +
                '               <div class="score phone rl-fixed-size rl-align-center-self">{score}</div>' +
                '           </div>' +
                '       <tpl if="tags">' +
                '          <div class="rl-tag-list">' +
                '               <tpl for="tags">' +
                '                   <div class="rl-tag" style="background-color:{color}; color: {textColor || \'\'}">{value}</div>' +
                '               </tpl>' +
                '          </div>' +
                '       </tpl>' +
                '   </div>' +
                '   <button class="{hideRating} comment-button rl-fixed-size">' +
                '       <img src="resources/images/reltio/relations/comment.svg"/>' +
                '   </button>' +
                '   <tpl if="!suggested">' +
                '       <div class="rl-info-button handler-info">' +
                '           <div></div>' +
                '       </div>' +
                '   </tpl>' +
                '</div>')
        },

        fields: ['showVote', 'avatar', 'defaultImage', 'label', 'secondaryLabel', 'tags',
            'userVote', 'positive', 'negative', 'score', 'showVote', 'suggested',
            /*used externally:*/ 'children', 'comment']
    },

    /**
     * @protected
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    relationsInfoLayout: function (record) {
        if (!record.get('showVote')) {
            this.registerHandler('positive-button', 'emptyFn');
            this.registerHandler('negative-button', 'emptyFn');
        } else {
            this.registerHandler('positive-button', 'positive', true);
            this.registerHandler('negative-button', 'negative', true);
        }
        this.registerHandler('comment-button', 'comment', true);
        this.registerHandler('handler-info', 'selectItem');
        this.registerHandler('handler-root', Ext.emptyFn);
        this.registerHandler('rl-title', 'entityTap');

        var template = Ext.os.is.Phone ?
            MobileUI.components.list.items.MRelationsListItem.templates.phone:
            MobileUI.components.list.items.MRelationsListItem.templates.tablet,
            userVote = record.get('userVote'),
            disabled, hideRating;
        if (record.get('showVote') !== null && record.get('showVote') !== undefined) {
            disabled = record.get('showVote') === false ? 'rl-disabled': '';
            hideRating = '';
        } else {
            disabled = 'rl-hidden';
            hideRating = 'rl-hidden';
        }

        return template.apply({
            avatar: record.get('avatar') || record.get('defaultImage'),
            defaultAvatar: record.get('defaultImage'),
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            tags: (record.get('tags') || []).map(this.i18nTag),
            positiveButtonIcon: userVote >= 3 ?
                'resources/images/reltio/relations/up-active.svg' :
                'resources/images/reltio/relations/up.svg',
            positiveValue: i18n(record.get('positive') || 0),
            negativeButtonIcon: (userVote >= 1 && userVote < 3) ?
                'resources/images/reltio/relations/down-active.svg' :
                'resources/images/reltio/relations/down.svg',
            negativeValue: i18n(record.get('negative') || 0),
            score: i18n(record.get('score')),
            disabled: disabled,
            hideRating: hideRating,
            suggested: record.get('suggested') && 'suggested'
        });
    }
});
