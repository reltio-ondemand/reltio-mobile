/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MRelationsTreeItem', {
    requires: ['MobileUI.core.util.Util'],

    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item rl-relations-tree-item" style="padding-left: {offset}px">' +
            '   <tpl if="avatar != null || defaultAvatar">' +
            '       <div class="rl-avatar">' +
            '           <div style="background-image: url({avatar}), url({defaultAvatar})"></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="rl-title {selected}">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '   </div>' +
            '   <tpl if="state || stateIcon">' +
            '           <button class="handle-expand-button expand-button rl-fixed-size">' +
            '               <img class="twohops-item-{state}" src="{stateIcon}"</img>' +
            '           </button>' +
            '   </tpl>' +
            '</div>'),
        fields: ['avatar', 'defaultAvatar', 'label', 'secondaryLabel', 'offset', 'state', 'selected', 'stateIcon',
            /*Used externally: */ 'hidden', 'children', 'definition']
    },

    /**
     * @protected
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    relationsTreeLayout: function (record) {
        this.registerHandler('handle-expand-button', 'expand', true);
        return MobileUI.components.list.items.MRelationsTreeItem.template.apply({
            avatar: record.get('avatar') || record.get('defaultImage'),
            defaultAvatar: record.get('defaultImage'),
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            offset: record.get('offset'),
            state: record.get('state'),
            selected: record.get('selected'),
            stateIcon: record.get('stateIcon')
        });
    }
});
