/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MSavedSearchListItem', {

    requires: ['MobileUI.components.list.model.EListModel'],

    statics: {
        templates: {
            tablet: new Ext.XTemplate(
                '<div class="rl-item stick-right handler-root">' +
                '   <div class="rl-vbox rl-ss-first-column">' +
                '       <div class="rl-title">{label}</div>' +
                '       <div class="rl-text">{user}</div>' +
                '       <div class="rl-tag-list">' +
                '           <tpl for="tags">' +
                '               <div class="rl-tag" style="background-color:{color}">{value}</div>' +
                '           </tpl>' +
                '       </div>' +
                '   </div>' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-caption">{filtersCaption}</div>' +
                '       <div class="rl-ss-searchfilter searchfilter">{filters}</div>' +
                '   </div>' +
                '   <div class="rl-vbox rl-ss-total-column rl-align-center">' +
                '       <div class="rl-caption">{totalCaption}</div>' +
                '       <div class="rl-score">{score}</div>' +
                '   </div>' +
                '   <div class="rl-edit-button rl-edit-button-tablet">' +
                '       <div class="fake-button-white handler-edit-button"></div>' +
                '   </div>' +
                '</div>'),

            phone: new Ext.XTemplate(
                '<div class="rl-item stick-right handler-root">' +
                '   <div class="rl-vbox">' +
                '   <div class="rl-title">{label}</div>' +
                '   <div class="rl-text">{user}</div>' +
                '   <div class="rl-tag-list">' +
                '       <tpl for="tags">' +
                '           <div class="rl-tag" style="background-color:{color}">{value}</div>' +
                '       </tpl>' +
                '   </div>' +
                '   <div class="rl-caption rl-ss-filter-caption">{filtersCation}</div>' +
                '       <div class="rl-ss-searchfilter searchfilter">{filters}</div>' +
                '   </div>' +
                '   <div class="rl-vbox rl-ss-total-column rl-align-center">' +
                '       <div class="rl-caption">{totalCaption}</div>' +
                '       <div class="rl-score">{score}</div>' +
                '   </div>' +
                '   <div class="rl-edit-button">' +
                '       <div></div>' +
                '   </div>' +
                '</div>')
        },

        fields: [ 'label', 'secondaryLabel', 'tags', 'score', 'info' ]
    },


    /**
     * @protected
     * Method returns saved search layout
     * @param record {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    savedSearchLayout: function(record) {
        var template = Ext.os.is.Phone ?
            MobileUI.components.list.items.MSavedSearchListItem.templates.phone :
            MobileUI.components.list.items.MSavedSearchListItem.templates.tablet,
            tags = [],
            userData = record.get('userData');

        if (userData.isPublic) {
            tags.push({value: 'Public'});
        }

        if (userData.isGlobalFilter) {
            tags.push({value: 'Global filter', color: '#03B5FF'});
        }

        this.registerHandler('handler-root', 'selectItem');
        return template.apply({
            label: this.convertText(record.get('label')),
            user: this.convertText(record.get('secondaryLabel')),
            tags: tags.map(this.i18nTag),
            score: i18n(record.get('score')),
            filters: MobileUI.components.SearchFilter.generateHtml(record.get('info')),
            filtersCaption: i18n('Filters'),
            totalCaption: i18n('Total')
        });
    }
});
