/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MSelectImageListItem', {
    requires: ['MobileUI.core.util.Util'],
    initialize: function ()
    {
        this.callParent(arguments);

        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/up-active.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/up.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/down-active.svg');
        MobileUI.core.util.Util.preloadImage('resources/images/reltio/relations/down.svg');
    },

    statics: {
        templates: new Ext.XTemplate(
                '<div class="rl-item rl-relations-item handler-root">' +
                '   <tpl if="avatar != null">' +
                '       <div class="rl-big-avatar {state}">' +
                '           <div class = "rl-avatar-img" style="background-image: url({avatar})"></div>'+
                '           <tpl if="state">'+
                '               <div class="rl-selected-image" style="background-image: url(resources/images/reltio/profile/selectedImage.svg)"></div>'+
                '           </tpl>'+
                '       </div>' +
                '   </tpl>' +
                '   <div class="rl-vbox rl-align-center">' +
                '       <div class="rl-title">{label}</div>' +
                '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
                '       <div class="rl-subtext rl-collapsible">{thirdLabel}</div>' +
                '   </div>' +
                '</div>'),

        fields: ['avatar', 'state', 'label', 'secondaryLabel', 'thirdLabel', 'uri']
    },

    /**
     * @protected
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    selectImageLayout: function (record) {

        this.registerHandler('handler-root', 'selectItem');

        var template = MobileUI.components.list.items.MSelectImageListItem.templates;

        return template.apply({
            avatar: record.get('avatar'),
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            thirdLabel: this.convertText(record.get('thirdLabel')),
            state: record.get('state')
        });
    }
});
