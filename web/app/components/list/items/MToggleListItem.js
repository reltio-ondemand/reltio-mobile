/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MToggleListItem', {
    requires: ['MobileUI.core.util.Util'],
    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item">' +
            '   <div class="rl-text rl-align-center-self" style="padding-left: {offset}px">{label}</div>' +
            '   <div class="rl-align-center-self rl-fixed-size handle-toggle"></div>' +
            '</div>'),

        fields: ['lastInGroup', 'label', 'property', 'editor', 'state', 'offset']
    },

    /**
     * @param record {Object}
     * @return {String} html
     */
    toggleLayout: function(record) {
        this.hideSeparator = record.get('lastInGroup');

        return MobileUI.components.list.items.MToggleListItem.template.apply({
            label: i18n(this.convertText(record.get('label'))),
            offset: record.get('offset')
        });
    },

    toggleAfterLayout: function() {
        var record = this.getRecord();
        var toggle = Ext.create('Ext.field.Toggle', {
            value: record.get('property') ? 1 : 0,
            listeners: {
                change: record.get('editor')
            },
            disabled: record.get('state') === 'disabled'
        });
        //todo: stop re-render it each time
        var element = this.element.down('.handle-toggle');
        element.setHtml('');
        element = element.dom || element;
        toggle.setRenderTo(element);
    }
});
