/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.items.MTwoHopsConnectionsListItem', {
    requires: ['MobileUI.core.util.Util'],
    statics: {
        template: new Ext.XTemplate(
            '<div class="rl-item rl-twohops-item handler-root" style="padding-left: {offset}px">' +
            '   <tpl if="avatar != null || defaultAvatar">' +
            '       <div class="rl-avatar">' +
            '           <div style="background-image: url({avatar}), url({defaultAvatar})"></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <div class="rl-vbox rl-align-center">' +
            '       <div class="handler-title rl-title">{label}</div>' +
            '       <div class="rl-text rl-collapsible">{secondaryLabel}</div>' +
            '       <tpl if="tags">' +
            '          <div class="rl-tag-list">' +
            '               <tpl for="tags">' +
            '                   <div class="rl-tag" style="background-color:{color}; color: {textColor || \'\'}">{value}</div>' +
            '               </tpl>' +
            '          </div>' +
            '       </tpl>' +
            '       <tpl if="this.isExpanded(state)">' +
            '           <div class="rl-inner-filter" data-name={actionUrl} data-index={index}>' +
            '           </div>' +
            '       </tpl>' +
            '   </div>' +
            '   <tpl if="showInfo">' +
            '       <div class="rl-info-button handler-info">' +
            '           <div></div>' +
            '       </div>' +
            '   </tpl>' +
            '   <tpl if="state || stateIcon">' +
            '           <button class="expand-button rl-fixed-size">' +
            '               <img class="twohops-item-{state}" src="{stateIcon}"</img>' +
            '           </button>' +
            '   </tpl>' +
            '</div>',
            {
                isExpanded: function(state)
                {
                    return state === 'expanded';
                }
            }
        ),
        fields: ['state', 'offset', 'avatar', 'defaultImage', 'label', 'secondaryLabel',
            'tags', 'stateIcon', 'state', 'actionLabel', 'index']
    },

    /**
     * @protected
     * @param {MobileUI.components.list.model.EListModel} record
     * @return {String} html
     */
    twoHopsInfoLayout: function (record)
    {
        this.registerHandler('expand-button', function(event) {
            var record = event.record,
                item = event.item,
                store = item.dataview.getStore(),
                index = store.indexOf(record);

            if (record.get('state') === 'collapsed')
            {
                item.dataview.fireEvent('expand', {
                    record: record,
                    index: index,
                    element: event.element
                });
            }
            else
            {
                item.dataview.fireEvent('collapse', {
                    record: record,
                    index: index,
                    element: event.element
                });
            }
        }, true);

        this.registerHandler('handler-root', Ext.emptyFn);
        this.registerHandler('handler-info', 'showRelationAttributes');
        this.registerHandler('handler-title', 'selectItem');

        return MobileUI.components.list.items.MTwoHopsConnectionsListItem.template.apply({
            offset: 20 + record.get('offset'),
            avatar: record.get('avatar') || record.get('defaultImage'),
            defaultAvatar: record.get('defaultImage'),
            label: this.convertText(record.get('label')),
            secondaryLabel: this.convertText(record.get('secondaryLabel')),
            tags: (record.get('tags') || []).map(this.i18nTag),
            stateIcon: record.get('stateIcon'),
            state: record.get('state'),
            actionUrl: record.get('actionLabel'),
            index: record.get('index'),
            showInfo: Ext.isString(record.get('userData'))
        });
    }
});
