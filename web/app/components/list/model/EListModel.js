/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.components.list.model.EListModel', {
    extend: 'Ext.data.Model',

    constructor: function() {
        if (!MobileUI.components.list.model.EListModel.fields) {
            var items = MobileUI.components.list.items;
            var fields = {};

            MobileUI.components.list.model.EListModel.generalFields.forEach(function(field) { fields[field] = field; });

            Object.keys(items).
                map(function(key) { return items[key]; }).
                forEach(function(item) {
                    (item.fields || []).forEach(function(field) { fields[field] = field; });
                });

            MobileUI.components.list.model.EListModel.fields =
                    Object.keys(fields).
                    map(function(field) { return {name: field};});
        }
        this.setFields(MobileUI.components.list.model.EListModel.fields);
        this.callParent(arguments);
    },

    statics: {
        /** @private */
        generalFields: [ 'type', 'userData', 'disableSwipe', 'allowedSwipeElements', 'disableButtons'],

        /** @private */
        fields: null
    }
});
