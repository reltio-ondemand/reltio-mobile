/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.AddComments', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            radioFields: 'addcommentsview radiofield[name=addComments-comment]',
            main: 'addcommentsview',
            closeButton: 'button[name=addComments-closeButton]',
            comments: 'addcommentsview fieldset[name=comments]'
        },
        control: {
            radioFields: {
                check: 'onRadioCheck'
            },
            closeButton: {
                tap: 'onCloseButtonTap'
            }
        },
        views: [
            'MobileUI.view.AddComments'
        ]
    },
    preventClose: false,
    entityUrl: null,

    /**
     * @private
     */
    onRadioCheck: function(){
        var comment = this.getMain().down('radiofield[name=addComments-comment]').getGroupValue(),
            action;
        action = {
            value: 1,
            comment: comment || null,
            uri: this.relation
        };
        this.setComment(action, this.onCloseButtonTap);
    },
    /**
     * @private
     */
    onCloseButtonTap: function ()
    {
        if (this.preventClose)
        {
            return;
        }
        var url = 'relations/' +
                MobileUI.core.util.HashUtil.encodeUri(this.facet + '/' + this.entityUrl);
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
        MobileUI.core.Navigation.redirectTo(url, false);
    },
    /**
     * Method sets params to comment controller
     * @param params {Object} params
     */
    setParams: function (params)
    {
        if (params.length > 1)
        {
            this.relation = params.splice(params.length - 2, params.length - 1).join('/');
            this.entityUrl = params.splice(params.length - 2, params.length - 1).join('/');
            this.facet = params.splice(1, params.length - 1).join('/');
            var facet = MobileUI.core.Services.getConfigurationManager().getExtensionById(this.facet);
            var comments = Ext.isEmpty(facet) ? null: facet.negativeComments;
            var items = MobileUI.controller.Relations.createTheCommentItems((
                    Ext.isArray(comments) ? comments : ['Has left the practice',
                        'Has moved away',
                        'Has passed away',
                        'Has retired',
                        'Other'
                    ]).map(function (item) {
                    return {label: i18n(item), value: item};
                })
            );
            this.getComments().setItems(items);

        }
    },
    setComment: function (action, callback)
    {
        MobileUI.core.session.Session.addRelationRating(action.uri, action.value, action.comment, callback, null, this);
    }
});
