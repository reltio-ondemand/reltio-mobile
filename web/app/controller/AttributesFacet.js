/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.AttributesFacet', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MAttributesFacets',
        'MobileUI.controller.MProfileBand',
        'MobileUI.controller.MEntityAction',
        'MobileUI.controller.MAddAttribute'
    ],
    mixins: {
        attributesFacet: 'MobileUI.controller.MAttributesFacets',
        profileBand: 'MobileUI.controller.MProfileBand',
        action: 'MobileUI.controller.MEntityAction',
        addAttribute: 'MobileUI.controller.MAddAttribute'
    },
    config: {
        refs: {
            profileBand: 'facetprofileband[name=attributesFacetProfileBand]',
            attributesFacet: 'elist[name=attributesFacetList]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            panel: 'panel[name=attributesfacet]'
        },
        control: {
            attributesFacet: {
                recalculate: 'recalculateHeight',
                edit: 'onComplexAttributeTap',
                selectItem:'onReferenceAttributeSelect'
            },
            panel: {
                pullRefresh: 'onAttributeFacetPullRefresh'
            },
            profileBand:{
                blockTap :'onBlockTap',
                favorited: 'onFavoriteTap'
            }
        },
        views: [
            'MobileUI.view.AttributesFacet'
        ]
    },
    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    this.getBackToolbarBtn().setHidden(false);
                }
            }
        }, this);
    }
});
