/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Base', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.dialogs.SavedSearches',
        'MobileUI.components.TypeAheadPopup'
    ],
    mixins: {
        menuToolbarButton: 'MobileUI.controller.MMenuToolbarButton'
    },

    config: {
        refs: {
            menuButton: 'button[name=menuBtn]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            globalFilterMenu: 'button[name=globalFilterMenu]',
            searchField: 'textfield[name=search]',
            searchBtn: 'button[name=searchBtn]'
        },
        control: {
            menuButton: {
                tap: 'onMenuButtonTap'
            },
            backToolbarBtn: {
                tap: 'onBackToolbarTap'
            },
            globalFilterMenu: {
                tap: 'onGlobalFilterMenuTap'
            },
            searchField: {
                focus: 'onFocus',
                keyup: 'onKeyUp',
                clearicontap: 'onClearButtonTap',
                initialize: 'updateKeyWord'
            },
            searchBtn: {
                tap: 'onToolbarSearch'
            }
        }
    },

    /** @private */
    globalFilterPopup: null,

    /**@type {MobileUI.components.TypeAheadPopup} @private */
    popup: null,

    launch: function() {
        MobileUI.core.search.SearchParametersManager.on('globalFilterChanged', this.onGlobalFilterChange, this);
        this.getApplication().getController('ViewActivator').on('activated', function () {
            var globalFilterMenu = this.getGlobalFilterMenu();
            if (globalFilterMenu) {
                globalFilterMenu.on('painted', this.onGlobalFilterMenuInitialized, this);
            }

            this.updateKeyWord();
        }, this);
    },

    /**
     * On back button tap
     */
    onBackToolbarTap: function ()
    {
        MobileUI.core.Navigation.back();
    },

    /** @private */
    onGlobalFilterMenuTap: function() {
        var globalFilter = MobileUI.core.search.SearchParametersManager.getGlobalFilter();
        if (globalFilter.uris.length === 0) {
            if (!this.popup || this.popup.isDestroyed) {
                this.popup = new MobileUI.components.Popup();
                this.popup.setWidth(240);
                this.popup.setMinHeight(60);
                this.popup.add({
                    xtype: 'component',
                    html: i18n('Global filter is not selected. Please select a Global Filter from %1.',
                        '<span href="#" class="global-filter-saved-searches-link">' + i18n('Saved Searches')+ '</span>'),
                    style: 'padding: 20px',
                    listeners: [
                        {
                            element: 'element',
                            delegate: 'span.global-filter-saved-searches-link',
                            event: 'tap',
                            fn: function() {
                                this.popup.hide();
                                MobileUI.controller.dialogs.SavedSearches.show();
                            },
                            scope: this
                        }
                    ]
                });
            }
            if (this.popup.isHidden() !== false)
            {
                this.popup.showBy(this.getGlobalFilterMenu(), 'tr-br');
            }
        }
        else {
            if (!MobileUI.core.SuiteManager.hasDialog('globalfilterdialog')) {
                MobileUI.core.SuiteManager.addDialog('globalfilterdialog', Ext.create('MobileUI.view.dialogs.GlobalFilter'));
            }
            MobileUI.core.SuiteManager.showDialog('globalfilterdialog');
        }
    },

    /** @private */
    onGlobalFilterMenuInitialized: function() {
        this.onGlobalFilterChange(MobileUI.core.search.SearchParametersManager.getGlobalFilter());
    },

    /** @private */
    onGlobalFilterChange: function(globalFilter) {
        var button = this.getGlobalFilterMenu();
        if (button) {
            var iconName = 'gf.svg';
            if (globalFilter.uris.length === 0) {
                iconName = 'gf-empty.svg';
            }
            else if (globalFilter.mute) {
                iconName = 'gf-mute.svg';
            }

            button.setIcon('resources/images/reltio/toolbar/' + iconName);
        }
    },

    /**
     * Method updates keyword in search
     */
    updateKeyWord: function ()
    {
        var keyword = MobileUI.core.search.SearchParametersManager.getTypeAhead();
        if (this.getSearchField())
        {
            this.getSearchField().setValue(keyword);
        }
    },
    /**
     * Focus listener
     * @protected
     */
    onFocus: function ()
    {
        this.value = this.getSearchField().getValue();
        if (this.value && this.value.length > 0)
        {
            this.showPopup(this.getSearchField());
            this.onTypeAheadSearch();
        }
    },
    /**
     * Key up listener
     * @protected
     */
    onKeyUp: function ()
    {
        this.value = this.getSearchField().getValue();
        MobileUI.core.search.SearchParametersManager.setTypeAhead(this.value);
        if (this.value && this.value.length > 0)
        {
            this.showPopup(this.getSearchField());
            this.onTypeAheadSearch();
        }
        else
        {
            this.clear();
        }
    },

    clear: function ()
    {
        this.hidePopup();
        this.clearPopup();
    },
    /**
     * Clear button tap listener
     * @protected
     */
    onClearButtonTap: function ()
    {
        this.clear();
        this.getSearchField().setValue('');
        MobileUI.core.search.SearchParametersManager.setTypeAhead(this.getSearchField().getValue());
        this.getSearchField().focus();
    },

    /**
     * @protected
     * Typeahead listener
     */
    onTypeAheadSearch: function ()
    {
        var search = {value: this.value};
        MobileUI.core.session.Session.typeAheadSearch(search, function (result)
        {
            var that = this;
            if (that.value !== that.self.value)
            {
                return;
            }
            result = MobileUI.core.Services.getSearchResultBuilder().buildContent(result, true);

            that.self.popup.setListData(result);
        }, function (e)
        {
            MobileUI.core.Logger.log(e);
        }, {self: this, value: this.value});
    },
    /**
     * Method clears all data
     */
    clearPopup: function ()
    {
        if (this.popup)
        {
            this.popup.clearData();
        }
    },
    /**
     * Method hides popup
     */
    hidePopup: function ()
    {
        if (this.popup)
        {
            this.popup.hide();
        }
    },
    /**
     * Method shows popup
     * @param target {Ext.Component} anchor component
     */
    showPopup: function (target)
    {
        if (Ext.isEmpty(this.popup) || this.popup.isDestroyed)
        {
            this.popup = Ext.create('MobileUI.components.TypeAheadPopup');
            this.popup.setTitle(i18n('Search'));
            this.popup.setWidth(320);
            this.popup.setMaxHeight(293);
            this.popup.setMinHeight(150);
            this.popup.on('selectItem', function (record)
            {
                this.getApplication().getController('Search').onSearchItemTap(record);
            }, this);
        }
        if (this.popup.isHidden() !== false)
        {
            this.popup.showBy(target, 'tc-bc');
            if (!this.getSearchField().getValue())
            {
                this.clearPopup();
            }
        }
    },

    onToolbarSearch: function ()
    {
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
        MobileUI.core.Navigation.redirectTo('search/typeahead', false);
    }
});
