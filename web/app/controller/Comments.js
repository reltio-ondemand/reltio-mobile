/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Comments', {
    extend: 'Ext.app.Controller',
    mixins: {
        comments: 'MobileUI.controller.MComments'
    },
    requires: ['MobileUI.controller.MComments'],
    config: {
        refs: {
            closeButton: 'button[name=comments-closeButton]',
            commentsList: 'elist[name=comments-list]'
        },
        control: {
            closeButton: {
                tap: 'onCloseButtonTap'
            }
        },
        views: [
            'MobileUI.view.Comments'
        ]
    },
    /**@type {String} @private */
    relation: null,
    /**@type {String} @private */
    facet: null,
    /**
     * @private
     */
    onCloseButtonTap: function ()
    {
        var url = 'relations/' + MobileUI.core.util.HashUtil.encodeUri(this.facet + '/' + this.entityUrl);
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
        MobileUI.core.Navigation.redirectTo(url, false);
    },

    /**
     * Method loads comments and shows it in list
     * @param params {Array}
     */
    showComments: function (params)
    {
        if (params.length > 1)
        {
            this.relation = params.splice(params.length - 2, params.length - 1).join('/');
            this.entityUrl = params.splice(params.length - 2, params.length - 1).join('/');
            this.facet = params.splice(1, params.length - 1).join('/');
            this.updateRelation();
        }
    },

    /**
     * Method return the relation url
     * @returns {String}
     */
    getRelation: function ()
    {
        return this.relation;
    }

});
