/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.ComplexAttributes', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfile',
        'MobileUI.controller.MComplexAttributes',
        'MobileUI.controller.MProfileBand'
    ],
    mixins: {
        baseProfile: 'MobileUI.controller.MProfile',
        complexAttributes: 'MobileUI.controller.MComplexAttributes',
        profileBand: 'MobileUI.controller.MProfileBand'
    },
    config: {
        refs: {
            profileBand: 'facetprofileband[name=complexProfileBand]',
            complexAttributesList: 'elist[name=complexAttributesList]',
            crumb: 'crumb[name=crumb]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            panel: 'panel[name=complexattributes]',
            cancelToolbarBtn: 'button[name=cancelComplexToolbarBtn]',
            doneToolbarBtn: 'button[name=doneToolbarBtn]'
        },
        control: {
            complexAttributesList: {
                edit:'onComplexAttributeTap',
                selectItem:'onReferenceAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                execute: 'onComplexListButtonExecute',
                'delete': 'onComplexAttributeDelete',
                entityTap: 'onProfileTitleTap'
            },
            panel: {
                pullRefresh: 'onComplexPullRefresh'
            },
            profileBand: {
                blockTap :'onBlockTap',
                favorited: 'onFavoriteTap'
            },
            cancelToolbarBtn: {
                tap: 'onCancelComplexToolbarBtn'
            },
            doneToolbarBtn: {
                tap: 'onDoneComplexToolbarBtn'
            }
        },
        views: [
            'MobileUI.view.ComplexAttributes',
            'MobileUI.view.EditableComplexAttributes'
        ]
    },

    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    var back = this.getBackToolbarBtn();
                    if (back) {
                        back.setHidden(false);
                    }
                }
            }
        }, this);
    }
});
