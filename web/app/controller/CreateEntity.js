/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.CreateEntity', {
    extend: 'Ext.app.Controller',
    statics: {
        ACTION_ID: 'com.reltio.plugins.CreateProfileAction',

        filterEntityTypes: function(entityTypes){
            entityTypes = entityTypes || [];
            var configuration = MobileUI.core.Services.getConfigurationManager(),
                extension = configuration.getExtensionById(MobileUI.controller.CreateEntity.ACTION_ID);
            if (!Ext.isEmpty(extension)) {
                if (!Ext.isEmpty(extension.includeUris)) {
                    entityTypes = entityTypes.filter(function (item) {
                        return extension.includeUris.indexOf(item.uri) !== -1;
                    });
                } else if (!Ext.isEmpty(extension.excludeUris)) {
                    entityTypes = entityTypes.filter(function (item) {
                        return extension.excludeUris.indexOf(item.uri) === -1;
                    });
                }
            }
            return entityTypes;
        },

        showChooseEntityTypeDialog: function(shouldInitLookups, listener){

            if (!MobileUI.core.SuiteManager.hasDialog('chooseattrtypeDialog')) {
                MobileUI.core.SuiteManager.addDialog('chooseattrtypeDialog', Ext.create('MobileUI.view.dialogs.ChooseAttributeType'));
            }
            var controller = MobileUI.app.getController('dialogs.ChooseAttributeType');

            var types = MobileUI.core.Metadata.getEntityTypes(function (item) { return !item['abstract']; });
            types = Object.keys(types).map(function(name) { return types[name]; });
            var md = MobileUI.core.PermissionManager.securityService().metadata;
            var canCreate =  types.some(function(item) { return md.canCreate(item); });

            if (canCreate) {
                controller.setTitleText(i18n('Create Profile'));
            }
            else {
                controller.setTitleText(i18n('Suggest New Profile'));
            }

            controller.setValue(null);
            controller.resetParseDisplayName();
            controller.setValueChangedHandler(listener, {shouldInitLookups: shouldInitLookups});
            types = MobileUI.core.Metadata.getEntityTypes(function (item) {
                return !item['abstract'] && MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(item);
            });
            types = Object.keys(types).map(function (name) {
                return types[name];
            });
            var items = MobileUI.controller.CreateEntity.filterEntityTypes(types)
                .map(function (item) {
                    return {value: item.label, uri: item.uri};
                });
            controller.setParseDisplayName(function(item){
                return item.value;
            });
            controller.setValues(items);

            MobileUI.core.SuiteManager.showDialog('chooseattrtypeDialog');
        }

    }
});
