/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.CustomViewController', {
    extend: 'Ext.app.Controller',
    mixins: {
        band: 'MobileUI.controller.MProfileBand'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.controller.MProfileBand',
        'MobileUI.core.Navigation',
        'MobileUI.components.MessageBox'
    ],
    config: {
        refs: {
            profileBand: 'facetprofileband[name=customViewProfileBand]',
            panel: 'panel[name=customViewPanel]',
            customViewComponent: 'customviewcomponent'
        },
        control: {
            profileBand: {
                blockTap: 'onBlockTap'
            }
        },
        views: [
            'MobileUI.view.CustomView'
        ]
    },

    showCustomView: function (params) {
        this.parsedParams = this.parseParams(params);
        var viewActivator = this.getApplication().getController('ViewActivator');
        if (viewActivator.waitForActivate()) {
            viewActivator.on('activated', function () {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1) {
                    this.showCustomViewInternal();
                }
            }, this, {single: true});
        }
        else {
            this.showCustomViewInternal();
        }
    },
    parseParams: function (params) {
        return {
            facetId: params[0],
            entityUrl: params.splice(1, 2).join('/')
        };
    },

    showCustomViewInternal: function () {
        if (MobileUI.core.session.Session.getEntityUri() === this.url) {
            this.processEntityRequest(MobileUI.core.session.Session.getEntity());
        } else {
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processEntityRequest, function (resp) {
                MobileUI.core.Logger.log(resp);
                if (resp) {
                    MobileUI.components.MessageBox.alert(i18n('Error'), resp.errorMessage);
                }
            }, this, false, true);
        }
    },
    processEntityRequest: function (entity) {
        var cm = MobileUI.core.Services.getConfigurationManager();
        var plugin = cm.getExtensionById(this.parsedParams.facetId);
        this.getCustomViewComponent().setParameters(plugin);
        this.fillBand(this.getProfileBand(), entity);
    }
});
