/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.EditRelationAttributes', {
    extend: 'Ext.app.Controller',
    mixins: {
        band: 'MobileUI.controller.MProfileBand',
        list: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        editable: 'MobileUI.controller.MEditableProfile',
        addAttribute: 'MobileUI.controller.MAddAttribute',
        'action': 'MobileUI.controller.MEntityAction'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.controller.MProfileBand',
        'MobileUI.core.Navigation',
        'MobileUI.controller.MEditableProfile',
        'MobileUI.controller.MEntityAction',
        'MobileUI.components.MessageBox'
    ],
    config: {
        refs: {
            editRelationAttributesList: 'elist[name=editRelationAttributesList]',
            profileBand: 'facetprofileband[name=editProfileBand]',
            saveButton: 'button[name=save-relation-button]',
            title: 'component[name=editRelationTitle]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            cancelEditRelationBtn: 'button[name=cancelEditRelation]',
            panel: 'panel[name=relationAttributes]',
            notificationToolbar: 'component[name=notificationToolbar]',
            notificationMessage: 'component[name=notificationMessage]'
        },
        control: {
            editRelationAttributesList: {
                edit: 'onComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete',
                execute: 'onListButtonExecute'
            },
            saveButton: {
                tap: 'onSaveButtonTap'
            },
            cancelEditRelationBtn: {
                tap: 'onCancelTap'
            }
        },
        views: [
            'MobileUI.view.EditRelationAttributes'
        ]
    },
    entity: null,
    connectedObject: null,
    relationAttributesEntity: null,
    parsedParams:null,

    onCancelTap: function(){
        MobileUI.components.MessageBox.show({
            title: i18n('Warning'),
            message: i18n('Are you sure you want to close the dialog?'),
            buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
            promptConfig: false,
            scope: this,
            fn: function (buttonId) {
                if (buttonId === 'yes') {
                    this.redirectBack();
                }
            }
        });
    },
    redirectBack: function(){
        var path;
        if (this.parsedParams.back === 'relation') {
            path = [this.parsedParams.relationId, this.parsedParams.entityUrl, this.parsedParams.relationUrl].join('/');
            MobileUI.core.Navigation.redirectForward('relation/' + MobileUI.core.util.HashUtil.encodeUri(path));
        }else if (this.parsedParams.back === 'twohopsconnection') {
            path = [this.parsedParams.relationId, this.parsedParams.entityUrl].join('/');
            MobileUI.core.Navigation.redirectForward('relations/twohopsconnection$' + MobileUI.core.util.HashUtil.encodeUri(path));
        } else {
            path = [this.parsedParams.relationId, this.parsedParams.entityUrl].join('/');
            MobileUI.core.Navigation.redirectForward('relations/' + MobileUI.core.util.HashUtil.encodeUri(path));
        }
    },
    /**
     * @private
     * method creates params from array of url params
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string, relationUrl: string}}
     */
    parseParams: function(params){
            return {
                relationId: params[0],
                entityUrl: params.splice(1, 2).join('/'),
                relationUrl: params.splice(1, params.length - 2).join('/'),
                back: params[params.length - 1]
            };
    },
    showEditRelation: function(params) {
        this.parsedParams = this.parseParams(params);
        var config =  MobileUI.core.Services.getConfigurationManager().getExtensionById(this.parsedParams.relationId);
        var caption = (config && config.caption) || '';
        this.getTitle().setHtml(i18n('Edit %1', Ext.util.Format.htmlEncode(i18n(caption))));
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedParams.entityUrl) {
            this.processRelationAttributes(MobileUI.core.session.Session.getEntity());
        }
        else {
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelationAttributes, function (resp) {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },
    /**
     * Method processes entity and fill band
     * @param entity {Object} entity object
     */
    processRelationAttributes: function (entity) {
        this.entity = entity;
        MobileUI.core.session.Session.getLookupsManager().initWithEntityType(entity.type);
        MobileUI.core.session.Session.getRelation(this.parsedParams.relationUrl, this.onRelationsResponse, function () {
            this.getPanel().setMasked(false);
        }, this);
        this.fillBand(this.getProfileBand(), entity);
    },
    /**
     * Relation response processor
     * @param response {Array} response
     */
    onRelationsResponse: function (response) {
        var relation = response[0];
        var relAttrType = Ext.clone(MobileUI.core.Metadata.getRelationType(relation.type)),
            config = MobileUI.core.Services.getConfigurationManager().getExtensionById(this.parsedParams.relationId);
        relAttrType.attributes = relAttrType.attributes || [];
        var hiddenAttributes = (config && config.hiddenRelationAttributes) || [];
        var relationEntity = MobileUI.core.entity.TemporaryEntity.createNewEntity(relation.type, relation,null, true, relation.uri);
        MobileUI.core.entity.TemporaryEntity.addEntity(relationEntity);
        this.relationAttributesEntity = relationEntity;
        var attribute;
        if (relation.startDate) {
            attribute = Ext.clone(MobileUI.core.relations.RelationsManager.ATTR_START_DATE);
            attribute.value = i18n(new Date(relation.startDate), MobileUI.core.I18n.DATE_INTERNAL);
            attribute.parentUri = this.relationAttributesEntity.uri;
            this.relationAttributesEntity.attributes.push(attribute);
        }
        if (relation.endDate) {
            attribute = Ext.clone(MobileUI.core.relations.RelationsManager.ATTR_END_DATE);
            attribute.value = i18n(new Date(relation.endDate), MobileUI.core.I18n.DATE_INTERNAL);
            attribute.parentUri = this.relationAttributesEntity.uri;
            this.relationAttributesEntity.attributes.push(attribute);
        }
        relAttrType.attributes.push(MobileUI.core.relations.RelationsManager.ATTR_START_DATE);
        relAttrType.attributes.push(MobileUI.core.relations.RelationsManager.ATTR_END_DATE);
        relAttrType.attributes = relAttrType.attributes.filter(function (item) {
            return !item.hidden && hiddenAttributes.indexOf(item.uri) === -1;
        });
        this.relationAttributesEntityStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(relAttrType, null, function (attrType) {
            return MobileUI.core.PermissionManager.securityService().metadata.canRead(attrType);
        }, true);
        this.connectedObject = [relation.startObject, relation.endObject].filter(function(item){
            return item.objectURI !== this.entity.uri;
        }, this);
        this.updateList(this.getEditRelationAttributesList(), this.relationAttributesEntity, this.relationAttributesEntityStructure, this.connectedObject);

    },
    /**
     * Method updates list
     * @param list {elist}
     * @param entity {Object} entity object
     * @param structure {Object} entity structure
     * @param connectedObject {entity} connected object
     */
    updateList: function (list, entity, structure, connectedObject) {
        var store,
            attributes = entity.attributes.filter(function(item){
            return item.parentUri === entity.uri;
        },this);
        var model = this.createModel(attributes, structure, entity, true);
        if (connectedObject.length > 0) {
            connectedObject = connectedObject[0];
            model.unshift({
                type: MobileUI.components.list.items.BaseListItem.types.actionProperty,
                label: connectedObject.label,
                disableSwipe: true,
                property: MobileUI.core.Metadata.getEntityType(connectedObject.type).label
            });
        }
        model = model.filter(MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInEditMode);
        store = list.getStore();
        store.clearData();
        store.add(model);
        list.refresh();
        this.updateListHeight(list);
    },
    /**
     * Method returns entity
     * @returns {Object}
     */
    getEditedEntity: function(){
        return this.relationAttributesEntity;
    },
    /**
     * Add button listneer
     * @param event {Object} event
     */
    onListButtonExecute: function(event){
        var store = this.getEditRelationAttributesList().getStore(),
            index, attributes;
        var uris = [MobileUI.core.relations.RelationsManager.ATTR_START_DATE, MobileUI.core.relations.RelationsManager.ATTR_END_DATE].map(function(item){
            return item.uri;
        });
        uris = uris.filter(function(item){
            return store.findBy(function (record)
            {
                var data = record.get('userData');
                if (Ext.isObject(data))
                {
                    var uri = data.editingUri || '';
                    return uri.indexOf('/'+item+'/') !== -1;
                }
                return false;
            }) !== -1;
        });
        var data = event.record.get('userData');
        if (data) {
            store = event.item.dataview.getStore();
            index = store.indexOf(event.record),
            attributes = data.attributes.filter(function(item){
               return uris.indexOf(item.uri) === -1;
            });
            var msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();
            this.showAddAttributeDialog(attributes, index, this.getEditRelationAttributesList(), this.relationAttributesEntity.uri, this.relationAttributesEntity.uri, this.relationAttributesEntity.uri, msg);
        }
    },
    /**
     * Item delete listener
     * @param record
     */
    onItemDelete: function(record) {
        var context = this;
        this.deleteAttribute.call(context, record);
        this.updateList(this.getEditRelationAttributesList(), this.relationAttributesEntity, this.relationAttributesEntityStructure, this.connectedObject);
    },
    /**
     * Save button listener
     */
    onSaveButtonTap: function () {
        this.validateLookups([this.getEditRelationAttributesList()])
            .then(function (result) {
                var validationPassed = MobileUI.core.entity.EntityUtils.validationPassed(result);
                if (validationPassed) {
                    this.onAfterAllValidations();
                }
            }.bind(this));
    },

    onAfterAllValidations: function(){
        var startDate = null,
            endDate = null;
        var filter = function (item)
        {
            if (item.uri.indexOf(MobileUI.core.relations.RelationsManager.ATTR_START_DATE.uri) !== -1)
            {
                if (item.value) {
                    startDate = MobileUI.core.I18n.parseDate(item.value, MobileUI.core.I18n.DATE_INTERNAL);
                    startDate = startDate && startDate.getTime();
                }
                return false;
            }
            if (item.uri.indexOf(MobileUI.core.relations.RelationsManager.ATTR_END_DATE.uri) !== -1)
            {
                if (item.value) {
                    endDate = MobileUI.core.I18n.parseDate(item.value, MobileUI.core.I18n.DATE_INTERNAL);
                    endDate = endDate && endDate.getTime();
                }
                return false;
            }
            return true;
        };

        var observer = Ext.create('MobileUI.core.Observer', {
            threadNames: ['deleteStart', 'deleteEnd', 'update'],
            listener: function () {
                MobileUI.core.StateManager.setState(MobileUI.controller.Relations.STATE_NAME, null);
                MobileUI.core.StateManager.setState(MobileUI.controller.RelationsTree.STATE_NAME, null);
                this.saveEntity(this.relationAttributesEntity, this.saveDone, this);
            },
            self: this
        });
        this.relationAttributesEntity.addedAttributes = (this.relationAttributesEntity.addedAttributes || []).filter(filter) ;
        this.relationAttributesEntity.editedAttributes = (this.relationAttributesEntity.editedAttributes || []).filter(filter) ;
        this.entity = this.relationAttributesEntity;
        var deleted = (this.relationAttributesEntity.deletedAttributes || []).filter(function(item){
            return !filter(item);
        });
        this.relationAttributesEntity.deletedAttributes = (this.relationAttributesEntity.deletedAttributes || []).filter(filter) ;
        var checkedThreads = ['deleteStart', 'deleteEnd'];
        deleted.forEach(function(item){
            if (item.uri.indexOf(MobileUI.core.relations.RelationsManager.ATTR_START_DATE.uri) !== -1){
                MobileUI.core.session.Session.removeActivenessAttribute(this.parsedParams.relationUrl, MobileUI.core.relations.RelationsManager.ATTR_START_DATE.uri, function(){
                    observer.checkThread('deleteStart');
                }, function(){
                    observer.checkThread('deleteStart');
                }, this);
                startDate = null;
                checkedThreads.splice(checkedThreads.indexOf('deleteStart'));
            }
            if (item.uri.indexOf(MobileUI.core.relations.RelationsManager.ATTR_END_DATE.uri) !== -1){
                MobileUI.core.session.Session.removeActivenessAttribute(this.parsedParams.relationUrl, MobileUI.core.relations.RelationsManager.ATTR_END_DATE.uri, function(){
                    observer.checkThread('deleteEnd');
                }, function(){
                    observer.checkThread('deleteEnd');
                }, this);
                endDate = null;
                checkedThreads.splice(checkedThreads.indexOf('deleteEnd'));
            }
        }, this);
        checkedThreads.forEach(function(threadName){
            observer.checkThread(threadName);
        });
        var obj = {};
        if (startDate){
            obj[MobileUI.core.relations.RelationsManager.ATTR_START_DATE.uri] = startDate;
        }
        if (endDate){
            obj[MobileUI.core.relations.RelationsManager.ATTR_END_DATE.uri] = endDate;
        }
        if (startDate || endDate){
            MobileUI.core.session.Session.updateActivenessAttribute(this.parsedParams.relationUrl, obj, function(){
                observer.checkThread('update');
            },function(){
                observer.checkThread('update');
            });
        } else {
            observer.checkThread('update');
        }
    },
    /**
     * After save action
     */
    saveDone: function() {
        MobileUI.core.entity.TemporaryEntity.removeAllEntities();
        MobileUI.core.session.Session.resetCurrentEntity();
        this.redirectBack();
    },
    /**
     * Before complex editor opened listener
     */
    beforeComplexEditorOpened: function(){
        this.beforeListener = true;
        MobileUI.core.SuiteManager.on('hideDialogs', function() {
            if (this.beforeListener){
                MobileUI.core.entity.TemporaryEntity.clearStateFrom(this.parsedParams.relationUrl);
                this.relationAttributesEntity = MobileUI.core.entity.TemporaryEntity.getEntity(this.parsedParams.relationUrl);
                this.updateList(this.getEditRelationAttributesList(), this.relationAttributesEntity, this.relationAttributesEntityStructure, this.connectedObject);
            }
        }, this, {single: true, delay: 700});
    },
    /**
     * Complext attribute selece listener
     * @param record {MobileUI.components.list.model.EListModel} record
     */
    onComplexAttributeSelect: function (record) {
        var context = this;
        context.originalUri = this.relationAttributesEntity.uri;
        context.parentUri = this.relationAttributesEntity.uri;
        context.entity = this.relationAttributesEntity;
        this.onComplexAttributeTap.call(context, record);
    }
});
