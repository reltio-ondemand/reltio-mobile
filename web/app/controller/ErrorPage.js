/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.ErrorPage', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.Navigation'],
    config: {
        refs: {
            infoLabel: 'label[name=result]'
        },
        views: [
            'MobileUI.view.ErrorPage'
        ]
    },

    showError: function (error) {
        error = error || {};
        var errorMsg = (error.code ? (error.code + ': ') : '') + (error.text || 'unknown error');
        this.getInfoLabel().setHtml('<span class="x-bold" style="display: block; text-align: left">' + i18n('Dear customer, problem occurred.') + '</span>' +
            '<span style="display: inline-block;">' + errorMsg + '</span>');
        MobileUI.core.session.Session.logout(Ext.emptyFn);
    }
});
