/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Feedback', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.core.Navigation',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.components.RemovableImage',
        'MobileUI.core.feedback.ZendeskProvider',
        'MobileUI.core.feedback.CustomProvider',
        'MobileUI.core.SuiteManager',
        'Ext.device.Device',
        'MobileUI.core.Services',
        'MobileUI.components.MessageBox'],
    statics: {
        ZENDESK: 'Zendesk',
        ZENDESK_REPORT: 'Bug',
        ZENDESK_ASK: 'Question',
        ZENDESK_REQUEST: 'Technical Task',
        ZENDESK_SUGGEST : 'Idea or Suggestion'
    },
    config: {
        refs: {
            cancelButton: 'button[name=feedback-cancel]',
            feedBackButton: 'button[name=feedback-send]',
            fileUpload: 'fileupload[name=file]',
            imagePanel: 'panel[name=imagepanel]',
            summary: 'textfield[name=summary]',
            ccs: 'component[name=email]',
            stepsToReproduce: 'textareafield[name=stepstoreproduce]',
            stepsToReproduceSeparator: 'component[name=stepstoreproduce-separator]',
            priority: 'component[name=priority]',
            prioritySeparator: 'component[name=priority-separator]',
            actualResults: 'textareafield[name=actualResults]',
            actualResultsSeparator: 'component[name=actualResults-separator]',
            expectedResults: 'textareafield[name=expectedResults]',
            expectedResultsSeparator: 'component[name=expectedResults-separator]',
            didItWork: 'component[name=didItWork]',
            didItWorkValue: 'component[name=didItWork-value]',
            didItWorkSeparator: 'component[name=didItWork-separator]',
            businessImpact: 'textfield[name=businessImpact]',
            businessImpactSeparator: 'component[name=businessImpact-separator]',
            dueDate: 'textfield[name=duedate]',
            reason: 'reltio-selectbox[name=reason]',
            description: 'textareafield[name=description]',
            rootPanel: 'feedbackview[name=feedbackview-root]',
            notificationToolbar: 'toolbar[name=feedback-notificationToolbar]',
            notificationMessage: 'component[name=feedback-notificationMessage]',
            notificationClose: 'component[name=feedback-closeNotificationBtn]'
        },
        control: {
            cancelButton: {
                tap: 'closeFeedback'
            },
            notificationClose:{
                tap: 'onCloseNotificationBtn'
            },

            feedBackButton: {
                tap: 'onFeedbackSend'
            },
            reason: {
                change: 'onReasonChanged'
            },
            description: {
                focus: 'onFocus',
                keyup: 'onFieldChanged'
            },
            summary: {
                focus: 'onFocus',
                keyup: 'onFieldChanged'
            },
            fileUpload: {
                uploaded: 'onImageUploaded',
                beforeUpload: 'onBeforeImageUploaded',
                failed: 'onUploadFailed'
            },
            rootPanel: {
                initialize: 'initFeedback'
            }
        },
        views: [
            'MobileUI.view.Feedback'
        ]
    },
    onFocus: function()
    {
        Ext.getBody().dom.scrollTop = 0;
    },
    onPainted: function ()
    {
        this.initFeedback();
    },
    /**
     * On cancel button tap listener
     */
    closeFeedback: function ()
    {
        this.getRootPanel().setMasked(null);
        this.beforeCancel();
        Ext.Viewport.setMasked(null);
        MobileUI.core.SuiteManager.hideDialogs(true);
    },
    /**
     * On feed back button tap listener
     */
    onFeedbackSend: function ()
    {
        this.getRootPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        var convertBoolean = function(value){
            return value === 1;
        };
        var getIfVisible = function(element, value, defaultValue){
            return !element.isHidden() ? value : defaultValue;
        };
        var ccs = this.getCcs() && this.getCcs().element.down('input');
        this.createIssue(
            getIfVisible(this.getSummary(), this.getSummary().getValue(), ''),
            getIfVisible(this.getDescription(), this.getDescription().getValue(), ''),
            getIfVisible(this.getPriority(), this.getPriority().getValue(), ''),
            getIfVisible(this.getStepsToReproduce(), this.getStepsToReproduce().getValue(), ''),
            getIfVisible(this.getActualResults(), this.getActualResults().getValue(), ''),
            getIfVisible(this.getExpectedResults(), this.getExpectedResults().getValue(), ''),
            getIfVisible(this.getDidItWork(), convertBoolean(this.getDidItWorkValue().getValue()), ''),
            getIfVisible(this.getBusinessImpact(),this.getBusinessImpact().getValue(), ''),
            getIfVisible(this.getDueDate(), this.getDueDate().getValue(), ''),
            ccs && ccs.dom.value || '',
            this.getImages(),
            this.getReason().getValue());
    },
    validate: function(){
        var required = [this.getDescription(), this.getSummary()];
        required = required.concat(this.group[this.getReason().getValue()] || []);
        required.forEach(function(item){
            item.removeCls('invalid-item');
        });
        this.getCcs().removeCls('invalid-item');
        var invalid = required.filter(function(item){
            return item.getValue && (item.getValue() === null || item.getValue() === '');
        });
        invalid.forEach(function(item){
            item.addCls('invalid-item');
        });
        var ccs = this.getCcs() && this.getCcs().element.down('input');

        if (ccs && !this.validateEmail(ccs.dom.value)){
            invalid.push(this.getCcs());
            this.getCcs().addCls('invalid-item');
        }
        return {validationFailed: invalid.map(function(item){
            return {value:item.getLabel()};
        })};
    },

    /**
     * Method removes mask
     */
    cancelMask: function ()
    {
        this.getRootPanel().setMasked(null);
    },

    onFieldChanged: function() {
        this.getFeedBackButton().setDisabled(!this.getDescription().getValue() && !this.getSummary().getValue());
    },
    validateEmail: function(value){
        if (Ext.isEmpty(value)){
            return true;
        } else {
            value = value.split(',');
            var reg = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
            return !value.some(function(item){
                return !reg.test(item.replace(/^\s+|\s+$/g,''));
            });
        }
    },

    /**@type {Array} @private*/
    images: null,
    /**
     * Method clears the bucket
     */
    beforeCancel: function ()
    {
        this.removeFromBucket(this.images);
        this.clearImages();
    },
    createGroup: function(reason, items){
        this.group[reason] = items;
        this.all = this.all.concat(items);
    },

    onReasonChanged: function(reason){
        this.all.forEach(function(item){
            item.setHidden(true);
        });
        (this.group[reason] || []).forEach(function(item){
            item.setHidden(false);
        });
    },
    /**
     * Method clears the images
     */
    clearImages: function()
    {
        this.images.splice(0, this.images.length);
    },
    /**
     * Method return the url's of the images
     * @returns {Array}
     */
    getImages: function ()
    {
        return this.images;
    },
    initGroup: function(){
        this.all = [];
        this.group = {};
        this.createGroup(MobileUI.controller.Feedback.ZENDESK_REPORT, [
            this.getStepsToReproduce(),
            this.getStepsToReproduceSeparator(),
            this.getActualResults(),
            this.getActualResultsSeparator(),
            this.getExpectedResults(),
            this.getExpectedResultsSeparator(),
            this.getActualResults(),
            this.getActualResultsSeparator(),
            this.getDidItWork(),
            this.getDidItWorkSeparator(),
            this.getBusinessImpact()
        ]);
        this.createGroup(MobileUI.controller.Feedback.ZENDESK_ASK, []);
        this.createGroup(MobileUI.controller.Feedback.ZENDESK_REQUEST, [this.getDueDate()]);
        this.createGroup(MobileUI.controller.Feedback.ZENDESK_SUGGEST, [
            this.getBusinessImpact()
        ]);
    },

    getDefaultFeedbackOptions: function(){
        return [
            {text: i18n('Report an Issue'), value: MobileUI.controller.Feedback.ZENDESK_REPORT},
            {text: i18n('Ask a question'), value: MobileUI.controller.Feedback.ZENDESK_ASK},
            {text: i18n('Request a Technical Task'), value: MobileUI.controller.Feedback.ZENDESK_REQUEST},
            {text: i18n('Suggest a new feature or enhancement'), value: MobileUI.controller.Feedback.ZENDESK_SUGGEST}
        ];
    },
    /**
     * Method inits the feedback
     */
    initFeedback: function () {
        this.images = [];
        this.customProvider = Ext.create('MobileUI.core.feedback.CustomProvider');
        this.zendeskProvider = Ext.create('MobileUI.core.feedback.ZendeskProvider');

        var options = this.getDefaultFeedbackOptions();
        this.initGroup();
        var configuration = MobileUI.core.Services.getConfigurationManager().getExtensionById('com.reltio.mobile.feedback.Feedback');
        if (Ext.isObject(configuration) && Ext.isArray(configuration.regarding)) {
            if (configuration.regarding.filter(function (item) {
                    return item.value;
                }).length > 0) {
                options = options.concat(configuration.regarding.map(function (item) {
                    return {text: item.name, value: item.value || MobileUI.controller.Feedback.ZENDESK};
                }));
            }
        }
        this.getReason().setOptions(options);
        this.getReason().setValue((configuration && configuration.defaultValue) || MobileUI.controller.Feedback.ZENDESK_REPORT);
    },
    /**
     * Method finds the image index by url. If image not found the method returns -1
     * @param url {String}
     * @returns {number}
     */
    findImageByUrl: function (url)
    {
        var i;
        for (i = 0; i < this.images.length; i++)
        {
            if (this.images[i].url === url)
            {
                return i;
            }
        }
        return -1;
    },
    /**
     * On image remove action
     * @param imgSrc {String} image source
     */
    onImgRemove: function (imgSrc)
    {
        var index = this.findImageByUrl(imgSrc),
            panel = this.getImagePanel();
        if (index !== -1)
        {
            this.images.splice(index, 1);
        }
        this.removeFromBucket(imgSrc);
        if (this.images.length === 0)
        {
            panel.setHidden(true);
            if (Ext.os.is.Phone)
            {
                this.getDescription().setMaxRows(10);
            }
            else
            {
                this.getDescription().setMaxRows(15);
            }
        }
    },
    /**
     * On image uploaded listener
     * @param resp {Object} callback response
     */
    onImageUploaded: function (resp)
    {
        this.image.setSrc(resp.url);
        this.images.push({url: resp.url, mime: resp.mime});
    },
    onBeforeImageUploaded: function ()
    {
        var panel = this.getImagePanel();
        if (panel.isHidden())
        {
            panel.setHidden(false);
            if (Ext.os.is.Phone)
            {
                this.getDescription().setMaxRows(4);
            }
            else
            {
                this.getDescription().setMaxRows(10);
            }
        }
        this.image = Ext.create('MobileUI.components.RemovableImage');
        this.image.on('remove', this.onImgRemove, this, {single: true});
        panel.add(
            this.image
        );
    },
    onUploadFailed: function(){
        if (this.image) {
            this.getImagePanel().remove(this.image);
        }
    },
    /**
     * Method removes the items from bucket
     * @param items {Array.<String>|String} items
     */
    removeFromBucket: function (items)
    {
        if (!Ext.isArray(items))
        {
            items = [items];
        }
        items = items.map(function (item)
        {
            return item.url;
        });
        MobileUI.core.Logger.info('Tries to remove image');
        var userName = MobileUI.core.session.Session.getUser() ? MobileUI.core.session.Session.getUser().full_name : '';

        if (!Ext.isEmpty(items))
        {
            MobileUI.core.Services.asyncRequest('/image_uploader', null, function (result)
            {
                MobileUI.core.Logger.error(result);
            }, {
                parse: 'json',
                method: 'DELETE',
                data: 'user=' + userName + '&images=' + items,
                signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            }, this);
        }
    },
    /**
     * Method attaches files to the issue
     * @param key {String|Number} issue id
     * @param userZendeskId {String} zendesk user id
     * @param files {Array.<Object>} file objects
     */
    attachFiles: function (key, userZendeskId, files)
    {
        if (MobileUI.core.session.Session.isExpired())
        {
            MobileUI.core.session.Session.refreshAccessToken(function ()
            {
                this.self.attachFiles(this.key, this.userZendeskId, this.files);
            }, {self: this, key: key, userZendeskId: userZendeskId, files: files});
            return;
        }
        MobileUI.core.Logger.info('Tries to attach image');
        var userName = MobileUI.core.session.Session.getUser() ? MobileUI.core.session.Session.getUser().full_name : '';
        MobileUI.core.Services.asyncRequest('/zendesk/files', function ()
            {
                MobileUI.core.Logger.info('Image successfully attached');
                this.self.showPopup(this.key);
                this.self.clearImages();

            },
            function (error)
            {
                MobileUI.core.Logger.error(error);
                this.self.clearImages();
            },
            {
                parse: 'json',
                method: 'POST',
                data: 'issueId=' + key + '&userZendeskId=' + userZendeskId + '&user=' + userName + '&files=' + Ext.encode(files),
                signer: Ext.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            }, {self: this, key: key, userZendeskId: userZendeskId, files: files});
    },
    /**
     * Method shows the result popup
     * @param popupInfo {Object} zendesk key
     */
    showPopup: function (popupInfo)
    {
        MobileUI.components.MessageBox.show({
            title: i18n(popupInfo.error || popupInfo.title),
            message: i18n(popupInfo.error ? popupInfo.description : popupInfo.text),
            buttons: {text: i18n('OK'), itemId: 'ok', ui:'action'},
            promptConfig: false,
            fn: function() {
                if (!popupInfo.error) {
                    this.closeFeedback();
                }
            },
            scope: this
        });
    },
    /**
     * Method creates issue
     * @param summary {String}
     * @param description {String}
     * @param priority
     * @param stepsToReproduce
     * @param actualResults
     * @param expectedResults
     * @param didItWork
     * @param businessImpact
     * @param dueDate
     * @param ccs
     * @param files {Array.<Object>}
     * @param options {Object?} object
     */
    createIssue: function (summary, description, priority, stepsToReproduce,
                           actualResults, expectedResults, didItWork, businessImpact, dueDate, ccs, files, options)
    {
        var issueInfo = {
            summary: summary,
            description: description,
            stepsToReproduce: stepsToReproduce,
            actualResults: actualResults,
            expectedResults: expectedResults,
            didItWork: didItWork,
            businessImpact: businessImpact,
            dueDate: dueDate,
            ccs: ccs,
            files: files,
            priority: priority,
            options: options
        };
        var toolbar = this.getNotificationToolbar();
        var notification = this.getNotificationMessage();
        toolbar.setHidden(true);
        var result = this.validate();
        if (MobileUI.core.entity.EntityUtils.validationPassed(result)){
            this.createIssueInternal(issueInfo);
        } else {
            var message = Ext.Array.unique(result.validationFailed.map(function(item){
                return item.value;
            }));
            message = i18n('Incorrect fields: %1', message.join(', '));
            message = '<div class="validation-header">'+message+'</div>';
            if (notification && !Ext.isEmpty(message)){
                toolbar.setHidden(false);
                notification.setHtml(message);
                notification.setCls('validation-message');
            }
            this.getRootPanel().setMasked(null);
        }

    },
    /**
     * Method creates issue
     * @private
     * @param issueInfo
     */
    createIssueInternal: function (issueInfo) {
        var environment = [
            'client: MobileUI',
            'browser:'+ Ext.browser.name + ' ' + Ext.browser.version,
            'engine:' + Ext.browser.engineName + ' ' + Ext.browser.engineVersion,
            'os version: ' + (Ext.os.version || '').toString(),
            'os name: ' + Ext.os.name,
            'user: ' + MobileUI.core.session.Session.getUser() ? MobileUI.core.session.Session.getUser().full_name || '' : '',
            'tenant: ' + MobileUI.core.Services.getTenantName(),
            'url: ' + document.location.toString(),
            'UI backend: ' + MobileUI.core.Services.getGeneralSettings() ? MobileUI.core.Services.getGeneralSettings().ui : '',
            'API:' + MobileUI.core.Services.getApiPath(),
            'client version: ' + MobileUI.core.Services.getGeneralSettings() ? MobileUI.core.Services.getGeneralSettings().version : ''
        ];
        if (MobileUI.core.session.Session.isExpired())
        {
            MobileUI.core.session.Session.refreshAccessToken(function ()
            {
                this.self.createIssueInternal(this.issueInfo);
            }, Ext.emptyFn, {self: this, issueInfo: issueInfo});
            return;
        }

        MobileUI.core.Logger.info('Trying to create image');
        var provider = this.zendeskProvider;
        var zendeskProviders = [
            MobileUI.controller.Feedback.ZENDESK,
            MobileUI.controller.Feedback.ZENDESK_ASK,
            MobileUI.controller.Feedback.ZENDESK_SUGGEST,
            MobileUI.controller.Feedback.ZENDESK_REPORT,
            MobileUI.controller.Feedback.ZENDESK_REQUEST
        ];
        if (issueInfo.options && zendeskProviders.indexOf(issueInfo.options) === -1)
        {
            MobileUI.core.Logger.info('Uses mail feedback provider');
            provider = this.customProvider;
        }
        provider.processIssue(issueInfo, environment, function(json){
            Ext.Viewport.setMasked(null);
            this.showPopup(json);
            this.clearImages();
        }, function(){
            Ext.Viewport.setMasked(null);
            this.clearImages();
        }, this);
    },
    onCloseNotificationBtn: function() {
        this.getNotificationToolbar().setHidden(true);
        this.getNotificationMessage().setHtml('');
        this.getNotificationMessage().setCls('notification-message');
    }
});
