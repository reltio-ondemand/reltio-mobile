/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.ForgotPassword', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.session.Session', 'MobileUI.core.Logger', 'MobileUI.components.MessageBox', 'MobileUI.core.Navigation'],
    config: {
        refs: {
            userName: 'textfield[name=userNameTextField-forgot]',
            captcha: 'recaptcha',
            resetButton: 'button[name=resetButton]',
            cancelButton: 'button[name=forgotpassword-cancelButton]'
        },
        control: {
            userName: {
                keyup: 'onFieldKeyUp',
                blur: 'onFieldBlur',
                focus: 'onFieldFocus'
            },
            resetButton: {
                tap: 'onReset'
            },
            cancelButton: {
                tap: 'onCancel'
            }
        },
        views: [
            'MobileUI.view.ForgotPassword'
        ]
    },
    scrollPosition: null,

    /**
     * Key up listener
     * @param objField {Ext.Text} key press textfield
     * @param e {Event} the focus event
     */
    onFieldKeyUp: function (objField, e)
    {
        var focus = function (element)
        {
            if (element)
            {
                element.focus();
            }
        };
        if (e.event.keyCode === 13)
        {
            if (this.validateField(this.getUserName(), focus) && this.validateField(this.getCaptcha(), focus))
            {
                this.onReset();
            }
            return false;
        }
    },

    /**
     * Method validates textfield
     * @private
     * @param textField {Ext.Text} textfield component
     * @param onFailure {Function} on failure action
     */
    validateField: function (textField, onFailure)
    {
        if (textField)
        {
            if (Ext.isEmpty(textField.getValue()))
            {
                if (Ext.isFunction(onFailure))
                {
                    onFailure(textField);
                }
                return false;
            }
        }
        return true;
    },

    onReset: function ()
    {
        var userName = this.getUserName().getValue(),
            captcha = this.getCaptcha().getValue();

        if (Ext.isEmpty(userName) || Ext.isEmpty(captcha))
        {
            MobileUI.core.util.Util.blurField();
            MobileUI.components.MessageBox.alert(i18n('Error'), i18n('Please, fill the fields'));
            return;
        }
        MobileUI.core.session.Session.resetPassword(userName, captcha, function (resp)
        {
            MobileUI.core.Navigation.redirectForward('password/' + resp.status);
        }, function (resp)
        {
            MobileUI.core.Logger.log(resp);
        }, this);
    },
    /** @private */
    onFieldFocus: function ()
    {
        // it's needed for the first input field.
        if (this.scrollPosition > 10)
        {
            document.body.scrollTop = this.scrollPosition;
            this.scrollPosition = null;
        }
    },
    /** @private */
    onFieldBlur: function ()
    {
        this.scrollPosition = document.body.scrollTop;
    },
    onCancel: function ()
    {
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
        MobileUI.core.Navigation.redirectTo('login');
    }
});
