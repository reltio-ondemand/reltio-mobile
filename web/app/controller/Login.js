/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Login', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.session.Session', 'MobileUI.core.Logger', 'MobileUI.core.Navigation','MobileUI.components.MessageBox'],
    mixins: {
        spinner: 'MobileUI.components.MSpinner'
    },
    config: {
        refs: {
            userNameTextField: 'textfield[name=userNameTextField]',
            passwordTextField: 'textfield[name=passwordTextField]',
            loginButton: 'button[name=logInButton]',
            forgotPwdButton: 'button[name=forgotPwdButton]',
            feedbackButton: 'button[name=feedbackButton]',
            policyButton: 'button[name=policyButton]'
        },
        control: {
            loginButton: {
                tap: 'onLogin'
            },
            forgotPwdButton: {
                tap: 'onForgot'
            },
            feedbackButton: {
                tap: 'onFeedBack'
            },
            policyButton: {
                tap: 'onPolicy'
            },
            userNameTextField: {
                clearicontap: 'onClearLogin',
                focus: 'onFieldFocus',
                blur: 'onFieldBlur',
                keyup: 'onFieldKeyUp'
            },
            passwordTextField: {
                focus: 'onFieldFocus',
                blur: 'onFieldBlur',
                keyup: 'onFieldKeyUp'
            }
        },
        views: [
            'MobileUI.view.Login'
        ]
    },
    onFeedBack: function ()
    {
        MobileUI.core.Navigation.redirectToUrl('http://support.reltio.com/');
    },

    onPolicy: function(){
        MobileUI.core.Navigation.redirectToUrl('http://reltio.com/privacy/');
    },
    /**
     * Key up listener
     * @param objField {Ext.Textfield} key press textfield
     * @param e {Event} the focus event
     */
    onFieldKeyUp: function (objField, e)
    {
        if (e.event.keyCode === 13)
        {
            if (this.validateField(this.getUserNameTextField()) && this.validateField(this.getPasswordTextField()))
            {
                this.onLogin();
            }
            return false;
        }
    },

    /**
     * Method validates textfield
     * @private
     * @param textField {Ext.Text} textfield component
     */
    validateField: function (textField)
    {
        if (textField)
        {
            if (Ext.isEmpty(textField.getValue()))
            {
                    textField.focus();
                return false;
            }
        }
        return true;
    },
    onLogin: function ()
    {
        var session = MobileUI.core.session.Session,
            userName = this.getUserNameTextField().getValue().trim();
        if (!(this.validateField(this.getUserNameTextField()) && this.validateField(this.getPasswordTextField())))
        {
            return;
        }
        this.getLoginButton().setText('');
        this.getLoginButton().setHtml(this.getSpinnerHtml('#ffffff'));
        this.getLoginButton().addCls('btn-spinner');

        MobileUI.core.Logger.debug('User "' + userName + '" tries to login');
        MobileUI.core.session.Session.login(userName, this.getPasswordTextField().getValue(), function ()
        {
            MobileUI.core.Logger.debug('User "' + userName + '" logged in');
            session.initSession(function ()
            {
                MobileUI.core.Logger.debug('Session inited redirect to default view');
                var viewActivator = this.getApplication().getController('ViewActivator');
                if (viewActivator.getPreviousState()) {
                    MobileUI.core.Navigation.redirectTo(viewActivator.getPreviousState());
                } else {
                    MobileUI.core.Navigation.redirectToDefault();
                }
            }, function (response)
            {
                MobileUI.core.util.Util.blurField();
                MobileUI.components.MessageBox.alert(i18n('Login failed'), i18n(response && (response.errorDetailMessage || response.errorMessage)) || i18n('Tenant not defined'));
                MobileUI.core.session.Session.logout(function(){
                    this.getLoginButton().setHtml('');
                    this.getLoginButton().setText(i18n('Log In'));
                    this.getLoginButton().removeCls('btn-spinner');
                }, this);
               
            }, this);
        }, function (resp)
        {
            MobileUI.core.util.Util.blurField();
            MobileUI.components.MessageBox.alert(i18n('Login failed'),  i18n(resp.errorMessage || resp.error_description || resp.error), Ext.emptyFn);
            MobileUI.core.Logger.warn(resp);
            this.getLoginButton().setHtml('');
            this.getLoginButton().setText(i18n('Log In'));
            this.getLoginButton().removeCls('btn-spinner');
        }, this);
    },

    onForgot: function ()
    {
        MobileUI.core.Logger.debug('User clicked on forgot button');
        MobileUI.core.Navigation.redirectForward('forgotpassword');
    },

    onClearLogin: function ()
    {
        MobileUI.core.Logger.debug('User clears login');
        this.getPasswordTextField().setValue('');
    },

    /** @private @type {Boolean} */
    scrollPosition: null,

    /** @private */
    onFieldFocus: function ()
    {
        if (this.scrollPosition !== null)
        {
            document.body.scrollTop = this.scrollPosition;
            this.scrollPosition = null;
        }
    },

    /** @private */
    onFieldBlur: function ()
    {
        if (!navigator.userAgent.match('CriOS'))
        {
            this.scrollPosition = document.body.scrollTop;
        }
    }
});
