/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MAddAttribute', {
    requires: ['MobileUI.components.ListItemEditor'],
    showAddAttributeDialog: function(attrTypes, index, list, originalUri, parentUri, entityStateUri, notificationText) {
        var context = {
            index: index,
            attrTypes: attrTypes,
            list: list,
            parentUri: parentUri,
            originalUri: originalUri,
            entityStateUri: entityStateUri,
            self: this
        };
        var addAttribute = function(attrLabel, canceled) {
            if (canceled) {
                return;
            }
            var context = this;
            var list = context.list,
                attrTypes = context.attrTypes,
                attribute, attrType, i, models = [],
                uri = context.originalUri;

            for (i = 0; i < attrTypes.length; i++) {
                if (attrTypes[i].label === attrLabel.label) {
                    attrType = attrTypes[i];
                    break;
                }
            }

            var isComplex = MobileUI.core.entity.EntityUtils.isComplex(attrType);
            if (uri.split('/').length === 2 && uri.indexOf('entities/') === 0) {
                uri = uri + '/attributes';
            }
            attribute = {
                value: MobileUI.core.entity.EntityUtils.isComplex(attrType) ? {} : '',
                attrType: attrType,
                uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(uri, attrType),
                parentUri: context.originalUri
            };
            context.attribute = attribute;
            if (!isComplex) {
                models = MobileUI.core.entity.EntityUtils.createAttributeModel(true)(models, attribute);
                models.forEach(function (model) {
                    if (model.userData) {
                        model.userData.editingUri = attribute.uri;
                    }
                });
                var record = Ext.create('MobileUI.components.list.model.EListModel', models[0]);
                var tmpItem = {
                    getRecord: function () {
                        return record;
                    },
                    recreate: function () {
                    }
                };
                context.models = models;
                context.self.addAttributeToModel(tmpItem, '', context);
                context.self.onValueChanged('', tmpItem, attrType, false);
                context.self.updateListHeight(list);
            } else {
                if (this.self.beforeComplexEditorOpened)
                {
                    this.self.beforeComplexEditorOpened();
                }
                if (attrType.type === 'Reference') {
                    this.self.addReferenceAttribute(attribute, this.originalUri, this.parentUri, true);
                }
                else {
                    this.self.showComplexAttributeEditor(attrType, this.originalUri, this.parentUri, attribute.uri,  true);
                }
                MobileUI.core.SuiteManager.on('hideDialogs', function() {
                    this.updateListHeight(list);
                }, this.self, {single: true, delay: 700});
            }

        };

        if (attrTypes.length === 1) {
            addAttribute.call(context, attrTypes[0]);
        }
        else {
            if (!MobileUI.core.SuiteManager.hasDialog('chooseattrtypeDialog')) {
                MobileUI.core.SuiteManager.addDialog('chooseattrtypeDialog', Ext.create('MobileUI.view.dialogs.ChooseAttributeType'));
            }
            var controller = MobileUI.app.getController('dialogs.ChooseAttributeType');
            controller.resetParseDisplayName();
            controller.setTitleText(i18n('Add attribute'));
            controller.setValue(null);
            controller.setValueChangedHandler(addAttribute, context);
            controller.setValues(attrTypes.map(function(attr) {
                return {label: attr.label, required: MobileUI.core.CardinalityChecker.isRequired(attr.required)};
            }));
            controller.setNotificationText(notificationText);
            controller.setParseDisplayName(function(item) {
                return item.label;
            });
            MobileUI.core.SuiteManager.showDialog('chooseattrtypeDialog');
        }
    },

    addAttributeToModel: function (item, value, context) {
        var store = context.list.getStore();
        store.insert(context.index + 1, context.models);
        item = context.list.getItemAt(context.index + 1);
        var record = item.getRecord();
        var editor = record.get('editor');
        record.set('label', value);
        item.recreate();
        if (editor) {
            delete editor.options;
            editor = Ext.clone(editor);
            editor.valueChangedCallback = context.self.onValueChanged;
            editor.options = {};
            if (context.self.editedEntity){
                editor.options.editedEntity = context.self.editedEntity;
            }
            editor.valueChangedContext = context.self;
            record.set('editor', editor);
        }
        var rootEntity = MobileUI.core.entity.TemporaryEntity.getEntity(context.entityStateUri);
        var attribute  = Ext.clone(context.attribute);
        attribute.ov = true;
        attribute.type = context.attribute.attrType.uri;
        attribute.name = attribute.attrType.name;
        rootEntity.attributes.push(attribute);
        rootEntity.addedAttributes.push(attribute);
        delete context.models;
        delete context.attribute;
        this.updateListHeight(item.dataview);
    }
});
