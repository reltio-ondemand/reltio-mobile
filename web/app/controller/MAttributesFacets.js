/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MAttributesFacets', {
    requires: ['MobileUI.components.list.MListHeight', 'MobileUI.components.list.MListModel'],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        profileBand: 'MobileUI.controller.MProfileBand',
        profileConfiguration: 'MobileUI.controller.MProfileConfiguration',
        listModel: 'MobileUI.components.list.MListModel'
    },
    parsedParams: null,
    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getProfileBand().setFavorite(item);
        },this);
    },
    showAttributesFacet: function (params)
    {
        var viewActivator = this.getApplication().getController('ViewActivator');
        this.parsedParams = this.parseParams(params);

        if (viewActivator.waitForActivate())
        {
            viewActivator.on('activated', function ()
            {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
                {
                    this.showAttributesFacetInternal();
                }
            }, this, {single: true});
        }
        else
        {
            this.showAttributesFacetInternal();
        }
    },
    onAttributeFacetPullRefresh: function ()
    {
        this.showAttributesFacetInternal(true);
    },
    /**
     * Method shows the complex attributes
     * @private
     * @param force {Boolean} should refresh entity
     */
    showAttributesFacetInternal: function (force)
    {
        if (MobileUI.core.Navigation.isBackAllow())
        {
            this.getBackToolbarBtn().setHidden(false);
        }
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedParams.entityUrl && !force)
        {
            this.processFacetAttributes(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.Logger.info('Trying to request entity:' + this.parsedParams.entityUrl);
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processFacetAttributes, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },

    processFacetAttributes: function (resp)
    {
        var attributes,
            configuration = MobileUI.core.Services.getConfigurationManager(),
            extension, keys, key, hiddenAttributes, tmpFiltered, filtered = {}, attributeName, list, store, model, groupByLabel = function (prev, current)
            {
                if (extension.attributes[attributeName].indexOf(current.attrType[attributeName]) !== -1)
                {
                    if (!prev[current.attrType.label])
                    {
                        prev[current.attrType.label] = [];
                    }
                    prev[current.attrType.label].push(current);
                }
                return prev;
            };
        extension = this.getProfileConfiguration(resp.type);
        if (extension)
        {
            hiddenAttributes = extension.hiddenAttributes;
        }
        attributes = MobileUI.core.entity.EntityUtils.processAttributes(resp, null, hiddenAttributes);
        this.fillBand(this.getProfileBand(), resp);
        extension = configuration.getExtensionById(this.parsedParams.facetId);
        keys = Object.keys(extension.attributes);
        if (!Ext.isEmpty(keys))
        {
            attributeName = keys[0];
            for (key in attributes)
            {
                if (attributes.hasOwnProperty(key))
                {
                    tmpFiltered = attributes[key].reduce(groupByLabel, filtered);
                    filtered = tmpFiltered;
                }
            }

        }
        list = this.getAttributesFacet();
        store = list.getStore();
        store.clearData();
        model = this.createAttributesFacetModel(filtered);
        store.add(model);
        this.updateListHeight(list);

    },

    /**
     * Method creates attribute model
     * @param attributes {Object} grouped attributes
     * @param editable {Boolean?}
     * @returns {Array}
     */
    createAttributesFacetModel: function (attributes)
    {
        var result = [], key, attributesInCategory, attributeInCategory, i,
            ItemUtils = MobileUI.core.entity.EntityItemUtils,
            categoriesCount = Object.keys(attributes).length,
            showCategory;
        for (key in attributes)
        {
            if (attributes.hasOwnProperty(key))
            {
                attributesInCategory = attributes[key];
                showCategory = (categoriesCount > 1 || categoriesCount === 1 && key !== 'Other');
                if (showCategory && attributesInCategory.length > 0)
                {
                    result.push(ItemUtils.createHeader(key));
                }
                for (i = 0; i < attributesInCategory.length; i++)
                {
                    attributeInCategory = attributesInCategory[i];
                    attributeInCategory.showLabel = false;
                    if (attributeInCategory.attrType.hidden === true)
                    {
                        continue;
                    }
                    if (attributeInCategory.value.length > 1)
                    {
                        result = result.concat(ItemUtils.processMultiValuesAttribute(attributeInCategory, attributeInCategory.attrType, i + 1 === attributesInCategory.length));
                    }
                    else
                    {
                        result.push(ItemUtils.processOneValueAttribute(attributeInCategory, attributeInCategory.attrType, i + 1 === attributesInCategory.length));
                    }
                }
            }
        }
        return result;
    },
    /**
     * Method parse the params
     * @private
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string}}
     */
    parseParams: function (params)
    {
        return {
            facetId: params[0],
            entityUrl: params.splice(1, params.length - 1).join('/')
        };
    }

});
