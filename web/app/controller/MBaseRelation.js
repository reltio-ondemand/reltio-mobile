/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MBaseRelation', {
    requires: [
        'MobileUI.core.relations.RelationsUtils'
    ],
    /**@type {MobileUI.components.Popup} @private */
    menu: null,
    /**
     * Menu tap listener
     * @protected
     */
    onMenuTap: function ()
    {
        var options, order, sortBy, key, sortByControl, radio, length,
            i = 0;
        if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1)
        {
            return;
        }
        if (Ext.isEmpty(this.menu) || this.menu.isDestroyed)
        {
            this.menu = Ext.create('MobileUI.components.Popup');
            this.menu.setFilledAnchor(true);
            this.menu.add({
                xtype: 'fieldset',
                name: 'sortOrder',
                title: i18n('Sort order'),
                cls: 'x-reltio-fieldset',
                defaults: {
                    labelWidth: 180
                },
                items: [{
                    xtype: 'radiofield',
                    name: 'sort',
                    label: i18n('Ascending'),
                    checked: true,
                    value: 'asc'
                }, {
                    xtype: 'radiofield',
                    name: 'sort',
                    label: i18n('Descending'),
                    value: 'desc',
                    cls: 'without-border'
                }]
            });
            options = this.getOptions();
            sortByControl = Ext.create('Ext.form.FieldSet', {
                name: 'sortBy',
                title: i18n('Sort by'),
                cls: 'x-reltio-fieldset',
                defaults: {
                    labelWidth: 180
                }
            });
            this.menu.add(sortByControl);
            if (!Ext.isEmpty(options))
            {
                length = Object.keys(options).length;
                for (key in options)
                {
                    if (options.hasOwnProperty(key))
                    {
                        radio = Ext.create('Ext.field.Radio', {
                            name: 'sortby',
                            label: i18n(options[key]),
                            value: key,
                            checked: true
                        });
                        if (i + 1 === length)
                        {
                            radio.setCls('without-border');
                        }
                        sortByControl.add(radio);
                        i++;
                    }
                }
            }
            sortByControl.add({
                xtype: 'radiofield',
                name: 'sortby',
                label: i18n('Name'),
                value: '',
                checked: Ext.isEmpty(options)
            });
            this.menu.setWidth(270);
            this.menu.setHeight(233);

        }
        if (this.menu.isHidden() !== false)
        {
            this.menu.showBy(this.getAdditionalMenuBtn());
            this.checkMenu();
            this.menu.addListener('hide', function ()
            {
                order = this.menu.down('radiofield[name=sort]').getGroupValue() || '';
                sortBy = this.menu.down('radiofield[name=sortby]').getGroupValue() || '';
                this.processMenuAction(order, sortBy);
            }, this, {single: true});
        }
        else
        {
            this.menu.hide();
        }
    },

    checkMenu: function ()
    {
        var groupValue = this.getGroupValue();
        this.menu.down('radiofield[name=sort]').setGroupValue(groupValue.order);
        this.menu.down('radiofield[name=sortby]').setGroupValue(groupValue.sortBy);
    }
});
