/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MBaseSearch', {
    requires: ['MobileUI.core.util.HashUtil','MobileUI.core.Navigation'],
    /** @private */
    searchResultsOffset: 0,

    /** @private */
    searchResultsPage: 30,

    /** @private */
    forceSearch: false,

    /**
     * Processes server response
     * @param resp {Array}
     */
    processSearchResults: function (resp)
    {
        var list = this.getFoundList(),
            store, result;

        if (this.getFilterList) {
            this.getFilterList().refresh();
        }

        if (this.getSearchField && this.getSearchField()) {
            this.getSearchField().setValue(MobileUI.core.search.SearchParametersManager.getTypeAhead());
        }

        if (list) {
            list.setMasked(false);
            if (!resp || resp.length === 0) {
                list.fetchDone(false);
                return;
            }
            result = MobileUI.core.Services.getSearchResultBuilder().buildContent(resp);
            store = list.getStore();
            store.add(result);
            list.fetchDone(true);

            if (this.updateKeyWord) {
                this.updateKeyWord();
            }
        }
    },

    processSearchTotalResults: function(resp) {
        if (this.getTotalLabel()){
            this.getTotalLabel().setHtml(i18n(resp.total));
            this.getTotalLabel().show();
        }
    },

    /**
     * Search item tap listener
     * @param dataEvent {Ext.direct.Event} dataEvent
     */
    onSearchItemTap: function (dataEvent)
    {
        if (Ext.isObject(dataEvent.getData().userData))
        {
            MobileUI.core.Navigation.redirectForward('profile/' +
                MobileUI.core.util.HashUtil.encodeUri(dataEvent.getData().userData.uri));
        }
    },

    /**
     * @private
     * @param {Boolean} [force]
     */
    resetSearchResults: function(force) {
        var list = this.getFoundList();
        if (list) {
            this.searchResultsOffset = -this.searchResultsPage;
            this.forceSearch = force;
            list.getStore().clearData();
            list.setMasked(true);
            list.setAllowFetch(true);
            list.startFetch();
        }
    },

    /**
     * On list refresh
     */
    onRefresh: function(){
        var list = this.getFoundList();
        if (list)
        {
            this.forceSearch = true;
            list.getStore().clearData();
            list.setMasked(true);
            this.searchResultsOffset = -this.searchResultsPage;
            this.onListFetch();
        }
    },
    /**
     * @private
     */
    onListFetch: function() {
        var totalLabel = this.getTotalLabel();
        if (totalLabel) {
            totalLabel.hide();
        }

        if (this.getFoundList()) {
            this.searchResultsOffset += this.searchResultsPage;
            MobileUI.core.search.SearchParametersManager.executeSearch(
                this.searchResultsPage, this.searchResultsOffset, this.forceSearch);
            this.forceSearch = false;
        }
    },

    onClearFilters: function() {
        MobileUI.core.search.SearchParametersManager.resetSearch();
        this.getApplication().getController('SearchFacet').resetFacets();
        this.getApplication().getController('SearchFacet').refreshFacets(true);
        this.resetSearchResults(true);
    },

    subscribeEvents: function() {
        MobileUI.core.session.Session.addListener('searchComplete', this.processSearchResults, this);
        MobileUI.core.session.Session.addListener('searchTotalComplete', this.processSearchTotalResults, this);

        this.getApplication().getController('ViewActivator').on('activated', function () {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                this.onShow();
                MobileUI.core.search.SearchParametersManager.on('globalFilterChanged', this.onGlobalFilterChanged, this);
            }
        }, this);

        this.getApplication().getController('ViewActivator').on('deactivated', function () {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1) {
                MobileUI.core.search.SearchParametersManager.un('globalFilterChanged', this.onGlobalFilterChanged, this);
            }
        }, this);
    },

    onGlobalFilterChanged: function() {
        this.resetSearchResults(true);
    }
});
