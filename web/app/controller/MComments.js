/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MComments', {
    requires: ['MobileUI.core.Logger'],
    /**@type {String} @private} */
    comment: null,
    /**
     * Method get the relation and update the list
     */
    updateRelation: function ()
    {
        var list = this.getCommentsList();
        list.setMasked(true);
        MobileUI.core.Logger.info('Tries to get relation:' + this.getRelation());
        MobileUI.core.session.Session.getRelation(this.getRelation(), function (resp)
        {
            var store, ratings, i, rating, item, model = [];
            if (Ext.isArray(resp) && !Ext.isEmpty(resp) && Ext.isArray(resp[0].ratings))
            {
                ratings = resp[0].ratings;
                for (i = 0; i < ratings.length; i++)
                {
                    rating = ratings[i];
                    item = {
                        type: MobileUI.components.list.items.BaseListItem.types.comments
                    };
                    item.label = rating.user;
                    if (rating.user === MobileUI.core.session.Session.getUserName())
                    {
                        this.comment = rating.comment;
                    }
                    item.secondaryLabel = !Ext.isEmpty(rating.comment) ? i18n(rating.comment) : '';
                    try
                    {
                        item.date = i18n(Ext.Date.parseDate(rating.timestamp, 'c'), MobileUI.core.I18n.DATE_LONG);
                    }
                    catch (ignore)
                    {
                        item.date = '';
                    }
                    item.userVote = rating.value || 0;
                    model.push(item);
                }
            }

            store = list.getStore();
            store.clearData();
            store.add(model);
            list.setMasked(false);
        }, function (resp)
        {
            MobileUI.core.Logger.error(resp);
            list.setMasked(false);
        }, this);
    },

    getUserComment: function ()
    {
        return this.comment || '';
    }
});
