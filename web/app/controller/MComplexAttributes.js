/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MComplexAttributes', {
    requires: ['MobileUI.components.list.MListHeight','MobileUI.components.list.MListModel'],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        addAttribute: 'MobileUI.controller.MAddAttribute'
    },
    /**@type{{}} */
    parsedParams: null,
    originalAttributes: null,
    /**
     * Method shows complex attributes
     * @param params {String} attribute url
     */
    showComplexAttribute: function (params)
    {
        var viewActivator = this.getApplication().getController('ViewActivator');
        this.parsedParams = this.parseParams(params);
        if (this.parsedParams) {
            if (viewActivator.waitForActivate()) {
                viewActivator.on('activated', function() {
                    if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1) {
                        this.showComplexAttributeInternal();
                    }
                }, this, {single: true});
            }
            else {
                this.showComplexAttributeInternal();
            }
        }
    },

    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getProfileBand().setFavorite(item);
        },this);
    },

    onComplexPullRefresh: function ()
    {
        this.showComplexAttributeInternal(true);
    },
    /**
     * Method shows the complex attributes
     * @private
     * @param force {Boolean} should refresh entity
     */
    showComplexAttributeInternal: function (force)
    {
        if (MobileUI.core.Navigation.isBackAllow())
        {
            var back = this.getBackToolbarBtn();
            if (back) {
                back.setHidden(false);
            }
        }
        var uri = (this.parsedParams.relationUrl || this.parsedParams.entityUrl);
        if (MobileUI.core.session.Session.getEntityUri() === uri && !force)
        {
            this.processComplexAttribute(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.Logger.info('Trying to request entity:' + uri);
            var tmp = MobileUI.core.entity.TemporaryEntity.getEntity(uri);
            if (tmp) {
                this.processComplexAttribute(tmp);
            }
            else {
                MobileUI.core.session.Session.requestEntity(uri, this.processComplexAttribute, function(resp) {
                    MobileUI.core.Logger.error(resp);
                }, this);
            }
        }
    },

    /**
     * Method processes the nested attributes
     * @private
     * @param attrType {Object} attribute type object
     * @param attributeObj {Object} attribute object
     * @returns {Array}
     */
    processNestedAttribute: function(attrType, attributeObj) {
        var nestedAttrTypes, i, value, result = [], filterOv = function(item)
        {
            return item.ov !== false;
        };
        nestedAttrTypes = attrType.attributes;

        for (i = 0; i < nestedAttrTypes.length; i++) {
            value = attributeObj.attribute.value[nestedAttrTypes[i].name];

            if (value) {
                if (Ext.isArray(value))
                {
                    value = value.filter(filterOv);
                }
                if (value.length > 0) {
                    result.push({attrType: nestedAttrTypes[i], value: value});
                }
            }
        }
        return result;
    },
    /**
     * Method processes the reference attributes
     * @private
     * @param attrType {Object} attribute type object
     * @param attributeObj {Object} attribute object
     * @returns {Array}
     */
    processReferenceAttribute: function (attrType, attributeObj, callback, context)
    {
        function filterOv(item) {
            return item.ov !== false;
        }

        attributeObj.attribute.attrType = attrType;

        MobileUI.core.session.Session.findEntity(attributeObj.attribute.refEntity.objectURI, function(refEntity) {
            if (!refEntity) {
                callback.call(context, []);
                return;
            }

            this.refEntity = refEntity;

            MobileUI.core.session.Session.findEntity(attributeObj.attribute.refRelation.objectURI, function(refRelation) {
                if (!refRelation) {
                    callback.call(context, []);
                    return;
                }

                this.refRelation = refRelation;
                if (attributeObj.attribute.refRelation.attributes) {
                    //Already edited
                    refRelation.attributes = attributeObj.attribute.refRelation.attributes;
                }
                else {
                    attributeObj.attribute.refRelation.attributes = refRelation.attributes;
                }

                var result = attributeObj.attribute.attrType.referencedAttributeURIs.map(function(typeUri) {
                    var attrType, value, attr;

                    if (typeUri.indexOf('configuration/relationTypes') === 0)
                    {
                        attrType = MobileUI.core.Metadata.findRelationAttributeByUri( null, typeUri );
                    }
                    else
                    {
                        attrType = MobileUI.core.Metadata.findEntityAttributeByUri( refEntity.type, typeUri );
                    }
                    if (!attrType) {
                        return null;
                    }

                    attr = refEntity.attributes[attrType.name] || refRelation.attributes[attrType.name];

                    if (Ext.isEmpty(attr) || Ext.isEmpty(attr.filter(filterOv))) {
                        return null;
                    }

                    if (MobileUI.core.entity.EntityUtils.isComplex(attrType)) {
                        value = attr.filter(filterOv);
                    }
                    else {
                        if (Ext.isArray(attr)) {
                            value = attr.filter(filterOv);
                        }
                        else {
                            value = [attr.value];
                        }
                    }

                    return {
                        attrType: attrType,
                        value: value
                    };
                }).filter(function(attr) {return attr;});

                callback.call(context, result);
            }, this);
        }, this);
    },

    processComplexAttribute: function (entity)
    {
        function fill(result) {
            this.getCrumb().setPath(path);
            key['Other'] = result;
            var attributes = attrType.attributes;
            if (attrType.referencedEntityTypeURI) {
                var refAttrType = MobileUI.core.Metadata.getRelationType(attrType.relationshipTypeURI);
                attributes = refAttrType.attributes;
            }
            entityModel = EntityUtils.createAttributesModel(key, false, {'Other': attributes});
            var referenceInlined = this instanceof MobileUI.controller.ComplexAttributes;
            entityModel.forEach(function(item) {
                if (item.editor) {
                    item.editor = Ext.clone(item.editor);
                    item.editor.valueChangedCallback = this.onValueChanged;
                    item.editor.valueChangedContext = this;

                    if (item.userData && !item.userData.editingUri) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].attrType.uri === item.editor.uri) {
                                item.userData.editingUri = result[i].uri;
                            }
                        }
                    }
                } else {
                    if (referenceInlined && item.type === 'entityInfo'){
                        item.info = false;
                    }
                }

            }, this);

            list = this.getComplexAttributesList();
            store = list.getStore();
            store.clearData();
            store.add(entityModel);
            this.updateListHeight(list);

            this.attributeType = attrType;
            this.entity = { attributes: attributeObj.attribute.value };
            this.tmpEntity = entity;
            this.attribute = attributeObj.attribute;
        }

        var uri = (this.parsedParams.relationUrl || this.parsedParams.entityUrl);
        var attrUri = uri + '/' + this.parsedParams.attributeUrl,
            EntityUtils = MobileUI.core.entity.EntityUtils,
            path = EntityUtils.collectPath(entity.attributes, entity.type, attrUri),
            attributeObj = EntityUtils.findAttribute(entity.attributes, attrUri, null, !this.inRelation),
            attrType, entityModel, list, store, key = {};

        this.originalAttributes = Ext.clone(attributeObj.attribute.value);
        this.addedInfo = attributeObj.attribute.addedInfo;

        attrType = attributeObj.attribute.attrType;
        if (!attrType) {
            attrType = MobileUI.core.Metadata.findEntityAttributeByName(entity.type, attributeObj.name) ||
                MobileUI.core.Metadata.findAttributeByName(attributeObj.name);
        }

        if (!Ext.isEmpty(this.parsedParams.relationUrl)) {
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, function (entity) {
                this.fillBand(this.getProfileBand(), entity);
            }, function (resp) {
                MobileUI.core.Logger.error(resp);
            }, this, false);
        } else {
            this.fillBand(this.getProfileBand(), MobileUI.core.session.Session.getEntity());
        }
        if (attrType.type === 'Nested')
        {
            fill.call(this, this.processNestedAttribute(attrType, attributeObj));
        }
        else if (attrType.type === 'Reference')
        {
            this.processReferenceAttribute(attrType, attributeObj, fill, this);
        }
    },

    /**
     * @private
     * @param params {Array} parameters
     * @returns {{entityUrl: string, attributeUrl: String}}
     */
    parseParams: function (params)
    {
        var entityUrl = '',
            attribute,
            attributeUrl = '',
            relationUrl = '',
            tmpEntityUri;
        if (Ext.isArray(params))
        {
            var index = params.indexOf('_relations_');
            if (index !== -1) {
                var end = params.splice(index+1);
                attribute = end.splice(2, end.length - 2);
                attributeUrl = attribute.join('/');
                relationUrl = end.join('/');
                params.splice(index);
            }else if (params.length > 2)
            {
                attribute = params.splice(2, params.length - 2);
                attributeUrl = attribute.join('/');
            }
            entityUrl = params.join('/');
        }

        return {entityUrl: entityUrl, relationUrl: relationUrl, attributeUrl: attributeUrl, tmpEntityUri: tmpEntityUri};
    },

    onCancelComplexToolbarBtn: function() {
        MobileUI.core.Navigation.back();
    },

    onDoneComplexToolbarBtn: function() {
        if (this.attribute) {
            if (this.attribute.addedInfo) {
                delete this.attribute.addedInfo;

                if (this.attribute.value) {
                    Object.keys(this.attribute.value).forEach(function(name) {
                        var hasValue = this.attribute.value[name].some(function(attr) {
                            return !!attr.value;
                        });

                        if (!hasValue) {
                            delete this.attribute.value[name];
                        }
                    }, this);
                }
            }
            if (this.attribute.refRelation) {
                var tmp = {attributes: []};
                this.saveValues(tmp, this.getComplexAttributesList().getStore(), true);
                tmp.attributes.forEach(function(attr) {
                    var original = this.attribute.refRelation.attributes[attr.attrType.name];
                    if (original) {
                        var changed = original.some(function(originalAttr) {
                            if (originalAttr.uri === attr.uri) {
                                originalAttr.value = attr.value[0];
                                return true;
                            }
                        });

                        if (!changed) {
                            original.push({
                                value: attr.value[0]
                            });
                        }
                    }
                }, this);

                this.updateAttributeLabel();
            }
        }
        MobileUI.core.Navigation.back();
    },

    onComplexListButtonExecute: function(eventData) {
        var data = eventData.record.get('userData');
        if (data) {
            var store = eventData.item.dataview.getStore(),
                index = store.indexOf(eventData.record),
                msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();

            this.showAddAttributeDialog(data.attributes, index, this.getComplexAttributesList(),
                this.parsedParams.entityUrl + '/' + this.parsedParams.attributeUrl, this.parsedParams.tmpEntityUri, null, msg);
        }
    },

    updateAttributeLabel: function() {
        var entity = this.tmpEntity;
        if (this.attributeType.type === 'Reference') {
            this.attribute.relationshipLabel = MobileUI.core.entity.EntityUtils.evaluateEntityLabel(
                this.attribute.refRelation, this.attribute.attrType.relationshipLabelPattern);

            entity = this.refEntity;
        }
        this.attribute.label = MobileUI.core.entity.EntityUtils.evaluateLabel(
            entity, entity.attributes, this.attributeType);
    },

    onComplexAttributeDelete: function(record) {
        this.deleteAttribute(record);
        this.processComplexAttribute(this.tmpEntity);
    }
});
