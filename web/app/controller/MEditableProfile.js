/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MEditableProfile', {
    requires: [
        'MobileUI.components.ListItemEditor',
        'MobileUI.view.dialogs.entities.SelectImage',
        'MobileUI.core.Logger',
        'MobileUI.core.Services',
        'MobileUI.core.session.Session',
        'MobileUI.controller.MProfileConfiguration',
        'MobileUI.components.MessageBox'
    ],
    mixins: {
        suggest: 'MobileUI.controller.MSuggestEdit',
        configuration: 'MobileUI.controller.MProfileConfiguration'
    },

    onCancelToolbarBtn: function(){
        MobileUI.components.MessageBox.show({
            title: i18n('Warning'),
            message: i18n('Are you sure you want to cancel all changes?'),
            buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
            promptConfig: false,
            scope: this,
            fn: function (buttonId) {
                if (buttonId === 'yes'){
                    MobileUI.core.entity.TemporaryEntity.removeAllEntities();
                    this.returnToViewMode(this.originalUri);
                }
            }
        });
    },


    onAvatarTap: function(){
        if (!MobileUI.core.SuiteManager.hasDialog('selectImage'))
        {
            MobileUI.core.SuiteManager.addDialog('selectImage', Ext.create('MobileUI.view.dialogs.entities.SelectImage'));
        }
        if (this.beforeComplexEditorOpened){
            this.beforeComplexEditorOpened();
        }
        MobileUI.core.SuiteManager.showDialog('selectImage');
        this.getApplication().getController('dialogs.entities.SelectImage').showImages(this.entityUri);
    },

    returnToViewMode: function(url, force){
        if (MobileUI.core.Navigation.isBackAllow() && !force) {
            MobileUI.core.Navigation.back();
        } else {
            MobileUI.core.Navigation.redirectForward('profile/' +
                MobileUI.core.util.HashUtil.encodeUri(url));
        }
    },

    editEntity: function (params) {
        var Session = MobileUI.core.session.Session;
        Session.getLookupsManager().cleanTouchedList();
        Session.getValidator().resetValidators();
        this.entityUri = this.originalUri = params.slice(0, 2).join('/');
        this.mode = params.length === 3 ? params.slice(2, 3).join('/')  : 'edit';
        this.parentUri = this.originalUri;
        this.entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.originalUri);
        if (!this.entity) {
            if (MobileUI.core.entity.TemporaryEntity.isTemp(this.originalUri)) {
                MobileUI.core.Navigation.redirectToDefault();
            }
            else {
                MobileUI.core.Navigation.redirectForward('profile/' +
                    MobileUI.core.util.HashUtil.encodeUri(this.originalUri), false);
            }
            return;
        }
        if (this.mode === 'create' && this.getTitle) {
            if (this.getTitle())
            {
                var canCreate = MobileUI.core.PermissionManager.securityService().metadata.canCreate(this.entity.type);
                var template = 'Create %1';

                if (!canCreate) {
                    template = 'Suggest new %1';
                }

                this.getTitle().setHtml(i18n(template, Ext.util.Format.htmlEncode(MobileUI.core.Metadata.getEntityType(this.entity.type).label)));
            }
        }
        if (this.entity) {
            MobileUI.core.session.Session.getLookupsManager().initWithEntityType(this.entity.type);
            if (this.entity.original) {
                MobileUI.core.session.Session.getLookupsManager().fillValuesFromEntity(this.entity.original);
            }
        }
        this.processEditEntity(this.getEditedEntity());
    },
    processEditEntity: function(entity)
    {
        var entityType = MobileUI.core.Metadata.getEntityType(entity && entity.type);
        if (!entityType){
            return;
        }
        var allAttributes = entityType.attributes;
        if (this.getFacetProfileBand && this.getFacetProfileBand()) {
            this.fillBand(this.getFacetProfileBand(), entity);
            this.getFacetProfileBand().setEditMode(true);
        }
        var extension = this.getProfileConfiguration(entity.type),
            hiddenAttributes = extension && extension.hiddenAttributes || [];

        allAttributes = (allAttributes || []).filter(function(attrType){
            return hiddenAttributes.indexOf(attrType.uri) === -1;
        });
        var requiredAttributes = allAttributes.filter(
            function (attr) {
                return MobileUI.core.CardinalityChecker.isRequired(attr) && !attr.hidden;
            });
        for (var i = 0; i < requiredAttributes.length; i++) {
            var found = false;
            if (!MobileUI.core.entity.EntityUtils.isComplex(requiredAttributes[i])) {
                for (var j = 0; j < entity.attributes.length; j++) {
                    if (entity.attributes[j].type === requiredAttributes[i].uri) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    entity.attributes.push({
                        name: requiredAttributes[i].name,
                        ov: true,
                        parentUri: entity.uri,
                        type: requiredAttributes[i].uri,
                        uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(entity.uri, requiredAttributes[i]),
                        value: ''
                    });
                }
            }
        }
        var filterOv = function (item) {
            return item.ov !== false;
        };
        this.attributes = entity.attributes.filter(function (item) {
            return item.parentUri === this.parentUri;
        }, this).filter(filterOv);

        var groupsConfig = extension && extension.attributes || [],
            structure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(entity.type, groupsConfig);
        this.showAttributesForEditing(this.attributes, structure, MobileUI.core.Metadata.getEntityType(entity.type));

    },
    showAttributesForEditing: function(attributes, structure){
        var model = this.createModel(attributes, structure);
        var list = this.getAttributesList();
        var store = list.getStore();
        var count = store.getCount();
        store.clearData();
        store.add(model);
        list.refresh();
        this.getPanel().setMasked(null);
        this.highlightValidationErrors(list);
        this.updateListHeight(list);
        if (count !== store.getCount()) {
            this.enableSaveButton();
        }
    },

    onListButtonExecuteInternal: function(eventData, list, entityStateUri, originalUri) {
        var data = eventData.record.get('userData');
        if (data) {
            var store = eventData.item.dataview.getStore(),
                index = store.indexOf(eventData.record),
                msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();

            this.enableSaveButton();
            if (msg && msg.indexOf('validation-header') !== -1){
                msg = null;
            }
            this.showAddAttributeDialog(data.attributes, index, list, originalUri || this.entityUri, this.parentUri, entityStateUri, msg);
        }
    },
    createModel: function (attributes, groupAttrTypes, editedEntity, showLabelAlways) {
        var entityModel;
        var modelObject = {},
            groups = {};
        groupAttrTypes.forEach(function (item) {
            modelObject[item.label] = [];
            item.values.forEach(function (attrType) {
                attributes.filter(function (item) {
                    return item.name === attrType.name;
                }).forEach(function (attribute) {
                    var showLabel = !MobileUI.core.entity.EntityUtils.isComplex(attrType) || showLabelAlways;
                    modelObject[item.label].push({
                        attrType: attrType, showLabel: showLabel, value: [attribute]
                    });
                });

            });
            groups[item.label] = item.values;
        });
        entityModel = MobileUI.core.entity.EntityUtils.createAttributesModel(modelObject,
            true, groups);
        entityModel.forEach(function (item) {
            var suggestedMode = this.entity ? this.entity.suggestedMode : editedEntity.suggestedMode;
            if (item.editor) {
                if (MobileUI.core.PermissionManager.securityService().metadata.canUpdateOrInitiate(item.editor) ||
                    (suggestedMode && MobileUI.core.PermissionManager.securityService().metadata.canInitiateRequest(item.editor))) {
                    delete item.editor.options;
                    item.editor = Ext.clone(item.editor);
                    item.editor.options = {};
                    item.editor.options.editedEntity = editedEntity;
                    item.editor.valueChangedCallback = this.onValueChanged;
                    item.editor.valueChangedContext = this;
                }
                else {
                    item.userData.attrType = item.editor;
                    delete item.editor;
                }
            }

            var uri = item.userData && item.userData.editingUri;
            if (uri) {
                var entity = this.getEditedEntity() && this.getEditedEntity().original && this.getEditedEntity().original.crosswalks;
                var crosswalks = (entity || []).
                    filter(function(xwalk) { return xwalk.attributes.indexOf(uri) !== -1; }).
                    map(function(xwalk) { return xwalk.type; });
                item.disableButtons = (crosswalks.indexOf('configuration/sources/Reltio') === -1 && crosswalks.length !== 0) ||
                    !MobileUI.core.PermissionManager.securityService().metadata.canDeleteOrInitiate(item.editor);
            }
        }, this);

        return entityModel;
    },

    onValueChanged: function(value, item, attrType, canceled) {
        if (canceled || !item.getRecord()) {
            return;
        }

        var entity = this.getEditedEntity();
        var editor = item.getRecord().get('editor');
        if (editor){
            entity = (editor.options && editor.options.editedEntity) ? editor.options.editedEntity : entity;
            entity = entity || this.editedEntity;
        }
        var label = item.getRecord().get('localizeLabel') || null;
        if (label){
            label = label.replace(' (' + value + ')', '');
        }
        var updateAttribute = function(attributes, value, label, uri, newAttribute){
            var filteredAttributes = attributes.filter(function (attribute) {
                return uri === attribute.uri;
            });
            if (filteredAttributes.length === 0 && newAttribute) {
                attributes.push(newAttribute);
                filteredAttributes = attributes;
            } else {
                filteredAttributes.forEach(function (attribute) {
                    var attrType = MobileUI.core.Metadata.findEntityAttributeByUri(null, attribute.type);
                    if (attribute.value !== value) {
                        if (attrType === 'Reference'){
                            return;
                        }
                        if (attrType && attrType.dependentLookupCode){
                            attribute.lookupCode = value;
                            attribute.displayName = label;
                        }
                        attribute.edited = true;
                        attribute.value = value;
                    }
                });
            }
            return filteredAttributes;
        };

        var uri = item.getRecord().get('userData').editingUri;

        var attributes = updateAttribute(entity.attributes, value, label, uri);
        attributes.forEach( function(item){
            updateAttribute(entity.editedAttributes, value, label, uri, item);
            updateAttribute(entity.addedAttributes, value, label, uri, this.isInCreateMode? item: null);
        },this);
        if (this.updateAttributeLabel) {
            this.updateAttributeLabel();
        }
        if (!this.saveInProgress) {
            this.enableSaveButton(entity);
        }
    },

    removeAttributeFromAttributes: function (attributes, uri, attrType, saveFirst, collect) {
        this.clearLookupsValues(uri);
        var clearLookup = function (source) {
            if (source && source.objectURI) {
                this.clearLookupsValues(source.objectURI);
            }
        }.bind(this);
        for (var i = 0; i < attributes.length; i++) {
            if (attributes[i].uri === uri && !saveFirst) {
                attributes[i].uri = '$$$DELETED$$$';
                if (MobileUI.core.entity.EntityUtils.isReference(attrType)){
                    var attribute = attributes[i];
                    clearLookup(attribute.refEntity);
                    clearLookup(attribute.refRelation);
                }
            } else if (attributes[i].parentUri === uri) {
                this.removeAttributeFromAttributes(attributes, attributes[i].uri, attrType, false, true);
            }
        }
        if (!collect) {
            return attributes.filter(function (item) {
                return item.uri !== '$$$DELETED$$$';
            });
        }
    },
    clearLookupsValues: function(uri){
        MobileUI.core.session.Session.getLookupsManager().removeValuesForAttribute(uri);
    },

    deleteAttributeInternal: function(entity, attributeUri, attrType, removeMode)
    {
        function attributeStartsWith(attribute, i) {
            return attribute.uri.indexOf(attributeUri) !== -1 ? i : -1;
        }

        function passedAttributes(index) {
            return index !== -1;
        }

        function deleteAttribute(source) {
            return function(index) {
                source.splice(index, 1);
            };
        }

        if (attributeUri && attrType) {
            var addedIndices = entity.addedAttributes
                .map(attributeStartsWith)
                .filter(passedAttributes);

            if (addedIndices.length > 0) {
                addedIndices
                    .reverse()
                    .forEach(deleteAttribute(entity.addedAttributes));
            }
            else if (removeMode !== 'clear') {
                var attr = entity.attributes.filter(function (item) {
                    return item.uri === attributeUri;
                });
                if (attrType.options && attrType.options.editedEntity) {
                    delete attrType.options.editedEntity;
                }
                entity.deletedAttributes.push({attrType: attrType, uri: attributeUri, attribute: attr.length > 0 ? attr[0]: null});
            }

            entity.editedAttributes
                .map(attributeStartsWith)
                .filter(passedAttributes)
                .reverse()
                .forEach(deleteAttribute(entity.editedAttributes));

            if (removeMode === 'clear'){
                var attribute = entity.attributes.filter(function(item){
                    return item.uri === attributeUri;
                });
                attribute = attribute.length > 0 ? attribute[0] : null;
                if (attribute !== null) {
                    if (attribute.label) {
                        attribute.label = '';
                    }
                    if (attribute.lookupCode) {
                        attribute.lookupCode = '';
                    }
                    if (attribute.displayName) {
                        attribute.displayName = '';
                    }
                    attribute.value = MobileUI.core.entity.EntityUtils.isComplex(attribute.attrType) ? {} : '';
                    delete attribute.objectURI;
                    delete attribute.refEntity;
                    delete attribute.refRelation;
                    delete attribute.relationURI;
                    delete attribute.relationshipLabel;
                }
                entity.attributes = this.removeAttributeFromAttributes(entity.attributes, attributeUri, attrType, true);
            } else {
                entity.attributes = this.removeAttributeFromAttributes(entity.attributes, attributeUri, attrType);
            }
            if (this.updateAttributeLabel) {
                this.updateAttributeLabel();
            }
        }
    },

    deleteAttribute: function(record, removeMode) {
        var data = record.get('userData'),
            attrType = record.get('editor');
        if (attrType && attrType.dependentLookupCode) {
            MobileUI.core.session.Session.getLookupsManager().removeDependentLookupValue(attrType.uri, data.editingUri);
        }
        this.deleteAttributeInternal(this.getEditedEntity(), data.editingUri, attrType, removeMode);
        this.enableSaveButton();
    },

    showComplexAttributeEditor: function(attrType, originalUri, parentUri, attributeUri, createNew)
    {
        this.getApplication().getController('dialogs.NestedAttributeEditor').showNestedEditor(attrType, originalUri,parentUri, attributeUri, createNew);
    },
    addReferenceAttribute: function(attribute, originalUri, parentUri, createNew){
        if (createNew){
            if (!MobileUI.core.SuiteManager.hasDialog('chooseEntity'))
            {
                MobileUI.core.SuiteManager.addDialog('chooseEntity', Ext.create('MobileUI.view.dialogs.ChooseEntity'));
            }
            this.getApplication().getController('dialogs.ChooseEntity').setEntityTypeFilter(MobileUI.core.Metadata.getEntityType(attribute.attrType.referencedEntityTypeURI), undefined, true);
            this.getApplication().getController('dialogs.ChooseEntity').setReferenceAttribute(attribute.attrType, parentUri, parentUri, attribute.uri);
            MobileUI.core.SuiteManager.showDialog('chooseEntity');

        }
    },
    onAttributeDelete: function(record)
    {
        if (record){
            this.enableSaveButton();
            this.deleteAttribute(record);
            this.processEditEntity(this.getEditedEntity());
        }
    },

    onSaveToolbarBtn: function() {
        this.saveInProgress = true;
        var Session = MobileUI.core.session.Session;
        var entityType = MobileUI.core.Metadata.getEntityType(this.entity.type);
        Session.getValidator().resetValidators();

        var requiredValidation = this.allRequiredAttributesFilled(this.getAttributesList(),
            entityType.attributes);
        var lookupValidation = this.validateLookups([this.getAttributesList()]);

        var validations = [requiredValidation, lookupValidation];
        var isExternalValidationNeeded = entityType.cardinality || (entityType.lifecycleActions && entityType.lifecycleActions.validate && entityType.lifecycleActions.validate.length);
        if (isExternalValidationNeeded){
            var externalValidation = Session.prepareForValidation(this.entity).then(function(result){
                return Session.validateEntity(result);
            });
            validations.push(externalValidation);
        }
        Promise.all(validations).then(function(results){
            var result = {};
            results.forEach(function (item) {
                result = MobileUI.core.util.Util.mergeWithArray(item, result);
            });
            var validationPassed = MobileUI.core.entity.EntityUtils.validationPassed(result);
            if (validationPassed) {
                this.onAfterValidation();
            } else {
                this.highlightValidationErrors(this.getAttributesList());
                this.showValidationErrorMessage(result);
                this.updateListHeight(this.getAttributesList());
                this.enableSaveButton();
            }
        }.bind(this));
    },
    validateLookups: function (lists) {
        var me = this;
        return new Promise(function (resolve) {
            MobileUI.core.session.Session.getLookupsManager().validateLookups(function (lookupValidationState) {
                if (lookupValidationState.length > 0) {
                    var labels = lookupValidationState
                        .map(function (state) {
                            return state.attrTypeUri;
                        })
                        .filter(function (uri) {
                            return uri;
                        })
                        .filter(function (item, pos, self) {
                            return self.indexOf(item) === pos;
                        })
                        .map(function (uri) {
                            return MobileUI.core.Metadata.findEntityAttributeByUri(null, uri);
                        })
                        .filter(function (item) {
                            return item;
                        })
                        .map(function (item) {
                            return item.label;
                        });

                    setTimeout(function () {
                        lists.map(function (list) {
                            return list.getStore && list.getStore();
                        }).filter(function (store) {
                            return store;
                        }).forEach(function (store) {
                            store.each(function (record) {
                                var editor = record.get('editor');
                                var value = record.get('label');
                                if (editor) {
                                    var invalid = lookupValidationState.some(function (state) {
                                        return state.attrTypeUri === editor.uri && state.values.indexOf(value) !== -1;
                                    });
                                    if (invalid) {
                                        record.set('invalid', true);
                                    } else if (record.get('invalid')) {
                                        record.set('invalid', false);
                                    }
                                }
                            });
                        });
                    }, 200);
                    resolve({invalid: labels});
                } else {
                    resolve({});
                }
            }, me);
        });
    },

    onAfterValidation: function() {
        var self = this;
        this.entityForSave = MobileUI.core.entity.TemporaryEntity.getEntity(this.originalUri);
        delete this.changeRequestId;
        this.sentEntities = [];

        var startSave = (function() {
            this.saveEntity(this.entityForSave, function(entity) {
                if (this.entityForSave.suggestedMode) {
                    if (!entity){
                        this.saveDone();
                        return;
                    }

                    this.showDCRCommentDialog()
                        .then(function(){
                            self.sendChangeRequestReview();
                            self.saveDone();
                        });
                }
                else {
                    this.saveDone(false, entity.uri);
                }
            }, this);
        }).bind(this);

        //No advanced entity
        if (this.entityForSave.suggestedMode) {
            MobileUI.core.session.Session.createDCR(function(resp) {
                    this.changeRequestId = resp.uri.split('/')[1];
                    startSave();
                },
                function(resp) {
                    MobileUI.core.Logger.error(resp);
                    MobileUI.components.MessageBox.alert(i18n('Error'), i18n('Can\'t create data change request'), this.saveDone, this);
                }, this);
        }
        else {
            startSave();
        }
    },

    saveEntity: function(entity, callback, context) {
        var editedAttributes = Array.prototype.concat.apply([], entity.addedAttributes);

        entity.editedAttributes.forEach(function(attribute){
            var exists = editedAttributes.some(function(item){
                return item.uri && attribute.uri && attribute.uri === item.uri;
            });
            if (!exists){
                editedAttributes.push(attribute);
            }
        });

        var createdReferences = editedAttributes.filter(function(attr) {
            return MobileUI.core.entity.EntityUtils.isReference(attr.attrType) &&
                MobileUI.core.entity.TemporaryEntity.isTemp(attr.refEntity.objectURI);
        });
        function replaceUri(object, original, target) {
            Object.keys(object).forEach(function(key) {
                var value = object[key];
                if (typeof value === 'object') {
                    if (key !== 'valueChangedContext') {
                        replaceUri(value, original, target);
                    }
                }
                else if (typeof value === 'string') {
                    var index = value.indexOf(original);
                    if (index !== -1) {
                        object[key] = value.substr(0, index) + target + value.substr(index + original.length);
                    }
                }
            });
        }

        var observer = Ext.create('MobileUI.core.Observer', {
            threadNames: createdReferences,
            listener: function() {
                this.saveCurrentEntity(entity, callback, context);
            },
            self: this
        });

        createdReferences.forEach(function(attr) {
            this.saveEntity(MobileUI.core.entity.TemporaryEntity.getEntity(attr.refEntity.objectURI), function(savedEntity) {
                if (savedEntity) {
                    replaceUri(entity, attr.refEntity.objectURI, savedEntity.uri);
                    MobileUI.core.entity.TemporaryEntity.addEntity(savedEntity);
                }
                observer.checkThread(attr);
            }, this);
        }, this);
    },

    saveCurrentEntity: function(entity, callback, context) {
        var addedAttributesUris = entity.addedAttributes.map(function(item) {
            return item.uri;
        });
        entity.editedAttributes = entity.editedAttributes.filter(function(item) {
            return addedAttributesUris.indexOf(item.uri) === -1;
        });
        var hasErrors = function (resp) {
            return Ext.isArray(resp) && resp.some(function (item) {
                    return item.errors;
                });
        };
        this.suggestChanges(entity, this.changeRequestId,
            function(resp) {
                if (resp.changes) { //DCR
                    var changesUris = Object.keys(resp.changes).filter(function(uri) {
                        return this.sentEntities.indexOf(uri) === -1;
                    }, this);
                    var uri = changesUris[0];
                    this.sentEntities.push(uri);
                    callback.call(context, resp.changes[uri][0].newValue);
                } else if(hasErrors(resp)){
                    MobileUI.components.MessageBox.alert(i18n('Error'), i18n('Can\'t create data change request'), function() {
                        callback.call(context);
                    });
                }
                else {
                    callback.call(context, resp[0].object);
                }
            },
            function(resp) {
                MobileUI.core.Logger.error(resp);
                MobileUI.components.MessageBox.alert(i18n('Error'), i18n('Can\'t create data change request'), function() {
                    callback.call(context);
                });
            }, this);
    },

    fixProfilePicUri: function(entity){
        var attribute = (entity.addedAttributes || []).filter(function(item){
            return item.oldUri === this.entity.defaultProfilePic;
        }, this);
        if (!Ext.isEmpty(attribute)){
            entity.defaultProfilePic = attribute[0].uri;
        }
    },
    setDefaultImage: function(){
        if (this.entity.defaultProfilePic){
            this.fixProfilePicUri(this.entity);
            MobileUI.core.session.Session.insertDefaultImageByAttributeURL(this.entity.uri, this.entity.defaultProfilePic, this.saveDone,this.saveDone, this);
        }else {
            this.saveDone();
        }
    },
    showValidationErrorMessage: function(result){
        var message = MobileUI.core.entity.EntityUtils.validationMessage(result);
        var toolbar = this.getNotificationToolbar();
        var notification = this.getNotificationMessage();
        if (notification && !Ext.isEmpty(message)){
            toolbar.setHidden(false);
            notification.setHtml(message);
            notification.setCls('validation-message');
        }
    },
    enableSaveButton: function(entity) {
        entity = entity || this.entity;
        this.saveInProgress = false;
        if (this.getSaveToolbarBtn && this.getSaveToolbarBtn() && entity) {
            if (this.actualizeSaveButton && !this.actualizeSaveButton()){
                return;
            }
            var enabled = false;
            if (entity) {
                if (!Ext.isEmpty(entity.addedAttributes)) {
                    enabled = true;
                }
                else if (!Ext.isEmpty(entity.deletedAttributes)) {
                    enabled = true;
                }
                else {
                    if (Ext.isArray(entity.attributes)) {
                        if (entity.attributes.some(function (attr) {
                                return attr.value;
                            })) {
                            enabled = true;
                        }
                    }
                    else {
                        var attrs = entity.attributes;
                        if (Object.keys(attrs).some(function (attr) {
                                return attrs[attr].value;
                            })) {
                            enabled = true;
                        }
                    }
                }
            }
            if (enabled) {
                this.getSaveToolbarBtn().setDisabled(false);
            }
        }
    },

    allRequiredAttributesFilled: function(list, allAttributes, entity) {
        entity = entity || this.entity;
        return new Promise(function(resolve){
            setTimeout(function() { //need to wait a bit, while change callback finished work
                var records = [];
                list.getStore().each(function(record){
                    records.push(record);
                });
                var requiredAttributes = MobileUI.core.session.Session.getValidator().validateRequired(allAttributes, records, entity);
                resolve({required:requiredAttributes.map(function(attr) { return attr.label; })});
            }, 200);
        });

    },

    highlightValidationErrors: function(list){
        var validator = MobileUI.core.session.Session.getValidator();
        list.getStore().each(function(record){
            var type = record.get('type');
            var types = MobileUI.components.list.items.BaseListItem.types;
            record.set('error', null);
            var error;
            switch (type){
                case types.header:
                    error = validator.collectHeaderErrors(record.get('editor').uri);
                    if (error){
                        record.set('error', error.error);
                    }
                    break;

                case types.property:
                case types.entityInfo:
                    error = validator.collectAttributeErrors(record.get('userData').editingUri, record.get('editor'));
                    if (error){
                        record.set('error', error.error);
                    }
                    break;
            }

        });
    },

    showDCRCommentDialog: function(){
        var self = this;

        return new Promise(function(resolve) {
            MobileUI.components.MessageBox.show({
                title: i18n('Suggest changes'),
                message: i18n('Your changes will be sent for review'),
                buttons: [{text: i18n('OK'),  itemId: 'ok'}],
                cls: 'commentChangeRequestDialog',
                prompt: {
                    xtype: 'textareafield',
                    placeHolder: i18n('Comment (optional)')
                },
                scope: self,
                fn: function (buttonId, comment) {
                    if (buttonId === 'ok'){
                        self.changeRequestComment = comment;
                        resolve();
                    }
                }
            });
        });
    },

    onCloseNotificationBtn: function() {
        this.getNotificationToolbar().setHidden(true);
        this.getNotificationMessage().setHtml('');
        this.getNotificationMessage().setCls('notification-message');
    },

    updateDependentLookups: function (model) {
        var lookupValidationState = MobileUI.core.session.Session.getLookupsManager().getInvalidLookupsList();
        model.forEach(function (item) {
            var editor = item.editor;
            var value = item.label;
            if (editor) {
                var invalid = lookupValidationState.some(function (state) {
                    return state.attrTypeUri === editor.uri && state.values.indexOf(value) !== -1;
                });
                if (invalid) {
                    item.invalid = true;
                } else if (item.invalid) {
                    item.invalid = false;
                }
            }
        });
    },

    sendChangeRequestReview: function(){
        var workflowPath = MobileUI.core.Services.getGeneralSettings().workflowPath;
        var workflowUrl = workflowPath + '/workflow/' + MobileUI.core.Services.getTenantName() + '/processInstances';
        var requestOptions = {
            method: 'POST',
            data: {
                processType: 'dataChangeRequestReview',
                objectURIs: ['changeRequests/' + this.changeRequestId].concat(this.sentEntities)
            },
            headers: {
                EnvironmentURL: MobileUI.core.Services.getEnvironmentUrl()
            }
        };

        if (this.changeRequestComment && this.changeRequestComment.trim()){
           requestOptions.data.comment =  this.changeRequestComment;
        }

        return new Promise(function(resolve, reject){
            if (workflowPath) {
                MobileUI.core.session.Session.sendRequest(workflowUrl, [], requestOptions, resolve, reject);
            } else {
                reject('workflowPath not specified');
            }
        });
    }
});
