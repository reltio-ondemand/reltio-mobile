/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MEntityAction', {
    /**
     * On complex attribute tap listener
     * @protected
     * @param event {Ext.direct.Event} event
     */
    onComplexAttributeTap: function (event)
    {
        if (this.enableSaveButton) {
            this.enableSaveButton();
        }

        var data = event.getData();
        var userData = data.userData;

        if (data.editor && this.refEntity && (this.refEntity.type === data.editor.referencedEntityTypeURI)) {
            //inside complex attribute
            var info = {
                attrType: data.editor,
                value: '',
                uri: userData.editingUri,
                parent: this.attribute,
                parentUri: this.attribute.uri,
                tmpEntityUri: this.tmpEntity.uri
            };
            var attrType = info.attrType,
                entityType = MobileUI.core.Metadata.getEntityType(this.refEntity.type);
            if (!MobileUI.core.SuiteManager.hasDialog('chooseEntity'))
            {
                MobileUI.core.SuiteManager.addDialog('chooseEntity', Ext.create('MobileUI.view.dialogs.ChooseEntity'));
            }
            MobileUI.core.SuiteManager.showDialog('chooseEntity');


            this.getApplication().getController('dialogs.ChooseEntity').setEntityTypeFilter(entityType, undefined, true);
            this.getApplication().getController('dialogs.ChooseEntity').setReferenceAttribute(info, this.tmpEntity.uri);
            this.getApplication().getController('dialogs.ChooseEntity').on('refEntitySelected',
                function(info, newReference) {
                    attrType.valueChangedCallback.call(attrType.valueChangedContext,
                        newReference, {getRecord: function() {return event;}}, attrType);
                    this.showComplexAttributeEditor(this.attribute.uri, this.editable && this.tmpEntity.uri);
                }, this, {single: true});
        }
        else {
            if (userData && userData.uri) {
                if (this.beforeComplexEditorOpened)
                {
                    this.beforeComplexEditorOpened();
                }
                if (data.editor && MobileUI.core.entity.EntityUtils.isReference(data.editor.type))
                {
                    if (userData.actionLabel) {
                        if (!MobileUI.core.SuiteManager.hasDialog('referenceDialog')) {
                            MobileUI.core.SuiteManager.addDialog('referenceDialog', Ext.create('MobileUI.view.dialogs.ReferenceAttributeEditor'));
                        }
                        MobileUI.core.SuiteManager.showDialog('referenceDialog');

                        this.getApplication().getController('dialogs.ReferenceAttributeEditor').editRelation(data.editor, this.originalUri, this.parentUri, userData.uri);
                    }
                    else {
                        this.addReferenceAttribute({attrType:data.editor, uri: data.userData.uri}, this.originalUri, this.parentUri, true);
                    }
                }else {
                    if (this.showComplexAttributeEditor && data.editor ){
                        this.showComplexAttributeEditor(data.editor, this.originalUri, this.parentUri, userData.uri, this.isInCreateMode && Ext.isEmpty(data.label));
                    }
                    else if (userData.attrType) {
                        //TODO: show complex attribute without editing
                        this.showComplexAttributeEditor(userData.attrType, this.originalUri, this.parentUri, userData.uri, this.isInCreateMode && Ext.isEmpty(data.label));
                    }
                    else {
                        var uri = userData.uri.indexOf('relations/') === 0 && !this.inRelation ? this.parsedParams.entityUrl+ '$_relations_$'+ userData.uri : userData.uri;

                        if (this.parsedParams && uri.indexOf(this.parsedParams.entityUrl) !== 0 && this.parsedParams.attributeUrl)
                        {
                            uri = uri.split('/attributes');
                            uri = this.parsedParams.entityUrl + '/' + this.parsedParams.attributeUrl + uri[1];
                        }
                        MobileUI.core.Navigation.redirectForward(this.getBaseUrl() +
                            MobileUI.core.util.HashUtil.encodeUri(uri));
                    }
                }
            }
            else if (data.editor && MobileUI.core.entity.EntityUtils.isComplex(data.editor.type)) {
                //Empty complex attribute
                var store = event.item.dataview.getStore(),
                    index = store.indexOf(event.item.getRecord()),
                    msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();

                store.removeAt(index);
                this.entity = this.attribute.refRelation;
                this.showAddAttributeDialog([data.editor], index, this.getComplexAttributesList(), this.entity.objectURI, this.tmpEntity.uri, null, msg);
            }
        }
    },
    /** @private */
    getBaseUrl: function() {
        return  'profile/';
    },

    fixEntityUri: function(uri, temporaryUri) {
        if (temporaryUri) {
            uri = uri + '/' + temporaryUri;
        }
        return uri;
    },

    onProfileTitleTap: function (event)
    {
        var userData = event.getData().userData;
        if (userData && userData.actionLabel)
        {
            MobileUI.core.Navigation.redirectForward('profile/' + userData.actionLabel.replace(/\//g, MobileUI.core.util.HashUtil.VALUE_SEPARATOR));
        }
    },
    /**
     * On Reference attribute select listener
     * @protected
     * @param event {Ext.direct.Event} event
     */
    onReferenceAttributeSelect: function (event)
    {
        if (!this.editable) {
            var userData = event.getData().userData;
            if (userData && userData.actionLabel) {
                this.showComplexAttributeEditor(userData.actionLabel, this.editable && this.url);
            }
        }
    }
});
