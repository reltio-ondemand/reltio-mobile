/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MProfile', {
    requires: ['MobileUI.core.entity.EntityUtils',
        'MobileUI.core.Metadata',
        'MobileUI.core.Services',
        'MobileUI.core.entity.EntityItemUtils',
        'MobileUI.core.Navigation',
        'MobileUI.components.list.MListHeight',
        'MobileUI.controller.MProfileConfiguration',
        'MobileUI.components.MessageBox'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        actions: 'MobileUI.controller.MEntityAction',
        profileConfiguration: 'MobileUI.controller.MProfileConfiguration'
    },
    statics: {
        TREE_VIEW: 'com.reltio.plugins.entity.TreeView',
        prevEntityUri: null
    },
    /**@type {String} @private} */
    url: null,
    editable: false,
    entity: null,
    originalUri: null,

    onListButtonExecute: function() {
        var tmpEntity = MobileUI.core.entity.EntityUtils.makeEditableEntity(this.entity);
        tmpEntity.suggestedMode = !MobileUI.core.PermissionManager.securityService().metadata.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.UPDATE, this.entity.type) &&
            MobileUI.core.PermissionManager.securityService().metadata.canInitiateRequest(this.entity.type);
        MobileUI.core.session.Session.setEditSubMode(tmpEntity.suggestedMode? 'suggest': 'edit');
        MobileUI.core.entity.TemporaryEntity.addEntity(tmpEntity);
        MobileUI.core.Navigation.redirectForward('edit/' +
            MobileUI.core.util.HashUtil.encodeUri(tmpEntity.uri));
    },
    /**
     * On pull refresh listener
     */
    onPullRefresh: function () {
        this.showEntityInternal(true);
    },
    /**
     * Method shows entity
     * @param params {Array} params
     */
    showEntity: function (params) {
        this.url = params.join('/');
        var viewActivator = this.getApplication().getController('ViewActivator');
        if (viewActivator.waitForActivate()) {
            viewActivator.on('activated', function () {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1) {
                    this.showEntityInternal();
                }
            }, this, {single: true});
        }
        else {
            this.showEntityInternal();
        }
    },
    /**
     * Method updates entity
     * @param force {Boolean?} if true the request will be sent
     */
    showEntityInternal: function (force) {
        if (MobileUI.core.Navigation.isBackAllow()) {
            this.getBackToolbarBtn().setHidden(false);
        }
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        if (MobileUI.core.session.Session.getEntityUri() === this.url && !force) {
            this.processEntityRequest(MobileUI.core.session.Session.getEntity());
        }
        else {
            MobileUI.core.Logger.info('Trying to request entity:' + this.url);
            MobileUI.core.session.Session.requestEntity(this.url, this.processEntityRequest, function (resp) {
                MobileUI.core.Logger.log(resp);

                if (resp) {
                    MobileUI.components.MessageBox.alert(i18n('Error'), resp.errorMessage);
                }

                this.getPanel().setMasked(null);
            }, this, false, true);
        }
    },
    /**
     * Method processes entity request
     * @param entity {Object}
     */
    processEntityRequest: function (entity) {
        this.entity = entity;
        var extension = this.getProfileConfiguration(entity.type),
            groupsConfig = extension && extension.attributes || [],
            hiddenAttributes = extension && extension.hiddenAttributes || [],
            groupAttrTypes = {},
            attributes, filtered;

        this.fillBand(this.getProfileBand(), entity);
        this.getProfileBand().show();
        attributes = this.prepareAttributes(entity, groupsConfig, hiddenAttributes);
        attributes = Object
            .keys(attributes)
            .reduce(function (acc, key) {
                acc[key] = attributes[key].filter(function (item) {
                    return item.value.length > 0;
                });
                return acc;
            }, {});
        filtered = this.processConnections(entity.type, attributes, this.editable);
        this.fillAttributesList(filtered, groupAttrTypes, entity);
        this.trackActivity(entity);
    },
    /**
     * @private
     * Method prepares the attributes
     * @param entity {Object} api response
     * @param groupsConfig {Array}
     * @param hiddenAttributes {Array}
     * @return {Object}
     */
    prepareAttributes: function (entity, groupsConfig, hiddenAttributes) {
        var attributes = MobileUI.core.entity.EntityUtils.processAttributes(entity, groupsConfig, hiddenAttributes);
        return attributes;
    },
    /**
     * Method fills profile attribute list.
     * @private
     * @param attributes {Object} entities attributes
     * @param groupAttrTypes {Object}
     */
    fillAttributesList: function (attributes, groupAttrTypes, entity) {
        var list, store,
            entityModels = MobileUI.core.entity.EntityUtils.createAttributesModel(attributes,
                this.editable, groupAttrTypes);

        if (MobileUI.core.PermissionManager.securityService().metadata.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.UPDATE, entity.type)) {
            entityModels.unshift({
                type: MobileUI.components.list.items.BaseListItem.types.buttons,
                label: i18n('Edit profile'),
                avatar: 'resources/images/reltio/list/pen.svg',
                info: 'separate',
                lastInGroup: true
            });
        } else if (MobileUI.core.PermissionManager.securityService().metadata.canInitiateRequest(entity.type)) {
            entityModels.unshift({
                type: MobileUI.components.list.items.BaseListItem.types.buttons,
                label: i18n('Suggest profile changes'),
                avatar: 'resources/images/reltio/list/pen.svg',
                info: 'separate',
                lastInGroup: true
            });
        }

        list = this.getAttributesList();
        store = list.getStore();
        store.clearData();
        store.add(entityModels);
        list.refresh();
        this.getPanel().setMasked(null);
        this.updateListHeight(list);
    },

    onGlobalFilterChanged: function() {
        MobileUI.core.SuiteManager.on('hideDialogs', function(){
            this.onPullRefresh();
        }, this, {single: true});
    },

    trackActivity: function(entity) {
        if (MobileUI.controller.MProfile.prevEntityUri !== entity.uri) {
            MobileUI.core.session.Session.describeActivity(null, 'USER_PROFILE_VIEW',
                JSON.stringify({uri: entity.uri, label: entity.label}));
            MobileUI.controller.MProfile.prevEntityUri = entity.uri;
        }
    }
});
