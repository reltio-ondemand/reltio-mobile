/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MProfileBand', {
    /**
     * Method fills profile band
     * @param band {profileband} profile band
     * @param entity {Object} entity
     */
    fillBand: function (band, entity)
    {
        var realEntity = entity.original ? entity.original : entity;

        var eu = MobileUI.core.entity.EntityUtils,
            attributes = this.parseAttributes(realEntity),
            bc = this.parseBusinessCard(realEntity, attributes),
            entityType, image, badgeColor, alternativeImage;

        entityType = MobileUI.core.Metadata.getEntityType(realEntity.type) || '';
        if (entity.original){
            var profilePicUri = entity.defaultProfilePic || realEntity.defaultProfilePic;
            var sources = eu.getImageSources(entity).filter(function(item){
                return item.uri === profilePicUri ;
            });
            image = Ext.isEmpty(sources) ? null: sources[0].value;
        }
        else if (realEntity.defaultProfilePicValue) {
            image = realEntity.defaultProfilePicValue;

            if (Ext.isObject(image)) {
                image = eu.getUrlThumbnail(image);
            }
        } else {
            image = eu.getImageAttributeValue(realEntity, realEntity.defaultProfilePic);
        }
        alternativeImage = MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage);
        badgeColor = entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR;
        band.setAvatar(image, MobileUI.core.Services.getBorderlessImageUrl(alternativeImage));
        band.setFavorite(realEntity.isFavorite);

        var EntityUtils = MobileUI.core.entity.EntityUtils;

        band.setText(EntityUtils.buildPatternProperty(realEntity, EntityUtils.LABEL),
            EntityUtils.buildPatternProperty(realEntity, EntityUtils.SECONDARY_LABEL),
            bc.join(', '));
        band.setBadgeColor(badgeColor);
    },

    /**
     * On favorite tap
     */
    processFavorite: function(mark, uri, success, self)
    {
        var logger = function(item){
            MobileUI.core.Logger.error(item);
        };
        if (mark)
        {
            MobileUI.core.session.Session.markEntity(uri,success,logger,self);
        } else
        {
            MobileUI.core.session.Session.unmarkEntity(uri,success,logger,self);
        }
    },

    /**
     * The block tap listener
     */
    onBlockTap: function()
    {
       if (MobileUI.core.session.Session.getEntityUri())
       {
           var uri = MobileUI.core.session.Session.getEntityUri();
           MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
           MobileUI.core.Navigation.redirectTo('profile/' + MobileUI.core.util.HashUtil.encodeUri(uri));
       }
    },

    parseAttributes: function(entity) {
        var attrTypes = {},
            entityType = MobileUI.core.Metadata.getEntityType(entity.type);

        entityType.attributes.forEach(function (attr) {
            attrTypes[attr.uri] = attr;
            attrTypes[attr.name] = attr;
        });

        return attrTypes;
    },

    parseBusinessCard: function(entity, attrTypes) {
        var metadata = MobileUI.core.Metadata,
            entityType = metadata.getEntityType(entity.type),
            businessCardAttributes = [],
            i, j;
        if (entity.attributes && entityType.businessCardAttributeURIs) {
            for (i = 0; i < entityType.businessCardAttributeURIs.length; i++) {
                var attrType = attrTypes[entityType.businessCardAttributeURIs[i]];
                if (attrType) {
                    var attr = entity.attributes[attrType.name];
                    if (attr) {
                        if (Ext.isArray(attr)) {
                            var survived = '';
                            for (j = 0; j < attr.length; j++) {
                                if (attr[j].ov === true) {
                                    survived = attr[j];
                                    break;
                                }
                            }
                            attr = survived;
                        }

                        var text = attr.hasOwnProperty('label') ?
                            (attr.relationshipLabel ? (attr.relationshipLabel + ', ') : '') + attr.label :
                            attr.value;

                        if (typeof text !== 'string') {
                            text = MobileUI.core.entity.EntityUtils.evaluateLabel(entity, text, attrType);
                        }

                        businessCardAttributes.push(text);
                    }
                }
            }
        }
        return businessCardAttributes;
    }
});
