/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MProfileFacets', {
    requires: ['MobileUI.core.entity.EntityUtils',
        'MobileUI.core.Metadata',
        'MobileUI.core.relations.RelationsUtils',
        'MobileUI.core.Services',
        'MobileUI.core.session.Session',
        'MobileUI.core.Observer',
        'MobileUI.controller.MAttributesFacets',
        'MobileUI.core.facet.FacetUtils'
    ],
    mixins: {
        'attributesFacets': 'MobileUI.controller.MAttributesFacets'
    },
    statics: {
        DEFAULT_IMAGE: 'http://reltio.images.s3.amazonaws.com/api/images/defaultImage/no-photo.png'
    },
    /**@type {Array} @private*/
    facetsRequests: null,
    /**@type {Array} @private*/
    treeRequests: null,
    attributesFacet: null,
    connectionsObserver: null,
    /**@type {String} @private */
    facetsEntityType: null,

    entityType: null,
    attributes: null,

    refreshProfileFacets: function() {
        if (this.entityType && this.attributes) {
            this.processConnections(this.entityType, this.attributes, true);
        }
    },

    /**
     * Method processes connection
     * @param entityType {String} entity type
     * @param attributes {Object} attributes
     * @param keepAttributes {Boolean} do not modify attributes
     */
    processConnections: function (entityType, attributes, keepAttributes)
    {
        this.entityType = entityType;
        this.attributes = Ext.clone(attributes);

        var configuration = MobileUI.core.Services.getConfigurationManager(),
            FacetUtils = MobileUI.core.facet.FacetUtils,
            perspective, views, i, view, requestsContent, filterFunction, result, url, keys, key,
            contains = function (a, b)
            {
                return b.indexOf(a) !== -1;
            };
        this.attributesFacet = [];
        this.facetsRequests = [];
        this.treeRequests = [];
        this.facetsEntityType = entityType;
        perspective = configuration.findPerspective(entityType);
        perspective = configuration.getExtensionById(perspective);
        result = FacetUtils.createPredefinedFacets(attributes, configuration, keepAttributes);
        this.attributesFacet.push(result.attributesFacet);
        if (perspective && perspective.views)
        {
            views = perspective.views.filter(function(item){
                return item.id !== MobileUI.core.facet.FacetUtils.PLACES_FACET;
            });
            for (i = 0; i < views.length; i++)
            {
                view = Ext.clone(configuration.getExtensionById(views[i]));
                if (view['class'] === MobileUI.core.facet.FacetUtils.TREE_VIEW)
                {
                    this.treeRequests.push(view);
                }
                else if (view['class'] === MobileUI.core.facet.FacetUtils.ATTRIBUTES_FACET)
                {
                    keys = Object.keys(view.attributes);
                    if (!Ext.isEmpty(keys))
                    {
                        key = keys[0];
                        url = FacetUtils.createFacetsUrl(MobileUI.core.facet.FacetUtils.ATTRIBUTES_FACET, view.id);
                        filterFunction = FacetUtils.makeFilterFunction(key, view.attributes[key], contains);
                        result = FacetUtils.convertAttributesToFacets(view.caption, view.image, url, view.id, attributes, filterFunction, keepAttributes);
                        this.attributesFacet.push(result.attributesFacet);
                    }
                } else if (view['class'] === MobileUI.core.facet.FacetUtils.CUSTOM_FACET) {
                    url = FacetUtils.createFacetsUrl(MobileUI.core.facet.FacetUtils.CUSTOM_FACET, view.id);
                    this.attributesFacet.push({
                            badgeText: '',
                            caption: view.caption,
                            id: view.id,
                            image: view.image,
                            defaultImage: view.image,
                            action: url,
                            alwaysVisible: true
                    });
                }
                else
                {
                    MobileUI.core.relations.RelationsUtils.processRelations(view);
                    this.facetsRequests.push(view);
                }
            }
            this.connectionsObserver = Ext.create('MobileUI.core.Observer', {
                threadNames: ['tree', 'relations'],
                listener: this.updateProfileFacets,
                self: this
            });
            if (!Ext.isEmpty(this.facetsRequests))
            {
                requestsContent = this.facetsRequests.map(function (item)
                {
                    return item.content;
                });
                MobileUI.core.session.Session.requestRelationsImmediately(requestsContent, null, null, 0, null, this.showConnections, function(){
                    this.facetsRequests.forEach(function (request) {
                        request.total = 0;
                        request.alwaysVisible = true;
                    });
                    this.connectionsObserver.checkThread('relations');
                }, this);
            }
            else
            {
                this.connectionsObserver.checkThread('relations');
            }
            if (!Ext.isEmpty(this.treeRequests))
            {
                requestsContent = this.treeRequests.map(function (item)
                {
                    return item.graph ? item.graph.type : '';
                });
                MobileUI.core.session.Session.requestTree(requestsContent, this.showTreeConnections, function(){
                    (this.treeRequests || []).forEach(function(request){
                        request.total = 0;
                        request.alwaysVisible = true;
                    }, this);
                    this.connectionsObserver.checkThread('tree');
                }, this);
            }
            else
            {
                this.connectionsObserver.checkThread('tree');
            }
        }
        this.updateProfileFacets();
        return result.attributes;
    },
    /**
     * Method shows connections
     * @param resp {Array}
     */
    showConnections: function (resp)
    {
        var i;
        if (Ext.isArray(resp))
        {
            if (resp.length === this.facetsRequests.length)
            {
                for (i = 0; i < this.facetsRequests.length; i++)
                {
                    this.facetsRequests[i].total = resp[i].total || 0;
                }
            }
        }
        this.connectionsObserver.checkThread('relations');
    },
    showTreeConnections: function (resp)
    {
        var i, calculateTotal = function(prev, item){
            return prev + item.total;
        };
        //TODO: fix it after discussion with stepan;
        for (i = 0; i < this.treeRequests.length; i++)
        {
            var childCounts = Ext.isArray(resp.root.children) ?
                resp.root.children.reduce(calculateTotal, 0)
                : 0;
            this.treeRequests[i].total = resp.root.total + childCounts + 1;
        }
        this.connectionsObserver.checkThread('tree');
    },

    updateProfileFacets: function ()
    {
        if (this.editable) {
            return;
        }

        var viewId, sortedFacets,
            profileFacets = this.getProfileFacets(),
            configuration = MobileUI.core.Services.getConfigurationManager(),
            facetsRequests = this.facetsRequests || [],
            treeRequests = this.treeRequests || [],
            facets, allFacets, perspective, i, j,
            FacetUtils = MobileUI.core.facet.FacetUtils,
            mapFunction = function (item)
            {
                var defaultImg = MobileUI.controller.MProfileFacets.DEFAULT_IMAGE,
                    entityType;

                if (item.content && !Ext.isEmpty(item.content.entityTypes) && item.content.entityTypes[0])
                {
                    entityType = MobileUI.core.Metadata.getEntityType(item.content.entityTypes[0]) || '';
                    defaultImg = MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage);
                }
                if (item.content && item.contentSecondLevel)
                {
                    item['class'] = MobileUI.core.facet.FacetUtils.TWO_HOPS_FACET;
                }
                return {
                    badgeText: item.total,
                    id: item.id,
                    alwaysVisible: item.alwaysVisible,
                    caption: item.caption,
                    image: item.image || MobileUI.core.Services.getBorderlessImageUrl(defaultImg),
                    defaultImage: MobileUI.core.Services.getBorderlessImageUrl(defaultImg),
                    action: FacetUtils.createFacetsUrl(item['class'], item.id)
                };
            };
        if (profileFacets) {
            facets = facetsRequests.map(mapFunction).map(function(item) {
                item.alwaysVisible = true;
                return item;
            }).concat(treeRequests.map(mapFunction));
            allFacets = this.attributesFacet.concat(facets);
            perspective = configuration.findPerspective(this.facetsEntityType);
            perspective = configuration.getExtensionById(perspective);
            sortedFacets = [];
            if (perspective && Ext.isArray(perspective.views))
            {
                for (i = 0; i < perspective.views.length; i++)
                {
                    viewId = perspective.views[i];
                    for (j =0; j<allFacets.length;j++)
                    {
                      if (allFacets[j].id === viewId)
                      {
                          sortedFacets.push(allFacets[j]);
                          break;
                      }
                    }
                    allFacets.splice(j,1);
                }
            }
            sortedFacets = sortedFacets.concat(allFacets);
            profileFacets.setFacets(sortedFacets);
        }
    }

});
