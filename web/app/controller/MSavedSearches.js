/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MSavedSearches', {
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'Ext.LoadMask',
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.core.PermissionManager',
        'MobileUI.components.facets.DateRangeSearchFacet',
        'MobileUI.components.MessageBox'
    ],
    mixins: {
        searchView: 'MobileUI.controller.MSavedSearchesView'
    },
    statics: {
        /**
         * @param savedSearch {{}}
         * @return {{keyword: string, facets: {}, params: Array}|null}
         */
        extractSavedSearchUIData: function(savedSearch) {
            var state = savedSearch && savedSearch.uiState && savedSearch.uiState.state;
            if (!state) {
                return null;
            }

            var model = state.model;
            if (!model) {
                return null;
            }

            var arr = [].concat(state.facets,
                    (state.advanced && state.advanced.searchParams) || [],
                    (state.advanced && state.advanced.fastSearchParams) || []),
                params = [],
                keyword = '',
                facets = {},
                i;

            for (var fieldName in  model) {
                if (model.hasOwnProperty(fieldName)) {
                    if (fieldName === '$$keyword') {
                        keyword = model[fieldName].labels[0];
                    }
                    else {
                        var param = {
                            fieldName: fieldName,
                            filter: model[fieldName].filter || 'equals',
                            labels: model[fieldName].labels
                        };

                        if ((fieldName === 'type') && param.labels) {
                            param.labels = param.labels.map(i18n);
                        }

                        for (i = 0; i < arr.length; i++) {
                            if (arr[i].fieldName === fieldName) {
                                param.values = arr[i].values;
                                param.title = arr[i].title;
                                if (arr[i].uri){
                                    param.uri = arr[i].uri;
                                }
                                if (arr[i].facetClass === 'com.reltio.plugins.search.dev.DateRangeSearchFacet')
                                {
                                    param.subQuery = arr[i].subQuery;
                                }
                            }
                        }

                        if (!param.values || (param.values.length === 0)) {
                            for (i = 0; i < arr.length; i++) {
                                if (arr[i].children) {
                                    for (var j = 0; j < arr[i].children.length; j++) {
                                        if (arr[i].children[j].fieldName === fieldName) {
                                            param.values = arr[i].children[j].values;
                                            param.title = arr[i].children[j].title;
                                        }
                                    }
                                }
                            }
                        }

                        if (fieldName === 'tags' ||fieldName === MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME ) {
                            facets[fieldName] = param;
                        }
                        else {
                            params.push(param);
                        }
                    }
                }
            }

            return {keyword: keyword, facets: facets, params: params};
        }
    },

    /**
     * @private
     * @param {String} uri
     * @param {Function} [callback]
     * @param {*} [self]
     */
    deleteSavedSearch: function(uri, callback, self) {
        MobileUI.components.MessageBox.show({
            title: i18n('Confirm'),
            message: i18n('Delete Saved Search?'),
            buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
            promptConfig: false,
            scope: this,
            fn: function (buttonId) {
                if (buttonId === 'yes') {
                    var list = this.getSearchesList && this.getSearchesList();
                    if (list) {
                        list.setMasked(true);
                    }
                    MobileUI.core.session.Session.deleteSavedSearch(uri, callback, callback, self);
                    MobileUI.core.search.SearchParametersManager.removeGlobalFilterUrl(uri);
                }
            }
        });
    },

    /**
     * @param {String} savedSearchUri
     */
    showSavedSearch: function(savedSearchUri) {
        MobileUI.core.session.Session.getSavedSearch(savedSearchUri, function(savedSearch) {
                var processed = MobileUI.controller.MSavedSearches.extractSavedSearchUIData(savedSearch);
                MobileUI.core.search.SearchParametersManager.setTypeAhead(processed.keyword);
                MobileUI.core.search.SearchParametersManager.setSavedSearch(savedSearch);
                MobileUI.core.search.SearchParametersManager.setKeyword(processed.keyword);
                MobileUI.core.search.SearchParametersManager.setFacets(processed.facets);
                MobileUI.core.search.SearchParametersManager.setAdvancedSearchParameters(processed.params);
                MobileUI.core.SuiteManager.hideDialogs(true);
                MobileUI.core.Navigation.redirectForward('search/init');
            },
            function(error) {
                MobileUI.core.Logger.log(error);
            }, this);
    },

    /**
     * @private
     * @param {Function} callback
     * @param {*} self
     */
    save: function(callback, self) {
        function saved(json) {
            var uri;

            if (Ext.isArray(json) && json.length !== 0) {
                uri = Ext.isObject(json[0].object) ? json[0].object.uri : null;
            }
            uri = this.savedSearch.uri || uri;

            if (this.setAsGlobalFilter === true) {
                MobileUI.core.search.SearchParametersManager.addGlobalFilterUrl(uri);
            }
            else if (this.setAsGlobalFilter === false) { //Can be undefined, so check needed
                MobileUI.core.search.SearchParametersManager.removeGlobalFilterUrl(uri);
            }
            if (this.setSavedSearchUri) {
                this.setSavedSearchUri(uri);
            }

            callback.apply(self);
        }

        function failed(error) {
            MobileUI.components.MessageBox.alert(i18n('Save failed'), error, Ext.emptyFn);
        }

        var appliedSavedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch(),
            user = MobileUI.core.session.Session.getUser(),
            fullName;

        if (this.savedSearch) {
            if (!this.savedSearch.uri || appliedSavedSearch.uri === this.savedSearch.uri) {
                appliedSavedSearch.uri = this.savedSearch.uri;
                appliedSavedSearch.name = this.savedSearch.name;
                appliedSavedSearch.isPublic = this.savedSearch.isPublic;
                appliedSavedSearch.isFavorite = true;
                this.savedSearch = appliedSavedSearch;
            }
            fullName = user ? user.full_name : '';
            if (this.savedSearch.owner !== fullName &&
                this.savedSearch.uri) {
                saved.call(this);
            }
            else {
                this.savedSearch.name = this.savedSearch.name || (this.getSuggestedName && this.getSuggestedName());
                if (!Ext.isEmpty(this.savedSearch.name)) {
                    if (this.getPropertiesList) {
                        this.getPropertiesList().setMasked(true);
                    }

                    if (this.savedSearch.uri) {
                        MobileUI.core.session.Session.updateSavedSearch(this.savedSearch,
                            saved, failed, this);
                    }
                    else {
                        MobileUI.core.session.Session.createSavedSearch(this.savedSearch,
                            saved, failed, this);
                    }
                }
                else {
                    MobileUI.components.MessageBox.alert(i18n('Save failed'), i18n('Empty name is not allowed'), Ext.emptyFn);
                }
            }
        }
        else {
            callback.apply(self);
        }
    }
});
