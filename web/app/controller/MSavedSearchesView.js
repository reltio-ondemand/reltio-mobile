/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MSavedSearchesView', {
    requires: [],

    /**
     * @param name {String}
     * @param changeHandler {Function}
     * @param context {Object|null}
     * @returns {Object}
     */
    createNameTextfield: function(name, changeHandler, context) {
        return {
            type: MobileUI.components.list.items.BaseListItem.types.custom,
            editor: function() {
                return Ext.create('Ext.field.Text', {
                    cls: 'input-text',
                    value: name || '',
                    placeHolder: i18n('Name'),
                    listeners: {
                        change: changeHandler,
                        scope: context
                    }
                });
            }
        };
    },

    /**
     * @param label {String}
     * @param on {Boolean}
     * @param changeHandler {Function}
     * @param context {Object}
     * @param last {Boolean}
     * @returns {Object}
     */
    createToggle: function(label, on, changeHandler, context, last) {
        return {
            type: MobileUI.components.list.items.BaseListItem.types.toggle,
            label: label,
            property: on,
            editor: function() {
                changeHandler.apply(context, arguments);
            },
            lastInGroup: last
        };
    },

    /**
     * @returns {Object}
     */
    createDeleteButton: function() {
        return {
            type: MobileUI.components.list.items.BaseListItem.types.buttons,
            label: [i18n('Delete')],
            info: 'saved-search-delete-button',
            lastInGroup: true
        };
    }
});
