/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MSuggestEdit', {
    statics: {
        modifyEntity: function(entity, modified, forValidation) {

            var makeAttr = function(attr) {
                var cleanAttributes = function(attribute){
                  if (attribute && Ext.isObject(attribute.value)){
                      for (var key in attribute.value){
                          if (attribute.value.hasOwnProperty(key)){
                              if (Ext.isArray(attribute.value[key])) {
                                  /*jshint loopfunc: true */
                                  attribute.value[key] = attribute.value[key].filter(function (attributeValue) {
                                      var clean = MobileUI.core.entity.TemporaryEntity.isTemp(attributeValue.uri) &&
                                          (Ext.isEmpty(attributeValue.value) || 
                                          Ext.isObject(attributeValue.value) && Object.keys(attributeValue.value).length === 0);
                                      return !clean;
                                  });
                                  /*jshint loopfunc: false */
                                  if (Ext.isEmpty(attribute.value[key])) {
                                      delete attribute.value[key];
                                  }
                              }
                          }
                      }
                  }
                };
                var result = {
                    ov: attr.ov,
                    type: attr.type,
                    uri: attr.uri,
                    value: attr.value
                };
                if (attr.refEntity){
                    result.refEntity = attr.refEntity;
                }
                if (attr.refRelation){
                    result.refRelation = attr.refRelation;
                }
                cleanAttributes(result);
                return result;
            };

            var modifiedAttributes = [];
            if (modified){
                if (modified.addedAttributes) {
                   var addedAttributes = modified.addedAttributes.filter(function (attribute) {
                       if (attribute.value !== null || attribute.value !== undefined){
                           return !Ext.isEmpty(attribute.value);
                       }
                       return true;
                   });
                    Array.prototype.push.apply(modifiedAttributes, addedAttributes);
                }
                if (modified.editedAttributes) {
                    Array.prototype.push.apply(modifiedAttributes, modified.editedAttributes);
                }
                Array.prototype.push.apply(modifiedAttributes, (modified.deletedAttributes || []).map(function (attr) {
                    var result = Ext.clone(attr.attribute);
                    result.attrType = attr.attrType;
                    result.deleted = true;
                    result.uri = attr.uri;
                    return result;
                }));
            }
            var findReferenceAttribute = function(entity, uri){
                return MobileUI.core.util.Util.flatten(Object.keys((entity && entity.attributes) || {}).map(function(attributeKey){
                    return entity.attributes[attributeKey];
                })).filter(function(item){
                    return item.uri === uri;
                })[0];
            };
            var copyPathToParent = function (original, parentUri, entityUri, value) {
                var parent = MobileUI.core.entity.EntityUtils.findAttribute(original.attributes, parentUri);
                if (!parent){
                    return value;
                }
                if (value) {
                    var oldValue = value;
                    value = {};
                    value.topParent = {
                        name: parent.attribute ? parent.attribute.name : parent.name,
                        uri: parent.attribute ? parent.attribute : parent.uri,
                        value: {}
                    };
                    value.topParent.value[oldValue.topParent.name] = [oldValue.topParent];
                    value.parent = oldValue.parent;
                } else {
                    value = {
                        parent: {
                            name: parent.attribute ? parent.attribute.name : parent.name,
                            uri: parent.attribute ? parent.attribute.uri : parent.uri,
                            value: {}
                        }
                    };
                    value.topParent = value.parent;
                }
                parentUri = parent.attribute ? parent.attribute.parentUri : parent.parentUri;
                if (parent.attribute && parent.attribute.parentUri === entityUri) {
                    return {parent:value, topParent: value};
                } else {
                    return copyPathToParent(original, parentUri, entityUri, value);
                }
            };
            modifiedAttributes.forEach(function(modifiedAttr) {
                if (modifiedAttr.attrType && modifiedAttr.attrType.type === 'Reference') {
                    var attributes;
                    var existing = !(modified.addedAttributes || []).some(function(attribute){
                        return attribute.uri === modifiedAttr.uri;
                    });
                    if (existing){
                        var referenceInSlice = findReferenceAttribute(entity, modifiedAttr.uri);
                        if (referenceInSlice) {
                            attributes = referenceInSlice.value || {};
                            modifiedAttr.refRelation = referenceInSlice.refRelation ? referenceInSlice.refRelation
                                : {
                                    objectURI: modifiedAttr.refRelation.objectURI,
                                    type: modifiedAttr.refRelation.type
                                };
                        } else {
                            attributes = {};
                            modifiedAttr.refRelation = {
                                objectURI: modifiedAttr.refRelation.objectURI,
                                type: modifiedAttr.refRelation.type
                            };
                        }
                        modifiedAttr.refEntity = {
                            objectURI: modifiedAttr.refEntity.objectURI,
                            type: modifiedAttr.refEntity.type
                        };
                        delete modifiedAttr.type;


                        modifiedAttr.value = attributes;
                    } else {
                        attributes = modifiedAttr.value || (modifiedAttr.value = {});
                        if (modifiedAttr.refRelation && !forValidation) {
                            delete modifiedAttr.refRelation.objectURI;
                        }
                    }
                    var relation = {attributes: attributes, uri: modifiedAttr.relationURI};
                    var relEntity = MobileUI.core.entity.TemporaryEntity.getEntity(modifiedAttr.relationURI);
                    if (existing) {
                        MobileUI.core.util.Util.forEach(attributes, function (key, value, obj) {
                            if (key === 'uri' && obj[key]) {
                                obj[key] = value.replace(modifiedAttr.uri, modifiedAttr.relationURI + '/attributes');
                            }
                        }, null, true);
                        MobileUI.core.util.Util.forEach(relEntity, function (key, value, obj) {
                            if (key === 'uri' && obj[key]) {
                                obj[key] = value.replace(modifiedAttr.uri, modifiedAttr.relationURI + '/attributes');
                            }
                        }, null, true, ['editor', 'valueChangedContext']);
                    }
                    MobileUI.controller.MSuggestEdit.modifyEntity(relation, relEntity, forValidation);
                    var refEntity = MobileUI.core.entity.TemporaryEntity.getEntity(modifiedAttr.refEntity.objectURI);
                    if (refEntity && !existing) {
                        modifiedAttr.attrType.referencedAttributeURIs.forEach(function (refAttrUri) {
                            var attr = MobileUI.core.entity.EntityUtils.findAttributeByTypeUri(refEntity.attributes, refAttrUri);
                            if (attr && attr.length > 0) {
                                var attrType = MobileUI.core.Metadata.findEntityAttributeByUri(null, refAttrUri);
                                if (!attrType) {
                                    attrType = MobileUI.core.Metadata.findRelationAttributeByUri(null, refAttrUri);
                                }
                                attributes[attrType.name] = makeAttr(attr[0]);
                            }
                        });
                    }
                }

                var parent = MobileUI.core.entity.EntityUtils.findAttribute(entity.attributes, modifiedAttr.parentUri);
                parent = (entity.uri === modifiedAttr.parentUri) ? entity.attributes :
                    (parent && (parent.attribute && parent.attribute.value || parent.value));


                if (!parent){
                    parent = MobileUI.core.entity.EntityUtils.findAttribute(modified.original.attributes, modifiedAttr.parentUri);
                    var tmpAttributes;
                    if (parent) {
                        if (parent.attribute && parent.attribute.parentUri === entity.uri) {
                            var tmpAttribute = {
                                name: parent.attribute.name,
                                uri: parent.attribute.uri,
                                value: {}
                            };
                            if (parent.attribute.refRelation) {
                                tmpAttribute.refRelation = {
                                    objectURI: parent.attribute.refRelation.objectURI
                                };
                            }
                            if (!entity.attributes[parent.attribute.name]) {
                                entity.attributes[parent.attribute.name] = [];
                            }
                            tmpAttributes = entity.attributes[parent.attribute.name].filter(function (item) {
                                return item.uri === tmpAttribute.uri;
                            });
                            if (Ext.isEmpty(tmpAttributes)) {
                                entity.attributes[parent.attribute.name].push(tmpAttribute);
                                parent = tmpAttribute.value;
                            } else {
                                parent = tmpAttributes[0].value;
                            }
                        } else {
                            var result = copyPathToParent(modified.original, parent.uri, entity.uri);
                            if (!entity.attributes[result.topParent.name]) {
                                entity.attributes[result.topParent.name] = [];
                            }
                            tmpAttributes = entity.attributes[result.topParent.name].filter(function (item) {
                                return item.uri === result.topParent.uri;
                            });
                            if (Ext.isEmpty(tmpAttributes)) {
                                entity.attributes[result.topParent.name].push(result.topParent);
                            }

                            parent = result.parent.value;
                        }
                    }
                }
                if (parent) {
                    var values = parent[modifiedAttr.name] || (parent[modifiedAttr.name] = []);
                    var uriEnd = modifiedAttr.uri.substr(modifiedAttr.uri.lastIndexOf('/') + 1);
                    var found = false;
                    for (var i = values.length - 1; i >= 0; i--) {
                        var existedUriEnd = values[i].uri.substr(values[i].uri.lastIndexOf('/') + 1);
                        if (existedUriEnd === uriEnd) {
                            if (modifiedAttr.deleted) {
                                values.splice(i, 1);
                            }
                            else {
                                values[i] = makeAttr(modifiedAttr);
                                found = true;
                            }
                            break;
                        }
                    }
                    if (!found && !modifiedAttr.deleted) {
                        var needNewAttribute = true;
                        var attrType = modifiedAttr.attrType ? modifiedAttr.attrType : MobileUI.core.Metadata.findEntityAttributeByUri(null, modifiedAttr.type);

                        if (attrType && attrType.cardinality && attrType.cardinality.minValue === 1 && !forValidation){
                            var nonOvReltio = MobileUI.core.entity.EntityUtils.findAttributeByTypeUri(entity.attributes, attrType.uri);
                            if (!Ext.isEmpty(nonOvReltio)){
                                values.filter(function(value){
                                    return nonOvReltio[0].uri === value.uri;
                                }).forEach(function(value){
                                    needNewAttribute = false;
                                    Object.keys(value)
                                        .filter(function (key) {
                                            return key !== 'uri';
                                        })
                                        .forEach(function (key) {
                                            delete value[key];
                                        });
                                    var newValue = makeAttr(modifiedAttr);
                                    Object.keys(newValue)
                                        .filter(function (key) {
                                            return key !== 'uri';
                                        })
                                        .forEach(function (key) {
                                            value[key] = newValue[key];
                                        });
                                });
                            }
                        }
                        if (needNewAttribute){
                            values.push(makeAttr(modifiedAttr));
                        }
                    }
                }
            });
        }
    },

    suggestChanges: function (entity, changeRequestId, success, failure, self) {
        var isTemp = !!MobileUI.core.entity.TemporaryEntity.isTemp(entity.uri);

        if (isTemp) {
            var result = {
                roles: [],
                type: entity.type,
                uri: entity.uri,
                attributes: {}
            };
            MobileUI.controller.MSuggestEdit.modifyEntity(result, entity);
            MobileUI.core.session.Session.createEntity(result, changeRequestId, success, failure, this);
        }
        else {
            MobileUI.core.session.Session.requestSlicedEntity(entity.uri, function(resp) {
                MobileUI.controller.MSuggestEdit.modifyEntity(resp, entity);
                MobileUI.core.session.Session.overrideEntity(resp, success, failure, self, changeRequestId);
            }, function(err) {
                MobileUI.core.Logger.error(err);
                failure(err);
            }, this);
        }
    }
});
