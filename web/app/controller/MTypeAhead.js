/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.MTypeAhead', {
    requires: ['MobileUI.core.search.SearchParametersManager'],
    oldValue: null,
    /**
     * @protected
     * On clear icon listener
     */
    onClearIcon: function ()
    {
        if (this.typeaheadBehaviour()) {
            MobileUI.core.search.SearchParametersManager.setTypeAhead('');
        }
        this.clearList();
        this.getSearchField().focus();
    },

    /**
     * @protected
     * search field keyup listner
     */
    onKeyUp: function ()
    {
        this.value = this.getSearchField().getValue();
        MobileUI.core.search.SearchParametersManager.setTypeAhead(this.value);
        if (this.value === this.oldValue)
        {
           return;
        }
        this.oldValue = this.value;
        if (this.value && this.value.length > 0)
        {
            this.onTypeAheadSearch();
        }
        else
        {
            this.clearList();
        }
    },

    /**
     * Method clears data
     */
    clearList: function ()
    {
        var list = this.getList();
        if (list) {
            var store = list.getStore();
            store.clearData();
            list.refresh();
            if (!this.typeaheadBehaviour()) {
                this.hideAddNewEntity();
            }
        }
    },

    dataReceived: function(result, search) {
        if (this.value !== search.value) {
            return;
        }
        result = MobileUI.core.Services.getSearchResultBuilder().buildContent(result, true);

        var list = this.getList(),
            store = list && list.getStore();
        if (store) {
            if (this.offset === 0) {
                list.showSpinner(false);
                store.clearData();
            }
            store.add(result);
        }
        if (!this.typeaheadBehaviour()) {
            list.fetchDone(result.length === this.limit);
        }
    }
});
