/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.NewEditMode', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfileBand',
        'MobileUI.components.editablelist.MAttributesPanel',
        'MobileUI.components.editablelist.MTransactions',
        'MobileUI.components.editablelist.MShowEditor',
        'MobileUI.components.editablelist.editors.ChooseAttributeType',
        'MobileUI.components.editablelist.editors.DependentLookupValues',
        'MobileUI.components.editablelist.editors.EntitySelector',
        'MobileUI.components.editablelist.editors.NestedAttributeEditor',
        'MobileUI.components.editablelist.editors.ReferenceAttributeEditor',
        'MobileUI.components.editablelist.editors.SelectImageEditor',
        'MobileUI.core.Services',
        'MobileUI.core.session.Session'
    ],
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        profileBand: 'MobileUI.controller.MProfileBand',
        profileConfiguration: 'MobileUI.controller.MProfileConfiguration',
        attributesPanel: 'MobileUI.components.editablelist.MAttributesPanel',
        transactions: 'MobileUI.components.editablelist.MTransactions',
        showEditor: 'MobileUI.components.editablelist.MShowEditor'
    },
    config: {
        refs: {
            facetProfileBand: 'facetprofileband[name=newEditModeProfileBand]',
            attributesList: 'elist[name=newEditModeAttributesList]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            title: 'component[name=title]',
            panel: 'panel[name=editableProfile]',
            cancelToolbarBtn: 'button[name=newEditModeCancelToolbarBtn]',
            saveToolbarBtn: 'button[name=newEditModeSaveToolbarBtn]',
            notificationToolbar: 'component[name=new-notificationToolbar]',
            notificationMessage: 'component[name=new-notificationMessage]',
            closeNotificationBtn: 'button[name=new-closeNotificationBtn]'
        },
        control: {
            attributesList: {
                selectItem: 'onListSelectItem',
                changeValue: 'onListChangeValue',
                addAttribute: 'onListAddAttribute',
                'delete': 'onListDeleteAttribute',
                recalculate: 'recalculateHeightDelayed'
            },
            facetProfileBand: {
                avatarTap: 'onAvatarTap'
            },
            cancelToolbarBtn: {
                tap: 'onCancelToolbarButtonTap'
            },
            saveToolbarBtn: {
                tap: 'onSaveButtonTap'
            },
            closeNotificationBtn: {
                tap: 'onCloseNotificationBtn'
            }
        },
        views: [
            'MobileUI.view.NewEditMode'
        ]
    },

    entityUri: null,
    entity: null,
    entityType: null,
    createMode: false,

    launch: function () {

    },

    getSource: function () {
        return {
            uri: this.entity.uri,
            value: this.branch.getCurrentProjection()
        };
    },

    getCurrentEntity: function () {
        return this.createMode ?
            this.entity :
            MobileUI.core.session.Session.getModifiedEntity();
    },

    entryPoint: function (paramsArray) {
        this.entityUri = paramsArray.join('/').replace('uri//', 'uri$$$');

        if (MobileUI.core.entity.EntityUtils.isTemporaryUri(this.entityUri)) {
            this.createMode = true;
            var entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri);
            if (entity) {
                this.onSuccessRequestEntity(entity);
            } else {
                MobileUI.core.Navigation.redirectForward('dashboard/init');
            }

        } else {
            this.createMode = false;
            MobileUI.core.session.Session.requestEntity(this.entityUri, this.onSuccessRequestEntity, this.onFailRequestEntity, this, false, true);
        }
        this.getDoneButton().setDisabled(this.createMode);
    },
    getTitleText: function(entity){
        if (this.createMode) {
            var canCreate = MobileUI.core.PermissionManager.securityService().metadata.canCreate(entity.type);
            var template = 'Create %1';

            if (!canCreate) {
                template = 'Suggest new %1';
            }
            return i18n(template, Ext.util.Format.htmlEncode(MobileUI.core.Metadata.getEntityType(entity.type).label));
        } else {
            return i18n('Edit Profile');
        }
    },

    onFailRequestEntity: function (response) {
        MobileUI.core.Logger.log(response);

        if (response) {
            MobileUI.components.MessageBox.alert(i18n('Error'), response.errorMessage);
        }

        this.getPanel().setMasked(null);
    },

    onSuccessRequestEntity: function (entity) {
        this.getTitle().setHtml(this.getTitleText(entity));
        this.errors = null;
        this.entity = entity;
        this.entityType = MobileUI.core.Metadata.getEntityType(entity.type);
        this.defaultProfilePic = entity.defaultProfilePic;
        MobileUI.core.entity.TemporaryEntity.removeAllEntities();

        MobileUI.core.session.Session.getLookupsManager().initWithEntityType(this.entity.type);
        MobileUI.core.session.Session.getLookupsManager().fillValuesFromEntity(this.entity);
        MobileUI.core.session.Session.getLookupsManager().cleanTouchedList();

        this.createTransaction(this.getCurrentEntity().attributes);
        this.initTransaction(entity.uri + '/attributes', this.transaction);

        this.refreshList();
        this.initProfileBand();

        this.getSaveToolbarBtn().setDisabled(false);
    },

    refreshList: function () {
        var list = this.getAttributesList();
        var store = list.getStore();
        var attrTypes = this.createMode ? (this.entityType.attributes || []).filter(function (attrType) {
            return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
        }) : this.entityType.attributes;
        this.getModelsFromSource(this.entityType, attrTypes, this.getProfileConfiguration(this.entity.type)).then(function (models) {
            store.clearData();
            store.add(models);

            list.refresh();

            var task = Ext.create('Ext.util.DelayedTask', function () {
                this.updateListHeight(list);
            }, this);
            task.delay(500);
        }.bind(this));
    },

    returnToViewMode: function (forceRedirect) {
        if (MobileUI.core.Navigation.isBackAllow() && !forceRedirect) {
            MobileUI.core.Navigation.back();
        } else {
            MobileUI.core.Navigation.redirectForward('profile/' + MobileUI.core.util.HashUtil.encodeUri(this.entityUri));
        }
    },

    onSaveButtonTap: function () {
        this.getSaveToolbarBtn().setDisabled(true);
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        this.branch.save();

        var entity = this.getCurrentEntity();
        entity.attributes = this.transaction.getCurrentProjection();

        MobileUI.core.session.Session.validateAll(entity)
            .then(function (errors) {
                if (errors.length) {
                    this.errors = errors;
                    this.initTransaction(this.entity.uri + '/attributes', this.transaction);
                    this.refreshList();
                    this.showValidationErrors();
                    this.getSaveToolbarBtn().setDisabled(false);
                    this.getPanel().setMasked(null);
                    return false;
                } else {
                    return MobileUI.core.session.Session.saveAllPendingChanges(entity);
                }
            }.bind(this))
            .then(function (result) {
                if (result.changes) {
                    this.showChangeRequestReviewDialog(result);
                } else if (result !== false) {
                    var entity = Ext.isArray(result) ? result[0].object : result;
                    this.entityUri = entity.uri;
                    MobileUI.core.session.Session.resetCurrentEntity();

                    this.returnToViewMode(this.createMode);
                }
            }.bind(this));
    },

    showChangeRequestReviewDialog: function (saveResult) {
        MobileUI.components.MessageBox.show({
            title: i18n('Suggest changes'),
            message: i18n('Your changes will be sent for review'),
            buttons: [{text: i18n('OK'), itemId: 'ok'}],
            cls: 'commentChangeRequestDialog',
            prompt: {
                xtype: 'textareafield',
                placeHolder: i18n('Comment (optional)')
            },
            scope: this,
            fn: function (buttonId, comment) {
                if (buttonId === 'ok') {
                    MobileUI.core.session.Session.sendChangeRequestToReview(saveResult.uri, Object.keys(saveResult.changes), comment)
                        .then(this.returnToViewMode.bind(this, false));
                }
            }
        });
    },
    getDoneButton: function(){
        return this.getSaveToolbarBtn();
    },

    onCancelToolbarButtonTap: function () {
        MobileUI.components.MessageBox.show({
            title: i18n('Warning'),
            message: i18n('Are you sure you want to cancel all changes?'),
            buttons: [{text: i18n('No'), itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
            promptConfig: false,
            scope: this,
            fn: function (buttonId) {
                if (buttonId === 'yes') {
                    this.returnToViewMode();
                }
            }
        });
    },

    onCloseNotificationBtn: function () {
        this.getNotificationToolbar().setHidden(true);
        this.getNotificationMessage().setHtml('');
        this.getNotificationMessage().setCls('notification-message');
    },

    initProfileBand: function () {
        var band = this.getFacetProfileBand();

        band.setEditMode(true);
        this.fillBand(band, this.getCurrentEntity());
    },

    onAvatarTap: function () {
        var editor = this.showEditor('MobileUI.components.editablelist.editors.SelectImageEditor', function (editor) {
            editor.init(this.getCurrentEntity(), this.branch);
            editor.refreshList();
        });

        editor.on('done', function (data) {
            var defaultProfilePic = data.defaultProfilePic;
            var defaultProfilePicValue = data.defaultProfilePicValue;
            var band = this.getFacetProfileBand();
            if (defaultProfilePic && MobileUI.core.entity.EntityUtils.isTemporaryUri(defaultProfilePic)) {
                this.getCurrentEntity().defaultProfilePic = this.defaultProfilePic;
            } else {
                this.getCurrentEntity().defaultProfilePic = defaultProfilePic;
            }
            this.getCurrentEntity().defaultProfilePicValue = defaultProfilePicValue;
            var entity = Ext.merge(this.getCurrentEntity(), {attributes: this.getSource().value});
            this.fillBand(band, entity);
            this.refreshList();
        }, this, {order: 'after'});
    }
});
