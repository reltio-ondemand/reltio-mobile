/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.NewPassword', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.session.Session', 'MobileUI.core.Logger', 'MobileUI.components.MessageBox', 'MobileUI.core.Navigation'],
    config: {
        refs: {
            password: 'passwordfield[name=newPasswordField]',
            rePassword: 'passwordfield[name=re-newPasswordField]',
            submitButton: 'button[name=submitButton]'
        },
        control: {
            submitButton: {
                tap: 'onSubmit'
            }
        },
        views: [
            'MobileUI.view.NewPassword'
        ]
    },
    token: '',
    /**
     * Set token from parameters
     * @param token {String} token
     */
    setToken: function(token){
        this.token = token;
    },
    /**
     * @private
     * @param text{String} error message
     */
    showError: function(text){
        MobileUI.components.MessageBox.alert(i18n('Error'), text);
    },
    /**
     * @private
     * on submit listener
     */
    onSubmit: function() {
        var first = this.getPassword().getValue(),
            second = this.getRePassword().getValue();
        if (this.token.length === 0) {
            this.showError(i18n('Malformed token'));
            return;
        }
        if (first.length < 1) {
            this.showError(i18n('Your password must be at least 1 characters length'));
            return;
        }
        if (first !== second) {
            this.showError(i18n('Please make sure that your password confirmation matches your new password'));
            return;
        }
        MobileUI.core.session.Session.restorePassword(this.token, first,function(result){
            if (result.status && result.status === 'login') {
                MobileUI.core.Navigation.redirectForward(result.status);
            } else {
                MobileUI.core.Navigation.redirectForward('password/passwordnotreset');
            }
        },function(){
            MobileUI.core.Navigation.redirectForward('password/passwordnotreset');
        }, this);

    }

});
