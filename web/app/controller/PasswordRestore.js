/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.PasswordRestore', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.Navigation'],
    config: {
        refs: {
            infoLabel: 'label[name=result]',
            backButton: 'button[name=backButton]'
        },
        control: {
            backButton: {
                tap: 'onBack'
            }
        },
        views: [
            'MobileUI.view.PasswordRestore'
        ]
    },
    /**@type {String}*/
    url: null,

    setMode: function (mode)
    {
        if (mode === 'passwordreset')
        {
            this.getInfoLabel().setHtml('<span class="x-bold">' + i18n('Your password was successfully reset.') + '</span>' +
                                        '<span style="display: inline-block;">' + i18n('To access your account, please check your email for additional instructions.') + '</span>');
            this.url = 'login';
        }
        else if (mode === 'invalidcaptcha')
        {
            this.getInfoLabel().setHtml('<span class="x-bold">' + i18n('The characters you entered are incorrect.') + '</span>' +
                                        '<span style="display: inline-block;">' + i18n('Please go back and enter a valid character sequence as requested.') + '</span>');

            this.url = 'forgotpassword';
        }
        else if ((mode === 'passwordnotreset'))
        {
            this.getInfoLabel().setHtml('<span class="x-bold">' + i18n('We could not reset your password.') + '</span>' +
                                        '<span style="display: inline-block;">' + i18n('You may check your username and try resetting your password again.') + '</span>');
            this.url = 'forgotpassword';
        }

    },

    onBack: function ()
    {
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
        MobileUI.core.Navigation.redirectTo(this.url);
    }
});
