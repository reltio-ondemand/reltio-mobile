/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.PlacesFacet', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfileBand',
        'MobileUI.controller.MEntityAction',
        'MobileUI.controller.MAddAttribute',
        'MobileUI.components.list.MListHeight',
        'MobileUI.core.relations.RelationsUtils',
        'MobileUI.core.entity.EntityUtils',
        'MobileUI.core.facet.FacetUtils',
        'MobileUI.controller.MProfileBand'
    ],
    mixins: {
        profileBand: 'MobileUI.controller.MProfileBand',
        action: 'MobileUI.controller.MEntityAction',
        addAttribute: 'MobileUI.controller.MAddAttribute',
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        profileConfiguration: 'MobileUI.controller.MProfileConfiguration'
    },
    config: {
        refs: {
            profileBand: 'facetprofileband[name=placesFacetProfileBand]',
            placesFacet: 'elist[name=placesFacetList]',
            placesMap: 'reltiomap[name=placesMap]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            panel: 'panel[name=placesfacet]'
        },
        control: {
            placesFacet: {
                recalculate: 'recalculateHeight',
                entityTap: 'onProfileTitleTap',
                edit: 'onComplexAttributeTap'
            },
            panel: {
                pullRefresh: 'onPlacesFacetPullRefresh'
            },
            placesMap: {
                expanded: 'onMapExpanded',
                beforeCollapse: 'onBeforeMapCollapse',
                beforeExpand: 'onBeforeMapExpand'
            },
            profileBand: {
                blockTap: 'onBlockTap',
                favorited: 'onFavoriteTap'
            }
        },
        views: [
            'MobileUI.view.PlacesFacet'
        ]
    },
    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    this.getBackToolbarBtn().setHidden(false);
                }
            }
        }, this);
    },
    parsedEntityParams: null,
    showPlacesFacet: function (params)
    {
        var viewActivator = this.getApplication().getController('ViewActivator');
        this.parsedEntityParams = this.parseUriParams(params);

        if (viewActivator.waitForActivate())
        {
            viewActivator.on('activated', function ()
            {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
                {
                    this.showPlacesFacetInternal();
                }
            }, this, {single: true});
        }
        else
        {
            this.showPlacesFacetInternal();
        }
    },
    onPlacesFacetPullRefresh: function ()
    {
        this.showPlacesFacetInternal(true);
    },
    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getProfileBand().setFavorite(item);
        },this);
    },
    /**
     * Method shows the places attributes
     * @private
     * @param force {Boolean?} should refresh entity
     */
    showPlacesFacetInternal: function (force)
    {
        if (MobileUI.core.Navigation.isBackAllow())
        {
            this.getBackToolbarBtn().setHidden(false);
        }
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedEntityParams.entityUrl && !force)
        {
            this.processPlacesAttributes(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.Logger.info('Trying to request entity:' + this.parsedEntityParams.entityUrl);
            MobileUI.core.session.Session.requestEntity(this.parsedEntityParams.entityUrl, this.processPlacesAttributes, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },

    processPlacesAttributes: function (resp)
    {
        var list, store, attributes, filtered, model, locations, j, extension, hiddenAttributes,
            configuration = MobileUI.core.Services.getConfigurationManager();
        this.fillBand(this.getProfileBand(), resp);
        extension = this.getProfileConfiguration(resp.type);
        if (extension)
        {
            hiddenAttributes = extension.hiddenAttributes;
        }
        attributes = MobileUI.core.entity.EntityUtils.processAttributes(resp, null, hiddenAttributes);
        filtered = MobileUI.core.facet.FacetUtils.createPredefinedFacets(attributes, configuration).attributesFacet.attributes;
        locations = [];

        var length = filtered.map(function(item){
           return item.value ? item.value.length : 0;
        });
        var observer = Ext.create('MobileUI.core.Observer', {
            threadNames: MobileUI.core.util.Util.generateArithmeticSequences(length),
            listener: function () {
                this.getPlacesMap().setMarkers(locations);
            },
            self: this
        });
        var counter = 0;
        for (var itemIndex = 0; itemIndex < filtered.length; itemIndex++) {
            var item = filtered[itemIndex];
            var i, value, geoLocation, loc, lat, lon, point;

            if (Ext.isArray(item.value)) {
                for (i = 0; i < item.value.length; i++) {
                    value = item.value[i].value;
                    geoLocation = value['GeoLocation'];
                    if (geoLocation) {
                        for (j = 0; j < geoLocation.length; j++) {
                            loc = geoLocation[j];
                            lat = loc.value['Latitude'];
                            lon = loc.value['Longitude'];
                            if (lat && lat.length > 0 && lat[0].value && lon && lon.length > 0 && lon[0].value) {
                                point = new google.maps.LatLng(Number(lat[0].value), Number(lon[0].value));
                                locations.push({label: item.value[i].label, point: point});
                            }
                        }
                        observer.checkThread(counter);
                    } else if (item.value[i].label) {
                        /*jshint loopfunc: true */
                        MobileUI.core.util.Util.locateGeoposition(item.value[i].label, MobileUI.core.session.Session, function(err, lat, lng){
                            if (!err) {
                                point = new google.maps.LatLng(lat, lng);
                                locations.push({label: this.label, point: point});
                            }
                            observer.checkThread(this.index);
                        }, {label: item.value[i].label, index: counter});
                        /*jshint loopfunc: false */
                    } else {
                        observer.checkThread(counter);
                    }
                    counter++;
                }
            }
        }
        list = this.getPlacesFacet();
        store = list.getStore();
        store.clearData();
        model = MobileUI.core.entity.EntityUtils.createAttributesModel({'Address': filtered});
        store.add(model);
        this.updateListHeight(list);

    },
    /**
     * Method parse the params
     * @private
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string}}
     */
    parseUriParams: function (params)
    {
        return {
            entityUrl: params.join('/')
        };
    },
    /**
     * Map expanded listener
     */
    onMapExpanded: function ()
    {
        this.getPlacesFacet().setHidden(true);
        this.getPanel().setScrollable(false);
    },
    /**
     * Map expanded listener
     */
    onBeforeMapCollapse: function ()
    {
        this.getPanel().setScrollable(true);
        this.getPlacesFacet().setHidden(false);
        this.updateListHeight(this.getPlacesFacet());
    },

    onBeforeMapExpand: function ()
    {
        this.updateListHeight(this.getPlacesFacet());
    }
});
