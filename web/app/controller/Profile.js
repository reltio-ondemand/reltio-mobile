/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Profile', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfile',
        'MobileUI.controller.MProfileFacets',
        'MobileUI.controller.MProfileBand'
    ],
    mixins: {
        baseProfile: 'MobileUI.controller.MProfile',
        profileFacets: 'MobileUI.controller.MProfileFacets',
        profileBand: 'MobileUI.controller.MProfileBand'
    },
    config: {
        refs: {
            profileBand: 'profileband[name=profileBand]',
            facetProfileBand: 'facetprofileband[name=facetProfileBand]',
            profileFacets: 'profilefacets[name=profileFacets]',
            attributesList: 'elist[name=attributesList]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            panel: 'panel[name=profile]',
            cancelToolbarBtn: 'button[name=cancelToolbarBtn]',
            saveToolbarBtn: 'button[name=saveToolbarBtn]'
        },
        control: {
            attributesList: {
                edit: 'onComplexAttributeTap',
                selectItem:'onReferenceAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                execute: 'onListButtonExecute',
                'delete': 'onAttributeDelete',
                entityTap: 'onProfileTitleTap'
            },
            profileBand: {
                favorited: 'onFavoriteTap'
            },
            panel: {
                pullRefresh: 'onPullRefresh'
            },
            cancelToolbarBtn: {
                tap: 'onCancelToolbarBtn'
            }
        },
        views: [
            'MobileUI.view.Profile'
        ]
    },
    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getProfileBand().setFavorite(item);
        },this);
    },
    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                MobileUI.core.search.SearchParametersManager.on('globalFilterChanged', this.onGlobalFilterChanged, this);

                if (MobileUI.core.Navigation.isBackAllow()) {
                    var back = this.getBackToolbarBtn();
                    if (back) {
                        back.setHidden(false);
                    }
                }
            }
        }, this);
        this.getApplication().getController('ViewActivator').on('deactivated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1) {
                MobileUI.core.search.SearchParametersManager.un('globalFilterChanged', this.onGlobalFilterChanged, this);
            }
        }, this);
    },

    saveDone: function() {
        MobileUI.core.entity.TemporaryEntity.removeAllEntities();
        MobileUI.core.session.Session.resetCurrentEntity();
        MobileUI.core.Navigation.redirectForward('profile/' +
            MobileUI.core.util.HashUtil.encodeUri(this.originalUri));
    }

});
