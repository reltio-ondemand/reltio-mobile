/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.RelationAttributes', {
    extend: 'Ext.app.Controller',
    mixins: {
        band: 'MobileUI.controller.MProfileBand',
        list: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.controller.MProfileBand',
        'MobileUI.core.Navigation'
    ],
    config: {
        refs: {
            relationAttributesList: 'elist[name=relationAttributesList]',
            profileBand: 'facetprofileband[name=profileBand]',
            panel: 'panel[name=relationAttributes]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            title: 'component[name=relationTitle]'
        },
        control: {
            relationAttributesList: {
                edit: 'onComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete',
                selectItem: 'onItemSelect',
                execute: 'onEditButtonExecute'
            },
            profileBand: {
                blockTap :'onBlockTap'
            }
        },
        views: [
            'MobileUI.view.RelationAttributes'
        ]
    },
    entity: null,
    inRelation: true,

    parsedParams:null,
    /**
     * @private
     * method creates params from array of url params
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string, relationUrl: string}}
     */
    parseParams: function(params){
            return {
                relationId: params[0],
                entityUrl: params.splice(1, 2).join('/'),
                relationUrl: params.splice(1, params.length - 1).join('/')
            };
    },

    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    var back = this.getBackToolbarBtn();
                    if (back) {
                        back.setHidden(false);
                    }
                }
            }
        }, this);
    },
    /**
     * Method shows relation attributes
     * it is entry point to this controller
     * @param params {Array}
     */
    showRelation: function(params){
        this.parsedParams = this.parseParams(params);
        var viewActivator = this.getApplication().getController('ViewActivator');
        if (viewActivator.waitForActivate())
        {
            viewActivator.on('activated', function ()
            {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
                {
                    this.showRelationAttributesInternal();
                }
            }, this, {single: true});
        }
        else
        {
            this.showRelationAttributesInternal();
        }
    },
    /**
     * @private
     * The internal method which is provide caching for the entity
     */
    showRelationAttributesInternal: function(){
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedParams.entityUrl)
        {
            this.processRelationAttributes(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelationAttributes, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },
    /**
     * Method processes entity and fill band
     * @param entity {Object} entity object
     */
    processRelationAttributes: function(entity){
        this.entity = entity;
        MobileUI.core.session.Session.getRelation(this.parsedParams.relationUrl, this.onRelationsResponse , function(){
            this.getPanel().setMasked(false);
        }, this);
        this.fillBand(this.getProfileBand(), entity);
    },
    /**
     * Method fills the
     * @param response
     */
    onRelationsResponse: function(response){
        var list, store,
            other = [],
            groups = {},
            key, i,
            attrType,attr,relationType,
            relation = response[0],
            relationAttributesModel;

        var config =  MobileUI.core.Services.getConfigurationManager().getExtensionById(this.parsedParams.relationId);
        var caption = (config && config.caption) || '';
        this.getTitle().setHtml(i18n('View %1', Ext.util.Format.htmlEncode(i18n(caption))));
        var hiddenAttributes = (config && config.hiddenRelationAttributes) || [];
        relationType = MobileUI.core.Metadata.getRelationType(relation.type);
        groups.Other = [];
        if (Ext.isArray(relationType.attributes)) {
            for (i = 0; i < relationType.attributes.length; i++) {
                attrType = relationType.attributes[i];
                attr = relation.attributes[attrType.name];
                if (!attr || hiddenAttributes.indexOf(attr[0].type) !== -1) {
                    continue;
                }
                attr = {value: attr, attrType: attrType, showLabel: true};
                if (MobileUI.core.entity.EntityUtils.isComplex(attrType)) {
                    key = attrType.label;
                    if (!groups[key]) {
                        groups[key] = [];
                    }
                    attr.showLabel = false;
                    groups[key].push(attr);
                }
                else {
                    other.push(attr);
                }
            }
            groups.Other = other;
        }
        groups.Other.push.apply(groups.Other, MobileUI.core.relations.RelationsUtils.processSpecialAttributes(relation, hiddenAttributes));

        relationAttributesModel = MobileUI.core.entity.EntityUtils.createAttributesModel(groups, false, {});
        var connectedObject = [relation.startObject, relation.endObject].filter(function(item){
            return item.objectURI !== this.entity.uri;
        }, this);
        if (connectedObject.length > 0) {
            connectedObject = connectedObject[0];
            relationAttributesModel.unshift({
                type: MobileUI.components.list.items.BaseListItem.types.actionProperty,
                label: MobileUI.core.entity.EntityUtils.processLabel(connectedObject.label),
                userData: connectedObject.objectURI,
                property: MobileUI.core.Metadata.getEntityType(connectedObject.type).label
            });
        }
        if (MobileUI.core.PermissionManager.securityService().metadata.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.UPDATE, relation.type)) {
            relationAttributesModel.unshift({
                type: MobileUI.components.list.items.BaseListItem.types.buttons,
                label: i18n('Edit relation'),
                avatar: 'resources/images/reltio/list/pen.svg',
                info: 'separate',
                lastInGroup: true
            });
        }
        list = this.getRelationAttributesList();
        store = list.getStore();
        store.clearData();
        store.add(relationAttributesModel);
        list.refresh();
        this.getPanel().setMasked(null);
        this.updateListHeight(list);
    },
    /**
     * Complex relation attribute select listener
     * @param record
     */
    onComplexAttributeSelect: function(record){
        MobileUI.core.Navigation.redirectForward('relation/complex$' + MobileUI.core.util.HashUtil.encodeUri(record.get('userData').uri));
    },
    /**
     * The entity tap listener
     * @param record
     */
    onItemSelect: function (record) {
        if (record.get('type') === MobileUI.components.list.items.BaseListItem.types.actionProperty) {
            MobileUI.core.Navigation.redirectForward('profile/' +
                MobileUI.core.util.HashUtil.encodeUri(record.get('userData')));
        }
    },
    onEditButtonExecute: function(){
        var path = [this.parsedParams.relationId, this.parsedParams.entityUrl, this.parsedParams.relationUrl, 'relation'].join('/');
        MobileUI.core.Navigation.redirectForward('relation/edit$' + MobileUI.core.util.HashUtil.encodeUri(path), false);
    }
});
