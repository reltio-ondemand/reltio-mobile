/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.RelationComplexAttributes', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfile',
        'MobileUI.controller.MComplexAttributes',
        'MobileUI.controller.MProfileBand'
    ],
    mixins: {
        baseProfile: 'MobileUI.controller.MProfile',
        complexAttributes: 'MobileUI.controller.MComplexAttributes',
        profileBand: 'MobileUI.controller.MProfileBand'
    },
    config: {
        refs: {
            complexAttributesList: 'elist[name=relationComplexAttributesList]',
            crumb: 'crumb[name=relationComplexCrumb]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            title: 'component[name=relationComplexTitle]',
            panel: 'panel[name=relaioncomplexattributes]',
            cancelToolbarBtn: 'button[name=cancelComplexToolbarBtn]',
            doneToolbarBtn: 'button[name=doneToolbarBtn]'
        },
        control: {
            complexAttributesList: {
                edit:'onComplexAttributeTap',
                recalculate: 'recalculateHeightDelayed',
                execute: 'onComplexListButtonExecute',
                'delete': 'onComplexAttributeDelete'
            },

            cancelToolbarBtn: {
                tap: 'onCancelComplexToolbarBtn'
            },

            doneToolbarBtn: {
                tap: 'onDoneComplexToolbarBtn'
            }
        },
        views: [
            'MobileUI.view.RelationComplexAttributes'
        ]
    },

    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    var back = this.getBackToolbarBtn();
                    if (back) {
                        back.setHidden(false);
                    }
                }
            }
        }, this);
    },
    /**
     * Parses the url params
     * @param params {Array}
     * @returns {{relationUri: string, attributeUrl: string}}
     */
    parseUriParams: function(params){
        var relationUri = '',
            attribute,
            attributeUrl = '';
        if (Ext.isArray(params))
        {
            if (params.length > 2)
            {
                attribute = params.splice(2, params.length - 2);
                attributeUrl = attribute.join('/');
            }
            relationUri = params.join('/');
        }

        return {relationUri: relationUri, attributeUrl: attributeUrl};
    },
    /**
     * Entry posint
     * @param params {Array}
     */
    showRelation: function (params)
    {
        var viewActivator = this.getApplication().getController('ViewActivator');
        this.parsedParams = this.parseUriParams(params);
        if (this.parsedParams) {
            if (viewActivator.waitForActivate()) {
                viewActivator.on('activated', function() {
                    if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1) {
                        this.showComplexRelationAttributeInternal();
                    }
                }, this, {single: true});
            }
            else {
                this.showComplexRelationAttributeInternal();
            }
        }
    },
    /**
     * Method gets the relation and shows the complex attribute in list
     */
    showComplexRelationAttributeInternal: function(){
        MobileUI.core.session.Session.getRelation(this.parsedParams.relationUri, this.processComplexRelationAttribute , function(){
            this.getPanel().setMasked(false);
        }, this);
    },
    /**
     * Method processes the relation response
     * @param response {Object} relation
     */
    processComplexRelationAttribute: function(response){
        var relation = response[0];
        var attrUri = this.parsedParams.relationUri + '/' + this.parsedParams.attributeUrl,
            EntityUtils = MobileUI.core.entity.EntityUtils,
            path = EntityUtils.collectPath(relation.attributes, relation.type, attrUri),
            attributeObj = EntityUtils.findAttribute(relation.attributes, attrUri, null, true),
            attrType;

        attrType = attributeObj.attribute.attrType;
        if (!attrType) {
            attrType = MobileUI.core.Metadata.findEntityAttributeByName(MobileUI.core.Metadata.getRelationType(relation.type), attributeObj.name);
        }
        this.getTitle().setHtml(i18n('View %1', Ext.util.Format.htmlEncode(i18n(attrType.label))));
        this.getCrumb().setPath(path);
        var nested = this.processNestedAttribute(attrType, attributeObj);
        this.fillList(nested, attrType);
    },
    /**
     * Method fills the list
     * @param result {Array} the nested attribute information
     * @param attrType {Object} nested attribute type
     */
    fillList: function (result, attrType) {
        var attributes, entityModel, list, store, key = {};
        key['Other'] = result;
        attributes = attrType.attributes;
        if (attrType.referencedEntityTypeURI) {
            var refAttrType = MobileUI.core.Metadata.getRelationType(attrType.relationshipTypeURI);
            attributes = refAttrType.attributes;
        }
        entityModel = MobileUI.core.entity.EntityUtils.createAttributesModel(key, false, {'Other': attributes});
        list = this.getComplexAttributesList();
        store = list.getStore();
        store.clearData();
        store.add(entityModel);
        this.updateListHeight(list);
    }
});
