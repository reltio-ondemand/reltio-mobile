/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Relations', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfileBand',
        'MobileUI.components.RelationsFacet',
        'MobileUI.controller.MComments',
        'MobileUI.components.Popup',
        'MobileUI.controller.MBaseRelation',
        'MobileUI.core.relations.RelationsUtils',
        'MobileUI.core.StateManager',
        'MobileUI.controller.MErrorHandling'
    ],
    mixins: {
        profileBand: 'MobileUI.controller.MProfileBand',
        comments: 'MobileUI.controller.MComments',
        baseRelations: 'MobileUI.controller.MBaseRelation',
        errorHandling: 'MobileUI.controller.MErrorHandling'
    },
    config: {
        refs: {
            relationsProfileBand: 'facetprofileband[name=relationsProfileBand]',
            relationsFacet: 'relationsfacet[name=relationsFacets]',
            relationsList: 'elist[name=relationsList]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            additionalMenuBtn: 'button[name=additionalMenu]',
            panel: 'panel[name=relations]'
        },
        control: {
            relationsFacet: {
                applyFilter: 'onFilterApplied'
            },
            additionalMenuBtn: {
                tap: 'onMenuTap'
            },
            relationsList: {
                positive: 'onPositive',
                negative: 'onNegative',
                entityTap: 'onEntitySelect',
                selectItem: 'onRelationSelect',
                comment: 'onComment',
                fetch: 'onRelationsListFetch',
                editRelation: 'onEditRelation',
                addRelation: 'onAddRelation',
                pullRefresh: 'onPullRefresh',
                'delete': 'onRelationDelete'
            },
            relationsProfileBand: {
                blockTap: 'onBlockTap',
                favorited: 'onFavoriteTap'
            }
        },
        views: [
            'MobileUI.view.Relations'
        ]
    },
    /**@type {MobileUI.components.Popup} @private */
    commentPopup: null,
    /**@type {MobileUI.components.Popup} @private */
    votePopup: null,
    /**@type {elist} @private */
    commentList: null,
    /**@type {String} @private */
    relation: null,
    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                MobileUI.core.search.SearchParametersManager.on('globalFilterChanged', this.onGlobalFilterChanged, this);
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    this.getBackToolbarBtn().setHidden(false);
                }
                this.getAdditionalMenuBtn().setHidden(false);
            }
        }, this);
        this.getApplication().getController('ViewActivator').on('deactivated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1) {
                MobileUI.core.search.SearchParametersManager.un('globalFilterChanged', this.onGlobalFilterChanged, this);
            }
        }, this);
    },
    /**
     * On negative listener
     * @param event {Object} event object
     * @param callback {Function?} callback function
     * @param self {Object?} self object
     */
    onNegativeAdd: function(event, callback, self)
    {
        if (Ext.os.is.Phone) {
            if (callback)
            {
                callback.call(self);
            }
            var relUrl = event.record.get('userData'),
                params = this.getParsedParams();
            if (relUrl)
            {
                MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
                this.setSavedFilter(this.getFilter());
                MobileUI.core.Navigation.redirectTo('relations/' +
                    MobileUI.core.util.HashUtil.encodeUri('addcomments/'+params.relationId+'/' + params.entityUrl+ '/'+relUrl), false);
            }
        }
        else {
            if (Ext.isEmpty(this.votePopup) || this.votePopup.isDestroyed) {
                this.votePopup = Ext.create('MobileUI.components.Popup');
                this.votePopup.setWidth(MobileUI.components.Popup.POPUP_WIDTH);
                this.votePopup.setHeight(260);
                this.votePopup.setHideOnOrientationChange(false);

            }
            if (this.votePopup.isHidden() !== false) {
                this.votePopup.removeAll(true, true);
                this.votePopup.add(this.createAddCommentsView());
                this.votePopup.showBy(event.element, 'cr-tl?');
                this.relation = event.record.get('userData');
                this.votePopup.setHideOnMaskTap(false);
            }
            else {
                this.votePopup.hide();
            }
        }
    },

    /**
     * Comment tap listener
     * @param event{{record:Object, element: Object}} event
     */
    onComment: function (event)
    {
        if (Ext.os.is.Phone) {
            var relUrl = event.record.get('userData'),
                url, params;
            if (relUrl)
            {
                params = this.getParsedParams();
                this.setSavedFilter(this.getFilter());
                url = 'relations/'+
                    MobileUI.core.util.HashUtil.encodeUri('comments/'+params.relationId + '/' + params.entityUrl  + '/' + relUrl);
                MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
                MobileUI.core.Navigation.redirectTo(url, false);
            }
        }
        else {
            if (Ext.isEmpty(this.commentPopup) || this.commentPopup.isDestroyed) {
                this.commentPopup = Ext.create('MobileUI.components.Popup');
                this.commentPopup.setWidth(MobileUI.components.Popup.POPUP_WIDTH);
                this.commentPopup.setHeight(MobileUI.components.Popup.POPUP_HEIGHT);

            }
            if (this.commentPopup.isHidden() !== false) {
                this.showCommentList();
                this.commentPopup.showBy(event.element, 'cr-tl?');
                this.relation = event.record.get('userData');
            }
            else {
                this.commentPopup.hide();
            }
        }
    },
    /**
     * Method creates list of comments view
     * @returns {*[]}
     */
    createCommentsView: function ()
    {
        var that = this;
        return [
            {
                xtype: 'toolbar',
                docked: 'top',
                cls: 'toolbar-inv',
                items: [
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        html: i18n('History'),
                        cls: 'header'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    }
                ]
            },
            {
                xtype: 'elist',
                flex: 1,
                name: 'comments-list',
                defaultType: 'eDataItem',
                listeners: {
                    scope: that,
                    painted: 'onCommentListShown'
                },
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ];
    },
    getCommentItems: function () {
        var comments = this.relationConfig[0].negativeComments;
        return MobileUI.controller.Relations.createTheCommentItems((
                Ext.isArray(comments) ? comments : ['Has left the practice',
                    'Has moved away',
                    'Has passed away',
                    'Has retired',
                    'Other'
                ]).map(function (item) {
                return {label: i18n(item), value: item};
            })
        );
    },
    /**
     * Method creates add comment view
     * @returns {*[]}
     */
    createAddCommentsView: function ()
    {
        var that = this;
        return [
            {
                xtype: 'toolbar',
                docked: 'top',
                cls: 'toolbar-inv',
                items: [
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Add comment')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                name: 'sortOrder',
                cls: 'x-reltio-fieldset',
                flex: 1,
                defaults: {
                    listeners: {
                        scope: that,
                        check: 'onCloseVote'
                    },
                    labelWidth: '80%'
                },
                items: this.getCommentItems()
            }
        ];
    },
    /**
     * Method shows the comments list
     */
    showCommentList: function ()
    {
        this.commentPopup.removeAll(true, true);
        this.commentList = null;
        this.commentPopup.add(this.createCommentsView());
    },
    /**
     * Method invokes when the comment list will be shown
     */
    onCommentListShown: function ()
    {
        this.updateRelation();
    },
    /**
     * Method return the relation url
     * @returns {String}
     */
    getRelation: function ()
    {
        return this.relation;
    },
    /**
     * Method returns the comment list
     * @returns {elist}
     */
    getCommentsList: function ()
    {
        if (!this.commentList)
        {
            this.commentList = Ext.ComponentQuery.query('elist[name=comments-list]');
        }
        if (this.commentList.length > 0)
        {
            this.commentList = this.commentList[0];
        }
        return this.commentList;
    },
    /**
     * Method updates the comment on API and invokes callback
     * @param callback {Function?} callback
     */
    updateComment: function (callback)
    {
        var component = this.votePopup.down('radiofield[name=addComments-comment]'),
            comment, action;

        if (component)
        {
            comment = component.getGroupValue();
        }
        action = {
            value: 1,
            comment: comment || '',
            uri: this.relation,
            method: 'add'
        };
        this.updateRating(action, callback, this);

    },
    /**
     * On close add comment dialog listener
     */
    onCloseVote: function ()
    {
        this.updateComment(function(){
            this.votePopup.hide();
        });
    },
    /**@type {Array} @private */
    relationConfig: null,
    statics: {
        TYPE_RATING: 'rating',
        ADD_RATING: 'add',
        REMOVE_RATING: 'remove',
        NEGATIVE_VOTE: 1,
        POSITIVE_VOTE: 5,
        ADD_ITEM_URI:'add_item',
        STATE_NAME: 'Relation',
        createTheCommentItems:function(array){
            array = array || [];
            var length = array.length;
            return array.map(function(item, index){
                return {
                    xtype: 'radiofield',
                    name: 'addComments-comment',
                    label: item.label,
                    value: item.value,
                    cls: index === length - 1 ? 'without-border':''
                };
            });
        }
    },
    /**@type {String} @private */
    filter: null,
    /**@type {Object} @private */
    ratings: null,
    total: null,
    /**@type {{relationId: *, entityUrl: string}} @private */
    parsedParams: null,
    /**@type {String} @private */
    savedFilter: null,
    /**
     * On pull refresh listener
     */
    onPullRefresh: function ()
    {
        MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelation, function (resp)
        {
            MobileUI.core.Logger.error(resp);
        }, this);
    },

    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getRelationsProfileBand().setFavorite(item);
        },this);
    },

    getOptions: function ()
    {
        if (!Ext.isEmpty(this.relationConfig[0].sortOptions))
        {
            return this.relationConfig[0].sortOptions;
        }
    },

    /**
     * Method returns parsed params
     * @returns {{relationId: *, entityUrl: string}}
     */
    getParsedParams: function ()
    {
        return this.parsedParams;
    },
    /**
     * Method returns saved filter
     */
    getSavedFilter: function ()
    {
        return this.savedFilter;
    },

    /**
     * Method returns filter
     */
    getFilter: function ()
    {
        return this.filter;
    },

    setSavedFilter: function (savedFilter)
    {
        this.savedFilter = savedFilter;
    },

    /**
     * Method shows relations entrypoint
     * @param params
     */
    showRelation: function (params)
    {
        this.parsedParams = this.parseParams(params);
        var viewActivator = this.getApplication().getController('ViewActivator');
        if (viewActivator.waitForActivate())
        {
            viewActivator.on('activated', function ()
            {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
                {
                    this.showRelationInternal();
                }
            }, this, {single: true});
        }
        else
        {
            this.showRelationInternal();
        }
    },

    showRelationInternal: function(){
        this.filter = this.getSavedFilter();
        this.setSavedFilter(null);
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        var state = MobileUI.core.StateManager.getState(MobileUI.controller.Relations.STATE_NAME);
        if (MobileUI.core.Navigation.isBackAction() && state)
        {
            if (state.entityUri === this.parsedParams.entityUrl)
            {
                this.restoreState(state);
                return;
            }
        }

        MobileUI.core.StateManager.setState(MobileUI.controller.Relations.STATE_NAME,null);
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedParams.entityUrl)
        {
            this.processRelation(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelation, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },

    restoreState: function(state)
    {
        this.filter = state.filter;
        MobileUI.core.session.Session.setEntity(state.entity);
        this.processRelation(state.entity, true);
        this.relationConfig[0].content['order'] = state.order;
        var facetContent = this.relationConfig[0];
        facetContent.total = state.total;
        this.relationConfig[0].content['sortBy'] = state.sortBy;
        this.relationListOffset = state.relationListOffset;
        this.relationListPage = state.relationListPage;
        this.getRelationsFacet().setFacet(facetContent);
        this.getRelationsFacet().setFilter(this.filter);
        this.getRelationsFacet().setFilterEnabled(!Ext.isEmpty(this.filter));
        var store = this.getRelationsList().getStore();
        store.add(state.model);
        MobileUI.core.StateManager.setState(MobileUI.controller.Relations.STATE_NAME, null);
        this.getPanel().setMasked(null);
        var recordIndex = store.findExact('actionLabel', state.actionLabel);
        if (recordIndex !== -1)
        {
            var record = store.getAt(recordIndex);
            this.getRelationsList().scrollToRecord(record);
        }
    },
    saveState: function(actionLabel){
        var state = {};
        state.entityUri = this.parsedParams.entityUrl;
        state.filter = this.filter;
        state.entity = this.entity;
        state.order = this.relationConfig[0].content['order'];
        state.sortBy  = this.relationConfig[0].content['sortBy'];
        state.relationListOffset = this.relationListOffset;
        state.total = this.total;
        state.relationListPage = this.relationListPage;
        state.model = [];
        state.actionLabel = actionLabel;
        this.getRelationsList().getStore().each(function (rec)
        {
            var state = {},
                key;
            for (key in rec.raw)
            {
                if (rec.raw.hasOwnProperty(key))
                {
                    state[key] = rec.data[key]? rec.data[key]: rec.raw[key];
                }
            }
            this.push(state);
        }, state.model);
        MobileUI.core.StateManager.setState(MobileUI.controller.Relations.STATE_NAME,state);
    },
    /**
     * Method processes relations
     * @private
     * @param entity {Object} entity
     * @param notFillRelations {Boolean?} not fill relations
     */
    processRelation: function (entity, notFillRelations)
    {
        var configuration = MobileUI.core.Services.getConfigurationManager(),
            relationConfig, facet;
        relationConfig = Ext.clone(configuration.getExtensionById(this.parsedParams.relationId));
        MobileUI.core.relations.RelationsUtils.processRelations(relationConfig);
        this.relationConfig = [relationConfig];
        facet = Ext.clone(relationConfig);
        facet.total = 0;
        this.entity = entity;
        this.getRelationsFacet().setFacet(facet);
        if (!notFillRelations)
        {
            this.requestTotal(this.fillRelation, this);
        }
        this.fillBand(this.getRelationsProfileBand(), entity);
    },

    requestTotal: function(callback, context) {
        var relationConfig;

        if (!Ext.isEmpty(this.relationConfig))
        {
            relationConfig = this.relationConfig.map(function (item)
            {
                var result = Ext.clone(item.content);
                result.order = Ext.isEmpty(result.order, false) ? item.sortOrder : result.order;
                return result;
            });

            var filter = !Ext.isEmpty(this.relationConfig) && this.relationConfig[0].filter;
            filter = filter || null;
            if (filter) {
                if (this.filter) {
                    filter = this.filter + ' and (' + filter + ')';
                }
            }
            else {
                filter = this.filter;
            }
            MobileUI.core.session.Session.requestRelationsImmediately(relationConfig, filter, null, 0, 0,
                function(resp) {
                    var facet = Ext.clone(this.relationConfig);

                    facet[0].total = resp[0].total;
                    this.total = facet[0].total;
                    this.getRelationsFacet().setFacet(facet[0]);
                    callback.call(context);
                },
                function(resp) {
                    MobileUI.core.Logger.error(resp);
                    callback.call(context);
                }, this);
        }
        else {
            callback.call(context);
        }
    },

    /** @private */
    relationListOffset: 0,

    /** @private */
    relationListPage: 15,

    /**@private*/
    initRelationsList: function(store){
        var name = i18n(this.relationConfig && this.relationConfig[0] && this.relationConfig[0].actionLabel) || i18n('Add Member');
        if (store){
            store.clearData();
            var RelationsUtils = MobileUI.core.relations.RelationsUtils;
            if (RelationsUtils.isCorrectRelationType(RelationsUtils.getContentRelationsType(this.relationConfig[0].content),this.relationConfig[0]))
            {
                store.add([{
                    label: name,
                    actionLabel: MobileUI.controller.Relations.ADD_ITEM_URI,
                    disableSwipe: true,
                    type: MobileUI.components.list.items.BaseListItem.types.facetItem,
                    avatar: 'resources/images/reltio/relations/add-member.svg'
                }]);
            }
        }
    },
    /**
     * Method fills relation
     * @param max {Number?} max items count
     * @param offset {Number?} offset
     */
    fillRelation: function (max, offset)
    {
        var relationConfig;
        var suggestionConfig = [];
        var suggestedIndex = 0;

        max = max || 15;
        offset = offset || 0;

        this.relationListOffset = offset;
        this.relationListPage = max;

        function fillSortBy(item, result) {
            if (Ext.isEmpty(result.sortBy, true) && !Ext.isEmpty(item.sortOptions)) {
                for (var key in item.sortOptions) {
                    if (item.sortOptions.hasOwnProperty(key)) {
                        result.sortBy = key;
                        break;
                    }
                }
            }
        }

        if (!Ext.isEmpty(this.relationConfig))
        {
            relationConfig = this.relationConfig.map(function (item)
            {
                var result = Ext.clone(item.content);
                result.order = Ext.isEmpty(result.order, false) ? item.sortOrder : result.order;

                fillSortBy(item, result);

                if (item.suggested) {
                    result.suggested = [];
                    Array.prototype.push.apply(suggestionConfig, item.suggested.map(function(suggestedItem) {
                        suggestedItem = Ext.clone(suggestedItem);
                        suggestedItem.id = (++suggestedIndex).toString();
                        result.suggested.push(suggestedItem.id);

                        fillSortBy(item, suggestedItem);
                        return suggestedItem;
                    }));
                }

                return result;
            });
            relationConfig = relationConfig.concat(suggestionConfig);

            var filter = !Ext.isEmpty(this.relationConfig) && this.relationConfig[0].filter;
            filter = filter || null;
            if (filter) {
                if (this.filter) {
                    filter = this.filter + ' and (' + filter + ')';
                }
            } else {
                filter = this.filter;
            }
            MobileUI.core.session.Session.requestRelationsImmediately(relationConfig, filter, null,  max, offset, this.onRelationsResponse , function(resp){
                this.getPanel().setMasked(null);
                this.getPanel().setMasked(false);
                if(resp && resp.errorDetailMessage) {
                    this.showErrorMessage(this.getRelationsList().getStore(), resp.errorDetailMessage)
                }
            }, this);
        }
    },
    onRelationsResponse: function(resp)
    {
        var facet = Ext.clone(this.relationConfig),
            connections,
            model = [],
            i,
            list = this.getRelationsList(),
            store = list.getStore();
        if (this.relationListOffset === 0) {
            this.initRelationsList(store);
        }
        if (!Ext.isEmpty(resp))
        {
            this.getRelationsFacet().setFilter(this.filter);
            this.getRelationsFacet().setFilterEnabled(!Ext.isEmpty(this.filter));
            connections = resp[0].connections || [];
            var connectionsCount = connections.length;
            var connectedEntities = connections.filter(function(connection) {
                return !connection.suggested;
            }).map(function(connection) {
                return connection.entity.entityUri;
            });
            var entities = connections.map(function(connection) {
                return connection.entity.entityUri;
            });
            connections = connections.filter(function(connection, index) {
                return (!connection.suggested ||
                    ((connectedEntities.indexOf(connection.entity.entityUri) === -1) &&
                    (entities.indexOf(connection.entity.entityUri, index + 1) === -1)));
            });

            for (i = 0; i < connections.length; i++){
                model.push(this.processConnection(connections[i], facet[0].type));
            }
            store.add(model);
            list.fetchDone(connectionsCount === this.relationListPage);
        }
        this.getPanel().setMasked(null);
        this.getPanel().setMasked(false);
    },
    /** @private */
    onRelationsListFetch: function ()
    {
        this.fillRelation(this.relationListPage, this.relationListOffset + this.relationListPage);
    },

    /**
     * Method convert connections to the model items
     * @param connection {Object} connection object
     * @param facetType {String} facet type
     * @returns {{}}
     */
    processConnection: function (connection, facetType)
    {
        var entity, entityType, item = {}, label;
        entity = connection.entity;
        label = MobileUI.core.entity.EntityUtils.processLabel(entity.entityLabel);
        item.label = label;
        entityType = MobileUI.core.Metadata.getEntityType(entity.entityType);
        item.type = MobileUI.components.list.items.BaseListItem.types.relationsInfo;
        item.suggested = connection.suggested;
        item.tags = [
            {
                color: entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR,
                value: entityType.label
            },
            {
                color: MobileUI.core.search.SearchResultBuilder.CONNECTION_BADGE_COLOR,
                textColor: MobileUI.core.search.SearchResultBuilder.CONNECTION_BADGE_TEXT_COLOR,
                value: connection.relation.relationLabel
            }];

        item.avatar = entity.defaultProfilePicValue;
        item.defaultImage = MobileUI.core.Services.getBorderlessImageUrl(MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
        item.allowedSwipeElements = [];
        if (item.suggested) {
            if (MobileUI.core.PermissionManager.securityService().metadata.canCreate(connection.relation.relationType)){
                item.allowedSwipeElements.push('addRelation');
            }
            item.userData = connection.entity.entityUri;
        }
        else {
            if (MobileUI.core.PermissionManager.securityService().metadata.canDelete(connection.relation.relationType)){
                item.allowedSwipeElements.push('delete');
            }
            if (MobileUI.core.PermissionManager.securityService().metadata.canUpdate(connection.relation.relationType)){
                item.allowedSwipeElements.push('editRelation');
            }
            item.userData = connection.relation.relationUri;
        }

        item.actionLabel = MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) ? entity.entityUri : null;

        if ((facetType === MobileUI.controller.Relations.TYPE_RATING) && !item.suggested)
        {
            item = Ext.merge({}, item, this.extractRatings(connection.relation));
        }
        return item;
    },
    /**
     * Method extracts rating from relation
     * @private
     * @param relation {Object}
     * @returns {{}}
     */
    extractRatings: function (relation)
    {
        var item = {
                type: MobileUI.components.list.items.BaseListItem.types.relationsInfo
            },
            ratings, rating, collection,
            isVotedByUser = function (item)
            {
                return item.user === MobileUI.core.session.Session.getUserName();
            }, collect = function (previousValue, currentValue)
            {
                if (currentValue.value === 5)
                {
                    previousValue.positive += 1;
                }
                else
                {
                    previousValue.negative += 1;
                }
                return previousValue;
            };
        item.showVote = MobileUI.core.PermissionManager.securityService().metadata.canUpdate(relation.relationType);
        item.score = relation.consolidatedRating ? Math.round(relation.consolidatedRating * 10) / 10 : 0;
        ratings = relation.ratings || [];
        rating = ratings.filter(isVotedByUser);
        collection = ratings.reduce(collect, {negative: 0, positive: 0});
        item.negative = Ext.isNumber(collection.negative) ? collection.negative : 0;
        item.positive = Ext.isNumber(collection.positive) ? collection.positive : 0;
        item.userVote = rating.length > 0 ? rating[0].value : null;
        item.comment = rating.length > 0 ? rating[0].comment : null;
        return item;
    },
    /**
     * Method creates add or delete action
     * @param event {Object} event record
     * @param value {String|Number} user vote value
     * @returns {{value: *, comment: *, uri: *}}
     */
    createAction: function (event, value)
    {
        var action = {
            value: value,
            comment: event.get('comment'),
            uri: event.get('userData')
        };
        action.method = (event.get('userVote') === value) ? 'delete' : 'add';
        return action;
    },
    /**
     * On negative tap listener
     * @protected
     * @param evt
     */
    onNegative: function (evt)
    {
        this.confirmVote(evt.record, MobileUI.controller.Relations.NEGATIVE_VOTE, function (event)
        {
            var action = this.createAction(event, MobileUI.controller.Relations.NEGATIVE_VOTE);
            if (action.method === 'add')
            {
                this.onNegativeAdd(evt, Ext.emptyFn, this);
            } else {
                this.updateRating(action);
            }
        });
    },
    /**
     * On positive tap listener
     * @protected
     * @param evt
     */
    onPositive: function (evt)
    {
        this.confirmVote(evt.record, MobileUI.controller.Relations.POSITIVE_VOTE, function (event, vote)
        {
            if (vote === MobileUI.controller.Relations.POSITIVE_VOTE)
            {
                event.set('comment', '');
            }
            var action = this.createAction(event, MobileUI.controller.Relations.POSITIVE_VOTE);
            this.updateRating(action);
        });
    },

    /**
     * @private
     * @param event
     * @param vote
     * @param callback
     */
    confirmVote: function(event, vote, callback)
    {
        if (Ext.isNumber(event.get('userVote')) && event.get('userVote') !== vote)
        {
            MobileUI.components.MessageBox.show({
                title: i18n('Warning'),
                message: i18n('Are you sure you want to change your vote?'),
                buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
                promptConfig: false,
                scope: this,
                fn: function (buttonId) {
                    if (buttonId === 'yes') {
                        callback.call(this, event, vote);
                    }
                }
            });
        }
        else
        {
            callback.call(this, event);
        }
    },
    /**
     * On enity select listener
     * @protected
     * @param event {Object} event record
     */
    onEntitySelect: function (event)
    {
        var actionLabel = event.get('actionLabel');
        if (!actionLabel)
        {
            return;
        }
        if (actionLabel === MobileUI.controller.Relations.ADD_ITEM_URI)
        {
            this.addRelation();
        }
        else if (actionLabel)
        {
            this.saveState(actionLabel);
            MobileUI.core.Navigation.redirectForward('profile/' +
                MobileUI.core.util.HashUtil.encodeUri(actionLabel));
        }
    },
    /**
     * Relation delete listener
     * @param event {Object} event
     */
    onRelationDelete: function (event)
    {
        if (event.getData())
        {
            var uri = event.getData().userData;
            if (!Ext.isEmpty(uri))
            {
                this.getPanel().setMasked({
                    xtype: 'loadmaskex',
                    message: '',
                    cls: 'opaquemask',
                    indicator: true
                });
                MobileUI.core.session.Session.removeRelation(uri, function (resp)
                {
                    var list, store, index, that = this;
                    if (Ext.isEmpty(resp))
                    {
                        MobileUI.core.Logger.error('The remove realtion ' + uri + 'is not done');
                        return;
                    }

                    resp = Ext.isArray(resp) ? resp[0] : resp;

                    if (resp && resp.status === 'success')
                    {
                        list = this.getRelationsList();
                        if (list)
                        {
                            store = list.getStore();
                            index = store.findExact('userData', uri);
                            if (index !== -1)
                            {
                                store.removeAt(index);
                                store.sync();
                            }
                            this.getRelationsFacet().decreaseItemsCount();
                        }
                    }

                    that.getPanel().setMasked(null);
                }, function (error)
                {
                    this.getPanel().setMasked(null);
                    MobileUI.core.Logger.error(error);
                }, this);
            }
        }
    },
    /**
     * Method sends request to the API for rating update
     * @param action {object} action object
     * @param callback {Function?} callback function
     * @param self {Object?}  self object
     */
    updateRating: function (action, callback, self)
    {
        var onRatingUpdated = function ()
        {
            if (this.callback)
            {
                this.callback.call(this.callbackSelf);
            }
            MobileUI.core.Logger.info('Trying to get relation:' + this.uri);

            MobileUI.core.session.Session.getRelation(this.uri, function (resp)
            {
                var that = this,
                    item = that.self.extractRatings(resp[0]),
                    list, store, record, index;

                list = that.self.getRelationsList();
                if (list)
                {
                    store = list.getStore();
                    index = store.findExact('userData', that.uri);
                    if (index !== -1)
                    {
                        record = store.getAt(index);
                        record.set('negative', item.negative);
                        record.set('positive', item.positive);
                        record.set('score', item.score);
                        record.set('userVote', item.userVote);
                        record.set('comment', item.comment || '');
                        record.setDirty();
                        store.sync();
                    }
                }
            }, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, {self: this.self, uri: this.uri});
        }, list, store, index, record;
        list = this.getRelationsList();
        store = list.getStore();
        index = store.findExact('userData', action.uri);
        record = store.getAt(index);
        record.set('userVote', action.value);

        if (action.method === 'add')
        {
            MobileUI.core.session.Session.addRelationRating(action.uri, action.value, action.comment, onRatingUpdated, null, {
                self: this,
                callbackSelf: self,
                callback: callback,
                uri: action.uri
            });
        }
        else
        {
            MobileUI.core.session.Session.removeRelationRating(action.uri, onRatingUpdated, null, {
                self: this,
                callbackSelf: self,
                callback: callback,
                uri: action.uri
            });
        }
    },
    /**
     * Method parse the params
     * @private
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string}}
     */
    parseParams: function (params)
    {
        return {
            relationId: params[0],
            entityUrl: params.splice(1, params.length - 1).join('/')
        };
    },
    /**
     * Filter applied listener
     * @param filterText {String} filter text
     */
    onFilterApplied: function (filterText)
    {
        this.filter = filterText;
        this.fillRelation();
    },

    /**
     * Menu callback
     * @param order
     * @param sortBy
     */
    processMenuAction: function(order, sortBy)
    {
        if (this.relationConfig[0] && this.relationConfig[0].content)
        {
            var currentOrder = this.relationConfig[0].content['order'];
            var currentSortBy = this.relationConfig[0].content['sortBy'];
            if (currentOrder !== order || currentSortBy !== sortBy) {
                this.relationConfig[0].content['order'] = order;
                this.relationConfig[0].content['sortBy'] = sortBy;
                this.fillRelation();
            }
        }
    },
    getGroupValue: function(){
        return {
            order: this.relationConfig[0].content['order'],
            content: this.relationConfig[0].content['sortBy']
        };
    },
    /**
     * Add relation listener
     */
    addRelation: function ()
    {
        var content = !Ext.isEmpty(this.relationConfig)? this.relationConfig[0].content: null;
        MobileUI.core.entity.TemporaryEntity.removeAllEntities();
        this.getApplication().getController('dialogs.RelationshipType').showRelationshipTypeDialog(content, this.relationConfig[0]);
        MobileUI.core.relations.RelationsManager.on('createRelation', function(){
            this.onPullRefresh();
        },this,{single:true});
    },
    /**
     * On global filter changed listener
     */
    onGlobalFilterChanged: function ()
    {
        this.onPullRefresh();
    },
    /**
     * On relation select listener
     */
    onRelationSelect: function(event){
        var actionLabel = event.get('actionLabel');
        if (actionLabel === MobileUI.controller.Relations.ADD_ITEM_URI)
        {
            this.addRelation();
            return;
        }
        var uri = [this.parsedParams.relationId,
            this.parsedParams.entityUrl,
            event.getData().userData
        ].join('/');
        this.saveState(actionLabel);
        MobileUI.core.Navigation.redirectForward('relation/' +
            MobileUI.core.util.HashUtil.encodeUri(uri));
    },
    /**
     * Edit relation listener
     * @param record {Object}
     */
    onEditRelation: function(record)
    {
        var path = [this.parsedParams.relationId,
            this.parsedParams.entityUrl,
            record.getData().userData,
            'relations'
        ].join('/');
        MobileUI.core.Navigation.redirectForward('relation/edit$' + MobileUI.core.util.HashUtil.encodeUri(path), false);
    },

    onAddRelation: function(event) {
        if (event.getData()) {
            var url = event.getData().userData;
            if (!Ext.isEmpty(url)) {
                this.getPanel().setMasked({
                    xtype: 'loadmaskex',
                    message: '',
                    cls: 'opaquemask',
                    indicator: true
                });

                var RelationsUtils = MobileUI.core.relations.RelationsUtils;
                var config = MobileUI.core.Services.getConfigurationManager().getExtensionById(this.parsedParams.relationId);
                var typeObject = RelationsUtils.getContentRelationsType(config.content);
                var type = typeObject.result;
                var relationType;
                var inRelation = false;
                if (type === RelationsUtils.SINGLE_IN_RELATION) {
                    relationType = typeObject.inRelations[0];
                    inRelation = true;
                }
                else if (type === RelationsUtils.SINGLE_OUT_RELATION) {
                    relationType = typeObject.outRelations[0];
                }

                if (relationType) {
                    var startEntityUri = inRelation ? url : this.parsedParams.entityUrl;
                    var endEntityUri = inRelation ? this.parsedParams.entityUrl : url;

                    MobileUI.core.session.Session.addRelation(startEntityUri,
                        endEntityUri, relationType.uri || relationType, null, null, null,
                        function(resp) {
                            var value, comment,
                                uri = Ext.isArray(resp) ? resp[0].object.uri : null;
                            if (this.relationConfig[0]) {
                                value = this.relationConfig[0].autoVote || MobileUI.controller.Relations.POSITIVE_VOTE;
                                value = parseInt(value, 10);
                                if (this.relationConfig[0].type === MobileUI.controller.Relations.TYPE_RATING && value > 0 && !this.relationConfig[0].disableAutoVote) {
                                    comment = value === MobileUI.controller.Relations.NEGATIVE_VOTE ? this.relationConfig[0].autoComment : '';
                                    comment = comment || '';
                                    MobileUI.core.session.Session.addRelationRating(uri, value, comment, this.afterSave, null, this);
                                    return;
                                }
                            }
                            this.afterSave();
                        }, function(error) {
                            this.getPanel().setMasked(null);
                            MobileUI.core.Logger.error(error);
                        }, this);
                }
                else {
                    this.getPanel().setMasked(null);
                }
            }
        }
    },
    afterSave: function(){
        this.getPanel().setMasked(null);
        this.requestTotal(this.fillRelation, this);
    }
});
