/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.RelationsTree', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfileBand',
        'MobileUI.components.MessageBox',
        'MobileUI.controller.MErrorHandling'
    ],
    mixins: {
        profileBand: 'MobileUI.controller.MProfileBand',
        errorHandling: 'MobileUI.controller.MErrorHandling'
    },
    config: {
        refs: {
            relationsTreeProfileBand: 'facetprofileband[name=relationsTreeProfileBand]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            relationsTreeFacet: 'relationstreefacet[name=relationsTreeFacet]',
            relationsTree: 'reltiotree[name=relationsTree]',
            panel: 'panel[name=relations-tree]'
        },
        control: {
            relationsTree: {
                selectItem: 'onRelationTreeItemTap',
                pullRefresh: 'onPullRefresh',
                expand: 'onRelationTreeExpand',
                'delete': 'onDelete',
                addParent: 'onAddParent',
                addChild: 'onAddChild'
            },
            relationsTreeFacet: {
                navigate: 'onNavigate'
            },
            relationsTreeProfileBand: {
                blockTap :'onBlockTap',
                favorited: 'onFavoriteTap'
            }
        },
        views: [
            'MobileUI.view.RelationsTree'
        ]
    },
    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    this.getBackToolbarBtn().setHidden(false);
                }
            }
        }, this);
    },
    statics: {
        OFFSET: 35,
        STATE_NAME: 'relationsTree'
    },
    /**@type {Object} @private*/
    root: null,
    /**@type {Array} @private*/
    tmpStore: null,
    /**@type {Object} @private*/
    parsedParams: null,
    /**@type {String} @private*/
    graphType: null,
    allowedRelations: [],

    loadSecondaryLabels: function(uris) {
        MobileUI.core.session.Session.sendRequest('/entities?filter={0}&select=uri,secondaryLabel&max={1}&offset=0&scoreEnabled=false',
            [uris.map(function(uri) { return 'equals(uri,' + uri + ')'; }).join(' or '), uris.length], {},
            function(resp) {
                var relTree = this.getRelationsTree();
                resp.forEach(function(entity) {
                    if (relTree) {
                        var store = relTree.getStore();
                        if (store) {
                            var index = store.findExact('userData', entity.uri);
                            if (index !== -1) {
                                var item = store.getAt(index);
                                if (item) {
                                    item.set('secondaryLabel', entity.secondaryLabel);
                                    var def = item.get('definition');
                                    def.secondaryLabel = entity.secondaryLabel;
                                    this.tmpStore[index].secondaryLabel = def.secondaryLabel;
                                }
                            }
                        }
                    }
                }, this);
            },
            function(resp) {
                MobileUI.core.Logger.error(resp);
            }, this);
    },

    /**
     * on pull refresh listener
     */
    onPullRefresh: function ()
    {
        this.mask();
        MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelationsTree, function (resp)
        {
            this.unmask();
            MobileUI.core.Logger.error(resp);
        }, this);
    },
    saveState: function(actionLabel){
        var state = {};
        state.root = this.root;
        state.tmpStore = this.tmpStore;
        state.graphType = this.graphType;
        state.actionLabel = actionLabel;
        state.config = this.config;
        state.entity = this.entity;
        state.entityUri = this.parsedParams.entityUrl;
        state.model = [];
        this.getRelationsTree().getStore().each(function (rec)
        {
            var state = {},
                key;
            for (key in rec.raw)
            {
                if (rec.raw.hasOwnProperty(key))
                {
                    state[key] = rec.data[key]? rec.data[key]: rec.raw[key];
                }
            }
            this.push(state);
        }, state.model);
        MobileUI.core.StateManager.setState(MobileUI.controller.RelationsTree.STATE_NAME, state);
    },

    restoreState: function () {
        var state = MobileUI.core.StateManager.getState(MobileUI.controller.RelationsTree.STATE_NAME);
        this.root = state.root;
        this.tmpStore = state.tmpStore;
        this.graphType = state.graphType;
        this.config = state.config;
        MobileUI.core.session.Session.setEntity(state.entity);
        var store = this.getRelationsTree().getStore();
        store.add(state.model);
        this.fillBand(this.getRelationsTreeProfileBand(), state.entity);
        this.getRelationsTreeFacet().setFacet(this.config);
        MobileUI.core.StateManager.setState(MobileUI.controller.RelationsTree.STATE_NAME, null);
        var recordIndex = store.findExact('userData', state.actionLabel);
        if (recordIndex){
            var record = store.getAt(recordIndex);
            //there is no another way to scroll in case of variable height.
            // It happens b/c bugs in the sencha code
            // So, we should use this hack
            [30,50,100,200].forEach(function(time){
                Ext.Function.defer(function () {
                    if (this.itemsTransformed) {
                        this.getRelationsTree().scrollToRecord(record);
                    }
                }, time, this);
            },this);
        }
        this.getPanel().setMasked(null);
    },

    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getRelationsTreeProfileBand().setFavorite(item);
        },this);
    },
    /**
     * Method shows the relations tree
     * @param params
     */
    showRelationsTree: function (params)
    {
        this.parsedParams = this.parseParams(params);
        this.getPanel().setMasked( {
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        var state = MobileUI.core.StateManager.getState(MobileUI.controller.RelationsTree.STATE_NAME);
        if (MobileUI.core.Navigation.isBackAction() && state)
        {
            if (state.entityUri === this.parsedParams.entityUrl)
            {
                this.restoreState(state);
                return;
            }
        }
        if (MobileUI.core.session.Session.getEntityUri() === this.parsedParams.entityUrl)
        {
            this.processRelationsTree(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.Logger.info('Trying to get relation:' + this.parsedParams.entityUrl);
            MobileUI.core.session.Session.requestEntity(this.parsedParams.entityUrl, this.processRelationsTree, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },
    /**
     * Method processes relations tree
     * @param entity {Object} entity object
     */
    processRelationsTree: function (entity)
    {
        this.unmask();
        var configuration = MobileUI.core.Services.getConfigurationManager(),
            facet;
        this.config = configuration.getExtensionById(this.parsedParams.relationId);

        var graphType = MobileUI.core.Metadata.getGraphType(this.config.graph.type);
        var mdp = MobileUI.core.PermissionManager.securityService().metadata;

        this.allowedRelations = graphType.relationshipTypeURIs.filter(function(uri) {
            return mdp.canCreateOrInitiate(uri);
        });

        facet = Ext.clone(this.config);
        facet.total = 0;
        this.getRelationsTreeFacet().setFacet(facet);
        this.fillBand(this.getRelationsTreeProfileBand(), entity);
        this.entity = entity;
        this.initialFillTree();
    },
    /**
     * Method creates and sends the request to api
     * @private
     */
    initialFillTree: function ()
    {
        var requestsContent;
        if (!Ext.isEmpty(this.config))
        {
            requestsContent = [this.config].map(function (item)
            {
                return item.graph ? item.graph.type : '';
            });
            this.graphType = requestsContent[0];
            MobileUI.core.Logger.info('Trying to get tree:' + requestsContent[0]);
            MobileUI.core.session.Session.getFullTree(requestsContent, function (resp)
            {
                var that = this, childCounts;
                that.createTree(resp);
                that.config.total = 0;
                if (resp.root)
                {
                    childCounts = Ext.isArray(resp.root.children) ?
                        resp.root.children.reduce(function(prev, item){
                            return prev + item.total;
                        },0)
                        : 0;
                    that.config.total = resp.root.total + childCounts + 1;
                }
                that.getRelationsTreeFacet().setFacet(that.config);
                //we need this delay to pull-to-refresh plugin
                Ext.Function.defer(function ()
                {
                    var that = this;
                    if (that.itemsTransformed)
                    {
                        that.onNavigate();
                        this.getPanel().setMasked(null);
                        that.getPanel().setMasked(false);
                    }
                }, 500, that);
            }, function (resp)
            {
                if(resp && resp.errorDetailMessage) {
                    var store = this.getRelationsTree().getStore();
                    this.showErrorMessage(store, resp.errorDetailMessage)
                }
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },
    /**
     * Method creates tree from the api response
     * @private
     * @param resp {Object} api response
     */
    createTree: function (resp)
    {
        var cloneResp = Ext.clone(resp),
            store = this.getRelationsTree().getStore(),
            items;
        this.root = cloneResp.root;
        this.getRelationsTree().on('itemsTransformed',
            function ()
            {
                this.itemsTransformed = true;
            }, this);
        this.getRelationsTree().on('itemtouchstart',
            function ()
            {
                this.itemsTransformed = false;
            }, this);
        MobileUI.core.util.Util.forEach(this.root, function (key, value, item)
        {
            var itemKey, i;
            if (item.entity)
            {
                for (itemKey in item.entity)
                {
                    if (item.entity.hasOwnProperty(itemKey))
                    {
                        item[itemKey] = item.entity[itemKey];
                    }
                }
                delete item.entity;
                item.name = item.label;
                item.traversedRelations = item.children ? item.children.length : 0;
                item.untraversedRelations = item.children ? 0 : item.total;
                if (item !== cloneResp.root)
                {
                    item.traversedRelations++;
                }
            }
            if (item.children)
            {
                for (i = 0; i < item.children.length; i++)
                {
                    if(Ext.isArray(item.children[i].relation))
                    {
                        var entity = item.children[i].entity;
                        if(!entity)
                        {
                            entity = {
                                uri: item.children[i].uri,
                                name: item.children[i].name,
                                label: item.children[i].label
                            };
                        }
                    }
                    item.children[i].parentUri = item.uri;
                    if (item.children[i].relation)
                    {
                        item.children[i].relation.startObject = {
                            objectURI: item.uri,
                            directionalLabel: ''
                        };
                        item.children[i].relation.endObject = {
                            objectURI: item.children[i].uri,
                            directionalLabel: ''
                        };
                    }
                }
            }
        }, this, true);

        items = this.createItems(this.root, 0, true);
        this.tmpStore = Ext.clone(items);
        store.clearData();
        store.add(items);

    },
    /**
     * Method creates tree item
     * @param itemDefinition {{defaultProfilePic:String,label:String,secondaryLabel:string}} item definition
     * @param index {Number} the item offset index in the tree
     * @param state {String}  state possible values: ['expanded'||'collapsed']
     * @param uri {String} entity uri
     * @returns {{}}
     */
    createItem: function (itemDefinition, index, state, uri)
    {
        var entityType = MobileUI.core.Metadata.getEntityType(itemDefinition.type),
            item = {},label;

        if (itemDefinition.defaultProfilePic)
        {
            item.avatar = itemDefinition.defaultProfilePic;
        }
        else
        {
            item.avatar = MobileUI.core.Services.getBorderlessImageUrl(MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
        }

        label = MobileUI.core.entity.EntityUtils.processLabel(itemDefinition.label);
        item.type = MobileUI.components.list.items.BaseListItem.types.relationsTree;
        item.label = label;
        item.secondaryLabel = itemDefinition.secondaryLabel || '';
        item.offset = 20 + ((index || 0) * MobileUI.controller.RelationsTree.OFFSET || 0);
        item.index = index;
        item.hidden = false;

        var mdp = MobileUI.core.PermissionManager.securityService().metadata;

        itemDefinition.allowDelete = !!itemDefinition.parentUri && mdp.canDelete(itemDefinition.relation.type);

        itemDefinition.allowAddParent = !itemDefinition.parentUri && (this.allowedRelations.length > 0);

        itemDefinition.allowAddChild = (this.allowedRelations.length > 0);


        switch (state)
        {
            case 'expanded':
                item.stateIcon = 'resources/images/reltio/tree/expanded.svg';
                item.children = 'loaded';
                break;
            case 'collapsed':
                item.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
                item.children = 'not';
                break;
            default:
                item.stateIcon = '';
        }
        item.state = state;
        item.userData = uri;
        item.definition = itemDefinition;
        item.selected = uri === this.parsedParams.entityUrl ? 'selected' : '';
        return item;
    },
    /**
     * Methods goes through the tree and creates tree items
     * @param root {Object}
     * @param index {Number} initial tree offset index
     * @returns {Array}
     */
    createItems: function (root, index, doNotFetchSecondaryLabels)
    {
        var items = [], i, state;
        if (root)
        {
            if (root.multipleParents) {
                MobileUI.components.MessageBox.alert(i18n('Error'),  i18n('Tree cannot be displayed because %1 has two parents', root.entity.label));
                return [];
            }
            state = (root.total > 0) ? (Ext.isArray(root.children) ? 'expanded' : 'collapsed') : '';
            items.push(this.createItem(root, index, state, root.uri, doNotFetchSecondaryLabels));
            if (!Ext.isEmpty(root.children))
            {
                for (i = 0; i < root.children.length; i++)
                {
                    items = items.concat(this.createItems(root.children[i], index + 1, true));
                }
            }

            if (!doNotFetchSecondaryLabels) {
                var uris = root.secondaryLabel ? [] : [root.uri];

                if (root.children) {
                    Array.prototype.push.apply(uris,
                        root.children
                            .filter(function(child) { return !child.secondaryLabel; })
                            .map(function(child) { return child.uri; }));
                }
                var store = this.getRelationsTree().getStore();
                this.loadSecondaryLabels(uris.filter(function(entity) {
                    var index = store.findExact('userData', entity.uri);
                    var item = this.tmpStore[index];
                    return !item || !item.secondaryLabel;
                }, this));
            }
        }
        return items;
    },
    /**
     * Method searches for node in the tree. Method returns null if node not found.
     * @param root {Object} tree root
     * @param uri  {String} object uri
     * @returns {{}|null}
     */
    searchForNode: function (root, uri)
    {
        var i, node;
        if (root.uri === uri)
        {
            return root;
        }
        if (root.children)
        {
            for (i = 0; i < root.children.length; i++)
            {
                node = this.searchForNode(root.children[i], uri);
                if (node)
                {
                    return node;
                }
            }
        }
        return null;
    },
    /**
     * On navigate listener
     * @param item {Object?} item on which we should navigate
     */
    onNavigate: function (item)
    {
        var i, path,
            store = this.getRelationsTree().getStore(),
            record, recordIndex, entityUrl;
        item = item ? item.treeItem : null;
        entityUrl = item || this.parsedParams.entityUrl;
        path = this.getPathToNode(entityUrl);
        for (i = path.length - 1; i >= 0; i--)
        {
            recordIndex = store.findExact('userData', path[i].userData);
            if (recordIndex !== -1)
            {
                record = store.getAt(recordIndex);
                if (record.get('state') === 'collapsed')
                {
                    this.expandNode(record, recordIndex, record.get('index'), store);
                }
            }
        }
        recordIndex = store.findExact('userData', entityUrl);
        record = store.getAt(recordIndex);
        this.scrollToRecord(record);
    },
    /**
     * Scroll to record
     * @param record {{}}
     */
    scrollToRecord: function (record)
    {
        this.getRelationsTree().scrollToRecord(record, {duration: 80}, true);
        //We can't get when the all items are transformed, therefore we should check it several times
        Ext.Function.defer(function ()
        {
            var that = this;
            if (that.itemsTransformed)
            {
                that.scrollToRecord(record);
            }
        }, 120, this);
        this.itemsTransformed = false;

    },
    /**
     * Method returns the path from node parents
     * @param uri {String} node uri
     * @returns {Array}
     */
    getPathToNode: function (uri)
    {
        var i, index, result = [];
        for (i = 0; i < this.tmpStore.length; i++)
        {
            if (this.tmpStore[i].userData === uri)
            {
                index = i;
                break;
            }
        }
        for (i = index; i > 0; i--)
        {
            if (this.tmpStore[i].index > this.tmpStore[i - 1].index)
            {
                result.push(this.tmpStore[i - 1]);
            }
        }
        return result;
    },
    /**
     * Method adds new nodes to the tree
     * @param nodeId {String} node id (in our case it's node uri)
     * @param listener {Function?} callback listener
     * @param index {Number} initial index for children nodes
     * @param self {Object} self object for callback
     */
    pending: function (nodeId, listener, index, self)
    {
        MobileUI.core.session.Session.getGraph(nodeId, this.graphType, 1, null, function (resp)
        {
            var i, treeResult,
                node = this.searchForNode(this.root, nodeId);
            for (i = 0; i < resp.entities.length; i++)
            {
                if (resp.entities[i].uri === nodeId)
                {
                    resp.entities[i] = node;
                    break;
                }
            }
            node.children = [];
            var result = this.createPendingTree(resp.entities, resp.relations, nodeId);
            if (result && result.multipleParents) {
                MobileUI.components.MessageBox.alert(i18n('Error'),  i18n('Tree cannot be displayed because %1 has two parents', result.entity.label));
                return;
            }
            treeResult = this.createItems(node, index);
            node.traversedRelations = node.children.length;
            if (node !== this.root)
            {
                node.traversedRelations++;
            }
            node.untraversedRelations = 0;
            if (listener)
            {
                listener.call(self, treeResult);
            }
        }, function(resp){
            if(resp && resp.errorDetailMessage) {
                var store = this.getRelationsTree().getStore();
                this.showErrorMessage(store, resp.errorDetailMessage)
            }
            MobileUI.core.Logger.error(resp);
        }, this);
    },
    /**
     * Method parses the api response and creates the tree model
     * @param entities {Array} entities array
     * @param relations {Array} relations array
     * @param forNode {String} init node uri
     * @returns {*}
     */
    createPendingTree: function (entities, relations, forNode)
    {
        var result = null,
            map = {}, i, rel, parent, child;
        for (i = 0; i < entities.length; i++)
        {
            map[entities[i].uri] = entities[i];
        }

        for (i = 0; i < relations.length; i++)
        {
            rel = relations[i];
            if (rel.direction !== 'directed')
            {
                continue;
            }

            if (forNode === undefined || forNode === rel.startObject.objectURI)
            {
                parent = map[rel.startObject.objectURI];
                if (parent.children === undefined)
                {
                    parent.children = [];
                }

                child = map[rel.endObject.objectURI];
                child.relation = rel;
                child.parentUri = parent.uri;
                child.name = child.label;
                parent.children.push(child);

            }
            else if (forNode === rel.endObject.objectURI && !result)
            {
                parent = map[rel.endObject.objectURI];
                if (parent && parent.parentUri && parent.parentUri !== rel.startObject.objectURI)
                {
                    result = {multipleParents: true, entity: parent};
                }
                else {
                    map[forNode].relation = rel;
                }
            }
        }
        return result;
    },
    /**
     * Method collapses the node in the tree
     * @param treeItem {MobileUI.components.list.model.EListModel} tree item model
     * @param index  {Number} item index in the tree
     * @param offset {Number} the offset index
     * @param store {Ext.data.Store} store
     */
    collapseNode: function (treeItem, index, offset, store)
    {
        var recordsToRemove, i, tmpRecord;
        recordsToRemove = [];
        for (i = index + 1; i < store.getCount(); i++)
        {
            tmpRecord = store.getAt(i);
            if (tmpRecord.get('index') === offset)
            {
                break;
            }
            recordsToRemove.push(tmpRecord);
        }
        store.remove(recordsToRemove);
        treeItem.set('state', 'collapsed');
        treeItem.set('stateIcon', 'resources/images/reltio/tree/collapsed.svg');
    },
    /**
     * Method expandes the node in the tree
     * @param treeItem {MobileUI.components.list.model.EListModel} tree item model
     * @param index  {Number} item index in the tree
     * @param offset {Number} the offset index
     * @param store {Ext.data.Store} store
     */
    expandNode: function (treeItem, index, offset, store)
    {
        var uri, tmpRecord, tmpIndex, i, recordsToPush;
        uri = treeItem.get('userData');
        tmpRecord = null;
        for (i = 0; i < this.tmpStore.length; i++)
        {
            if (this.tmpStore[i].userData === uri)
            {
                tmpRecord = this.tmpStore[i];
                tmpIndex = i;
                break;
            }
        }
        if (tmpRecord)
        {
            recordsToPush = [];
            for (i = tmpIndex + 1; i < this.tmpStore.length; i++)
            {
                if (this.tmpStore[i].index === offset + 1)
                {
                    if (this.tmpStore[i].state === 'expanded')
                    {
                        this.tmpStore[i].stateIcon = 'resources/images/reltio/tree/collapsed.svg';
                        this.tmpStore[i].children = 'loaded';
                        this.tmpStore[i].state = 'collapsed';
                    }
                    recordsToPush.push(this.tmpStore[i]);
                }
                else if (this.tmpStore[i].index === offset)
                {
                    break;
                }
            }
        }
        store.insert(index + 1, recordsToPush);
        treeItem.set('state', 'expanded');
        treeItem.set('stateIcon', 'resources/images/reltio/tree/expanded.svg');
    },
    /**
     * Item tap listener
     * @protected
     * @param treeItem {MobileUI.components.list.model.EListModel} tree item model
     */
    onRelationTreeItemTap: function (treeItem) {
        this.saveState(treeItem.get('userData'));
        MobileUI.core.Navigation.redirectForward('profile/' +
            MobileUI.core.util.HashUtil.encodeUri(String(treeItem.get('userData'))));
    },

    onRelationTreeExpand: function(eventData) {
        var state, offsetIndex,
            record = eventData.record,
            store = this.getRelationsTree().getStore(),
            index = store.indexOf(record);

        if (record.get('children') === 'loaded')
        {
            state = record.get('state');
            offsetIndex = record.get('index');
            if (state === 'expanded')
            {
                this.collapseNode(record, index, offsetIndex, store);
            }
            else if (state === 'collapsed')
            {
                this.expandNode(record, index, offsetIndex, store);
            }
        }
        else
        {
            this.pending(record.get('userData'), function (resp)
            {
                if (!Ext.isEmpty(resp))
                {
                    var recordIndex = store.findExact('userData', resp[0].userData);
                    resp[0].children = 'loaded';
                    resp[0].state = 'expanded';
                    resp[0].stateIcon = 'resources/images/reltio/tree/expanded.svg';

                    if (recordIndex !== -1)
                    {
                        this.tmpStore = this.tmpStore.slice(0, recordIndex).concat(Ext.clone(resp)).concat(this.tmpStore.slice(recordIndex + 1));
                        store.removeAt(recordIndex);
                        store.insert(recordIndex, resp);
                    }
                }
            }, Number(record.get('index')), this);
        }
    },
    /**
     * Method parse the params
     * @private
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string}}
     */
    parseParams: function (params)
    {
        return {
            relationId: params[0],
            entityUrl: params.splice(1, params.length - 1).join('/')
        };
    },

    onDelete: function(record) {
        var controller = this;
        var store = this.getRelationsTree().getStore();

        MobileUI.components.MessageBox.show({
            title: i18n('Warning'),
            message: i18n('Are you sure you want to delete relation?'),
            buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
            promptConfig: false,
            scope: this,
            fn: function (buttonId) {
                if (buttonId === 'yes') {
                    var def = record.get('definition');
                    var uri = def.relation.uri;

                    var index = store.findExact('userData', def.parentUri);
                    var parent = store.getAt(index);

                    var deletedUri = record.get('userData');
                    var children = parent.get('definition').children;
                    for (var i = 0; i < children.length; i++) {
                        if (children[i].uri === deletedUri) {
                            children.splice(i, 1);
                            break;
                        }
                    }

                    if (children.length === 0) {
                        parent.set('state', '');
                        parent.set('stateIcon', '');
                    }
                    controller.removeSubtree(record);

                    MobileUI.core.session.Session.removeRelation(uri, function() {
                            controller.unmask();
                        },
                        function(resp) {
                            MobileUI.core.Logger.error(resp);
                            controller.onPullRefresh();
                        });
                }
            }
        });
    },

    onAddParent: function(record) {
        this.addRelation(record, true);
    },

    onAddChild: function(record) {
        this.addRelation(record, false);
    },

    addRelation: function(record, inRelation) {
        var graphType = MobileUI.core.Metadata.getGraphType(this.config.graph.type);

        var content = {
            entityTypes: [this.root.type]
        };

        content[inRelation ? 'inRelations' : 'outRelations'] = this.allowedRelations;

        var config = {
            caption: graphType.label,
            content: content
        };

        MobileUI.core.relations.RelationsManager.setBaseEntityUri(record.get('userData'));

        this.getApplication().getController('dialogs.RelationshipType').showRelationshipTypeDialog(content, config);
        MobileUI.core.relations.RelationsManager.on('beforeCreateRelation', this.__onBeforeCreate, this);
        MobileUI.core.SuiteManager.on('hideDialogs', function() {
            MobileUI.core.relations.RelationsManager.un('beforeCreateRelation', this.__onBeforeCreate, this);
        },this);
        MobileUI.core.relations.RelationsManager.on('createRelation', function(connectedEntityUri) {
            if (inRelation) {
                this.onPullRefresh();
            }
            else {
                var store = this.getRelationsTree().getStore();
                var index = store.findExact('userData', record.get('userData'));
                var parentItem = store.getAt(index);
                if (parentItem.get('state') !== 'expanded') {
                    parentItem.set('children', 'not');
                    this.onRelationTreeExpand({record: parentItem});
                }
                else {
                    this.mask();
                    MobileUI.core.session.Session.getGraph(connectedEntityUri, this.graphType, 1, null,
                        function(resp) {
                            var result = this.createPendingTree(resp.entities, resp.relations, connectedEntityUri);

                            if (result && result.multipleParents) {
                                MobileUI.components.MessageBox.alert(i18n('Error'),  i18n('Tree cannot be displayed because %1 has two parents', result.entity.label));
                            }

                            for (var i = 0; i < resp.entities.length; i++) {
                                if (resp.entities[i].uri === connectedEntityUri) {
                                    var def = resp.entities[i];
                                    def.parentUri = record.get('userData');
                                    var item = this.createItem(def, record.get('index') + 1, def.children ? 'collapsed' : '', connectedEntityUri);

                                    this.loadSecondaryLabels([def.uri]);

                                    store.insert(index + 1, item);
                                    this.tmpStore.splice(index + 1, 0, item);
                                    break;
                                }
                            }

                            this.unmask();
                        },
                        function(resp) {
                            MobileUI.core.Logger.error(resp);
                            this.unmask();
                            this.onPullRefresh();
                        }, this);
                }
            }

        },this,{single:true});
    },

    __onBeforeCreate: function(uri){
        return this.getRelationsTree().getStore().findExact('userData', uri) === -1;
    },

    mask: function() {
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
    },

    unmask: function() {
        this.getPanel().setMasked(null);
    },

    removeSubtree: function(record) {
        var store = this.getRelationsTree().getStore();
        var index = store.findExact('userData', record.get('userData'));
        var recordIndex = record.get('index');
        var firstIndex = index;
        var r;
        var size = store.getCount();
        var autoSync = store.getAutoSync();
        if (autoSync) {
            store.setAutoSync(false);
        }

        if (index !== -1) {
            while (++index < size) {
                r = store.getAt(index);
                if (r.get('index') <= recordIndex) {
                    break;
                }
            }
        }

        this.tmpStore.splice(firstIndex, index - firstIndex);

        for (var i = --index; i >= firstIndex; i--) {
            store.removeAt(i);
        }

        store.setAutoSync(autoSync);
        store.sync();
    }
});
