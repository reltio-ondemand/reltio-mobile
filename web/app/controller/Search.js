/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.Search', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.controller.MBaseSearch',
        'MobileUI.core.session.Session',
        'MobileUI.components.TrueSearchField',
        'MobileUI.components.SearchFilter',
        'MobileUI.core.Navigation',
        'MobileUI.controller.dialogs.SavedSearches'
    ],
    mixins: {
        tag: 'MobileUI.controller.MBaseSearch',
        savedSearches: 'MobileUI.controller.MSavedSearches'
    },
    config: {
        refs: {
            searchField: 'textfield[name=search]',
            filterButton: 'button[name=filterButton]',
            savedSearchesButton: 'button[name=savedSearchesButton]',
            foundList: 'elist[name=foundList]',
            filterList: 'searchfilter',
            totalLabel: 'component[name=searchview-totalLabel]',
            additionalMenuBtn: 'button[name=additionalMenu]'
        },
        control: {
            filterButton: {
                tap: 'onFilterButtonTap'
            },
            savedSearchesButton: {
                tap: 'onSavedSearchesButtonTap'
            },
            searchField: {
                action: 'onSearch'
            },
            foundList: {
                initialize: 'hideMask',
                selectItem: 'onSearchItemTap',
                fetch: 'onListFetch',
                pullRefresh: 'onRefresh'
            },
            filterList: {
                clear: 'onClearFilters'
            },
            additionalMenuBtn: {
                tap: 'onMenuTap'
            }
        },
        views: [
            'MobileUI.view.Search'
        ]
    },

    /**@type {MobileUI.components.Popup} @private */
    popup: null,
    /**@type {Array} @private */
    oldParameters: null,

    /**
     * @private
     */
    onFilterButtonTap: function ()
    {
        MobileUI.core.Navigation.redirectForward('search/searchfacet');
    },

    onSavedSearchesButtonTap: function()
    {
        MobileUI.controller.dialogs.SavedSearches.show();
    },

    launch: function ()
    {
        this.subscribeEvents();
        MobileUI.core.Metadata.on('init', function() {
            var advancedParams = MobileUI.core.search.SearchParametersManager.getAdvancedSearchParameters();
            this.getApplication().getController('SearchFacet').refreshFacets(true);
            MobileUI.core.search.SearchParametersManager.setAdvancedSearchParameters(advancedParams);
        }, this, {single: true});

        this.getApplication().getController('ViewActivator').on('activated', function() {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1) {
                this.getAdditionalMenuBtn().setHidden(false);
            }
        }, this);
    },

    onShow: function ()
    {
        var list = this.getFoundList();
        list.setMasked(true);
        this.resetSearchResults(true);

        var typeahead = MobileUI.core.search.SearchParametersManager.getTypeAhead();
        MobileUI.core.search.SearchParametersManager.setKeyword(typeahead);
    },

    /**
     * On search listener
     * @protected
     * @param searchField {textfield} text field
     */
    onSearch: function (searchField)
    {
        var value = searchField.getValue();
        MobileUI.core.search.SearchParametersManager.setKeyword(value);
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_FAST_ANIMATION);
        MobileUI.core.Navigation.redirectTo('search/init');
        this.getApplication().getController('Base').hidePopup();
        searchField.blur();
        return false;
    },


    /**
     * Hide mask listener
     */
    hideMask: function ()
    {
        this.getFoundList().setMasked(false);
    },

    onMenuTap: function ()
    {
        if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1)
        {
            return;
        }
        if (Ext.isEmpty(this.menu) || this.menu.isDestroyed)
        {
            this.menu = Ext.create('MobileUI.components.Popup');
            this.menu.setFilledAnchor(true);

            var items = [
                {
                    xtype: 'radiofield',
                    name: 'opt',
                    label: i18n('Reset'),
                    value: 'reset',
                    cls: 'without-border'
                }
            ];
            if (MobileUI.controller.dialogs.SavedSearches.isAddNewSearchAllowed()) {
                items.unshift({
                    xtype: 'radiofield',
                    name: 'opt',
                    label: i18n('Save As'),
                    value: 'saveas'
                });
            }

            var savedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch(),
                user = MobileUI.core.session.Session.getUser(),
                fullName = Ext.isEmpty(user) ? '' : user.full_name,
                owned = (savedSearch.owner === fullName);

            var cached = MobileUI.core.session.Session.getCachedSavedSearch(savedSearch.uri),
                cachedState = cached && cached.uiState && cached.uiState.state ,
                facetsState = savedSearch && savedSearch.uiState && savedSearch.uiState.state && savedSearch.uiState.state;

            var equality = cached && cachedState && facetsState &&
                MobileUI.core.util.Util.equals(cachedState.facets, facetsState.facets) &&
                cachedState.keyword === facetsState.keyword;

            if (savedSearch.name && owned && !equality) {
                items.unshift({
                    xtype: 'radiofield',
                        name: 'opt',
                    label: i18n('Save'),
                    checked: false,
                    value: 'save'
                });
            }

            this.menu.add({
                xtype: 'fieldset',
                name: 'sortOrder',
                cls: 'x-reltio-fieldset',
                defaults: {
                    labelWidth: 180
                },
                items: items
            });

            this.menu.setWidth(180);
            this.menu.setHeight(items.length * 41 + 2);

        }
        if (this.menu.isHidden() !== false)
        {
            this.menu.showBy(this.getAdditionalMenuBtn());
            this.menu.element.on('tap', function ()
            {
                var opt = this.menu.down('radiofield[name=opt]').getGroupValue() || '';
                switch (opt) {
                    case 'save':
                        var list = this.getFoundList();
                        list.setMasked(true);
                        this.savedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch();
                        this.save(function() {
                            list.setMasked(false);
                        });
                        break;
                    case 'saveas':
                        var savedSearch = MobileUI.core.search.SearchParametersManager.getSavedSearch();
                        var name = Ext.isEmpty(savedSearch.name) ? '' : i18n('%1 copy', savedSearch.name);
                        MobileUI.controller.dialogs.EditSavedSearch.show(null, true, name);
                        break;
                    case 'reset':
                        this.onClearFilters();
                        break;
                }
                this.menu.hide();
            }, this, {single: true});
        }
        else
        {
            this.menu.hide();
        }

        this.menu.addListener('hide', function() {
            this.menu = null;
        }, this);
    }
});
