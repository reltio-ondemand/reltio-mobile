/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.SearchFacet', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.core.search.SearchEngine',
        'MobileUI.controller.MBaseSearch',
        'MobileUI.core.Navigation',
        'MobileUI.components.facets.EntityTypeSearchFacet',
        'MobileUI.components.facets.SimpleSearchFacet',
        'MobileUI.components.facets.FavoriteFacet',
        'MobileUI.components.facets.AdvancedSearch'
    ],
    mixins: {
        tag:'MobileUI.controller.MBaseSearch'
    },
    config: {
        refs: {
            searchButton: 'button[name=search-facet-searchButton]',
            cancelButton: 'button[name=search-facet-cancelButton]',
            facets: 'container[name=searchfacetview-facets]',
            advanced: 'container[name=searchfacetview-advanced]',
            total: 'component[name=total]',
            hint: 'component[name=searchfacetview-hint]'
        },
        control: {
            searchButton: {
                tap: 'onSearch'
            },
            cancelButton: {
                tap: 'onCancelButtonTap'
            },
            facets : {
                initialize : 'onShow'
            }
        },
        views: [
            'MobileUI.view.SearchFacet'

        ]
    },
    facets: null,
    init: function(){
        this.advancedSearch = Ext.create('MobileUI.components.facets.AdvancedSearch');
        if (MobileUI.core.Metadata.isInited()) {
            this.advancedSearch.getMetadataManager().processMetadata();
        } else {
            MobileUI.core.Metadata.on('init', function () {
                this.advancedSearch.getMetadataManager().processMetadata();
            }, this);
        }
        this.advancedSearch.on('valueChanged', this.onTotal, this);
    },
    /**
     * @private
     */
    onCancelButtonTap: function() {
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
        MobileUI.core.Navigation.redirectTo('search/init');
    },

    /**
     * @private
     */
    refreshFacets: function(setFacets) {
        var facets = {};
        //facets[MobileUI.components.facets.FavoriteFacet.FIELD_NAME] = {fieldName:MobileUI.components.facets.FavoriteFacet.FIELD_NAME, title: 'Favorite', state: null};
        facets[MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME] = {collapsed: true, fieldName:MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME, title: i18n('Entity types'), state: null};
        facets['tags'] ={collapsed: true, fieldName:'tags', title: i18n('Tags'), state: null, actionLabel: i18n('Add Tag')};
        var state = MobileUI.core.search.SearchParametersManager.getFacets() || {};
        this.facets = [];
        if (this.getFacets())
        {
            this.getFacets().removeAll();
        }
        var facetsCount = Object.keys(facets).length;
        var i = 1;
        for (var key in facets){
            if(facets.hasOwnProperty(key))
            {
                var facetState = state[key] ? state[key] : facets[key];
                facetState.actionLabel = facets[key].actionLabel;
                this.facets.push(this.createSpecificFacet(facetState, facetsCount > i));
                i++;
            }
        }
        if(setFacets)
        {
           this.setFacets();
        }
        this.onTotal();
    },

    createSpecificFacet: function(params, needSeparator)
    {
        var facet, obj = {};
        if(params.fieldName === MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME){
            facet = Ext.create('MobileUI.components.facets.EntityTypeSearchFacet');
            if (params.values && !params.state)
            {
                params.state = MobileUI.components.facets.EntityTypeSearchFacet.fromValues(params.values);
            }
            facet.setParameters(params);
            facet.on('entityChanged', this.onEntityTypeChanged, this);
            if (params.state)
            {
                obj[MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME] = params.state;
                facet.refreshFacet(obj);
            }
        } else if (params.fieldName === MobileUI.components.facets.FavoriteFacet.FIELD_NAME){
            facet = Ext.create('MobileUI.components.facets.FavoriteFacet');
            facet.setParameters(params);
            if (params.state)
            {
                facet.refreshFacet(params.state);
            }
        }
        else {
            facet = Ext.create('MobileUI.components.facets.SimpleSearchFacet');
            facet.setParameters(params);
            if (params.values && !params.state)
            {
                params.state = MobileUI.components.facets.SimpleSearchFacet.fromValues(params.values);
            }
            facet.refreshFacet(params.state);
        }
        facet.on('valueChanged', this.onTotal, this);
        if (this.getFacets()){
            this.getFacets().add(facet);
            if (needSeparator)
            {
                this.getFacets().add({
                    xtype: 'component',
                    html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
                });
            }
        }
       return facet;
    },
    onEntityTypeChanged: function(uri){
        this.onEntityTypeChangedFlag = true;
        if (this.needRestore){
            this.needRestore = false;
            this.advancedSearch.restoreState(uri);
        }else {
            this.advancedSearch.onEntityTypeChanged(uri);
        }
        this.onEntityTypeChangedFlag = false;

        var hint = this.getHint();
        if (hint) {
            hint.setHidden(
                (this.advancedSearch.mainContainer && this.advancedSearch.mainContainer.items.length > 0) ||
                (this.advancedSearch.complexContainers && this.advancedSearch.complexContainers.length > 0));
        }
    },
    onTotal: function(){
        if (this.onEntityTypeChangedFlag)
        {
            return;
        }
        var params = this.facets.map(function(item){
           return item.getSearchParams();
        }).filter(function(item){
            return item && !Ext.isEmpty(item.values);
        }), isFacetInvalid = this.facets.some(function(item){
                return item.isValid && item.isValid() === false;
            });
        var advancedParams = this.advancedSearch.getParameters();
        params.push.apply(params, advancedParams);
        if (this.getSearchButton()){
            if (isFacetInvalid)
            {
                this.setTotal(0);
                this.getSearchButton().setDisabled(true);
                return;
            }
            this.getSearchButton().setDisabled(false);
        }
        var keyword = MobileUI.core.search.SearchParametersManager.getKeyword();
        if (keyword){
            params.push({
                fieldName: 'attributes',
                filter: 'containsWordStartingWith',
                values: [keyword],
                notForState: true
            });
        }
        if (this.getTotal())
        {
            MobileUI.core.session.Session.getSearchEngine().totalSearch(null, params, function (result)
            {
                var count = result[MobileUI.core.Services.getTenantName()] || 0;
                this.setTotal(count);
            }, null, this);
        }
    },
    setTotal: function(count) {
        if (this.getTotal()) {
            this.getTotal().setHtml('<div class ="header reltio-search">' +
                '    <span class ="filter-button">' + i18n('TOTAL') + '</span>' +
                '    <span class= "badge">' + i18n(count) + '</span>' +
                '</div>');
        }
    },
    resetFacets: function(){
        this.advancedSearch.resetParameters();
        (this.facets || []).forEach(function(item){
            if (item.resetFacet)
            {
                item.resetFacet();
            }
        });
    },

    setFacets: function()
    {
        var facets = {};
        this.facets.filter(function(item){
            return item && item.getState() && !Ext.isEmpty(item.getState().values);
        }).forEach(function (facet) {
            var state = facet.getState();
            facets[state.fieldName] = state;
        });

        if(Object.keys(facets).length === 0)
        {
            facets = null;
        }
        MobileUI.core.search.SearchParametersManager.setFacets(facets);
        var advancedParams = this.advancedSearch.getParameters();
        MobileUI.core.search.SearchParametersManager.setAdvancedSearchParameters(advancedParams);

    },

    onSearch: function()
    {
        setTimeout(function(){
            this.setFacets();
            MobileUI.core.Navigation.redirectForward('search/init');
        }.bind(this), 200);
    },

    /**
     * @private
     */
    onShow: function()
    {
        this.refreshFacets();
        MobileUI.core.session.Session.getLookupsManager().resetHierarchy();

        this.needRestore = true;
    }
});
