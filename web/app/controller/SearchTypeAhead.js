/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.SearchTypeAhead', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.Logger', 'MobileUI.core.search.SearchParametersManager', 'MobileUI.core.Navigation'],
    mixins: {
        typeAheadMixin: 'MobileUI.controller.MTypeAhead'
    },
    config: {
        refs: {
            cancelBtn: 'button[name=cancelBtn]',
            searchField: 'textfield[name=searchtypeaheadview-search]',
            list: 'elist[name=typeAheadList]'
        },
        control: {
            list: {
                selectItem: 'onSearchItemTap'
            },
            cancelBtn: {
                tap: 'onBack'
            },
            searchField: {
                keyup: 'onKeyUp',
                clearicontap: 'onClearIcon',
                action: 'onSearch',
                initialize: 'onSearchFieldPainted'
            }
        },
        views: ['MobileUI.view.SearchTypeAhead']
    },
    offset: 0,
    value: null,

    /** @private */
    typeaheadBehaviour: function() { return true; },

    /**
     * On search item tap listener
     */
    onSearchItemTap: function (item)
    {
        this.getApplication().getController('Search').onSearchItemTap(item);
    },

    /**
     * @protected
     * Typeahead listener
     */
    onTypeAheadSearch: function() {
        MobileUI.core.session.Session.typeAheadSearch({value: this.value},
            this.dataReceived, function(e) {
                MobileUI.core.Logger.log(e);
            }, this);
    },

    /**
     * @protected
     * back button listener
     */
    onBack: function ()
    {
        MobileUI.core.Navigation.stepBack('default', MobileUI.core.Navigation.FADE_ANIMATION);
    },

    /**
     * @protected
     * Search field listener
     */
    onSearchFieldPainted: function ()
    {
        var keyword = MobileUI.core.search.SearchParametersManager.getTypeAhead();
        var searchField = this.getSearchField();
        if (keyword !== null)
        {
            searchField.setValue(keyword);
            if (!Ext.isEmpty(keyword))
            {
                this.onTypeAheadSearch();
            }
        }

        setTimeout(function() {
            searchField.focus();
        }, 100);
    },
    /**
     * @protected
     * search action listener
     */
    onSearch: function ()
    {
        var searchField = this.getSearchField();
        MobileUI.core.search.SearchParametersManager.setKeyword(searchField.getValue());
        MobileUI.core.Navigation.setAnimation('default', MobileUI.core.Navigation.FADE_ANIMATION);
        MobileUI.core.Navigation.redirectTo('search/init');
    }

});
