/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.TwoHopsConnections', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.controller.MProfileBand',
        'MobileUI.components.RelationsFacet',
        'MobileUI.core.relations.RelationsUtils',
        'MobileUI.controller.MBaseRelation',
        'MobileUI.controller.MErrorHandling'
    ],
    mixins: {
        profileBand: 'MobileUI.controller.MProfileBand',
        baseRelations: 'MobileUI.controller.MBaseRelation',
        errorHandling: 'MobileUI.controller.MErrorHandling'

    },
    config: {
        refs: {
            twoHopsProfileBand: 'facetprofileband[name=twohopsProfileBand]',
            twoHopsFacet: 'relationsfacet[name=twohopsFacets]',
            twoHopsList: 'elist[name=twohopsList]',
            backToolbarBtn: 'button[name=backToolbarBtn]',
            additionalMenuBtn: 'button[name=additionalMenu]',
            panel: 'panel[name=twohops]',
            mainToolbar: 'toolbar'
        },
        control: {
            twoHopsFacet: {
                applyFilter: 'onFilterApplied'
            },
            additionalMenuBtn: {
                tap: 'onMenuTap'
            },

            twoHopsList: {
                selectItem: 'onEntitySelect',
                collapse: 'onCollapse',
                expand: 'onExpand',
                fetch: 'onRelationsListFetch',
                fetchDone: 'onRelationsListFetchDone',
                pullRefresh: 'onPullRefresh',
                'delete': 'onRelationDelete',
                refresh: 'onTwoHopsRefresh',
                editRelation: 'onEditRelation',
                showRelationAttributes: 'onShowRelationAttributes'
            },
            twoHopsProfileBand:{
                blockTap :'onBlockTap',
                favorited: 'onFavoriteTap'
            }
        },
        views: [
            'MobileUI.view.TwoHopsConnections'
        ]
    },

    launch: function ()
    {
        this.getApplication().getController('ViewActivator').on('activated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
            {
                MobileUI.core.search.SearchParametersManager.on('globalFilterChanged', this.onGlobalFilterChanged, this);
                if (MobileUI.core.Navigation.isBackAllow())
                {
                    this.getBackToolbarBtn().setHidden(false);
                }
                this.getAdditionalMenuBtn().setHidden(false);
            }
        }, this);
        this.getApplication().getController('ViewActivator').on('deactivated', function ()
        {
            if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) === -1) {
                MobileUI.core.search.SearchParametersManager.un('globalFilterChanged', this.onGlobalFilterChanged, this);
            }
        }, this);
    },

    statics: {
        SHOW_MORE_ITEM:'show_more'
    },
    /**@private @type {object} */
    twoHopsParams: null,
    /**@type {Array} @private */
    tmpStore: null,
    /**@type {String} @private */
    filter: null,
    /**@type {String} @private */
    savedFilter: null,
    /** @private */
    relationListOffset: 0,
    /** @private */
    relationListPage: 25,
    /** @private */
    offsets: {},
    secondaryFilter: {},
    inFetch: null,
    /**
     * Method parse the params
     * @private
     * @param params {Array}
     * @returns {{relationId: *, entityUrl: string}}
     */
    parseTwoHopsParams: function (params)
    {
        return {
            relationId: params[1],
            entityUrl: params.splice(2, params.length - 1).join('/')
        };
    },
    onFavoriteTap: function(item){
        this.processFavorite(item,MobileUI.core.session.Session.getEntityUri(), function(){
            this.getTwoHopsProfileBand().setFavorite(item);
        },this);
    },
    /**
     * Method parses params and shows connections
     * @param params
     */
    showTwoHopsConnection: function (params)
    {
        this.twoHopsParams = this.parseTwoHopsParams(params);
        this.showConnectionsInternal();
    },
    /**
     * On pull refresh listener
     */
    onPullRefresh: function ()
    {
        MobileUI.core.session.Session.requestEntity(this.twoHopsParams.entityUrl, this.showConnectionsInternal, function (resp)
        {
            MobileUI.core.Logger.error(resp);
        }, this);
    },
    onTwoHopsRefresh: function()
    {
        var renderSelector = Ext.query('div.rl-inner-filter');
        for (var i in renderSelector)
        {
            if (renderSelector.hasOwnProperty(i)) {
                this.createFilterField(renderSelector[i]);
            }
        }
    },
    createFilterField: function(element){
        if (Ext.get(element) && !Ext.get(element).down('form'))
        {
            Ext.create('MobileUI.components.TrueSearchField', {
                renderTo: element,
                value: this.secondaryFilter[element.getAttribute('data-name')] || '',
                cls: 'inner-search-field',
                width: Ext.os.is.Phone? 155 : 250,
                placeHolder: i18n('Filter'),
                listeners:{
                    clearicontap: function (item)
                    {
                        item.focus();
                        this.task.cancel();
                    },
                    focus: function (item) {
                        if (Ext.os.name === 'Android') {
                            if (this.getTwoHopsList()) {
                                var scroller = this.getTwoHopsList().getScrollable().getScroller();
                                var offset = item.element.dom.offsetTop;
                                scroller.scrollTo(0, offset);
                            }
                            this.getMainToolbar().setHidden(true);
                            this.getTwoHopsProfileBand().setHidden(true);
                            this.getTwoHopsFacet().setHidden(true);
                        }
                    },
                    blur: function(item){
                        if (Ext.os.name === 'Android')
                        {
                            this.getTwoHopsFacet().setHidden(false);
                            this.getTwoHopsProfileBand().setHidden(false);
                            this.getMainToolbar().setHidden(false);
                        }

                        this.task = Ext.create('Ext.util.DelayedTask', function(){
                            this.onInnerFilter(item);
                        });
                        this.task.setScope(this);
                        this.task.delay(200);
                    },
                    action: function (item)
                    {
                        this.task.cancel();
                        this.onInnerFilter(item);
                    },
                    scope:this
                }
            });
        }
    },

    onInnerFilter: function(item)
    {
        var parent = item.element.parent();
        if (parent)
        {
            var name = parent.dom.getAttribute('data-name');
            this.secondaryFilter[name] = item.getValue();
            var uri = this.getActionLabel(name);
            this.onParentDelete(name);
            this.pending(uri, function (resp) {
                    this.processLoadedChildren(resp, name);
                    this.getPanel().setMasked(null);
                }, Number(parent.dom.getAttribute('data-index')), this.relationListPage, 0, item.getValue()
            );
        }
    },
    /**
     * Method returns saved filter
     */
    getSavedFilter: function ()
    {
        return this.savedFilter;
    },
    /**
     * on collapse listener
     * @param obj {Object} event object
     */
    onCollapse: function (obj)
    {
        this.onExpand(obj);
    },
    /**
     * on expand listener
     * @param obj {Object} event object
     */
    onExpand: function (obj)
    {
        if (this.inFetch)
        {
            return;
        }
        var item = obj.record,
            state, offsetIndex,
            store = this.getTwoHopsList().getStore();
        if (item.get('children') === 'loaded')
        {
            state = item.get('state');
            offsetIndex = item.get('index');
            if (state === 'expanded')
            {
                this.collapseNode(item, obj.index, offsetIndex, store);
            }
            else if (state === 'collapsed')
            {
                this.expandNode(item, obj.index, offsetIndex, store);
            }
            this.onTwoHopsRefresh();
        }
        else
        {
            this.getPanel().setMasked({
                xtype: 'loadmaskex',
                message: '',
                cls: 'opaquemask',
                indicator: true
            });
            this.pending(this.getActionLabel(item.get('actionLabel')), function (resp)
            {
                this.processLoadedChildren(resp, item.get('actionLabel'));
                this.getPanel().setMasked(null);
            }, Number(item.get('index')), this.relationListPage, 0, '');
        }
        var list = this.getTwoHopsList();
        if (list && list.getScrollable() && list.getScrollable().getScroller())
        {
            var scroller = list.getScrollable().getScroller();
            setTimeout(function() {
                scroller.refresh();
            }, 250);
        }
    },
    /**
     * Method expands node if it was collapsed
     * @param item {MobileUI.components.list.model.EListModel} record
     * @param index {Number} record index
     * @param offset {Number} offset
     * @param store {Object} list store
     */
    expandNode: function (item, index, offset, store)
    {
        var uri, tmpRecord, tmpIndex, i, recordsToPush;
        uri = item.get('actionLabel');
        tmpRecord = null;
        for (i = 0; i < this.tmpStore.length; i++)
        {
            if (this.tmpStore[i].actionLabel === uri)
            {
                tmpRecord = this.tmpStore[i];
                tmpIndex = i;
                break;
            }
        }
        if (tmpRecord)
        {
            recordsToPush = [];
            for (i = tmpIndex + 1; i < this.tmpStore.length; i++)
            {
                var tmpItem = this.tmpStore[i];
                if (tmpItem.index === offset + 1)
                {
                    tmpItem.type = MobileUI.components.list.items.BaseListItem.types.twoHopsInfo;
                    if (tmpItem.state === 'expanded')
                    {
                        tmpItem.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
                        tmpItem.children = 'loaded';
                        tmpItem.state = 'collapsed';
                    }
                    recordsToPush.push(tmpItem);
                }
                else if (tmpItem.index === offset)
                {
                    break;
                }
            }
        }
        store.insert(index + 1, recordsToPush);
        item.set('state', 'expanded');
        item.set('stateIcon', 'resources/images/reltio/tree/expanded.svg');
    },
    /**
     * Method collapse node if it was expanded
     * @param item {MobileUI.components.list.model.EListModel} record
     * @param index {Number} record index
     * @param offset {Number} offset
     * @param store {Object} list store
     */
    collapseNode: function(item, index, offset, store)
    {
        var recordsToRemove, i, tmpRecord;
        recordsToRemove = [];
        for (i = index + 1; i < store.getCount(); i++)
        {
            tmpRecord = store.getAt(i);
            if (tmpRecord.get('index') === offset)
            {
                break;
            }
            recordsToRemove.push(tmpRecord);
        }
        store.remove(recordsToRemove);
        item.set('state', 'collapsed');
        item.set('stateIcon', 'resources/images/reltio/tree/collapsed.svg');
    },
    /**
     * Method processes the response and add it as children elements to parent element
     * @param resp  {Object} response
     * @param actionLabel {String} parent action uri
     */
    processLoadedChildren:function(resp, actionLabel)
    {

        if (!Ext.isEmpty(resp))
        {
            var store = this.getTwoHopsList().getStore(),
                recordIndex = store.findExact('actionLabel', actionLabel),
                record, records = [];

            if (recordIndex !== -1)
            {
                record = store.getAt(recordIndex).raw;
                record.type = MobileUI.components.list.items.BaseListItem.types.twoHopsInfo;
                record.children = 'loaded';
                record.stateIcon = 'resources/images/reltio/tree/expanded.svg';
                record.state = 'expanded';
                store.removeAt(recordIndex);
                records.push(record);
                var RelationsUtils = MobileUI.core.relations.RelationsUtils;
                if (RelationsUtils.isCorrectRelationType(RelationsUtils.getContentRelationsType(this.configuration[0].content), this.configuration[0])){
                    records.push({
                        label: i18n(this.configuration[0].secondActionLabel) || i18n('Add Member'),
                        disableSwipe: true,
                        index: 1,
                        actionLabel: MobileUI.controller.Relations.ADD_ITEM_URI,
                        userData: {specialUri: MobileUI.core.util.Util.generateUUID(6), parent: actionLabel},
                        offset: MobileUI.controller.RelationsTree.OFFSET,
                        type: MobileUI.components.list.items.BaseListItem.types.twoHopsInfo,
                        avatar: 'resources/images/reltio/relations/add-member.svg'
                    });
                }
                this.addChildren(records, resp, actionLabel, recordIndex, store);
            }
            var list = this.getTwoHopsList();
            if (list && list.getScrollable() && list.getScrollable().getScroller())
            {
                var scroller = list.getScrollable().getScroller();
                setTimeout(function() {
                    scroller.refresh();
                }, 250);
            }
            this.onTwoHopsRefresh();
        }
    },
    /**
     * Show more listener
     * @param resp  {Object} response
     * @param parent {String} parent uri
     * @param store {Object} list store
     */
    onShowMoreLoad: function(resp, parent, store){
        var i,  index, tmpIndex,records;
        records = (resp[0].connections || []).map(function (item)
        {
            return this.processConnection(item, 1, null);
        }, this);
        if ((resp[0].total > this.offsets[parent] + this.relationListPage))
        {
            records.push({
                label: i18n('Show More'),
                disableSwipe: true,
                index: 1,
                actionLabel: MobileUI.controller.TwoHopsConnections.SHOW_MORE_ITEM,
                userData: {specialUri: MobileUI.core.util.Util.generateUUID(6), parent: parent, showMore: true},
                offset: MobileUI.controller.RelationsTree.OFFSET,
                type: MobileUI.components.list.items.BaseListItem.types.twoHopsInfo,
                avatar: 'resources/images/reltio/relations/show-more.svg'
            });
        }
        for (i = 0; i < this.tmpStore.length; i++)
        {
            if (this.tmpStore[i].userData && this.tmpStore[i].userData.parent === parent && this.tmpStore[i].userData.showMore)
            {
                tmpIndex = i;
                break;
            }
        }
        this.tmpStore = this.tmpStore.slice(0, tmpIndex - 1).concat(records).concat(this.tmpStore.slice(tmpIndex + 1));
        index = store.findBy(function (record)
        {
            var data = record.get('userData');
            if (Ext.isObject(data))
            {
                return data.parent === parent && data.showMore;
            }
            return false;
        });
        if (index !== -1)
        {
            store.removeAt(index);
            store.insert(index, records);
        }
        var list = this.getTwoHopsList();
        if (list && list.getScrollable() && list.getScrollable().getScroller())
        {
            var scroller = list.getScrollable().getScroller();
            setTimeout(function() {
                scroller.refresh();
            }, 250);
        }
    },
    /**
     * Method add processed children to store
     * @param records {Array} list of records
     * @param resp {Object} api response
     * @param parent {String} parentUri
     * @param recordIndex {Number} place to add children
     * @param store {Object} list store
     */
    addChildren: function(records, resp, parent, recordIndex, store){
        var i,  tmpIndex;
        records = records.concat((resp[0].connections || []).map(function (item)
        {
            return this.processConnection(item, 1, null);
        }, this));
        this.offsets[parent] = this.relationListPage;
        if ((resp[0].total > this.relationListPage))
        {
            records.push({
                label: i18n('Show More'),
                disableSwipe: true,
                index: 1,
                userData: {parent: parent, showMore: true},
                actionLabel: MobileUI.controller.TwoHopsConnections.SHOW_MORE_ITEM,
                offset: MobileUI.controller.RelationsTree.OFFSET,
                type: MobileUI.components.list.items.BaseListItem.types.twoHopsInfo,
                avatar: 'resources/images/reltio/relations/show-more.svg'
            });
        }
        for (i = 0; i < this.tmpStore.length; i++)
        {
            if (this.tmpStore[i].actionLabel === parent)
            {
                tmpIndex = i;
                break;
            }
        }
        this.tmpStore = this.tmpStore.slice(0, tmpIndex).concat(records).concat(this.tmpStore.slice(tmpIndex + 1));
        store.insert(recordIndex, records);
    },
    /**
     * Method loads the relation data
     * @param uri {String} relation
     * @param success {Function?} success callback
     * @param index {Number?} index
     * @param max {Number?} max items count
     * @param offset {Number?} offset
     * @param filter {String?} filter
     */
    pending: function (uri, success, index, max, offset, filter)
    {
        var config = Ext.clone(this.configuration[0].contentSecondLevel);
        delete config.labels;
        filter = (filter !== null && filter !== undefined) ? filter: this.filter;
        MobileUI.core.session.Session.requestRelationsImmediately([config], filter, uri, max, offset, success, function(resp){
            this.getPanel().setMasked(null);
            this.getPanel().setMasked(false);
            if(resp && resp.errorDetailMessage) {
                this.showErrorMessage(this.getTwoHopsList().getStore(), resp.errorDetailMessage)
            }
        }, this);
    },
    /**
     * Method returns filter
     */
    getFilter: function ()
    {
        return this.filter;
    },
    /**
     * Method sets the filter
     * @param savedFilter
     */
    setSavedFilter: function (savedFilter)
    {
        this.savedFilter = savedFilter;
    },
    /**
     * @private
     * Method shows connection
     */
    showConnectionsInternal: function ()
    {
        this.filter = this.getSavedFilter();
        this.setSavedFilter(null);
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        if (MobileUI.core.session.Session.getEntityUri() === this.twoHopsParams.entityUrl)
        {
            this.processTwoHopsRelation(MobileUI.core.session.Session.getEntity());
        }
        else
        {
            MobileUI.core.session.Session.requestEntity(this.twoHopsParams.entityUrl, this.processTwoHopsRelation, function (resp)
            {
                MobileUI.core.Logger.error(resp);
            }, this);
        }
    },
    /**
     * Method processes two hops
     * @private
     * @param entity {Object} entity
     */
    processTwoHopsRelation: function (entity)
    {
        var configuration = MobileUI.core.Services.getConfigurationManager(),
            config, facet;
        config = configuration.getExtensionById(this.twoHopsParams.relationId);
        MobileUI.core.relations.RelationsUtils.processRelations(config);
        this.configuration = [config];
        facet = Ext.clone(config);
        facet.total = 0;
        this.getTwoHopsFacet().setFacet(facet);
        this.fillRelation(this.twoHopsParams.entityUrl);
        this.fillBand(this.getTwoHopsProfileBand(), entity);
    },

    /**@private*/
    initRelationsList: function (store)
    {
        var item = {
            label: i18n(this.configuration[0].actionLabel) || i18n('Add Member'),
            actionLabel: MobileUI.controller.Relations.ADD_ITEM_URI,
            userData: {specialUri: MobileUI.core.util.Util.generateUUID(6)},
            disableSwipe: true,
            type: MobileUI.components.list.items.BaseListItem.types.twoHopsInfo,
            avatar: 'resources/images/reltio/relations/add-member.svg'
        };
        if (store)
        {
            store.clearData();
            var RelationsUtils = MobileUI.core.relations.RelationsUtils;
            if (RelationsUtils.isCorrectRelationType(RelationsUtils.getContentRelationsType(this.configuration[0].content), this.configuration[0]))
            {
                store.add([item]);
            }
        }
        return item;
    },

    getOptions: function ()
    {
        if (!Ext.isEmpty(this.configuration[0].sortOptions))
        {
            return this.configuration[0].sortOptions;
        }

    },
    /**
     * Method fills relation
     * @param entityUrl {String?} entity uri
     * @param max {Number?} max items count
     * @param max {Number?} max items count
     * @param offset {Number?} offset
     */
    fillRelation: function (entityUrl, max, offset)
    {
        var config,
            list = this.getTwoHopsList(),
            store = list.getStore();

        max = max || this.relationListPage;
        offset = offset || 0;

        if (offset === 0)
        {
            this.initRelationsList(store);
        }

        this.relationListOffset = offset;
        this.relationListPage = max;

        if (!Ext.isEmpty(this.configuration))
        {
            config = this.configuration.map(function (item)
            {
                var result = item.content,
                    key;
                result.order = Ext.isEmpty(result.order, false) ? item.sortOrder : result.order;
                if (Ext.isEmpty(result.sortBy, true) && !Ext.isEmpty(item.sortOptions))
                {
                    for (key in item.sortOptions)
                    {
                        if (item.sortOptions.hasOwnProperty(key))
                        {
                            result.sortBy = key;
                            break;
                        }
                    }
                }
                return result;
            });

            MobileUI.core.session.Session.requestRelationsImmediately(config, this.filter, null, max, offset, function (resp)
            {
                var that = this, facet = Ext.clone(that.configuration),
                    connections, item,
                    model = [],
                    i;
                if (!Ext.isEmpty(resp) && !Ext.isEmpty(resp))
                {
                    facet[0].total = resp[0].total;
                    this.getTwoHopsFacet().setFacet(facet[0]);
                    this.getTwoHopsFacet().setFilter(that.filter);
                    this.getTwoHopsFacet().setFilterEnabled(!Ext.isEmpty(that.filter));

                    connections =  (resp[0].connections || []).map(function (item)
                    {
                        item.children = [];
                        return item;
                    });

                    if (!Ext.isEmpty(connections))
                    {
                        for (i = 0; i < connections.length; i++)
                        {
                            model.push(this.processConnection(connections[i], 0, 'collapsed'));
                        }
                    }
                    //we should reinit list on data received
                    if (offset === 0)
                    {
                        item = this.initRelationsList(store);
                        this.tmpStore = [item];
                    }
                    this.tmpStore = this.tmpStore.concat(Ext.clone(model));
                    store.add(model);
                    list.fetchDone(model.length === max);
                }
                this.getPanel().setMasked(null);
            }, function(resp){
                this.getPanel().setMasked(null);
                this.getPanel().setMasked(false);
                if(resp && resp.errorDetailMessage) {
                    this.showErrorMessage(this.getTwoHopsList().getStore(), resp.errorDetailMessage)
                }
            }, this);
        }
    },

    /** @private */
    onRelationsListFetch: function ()
    {
        this.inFetch = true;
        this.fillRelation(this.twoHopsParams.entityUrl, this.relationListPage, this.relationListOffset + this.relationListPage);
    },

    onRelationsListFetchDone: function()
    {
        this.inFetch = false;
        var list = this.getTwoHopsList();
        if (list && list.getScrollable() && list.getScrollable().getScroller())
        {
            var scroller = list.getScrollable().getScroller();
            setTimeout(function() {
                scroller.refresh();
            }, 250);
        }
    },

    /**
     * Method convert connections to the model items
     * @param connection {Object} connection object
     * @param index
     * @param state
     */
    processConnection: function (connection, index, state)
    {
        var entity, entityType, item = {}, label;

        entity = connection.entity;
        label = MobileUI.core.entity.EntityUtils.processLabel(entity.entityLabel);
        item.label = label;
        entityType = MobileUI.core.Metadata.getEntityType(entity.entityType);
        item.type = MobileUI.components.list.items.BaseListItem.types.twoHopsInfo;
        item.tags = [{
            color: entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR,
            value: entityType.label
        }, {
            color: MobileUI.core.search.SearchResultBuilder.CONNECTION_BADGE_COLOR,
            textColor: MobileUI.core.search.SearchResultBuilder.CONNECTION_BADGE_TEXT_COLOR,
            value: connection.relation.relationLabel
        }];

        item.type = MobileUI.components.list.items.BaseListItem.types.twoHopsInfo;
        item.avatar = entity.defaultProfilePicValue;
        item.allowedSwipeElements = [];
        if (MobileUI.core.PermissionManager.securityService().metadata.canDelete(connection.relation.type)){
            item.allowedSwipeElements.push('delete');
        }
        if (MobileUI.core.PermissionManager.securityService().metadata.canUpdate(connection.relation.type)){
            item.allowedSwipeElements.push('editRelation');
        }
        item.defaultImage = MobileUI.core.Services.getBorderlessImageUrl(MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
        item.offset = (index || 0) * MobileUI.controller.RelationsTree.OFFSET || 0;
        item.index = index;
        item.hidden = false;
        switch (state)
        {
            case 'expanded':
                item.stateIcon = 'resources/images/reltio/tree/expanded.svg';
                item.children = 'loaded';
                break;
            case 'collapsed':
                item.stateIcon = 'resources/images/reltio/tree/collapsed.svg';
                item.children = 'not';
                break;
            default:
                item.stateIcon = '';
        }
        item.state = state;
        item.actionLabel = MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) ? entity.entityUri + '$$$'+ MobileUI.core.util.Util.generateUUID() : null;
        item.userData = connection.relation.relationUri;
        return item;
    },

    getActionLabel: function(label){
        if (label && label.indexOf('$$$') !== -1)
        {
            label = label.substring(0, label.indexOf('$$$'));
        }
        return label;
    },
    /**
     * On show relation attributes listener
     * @param event
     */
    onShowRelationAttributes: function(event)
    {
        var uri = [this.twoHopsParams.relationId,
            this.twoHopsParams.entityUrl,
            event.getData().userData
        ].join('/');

        MobileUI.core.Navigation.redirectForward('relation/' +
            MobileUI.core.util.HashUtil.encodeUri(uri));
    },
    /**
     * On enity select listener
     * @protected
     * @param event {Object} data record
     */
    onEntitySelect: function (event)
    {
        var actionLabel = this.getActionLabel(event.get('actionLabel')), userData;
        if (!actionLabel)
        {
            return;
        }
        if (actionLabel !== MobileUI.controller.Relations.ADD_ITEM_URI && actionLabel !== MobileUI.controller.TwoHopsConnections.SHOW_MORE_ITEM)
        {
            MobileUI.core.Navigation.redirectForward('profile/' +
                MobileUI.core.util.HashUtil.encodeUri(actionLabel));
        }
        else
        {
            userData = event.get('userData');
            if (userData)
            {
                if (userData.showMore)
                {
                    if (this.inFetch)
                    {
                        return;
                    }
                    this.onShowMore(userData.parent);
                }
                else if (userData.parent)
                {
                    this.addRelation(userData.parent);
                }
                else
                {
                    this.addRelation();
                }
            }
        }
    },
    onShowMore: function(parent) {
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
        this.pending(this.getActionLabel(parent), function(resp){
            var list = this.getTwoHopsList();
            this.onShowMoreLoad(resp, parent, list.getStore());
            this.getPanel().setMasked(null);
            this.offsets[parent] += this.relationListPage;
        }, null, this.relationListPage, this.offsets[parent], this.secondaryFilter[parent] || '');
    },

    /**
     * Relation delete listener
     * @param event {Object} event
     */
    onRelationDelete: function (event)
    {
        if (event.getData())
        {
            var uri = event.getData().userData;
            if (!Ext.isEmpty(uri))
            {
                this.getPanel().setMasked({
                    xtype: 'loadmaskex',
                    message: '',
                    cls: 'opaquemask',
                    indicator: true
                });
                MobileUI.core.session.Session.removeRelation(uri, function (resp)
                {
                    if (Ext.isEmpty(resp))
                    {
                        MobileUI.core.Logger.error('The remove relation ' + uri + 'is not done');
                        return;
                    }

                    resp = Ext.isArray(resp) ? resp[0] : resp;
                    if (resp && resp.status === 'success')
                    {
                        this.onPullRefresh();
                    }
                    this.getPanel().setMasked(null);
                }, function (error)
                {
                    this.getPanel().setMasked(null);
                    MobileUI.core.Logger.error(error);
                }, this);
            }
        }
    },

    /**
     * Filter applied listener
     * @param filterText {String} filter text
     */
    onFilterApplied: function (filterText)
    {
        this.filter = filterText;
        this.fillRelation();
    },

    /**
     * Add relation listener
     */
    addRelation: function (parent)
    {
        if (parent)
        {
            MobileUI.core.relations.RelationsManager.setBaseEntityUri(this.getActionLabel(parent));
            this.getApplication().getController('dialogs.RelationshipType').showRelationshipTypeDialog(this.configuration[0].contentSecondLevel, {allowOnlyCreateNew: this.configuration[0].allowOnlyCreateNewSecondLevel, hiddenRelationAttributes: this.configuration[0].hiddenRelationAttributesSecondLevel});
            var addRelation = function ()
            {
                var that = this.self;
                var parent = this.parent;
                that.onParentDelete(parent);
                that.getPanel().setMasked({
                    xtype: 'loadmaskex',
                    message: '',
                    cls: 'opaquemask',
                    indicator: true
                });
                that.pending(that.getActionLabel(parent), function (resp)
                    {
                        that.processLoadedChildren(resp, parent);
                        that.getPanel().setMasked(null);
                    }, 0, that.relationListPage, 0
                );
            };
            var context = {self:this, parent: parent};
            MobileUI.core.relations.RelationsManager.on('createRelation', addRelation, context , {single: true});

            MobileUI.core.SuiteManager.on('hideDialogs', function ()
            {
                MobileUI.core.relations.RelationsManager.resetBaseEntityUri();
                MobileUI.core.relations.RelationsManager.un('createRelation', addRelation, context, {single: true});
            }, this, {single: true, delay:500});
        }
        else
        {
            this.getApplication().getController('dialogs.RelationshipType').showRelationshipTypeDialog(this.configuration[0].content, this.configuration[0]);
            var callback = function ()
            {
                this.onPullRefresh();
            };
            MobileUI.core.relations.RelationsManager.on('createRelation', callback, this, {single: true});
            MobileUI.core.SuiteManager.on('hideDialogs', function ()
            {
                MobileUI.core.relations.RelationsManager.un('createRelation', callback);
                MobileUI.core.relations.RelationsManager.resetBaseEntityUri();
            }, this, {single: true, delay:500});
        }
    },

    getGroupValue: function ()
    {
        return {
            order: this.configuration[0].content['order'],
            content: this.configuration[0].content['sortBy']
        };
    },

    onParentDelete: function(parent)
    {
        var recordsToRemove = [], i, tmpRecord,
            list = this.getTwoHopsList(),
            store = list.getStore(),
            index = store.findExact('actionLabel', parent);
        if (index !== -1)
        {
            for (i = index + 1; i < store.getCount(); i++)
            {
                tmpRecord = store.getAt(i);
                if (tmpRecord.get('index') === 0)
                {
                    break;
                }
                recordsToRemove.push(tmpRecord);
            }
            store.remove(recordsToRemove);
        }
        this.tmpStore = this.tmpStore.filter(function(item){
            for (var i = 0; i < recordsToRemove.length; i++)
            {
                if (recordsToRemove[i].get('actionLabel') && recordsToRemove[i].get('actionLabel') === item.actionLabel)
                {
                    return false;
                }
                else if (recordsToRemove[i].get('userData') && item.userData && item.userData.specialUri && recordsToRemove[i].get('userData').specialUri === item.userData.specialUri)
                {
                    return false;
                }
            }
            return true;
        });
    },
    /**
     * Menu callback
     * @param order
     * @param sortBy
     */
    processMenuAction: function (order, sortBy)
    {
        if (this.configuration[0] && this.configuration[0].content)
        {
            var currentOrder = this.configuration[0].content['order'];
            var currentSortBy = this.configuration[0].content['sortBy'];
            if (currentOrder !== order || currentSortBy !== sortBy)
            {
                this.configuration[0].content['order'] = order;
                this.configuration[0].content['sortBy'] = sortBy;
                this.fillRelation();
            }
        }
    },

    /**
     * Edit relation listener
     * @param record {Object}
     */
    onEditRelation: function(record)
    {
        var path = [this.twoHopsParams.relationId,
            this.twoHopsParams.entityUrl,
            record.getData().userData,
            'twohopsconnection'
        ].join('/');
        MobileUI.core.Navigation.redirectForward('relation/edit$' + MobileUI.core.util.HashUtil.encodeUri(path), false);
    },

    onGlobalFilterChanged: function ()
    {
        this.onPullRefresh();
    }
});
