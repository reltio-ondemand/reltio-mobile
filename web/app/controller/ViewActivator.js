/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.ViewActivator', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.core.Navigation',
        'MobileUI.core.ViewManager',
        'MobileUI.core.Metadata',
        'MobileUI.core.Logger',
        'MobileUI.core.I18n',
        'MobileUI.core.util.Cookie',
        'MobileUI.components.MessageBox',
        'MobileUI.core.session.Session'
    ],
    statics: {
        SEARCH_PARAMS_PROCESSOR: function (params)
        {
            var result = {},
                viewMap = {
                    typeahead: 'searchtypeahead',
                    searchfacet: 'searchfacet',
                    saved: 'savedsearches',
                    defaultView: 'search'
                };

            result.showMenu = true;
            result.view = viewMap[params.trim()] || viewMap.defaultView;
            return result;
        },
        TENANT_NOT_EXISTS_ERROR: 127,
        TENANT_NOT_EXISTS_ERROR_2: 309
    },
    config: {
        before: {
            processLogin: ['checkReady', 'initI18nInsecure', 'beforeLogin'],
            processAfterRestorePassword: ['checkReady', 'initI18nInsecure'],
            processForgotPassword: ['checkReady', 'initI18nInsecure'],
            processRestorePassword: ['checkReady', 'initI18nInsecure'],
            processProblems: ['checkReady', 'initI18nInsecure'],
            processSearch: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processProfile: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processEditProfile: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processNewEditMode: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processRelations: ['checkReady',  'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processRelation: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processAttributesFacet: ['checkReady',  'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processCustomViewFacet: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processPlacesFacet: ['checkReady', 'initMap', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processRelationsTree: ['checkReady', 'authenticate','checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processFeedBack: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences'],
            processDashboard: ['checkReady', 'authenticate', 'checkForANewVersion', 'initI18n', 'loadMetadata', 'loadUserPreferences']
        },
        routes: {
            'search/:params': {
                action: 'processSearch',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'profile/:params': {
                action: 'processProfile',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'edit/:params': {
                action: 'processNewEditMode',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'attributesfacet/:params': {
                action: 'processAttributesFacet',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'customview/:params': {
                action: 'processCustomViewFacet',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'placesfacet/:params': {
                action: 'processPlacesFacet',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'relations/:params': {
                action: 'processRelations',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'relation/:params': {
                action: 'processRelation',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'relationstree/:params': {
                action: 'processRelationsTree',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            'forgotpassword': {
                action: 'processForgotPassword'
            },
            'problems': {
                action: 'processProblems'
            },
            'password/:params': {
                action: 'processRestorePassword'
            },
            'newPassword/:params': {
                action: 'processAfterRestorePassword'
            },
            'login': {
                action: 'processLogin'
            },
            'feedback': {
                action: 'processFeedBack'
            },
            'dashboard/:params': {
                action: 'processDashboard',
                conditions: {
                    ':params': '[0-9a-zA-Z.~%_\\$]+'
                }
            },
            ':emptyparams': {
                action: 'redirectOnLogin',
                conditions: {
                    ':emptyparams': '?![\\s\\S]'
                }
            }
        }
    },
    /**@type{MobileUI.core.ViewManager}*/
    viewManager: null,
    /**@type{Boolean}*/
    tenantExists: null,
    mapIsReady: false,
    /**
     * @private
     * @type{Boolean}
     */
    userPreferencesReady: false,

    redirectOnLogin: function()
    {
        var location = MobileUI.core.Services.getLocationObject();
        if (location.hash && location.hash.length > 1){
            this.locationHash = location.hash.substring(1);
        }
        MobileUI.core.Navigation.redirectForward('login');
    },
    getPreviousState: function(){
        return this.locationHash;
    },
    resetPreviousState: function(){
        this.locationHash = null;
    },
    init: function ()
    {
        //noinspection JSUnusedLocalSymbols
        Ext.Viewport.on('activeitemchange', function (name, value)
        {
            if (value && value.getId){
                MobileUI.core.session.Session.setActiveView(value.getId());
            }
            if (!MobileUI.core.SuiteManager.isDialog(value)) {
                this.fireEvent('deactivated');
            }
        }, this);
        this.viewManager = Ext.create('MobileUI.core.ViewManager');
        this.viewManager.initialize();
        this.viewManager.on('activated', function ()
        {
            this.fireEvent('activated');
        }, this);

        this.viewManager.on('rootClicked', function (event)
        {
            this.fireEvent('rootClicked',event);
        }, this);
    },
    /**
     * Route search processor
     * @protected
     * @param params {String}
     */
    processSearch: function (params)
    {
        params = params || '';
        this.viewManager.show(MobileUI.controller.ViewActivator.SEARCH_PARAMS_PROCESSOR(params));
    },
    /**
     * Route dashboard processor
     * @protected
     * @param params {String}
     */
    processDashboard: function(params) {
        params = params || '';
        var controllerName = 'dashboard.Dashboard';
        this.viewManager.show({view: 'dashboard', showMenu: true, singleView: true});
        this.getApplication().getController(controllerName).showDashboard(params);
    },

    initMap: function(action){
        if(this.mapIsReady){
            action.resume();
        }
        else
        {
            var me = this;
            var script = document.createElement('script');
            script.setAttribute('src', 'https://www.google.com/jsapi');
            script.onload = function () {
                var clientId = MobileUI.core.Services.getGeneralSettings().mapClientId;
                var otherParams = clientId ? 'key=' + clientId : '';
                google.load('maps', '3.9', {
                    other_params: otherParams + '&sensor=false',
                    callback: function () {
                        me.mapIsReady = true;
                        action.resume();
                    }
                });
            };
            document.head.appendChild(script);

        }
    },
    /**
     * Route relation processor
     * @protected
     * @param params {String}
     */
    processRelations: function (params)
    {
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        if (paramsArray[0] === 'comments')
        {
            controllerName = 'Comments';
            this.viewManager.show({view: 'comments', showMenu: true});
            this.getApplication().getController(controllerName).showComments(paramsArray);
        }
        else if (paramsArray[0] === 'addcomments')
        {
            controllerName = 'AddComments';
            this.viewManager.show({view: 'addcomments', showMenu: true});
            this.getApplication().getController(controllerName).setParams(paramsArray);
        }
        else if (paramsArray[0] === 'twohopsconnection')
        {
            controllerName = 'TwoHopsConnections';
            this.viewManager.show({view: 'twohopsconnection', showMenu: true});
            this.getApplication().getController(controllerName).showTwoHopsConnection(paramsArray);
        }
        else
        {
            controllerName = 'Relations';
            this.viewManager.show({view: 'relations', showMenu: true});
            this.getApplication().getController(controllerName).showRelation(paramsArray);
        }
    },
    /**
     * Route concrete relation processor
     * @param params
     */
    processRelation: function(params){
        var paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        if (paramsArray[0] === 'complex') {
            this.viewManager.show({view: 'relationcomplexattributes', showMenu: true, singleView: true});
            paramsArray.shift();
            this.getApplication().getController('RelationComplexAttributes').showRelation(paramsArray);
        } else if (paramsArray[0] === 'edit') {
            this.viewManager.show({view: 'editrelationattributes', showMenu: true, singleView: true});
            paramsArray.shift();
            this.getApplication().getController('EditRelationAttributes').showEditRelation(paramsArray);
        } else{
            this.viewManager.show({view: 'relationattributes', showMenu: true, singleView: true});
            this.getApplication().getController('RelationAttributes').showRelation(paramsArray);
        }
    },
    /**
     * Route attributes facet processor
     * @protected
     * @param params {String}
     */
    processAttributesFacet: function (params)
    {
        params = params || '';
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        controllerName = 'AttributesFacet';
        this.viewManager.show({view: 'attributesfacet', showMenu: true});
        this.getApplication().getController(controllerName).showAttributesFacet(paramsArray);
    },

    /**
     * Route places facet processor
     * @protected
     * @param params {String}
     */
    processPlacesFacet: function (params)
    {
        params = params || '';
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        controllerName = 'PlacesFacet';
        this.viewManager.show({view: 'placesfacet', showMenu: true});
        this.getApplication().getController(controllerName).showPlacesFacet(paramsArray);
    },
    /**
     * Route relation tree processor
     * @protected
     * @param params {String}
     */
    processRelationsTree: function (params)
    {
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        controllerName = 'RelationsTree';
        this.viewManager.show({view: 'relationstree', showMenu: true});
        this.getApplication().getController(controllerName).showRelationsTree(paramsArray);
    },
    /**
     * Method return true if on the switch window the activate event will be fired
     * @returns {boolean}
     */
    waitForActivate: function ()
    {
        return this.viewManager.waitForActivate();
    },
    /**
     * Route profile processor
     * @protected
     * @param params {String}
     */
    processProfile: function (params)
    {
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        if (paramsArray.length === 2)
        {
            controllerName = 'Profile';
            this.viewManager.show({view: 'profile', showMenu: true});
            this.getApplication().getController(controllerName).showEntity(paramsArray);
        }
        else if (paramsArray.length > 2)
        {
            controllerName = 'ComplexAttributes';
            this.viewManager.show({view: 'complex', showMenu: true});
            this.getApplication().getController(controllerName).showComplexAttribute(paramsArray);
        }
    },
    processCustomViewFacet: function(params){
        var paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        this.viewManager.show({view: 'custom', showMenu: true, singleView: true});
        this.getApplication().getController('CustomViewController').showCustomView(paramsArray);
    },

    processNewEditMode: function(params){
        var controllerName, paramsArray;
        params = params || '';
        paramsArray = params.split('$');
        if (paramsArray.length >= 2)
        {
            controllerName = 'NewEditMode';
            this.viewManager.show({view: 'neweditmode', showMenu: true});
            this.getApplication().getController(controllerName).entryPoint(paramsArray);
        }
    },
    /**
     * Route forgot password processor
     * @protected
     */
    processForgotPassword: function ()
    {
        this.viewManager.show({view: 'forgotpassword', showMenu: false});
    },

    processProblems: function(){
        this.viewManager.show({view: 'errorpage', showMenu: false});
        this.getApplication().getController('ErrorPage').showError(this.error);
        this.error = null;
    },
    /**
     * Route restore password processor
     * @protected
     * @param parameters {String}
     */
    processRestorePassword: function (parameters)
    {
        this.viewManager.show({view: 'passwordrestore', showMenu: false});
        this.getApplication().getController('PasswordRestore').setMode(parameters);
    },
    /**
     * Route new password processor
     * @param parameters
     */
    processAfterRestorePassword: function(parameters)
    {
        this.getApplication().getController('NewPassword').setToken(parameters);
        this.viewManager.show({view: 'newpassword', showMenu: false});
    },
    /**
     * Route processes feedback
     */
    processFeedBack: function ()
    {
        this.viewManager.show({view: 'feedback', showMenu: false});
    },

    /**
     * Route login processor
     * @protected
     */
    processLogin: function ()
    {
        this.viewManager.show({view: 'login', showMenu: false});
    },
    /**
     * Method returns is menu shown or not
     * @returns {*}
     */
    isMenuShown: function ()
    {
        return this.viewManager.isMenuShown();
    },
    /**
     * Authentificate filter
     * @protected
     * @param action {Ext.app.Action}
     */
    authenticate: function (action)
    {
        var Session = MobileUI.core.session.Session;

        if (Session.getAccessToken() !== null)
        {
            if (this.tenantExists)
            {
                action.resume();
            }
            else
            {
                Session.initSession(function ()
                {
                    var that = this;
                    that.tenantExists = true;
                    action.resume();
                }, function (resp)
                {
                    if (resp)
                    {
                        MobileUI.components.MessageBox.alert('Login failed', resp.errorMessage);
                    }
                }, this);
            }
        }
        else
        {
            MobileUI.core.session.Session.refreshAccessToken(
                function ()
                {
                    var that = this;
                    Session.initSession(function ()
                    {
                        action.resume();
                    }, that.onAuthenticationFailed, that);

                }, this.onAuthenticationFailed, this);
        }
    },

    checkForANewVersion: function(action){
        MobileUI.core.Services.getUserPreference('version',
            '*',
            function (version) {
                if (version && window.localStorage && version !== window.localStorage.getItem('mobileUIVersion')){
                    Object.keys(window.localStorage).filter(function(key){
                        return key.indexOf('-') !== -1;
                    }).forEach(function(key){
                        window.localStorage.removeItem(key);
                    });
                    window.localStorage.setItem('mobileUIVersion',version);
                }
                action.resume();
            },
            function() {
                action.resume();
            }, this);
    },

    loadMetadata: function (action)
    {
        var md = MobileUI.core.Metadata;
        if (md.isInited())
        {
            this.loadLookups(action);
        }
        else
        {
            md.on('init', function ()
            {
                if (window.standaloneObserver) {
                    window.standaloneObserver.disconnect();
                    delete window.standaloneObserver;
                }
                this.loadLookups(action);
            }, this, {single: true});
            md.init();
        }
    },
    loadLookups: function(action) {
        if (!MobileUI.core.Metadata.isDependentLookupsEnabled()){
            MobileUI.core.Lookups.loadLookups(action.resume, action.resume, action);
        }else {
            action.resume();
        }
    },
    /**
     * Load user preferences
     * @protected
     * @param action {Ext.app.Action}
     */
    loadUserPreferences: function (action)
    {
        if (!this.userPreferencesReady)
        {
            this.userPreferencesReady = true;

            MobileUI.core.search.SearchParametersManager.initGlobalFilter(function() {
                var uris = MobileUI.core.search.SearchParametersManager.getGlobalFilter().uris,
                    observer;

                if (uris.length > 0) {
                    observer = Ext.create('MobileUI.core.Observer', {
                        threadNames: uris,
                        listener:  function() {
                            var ss = MobileUI.core.search.SearchParametersManager.getSavedSearch();
                            if (ss && ss.uri) {
                                //Cache original SS to be able to compare with current one and hide 'Save' button if equals
                                MobileUI.core.session.Session.getSavedSearch(ss.uri, action.resume, action.resume, action);
                            }
                            else {
                                action.resume();
                            }
                        }
                    });

                    uris.forEach(function(uri) {
                        MobileUI.core.session.Session.getSavedSearch(uri, function() {
                                observer.checkThread(uri);
                            },
                            function(error) {
                                MobileUI.core.Logger.log(error);
                            });
                    });
                }
                else {
                    action.resume();
                }
            });
        }
        else
        {
            action.resume();
        }
    },

    /**
     * @protected
     * Auth failed listener
     */
    onAuthenticationFailed: function ()
    {
        this.redirectOnLogin();
    },
    /**
     * Configuration ready filter
     * @protected
     * @param action {Ext.app.Action}
     */
    checkReady: function (action)
    {
        if (MobileUI.core.Services.getGeneralSettings() !== null)
        {

            action.resume();
        }
        else
        {
            MobileUI.core.Services.updateSettings(function ()
            {
                this.resume();
            }, action);
        }
    },

    initI18n: function (action) {
        MobileUI.core.I18n.init(function () {
            action.resume();
        });
    },

    initI18nInsecure: function(action) {
        MobileUI.core.I18n.init(function() {
            action.resume();
        }, true);
    },

    /**
     * Before login
     * @protected
     * @param action {Ext.app.Action}
     */
    beforeLogin: function (action)
    {
        MobileUI.core.session.Session.checkAccessTokenFromStore();
        var arrTokens = MobileUI.core.util.Cookie.get('tokens');
        var removeCookie = function(cookieName) {
            MobileUI.core.util.Cookie.del(cookieName);
            MobileUI.core.util.Cookie.del(cookieName, '/');
        };
        var code = MobileUI.core.util.Cookie.get('error_code');
        if (code){
            var text = MobileUI.core.util.Cookie.get('error_text');
            removeCookie('error_text');
            removeCookie('error_code');
            this.error = {
                code: code,
                text: text
            };

            MobileUI.core.Navigation.redirectTo('problems');
            return;
        }

        if (MobileUI.core.session.Session.getAccessToken() !== null)
        {
            MobileUI.core.session.Session.initSession(function ()
            {
                var that = this;
                that.tenantExists = true;
                MobileUI.core.session.Session.requestUserInfo();
                if (that.getPreviousState()) {
                    MobileUI.core.Navigation.redirectTo(that.getPreviousState());
                }else{
                    MobileUI.core.Navigation.redirectToDefault();
                }
            }, function (resp)
            {
                if (resp)
                {
                    MobileUI.core.session.Session.removeTokens();
                    if (MobileUI.core.util.Util.supportSSO()){
                        if (resp.errorCode === MobileUI.controller.ViewActivator.TENANT_NOT_EXISTS_ERROR || resp.errorCode === MobileUI.controller.ViewActivator.TENANT_NOT_EXISTS_ERROR_2){
                            MobileUI.core.util.Util.redirect(MobileUI.core.util.Util.getTenantNotExistsURL());
                        } else {
                            MobileUI.core.util.Util.redirect(MobileUI.core.util.Util.getLogoutURL());
                        }
                    } else {
                        MobileUI.components.MessageBox.alert('Login failed', resp.errorMessage);
                    }
                }

            }, this);
        } else if(arrTokens) {
            removeCookie('tokens');
            arrTokens = arrTokens.split('|');
            arrTokens.forEach(function (token) {
                var arr = token.replace(/"/g,'').split('=');
                if (arr[0] && arr[1]) {
                    MobileUI.core.util.Storage.setItem(arr[0], arr[1], true);
                }
            });
            MobileUI.core.util.Storage.setItem('expired', new Date().getTime() + 120000);
            if (this.getPreviousState()) {
                MobileUI.core.Navigation.redirectTo(this.getPreviousState());
            }else{
                this.beforeLogin(action);
            }
        }
        else
        {
            var settings = MobileUI.core.Services.getGeneralSettings();
            if (MobileUI.core.util.Util.supportSSO()) {
                var uri = window.location.origin + window.location.pathname.replace('//','/') +
                    (window.location.search || '') + '#'+(this.locationHash|| '');
                settings.sso = settings.sso.replace('{0}', btoa(uri));
                MobileUI.core.Navigation.redirectToUrl(settings.sso);
            } else {
                action.resume();
            }
        }
    }
});
