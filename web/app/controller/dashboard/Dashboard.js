/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dashboard.Dashboard', {
    extend: 'Ext.app.Controller',

    requires: [
        'MobileUI.core.Navigation',
        'MobileUI.components.dashboard.MySavedSearches',
        'MobileUI.components.dashboard.Chart',
        'MobileUI.components.dashboard.RecentEntities',
        'MobileUI.components.dashboard.CustomViewBoard'
    ],
    mixins: {
        tag: 'MobileUI.controller.MBaseSearch'
    },

    config: {
        refs: {
            searchField: 'textfield[name=search]',
            rootPanel: 'panel[name=dashboardview-root]',
            backToolbarBtn: 'button[name=backToolbarBtn]'
        },
        views: [
            'MobileUI.view.dashboard.Dashboard'
        ]
    },

    singleItemId: null,


    showDashboard: function (singleItemId)
    {
        var viewActivator = this.getApplication().getController('ViewActivator');

        if (viewActivator.waitForActivate())
        {
            viewActivator.on('activated', function ()
            {
                if (this.getViews().indexOf(Ext.getClass(Ext.Viewport.getActiveItem()).getName()) !== -1)
                {
                    if (MobileUI.core.Navigation.isBackAllow()) {
                        var back = this.getBackToolbarBtn();
                        if (back) {
                            back.setHidden(false);
                        }
                    }
                    this.showDashboardInternal(singleItemId);
                }
            }, this, {single: true});
        }
        else
        {
            this.showDashboardInternal(singleItemId);
        }
    },

    /**
     * @private
     */
    showDashboardInternal: function(singleItemId) {
        this.singleItemId = singleItemId;

        if (singleItemId === 'init') {
            singleItemId = null;
        }
        var config = this.getDashboardConfig();
        if (singleItemId) {
            config = config.
                filter(function(item) {
                    return item.id === singleItemId;
                });
        }

        config = config.map(function(item) {
           return {
               xtype: item.type,
               params: item.params || {},
               single: !!singleItemId,
               itemId: item.id
           };
        });

        var panel = this.getRootPanel();
        if (singleItemId) {
            panel.setScrollable(false);
            panel.addCls('single-item');
        }
        panel.add(config);
    },

    onPullRefresh: function() {
        this.getRootPanel().removeAll();
        this.showDashboard(this.singleItemId);
    },

    getDashboardConfig: function() {
        var cm = MobileUI.core.Services.getConfigurationManager();
        var plugin = cm.getExtensionById('com.reltio.mobile.dashboard.General');
        return ((plugin && plugin.items) || []).map(cm.getExtensionById, cm);
    }
});
