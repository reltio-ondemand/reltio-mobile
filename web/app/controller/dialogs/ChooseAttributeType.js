/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.ChooseAttributeType', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    mixins: {
        listEditor: 'MobileUI.controller.editors.MListEditor'
    },
    config: {
        refs: {
            title: 'component[name=chooseattrtype-title]',
            cancelBtn: 'button[name=chooseattrtype-cancelBtn]',
            doneBtn: 'button[name=chooseattrtype-doneBtn]',
            searchField: 'textfield[name=chooseattrtype-search]',
            list: 'elist[name=chooseattrtype-list]',
            panel: 'chooseattrtype',
            backButton: 'button[name=chooseattrtype-backButton]',
            notificationToolbar: 'toolbar[name=notificationToolbarChooseAttr]',
            notificationMessage: 'component[name=notificationMessageChooseAttr]'
        },
        control: {
            list: {
                selectItem: 'onSearchItemTap',
                painted: 'onListPainted'
            },
            searchField: {
                keyup: 'onKeyUp',
                clearicontap: 'onClearIcon'
            },
            panel: {
                initialize: 'onPanelInitialize'
            }
        },
        views: ['MobileUI.view.dialogs.ChooseAttributeType']
    },

    /** @private */
    data: [],
    getSelectImmediate: true,

    getData: function(callback, context) {
        callback.call(context, this.data);
    },

    getRecordValue: function(record) {
        return record.get('userData').value;
    },

    getDisplayName: function(item) {
        if (this.parseName){
            return this.parseName(item);
        }
        return item.label;
    },

    isRequired: function(item) {
        return item.required;
    },

    resetParseDisplayName: function(){
        this.parseName = null;
    },

    setParseDisplayName: function(func){
        if(Ext.isFunction(func)){
            this.parseName = func;
        }
    },

    isItemSelected: function(item, selected) {
        return item === selected;
    },

    onPanelInitialize: function() {
        this.fillList();
    },
    /**
     * @param values {Array<String>}
     */
    setValues: function(values) {
        this.getSearchField().setValue('');
        this.data = values;
        this.cache = null;
        this.fillList();
    },

    setNotificationText: function(text) {
        this.getNotificationMessage().setCls('notification-message');
        if (text) {
            this.getNotificationMessage().setHtml(text);
            this.getNotificationToolbar().setHidden(false);
        }
        else {
            this.getNotificationMessage().setHtml('');
            this.getNotificationToolbar().setHidden(true);
        }
    }
});
