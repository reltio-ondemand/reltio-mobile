/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.ChooseEntity', {
    extend: 'Ext.app.Controller',
    /**
     * Events:
     * @event refEntitySelected
     * @event cancelled
     */
    mixins: {
        typeAheadMixin: 'MobileUI.controller.MTypeAhead',
        backButton: 'MobileUI.controller.dialogs.MBackButton'
    },
    config: {
        refs: {
            title: 'component[name=chooseentitydialog-title]',
            cancelBtn: 'button[name=chooseentitydialog-cancelBtn]',
            searchField: 'textfield[name=chooseentitydialog-search]',
            list: 'elist[name=chooseentitydialog-list]',
            addNewEntityBtn: 'button[name=chooseentitydialog-addNewEntityBtn]',
            fakeButton: 'component[name=chooseentitydialog-placeholder]',
            backButton: 'component[name=chooseentitydialog-backToolbarBtn]',
            panel: 'panel[name=choose-entity]'
        },
        control: {
            list: {
                fetch: 'onListFetch',
                selectItem: 'onSearchItemTap'
            },
            addNewEntityBtn: {
                tap: 'onAddNewEntityBtn'
            },
            cancelBtn: {
                tap: 'onBackButtonTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            panel: {
                initialize: 'onPanelPainted'
            },
            searchField: {
                keyup: 'onKeyUp',
                clearicontap: 'onClearIcon',
                initialize: 'onSearchFieldPainted'
            }
        },
        views: ['MobileUI.view.dialogs.ChooseEntity']
    },

    /** @private */
    entityType: '',
    /** @private */
    entityTypes: null,
    /** @private */
    offset: 0,
    /** @private */
    limit: 50,
    /** @private */
    referenceAttribute: null,
    /** @private */
    attributeUri: null,
    /** @private */
    entityUri: null,
    /** @private */
    typeaheadValue: null,
    /** @private */
    typeaheadBehaviour: function ()
    {
        return false;
    },

    statics: {
        showCreateEntity: function (app, entityType, name, configuration)
        {
            app.getController('dialogs.CreateRelation').showDialog(null, entityType, name, null, configuration);
        }
    },
    /**
     * on panel painted listener
     */
    onPanelPainted: function()
    {
        this.typeaheadValue = MobileUI.core.search.SearchParametersManager.getTypeAhead();
    },
    /**
     * On search item tap listener
     */
    onSearchItemTap: function (item)
    {
        var uri = item.getData()['userData'].uri;
        this.restoreTypeAheadState();
        if (this.referenceAttribute !== null && uri)
        {
            this.getList().setMasked(true);
            MobileUI.core.session.Session.requestEntity(uri, function (response)
            {
                this.getApplication().getController('dialogs.ReferenceAttributeEditor').chooseReferenceAttribute(this.entityUri, this.attributeUri, this.parentUri, this.referenceAttribute, response);
                this.getList().setMasked(null);
                MobileUI.core.entity.TemporaryEntity.addEntity(response);
                this.fireEvent('refEntitySelected', this.referenceAttribute, response);
            }, null, this, true);
        }
        else
        {
            this.getApplication().getController('dialogs.CreateRelation').showDialog(uri, null, null, null, this.configuration);
        }
    },

    clearAll: function ()
    {
        this.clearList();
        this.referenceAttribute = null;
        this.attributeUri = null;
        this.getSearchField().reset();
        this.value = null;
    },

    onAddNewEntityBtn: function ()
    {
        this.restoreTypeAheadState();
        if (this.referenceAttribute !== null)
        {
            if (!MobileUI.core.SuiteManager.hasDialog('referenceDialog'))
            {
                MobileUI.core.SuiteManager.addDialog('referenceDialog', Ext.create('MobileUI.view.dialogs.ReferenceAttributeEditor'));
            }
            MobileUI.core.SuiteManager.showDialog('referenceDialog');

            this.getApplication().getController('dialogs.ReferenceAttributeEditor').createReferenceAttribute(this.entityUri, this.attributeUri, this.parentUri, this.referenceAttribute, this.getSearchField().getValue());

        }
        else
        {
            MobileUI.controller.dialogs.ChooseEntity.showCreateEntity(
                this.getApplication(), this.entityType, this.getSearchField().getValue(),
                this.configuration);
        }
    },

    hideAddNewEntity: function ()
    {
        this.getAddNewEntityBtn().removeCls('expanded');
    },

    showAddNewEntity: function () {
        if (MobileUI.core.PermissionManager.securityService().metadata.canCreate(this.entityType)) {
            var button = this.getAddNewEntityBtn();
            button.addCls('expanded');
            button.setText(i18n('Create "%1" as new %2', this.getSearchField().getValue(), this.entityType.label));
        }
    },

    setEntityTypeFilter: function (entityType, entityTypes, dontSaveState, config)
    {
        this.entityType = entityType;
        this.entityTypes = entityTypes;
        this.configuration = config;
        if (!dontSaveState && !this.isOnBackAction())
        {
            MobileUI.core.SuiteManager.putState('chooseEntity', {
                dialog: 'chooseEntity',
                controller: 'dialogs.ChooseEntity',
                method: 'setEntityTypeFilter',
                parameters: [this.entityType, this.entityTypes, false, config]
            });
        }
        if (Ext.isEmpty(this.entityTypes) && this.entityTypes !== undefined)
        {
            this.entityTypes = Ext.Object.getValues(MobileUI.core.Metadata.getEntityDescendants(entityType.uri));
            this.entityTypes.push(entityType);
        }

        var title = this.getTitle();
        if (title)
        {
            var caption = this.configuration ? this.configuration.caption : null;
            title.setHtml(Ext.util.Format.htmlEncode(i18n('Add %1', caption || this.entityType.label)));
        }
        this.clearAll();
    },
    setReferenceAttribute: function (referenceAttribute, entityUri,parentUri, attributeUri)
    {
        this.referenceAttribute = Ext.clone(referenceAttribute);
        this.attributeUri = attributeUri;
        this.entityUri = entityUri;
        this.parentUri = parentUri;
    },
    /**
     * @protected
     * Typeahead listener
     */
    onTypeAheadSearch: function ()
    {
        this.showAddNewEntity();

        this.offset = -this.limit;
        this.getList().startFetch();
    },

    onListFetch: function ()
    {
        this.offset += this.limit;
        this.doSearch();
    },

    /**
     * @protected
     * Typeahead listener
     */
    doSearch: function ()
    {
        var search = {value: this.value},
            type;
        if(Ext.isArray(this.entityTypes))
        {
            type = this.entityTypes.map(function(item) {return item.uri.substring(item.uri.lastIndexOf('/') + 1);});
        }else
        {
            type = this.entityType.uri.substring(this.entityType.uri.lastIndexOf('/') + 1);
        }
        MobileUI.core.session.Session.chooseEntitySearch(search,
            type, this.offset, this.limit,
            this.dataReceived, function (e)
            {
                MobileUI.core.Logger.log(e);
            }, this);
    },
    onBackButtonTap: function ()
    {
        this.restoreTypeAheadState();
        if (this.referenceAttribute === null)
        {
            MobileUI.core.SuiteManager.popState();
        }

        if (MobileUI.core.SuiteManager.getCurrentState() && MobileUI.core.SuiteManager.getCurrentState().state)
        {
            this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
        } else {
            MobileUI.core.SuiteManager.hideDialogs(true, function ()
            {
                this.fireEvent('cancelled');
            }, this);
        }
    },
    /**
     * @protected
     * Search field listener
     */
    onSearchFieldPainted: function ()
    {
        this.getTitle().setHtml(Ext.util.Format.htmlEncode(this.entityType.label));
    },
    /**
     * @private
     */
    restoreTypeAheadState: function()
    {
        MobileUI.core.search.SearchParametersManager.setTypeAhead(this.typeaheadValue);
    },

    /**
     * @protected
     */
    onCancelTap: function(){
        this.restoreTypeAheadState();


        MobileUI.core.SuiteManager.hideDialogs(false, function ()
        {
            //MobileUI.core.entity.TemporaryEntity.removeAllEntities();
            this.fireEvent('cancelled');
            MobileUI.core.relations.RelationsManager.setState({});
        }, this);
    }
});
