/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.CreateEntity', {
    extend: 'Ext.app.Controller',
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        complexAttribute: 'MobileUI.controller.dialogs.MComplexAttributeProcessing',
        backButton: 'MobileUI.controller.dialogs.MBackButton'
    },
    requires: ['MobileUI.components.list.MListHeight',
        'MobileUI.core.entity.TemporaryEntity',
        'MobileUI.components.list.MListModel',
        'MobileUI.core.entity.ReferencedAttributeUtils',
        'MobileUI.controller.dialogs.MComplexAttributeProcessing'],
    config: {
        refs: {
            list: 'elist[name=create-entity-list]',
            title: 'component[name=title-create-entity]',
            save: 'button[name=save-new-entity-button]',
            cancelButton: 'button[name=close-create-entity]',
            fakeButton: 'component[name=createentitydialog-placeholder]',
            backButton: 'component[name=createentitydialog-backButton]'
        },
        control: {
            cancelButton: {
                tap: 'cancelTap'
            },
            save: {
                tap: 'onSaveTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            list: {
                edit: 'onComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete'
            }
        },
        views: [
            'MobileUI.view.dialogs.CreateEntity'
        ]
    },
    entityUri: null,
    entityType: null,
    clearAll: function ()
    {
        var list = this.getList();
        if (list && list.getStore())
        {
            list.getStore().clearData();
            list.refresh();
        }
    },
    beforeNewEditorOpen: function ()
    {
        this.saveAttributesInternal();
    },
    saveAttributesInternal: function ()
    {
        var list = this.getList(),
            store = list.getStore(),
            entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri);
        this.saveValues(entity, store);
    },
    createEntity: function (entityType, entityUri, label, editable)
    {
        var model, list, store, entity, i, tmpAttributes,
            attributes = MobileUI.core.entity.EntityUtils.collectLabelAttributes(entityType);
        if (!MobileUI.core.SuiteManager.hasDialog('createEntity'))
        {
            MobileUI.core.SuiteManager.addDialog('createEntity', Ext.create('MobileUI.view.dialogs.CreateEntity'));
        }
        MobileUI.core.SuiteManager.showDialog('createEntity');
        this.clearAll();
        if (MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) &&
            MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(entityType))
        {
            this.getTitle().setHtml(Ext.util.Format.htmlEncode(i18n('Create %1', entityType.label)));
            entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri);
            if (Ext.isEmpty(entity))
            {
                entity = MobileUI.core.entity.TemporaryEntity.createNewEntity(entityType);
                for (i = 0; i < attributes.length; i++)
                {
                    if (MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attributes[i]))
                    {
                        entity.attributes.push({
                            attrType: attributes[i],
                            value: [],
                            parentUri: entity.uri,
                            uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(entity.uri + '/attributes', attributes[i])
                        });
                    }
                }
            }
            this.entityUri = entity.uri;
            this.entityType = entityType;
            this.checkBackButtonVisibility(this.getBackButton(), this.getFakeButton());
            if (!this.isOnBackAction())
            {
                MobileUI.core.SuiteManager.putState('createEntity', {
                    dialog: 'createEntity',
                    controller: 'dialogs.CreateEntity',
                    method: 'createEntity',
                    parameters: [entityType, this.entityUri, null]
                });
            }
            tmpAttributes = entity.attributes.filter(function (item)
            {
                return item.parentUri === this.entityUri;
            }, this);
            model = this.createListModel(tmpAttributes, label, editable);
            list = this.getList();
            store = list.getStore();
            store.clearData();
            store.add(model);
            this.updateListHeight(list);
        } else {
            this.checkBackButtonVisibility(this.getBackButton(), this.getFakeButton());
            if (!this.isOnBackAction())
            {
                MobileUI.core.SuiteManager.putState('createEntity', {
                    dialog: 'createEntity',
                    controller: 'dialogs.CreateEntity',
                    method: 'createEntity',
                    parameters: [entityType, this.entityUri, null]
                });
            }
            this.getSave().setHidden(true);
            model = [{property: null, type: MobileUI.components.list.items.BaseListItem.types.property, label:'Can\'t create entity', secondaryLabel: ''}];
            list = this.getList();
            store = list.getStore();
            store.clearData();
            store.add(model);
            this.getTitle().setHtml(Ext.util.Format.htmlEncode(i18n('Can\'t create %1', entityType.label)));
        }
    },
    onBackButtonTap: function(){
        MobileUI.core.SuiteManager.popState();
        this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
    },
    onSaveTap: function ()
    {
        var list = this.getList(),
            store = list.getStore(),
            entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri),
            labelPattern, secondaryLabelPattern;
        this.saveValues(entity, store);
        labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern(entity.type);
        secondaryLabelPattern = MobileUI.core.entity.EntityUtils.findSecondaryLabelPattern(entity.type);
        entity.label = MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(entity.attributes, labelPattern);
        entity.secondaryLabel = MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(entity.attributes, secondaryLabelPattern);
        MobileUI.core.SuiteManager.popState();
        MobileUI.core.SuiteManager.showDialog('dialogs.CreateRelation');
        this.getApplication().getController('dialogs.CreateRelation').showDialog(this.entityUri);
    },
    processReferenceEntitySelected: function ()
    {
        MobileUI.core.SuiteManager.showDialog('createEntity');
        MobileUI.core.SuiteManager.popState();
        this.createEntity(this.entityType, this.entityUri, null);
    }
});
