/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.CreateRelation', {
    extend: 'Ext.app.Controller',
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        editableProfile: 'MobileUI.controller.MEditableProfile',
        backButton: 'MobileUI.controller.dialogs.MBackButton',
        action: 'MobileUI.controller.MEntityAction',
        addAttribute: 'MobileUI.controller.MAddAttribute'
    },
    requires: [
        'MobileUI.components.list.MListHeight',
        'MobileUI.core.entity.TemporaryEntity',
        'MobileUI.controller.MEditableProfile',
        'MobileUI.components.list.MListModel',
        'MobileUI.controller.MEntityAction',
        'MobileUI.components.MessageBox'
    ],
    config: {
        refs: {
            list: 'elist[name=create-relation-list]',
            entityList: 'elist[name=create-relation-list-entity]',
            title: 'component[name=title-create-relation]',
            saveButton: 'button[name=create-relation-button]',
            cancelButton: 'button[name=close-create-relation]',
            fakeButton: 'component[name=createrelationdialog-placeholder]',
            backButton: 'component[name=createrelationdialog-backButton]',
            notificationToolbar: 'component[name=notificationToolbar]',
            notificationMessage: 'component[name=notificationMessage]',
            listsSeparator: 'component[name=create-relation-lists-separator]'
        },
        control: {
            cancelButton: {
                tap: 'cancelTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            saveButton: {
                tap: 'onSaveTap'
            },
            list: {
                edit: 'onComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete'
            },
            entityList: {
                edit: 'onEntityComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onEntityItemDelete'
            }
        },
        views: [
            'MobileUI.view.dialogs.CreateRelation'
        ]
    },

    /**@private*/
    isForRelationState: true,
    entityUri: null,
    createEntityOnTheFly: false,
    /**
     * Method clears the list
     */
    clearAll: function ()
    {
        var me = this;
        function clear(list){
            if (list && list.getStore())
            {
                list.getStore().clearData();
                list.refresh();
                me.checkSeparatorVisibility(me.updateListHeight(list) !== 0);
            }
        }
        clear(this.getList());
        clear(this.getEntityList());
    },

    checkSeparatorVisibility: function (state) {
        if (this.getListsSeparator()){
            this.getListsSeparator().setHidden(!state);
        }
    },

    fillListByEntity: function(entity) {
        if (Ext.isObject(entity.attributes)) {
            this.referenceEntityItem = {
                attrType: MobileUI.core.Metadata.getEntityType(entity.type),
                userData: entity,
                label: entity.label,
                secondaryLabel: entity.secondaryLabel,
                type: MobileUI.components.list.items.BaseListItem.types.entityInfo,
                disableSwipe: true
            };
        } else {
            this.referenceEntityItem = null;
            this.referenceEntity = entity;
            var entityType = MobileUI.core.Metadata.getEntityType(entity.type);
            if (entityType) {
                this.entityAttributeStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(entityType, null, function (attrType) {
                    return MobileUI.core.PermissionManager.securityService().metadata.canCreate(attrType);
                }, true);
            }
            var attributes = this.referenceEntity.attributes.filter(function (item) {
                return item.parentUri === this.referenceEntity.uri && MobileUI.core.entity.EntityUtils.isReference(item.attrType);
            }, this).map(function (item) {
                return item.type;
            }).sort().filter(function (item, i, array) {
                if (i >= 0 && item === array[i - 1]) {
                    return item;
                }
            });
            if (!this.isOnBackAction() && entity.label)
            {
                var labelAttributes = this.referenceEntity.attributes;
                for (var i = 0; i< labelAttributes.length; i++)
                {
                    var attr = labelAttributes[i];
                    if (attr && attr.attrType && attr.attrType.type === 'String'){
                        attr.value = entity.label;
                        this.referenceEntity.addedAttributes.push(attr);
                        break;
                    }
                }
            }
            this.referenceEntity.attributes = this.referenceEntity.attributes.filter(function(item){
                return attributes.indexOf(item.type) === -1 || !Ext.isEmpty(item.refEntity);
            });
            this.updateList(this.getEntityList(), this.referenceEntity, this.entityAttributeStructure);
        }

    },
    updateList: function (list, entity, structure) {
        var attributes = entity.attributes.filter(function (item) {
            return item.parentUri === entity.uri;
        });
        var store,
            model = this.createModel(attributes, structure, entity, true).map(function(item){
                if (item.type === 'entityInfo'){
                    item.type = 'property';
                }
                return item;
            });

        if (this.referenceEntityItem) {
            model.unshift(this.referenceEntityItem);
        }

        model = model.filter(MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInCreateMode, this);

        store = list.getStore();
        store.clearData();
        store.add(model);
        this.checkSeparatorVisibility(this.updateListHeight(list) !== 0);
    },
    /**
     * Method fills all the attribute
     * @param entity  {Object}
     */
    fillList: function (entity)
    {
       this.fillListByEntity(entity);
        this.relationEntity =  MobileUI.core.entity.TemporaryEntity.getEntity(this.relationUri);
        this.relationStructure = [{label: 'Other', values: this.relationEntity.attributes.map(function(item){
            return item.attrType;
        })}];
        this.updateList(this.getList(), this.relationEntity, this.relationStructure);
    },
    showDialog: function (entityUri, entityType, name, tmpEntityUri, configuration)
    {
        this.clearAll();
        if (!MobileUI.core.SuiteManager.hasDialog('createRelation'))
        {
            MobileUI.core.SuiteManager.addDialog('createRelation', Ext.create('MobileUI.view.dialogs.CreateRelation'));
        }
        MobileUI.core.SuiteManager.showDialog('createRelation');
        this.configuration = configuration;
        var caption = configuration.caption || null;
        this.getTitle().setHtml(caption ? Ext.util.Format.htmlEncode(i18n('Add %1', caption)) : i18n('Create relation'));
        var state = MobileUI.core.relations.RelationsManager.getState();
        if (!this.isOnBackAction())
        {
            var baseEntity = MobileUI.core.session.Session.getEntity();
            MobileUI.core.session.Session.getLookupsManager().initWithEntityType(baseEntity.type);
            MobileUI.core.session.Session.getLookupsManager().fillValuesFromEntity(baseEntity);

            state.attributes = state.attributes.map(function(item){
                if (MobileUI.core.PermissionManager.securityService().metadata.canCreate(item.attrType)) {
                    item.value = MobileUI.core.entity.EntityUtils.isComplex(item.attrType) ? {} : '';
                    item.type = item.attrType.uri;
                    item.ov = true;
                    item.name = item.attrType.name;
                    return item;
                }else {
                    return false;
                }
            }).filter(function(item){
                return item;
            });
            state.type = state.relationTypeChanged;
            state.uri = 'relation';
            var relationEntity = MobileUI.core.entity.TemporaryEntity.createNewEntity(state.type, state, null, false, state.uri);
            relationEntity.attributes = state.attributes;
            this.relationUri = relationEntity.uri;
        }

        if (entityUri && !this.isOnBackAction()) {
            MobileUI.core.relations.RelationsManager.getState().entityUri = entityUri;
            this.entityUri = entityUri;
            MobileUI.core.session.Session.requestEntity(entityUri, function(resp){
                MobileUI.core.entity.TemporaryEntity.addEntity(resp);
                this.createEntityOnTheFly = false;
                this.fillList(resp);
            }, function (resp) {
                this.createEntityOnTheFly = false;
                MobileUI.core.entity.TemporaryEntity.createNewEntity(entityType, resp, null, false, entityUri);
                MobileUI.core.Logger.error(resp);
            }, this, true);
        }
        else
        {
            var entity = tmpEntityUri ?
                    MobileUI.core.entity.TemporaryEntity.getEntity(tmpEntityUri) :
                    MobileUI.core.entity.TemporaryEntity.createNewEntity(entityType);

            if (!tmpEntityUri) {
                    var attributes = MobileUI.core.entity.EntityUtils.collectLabelAttributes(entityType);
                    entity.attributes = attributes.map(function (attributeObject) {
                    if (MobileUI.core.PermissionManager.securityService().metadata.canCreate(attributeObject)) {
                        return {
                            value: MobileUI.core.entity.EntityUtils.isComplex(attributeObject) ? {} : '',
                            type: attributeObject.uri,
                            attrType: attributeObject,
                            ov: true,
                            name: attributeObject.name,
                            uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(entity.uri, attributeObject),
                            parentUri: entity.uri
                        };
                    } else {
                        return false;
                    }
                }).filter(function (item) {
                    return item;
                });
            }
            state.entityUri = entity.uri;
            entity.label = this.isOnBackAction() ? entity.label : name;
            if (!this.isOnBackAction()) {
                this.createEntityOnTheFly = true;
            }
            this.entityUri = entity.uri;
            this.fillList(entity);
        }
        tmpEntityUri = this.entityUri;
        if (!this.isOnBackAction()){
            MobileUI.core.SuiteManager.putState('createRelation', {
                dialog: 'createRelation',
                controller: 'dialogs.CreateRelation',
                method: 'showDialog',
                parameters: [entityUri, entityType, name, tmpEntityUri, configuration]
            });
        }
    },

    isForRelation: function(){
        return this.isForRelationState;
    },

    saveInternal: function (callback, self)
    {
        var state = MobileUI.core.relations.RelationsManager.getState();
        var relEntity = MobileUI.core.entity.TemporaryEntity.getEntity('relation');
        state.attributes = relEntity ? relEntity.attributes : state.attributes;
        var inRelation = !!state.isInRelation;
        MobileUI.core.relations.RelationsManager.saveRelation(state, MobileUI.core.relations.RelationsManager.getBaseEntityUri(), MobileUI.core.Metadata.getRelationType(state.relationTypeChanged), inRelation ? 'in' : 'out', function (resp) {
            var uri, value, comment;
            callback = callback || Ext.emptyFn;
            uri = Ext.isArray(resp) ? resp[0].object.uri : null;
            if (this.configuration) {
                value = this.configuration.autoVote || MobileUI.controller.Relations.POSITIVE_VOTE;
                value = parseInt(value, 10);
                if (this.configuration.type === MobileUI.controller.Relations.TYPE_RATING && value > 0 && !this.configuration.disableAutoVote) {
                    comment = value === MobileUI.controller.Relations.NEGATIVE_VOTE ? this.configuration.autoComment : '';
                    comment = comment || '';
                    MobileUI.core.session.Session.addRelationRating(uri, value, comment, callback, null, this);
                    return;
                }
            }
            callback.apply(self);
        }, this);

    },
    shouldValidateEntity: function(){
        return this.referenceEntity;
    },
    onSaveTap: function () {
        this.getSaveButton().setDisabled(true);
        var validations = [this.validateLookups([this.getList(), this.getEntityList()])];
        if (this.shouldValidateEntity()){
            var attributes = MobileUI.core.entity.EntityUtils.collectLabelAttributes(this.referenceEntity.type);
            var requiredValidations = this.allRequiredAttributesFilled(this.getEntityList(), attributes, this.referenceEntity);
            validations.push(requiredValidations);
        }
        Promise.all(validations)
            .then(function (results) {
                var result = {};
                results.forEach(function (item) {
                    result = MobileUI.core.util.Util.mergeWithArray(item, result);
                });
                var validationPassed = MobileUI.core.entity.EntityUtils.validationPassed(result);
                if (validationPassed) {
                    this.onAfterAllValidations();
                } else {
                    this.getSaveButton().setDisabled(false);
                    this.highlightValidationErrors(this.getList());
                    this.highlightValidationErrors(this.getEntityList());
                    this.showValidationErrorMessage(result);
                    this.updateListHeight(this.getEntityList());
                    this.updateListHeight(this.getList());
                }
            }.bind(this));
    },

    onAfterAllValidations: function(){
        if (MobileUI.core.relations.RelationsManager.fireBeforeSaveRelationEvent(this.entityUri) === false){
            MobileUI.components.MessageBox.alert(i18n('Error'),  i18n('Selected entity already exist.'));
            return;
        }
        if (this.createEntityOnTheFly) {
            var entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri);
            var newEntity = MobileUI.core.entity.EntityUtils.createEntityFromTemporary(entity);
            var referencedAttributes = entity.addedAttributes.filter(function(item){
                return MobileUI.core.entity.EntityUtils.isReference(item.attrType);
            });
            MobileUI.core.session.Session.createEntity(newEntity, null,
                function (resp) {
                    var uri, onSuccess = function () {
                        MobileUI.core.SuiteManager.hideDialogs(true);
                        MobileUI.core.relations.RelationsManager.fireSaveRelationEvent(uri);
                    };
                    if (Ext.isArray(resp)) {
                        uri = Ext.isObject(resp[0].object) ? resp[0].object.uri : '';
                    }
                    if (!Ext.isEmpty(uri)) {
                        MobileUI.core.relations.RelationsManager.getState().entityUri = uri;
                        if (referencedAttributes.length > 0) {

                            var attrs = referencedAttributes.map(function (item) {
                                return item.uri;
                            });
                            var observer = Ext.create('MobileUI.core.Observer', {
                                    threadNames: attrs,
                                    listener: function () {
                                        this.saveInternal(onSuccess);
                                    },
                                    self: this
                                }),
                                success = function () {
                                    observer.checkThread(this.uri);
                                },
                                failure = function (resp) {
                                    MobileUI.core.Logger.error(resp);
                                    observer.checkThread(this.uri);
                                };
                            referencedAttributes.forEach(function (item) {
                                var cw = Ext.isEmpty(item.refEntity.crosswalks) ? {type: 'configuration/sources/Reltio'} : item.refEntity.crosswalks[0];
                                MobileUI.core.session.Session.insertReferencedAttribute(
                                    item.attrType, uri,
                                    item.refEntity.objectURI, cw,
                                    item.refRelation.objectURI,
                                    success, failure, {uri: item.uri});
                            }, this);
                        } else {
                            this.saveInternal(onSuccess);
                        }

                    }
                    else {
                        MobileUI.core.Logger.error('Can\'t save temporary entity');
                    }
                }, null, this);
        }
        else {
            this.saveInternal(function () {
                MobileUI.core.SuiteManager.hideDialogs(true);
                MobileUI.core.relations.RelationsManager.fireSaveRelationEvent(this.entityUri);
            }, this);
        }
    },
    getEditedEntity: function(){
        return this.entity;
    },

    onBackButtonTap: function(){
        MobileUI.core.SuiteManager.popState();
        this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
    },

    onEntityComplexAttributeSelect: function(record) {
        var context = this;
        context.originalUri = this.referenceEntity.uri;
        context.parentUri = this.referenceEntity.uri;
        context.entity = this.referenceEntity;
        var data = record.getData();
        if (data.editor && data.editor.type === 'Reference' && Ext.isEmpty(data.label))
        {
            var list = this.getEntityList();
            var store = list.getStore();
            var index = store.findExact('id', data.id),
                msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();

            this.showAddAttributeDialog([data.editor], index, list, context.originalUri, context.parentUri, context.originalUri, msg);
        }else{
            this.onComplexAttributeTap.call(context, record);
        }
    },

    onComplexAttributeSelect: function (record) {
        var context = this;
        context.originalUri = this.relationEntity.uri;
        context.parentUri = this.relationEntity.uri;
        context.entity = this.relationEntity;
        this.onComplexAttributeTap.call(context, record);
    },

    onEntityItemDelete: function(record){
        var context = this;
        context.entity = this.referenceEntity;
        this.deleteAttribute.call(context, record,  'clear');
        this.updateList(this.getEntityList(), this.referenceEntity, this.entityAttributeStructure);
    },
    onItemDelete: function(record){
        var context = this;
        context.entity = this.relationEntity;
        this.deleteAttribute.call(context, record,  'clear');
        this.updateList(this.getList(), this.relationEntity, this.relationStructure);
    },

    processReferenceEntitySelected: function (attribute, value)
    {
        attribute.value = [value];
        this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
    }
});
