/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.EditSavedSearch', {
    extend: 'Ext.app.Controller',
    mixins: {
        savedSearches: 'MobileUI.controller.MSavedSearches',
        listModel: 'MobileUI.components.list.MListModel',
        listHeight: 'MobileUI.components.list.MListHeight'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'Ext.field.Toggle',
        'MobileUI.core.Navigation',
        'MobileUI.components.MessageBox'
    ],
    config: {
        refs: {
            propertiesList: 'elist[name=editsavedsearch-list]',
            cancelButton: 'button[name=editsavedsearch-cancelButton]',
            saveButton: 'button[name=editsavedsearch-saveButton]',
            backButton: 'button[name=editsavedsearch-backButton]'
        },
        control: {
            cancelButton: {
                tap: 'onCancel'
            },
            saveButton: {
                tap: 'onSave'
            },
            propertiesList: {
                execute: 'onDeleteButtonTap'
            },
            editsavedsearchdialog: {
                show: 'onShow'
            },
            backButton: {
                tap: 'onBack'
            }
        },
        views: [
            'MobileUI.view.dialogs.EditSavedSearch'
        ],

        savedSearchUri: null,
        enableAfterSave: false,
        suggestedName: ''
    },

    statics: {
        /**
         * @param searchUri {string} empty for new search
         * @param enableAfterSave {boolean} automatically enable search when saved
         */
        show: function(searchUri, enableAfterSave, suggestedName) {
            if (!MobileUI.core.SuiteManager.hasDialog('editsavedsearchdialog')) {
                MobileUI.core.SuiteManager.addDialog('editsavedsearchdialog', Ext.create('MobileUI.view.dialogs.EditSavedSearch'));
            }
            var controller = MobileUI.app.getController('dialogs.EditSavedSearch');
            controller.setSavedSearchUri(searchUri);
            controller.setEnableAfterSave(enableAfterSave);
            controller.setSuggestedName(suggestedName);
            MobileUI.core.SuiteManager.showDialog('editsavedsearchdialog');
        }
    },

    /** @private */
    savedSearch: null,

    /** @private */
    setAsGlobalFilter: null,

    /**
     * @private
     */
    onCancel: function() {
        MobileUI.core.SuiteManager.back();
    },

    /**
     * @private
     */
    onSave: function() {
        this.save(function() {
            if (this.getEnableAfterSave()) {
                this.showSavedSearch(this.getSavedSearchUri());
            }
            else {
                this.onCancel();
            }
        }, this);
    },

    onShow: function() {
        this.refreshEditors();
    },

    /**
     * @private
     */
    onDeleteButtonTap: function() {
        this.deleteSavedSearch(this.savedSearch.uri, this.onCancel, this);
    },

    /**
     * @private
     * @property {String} savedSearchUri
     */
    refreshEditors: function() {
        var savedSearchUri = this.getSavedSearchUri();

        this.setAsGlobalFilter = null;
        this.getPropertiesList().setMasked(true);

        if (savedSearchUri) {
            MobileUI.core.session.Session.getSavedSearch(savedSearchUri, function(savedSearch) {
                    this.fillEditorList(savedSearch);
                },
                function(error) {
                    MobileUI.components.MessageBox.alert(i18n('Error response'), error);
                    MobileUI.core.Logger.log(error);
                }, this);
        }
        else {
            this.fillEditorList({
                query: MobileUI.core.session.Session.getSearchEngine().getQuery() || '',
                owner: MobileUI.core.session.Session.getUser() ? MobileUI.core.session.Session.getUser().full_name : '',
                create: true
            });
        }
    },

    /** @private */
    onNameTextFieldChanged: function(e) {
        this.savedSearch.name = e.getValue();
    },

    /** @private */
    onGlobalFilterToggleChanged: function(e) {
        this.setAsGlobalFilter = !!e.getValue();
    },

    /** @private */
    onPublicToggleChanged: function(e) {
        this.savedSearch.isPublic = !!e.getValue();
    },

    /** private */
    fillEditorList: function(savedSearch) {
        this.savedSearch = savedSearch;
        var globalFilterUris = MobileUI.core.search.SearchParametersManager.getGlobalFilter().uris,
            list = this.getPropertiesList(),
            store = list.getStore(),
            nameTextField = this.createNameTextfield(savedSearch.name || this.getSuggestedName(), this.onNameTextFieldChanged, this),
            globalFilterToggle = this.createToggle(i18n('Global filter'), globalFilterUris.indexOf(savedSearch.uri) !== -1,
                this.onGlobalFilterToggleChanged, this, false),
            items = [nameTextField, globalFilterToggle];

        items.push(this.createToggle(i18n('Public'), savedSearch.isPublic, this.onPublicToggleChanged, this, true));

        if (!savedSearch.create && !this.getSuggestedName()) {
            items.push({ type: MobileUI.components.list.items.BaseListItem.types.header });
            items.push(this.createDeleteButton());
            items.push({ type: MobileUI.components.list.items.BaseListItem.types.header });
        }

        store.clearData();
        store.add(items);

        list.refresh();
        list.setMasked(false);
        this.updateListHeight(list);
    },

    onBack: function() {
        MobileUI.core.SuiteManager.back();
    }
});
