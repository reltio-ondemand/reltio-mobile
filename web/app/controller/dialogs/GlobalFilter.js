/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.GlobalFilter', {
    extend: 'Ext.app.Controller',
    /**
     * Events:
     * @event refEntitySelected
     */
    mixins: {
        backButton: 'MobileUI.controller.dialogs.MBackButton'
    },
    config: {
        refs: {
            closeBtn: 'button[name=globalfilterdialog-closeBtn]',
            panel: 'panel[name=global-filter-dialog]',
            toggle: 'togglefield[name=globalfilterdialog-toggle]',
            filters: 'panel[name=globalfilterdialog-filters]'
        },
        control: {
            closeBtn: {
                tap: 'onCloseTap'
            },
            panel: {
                initialize: 'onPanelPainted'
            },
            toggle: {
                change: 'onToggleValueChanged'
            }
        },
        views: ['MobileUI.view.dialogs.GlobalFilter']
    },

    onPanelPainted: function() {
        var toggle = this.getToggle();
        var filters = this.getFilters();
        var globalFilter = MobileUI.core.search.SearchParametersManager.getGlobalFilter();
        toggle.setValue(!globalFilter.mute);
        var items = globalFilter.uris.
            map(function(uri) {
                var savedSearch = MobileUI.core.session.Session.getCachedSavedSearch(uri);
                var ssUIData = MobileUI.controller.MSavedSearches.extractSavedSearchUIData(savedSearch);
                return ssUIData ? MobileUI.core.search.SearchParametersManager.buildSearchParameters(
                    ssUIData.keyword, ssUIData.params, ssUIData.facets) : null;
            }).
            filter(function(data) { return !!data; }).
            map(function(data) {
                return {
                    xtype: 'component',
                    cls: 'global-filter-item searchfilter',
                    html: MobileUI.components.SearchFilter.generateHtml(data)
                };
            });

        var separated = [],
            separator = {
                xtype: 'component',
                cls: 'rl-item-separator'
            };
        items.forEach(function(item) {
            separated.push(item);
            separated.push(separator);
        });

        if (separated.length > 0) {
            separated[separated.length-1] = {
                xtype: 'component',
                cls: 'rl-item-separator last'
            };
        }

        filters.removeAll();
        filters.add(separated);
    },

    onCloseTap: function() {
        MobileUI.core.SuiteManager.hideDialogs(true);
    },

    onToggleValueChanged: function() {
        var toggle = this.getToggle();
        var gf = MobileUI.core.search.SearchParametersManager.getGlobalFilter();
        var value = !toggle.getValue();
        if (gf.mute !== value) {
            MobileUI.core.search.SearchParametersManager.setGlobalFilterMute(value);
        }
    }
});
