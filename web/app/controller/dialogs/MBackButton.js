/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.MBackButton', {
    isOnBack: false,
    checkBackButtonVisibility: function (backButton, fakeButton)
    {
        var allowBack = MobileUI.core.SuiteManager.isAllowBackButton();
        backButton.setHidden(!allowBack);
        if (fakeButton)
        {
            fakeButton.setHidden(allowBack);
        }
    },

    cancelTap: function ()
    {
        if (this.onBeforeCancel)
        {
            this.onBeforeCancel();
        }
        MobileUI.core.SuiteManager.hideDialogs(false, function ()
        {
            //MobileUI.core.entity.TemporaryEntity.removeAllEntities();
            MobileUI.core.relations.RelationsManager.setState({});
        }, this);
    },
    processState: function (state)
    {
        var that = this.getApplication().getController(state.controller);
        if (state.controller)
        {
            that.isOnBack = true;
            MobileUI.core.SuiteManager.showDialog(state.dialog, true);
            that[state.method].apply(that, state.parameters);
            that.isOnBack = false;
        }
    },
    isOnBackAction: function ()
    {
        return this.isOnBack;
    }
});
