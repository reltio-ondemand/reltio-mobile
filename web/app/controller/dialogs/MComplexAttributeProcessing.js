/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.MComplexAttributeProcessing', {

    onComplexAttributeSelect: function (record)
    {
        var info = record.get('info'), entityType, attrType;
        attrType = info.attrType;
        if (attrType.type === 'Reference')
        {
            if (this.beforeNewEditorOpen)
            {
                this.beforeNewEditorOpen();
            }
            entityType = MobileUI.core.Metadata.getEntityType(attrType.referencedEntityTypeURI);
            if (!MobileUI.core.SuiteManager.hasDialog('chooseEntity'))
            {
                MobileUI.core.SuiteManager.addDialog('chooseEntity', Ext.create('MobileUI.view.dialogs.ChooseEntity'));
            }
            MobileUI.core.SuiteManager.showDialog('chooseEntity');
            this.getApplication().getController('dialogs.ChooseEntity').setEntityTypeFilter(entityType, undefined, true);
            this.getApplication().getController('dialogs.ChooseEntity').setReferenceAttribute(info, this.entityUri);
            this.getApplication().getController('dialogs.ChooseEntity').on('refEntitySelected', function (attribute, value)
            {
               if(this.processReferenceEntitySelected)
               {
                   this.processReferenceEntitySelected(attribute, value);
               }
            }, this, {single: true});
        }
        else if (attrType.type === 'Nested')
        {
            if (this.beforeNewEditorOpen)
            {
                this.beforeNewEditorOpen();
            }
            if (this.isForRelation && this.isForRelation())
            {
                this.getApplication().getController('dialogs.NestedAttributeEditor').showRelationNestedEditor(info,true,false, attrType);
            }else {
                var uri = this.entityUri;
                this.getApplication().getController('dialogs.NestedAttributeEditor').showNestedEditor(uri, info.uri,true,false, attrType);
            }
        }
    },

    /**@protected */
    onItemDelete: function(event, list)
    {
        if (event.getData())
        {
            list = list && list.getStore ? list: null;
            var userData = event.getData().userData,
                store = (list || this.getList()).getStore(),
                value = userData.uri,
                record, entity,
                index = store.findBy(function (record)
                {
                    var data = record.get('userData');
                    if (Ext.isObject(data))
                    {
                        return data.uri === value;
                    }
                    return false;
                });
            if (index !== -1)
            {
                record = store.getAt(index);
                record.set('label', '');
                record.setDirty();
                store.sync();
            }
            if (this.isForRelation && this.isForRelation())
            {
                entity = MobileUI.core.relations.RelationsManager.getState();
            } else
            {
                entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri);
            }
            MobileUI.core.entity.EntityUtils.removeAttribute(entity, value);
        }
    }
});
