/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.NestedAttributeEditor', {
    extend: 'Ext.app.Controller',
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        attributeProcessing: 'MobileUI.controller.MEditableProfile',
        addAttribute: 'MobileUI.controller.MAddAttribute',
        backButton: 'MobileUI.controller.dialogs.MBackButton'
    },
    requires: ['MobileUI.components.list.MListHeight',
        'MobileUI.core.entity.TemporaryEntity',
        'MobileUI.controller.MEditableProfile',
        'MobileUI.core.entity.ReferencedAttributeUtils'],
    config: {
        refs: {
            list: 'elist[name=nested-attribute-editor-list]',
            title: 'component[name=title-nested-attribute-editor]',
            saveButton: 'button[name=save-nested-attribute-button]',
            saveToolbarBtn: 'button[name=save-nested-attribute-button]',
            cancelButton: 'button[name=close-nested-attribute-editor]',
            fakeButton: 'component[name=nestedattributeeditordialog-placeholder]',
            backButton: 'component[name=nestedattributeeditordialog-backToolbarBtn]',
            notificationToolbar: 'component[name=notificationToolbarComplex]',
            notificationMessage: 'component[name=notificationMessageComplex]',
            closeNotificationBtn: 'button[name=closeNotificationBtnComplex]',
            panel: 'panel[name=nested-attribute-editor]'
        },
        control: {
            cancelButton: {
                tap: 'onCancelButtonTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            list: {
                edit: 'onComplexAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete',
                execute: 'onListButtonExecute',
                scrollRequired: 'onScrollRequired'
            },
            saveButton: {
                tap: 'onSaveButtonTap'
            },
            closeNotificationBtn: {
                tap: 'onCloseNotificationBtn'
            }
        },
        views: [
            'MobileUI.view.dialogs.NestedAttributeEditor'
        ]
    },
    entityUri: null,
    nestedAttributeUri: null,
    nestedAttribute: null,
    isForRelationValue: false,
    /**
     * Method clears the list
     */
    clearAll: function ()
    {
        var list = this.getList();
        if (list && list.getStore())
        {
            list.getStore().clearData();
            list.refresh();
        }
    },
    onListButtonExecute: function(event){
        this.onListButtonExecuteInternal(event, this.getList(), this.entityUri + '/'+this.nestedAttributeUri, this.nestedAttributeUri);
    },

    onCancelButtonTap: function () {
        if (this.onBeforeCancel) {
            this.onBeforeCancel();
        }
        MobileUI.core.session.Session.getLookupsManager().cancelState(this.nestedAttributeUri);
        MobileUI.core.SuiteManager.popState();
        if (MobileUI.core.SuiteManager.getCurrentState() && MobileUI.core.SuiteManager.getCurrentState().state)
        {
            this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
        } else {
            MobileUI.core.SuiteManager.hideDialogs(true);
            MobileUI.core.relations.RelationsManager.setState({});
        }
    },
    /**
     * Method shows nested editor using the attribute list
     */
    showNestedEditor: function (nestedAttrType, entityUri, parentUri, uri, isCreate, labelPatternOnly)
    {
        var entity;

        if (!MobileUI.core.SuiteManager.hasDialog('nestedEditor'))
        {
            MobileUI.core.SuiteManager.addDialog('nestedEditor', Ext.create('MobileUI.view.dialogs.NestedAttributeEditor'));
        }
        MobileUI.core.SuiteManager.showDialog('nestedEditor');
        this.nestedAttributeUri = uri;
        this.parentUri = parentUri;
        this.entityUri = entityUri;
        if (!this.isOnBackAction())
        {
            MobileUI.core.session.Session.getLookupsManager().createState(uri);
            MobileUI.core.SuiteManager.putState('nestedEditor', {
                dialog: 'nestedEditor',
                controller: 'dialogs.NestedAttributeEditor',
                method: 'showNestedEditor',
                parameters: [nestedAttrType, entityUri, parentUri, uri,  isCreate, labelPatternOnly]
            });
        }
        entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri, uri);
        if (!entity) {
            entity = MobileUI.core.entity.TemporaryEntity.saveState(MobileUI.core.entity.TemporaryEntity.getEntity(entityUri), uri);
        }
        this.entity = entity;
        this.options = {
            nestedAttrType: nestedAttrType,
            isCreate: isCreate,
            labelPatternOnly: labelPatternOnly
        };

        this.getTitle().setHtml(i18n('Edit %1', Ext.util.Format.htmlEncode(i18n(nestedAttrType.label)) || i18n('Attribute')));

        this.refreshContent(entity,this.options);
    },

    onComplexAttributeSelect: function(record) {
        this.showNestedEditor(record.get('editor'), this.entityUri, this.nestedAttributeUri, record.get('userData').uri, this.options.isCreate);
    },

    getRequiredAttributes: function (attrType, entity, parentUri) {
        var extension = this.getProfileConfiguration(entity.type),
            hiddenAttributes = extension && extension.hiddenAttributes || [];

        var allAttributes = (attrType.attributes || [])
            .filter(function (attrType) {
                return hiddenAttributes.indexOf(attrType.uri) === -1;
            });
        var requiredAttributes = allAttributes
            .filter(function (attr) {
                return MobileUI.core.CardinalityChecker.isRequired(attr) && !attr.hidden;
            })
            .filter(function (attr) {
                return !MobileUI.core.entity.EntityUtils.isComplex(attr);
            });
        return requiredAttributes
            .filter(function (attributeType) {
                return !entity.attributes.some(function (attribute) {
                    return attribute.type === attributeType.uri;
                });
            }).map(function (attrType) {
                return {
                    name: attrType.name,
                    ov: true,
                    parentUri: parentUri,
                    type: attrType.uri,
                    uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(parentUri, attrType),
                    value: ''
                };
            }, this);
    },

    refreshContent: function(entity, options){
        var list, store, model, tmpAttributes;
        this.clearAll();
        var nestedAttrType = options.nestedAttrType;
        var isCreate = options.isCreate;
        var labelPatternOnly = options.labelPatternOnly;
        var path = nestedAttrType.label;
        var structure;
        this.isInCreateMode = isCreate;
        var configuration = MobileUI.core.Services.getConfigurationManager(),
            perspective = configuration.findPerspective(entity.type),
            extension = configuration.getExtensionById(perspective),
            hiddenAttributes = extension && extension.hiddenAttributes || [];

        var policy = function (item) {
            return this.indexOf(item.uri) === -1;
        }.bind(hiddenAttributes);

        if (isCreate)
        {
            if (this.options.onDelete){
                tmpAttributes = entity.attributes.filter(function (item) {
                    return item.parentUri === this.nestedAttributeUri;
                }, this);
                tmpAttributes.forEach(function(attribute){
                    var attrType = this.options.nestedAttrType.attributes.filter(function(item){
                        return item.name === attribute.name;
                    });
                    attribute.attrType = attrType.length > 0 ? attrType[0]: null;
                },this);
            }
            if (!tmpAttributes) {
                if (labelPatternOnly) {
                    tmpAttributes = MobileUI.core.entity.EntityUtils.collectLabelAttributes(nestedAttrType);
                } else {
                    tmpAttributes = nestedAttrType.attributes;
                }
                tmpAttributes = tmpAttributes.map(function (attributeObject) {
                    return {
                        value: MobileUI.core.entity.EntityUtils.isComplex(attributeObject) ? {} : '',
                        type: attributeObject.uri,
                        attrType: attributeObject,
                        ov: true,
                        name: attributeObject.name,
                        uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(this.nestedAttributeUri, attributeObject),
                        parentUri: this.nestedAttributeUri
                    };
                }, this);
            }
            var nestedAttributesExists = entity.attributes.some(function (item) {
                return item.uri === this.nestedAttributeUri;
            }, this);
            var nestedAttr = {
                value: MobileUI.core.entity.EntityUtils.isComplex(nestedAttrType) ? {} : '',
                type: nestedAttrType.uri,
                attrType: nestedAttrType,
                ov: true,
                name: nestedAttrType.name,
                uri: this.nestedAttributeUri,
                parentUri: this.parentUri
            };
            if (!nestedAttributesExists) {
                entity.attributes.push(nestedAttr);
            }
            var nestedInAddedAttributesExists = entity.addedAttributes.some(function (item) {
                return item.uri === this.nestedAttributeUri;
            }, this);
            if (!nestedInAddedAttributesExists) {
                entity.addedAttributes.push(nestedAttr);
            }
            if (!this.options.onDelete) {
                entity.attributes = entity.attributes.concat(tmpAttributes);
            }
            this.options.onDelete = false;

            structure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(nestedAttrType, null, policy);
            
            structure.forEach(function(item){
                if (item.values && (item.values.length === 1) && (item.values[0].attributes)) {
                    return;
                }
                item.label = path;
            });
            model = this.createModel(tmpAttributes, structure);
            model = model.filter(MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInCreateMode, this);
        } else {
            var filterOv = function(item)
            {
                return item.ov !== false;
            };

            entity.attributes = entity.attributes.concat(this.getRequiredAttributes(this.options.nestedAttrType, entity, this.nestedAttributeUri));
            tmpAttributes = entity.attributes
                .filter(function (item) {
                    return item.parentUri === this.nestedAttributeUri;
                }, this)
                .filter(filterOv);

            tmpAttributes.forEach(function(attribute){
                var attrType = this.options.nestedAttrType.attributes.filter(function(item){
                    return item.name === attribute.name;
                });
                attribute.attrType = attrType.length > 0 ? attrType[0]: null;
            },this);
            structure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(nestedAttrType, null, policy);
            structure.forEach(function(item){
                if (item.values && (item.values.length === 1) && (item.values[0].attributes)) {
                    return;
                }
                item.label = path;
            });
            model = this.createModel(tmpAttributes, structure);
        }

        model.forEach(function(item, index) {
            item.lastInGroup = index === (model.length-1);
        });
        this.updateDependentLookups(model);

        list = this.getList();
        store = list.getStore();
        store.clearData();
        store.add(model);
        list.refresh();
        this.highlightValidationErrors(list);
        this.updateListHeight(list);
    },
    onItemDelete: function(record){
        this.deleteAttribute(record, this.isInCreateMode ? 'clear': null);
        this.options.onDelete = true;
        this.refreshContent(this.entity, this.options);
    },

    isForRelation: function(){
        return this.isForRelationValue;
    },

    actualizeSaveButton: function(){
        var innerAttributes = this.entity.attributes.filter(function(attribute){
            return attribute.parentUri === this.nestedAttributeUri;
        }, this);
        var nonEmpty = innerAttributes.some(function(attribute){
            return !Ext.isEmpty(attribute && attribute.value);
        });
        if (nonEmpty){
            return true;
        } else{
            this.getSaveToolbarBtn().setDisabled(true);
            return false;
        }
    },

    /**
     * @private
     * @returns {boolean}
     */
    saveNestedAttributeInternal: function ()
    {
        var nestedAttributes = this.entity.attributes.filter(function(attribute){
            return attribute.uri === this.nestedAttributeUri;
        }, this);
        var innerAttributes = this.entity.attributes.filter(function(attribute){
            return attribute.parentUri === this.nestedAttributeUri;
        }, this);
        var nested = nestedAttributes.length > 0 ? nestedAttributes[0]: null;
        if (nested)
        {
            var labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern(this.options.nestedAttrType.uri) || this.options.nestedAttrType.dataLabelPattern || null;
            var attributeProcessing = function(attribute){
                return !Ext.isObject(attribute.value) ? attribute.value : null;
            };
            nested.attrType = this.options.nestedAttrType;
            nested.label = MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(innerAttributes, labelPattern, attributeProcessing);
            var currentState = MobileUI.core.entity.TemporaryEntity.popState(this.entityUri);
            MobileUI.core.entity.TemporaryEntity.updateState(currentState, this.parentUri);
        }

        return true;
    },
    /**
     * Save button tap listener
     */
    onSaveButtonTap: function () {
        this.allRequiredAttributesFilled(this.getList(), this.options.nestedAttrType.attributes)
            .then(function (result) {
                var validationOk = MobileUI.core.entity.EntityUtils.validationPassed(result);
                if (validationOk) {
                    if (this.saveNestedAttributeInternal()) {
                        MobileUI.core.session.Session.getLookupsManager().saveState(this.nestedAttributeUri);
                        MobileUI.core.SuiteManager.popState();
                        var state = MobileUI.core.SuiteManager.getCurrentState();
                        if (state) {
                            this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
                        } else {
                            MobileUI.core.SuiteManager.hideDialogs(true);
                        }
                    }
                } else {
                    this.highlightValidationErrors(this.getList());
                    this.showValidationErrorMessage(result);
                    this.updateListHeight(this.getList());
                }
            }.bind(this));
    },
    getEditedEntity: function(){
        return this.entity;
    },
    /**
     * Back button tap listener
     */
    onBackButtonTap: function () {
        MobileUI.core.SuiteManager.popState();
        var currentState = MobileUI.core.SuiteManager.getCurrentState();
        if (currentState) {
            this.processState(currentState.state);
        } else {
            MobileUI.core.SuiteManager.hideDialogs(true);
        }
    },
    onScrollRequired: function(value) {
        var scrollable = this.getPanel().getScrollable();
        var scroller = scrollable && scrollable.getScroller();
        setTimeout(function() {
            scroller.scrollTo(0, value, {duration: 200});
        }, 200);
    }
});
