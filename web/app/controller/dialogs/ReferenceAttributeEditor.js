/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.ReferenceAttributeEditor', {
    extend: 'Ext.app.Controller',
    mixins: {
        listHeight: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        attributeProcessing: 'MobileUI.controller.MEditableProfile',
        backButton: 'MobileUI.controller.dialogs.MBackButton',
        addAttribute: 'MobileUI.controller.MAddAttribute',
        actions: 'MobileUI.controller.MEntityAction'
    },
    requires: ['MobileUI.components.list.MListHeight',
        'MobileUI.core.entity.TemporaryEntity',
        'MobileUI.core.entity.ReferencedAttributeUtils',
        'MobileUI.controller.MEditableProfile',
        'MobileUI.controller.MAddAttribute',
        'MobileUI.controller.dialogs.MComplexAttributeProcessing'],
    config: {
        refs: {
            list: 'elist[name=reference-attribute-editor-list]',
            entityList: 'elist[name=reference-attribute-editor-entity-list]',
            title: 'component[name=title-reference-attribute-editor]',
            saveButton: 'button[name=save-reference-attribute-button]',
            saveToolbarBtn: 'button[name=save-reference-attribute-button]',
            cancelButton: 'button[name=close-reference-attribute-editor]',
            fakeButton: 'component[name=referenceattributeeditordialog-placeholder]',
            backButton: 'component[name=referenceattributeeditordialog-backToolbarBtn]',
            listSeparator: 'component[name=reference-attribute-editor-list-separator]',
            notificationToolbar: 'component[name=notificationToolbarRef]',
            notificationMessage: 'component[name=notificationMessageRef]',
            closeNotificationBtn: 'button[name=closeNotificationBtnRef]',
            panel: 'panel[name=reference-attribute-editor]'
        },
        control: {
            cancelButton: {
                tap: 'onCancelButtonTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            list: {
                edit: 'onComplexRelationAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                selectItem: 'onSelectEntity',
                'delete': 'onRelationItemDelete',
                execute: 'onRelationListButtonExecute',
                scrollRequired: 'onScrollRequired'
            },
            entityList: {
                edit: 'onComplexEntityAttributeSelect',
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onEntityItemDelete',
                scrollRequired: 'onScrollRequired'
            },
            saveButton: {
                tap: 'onSaveButtonTap'
            },
            closeNotificationBtn: {
                tap: 'onCloseNotificationBtn'
            }
        },
        views: [
            'MobileUI.view.dialogs.ReferenceAttributeEditor'
        ]
    },
    /**
     * @private
     * @type {String}
     */
    entityUri: null,
    /**
     * @private
     * @type {Object}
     */
    referenceAttribute: null,
    /**
     * Method clears the list
     */
    clearAll: function () {
        var list = this.getList();
        if (list && list.getStore()) {
            list.getStore().clearData();
            list.refresh();
        }

        list = this.getEntityList();
        if (list && list.getStore()) {
            list.getStore().clearData();
            list.refresh();
        }
    },
    onCancelButtonTap: function () {
        if (this.onBeforeCancel) {
            this.onBeforeCancel();
        }
        MobileUI.core.SuiteManager.hideDialogs(true);
        MobileUI.core.relations.RelationsManager.setState({});
    },
    /**
     *  Method processes selected entity as reference attribute
     */
    processReferenceEntitySelected: function () {
        MobileUI.core.SuiteManager.showDialog('referenceDialog');
        this.createReferenceAttribute(this.entityUri, this.referenceAttribute, null, false);
    },
    /**
     * Method invokes before the open the new editor
     */
    beforeNewEditorOpen: function () {
        var entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri);
        this.saveReferenceAttributeInternal(entity);
    },
    onRelationListButtonExecute: function(event){
        var data = event.record.get('userData');
        if (data) {
            var store = event.item.dataview.getStore(),
                index = store.indexOf(event.record),
                msg = this.getNotificationMessage && this.getNotificationMessage().getHtml();

            if (this.getSaveToolbarBtn()) {
                this.getSaveToolbarBtn().setDisabled(false);
            }

            this.showAddAttributeDialog(data.attributes, index, this.getList(), this.relationAttributesEntity.uri, this.relationAttributesEntity.uri, this.relationAttributesEntity.uri, msg);
        }
    },
    onBeforeCancel: function(){
    },
    onSelectEntity: function(){
        //TODO: TBD;
        //if (!MobileUI.core.SuiteManager.hasDialog('chooseEntity'))
        //{
        //    MobileUI.core.SuiteManager.addDialog('chooseEntity', Ext.create('MobileUI.view.dialogs.ChooseEntity'));
        //}
        //this.getApplication().getController('dialogs.ChooseEntity').setEntityTypeFilter(this.relationAttributesEntity.original.type, undefined, true);
        //this.getApplication().getController('dialogs.ChooseEntity').setUpdateMode(true);
        //this.getApplication().getController('dialogs.ChooseEntity').on('refEntitySelected',
        //    function(info, newReference) {
        //        //attrType.valueChangedCallback.call(attrType.valueChangedContext,
        //        //    newReference, {getRecord: function() {return event;}}, attrType);
        //        //this.showComplexAttributeEditor(this.attribute.uri, this.editable && this.tmpEntity.uri);
        //    }, this, {single: true});
        //MobileUI.core.SuiteManager.showDialog('chooseEntity');
    },
    /**
     * Method processes selected entity as reference attribute
     * @param entityUri {String} current entity uri
     * @param attributeUri
     * @param parentUri
     * @param referenceAttribute {Object} reference attribute object
     * @param response
     */
    chooseReferenceAttribute: function (entityUri, attributeUri, parentUri, referenceAttribute, response) {
        if (!MobileUI.core.SuiteManager.hasDialog('referenceDialog')) {
            MobileUI.core.SuiteManager.addDialog('referenceDialog', Ext.create('MobileUI.view.dialogs.ReferenceAttributeEditor'));
        }
        if (!this.isOnBackAction()) {
            MobileUI.core.SuiteManager.putState('referenceDialog', {
                dialog: 'referenceDialog',
                controller: 'dialogs.ReferenceAttributeEditor',
                method: 'chooseReferenceAttribute',
                parameters: [entityUri, attributeUri, parentUri, referenceAttribute, response]
            });
        }
        MobileUI.core.SuiteManager.showDialog('referenceDialog');
        var relation = MobileUI.core.entity.TemporaryEntity.getEntity(referenceAttribute.relationURI);
        this.editRelationAttribute(entityUri, attributeUri, parentUri, referenceAttribute, relation, response, true);
        if (this.getSaveToolbarBtn()) {
            this.getSaveToolbarBtn().setDisabled(false);
        }
    },

    editRelation: function(attrType, entityUri, parentUri, attributeUri)
    {
        if (!this.isOnBackAction()) {
            MobileUI.core.SuiteManager.putState('referenceDialog', {
                dialog: 'referenceDialog',
                controller: 'dialogs.ReferenceAttributeEditor',
                method: 'editRelation',
                parameters: [attrType, entityUri, parentUri, attributeUri]
            });
        }
        var entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri, attributeUri);
        if (!entity) {
            entity = MobileUI.core.entity.TemporaryEntity.saveState(MobileUI.core.entity.TemporaryEntity.getCurrentState(entityUri), attributeUri);
        }
        var refAttribute = entity.attributes.filter(function (item) {
            return item.uri === attributeUri;
        }, this);
        if (refAttribute.length > 0){
            refAttribute = refAttribute[0];
            refAttribute.attrType = attrType;
            refAttribute.relationURI = refAttribute.refRelation.objectURI;
        }
        this.relationURI = refAttribute.relationURI;
        var relation = MobileUI.core.entity.TemporaryEntity.getEntity(refAttribute.relationURI);
        if (!relation)
        {
            MobileUI.core.session.Session.findEntity(refAttribute.relationURI, function(refRelation) {
                relation = MobileUI.core.entity.TemporaryEntity.createNewEntity(refRelation.type, refRelation,null, true, refAttribute.relationURI);
                MobileUI.core.entity.TemporaryEntity.addEntity(relation);
                var attributeType =  MobileUI.core.Metadata.getRelationType(attrType.relationshipTypeURI);
                relation.attributes.forEach(function(attribute){
                    var attrType = attributeType.attributes.filter(function(item){
                        return item.name === attribute.name;
                    });
                    attribute.attrType = attrType.length > 0 ? attrType[0]: null;
                },this);

                var refEntityURI = refAttribute.refEntity.objectURI;
                MobileUI.core.session.Session.findEntity(refEntityURI, function(refEntity) {
                    MobileUI.core.entity.TemporaryEntity.addEntity(refEntity);
                    this.editRelationAttribute(entityUri, attributeUri, parentUri, attrType, relation, refAttribute, false);
                }, this);
            }, this);

        }else{
            this.editRelationAttribute(entityUri, attributeUri, parentUri, attrType, relation, refAttribute, false);
        }
    },
    filterRelationAttributes: function(relAttrType, attributeUris){
        if (attributeUris && relAttrType){
            relAttrType = Ext.clone(relAttrType);
            relAttrType.attributes  = (relAttrType.attributes || []).filter(function(item){
                return attributeUris.indexOf(item.uri) !== -1;
            });
        }
        return relAttrType;
    },
    editRelationAttribute: function (entityUri, attributeUri, parentUri, referenceAttribute, relation, response, createMode) {
        if (this.getEntityList()) {
            this.getEntityList().setHidden(true);
        }
        this.getList().setStyle({'margin-top' : '10px'});
        if (this.getListSeparator()) {
            this.getListSeparator().setHidden(true);
        }
        var model,attributes;
        var relAttrType;
        this.isEntityListAvailable = false;
        this.referenceAttributeItem = null;
        this.entityUri = entityUri;
        this.attributeUri = attributeUri;
        this.attributeParentUri = parentUri;

        if (relation) {
            this.relationAttributesEntity = relation;
            this.relationAttributesEntity.relationURI = this.relationAttributesEntity.uri;
        } else {
            this.relationAttributesEntity = MobileUI.core.entity.TemporaryEntity.getEntity(referenceAttribute.relationURI);
            if (!this.relationAttributesEntity) {
                this.relationAttributesEntity = MobileUI.core.entity.TemporaryEntity.createNewEntity(referenceAttribute.relationshipTypeURI);
                referenceAttribute.relationURI = this.relationAttributesEntity.uri;
            }
        }
        MobileUI.core.session.Session.getUriMapping().registerMapping(this.relationAttributesEntity.uri, this.attributeUri);
        this.isInCreateMode = createMode;
        if (createMode) {
            this.referenceAttribute = {
                value: MobileUI.core.entity.EntityUtils.isComplex(referenceAttribute) ? {} : '',
                type: referenceAttribute.uri,
                attrType: referenceAttribute,
                ov: true,
                name: referenceAttribute.name,
                refEntity: {},
                refRelation: {},
                label: response.label,
                uri: attributeUri,
                parentUri: parentUri
            };
            this.referencedEntity = MobileUI.core.entity.EntityUtils.makeEditableEntity(response);
            this.getTitle().setHtml(i18n('Create %1', (referenceAttribute && Ext.util.Format.htmlEncode(i18n(referenceAttribute.label)) || '')));
            relAttrType = MobileUI.core.Metadata.getRelationType(referenceAttribute.relationshipTypeURI);
            relAttrType = this.filterRelationAttributes(relAttrType, referenceAttribute.referencedAttributeURIs);
            this.relationAttributesEntityStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(relAttrType, null, function (attrType) {
                return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
            }, true);
            if (!this.isOnBackAction()) {
                this.relationAttributesEntity.attributes = relAttrType.attributes.map(function (attributeObject) {
                    return {
                        value: MobileUI.core.entity.EntityUtils.isComplex(attributeObject) ? {} : '',
                        type: attributeObject.uri,
                        attrType: attributeObject,
                        ov: true,
                        name: attributeObject.name,
                        uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(this.relationAttributesEntity.uri, attributeObject),
                        parentUri: this.relationAttributesEntity.uri
                    };
                }, this);
            }
            attributes = this.relationAttributesEntity.attributes.filter(function(item){
                return item.parentUri === this.relationAttributesEntity.uri;
            },this);
            model = this.createModel(attributes, this.relationAttributesEntityStructure, this.relationAttributesEntity, true);

            model = model.filter(MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInCreateMode, this);

            this.referenceAttributeItem = {
                attrType: MobileUI.core.Metadata.getEntityType(response.type),
                userData: response,
                label: response.label || i18n('<No label>'),
                type: MobileUI.components.list.items.BaseListItem.types.entityInfo,
                disableSwipe: true
            };

            model.unshift(this.referenceAttributeItem);

            if (!this.isOnBackAction()) {
                var entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri, attributeUri);
                if (!entity) {
                    entity = MobileUI.core.entity.TemporaryEntity.saveState(MobileUI.core.entity.TemporaryEntity.getCurrentState(entityUri), attributeUri);
                }
                var preventAddAttribute = (entity.attributes || []).some(function(attribute){
                    return attribute.uri === this.referenceAttribute.uri;
                }, this);
                if (!preventAddAttribute){
                    entity.addedAttributes.push(this.referenceAttribute);
                    entity.attributes.push(this.referenceAttribute);
                }
                referenceAttribute.referencedEntityURI = response.uri;
            }
        }else {
            this.getTitle().setHtml(i18n('Edit %1', (referenceAttribute && Ext.util.Format.htmlEncode(i18n(referenceAttribute.label))) || i18n('Attribute')));
            relAttrType = MobileUI.core.Metadata.getRelationType(referenceAttribute.relationshipTypeURI);
            relAttrType = this.filterRelationAttributes(relAttrType, referenceAttribute.referencedAttributeURIs);
            this.relationAttributesEntityStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(relAttrType, null, function (attrType) {
                return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
            }, true);
            if (!this.isOnBackAction()) {
                this.relationAttributesEntity.editedAttributes = Ext.clone(this.relationAttributesEntity.attributes);
            }
            attributes = this.relationAttributesEntity.attributes.filter(function(item){
                return item.parentUri === this.relationAttributesEntity.uri;
            },this);
            model = this.createModel(attributes, this.relationAttributesEntityStructure, this.relationAttributesEntity, true);

            model = model.filter(MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInEditMode, this);

            this.referenceAttributeItem = {
                //attrType: MobileUI.core.Metadata.getEntityType(response.type),
                userData: response,
                label: response.label || i18n('<No label>'),
                type: MobileUI.components.list.items.BaseListItem.types.entityInfo,
                disableSwipe: true
            };

            this.referenceAttribute = { attrType: referenceAttribute };

            model.unshift(this.referenceAttributeItem);
        }
        var store,
            list = this.getList();
        store = list.getStore();
        store.clearData();
        this.updateDependentLookups(model);
        store.add(model);
        this.highlightValidationErrors(list);
        this.updateListHeight(list);
        this.editedEntity = this.relationAttributesEntity;

    },

    collectEntityAttributes: function (referencedAttrTypes, suggestMode) {
        var md = MobileUI.core.Metadata;
        return referencedAttrTypes.filter(function (uri) {
            return uri.indexOf('configuration/entityTypes') === 0;
        }).map(function (uri) {
            return md.findEntityAttributeByUri(null, uri);
        }).filter(function (attrType) {
            if (!attrType) {
                return false;
            }
            var security = MobileUI.core.PermissionManager.securityService().metadata;
            var canCreate = suggestMode ? security.canUpdateOrInitiate(attrType) : security.canUpdate(attrType);
            return canCreate && !attrType.hidden;
        });
    },
    /**
     * Method creates the reference attribute editor
     * @param entityUri {String} entity uri
     * @param attributeUri {String} attribute uri
     * @param parentUri {String}
     * @param referenceAttribute {Object} reference attribute
     * @param label {String?} label value for the first attribute
     */
    createReferenceAttribute: function (entityUri, attributeUri, parentUri, referenceAttribute, label) {
        var entity, labelAttributes;
        this.isInCreateMode = true;
        this.isEntityListAvailable = true;
        this.entityUri = entityUri;
        this.attributeParentUri = parentUri;
        this.referenceAttributeItem = null;
        this.clearAll();
        this.getTitle().setHtml(i18n('Create %1', (referenceAttribute && Ext.util.Format.htmlEncode(i18n(referenceAttribute.label)) || '')));
        this.attributeUri = attributeUri;

        entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri, attributeUri);
        if (!entity) {
            entity = MobileUI.core.entity.TemporaryEntity.saveState(MobileUI.core.entity.TemporaryEntity.getCurrentState(entityUri), attributeUri);
        }

        this.referenceAttribute = null;
        for (var i = 0; i < entity.attributes.length; i++) {
            if (entity.attributes[i].uri === attributeUri) {
                this.referenceAttribute = entity.attributes[i];
                break;
            }
        }
        var defaultAttribute = {
            value: MobileUI.core.entity.EntityUtils.isComplex(referenceAttribute) ? {} : '',
            type: referenceAttribute.uri,
            attrType: referenceAttribute,
            ov: true,
            name: referenceAttribute.name,
            refEntity: {},
            refRelation: {},
            uri: attributeUri,
            parentUri: parentUri
        };
        if (!this.referenceAttribute) {
            this.referenceAttribute = defaultAttribute;
        }
        else {
            Object.keys(defaultAttribute).forEach(function(key) {
                if (!this.referenceAttribute.hasOwnProperty(key)) {
                    this.referenceAttribute[key] = defaultAttribute[key];
                }
            }, this);
        }

        referenceAttribute = Ext.clone(referenceAttribute);
        if (!this.isOnBackAction()) {
            MobileUI.core.SuiteManager.putState('referenceDialog', {
                dialog: 'referenceDialog',
                controller: 'dialogs.ReferenceAttributeEditor',
                method: 'createReferenceAttribute',
                parameters: [entityUri, attributeUri, parentUri, referenceAttribute, label]
            });

        }

        this.referencedEntity =  MobileUI.core.entity.TemporaryEntity.getEntity(referenceAttribute.referencedEntityURI);
        if (!this.referencedEntity){
            this.referencedEntity = MobileUI.core.entity.TemporaryEntity.createNewEntity(referenceAttribute.referencedEntityTypeURI);
            referenceAttribute.referencedEntityURI = this.referencedEntity.uri;
            MobileUI.core.session.Session.getUriMapping().registerMapping(this.referencedEntity.uri, this.attributeUri);
        }
        this.referencedEntity.suggestedMode = !MobileUI.core.PermissionManager.securityService().metadata.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.UPDATE, this.referencedEntity.type);
        labelAttributes = this.collectEntityAttributes(this.referenceAttribute.attrType.referencedAttributeURIs, this.referencedEntity.suggestedMode);
        var tmpAttributes = labelAttributes.map(function (attributeObject) {
            var value = '';
            if (attributeObject && attributeObject.type === 'String'){
                value = label;
                label = '';
                if (this.getSaveToolbarBtn()) {
                    this.getSaveToolbarBtn().setDisabled(false);
                }
            }
            return {
                value: MobileUI.core.entity.EntityUtils.isComplex(attributeObject) ? {} : value,
                type: attributeObject.uri,
                attrType: attributeObject,
                ov: true,
                name: attributeObject.name,
                uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(this.referencedEntity.uri, attributeObject),
                parentUri: this.referencedEntity.uri
            };
        }, this);
        if (!this.isOnBackAction()) {
            entity.addedAttributes.push(this.referenceAttribute);
            if (!entity.attributes.some(function(attr) { return attr.uri === this.referenceAttribute.uri; }, this)) {
                entity.attributes.push(this.referenceAttribute);
            }
            this.referencedEntity.addedAttributes = tmpAttributes.filter(function (item) {
                return item.attrType.type === 'String' && !Ext.isEmpty(item.value);
            });
            this.referencedEntity.attributes = tmpAttributes;
        }

        this.relationAttributesEntity =  MobileUI.core.entity.TemporaryEntity.getEntity(referenceAttribute.relationURI);
        if (!this.relationAttributesEntity){
            this.relationAttributesEntity = MobileUI.core.entity.TemporaryEntity.createNewEntity(referenceAttribute.relationshipTypeURI);
            referenceAttribute.relationURI = this.relationAttributesEntity.uri;
            MobileUI.core.session.Session.getUriMapping().registerMapping(this.relationAttributesEntity.uri, this.attributeUri);
        }

        var relAttrType = MobileUI.core.Metadata.getRelationType(referenceAttribute.relationshipTypeURI);
        relAttrType = this.filterRelationAttributes(relAttrType, referenceAttribute.referencedAttributeURIs);
        if (!this.isOnBackAction()) {
            this.relationAttributesEntity.attributes = relAttrType.attributes.map(function (attributeObject) {
                return {
                    value: MobileUI.core.entity.EntityUtils.isComplex(attributeObject) ? {} : '',
                    type: attributeObject.uri,
                    attrType: attributeObject,
                    ov: true,
                    name: attributeObject.name,
                    uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(this.relationAttributesEntity.uri, attributeObject),
                    parentUri: this.relationAttributesEntity.uri
                };
            }, this);
        }
        this.referenceEntityStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure({attributes: labelAttributes}, null,null, true);

        var configuration = MobileUI.core.Services.getConfigurationManager(),
            perspective = configuration.findPerspective(referenceAttribute.referencedEntityTypeURI),
            extension = configuration.getExtensionById(perspective),
            hiddenAttributes = extension && extension.hiddenAttributes || [];

        var policy = function (item) {
            return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(item) && this.indexOf(item.uri) === -1;
        }.bind(hiddenAttributes);

        this.relationAttributesEntityStructure = MobileUI.core.entity.EntityUtils.createEditableEntityStructure(relAttrType, null, policy, true);
        this.getEntityList().setHidden(false);
        this.getList().setStyle({'margin-top' : '0' });
        if (this.getListSeparator()){
            this.getListSeparator().setHidden(false);
        }
        this.updateList(this.getEntityList(), this.referencedEntity, this.referenceEntityStructure);
        this.updateList(this.getList(), this.relationAttributesEntity, this.relationAttributesEntityStructure);
    },

    updateList: function (list, entity, structure) {
        var attributes = entity.attributes.filter(function(item){
            return item.parentUri === entity.uri && item.ov !== false;
        });
        var store,
            model = this.createModel(attributes, structure, entity, true).map(function(item){
            if (item.type === 'entityInfo'){
                item.type = 'property';
            }
            return item;
        });

        if (this.referenceAttributeItem) {
            model.unshift(this.referenceAttributeItem);
        }

        model = model.filter(this.isInCreateMode ?
                MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInCreateMode :
                MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInEditMode,
            this);

        store = list.getStore();
        store.clearData();
        this.updateDependentLookups(model);
        store.add(model);
        this.highlightValidationErrors(list);
        this.updateListHeight(list);
    },
    /**
     * @private
     * Method saves the reference attribute return true in case of no errors
     * @returns {boolean}
     */
    saveReferenceAttributeInternal: function ()
    {
        var entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.entityUri, this.attributeUri);
        if (!entity) {
            return true;
        }
        var referenceAttribute = entity.attributes.filter(function(attribute){
            return attribute.uri === this.attributeUri;
        }, this);
        var referenced = referenceAttribute.length > 0 ? referenceAttribute[0]: null;
        if (referenced)
        {
            var relationshipLabel = referenced.attrType.relationshipLabelPattern || null;
            var attributeProcessing = function(attribute){
                return !Ext.isObject(attribute.value) ? attribute.value : null;
            };
            var relationAttributes = this.relationAttributesEntity.attributes.filter(function(attribute){
                return attribute.parentUri === this.relationAttributesEntity.uri;
            }, this);
            referenced.relationshipLabel = MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(relationAttributes, relationshipLabel, attributeProcessing);
            if (referenced.relationshipLabel === i18n('<No label>')){
                referenced.relationshipLabel = '';
            }
            if (this.isInCreateMode){
                var labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern(this.referenceAttribute.attrType.referencedEntityTypeURI)  || null;

                referenced.objectURI = this.referencedEntity.uri;
                referenced.relationURI = this.relationAttributesEntity.uri;
                referenced.refEntity = {
                    type:this.referencedEntity.type,
                    objectURI: this.referencedEntity.uri
                };
                referenced.refRelation = {
                    objectURI: this.relationAttributesEntity.uri
                };
                var attributes = this.referencedEntity.attributes.filter(function(attribute){
                    return attribute.parentUri === this.referencedEntity.uri;
                }, this);
                referenced.label = (this.referencedEntity.original && !Ext.isEmpty(this.referencedEntity.original.label)) ? this.referencedEntity.original.label : MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(attributes, labelPattern, attributeProcessing);
            } else {
                var editedAttributes = this.relationAttributesEntity.editedAttributes.filter(function(item){
                    return item.edited;
                });
                this.relationAttributesEntity.editedAttributes = editedAttributes;
                Array.prototype.push.apply(entity.editedAttributes, editedAttributes);
                Array.prototype.push.apply(entity.addedAttributes, this.relationAttributesEntity.addedAttributes);
                Array.prototype.push.apply(entity.deletedAttributes, this.relationAttributesEntity.deletedAttributes);
            }
            entity.editedAttributes = entity.editedAttributes.filter(function(attribute){
                return attribute.uri !== this.uri;
            }, referenced);
            entity.editedAttributes.push(referenced);
            var currentState = MobileUI.core.entity.TemporaryEntity.popState(this.entityUri);
            MobileUI.core.entity.TemporaryEntity.updateState(currentState, this.attributeParentUri);
        }

        return true;
    },
    /**
     * Save button tap listener
     */
    onSaveButtonTap: function ()
    {
        var relAttrType = MobileUI.core.Metadata.getRelationType(this.referenceAttribute.attrType.relationshipTypeURI);
        relAttrType = this.filterRelationAttributes(relAttrType, this.referenceAttribute.attrType.referencedAttributeURIs);
        var validations = [];
        if (this.isEntityListAvailable) {
            var refAttrTypes = this.collectEntityAttributes(this.referenceAttribute.attrType.referencedAttributeURIs, this.referencedEntity && this.referencedEntity.suggestedMode);
            validations.push(this.allRequiredAttributesFilled(this.getEntityList(), refAttrTypes, this.editedEntity));
        }
        validations.push(this.allRequiredAttributesFilled(this.getList(), relAttrType.attributes, this.relationAttributesEntity));
        Promise.all(validations).then(function (results) {
            var result = {};
            results.forEach(function (item) {
                result = MobileUI.core.util.Util.mergeWithArray(item, result);
            });
            var validationPassed = MobileUI.core.entity.EntityUtils.validationPassed(result);
            if (validationPassed) {
                if (this.saveReferenceAttributeInternal()) {
                    this.beforeListener = false;
                    MobileUI.core.SuiteManager.popState();
                    var state = MobileUI.core.SuiteManager.getCurrentState();
                    if (state) {
                        this.processState(MobileUI.core.SuiteManager.getCurrentState().state);
                    } else {
                        MobileUI.core.SuiteManager.hideDialogs(true);
                    }
                }
            } else {
                if (this.isEntityListAvailable) {
                    this.highlightValidationErrors(this.getEntityList());
                }
                this.highlightValidationErrors(this.getList());
                this.showValidationErrorMessage(result);
            }
        }.bind(this));
    },
    getEditedEntity: function(){
        return this.entity;
    },
    /**
     * Back button tap listener
     */
    onBackButtonTap: function () {
        MobileUI.core.SuiteManager.popState();
        var currentState = MobileUI.core.SuiteManager.getCurrentState();
        if (currentState) {
            this.processState(currentState.state);
        } else {
            MobileUI.core.SuiteManager.hideDialogs(true);
        }
    },
    onEntityItemDelete: function(record){
        var context = Ext.clone(this);
        context.entity = this.referencedEntity;
        this.deleteAttribute.call(context, record, 'clear');
        this.updateList(this.getEntityList(), this.referencedEntity, this.referenceEntityStructure);
    },
    onRelationItemDelete: function(record){
        var context = Ext.clone(this);
        context.entity = this.relationAttributesEntity;
        this.deleteAttribute.call(context, record, this.isInCreateMode ? 'clear': null);
        this.updateList(this.getList(), this.relationAttributesEntity, this.relationAttributesEntityStructure);
    },
    onComplexRelationAttributeSelect: function(record){
        //TODO: we can't use clone here. Refactor.
        var context = Ext.clone(this);
        context.originalUri = this.relationAttributesEntity.uri;
        context.parentUri = this.relationAttributesEntity.uri;
        context.entity = this.relationAttributesEntity;
        this.onComplexAttributeTap.call(context, record);
    },
    beforeComplexEditorOpened: function(){
        this.beforeListener = true;
        //MobileUI.core.SuiteManager.on('dialogActivated', function(name){
        //    console.log(name);
        //},this);
        MobileUI.core.SuiteManager.on('hideDialogs', function() {
            if (this.beforeListener){
                this.entity = MobileUI.core.entity.TemporaryEntity.getEntity(this.originalUri);
                this.processEditEntity(this.entity);
            }
        }, this, {single: true, delay: 700});
    },
    onComplexEntityAttributeSelect: function(record){
        var context = Ext.clone(this);
        context.originalUri = this.referencedEntity.uri;
        context.parentUri = this.referencedEntity.uri;
        context.entity = this.referencedEntity;

        this.onComplexAttributeTap.call(context, record);
    },
    onScrollRequired: function(value) {
        var scrollable = this.getPanel().getScrollable();
        var scroller = scrollable && scrollable.getScroller();
        setTimeout(function() {
            scroller.scrollTo(0, value, {duration: 200});
        }, 200);
    }
});
