/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.RelationshipType', {
    extend: 'Ext.app.Controller',
    requires: [
        'MobileUI.core.relations.RelationsUtils',
        'MobileUI.core.SuiteManager',
        'MobileUI.view.dialogs.RelationshipType',
        'MobileUI.components.list.model.EListModel',
        'MobileUI.core.relations.RelationsManager'
    ],
    mixins: {
        backButton: 'MobileUI.controller.dialogs.MBackButton'
    },
    config: {
        refs: {
            cancelButton: 'button[name=close-relation-type-dialog]',
            list: 'elist[name=relationshiptypedialog-list]',
            fakeButton: 'component[name=relationshiptypedialog-placeholder]'
        },
        control: {
            cancelButton: {
                tap: 'onCancelTap'
            },
            list: {
                selectItem: 'onRelationshipTypeSelect'
            }
        },
        views: [
            'MobileUI.view.dialogs.RelationshipType'
        ]
    },
    statics: {
        /**
         * Method returns entity type by relation type
         * @param relationType {Object} relation type
         * @param isInRelation {Boolean} true if it is in relation or false if out relation
         * @returns {*}
         */
        getEntityType: function (relationType, isInRelation, content)
        {
            var entityTypes, entityType,
                md = MobileUI.core.Metadata,
                addParents = function (entityTypes)
                {
                    var length = entityTypes.length;
                    for (var i = 0; i < length; i++)
                    {
                        if (entityTypes[i].extendsTypeURI)
                        {
                            entityTypes.push(md.getEntityType(entityTypes[i].extendsTypeURI));
                        }
                    }
                    return entityTypes.length > length;
                },
                filter = function (entityTypes)
                {
                    var i, filterEntityType = function (item)
                    {
                        return item === this || !md.isEntityInstanceOf(item.uri, this.uri);
                    };
                    if (entityTypes.length > 1)
                    {
                        for (i = 0; i < entityTypes.length; i++)
                        {
                            entityTypes = entityTypes.filter(filterEntityType, entityTypes[i]);
                        }

                        if (entityTypes.length > 1)
                        {
                            if (addParents(entityTypes))
                            {
                                return filter(entityTypes);
                            }
                        }
                    }

                    return entityTypes[0];
                };
            if (!Ext.isEmpty(content.entityTypes))
            {
                entityTypes = content.entityTypes.map(function (uri)
                {
                    return md.getEntityType(uri);
                });

                entityType = filter(entityTypes);
            }
            else
            {
                var dirObject = isInRelation ? relationType.endObject : relationType.startObject;
                entityType = md.getEntityType(dirObject.objectTypeURI);
            }
            return entityType;
        }
    },
    /**@private*/
    content: null,
    /**@private*/
    baseConfig: null,
    /**@private*/
    allowOnlyCreateNew: false,
    /**
     * On relationship type select listener
     * @param record {MobileUI.components.list.model.EListModel} record
     */
    onRelationshipTypeSelect: function (record)
    {
        var userData = record.get('userData'),
            direction = userData.isInRelation ? MobileUI.core.relations.RelationsUtils.direction.IN
                : MobileUI.core.relations.RelationsUtils.direction.OUT;
        this.openChooseEntityDialog(this.content, userData.relationType.uri, direction);
    },
    /**
     * Method fills the list
     * @param items {Array} relationship types array
     */
    fillList: function (items)
    {
        var data,
            store = this.getList().getStore();
        store.clearData();
        data = items.map(function (item)
        {
            return {
                label: item.label,
                type: MobileUI.components.list.items.BaseListItem.types.facetItem,
                userData: {
                    isInRelation: item.isInRelation,
                    relationType: item.relationType
                }
            };
        });
        store.add(data);
    },
    /**
     * Method shows the dialog
     * @param content
     * @param options
     */
    showRelationshipTypeDialog: function (content, options)
    {
        var RelationsUtils = MobileUI.core.relations.RelationsUtils,
            typeObject, type;

        MobileUI.core.SuiteManager.hideDialogs(true);
        MobileUI.core.relations.RelationsManager.setState({});
        if (!Ext.isEmpty(content))
        {
            this.baseConfig = options;
            this.content = content;
            this.allowOnlyCreateNew = options.allowOnlyCreateNew;
            typeObject = RelationsUtils.getContentRelationsType(content);
            type = typeObject.result;
            if (type === RelationsUtils.MULTIPLE_RELATIONS)
            {
                if (!MobileUI.core.SuiteManager.hasDialog('relationshipType'))
                {
                    MobileUI.core.SuiteManager.addDialog('relationshipType', Ext.create('MobileUI.view.dialogs.RelationshipType'));
                }
                MobileUI.core.SuiteManager.showDialog('relationshipType');
                MobileUI.core.SuiteManager.clearState();
                MobileUI.core.SuiteManager.putState('relationshipType', {
                    dialog: 'relationshipType',
                    controller: 'dialogs.RelationshipType',
                    method: 'showRelationshipTypeDialog',
                    parameters: [content, options]
                });
                this.fillList(RelationsUtils.createRelationTypeSelectModel(content).items);
            }
            else if (type === RelationsUtils.SINGLE_IN_RELATION)
            {
                this.openChooseEntityDialog(this.content, typeObject.inRelations[0], MobileUI.core.relations.RelationsUtils.direction.IN);

            }
            else if (type === RelationsUtils.SINGLE_OUT_RELATION)
            {
                this.openChooseEntityDialog(this.content, typeObject.outRelations[0], MobileUI.core.relations.RelationsUtils.direction.OUT);
            }
            else
            {
                MobileUI.core.Logger.warn('The relation configuration is not correct');
            }
        }
    },
    /**
     * Method opens next dialog
     * @param relationsConfigContent {Object} contains the configuration content (config.content)
     * @param relationTypeUri {String} relationship type uri
     * @param direction {String} direction (in/out)
     */
    openChooseEntityDialog: function (relationsConfigContent, relationTypeUri, direction)
    {
        relationTypeUri = Ext.isObject(relationTypeUri) ? relationTypeUri.uri : relationTypeUri;
        var relationType = MobileUI.core.Metadata.getRelationType(relationTypeUri),
            entityType, state, isInRelation, attributes, entityTypes, hiddenAttributes;
        isInRelation = direction === MobileUI.core.relations.RelationsUtils.direction.IN;
        entityType = MobileUI.controller.dialogs.RelationshipType.getEntityType(relationType, isInRelation, this.content);
        hiddenAttributes = this.baseConfig.hiddenRelationAttributes || [];
        attributes = (MobileUI.core.Metadata.getRelationType(relationTypeUri).attributes || []).concat([
            MobileUI.core.relations.RelationsManager.ATTR_START_DATE,
            MobileUI.core.relations.RelationsManager.ATTR_END_DATE
        ]);
        attributes = attributes.filter(function (item)
        {
            return !item.hidden && hiddenAttributes.indexOf(item.uri) === -1;
        });
        state = {
            relationTypeChanged: relationTypeUri,
            isInRelation: isInRelation,
            attributes: attributes.map(function(item){
                return {attrType: item, uri: item.uri, parentUri: 'relation'};
            }),
            config: relationsConfigContent
        };
        MobileUI.core.relations.RelationsManager.setState(state);

        if (this.allowOnlyCreateNew)
        {
            MobileUI.controller.dialogs.ChooseEntity.showCreateEntity(this.getApplication(), entityType, null,  this.baseConfig);
        }
        else
        {
            if (!MobileUI.core.SuiteManager.hasDialog('chooseEntity'))
            {
                MobileUI.core.SuiteManager.addDialog('chooseEntity', Ext.create('MobileUI.view.dialogs.ChooseEntity'));
            }
            MobileUI.core.SuiteManager.showDialog('chooseEntity');
            if (relationsConfigContent.entityTypes)
            {
                entityTypes = relationsConfigContent.entityTypes.map(function (uri) {
                    return MobileUI.core.Metadata.getEntityType(uri);
                });
            }
            this.getApplication().getController('dialogs.ChooseEntity').setEntityTypeFilter(entityType,entityTypes, false, this.baseConfig);
        }
    },

    /**
     * @protected
     * on cancel tap listener
     */
    onCancelTap: function ()
    {
        MobileUI.core.SuiteManager.hideDialogs(true);
    }
});
