/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.SavedSearches', {
    extend: 'Ext.app.Controller',
    mixins: {
        savedSearches: 'MobileUI.controller.MSavedSearches',
        menuToolbarButton: 'MobileUI.controller.MMenuToolbarButton'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.core.Navigation',
        'MobileUI.controller.dialogs.EditSavedSearch'
    ],
    config: {
        refs: {
            backButton: 'button[name=savedsearches-backButton]',
            searchesList: 'elist[name=savedsearches-list]',
            hint: 'component[name=savedsearches-hint]',
            menuButton: 'button[name=menuBtn]'
        },
        control: {
            menuButton: {
                tap: 'onMenuButtonTap'
            },
            backButton: {
                tap: 'onBackButtonTap'
            },
            searchesList: {
                selectItem: 'onSearchesListSelect',
                'delete': 'onSearchesListDelete',
                addToGlobalFilter: 'onAddToGlobalFilter',
                removeFromGlobalFilter: 'onRemoveFromGlobalFilter',
                editSearch: 'onSearchesListEdit',
                fetch: 'onListFetch'
            },
            savedsearchesdialog: {
                show: 'onShow'
            }
        },
        views: [
            'MobileUI.view.dialogs.SavedSearches'
        ]
    },

    statics: {
        show: function() {
            if (!MobileUI.core.SuiteManager.hasDialog('savedsearchesdialog')) {
                MobileUI.core.SuiteManager.addDialog('savedsearchesdialog', Ext.create('MobileUI.view.dialogs.SavedSearches'));
            }
            MobileUI.core.SuiteManager.showDialog('savedsearchesdialog');
        },

        /**
         * Method return is add new saved search allow or not
         * @return {Boolean}
         */
        isAddNewSearchAllowed: function() {
            var searchManager = MobileUI.core.search.SearchParametersManager;
            var isEmpty = true,
                facets = searchManager.getFacets();
            for (var key in facets)
            {
                if (facets.hasOwnProperty(key))
                {
                    if (!MobileUI.controller.dialogs.SavedSearches.isFacetEmpty(facets[key], key))
                    {
                        isEmpty = false;
                    }
                }
            }
            return !(isEmpty &&
            Ext.isEmpty(searchManager.getAdvancedSearchParameters()) &&
            Ext.isEmpty(searchManager.getKeyword()));
        },
        /**
         * Method return is facet empty or not
         * @param facet {Object} facet object
         * @param key {String} key
         * @returns {boolean}
         */
        isFacetEmpty: function(facet, key){
            var result = true;
            if (facet)
            {
                if (key === MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME) {
                    var entityTypes = Object.keys(MobileUI.core.util.Util.filter(MobileUI.core.Metadata.getEntityTypes(), function (item) {
                        return item.searchable !== false && !item['abstract'];
                    }));
                    result =  Ext.Array.difference(entityTypes, facet.values).length === 0;
                } else {
                    result = Ext.isEmpty(facet.values);
                }
            }
            return result;
        }
    },

    listOffset: 0,
    listPageSize: 20,

    onBackButtonTap: function() {
        MobileUI.core.SuiteManager.hideDialogs(true);
    },

    /**
     * @private
     */
    onAdd: function() {
        MobileUI.controller.dialogs.EditSavedSearch.show();
    },

    /**
     * @private
     */
    onSearchesListDelete: function(record) {
        this.deleteSavedSearch(record.get('userData').uri, this.refreshList, this);

        this.updateHint();
    },

    /**
     * @private
     */
    onSearchesListSelect: function(record) {
        var userData = record.get('userData');
        if (userData && userData.uri) {
            this.showSavedSearch(userData.uri);
        }
        else {
            this.onAdd();
        }
    },

    onAddToGlobalFilter: function(record) {
        var userData = record.get('userData');
        MobileUI.core.search.SearchParametersManager.addGlobalFilterUrl(userData.uri);
        var list = this.getSearchesList();
        var index = list.getStore().findExact('userData', userData);
        var item = list.getItemAt(index);
        userData.isGlobalFilter = true;
        item.recreate();

        this.updateHint();
    },

    onRemoveFromGlobalFilter: function(record) {
        var userData = record.get('userData');
        MobileUI.core.search.SearchParametersManager.removeGlobalFilterUrl(userData.uri);
        var list = this.getSearchesList();
        var index = list.getStore().findExact('userData', userData);
        var item = list.getItemAt(index);
        userData.isGlobalFilter = false;
        item.recreate();

        this.updateHint();
    },

    /**
     * @private
     */
    onSearchesListEdit: function(record) {
        MobileUI.controller.dialogs.EditSavedSearch.show(record.get('userData').uri);
        this.updateHint();
    },

    /**
     * @private
     */
    onShow: function() {
        this.refreshList();
        var items = MobileUI.core.util.Util.filter(MobileUI.core.Metadata.getEntityTypes(), function (item) {
            return item.searchable !== false && !item['abstract'];
        });
        this.entityTypes = Object.keys(items);
        if (window.localStorage.getItem('saved-searches-hint') < 2) {
            this.getHint().setHidden(false);
        }
    },

    updateHint: function() {
        window.localStorage.setItem('saved-searches-hint',
            parseInt((window.localStorage.getItem('saved-searches-hint') || 0), 10) + 1);
    },

    /**
     * @private
     */
    refreshList: function() {
        this.listOffset = 0;
        var list = this.getSearchesList(),
            store = list.getStore();
        store.clearData();
        this.requestSearches();

        this.getSearchesList().setMasked(true);
    },

    requestSearches: function() {
        MobileUI.core.session.Session.findSavedSearches(this.listPageSize, this.listOffset, {
                countResults: true,
                findShared: true,
                sortOrder: 'ASC',
                sortBy: 'NAME',
                favoriteOnly: false
            },
            function(data) {
                var list = this.getSearchesList(),
                    store = list.getStore();

                if (MobileUI.controller.dialogs.SavedSearches.isAddNewSearchAllowed() && (this.listOffset === 0)) {
                    data = [{
                        label: 'Add Saved Search',
                        disableSwipe: true,
                        type: MobileUI.components.list.items.BaseListItem.types.facetItem,
                        avatar: 'resources/images/reltio/relations/add-member.svg'
                    }].concat(data);
                }

                store.add(data);
                if (this.listOffset === 0) {
                    list.setMasked(false);
                }
                else {
                    list.fetchDone(data.length === this.listPageSize);
                }
                this.listOffset += this.listPageSize;

                var totals = list.element.query('.rl-ss-total-column');
                var maxWidth = totals.reduce(function(max, item) {
                    return Math.max(max, item.offsetWidth);
                }, 0) + 1;
                maxWidth = '; width:' + maxWidth + 'px !important;';
                totals.forEach(function(item) {
                    item.style.cssText += maxWidth;
                });
            },
            function(error) {
                MobileUI.core.Logger.log(error);
            }, this);
    },

    onListFetch: function() {
        this.requestSearches();
    }
});
