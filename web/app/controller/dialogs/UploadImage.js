/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.UploadImage', {
    extend: 'Ext.app.Controller',
    mixins: {
        'back': 'MobileUI.controller.dialogs.MBackButton'
    },
    requires: [
      'MobileUI.components.MessageBox'
    ],
    config: {
        refs: {
            cancelButton: 'button[name=close-upload-image]',
            fakeButton: 'component[name=uploadimagedialog-placeholder]',
            backButton: 'component[name=uploadimagedialog-backToolbarBtn]',
            urlText: 'textfield[name=upload-image-url]',
            takePhotoButton: 'fileupload[name=upload-image-take-photo]',
            saveButton: 'button[name=upload-image-save]',
            panel: 'panel[name=upload-image]'

        },
        control: {
            cancelButton: {
                tap: 'cancelTap'
            },
            backButton: {
                tap: 'onBackTap'
            },
            saveButton: {
                tap: 'onSaveTap'
            },
            takePhotoButton: {
                beforeUpload: 'onTakePhotoButtonTap',
                uploaded: 'onImageUploaded',
                failed: 'onUploadFailed'
            }
        },
        views: [
            'MobileUI.view.dialogs.UploadImage'
        ]
    },
    initUploader: function(){
        if (!this.isOnBackAction())
        {
            MobileUI.core.SuiteManager.putState('uploadImage', {
                dialog: 'uploadImage',
                controller: 'dialogs.UploadImage',
                method: 'initUploader',
                parameters: []
            });
        }
        this.getUrlText().setValue('');
    },
    onTakePhotoButtonTap: function () {
        this.getPanel().setMasked({
            xtype: 'loadmaskex',
            message: '',
            cls: 'opaquemask',
            indicator: true
        });
    },
    onSaveTap: function(){
        if (Ext.isEmpty(this.getUrlText().getValue())){
            MobileUI.components.MessageBox.alert(i18n('Warning'), i18n('Please, choose an image'));
        } else {
            this.fireEvent('imageSelected', this.getUrlText().getValue());
            this.onBackTap();
        }
    },
    onBackTap: function(){
        MobileUI.core.SuiteManager.popState();
        this.processState(MobileUI.core.SuiteManager.getCurrentState() && MobileUI.core.SuiteManager.getCurrentState().state);
    },
    onImageUploaded: function (resp) {
        this.getPanel().setMasked(null);
        resp = Ext.isEmpty(resp) ? '' : resp.url;
        this.getUrlText().setValue(resp);
    },
    onUploadFailed: function () {
        this.getPanel().setMasked(null);
    }
});
