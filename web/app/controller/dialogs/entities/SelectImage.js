/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.dialogs.entities.SelectImage', {
    extend: 'Ext.app.Controller',
    mixins: {
        height: 'MobileUI.components.list.MListHeight',
        listModel: 'MobileUI.components.list.MListModel',
        back: 'MobileUI.controller.dialogs.MBackButton',
        editable: 'MobileUI.controller.MEditableProfile'
    },
    config: {
        refs: {
            list: 'elist[name=select-image-list]',
            cancelButton: 'button[name=close-select-image]',
            fakeButton: 'component[name=selectimagedialog-placeholder]',
            backButton: 'component[name=selectimagedialog-backToolbarBtn]'
        },
        control: {
            cancelButton: {
                tap: 'closeDialog'
            },
            backButton: {
                tap: 'backButton'
            },
            saveButton: {
                tap: 'onSaveTap'
            },
            list: {
                recalculate: 'recalculateHeightDelayed',
                'delete': 'onItemDelete',
                'selectItem': 'onItemSelect'
            }
        },
        views: [
            'MobileUI.view.dialogs.entities.SelectImage'
        ]
    },
    statics: {
        ADD_ITEM_URI: 'add_item'
    },

    extractName: function (item) {
        var name = item,
            position = item.lastIndexOf('/');
        if (position !== -1) {
            name = item.substring(position + 1);
        }
        return name;
    },
    extractFrom: function (item) {
        var source = 'Reltio';
        if (item.indexOf(MobileUI.core.Services.getGeneralSettings().imagePath) !== 0) {
            source = MobileUI.core.util.Util.splitUri(item).host || source;
        }
        return i18n('From') + ' ' + source;
    },
    fillDate: function (imageAttributeUri, entity) {
        var crosswalk = (entity.crosswalks || []).filter(function (crosswalk) {
            return crosswalk.attributes.some(function (attributeUri) {
                return attributeUri === imageAttributeUri;
            });
        });
        var date = !Ext.isEmpty(crosswalk) ? crosswalk[0].createDate : null;
        return Ext.Date.parseDate(date, 'c');
    },
    extractFields: function (image) {
        if (image.indexOf('data:') === 0) {
            var regex = /^data:.+\/(.+);base64,(.*)$/,
                matches = image.match(regex),
                ext = matches[1];
            return {
                label: ext,
                secondaryLabel: ''
            };
        } else {
            return {
                label: this.extractName(image),
                secondaryLabel: this.extractFrom(image)
            };
        }
    },
    showImages: function (entityUri, dontSave) {
        if (!dontSave && !this.isOnBackAction()) {
            MobileUI.core.SuiteManager.putState('selectImage', {
                dialog: 'selectImage',
                controller: 'dialogs.entities.SelectImage',
                method: 'showImages',
                parameters: [entityUri]
            });
        }
        var entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri, 'selectImage');
        if (!entity) {
            entity = MobileUI.core.entity.TemporaryEntity.saveState(MobileUI.core.entity.TemporaryEntity.getEntity(entityUri), 'selectImage');
        }
        this.entity = entity;
        var images = MobileUI.core.entity.EntityUtils.getImageSources(this.entity);
        var defaultProfilePic = this.entity.defaultProfilePic || (Ext.isObject(this.entity.original) ? this.entity.original.defaultProfilePic : '');

        images = images.map(function (image) {
            var info = this.extractFields(image.value);
            image.label = info.label;
            image.secondaryLabel = info.secondaryLabel;
            image.thirdLabel = i18n(this.fillDate(image.uri, this.entity.original), MobileUI.core.I18n.DATE_LONG);
            image.avatar = image.value;
            image.state = defaultProfilePic === image.uri ? 'selected' : '';
            return image;
        }, this);

        this.entityUri = entityUri;
        this.images = images;
        var list = this.getList(),
            store = list.getStore(),
            model;
        store.clearData();
        store.add([{
            label: i18n('Add image'),
            actionLabel: MobileUI.controller.dialogs.entities.SelectImage.ADD_ITEM_URI,
            disableSwipe: true,
            type: MobileUI.components.list.items.BaseListItem.types.facetItem,
            avatar: 'resources/images/reltio/relations/add-member.svg'
        }]);
        model = images.map(function (item) {
            item.type = MobileUI.components.list.items.BaseListItem.types.selectImage;
            return item;
        });
        store.add(model);
        list.refresh();
        this.updateListHeight(list);
    },
    onItemSelect: function (record) {
        if (record.get('actionLabel') === MobileUI.controller.dialogs.entities.SelectImage.ADD_ITEM_URI) {
            if (!MobileUI.core.SuiteManager.hasDialog('uploadImage')) {
                MobileUI.core.SuiteManager.addDialog('uploadImage', Ext.create('MobileUI.view.dialogs.UploadImage'));
            }
            MobileUI.core.SuiteManager.showDialog('uploadImage');
            var controller = this.getApplication().getController('dialogs.UploadImage');
            controller.initUploader();
            controller.on('imageSelected', function (uri) {
                this.onImageUploaded(uri);
            }, this, {single: true});
        } else {
            var list = this.getList();
            var uri = record.get('uri');
            list.getStore().each(function (innerRecord) {
                if (innerRecord.get('uri') === uri) {
                    innerRecord.set('state', 'selected');
                } else {
                    innerRecord.set('state', '');
                }
            });
            this.entity.defaultProfilePic = uri;
        }
    },

    onImageUploaded: function (uri) {
        var entityType = MobileUI.core.Metadata.getEntityType(this.entity.type),
            i, attrType;
        for (i = 0; i < entityType.attributes.length; i++) {
            attrType = entityType.attributes[i];
            if (MobileUI.core.entity.EntityUtils.isImageAttribute(entityType, attrType)) {
                break;
            }
        }
        var attributeUri = this.entity.uri + '/attributes';
        var attribute = {
            name: attrType.name,
            ov: true,
            parentUri: this.entity.uri,
            attrType: attrType,
            type: attrType.uri,
            uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(attributeUri, attrType),
            value: uri
        };
        this.entity.addedAttributes.push(attribute);
        this.entity.attributes.push(attribute);
        this.entity.defaultProfilePic = attribute.uri;
        this.showImages(this.entityUri, true);
    },
    backButton: function () {
        var currentState = MobileUI.core.entity.TemporaryEntity.popState(this.entityUri);
        MobileUI.core.entity.TemporaryEntity.updateState(currentState, this.entityUri);
        this.closeDialog();
    },
    closeDialog: function () {
        MobileUI.core.SuiteManager.hideDialogs(true);
    },
    onItemDelete: function(record){
        this.deleteAttributeInternal(this.entity, record.get('uri'), {type: 'ImageUrl'}, 'delete');
        this.showImages(this.entityUri, true);
        if (record.get('uri') === this.entity.defaultProfilePic){
            this.entity.defaultProfilePic = null;
        }
    }
});
