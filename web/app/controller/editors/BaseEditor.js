/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.BaseEditor', {
    extend: 'Ext.app.Controller',
    requires: ['MobileUI.core.Logger'],
    config: {
        control: {
            cancelBtn: {
                tap: 'onCancel'
            },
            doneBtn: {
                tap: 'onDone'
            },
            backButton: {
                tap: 'onBackButton'
            }
        }
    },

    /** @protected */
    valueChangedHandler: null,
    /** @private */
    valueChangedContext: null,
    /** @private */
    valueType: '',

    /** @protected */
    onDone: function() {
        MobileUI.core.SuiteManager.popDialogHistory();
        MobileUI.components.ListItemEditor.processState(MobileUI.core.SuiteManager.getCurrentState());
        if (this.valueChangedHandler) {
            this.valueChangedHandler.call(this.valueChangedContext, this.getValue());
        }
    },

    /** @protected */
    onCancel: function() {
        MobileUI.core.SuiteManager.hideDialogs(false, function() {
            if (this.valueChangedHandler) {
                this.valueChangedHandler.call(this.valueChangedContext, this.getValue(), true);
            }
        }, this);
    },

    setTitleText: function(text) {
        this.getTitle().setHtml(Ext.util.Format.htmlEncode(i18n(text)));
    },

    /**
     * @param handler {Function}
     * @param context {Object?}
     */
    setValueChangedHandler: function(handler, context) {
        this.valueChangedHandler = handler;
        this.valueChangedContext = context;
    },

    setValueType: function(valueType) {
        this.valueType = valueType;
    },

    onInputChanged: function() {
        if (this.getValue) {
            var value = this.getValue();
            var valid = MobileUI.core.util.Validator.isValid(value, this.valueType);
            this.setValid(valid);
        }
    },

    setValid: function(valid) {
        if (this.getInvalidMarker) {
            var marker = this.getInvalidMarker();
            if (marker) {
                marker.setHidden(valid);
                this.getDoneBtn().setHidden(!valid);
            }
        }
    },

    onClear: function() {
        var editor = this;
        setTimeout(function() {
            editor.onInputChanged();
        }, 10);
    },

    onBackButton: function() {
        MobileUI.core.SuiteManager.back();
    }
});
