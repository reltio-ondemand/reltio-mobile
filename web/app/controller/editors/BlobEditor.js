/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.BlobEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    requires: ['MobileUI.core.Logger'],
    config: {
        refs: {
            title: 'component[name=blobeditor-title]',
            cancelBtn: 'button[name=blobeditor-cancelBtn]',
            doneBtn: 'button[name=blobeditor-doneBtn]',
            field: 'textfield[name=blobeditor-field]',
            backButton: 'component[name=blobeditor-backButton]'
        },
        views: ['MobileUI.view.editors.BlobEditor']
    },

    getValue: function() {
        return this.getField().getValue();
    },

    setValue: function(value) {
        this.getField().setValue(value);
    }
});
