/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.BoolEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    requires: ['MobileUI.core.Logger'],
    config: {
        refs: {
            title: 'component[name=booleditor-title]',
            cancelBtn: 'button[name=booleditor-cancelBtn]',
            doneBtn: 'button[name=booleditor-doneBtn]',
            field: 'tripletoggle[name=booleditor-field]',
            backButton: 'component[name=booleditor-backButton]'
        },
        views: ['MobileUI.view.editors.BoolEditor']
    },

    getValue: function() {
        return this.getField().getValue();
    },

    setValue: function(value) {
        this.getField().setValue(value);
    }
});
