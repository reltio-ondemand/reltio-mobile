/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.EnumEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    mixins: {
        listEditor: 'MobileUI.controller.editors.MListEditor'
    },
    config: {
        refs: {
            title: 'component[name=enumeditor-title]',
            cancelBtn: 'button[name=enumeditor-cancelBtn]',
            doneBtn: 'button[name=enumeditor-doneBtn]',
            searchField: 'textfield[name=enumeditor-search]',
            list: 'elist[name=enumeditor-list]',
            panel: 'enumeditor',
            backButton: 'component[name=enumeditor-backButton]'
        },
        control: {
            list: {
                selectItem: 'onSearchItemTap',
                painted: 'onListPainted'
            },
            searchField: {
                keyup: 'onKeyUp',
                clearicontap: 'onClearIcon'
            },
            panel: {
                initialize: 'onPanelInitialize'
            }
        },
        views: ['MobileUI.view.editors.EnumEditor']
    },

    /** @private */
    data: [],

    getData: function(callback, context) {
        callback.call(context, this.data);
    },

    getRecordValue: function(record) {
        return record.get('userData').value;
    },

    getDisplayName: function(item) {
        return i18n(item);
    },

    isItemSelected: function(item, selected) {
        return item === selected;
    },

    onPanelInitialize: function() {
        this.fillList();
    },

    /**
     * @param values {Array<String>}
     */
    setValues: function(values) {
        this.getSearchField().setValue('');
        this.data = values;
        this.cache = null;
        this.fillList();
    }
});
