/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.LookupEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    requires: ['MobileUI.core.Logger', 'MobileUI.core.Lookups'],
    mixins: {
        listEditor: 'MobileUI.controller.editors.MListEditor'
    },
    config: {
        refs: {
            title: 'component[name=lookupeditor-title]',
            cancelBtn: 'button[name=lookupeditor-cancelBtn]',
            doneBtn: 'button[name=lookupeditor-doneBtn]',
            searchField: 'textfield[name=lookupeditor-search]',
            list: 'elist[name=lookupeditor-list]',
            backButton: 'component[name=lookupeditor-backButton]'
        },
        control: {
            list: {
                selectItem: 'onSearchItemTap',
                painted: 'onListPainted'
            },
            searchField: {
                keyup: 'onKeyUp',
                clearicontap: 'onClearIcon'
            }
        },
        views: ['MobileUI.view.editors.LookupEditor']
    },

    lookupCode: null,

    getData: function(callback, context) {
        if (!this.lookupCode) {
            callback.call(context, []);
        }
        else {
            var data = MobileUI.core.Lookups.getLookups(this.lookupCode);
            //remove children
            var result = {};
            Ext.Object.each((data.raw || {}), function(mainKey, item)
            {
                var key, obj = {};
                for (key in item){
                    if (item.hasOwnProperty(key))
                    {
                        if (key !== 'parent')
                        {
                            obj[key] = item[key];
                        }
                    }
                }
                result[mainKey] = obj;
            });
            callback.call(context, result);
        }
    },

    getDisplayName: function(item) {
        return i18n(item.displayName);
    },

    getRecordValue: function(record) {
        return record.get('userData').value.name;
    },

    isItemSelected: function(item, selected) {
        return item.name === selected;
    },

    /**
     * @param editorInfo
     */
    setInfo: function(editorInfo) {
        this.getSearchField().setValue('');
        this.lookupCode = editorInfo.lookupCode;
        this.cache = null;
        this.fillList();
    }
});
