/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.MListEditor', {

    /** @private */
    cache: null,

    /** @private */
    value: null,

    /** @private */
    label: null,
    /** @private */
    selectedItem: null,

    getValue: function() {
        return this.value;
    },

    getLocalizeLabel: function() {
        return this.label;
    },

    setValue: function(value) {
        this.value = value;
        this.label = null;
        this.fillList();
    },

    onSearchItemTap: function(data) {
        if (this.selectedItem && this.selectedItem.getRecord()) {
            this.selectedItem.getRecord().set('selected', false);
            this.selectedItem.recreate();
        }
        this.value = this.getRecordValue(data);
        this.label = this.getLabelValue(data);
        data.set('selected', true);
        data.item.recreate();
        this.selectedItem = data.item;

        if (this.getSelectImmediate) {
            this.onDone();
        }
    },

    fillList: function(filter) {
        if (!this.cache) {
            this.getData(function(data) {
                this.cache = data;
                this.fillList(filter);
            }, this);
        }
        else {
            var list = this.getList(),
                store = list.getStore();
            store.clearData();
            var filtered = Object.keys(this.cache).map(function(name) {
                return this.cache[name];
            }, this);
            if (filter) {
                var lowerFilter = filter.toLowerCase();
                filtered = filtered.filter(function(record) {
                    return this.getDisplayName(record).toLowerCase().indexOf(lowerFilter) !== -1;
                }, this);
            }
            store.add(filtered.map(function(item) {
                return {
                    label: this.getDisplayName(item),
                    userData: {value: item},
                    type: MobileUI.components.list.items.BaseListItem.types.selectable,
                    selected: this.isItemSelected(item, this.getValue()),
                    required: this.isRequired && this.isRequired(item)
                };
            }, this).sort(function(a,b){
                if (a.label > b.label) {
                    return 1;
                }
                if (a.label < b.label) {
                    return -1;
                }
                return 0;
            }));
            list.refresh();
            var items = list.getViewItems(),
                i;
            this.selectedItem = null;
            for (i = 0; i < items.length; i++) {
                if (items[i].getRecord().get('selected')) {
                    this.selectedItem = items[i];
                    break;
                }
            }
        }
    },

    /**
     * @protected
     * Search field listener
     */
    onListPainted: function() {
        this.fillList();
    },
    getLabelValue: function() {
        return null;
    },

    /**
     * @protected
     * search field keyup listner
     */
    onKeyUp: function() {
        this.fillList(this.getSearchField().getValue());
    },

    onClearIcon: function() {
        this.getSearchField().focus();
    }
});
