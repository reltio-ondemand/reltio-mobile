/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.StringEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    requires: ['MobileUI.core.Logger'],
    config: {
        refs: {
            title: 'component[name=stringeditor-title]',
            cancelBtn: 'button[name=stringeditor-cancelBtn]',
            doneBtn: 'button[name=stringeditor-doneBtn]',
            field: 'textfield[name=stringeditor-field]',
            invalidMarker: 'component[name=stringeditor-invalidMarker]',
            backButton: 'component[name=stringeditor-backButton]'
        },
        control: {
            field: {
                keyup: 'onInputChanged',
                initialize: 'onInputChanged',
                clearicontap: 'onClear'
            }
        },
        views: ['MobileUI.view.editors.StringEditor']
    },

    getValue: function() {
        return this.getField().getValue();
    },

    setValue: function(value) {
        this.getField().setValue(value);
    }
});
