/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.controller.editors.TypeaheadEditor', {
    extend: 'MobileUI.controller.editors.BaseEditor',
    mixins: {
        listEditor: 'MobileUI.controller.editors.MListEditor'
    },
    requires: [
        'MobileUI.core.session.Session',
        'MobileUI.core.Logger',
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.controller.MBaseSearch',
        'MobileUI.core.Navigation'
    ],
    config: {
        refs: {
            searchField: 'textfield[name=searchFacetItem]',
            cancelBtn: 'button[name=typeaheadeditor-cancelButton]',
            list: 'elist[name=typeaheadeditor-list]',
            title: 'component[name=typeaheadeditor-title]',
            doneBtn: 'button[name=typeaheadeditor-doneBtn]'

        },
        control: {
            searchField: {
                keyup: 'onSearchTextfieldKeyUp',
                clearicontap: 'onSearchTextfieldClearButtonTap'
            },
            cancelButton: {
                tap: 'onCancelButtonTap'
            },
            list: {
                selectItem: 'onSearchItemTap'
            }
        },
        views: [
            'MobileUI.view.editors.TypeaheadEditor'
        ]
    },
    fieldName: null,
    getSelectImmediate: true,

    setSearchFilter: function(filter){
        this.searchFilter =  filter;
    },
    getSearchFilter: function(){
        return this.searchFilter || Ext.emptyFn;
    },
    launch: function(){
        this.setSearchFilter(function(item, searchText, searchWords)
        {
            return item.split(/[^\w]+/).some(function(word) {
                return searchWords.some(function(searchWord) {
                    return word.indexOf(searchWord) === 0;
                });
            });
        });
    },
    /**
     * @private
     */
    onCancel: function() {
        MobileUI.core.SuiteManager.hideDialogs(true);
    },
    setFieldName: function(fieldName)
    {
        if (this.getList()){
            this.getList().getStore().removeAll();
        }
        this.fieldName = fieldName;
    },
    setTitleLabel: function(fieldName)
    {
        this.getTitle().setHtml(Ext.util.Format.htmlEncode(fieldName));
    },
    /**
     * @private
     */
    onSearchTextfieldKeyUp: function() {
        this.value = '';
        this.doFilter(this.getSearchField().getValue(),this.getList());
    },
    fillList: function (text)
    {
        this.doFilter(text, this.getList());
    },
    /**
     * @protected
     * @param {String} text
     * @param list {elist} list component
     */
    doFilter: function (text, list)
    {
        var store;
        if (list)
        {
            store = list.getStore();
            if (text)
            {
                MobileUI.core.Logger.info('Try to get all '+ this.fieldName + ' ' + text);
                MobileUI.core.session.Session.findFacet(this.fieldName, [text],
                    function (resp)
                    {
                        var lowerFilter = text.toLocaleLowerCase(),
                            searchWords = lowerFilter.split(/[^\w]+/)
                                .filter(function (searchWord) {
                                    return searchWord.trim() !== '';
                                }),
                            that = this, data;
                        data = MobileUI.core.Services.getSearchResultBuilder().
                            buildFacet(resp, that.fieldName).filter(function (facet) {
                                var result = facet.label;
                                if (result) {
                                    result = result.toLowerCase();
                                }
                                return this.getSearchFilter()(result, lowerFilter, searchWords);
                            },this);
                        store.clearData();
                        store.add(data);
                    },
                    function (resp)
                    {
                        MobileUI.core.Logger.error(resp);
                    }, this);
            }
            else
            {
                store.clearData();
                list.refresh();
            }
        }
    },
    getRecordValue: function(record) {
        return record.get('label');
    },

    /**
     * @private
     */
    onSearchTextfieldClearButtonTap: function() {
        this.getSearchField().focus();
    },

    getValue: function() {
        return this.value || this.getSearchField().getValue();
    },

    setValue: function(value) {
        this.value = value;
        this.getSearchField().setValue(value);
        this.fillList(value);
    }
});
