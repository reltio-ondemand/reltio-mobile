/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.CardinalityChecker', {
    requires: ['MobileUI.core.entity.EntityUtils', 'Ext.mixin.Observable'],
    mixins: {observable: 'Ext.mixin.Observable'},

    constructor: function (config) {
        this.initConfig(config);
    },
    statics: {
        isRequired: function (attrType) {
            return attrType && (attrType.required ||
                attrType.cardinality && attrType.cardinality.minValue > 0);
        },

        getMessage: function(attrType){
            var cardinality = attrType.cardinality;
            if (cardinality){
                if (cardinality.maxValue || cardinality.minValue !== 1){
                    if (cardinality.minValue && cardinality.maxValue){
                        if (cardinality.maxValue !== cardinality.minValue){
                            return i18n('This attribute can have minimum %1 value and maximum %2 values', cardinality.minValue, cardinality.maxValue);
                        } else {
                            return i18n('This attribute should have %1 value', cardinality.minValue);
                        }
                    } else if (cardinality.minValue){
                        return i18n('This attribute can have minimum %1 value', cardinality.minValue);

                    } else if (cardinality.maxValue){
                        return i18n('This attribute can have maximum %1 values', cardinality.maxValue);
                    }
                }
            }
            return null;
        }
    }

});
