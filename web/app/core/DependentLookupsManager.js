/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.DependentLookupsManager', {
    requires: ['MobileUI.core.entity.EntityUtils', 'Ext.mixin.Observable'],
    mixins: {observable: 'Ext.mixin.Observable'},
    constructor: function (config) {
        this.initConfig(config);
        this.cache = {};
    },
    state: null,
    prevStateName: null,
    lookupsHierarchy: null,
    editors: null,
    touched: null,
    cache: null,
    lookupAutocompleteConfiguration: null,

    makeKey: function(lookupCode, filter){
        filter = filter || '';
        return lookupCode+ ':'+filter;
    },

    getFromCache: function(lookupCode, parents, filter) {
        var key = this.makeKey(lookupCode, filter);
        var result = (this.cache[key] || []).filter(function(item){
            return Ext.Object.equals(item.parents, parents);
        });
        return result.length > 0 ? result[0].result: null;
    },

    addToCache: function(lookupCode, parents, filter, result){
        var key = this.makeKey(lookupCode, filter);
        if (!this.cache[key]){
            this.cache[key] = [];
        }
        this.cache[key].push({parents: parents, result: result});
    },

    getValues: function(lookupCode, editingUri, attrTypeUri, filter, success, failure, self){
        var parents = this.isInited() ? this.getParents(lookupCode, editingUri, attrTypeUri).reduce(function (prev, current) {
            var info = current.getInfo();
            var codeValues = (info.values || []).map(function (item) {
                return item.value;
            });
            if (!Ext.isEmpty(codeValues)) {
                var result = {
                    type: info.dependentLookupCode,
                    codeValues: codeValues
                };
                prev.push(result);
            }
            return prev;
        }, []) : [];
        var fromCache = this.getFromCache(lookupCode, parents, filter);
        if (fromCache) {
            if (success) {
                setTimeout(function(){
                    success.call(self, fromCache);
                }, 10);
            }
        } else {
            var cb = function (result) {
                this.me.addToCache(this.lookupCode, this.parents, this.filter, result);
                if (this.success) {
                    this.success.call(this.self, result);
                }
            }.bind({me: this, self: self, lookupCode: lookupCode, parents: parents, filter: filter, success: success});
            MobileUI.core.session.Session.resolveLookupValue(lookupCode, parents, filter, null, cb, failure, self);
        }
    },

    touchEditor: function(uri){
        if (!this.touched){
            this.touched = {};
        }
        if (uri){
            this.touched[uri] = true;
        }
    },
    populateValue: function(attrTypeUri, uri, value){
        var nodes = this.findAllNodes(this.getCurrentLookupsHierarchy(), function (item) {
            return item.uri === attrTypeUri;
        }).filter(function(node){
            return node && node.getInfo();
        });
        if (nodes.length > 0) {
            if (value !== null) {
                nodes.forEach(function (node) {
                    var values = node.getInfo().values;
                    var results = values
                        .filter(function (item) {
                            return item.uri === uri;
                        }, this);
                    if (results.length > 0) {
                        results.forEach(function (item) {
                            item.value = value;
                        });
                    } else {
                        values.push({value: value, uri: uri});
                    }
                }, this);
            }
            this.touchEditor(uri);
            this.fireEvent('dependentUpdated', MobileUI.core.util.Util.flatten(nodes.map(function (item) {
                return item.getChildren();
            })).map(function (item) {
                return item.getInfo() && item.getInfo().dependentLookupCode;
            }).filter(function (item) {
                return item;
            }));
        }
    },
    isSupportAutoPopulate: function (attrTypeUri) {
        if (!this.lookupAutocompleteConfiguration) {
            var cm = MobileUI.core.Services.getConfigurationManager();
            this.lookupAutocompleteConfiguration = cm.getExtensionById('com.reltio.plugins.ui.LookupAutocomplete');
        }
        if (!Ext.isEmpty(this.lookupAutocompleteConfiguration)) {
            if (this.lookupAutocompleteConfiguration.includeAttributes && Ext.isArray(this.lookupAutocompleteConfiguration.includeAttributes)) {
                return this.lookupAutocompleteConfiguration.includeAttributes.indexOf(attrTypeUri) !== -1;
            } else if (this.lookupAutocompleteConfiguration.excludeAttributes && Ext.isArray(this.lookupAutocompleteConfiguration.excludeAttributes)) {
                return this.lookupAutocompleteConfiguration.excludeAttributes.indexOf(attrTypeUri) === -1;
            }
        }
        return false;
    },

    isTouched: function(uri){
        if (Ext.isEmpty(uri)){
            return true;
        }
        if (!this.touched) {
            this.touched = {};
        }
        return this.touched[uri];
    },
    cleanTouchedList: function(){
        this.touched = {};
        this.cache = {};
    },
    initWithEntityType: function (entityTypeUri) {
        this.resetHierarchy();
        this.state = {};
        this.invalidLookupsList = [];
        this.stateKeyHistory = [];
        this.lookupsHierarchy = MobileUI.core.Metadata.getDependentLookupsHierarchy()[entityTypeUri] || {};
    },

    getCurrentLookupsHierarchy: function () {
        return this.lookupsHierarchy;
    },

    saveState: function (id) {
        var position = this.stateKeyHistory.indexOf(id);
        if (position > -1){
            var count = this.stateKeyHistory.length - position - 1;
            var removed = this.stateKeyHistory.splice(position, count);
            removed.forEach(function(item){
                delete this.state[item];
            }, this);
        }
    },

    createState: function (id) {
        this.stateKeyHistory.push(id);
        this.state[this.stateKeyHistory[this.stateKeyHistory.length - 1]] = Ext.clone(this.lookupsHierarchy);
    },

    cancelState: function (id) {
        var position = this.stateKeyHistory.indexOf(id);
        if (position > -1){
            var count = this.stateKeyHistory.length - position - 1;
            var removed = this.stateKeyHistory.splice(position, count);
            removed.forEach(function(item){
                delete this.state[item];
            }, this);
        }
    },

    isInited: function () {
        return !Ext.isEmpty(Object.keys(this.getCurrentLookupsHierarchy()));
    },
    printTree: function (tree, offset) {
        offset = offset || 0;
        if (!tree) {
            this.printTree(this.getCurrentLookupsHierarchy(), 0);
        } else {
            /*eslint no-console: ["error", { allow: ["log"] }] */
            console.log(offset, tree.getInfo());
            tree.getChildren().forEach(function (item) {
                this.printTree(item, offset + 1);
            }, this);
        }
    },

    fillValuesFromEntity: function (entity) {
        var processHierarchy = function (hierarchy, entity) {
            if (!Ext.isEmpty(Object.keys(hierarchy))) {
                var uri = hierarchy.getInfo() && hierarchy.getInfo().uri;
                if (uri) {
                    var result = MobileUI.core.entity.EntityUtils.findAttributeByTypeUri(entity.attributes, uri);
                    if (!Ext.isEmpty(result)) {
                        hierarchy.getInfo().values = result.filter(function(item){
                            return item.ov !== false && !(Ext.isEmpty(item.lookupCode) || Ext.isEmpty(item.value));
                        }).map(function (item) {
                            return {value: item.lookupCode || item.value, uri: item.uri};
                        });
                    }
                }
                hierarchy.getChildren().forEach(function (childNode) {
                    processHierarchy(childNode, entity);
                });
            }
        };
        if (this.isInited()) {
            processHierarchy(this.getCurrentLookupsHierarchy(), entity);
        }
    },

    clone: function (structure) {
        var result = new window.uiSDK.entity.MultiparentTree.Node(Ext.clone(structure.getInfo()));
        structure.getChildren().forEach(function (item) {
            result.addChild(this.clone(item));
        }, this);
        return result;
    },
    getParents: function (code, uri, attrTypeUri) {
        var parents = window.uiSDK.entity.MultiparentTree.findParentNodes(this.getCurrentLookupsHierarchy(), function (item) {
            if (attrTypeUri) {
                return item.uri === attrTypeUri && item.dependentLookupCode === code;
            }
            return item.dependentLookupCode === code;
        });
        var isRealParent = function(itemUri, uri){
            var endPart = itemUri.substring(uri.length);
            if (endPart.indexOf('/') === 0){
                endPart = endPart.substring(1);
            }
            return itemUri.indexOf(uri) === 0 && endPart.split('/').length === 2;
        };
        var testUri = function (parents, uri) {
            return parents.some(function (parent) {
                return (parent.getInfo().values || []).some(function (item) {
                    return isRealParent(MobileUI.core.session.Session.getUriMapping().resolveMapping(item.uri), uri);
                }, this);
            }, this);
        }.bind(this);

        if (parents && uri){
            var parentUri = MobileUI.core.session.Session.getUriMapping().resolveMapping(uri);
            var uris = parentUri.split('/');
            var found = false;
            var parentUris = [];
            while (uris.length > 0) {
                uris.pop();
                uris.pop();
                parentUri = uris.join('/');
                found = testUri(parents, parentUri);
                if (found){
                    parentUris.push(parentUri);
                }
            }
            parents = parents.map(function (parent) {
                return this.clone(parent);
            }, this);
            parents.forEach(function (parent) {
                parent.getInfo().values = (parent.getInfo().values || []).filter(function (item) {
                    return parentUris.some(function(parentUri){
                        return !Ext.isEmpty(parentUri) && isRealParent(MobileUI.core.session.Session.getUriMapping().resolveMapping(item.uri), parentUri);
                    }, this);
                }, this);
            }, this);
        }
        return parents;
    },
    findAllNodes: function (startNode, predicate) {
        var res = [];
        if (predicate(startNode.getInfo())) {
            return [startNode];
        }
        var children = startNode.getChildren();
        for (var i = 0; i < children.length; i++) {
            Array.prototype.push.apply(res, this.findAllNodes(children[i], predicate));
        }
        return res;
    },
    findNode: function (code) {
        return window.uiSDK.entity.MultiparentTree.find(this.getCurrentLookupsHierarchy(), function (item) {
            return item.dependentLookupCode === code;
        });
    },
    resetHierarchy: function () {
        this.lookupsHierarchy = {};
        this.state = {};
        this.stateKeyHistory = [];
        this.editors = [];
    },

    registerEditor: function (editor, uri) {
        if (!this.isInited()) {
            return;
        }
        this.editors.push(editor);
        editor.on('change', function (editor, value) {
            this.updateLookupsHierarchy({uri: editor.attrTypeUri}, {lookupCode: value, uri: uri});
        }, this);
    },

    updateLookupsHierarchy: function (attrType, attrValue) {
        var nodes = this.findAllNodes(this.getCurrentLookupsHierarchy(), function (item) {
            return item.uri === attrType.uri;
        }).filter(function (node) {
            return node && node.getInfo();
        });

        if (nodes.length > 0) {
            var value = attrValue.lookupCode;
            if (value !== null) {
                nodes.forEach(function (node) {
                    var values = node.getInfo().values;
                    var results = values.filter(function (item) {
                        return item.uri === attrValue.uri;
                    });
                    if (results.length > 0) {
                        results.forEach(function (item) {
                            item.value = value;
                        });
                    } else {
                        values.push({value: value, uri: attrValue.uri});
                    }
                });
            }

            this.fireEvent('dependentUpdated', MobileUI.core.util.Util.flatten(nodes.map(function (item) {
                return item.getChildren();
            })).map(function (item) {
                return item.getInfo() && item.getInfo().dependentLookupCode;
            }).filter(function (item) {
                return item;
            }));
        }
    },

    validateLookups: function (callback, self) {
        var me = this;

        var collectValues = function (items) {

            return (items || []).reduce(function (prev, item) {
                (item.getInfo().values || []).forEach(function(value) {
                    var parents = me.getParents(item.getInfo().dependentLookupCode, value.uri, item.getInfo().uri).reduce(function (prev, current) {
                        var info = current.getInfo();
                        var result = [];
                        if (info.dependentLookupCode) {
                            if (Ext.isEmpty(info.values)) {
                                result = [{
                                    type: info.dependentLookupCode,
                                    codeValues: []
                                }];
                            } else {
                                result = Ext.isEmpty(info.values) ? [] : {
                                    type: info.dependentLookupCode,
                                    codeValues: (info.values).map(function (item) {
                                        return item.value;
                                    })
                                };
                            }
                        }
                        return prev.concat(result);
                    }, []);
                    if (!Ext.isEmpty(parents)) {
                        var codeValue = {
                            type: item.getInfo().dependentLookupCode,
                            codeValue: value.value
                        };
                        var uri = item.getInfo() && item.getInfo().uri ? item.getInfo().uri : '';
                        prev.push({
                            attrTypeUri: uri,
                            parents: parents,
                            uri: value.uri,
                            codeValues: [codeValue]
                        });
                    }

                });
                if (item.getChildren().length > 0) {
                    prev = prev.concat(collectValues(item.getChildren()));
                }
                return prev;
            }, []);


        };


        if (this.isInited()) {
            var values = collectValues(this.getCurrentLookupsHierarchy().getChildren()).map(function (item, index) {
                item.index = index;
                return item;
            });

            var names = values.map(function (item) {
                return item.index;
            });
            var invalidLookups = [];

            var observer = Ext.create('MobileUI.core.Observer', {
                threadNames: names,
                listener: function () {
                    me.invalidLookupsList = invalidLookups;
                    callback.call(self, invalidLookups);
                },
                self: self
            });
            values.forEach(function (item) {
                MobileUI.core.session.Session.validateLookups(item.parents, item.codeValues, function (a) {
                    if (a.codeValues) {
                        var lookups = a.codeValues.filter(function (item) {
                            return item.status !== 'OK';
                        }).map(function (item) {
                            return item.codeValue;
                        }).filter(function (item) {
                            return item;
                        });
                        if (!Ext.isEmpty(lookups)) {
                            invalidLookups.push({attrTypeUri: this.attrTypeUri, uri: this.uri, values: lookups});
                        }
                    }
                    observer.checkThread(this.name);
                }, function () {
                    observer.checkThread(this.name);
                }, {observer: observer, name: item.index, attrTypeUri: item.attrTypeUri, uri: item.uri});
            });
        } else {
            callback.call(self, []);
        }
    },

    getInvalidLookupsList: function(){
        return this.invalidLookupsList;
    },

    applyAction: function(node, action){
        var res = null;
        action(node);
        var children = node.getChildren();
        for (var i = 0; i < children.length; i++) {
            res = this.applyAction(children[i], action);
        }
        return res;
    },
    removeValuesForAttribute: function (uri) {
        if (!this.isInited()) {
            return;
        }
        this.applyAction(this.getCurrentLookupsHierarchy(), function (node) {
            var info = node ? node.getInfo() : {};
            info.values = (info.values || []).filter(function (item) {
                return MobileUI.core.session.Session.getUriMapping().resolveMapping(item.uri || '').indexOf(MobileUI.core.session.Session.getUriMapping().resolveMapping(uri)) !== 0;
            });
        });
    },
    removeDependentLookupValue: function (attrTypeUri, uri) {
        if (!this.isInited()) {
            return;
        }
        this.findAllNodes(this.getCurrentLookupsHierarchy(), function (item) {
            return item.uri === attrTypeUri;
        }).filter(function(node){
            return node && node.getInfo();
        }).forEach(function(node){
            var values = node.getInfo().values;
            node.getInfo().values = values
                .filter(function (item) {
                    return uri !== undefined && item.uri !== uri;
                });
        });
    }

});
