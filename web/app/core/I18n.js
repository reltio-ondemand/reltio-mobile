/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.I18n', {
    requires: [
        'MobileUI.core.Logger'
    ],
    statics: {
        DATE_SHORT: '__dateShort',
        DATE_LONG: '__dateLong',
        DATE_INTERNAL: 'YYYY-MM-DD',

        NUMBER: '__number',
        DOLLAR_FIELD: '__dollarField',

        defaults: {
            '__dateShort': 'l',
            '__dateLong': 'LLL',
            '__number': {
                groupSize: 3,
                decimalSeparator: '.',
                groupSeparator: ','
            },
            '__dollarField': {
                groupSize: 3,
                decimalSeparator: '.',
                groupSeparator: ',',
                prefix: '$',
                digitsAfterDecimalSeparator: 2
            }
        },

        language: 'en',
        region: 'US',
        locale: 'en_US',
        dictionaries: [],
        config: null,

        metadataKeys: ['label', 'description', 'abbreviation'],
        //TODO: Pattern i18n
        //, 'dataLabelPattern', 'secondaryLabelPattern', 'relationshipLabelPattern', 'labelPattern'],

        getLanguagesToDownload: function (conf, locale, language) {
            var sameRegionLanguage = language + '_' + language.toUpperCase();
            var languageToDownload = [locale, language, sameRegionLanguage, 'en'];
            if (conf && conf.translations) {
                languageToDownload = languageToDownload.filter(function (language) {
                    return conf.translations[language.toLowerCase()];
                });
            }
            return languageToDownload;
        },

        init: function (next, insecure) {
            this.setLocale(this.detectLanguage());

            var config;

            if (insecure) {
                var settings = MobileUI.core.Services.getGeneralSettings();
                config = settings && settings.i18n || {};
            }
            else {
                var cm = MobileUI.core.Services.getConfigurationManager();
                var plugin = cm.getExtensionById('com.reltio.plugins.ui.LocalizationServer');
                config = plugin && plugin.configuration || {};
            }

            this.config = config;

            var languageChain = [this.locale, this.language];

            var initialized = MobileUI.core.I18n.isInited;
            if (insecure) {
                initialized = MobileUI.core.I18n.isInitedInsecure;
            }
            if (config.translations && !initialized) {
                if (config.translations) {
                    for (var k in config.translations) {
                        if (config.translations.hasOwnProperty(k)) {
                            config.translations[k.toLowerCase()] = config.translations[k.toLowerCase()] || config.translations[k];
                        }
                    }
                }

                languageChain = this.getLanguagesToDownload(config, this.locale, this.language);
                var dictionaties = {};
                var finishInitialization = function () {
                    if (insecure) {
                        MobileUI.core.I18n.isInitedInsecure = true;
                    }
                    else {
                        MobileUI.core.I18n.isInited = true;
                    }

                    this.dictionaries = languageChain.map(function (lang) {
                        return dictionaties[lang];
                    }).filter(function (lang) {
                        return !!lang;
                    });
                    next();
                }.bind(this);

                var observer = Ext.create('MobileUI.core.Observer', {
                    threadNames: languageChain,
                    listener: finishInitialization
                });

                languageChain.forEach(function (lang) {
                    Ext.Ajax.request({
                        url: config.translations[lang],
                        success: function (xhr) {
                            dictionaties[lang] = JSON.parse(xhr.responseText);
                            observer.checkThread(lang);
                        },
                        failure: function (xhr) {
                            MobileUI.core.Logger.error(xhr);
                            observer.checkThread(lang);
                        },
                        method: 'GET'
                    });
                });
            }
            else {
                next();
            }
        },

        detectLanguage: function () {
            var forceLocale = location.search.match(/[?&]locale=([\S-_]+)[&]?/i);
            this.forceLocale = (forceLocale && forceLocale[1]) || null;
            var systemLocale = (forceLocale && forceLocale[1]) || window.navigator.userLanguage || window.navigator.language;
            systemLocale = systemLocale || 'en-US';
            return systemLocale;
        },

        getForcedLocale: function () {
            return this.forceLocale;
        },

        setLocale: function(locale) {
            var separated = locale.replace(/-/g, '_').split('_');
            this.language = (separated[0] || '').toLowerCase();
            this.region = (separated[1] || this.language).toUpperCase();
            this.locale = this.language + '_' + this.region;

            var dashedLocale = this.locale.replace(/_/g, '-');
            if (dashedLocale !== moment.locale(dashedLocale)) {
                moment.locale(this.language);
            }
        },

        translate: function(subject, format) {
            var original = subject;

            if (Ext.isString(subject)) {
                var found = false, dict, translated;
                for (var i = 0; i < this.dictionaries.length; i++) {
                    dict = this.dictionaries[i];
                    translated = dict[subject];
                    if (translated) {
                        subject = translated;
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    dict = this.defaults;
                    translated = dict[subject];
                    if (translated) {
                        subject = translated;
                        found = true;
                    }
                }

                if (arguments.length > 1) {
                    var args = arguments;
                    subject = subject.replace(/%(\d)/g, function(match, p) {
                        return args[parseInt(p, 10)];
                    });
                }

                //Special case for non-string format localization.
                //Should return null if there is no localized subject with key starts with __
                if (!found && (subject.indexOf('__') === 0)) {
                    subject = null;
                }
            }
            else if (Ext.isDate(subject)) {
                format = format || this.DATE_SHORT;
                format = this.translate(format);
                subject = moment(subject).format(format);
                return subject; //avoid verbose second time
            }
            else if (Ext.isNumber(subject)) {
                format = format || this.NUMBER;
                format = this.translate(format);
                if (format.digitsAfterDecimalSeparator !== undefined) {
                    if (format.digitsAfterDecimalSeparator === 0) {
                        subject = parseInt(subject, 10);
                    }
                    else {
                        subject = subject.toFixed(format.digitsAfterDecimalSeparator);
                    }
                }
                if (!format.regExp) {
                    format.regExp = new RegExp('\\B(?=(\\d{' + (format.groupSize || 3) + '})+(?!\\d))', 'g');
                }

                var parts = subject.toString().split('.');
                if (format.groupSeparator) {
                    parts[0] = parts[0].replace(format.regExp, format.groupSeparator);
                }

                subject = (format.prefix || '') + parts.join(format.decimalSeparator || '.')+ (format.suffix || '');
            }

            if (this.config && this.config.verbose && Ext.isString(subject)) {
                if (subject === original) {
                    subject = '[' + subject + ']';
                }
                else {
                    subject = '/' + subject + '\\';
                }
            }
            return subject;
        },

        translateObject: function(object) {
            if (!this.config) {
                return object;
            }

            var localized = object;

            if (object) {
                if (Ext.isObject(object)) {
                    for (var key in object) {
                        if (Object.prototype.hasOwnProperty.call(object, key)) {
                            if (MobileUI.core.I18n.metadataKeys.indexOf(key) !== -1) {
                                localized[key] = i18n(localized[key]);
                            }
                            else {
                                localized[key] = MobileUI.core.I18n.translateObject(localized[key]);
                            }
                        }
                    }
                }
                else if (Ext.isArray(object)) {
                    for (var i = 0; i < object.length; i++) {
                        object[i] = this.translateObject(object[i]);
                    }
                }
            }

            return localized;
        },

        parseDate: function(date, format) {
            var parsed;

            if (Ext.isNumber(date)) {
                return new Date(date);
            }

            if (Ext.isString(date)) {
                if (format) {
                    format = this.translate(format);
                    var value = date;
                    if (this.config && this.config.verbose) {
                        value = date.substring(1, date.length-1);
                        format = format.substring(1, format.length-1);
                    }
                    parsed = moment(value, format);
                    if (parsed.isValid()) {
                        return parsed.toDate();
                    }
                }

                parsed = moment(date);
                if (parsed.isValid()) {
                    return parsed.toDate();
                }
                return null;
            }
            if (date instanceof Date) {
                return date;
            }
            throw 'Cannot parse given date: ' + date;
        },

        parseNumber: function(value, format) {
            format = format || this.NUMBER;
            format = this.translate(format);

            if (this.config && this.config.verbose) {
                value = value.substring(1, value.length-1);
            }

            if (format.prefix) {
                value = value.substring(format.prefix.length);
            }

            if (format.suffix) {
                value = value.substring(0, value.length - format.suffix.length);
            }

            if (format.groupSeparator) {
                value = value.split(format.groupSeparator).join('');
            }

            var dc = format.decimalSeparator || '.';
            if (dc !== '.') {
                value = value.split(format.decimalSeparator).join('.');
            }
            return Number(value);
        }
    }
});

window.i18n = function() {
    return MobileUI.core.I18n.translate.apply(MobileUI.core.I18n, arguments);
};
