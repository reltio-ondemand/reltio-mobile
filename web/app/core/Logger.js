/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Logger', {
    singleton: 'true',
    level: {
        OFF: {text: 'OFF', value: 0},
        FATAL: {text: 'FATAL', value: 1},
        ERROR: {text: 'ERROR', value: 2},
        WARN: {text: 'WARN', value: 3},
        INFO: {text: 'INFO', value: 4},
        DEBUG: {text: 'DEBUG', value: 5},
        TRACE: {text: 'TRACE', value: 6},
        ALL: {text: 'ALL', value: 7}
    },
    /**@type{{text: {String}, value: {Number}}} @private */
    currentLevel: null,
    init: function ()
    {
        var settings = MobileUI.core.Services.getGeneralSettings(),
            level,
            that = this,
            errorHandler = function (message)
            {
                that.sendLog(message, MobileUI.core.Logger.level.ERROR);
            };
        if (settings.level)
        {
            for (level in MobileUI.core.Logger.level)
            {
                if (MobileUI.core.Logger.level.hasOwnProperty(level))
                {
                    if (level.text === settings.level.toUpperCase().trim())
                    {
                        this.setLevel(level);
                        // eslint-disable-next-line no-console
                        console.error = errorHandler;
                        return;
                    }
                }
            }
        }
        if (settings.debugPath !== '')
        {
            this.setLevel(MobileUI.core.Logger.level.INFO);
        }
        else
        {
            // eslint-disable-next-line no-console
            console.error = function (message)
            {
                that.sendLog(message, MobileUI.core.Logger.level.ERROR);
            };
            this.setLevel(MobileUI.core.Logger.level.ERROR);
        }
    },
    /**
     * Set the default level which sends to server
     * @param level {{text: {String}, value: {Number}}}
     */
    setLevel: function (level)
    {
        this.currentLevel = level;
    },
    /**
     * Log error
     * @param message {*}
     */
    error: function (message)
    {
        this.log(message, MobileUI.core.Logger.level.ERROR);
    },
    /**
     * Log warning
     * @param message {*}
     */
    warn: function (message)
    {
        this.log(message, MobileUI.core.Logger.level.WARN);
    },
    /**
     * Log information message
     * @param message {*}
     */
    info: function (message)
    {
        this.log(message, MobileUI.core.Logger.level.INFO);
    },
    /**
     * Log information message
     * @param message {*}
     */
    debug: function (message)
    {
        this.log(message, MobileUI.core.Logger.level.DEBUG);
    },
    /**
     * Log fatal error
     * @param message {*}
     */
    fatal: function (message)
    {
        this.log(message, MobileUI.core.Logger.level.FATAL);
    },
    /**
     * Log message
     * @param message {*}
     * @param level {{}?} level
     */
    log: function (message, level)
    {
        if (console)
        {
            if (level)
            {
                switch (level.value)
                {
                    case MobileUI.core.Logger.level.WARN.value:
                    case MobileUI.core.Logger.level.ERROR.value:
                    case MobileUI.core.Logger.level.FATAL.value:
                        // eslint-disable-next-line no-console
                        console.warn(message);
                        break;
                    case MobileUI.core.Logger.level.OFF.value:
                        break;
                    default:
                        /*eslint no-console: ["error", { allow: ["log"] }] */
                        console.log(message);
                        break;
                }
            }
            else
            {
                /*eslint no-console: ["error", { allow: ["log"] }] */
                console.log(message);
            }
        }
        if (level && this.currentLevel && level.value <= this.currentLevel.value && this.currentLevel.value !== MobileUI.core.Logger.level.OFF.value)
        {
            this.sendLog(message, level);
        }
    },
    /**
     * @private
     * @param message {String} message
     * @param level {{}} level
     */
    sendLog: function (message, level)
    {
        MobileUI.core.Services.asyncRequest('/mobile_log', null, null,
            {
                method: 'POST',
                data: 'entry=' + Ext.encode(message) + '&level=' + level.text,
                signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            });
    }
});
