/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Lookups', {

    statics: {
        lookups: {},
        lookupsData: null,

        /**
         * @param lookupCode {String}
         */
        getLookups: function(lookupCode) {
            if (this.lookupsData) {
                var lookups = this.lookups[lookupCode];
                if (!lookups) {
                    lookups = this.lookups[lookupCode] =
                        new MobileUI.core.Lookups(lookupCode);
                }
                return lookups;
            }
        },

        /*
         * @param success {Function?}
         * @param failure {Function?}
         * @param context {Object?}
         */
        loadLookups: function(success, failure, context) {
            if (this.lookupsData) {
                success.call(context);
            }
            else {
                MobileUI.core.session.Session.sendRequest('/lookups', null, null,
                    function(data) {
                        MobileUI.core.Lookups.lookupsData = data;
                        if (data) {
                            if (success) {
                                success.call(context);
                            }
                        }
                        else if (failure) {
                            failure.call(context);
                        }
                    }, failure, context);
            }
        }
    },

    /** @private */
    raw: null,

    /** @private */
    tree: null,

    /**
     * @param code {String}
     */
    constructor: function(code) {
        var rawData = Ext.clone(MobileUI.core.Lookups.lookupsData);
        this.raw = rawData[code];
        this.tree = this.buildTree(rawData, code);
    },

    /**
     * @returns {Object}
     */
    getTree: function() {
        return this.tree;
    },

    /**
     * @returns {Object}
     */
    getRaw: function() {
        return this.raw;
    },

    /**
     * @param code {String}
     * @returns {String}
     */
    getDisplayName: function(code) {
        var lookup = this.raw[code];
        return lookup && lookup.displayName;
    },

    /** @private */
    buildTree: function(rawData, lookupCode) {
        var root = {children: [], parent: null, displayName: ''};
        var lookups = rawData[lookupCode];
        if (lookups) {
            for (var lookupKey in lookups) {
                if (lookups.hasOwnProperty(lookupKey)) {
                    var lookup = lookups[lookupKey];
                    this.processLookup(rawData, root, lookupCode, lookupKey, lookup);
                }
            }
        }
        this.sort(root);
        return root;
    },

    /** @private */
    processLookup: function(rawData, root, lookupCode, lookupKey, lookup) {
        if (lookup.path) {
            return;
        }
        lookup.name = lookupKey;
        lookup.path = lookupCode + '.' + lookupKey;
        if (lookup.parent) {
            var parentParts = lookup.parent.split('.');
            var parentLookups = rawData[parentParts[0]];
            if (parentLookups) {
                var parentLookup = parentLookups[parentParts[1]];
                if (parentLookup) {
                    lookup.parent = parentLookup;
                    var parentLookupProcessed = parentLookup.children !== undefined;
                    if (parentLookupProcessed) {
                        parentLookup.children.push(lookup);
                    }
                    else {
                        parentLookup.children = [lookup];
                        this.processLookup(rawData, root, parentParts[0], parentParts[1], parentLookup);
                    }
                }
            }
            else {
                root.children.push(lookup);
            }
        }
        if (!lookup.parent) {
            lookup.parent = root;
            root.children.push(lookup);
        }
    },

    /** @private */
    sort: function(root) {
        if (root) {
            if (root.children && root.children[0] && root.children[0].children) {
                root.children.forEach(this.sort, this);
            }
            else if (Ext.isArray(root.children)) {
                root.children.sort(function(a, b) {
                    if (a.displayName > b.displayName) {
                        return 1;
                    }
                    if (a.displayName < b.displayName) {
                        return -1;
                    }
                    return 0;
                });
            }
        }
    }
});
