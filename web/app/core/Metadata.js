/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//noinspection JSUnusedGlobalSymbols
Ext.define('MobileUI.core.Metadata', {
    singleton: true,
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.core.Logger'
    ],
    mixins: {
        observable: 'Ext.mixin.Observable'
    },
    RELTIO_XWALK_TYPE: 'configuration/sources/Reltio',
    URL_CONFIGURATION: '/configuration?options=showAccess',
    /** @type {String} @private */
    apiPath: null,
    /** @type {Object} @private */
    entityTypes: null,
    /** @type {Object} @private */
    roles: null,
    /** @type {Object} @private */
    relationTypes: null,
    /** @type {Object} @private */
    groupTypes: null,
    /** @type {Object} @private */
    graphTypes: null,
    /** @type {Object} @private */
    categoryTypes: null,
    /** @type {Object} @private */
    interactionTypes: null,
    /** @type {Array} @private */
    xwalkTypes: null,
    /** @type {Object} @private */
    survivorshipStrategies: null,
    /** @type {Boolean} @private */
    inited: null,
    /** @type {Object} @private */
    entityDescendants: {},
    /** @type {Object} @private */
    filteredTypes: null,
    /** @type {Object} @private */
    dependentLookupHierarchy: null,
    /** @type {boolean} @private */
    dependentLookupsEnabled: false,
    /**
     * Method return is metadata inited or not
     * @returns {Boolean}
     */
    isInited: function ()
    {
        return this.inited;
    },
    isDependentLookupsEnabled: function(){
        return this.dependentLookupsEnabled;
    },
    killMetadata: function()
    {
        this.inited = false;
    },

    init: function () {
        return MobileUI.core.session.Session.sendRequestAndReturnPromise(this.URL_CONFIGURATION)
            .then(function (response) {
                this.parseMetadata(response);
            }.bind(this))
            .catch(function (error) {
                MobileUI.core.Logger.log(error);
            });
    },

    /**
     * Method processes configuration and creates entity types
     * @private
     * @param configuration {Object} configuration json
     */
    processEntityTypes: function (configuration)
    {
        var arr = configuration.entityTypes,
            i, checkOverride, marshalled,
            type, entityType,
            prepareData = function (data)
            {
                var ret = data;
                if (!ret.entityTypeRoleURIs)
                {
                    ret.entityTypeRoleURIs = [];
                }
                if (!ret.attributes)
                {
                    ret.attributes = [];
                }
                if (!ret.businessCardAttributeURIs)
                {
                    ret.businessCardAttributeURIs = [];
                }
                return ret;
            };
        this.entityTypes = {};

        for (i = 0; i < arr.length; i++)
        {
            checkOverride = this.entityTypes[arr[i].uri];
            marshalled = prepareData(arr[i]);
            if (checkOverride)
            {
                MobileUI.core.Logger.error('Trying to override ', arr[i].uri, checkOverride, 'with', marshalled);
            }
            this.entityTypes[arr[i].uri] = marshalled;
        }

        for (type in this.entityTypes)
        {
            if (this.entityTypes.hasOwnProperty(type))
            {
                entityType = this.entityTypes[type];
                if (entityType.extendsTypeURI && this.entityTypes[entityType.extendsTypeURI])
                {
                    var parent = entityType;
                    while ((parent = this.entityTypes[parent.extendsTypeURI]))
                    {
                        MobileUI.core.util.Util.merge(parent, entityType, ['abstract']);
                    }
                }
            }
        }
    },
    /**
     * Method convert array to object
     * @private
     * @param arr
     * @returns {{}}
     */
    convertArrayToObject: function (arr)
    {
        var i, result = {};
        if (Ext.isArray(arr))
        {
            for (i = 0; i < arr.length; i++)
            {
                result[arr[i].uri] = arr[i];
            }
        }
        return result;
    },
    /**
     * Method processes sources
     * @private
     * @param configuration {Object} configuration json
     */
    processSources: function (configuration)
    {
        var i, hasReltioSource = false;
        this.xwalkTypes = Ext.Array.clone(configuration.sources);
        for (i = 0; i < this.xwalkTypes.length; i++)
        {
            if (this.xwalkTypes[i].uri === 'configuration/sources/Reltio')
            {
                hasReltioSource = true;
                break;
            }
        }
        if (!hasReltioSource)
        {
            this.xwalkTypes.push({
                uri: 'configuration/sources/Reltio',
                name: 'Reltio',
                description: '',
                label: 'Reltio',
                icon: 'images/source/reltio.png',
                abbreviation: 'Reltio'
            });
        }
    },

    /**
     * Method processes Survivorship Strategies
     * @private
     * @param configuration {Object} configuration json
     */
    processSurvivorshipStrategies: function (configuration)
    {
        var arr, i;
        arr = configuration.survivorshipStrategies;
        this.survivorshipStrategies = {};
        for (i = 0; i < arr.length; i++)
        {
            var strategy = arr[i];
            var uri = strategy.uri;
            var shortUri = uri.substring(uri.lastIndexOf('/') + 1);
            this.survivorshipStrategies[shortUri] = strategy;
        }
    },
    getRawConfiguration: function(){
        return this.rawConfiguration;
    },
    /**
     * Method parses the configuation repsonse and creates the metadata
     * @param json {Object} json response
     */
    parseMetadata: function (json)
    {
        json = MobileUI.core.I18n.translateObject(json);
        this.rawConfiguration = Ext.clone(json);
        this.processEntityTypes(json);
        this.processSources(json);
        this.processSurvivorshipStrategies(json);
        this.roles = this.convertArrayToObject(json.roles);
        this.relationTypes = this.convertArrayToObject(json.relationTypes);
        this.processDependentLookups();
        this.processCardinality();
        this.groupTypes = this.convertArrayToObject(json.groupTypes);
        this.graphTypes = this.convertArrayToObject(json.graphTypes);
        this.categoryTypes = this.convertArrayToObject(json.categoryTypes);
        this.interactionTypes = this.convertArrayToObject(json.interactionTypes);
        this.inited = true;
        this.fireEvent('init');
    },
    /**
     * Method returns entity type by uri
     * @param uri {String}
     * @returns {Object|null}
     */
    getEntityType: function (uri)
    {
        return MobileUI.core.Metadata.entityTypes[uri] || null;
    },
    /**
     * Method returns all entity types
     * @params predicate {Function?} predicate function
     * @params context {Object?} context
     * @returns {*}
     */
    getEntityTypes: function (predicate, context)
    {
        if (predicate)
        {
            return MobileUI.core.util.Util.filter(this.entityTypes, predicate, context);
        }
        return this.entityTypes;
    },

    calculateEntityDescendants : function(entityUri)
    {
        var ret = {},
            entityType,
            type, parent;

        for (type in this.entityTypes)
        {
            if (this.entityTypes.hasOwnProperty(type))
            {
                entityType = this.entityTypes[type];
                if (entityType.extendsTypeURI && this.entityTypes[entityType.extendsTypeURI])
                {
                    parent = entityType;
                    while ((parent = this.entityTypes[parent.extendsTypeURI]))
                    {
                        if (parent.uri === entityUri)
                        {
                            // everything from entityType up to (but not including) current 'parent' value should be included in the chain
                            parent = entityType;
                            ret[parent.uri] = parent;
                            while ((parent = this.entityTypes[parent.extendsTypeURI]))
                            {
                                if (parent.uri === entityUri)
                                {
                                    break;
                                }

                                ret[parent.uri] = parent;
                            }
                            break;
                        }
                    }
                }
            }
        }

        return ret;
    },


    getEntityDescendants : function(entityUri)
    {
        var descs = this.entityDescendants[entityUri];
        if (!descs)
        {
            descs = this.calculateEntityDescendants(entityUri);
            this.entityDescendants[entityUri] = descs;
        }
        return descs;
    },

    /**
     * Method returns role by uri
     * @param uri {String}
     * @returns {Object|null}
     */
    getRole: function (uri)
    {
        return this.roles[uri];
    },
    /**
     * Method returns roles
     * @returns {Object}
     */
    getRoles: function ()
    {
        return this.roles;
    },

    /**
     * Method returns relation type by uri
     * @param uri {String}
     * @returns {Object|null}
     */
    getRelationType: function (uri)
    {
        return this.relationTypes[uri];
    },
    /**
     * Method returns relation types
     * @returns {Object}
     */
    getRelationTypes: function ()
    {
        return this.relationTypes;
    },
    getDependentLookupsHierarchy: function () {
        var that = this;
        var findDependentAttributes = function (attributesParent) {
            var result = [];
            if (attributesParent.attributes) {
                result = attributesParent.attributes.reduce(function (prev, curr) {
                    if (curr.attributes) {
                        prev = prev.concat(findDependentAttributes(curr));
                    }
                    if (curr.referencedAttributeURIs){
                        var entityAttributes = curr.referencedAttributeURIs.filter(function(item){
                            return item.indexOf('/entityTypes/') !== -1;
                        }).map(function(item){
                            return that.findEntityAttributeByUri(null, item);
                        }).filter(function(item){
                            return item;
                        });
                        prev = prev.concat(findDependentAttributes({attributes:entityAttributes}));

                        var relationAttributes = curr.referencedAttributeURIs.filter(function(item){
                            return item.indexOf('/relationTypes/') !== -1;
                        }).map(function(item){
                            return that.findRelationAttributeByUri(null, item);
                        }).filter(function(item){
                            return item;
                        });
                        prev = prev.concat(findDependentAttributes({attributes:relationAttributes}));
                    }
                    if (curr.dependentLookupCode) {
                        prev.push(curr);
                    }
                    return prev;
                }, []);
            }
            return result;
        };
        var makeDependentLookupNode = function (uri, dependentLookupCode) {
            return {
                uri: uri,
                values: [],
                dependentLookupCode: dependentLookupCode
            };
        };
        var processDependentLookups = function (type, allDependentLookups) {
            var hasParents = [];
            var parent = new Node(makeDependentLookupNode(type.uri, type.dependentLookupCode));
            if (type.dependentLookupAttributes) {
                type.dependentLookupAttributes.forEach(function (parentAttribute) {
                    var dependentList = allDependentLookups.filter(function (item) {
                        return item.uri === parentAttribute;
                    });
                    if (!Ext.isEmpty(dependentList)) {
                        hasParents.push(dependentList[0].uri);
                        var result = processDependentLookups(dependentList[0], allDependentLookups);
                        hasParents = hasParents.concat(result.hasParents);
                        parent.addChild(result.node);
                    }
                });
            }
            return {node: parent, hasParents: hasParents};
        };
        if (!this.dependentLookupHierarchy) {
            this.dependentLookupHierarchy = {};
            var me = this;
            var dependentLookupsByEntityType = Object.keys(this.entityTypes).reduce(function (prev, curr) {
                prev[curr] = findDependentAttributes(me.entityTypes[curr]);
                return prev;
            }, {});
            var allDependentLookups = Object.keys(dependentLookupsByEntityType).reduce(function (prev, curr) {
                prev = prev.concat(dependentLookupsByEntityType[curr]);
                return prev;
            }, []);
            var MultiparentTree = window.uiSDK.entity.MultiparentTree;
            var Node = MultiparentTree.Node;
            var addNodeToHierarchy = function (node, entityTypeUri) {
                if (hasParents.indexOf(node.getInfo().uri) === -1) {
                    this.dependentLookupHierarchy[entityTypeUri].addChild(node);
                }
            }.bind(this);
            for (var entityTypeUri in dependentLookupsByEntityType) {
                if (dependentLookupsByEntityType.hasOwnProperty(entityTypeUri)) {
                    var attributes = dependentLookupsByEntityType[entityTypeUri];

                    if (!Ext.isEmpty(attributes)) {
                        var hasParents = [];
                        /*jshint loopfunc: true */
                        var dependentLookups = attributes.map(function (item) {
                            var result = processDependentLookups(item, allDependentLookups);
                            hasParents = hasParents.concat(result.hasParents);

                            return result.node;
                        });
                        /*jshint loopfunc: false */
                        this.dependentLookupHierarchy[entityTypeUri] = new Node({uri: entityTypeUri});
                        /*jshint loopfunc: true */
                        dependentLookups.forEach(function (node) {
                            addNodeToHierarchy(node, entityTypeUri);
                        }, this);
                        /*jshint loopfunc: false */
                    }
                }
            }
        }
        Object.keys(this.dependentLookupHierarchy).forEach(function(item){
            this.cleanLookups(this.dependentLookupHierarchy[item]);
        }, this);
        return Ext.clone(this.dependentLookupHierarchy, true);
    },

    cleanLookups: function(tree) {
        tree.getInfo().values = [];
        tree.getChildren().forEach(function (item) {
            this.cleanLookups(item);
        }, this);
    },
    processCardinality: function(){
        Object
            .keys(this.entityTypes)
            .map(function (key) {
                return this.entityTypes[key];
            }, this)
            .filter(function (entityType) {
                return this
                    .flattenAttributes(entityType.attributes)
                    .some(function (attrType) {
                        return attrType.cardinality;
                    });
            }, this)
            .forEach(function (entityType) {
                entityType.cardinality = true;
            });
    },
    flattenAttributes: function (attributes) {
        var me = this;
        return attributes.reduce(function (a, b) {
            return b.attributes ? a.concat([b], me.flattenAttributes(b.attributes)) : a.concat(b);
        }, []);
    },

    processDependentLookups: function () {
        var flattedAttributes = this.flattenAttributes(Object.keys(this.entityTypes)
            .map(function (uri) {
                return this.getEntityType(uri).attributes;
            }, this).reduce(function (a, b) {
                return a.concat(b);
            }, []));

        var relationAttributes = this.flattenAttributes(Object.keys(this.relationTypes)
            .map(function (uri) {
                return this.getRelationType(uri).attributes;
            }, this).reduce(function (a, b) {
                return a.concat(b);
            }, []).filter(function(item){
                return item;
            }));
        flattedAttributes = flattedAttributes.concat(relationAttributes);

        var parentAttributes = flattedAttributes.filter(function (item) {
            return item.dependentLookupAttributes;
        });
        if (!Ext.isEmpty(parentAttributes)) {
            flattedAttributes.filter(function (item) {
                return item.lookupCode;
            }).forEach(function (item) {
                item.dependentLookupCode = item.lookupCode;
            });
            this.dependentLookupsEnabled = true;
        }
    },
    /**
     * Method returns group type by uri
     * @param uri {String}
     * @returns {Object|null}
     */
    getGroupType: function (uri)
    {
        return this.groupTypes[uri];
    },
    /**
     * Method returns group types
     * @returns {*}
     */
    getGroupTypes: function ()
    {
        return this.groupTypes;
    },

    /**
     * Method returns all category types
     * @returns {Object}
     */
    getCategoryTypes: function ()
    {
        return this.categoryTypes;
    },
    /**
     * Method returns graph type by uri
     * @param uri
     * @returns {*}
     */
    getGraphType: function (uri)
    {
        return this.graphTypes[uri];
    },

    /**
     * Method returns crosswalk type by uri
     * @param uri {String}
     * @returns {Object|null}
     */
    getXwalkType: function (uri)
    {
        var i;
        for (i = 0; i < this.xwalkTypes.length; i++)
        {
            if (this.xwalkTypes[i].uri === uri ||
                this.xwalkTypes[i].label === uri ||
                this.xwalkTypes[i].uri.substring(this.xwalkTypes[i].uri.lastIndexOf('/') + 1) === uri)
            {
                return this.xwalkTypes[i];
            }
        }
        return null;
    },
    /**
     * Method returns all crosswalks types
     * @param sorted {Boolean} is sorted
     * @returns {Array}
     */
    getXwalkTypes: function (sorted /*optional*/)
    {
        var result = this.xwalkTypes;
        if (sorted)
        {
            result = Ext.Array.clone(result);
            result.sort(function (a, b)
            {
                return a.label < b.label ? -1 : (a.label > b.label ? 1 : 0);
            });
        }
        return result;
    },
    /**
     * Method returns interaction type by uri
     * @param uri {String}
     * @returns {*}
     */
    getInteractionType: function (uri)
    {
        return this.interactionTypes[uri];
    },

    /**
     * Method returns interaction types
     * @returns {Object}
     */
    getInteractionTypes: function ()
    {
        return this.interactionTypes;
    },
    /**
     * Method returns survivorship strategy type by uri
     * @param uri {String}
     * @returns {*}
     */
    getSurvivorshipStrategy: function (uri)
    {
        return this.survivorshipStrategies[uri.substring(uri.lastIndexOf('/') + 1)];
    },
    /**
     * Method returns survivorship strategies
     * @returns {Object}
     */
    getSurvivorshipStrategies: function ()
    {
        return this.survivorshipStrategies;
    },
    /**
     * Method finds the entity attribute by uri
     * @param entityType {String | Object?} entity type
     * @param uri {String} attribute uri
     * @returns {Object | null}
     */
    findEntityAttributeByUri: function (entityType, uri)
    {
        var i, entityTypeUri,
            namesPath, parentObjAttrs, parentObj,
            lookForName, j, ret = null;
        if (!entityType)
        {
            i = uri.indexOf('/attributes');
            if (i === -1)
            {
                return null;
            }
            entityTypeUri = uri.substring(0, i);
            entityType = this.entityTypes[entityTypeUri];
            entityType = entityType || this.relationTypes[entityTypeUri];
            if (!entityType)
            {
                return null;
            }
        }
        else if (typeof entityType === 'string')
        {
            entityType = this.entityTypes[entityType];
            if (!entityType)
            {
                return null;
            }
        }
        // uri may look like: configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode
        namesPath = uri.split('/attributes/');
        parentObjAttrs = entityType.attributes;
        parentObj = entityType;
        for (i = 0; i < namesPath.length; i++)
        {
            lookForName = namesPath[i];
            for (j = 0; j < parentObjAttrs.length; ++j)
            {
                if (parentObjAttrs[j].name === lookForName)
                {
                    parentObj = parentObjAttrs[j];
                    parentObjAttrs = parentObjAttrs[j].attributes || [];
                    j = parentObjAttrs.length + 1;
                    break;
                }
            }
        }
        if (parentObj.uri === uri)
        {
            ret = parentObj;
        }
        return ret;
    },
    /**
     * Method finds the relation attribute by uri
     * @param relationType {Object?} relation type uri
     * @param uri {String} relation attribute uri
     * @returns {Object | null}
     */
    findRelationAttributeByUri: function (relationType, uri)
    {
        var namesPath, parentObjAttrs, parentObj, i, relationUri, j, lookForName, ret = null;
        if (!relationType)
        {
            i = uri.indexOf('/attributes');
            if (i === -1)
            {
                return null;
            }
            relationUri = uri.substring(0, i);
            relationType = this.relationTypes[relationUri];
            if (!relationType)
            {
                return null;
            }
        }
        ret = null;
        // uri may look like: configuration/relationTypes/HasAddress/attributes/Alignment/attributes/OncologySalesAlignment
        namesPath = uri.split('/attributes/');
        parentObjAttrs = relationType.attributes || [];
        parentObj = relationType;
        for (i = 0; i < namesPath.length; ++i)
        {
            lookForName = namesPath[i];
            for (j = 0; j < parentObjAttrs.length; ++j)
            {
                if (parentObjAttrs[j].name === lookForName)
                {
                    parentObj = parentObjAttrs[j];
                    parentObjAttrs = parentObjAttrs[j].attributes || [];
                    j = parentObjAttrs.length + 1;
                    break;
                }
            }
        }
        if (parentObj.uri === uri)
        {
            ret = parentObj;
        }
        if (!ret)
        {
            uri = uri.split('relationTypes').join('entityTypes');
            parentObjAttrs = relationType.attributes || [];
            for (i = 0; i < parentObjAttrs.length; ++i)
            {
                if (parentObjAttrs[i].uri === uri)
                {
                    ret = parentObjAttrs[i];
                    break;
                }
            }
        }
        return ret;
    },

    getEntityAttributeHierarchy: function (uri, skipUriCheck) {
        uri = uri || '';
        var isAnalytic = this.isAnalyticAttribute(uri);
        var attrCollectionName = isAnalytic ? 'analyticsAttributes' : 'attributes';
        var namesPath = uri.split('/' + attrCollectionName + '/');
        if (namesPath.length < 2) {
            return;
        }


        var entityTypeUri = namesPath[0];
        var entityType = this.getEntityType(entityTypeUri) || this.getRelationType(entityTypeUri);
        if (!entityType) {
            return;
        }

        var parentObjAttrs = entityType[attrCollectionName];
        var parentObj = entityType;
        var ret = [parentObj];
        for (var i = 1; i < namesPath.length; ++i) { // start with 1, because parent object is already in the 'ret'
            var lookForName = namesPath[i];
            var oldLength = ret.length;

            for (var j = 0; j < parentObjAttrs.length; ++j) {
                if (parentObjAttrs[j] && parentObjAttrs[j].name === lookForName) {
                    parentObj = parentObjAttrs[j];
                    if (parentObj.type === 'Reference') {
                        /*jshint loopfunc: true */
                        parentObjAttrs = parentObj.referencedAttributeURIs
                            .map(function (uri) {
                                return this.findEntityAttributeByUri(null, uri) || this.findRelationAttributeByUri(null, uri);
                            }, this);
                        /*jshint loopfunc: false */
                    }
                    else {
                        parentObjAttrs = parentObjAttrs[j][attrCollectionName] || [];
                    }
                    ret.push(parentObj);
                    break;
                }
            }

            if (oldLength === ret.length) {
                return null;
            }
        }

        if (!skipUriCheck && parentObj.uri !== uri) {
            ret = null;
        }

        return ret;
    },

    findAttributeByName: function(name){
        name = name.indexOf('attributes.') === 0 ? name.substring('attributes.'.length) : name;
        var result = null, key, i;
        for (key in this.entityTypes)
        {
            if (this.entityTypes.hasOwnProperty(key))
            {
                result = this.findEntityAttributeByName(key, name);
                if (result)
                {
                    break;
                }
            }
        }
        for (key in this.relationTypes)
        {
            if (this.relationTypes.hasOwnProperty(key) && Ext.isArray(this.relationTypes[key].attributes))
            {
                var relationType = this.relationTypes[key];
                for (i = 0; i < relationType.attributes.length; ++i)
                {
                    if (relationType.attributes[i].name === name)
                    {
                        result = relationType.attributes[i];
                        break;
                    }
                }
            }
        }
        return result;
    },

    findEntityAttributeByName: function (entityType, name)
    {
        var attributes, attr, i, j,
            findEntity = function (uri)
            {
                return this.findEntityAttributeByUri(null, uri) || this.findRelationAttributeByUri(null, uri);
            },
            excludeEmpty = function (attr)
            {
                return !!attr;
            };
        name = name.split('.');
        if (Ext.isString(entityType))
        {
            entityType = this.entityTypes[entityType];
        }
        if (!entityType)
        {
            return null;
        }
        attributes = entityType.attributes;
        attr = null;
        for (i = 0; i < name.length; i++)
        {
            for (j = 0; j < attributes.length; j++)
            {
                if (attributes[j].name === name[i])
                {
                    attr = attributes[j];
                    if (i < name.length - 1)
                    {
                        if (attr.type === 'Reference')
                        {
                            attributes = attr.referencedAttributeURIs
                                .map(findEntity, this)
                                .filter(excludeEmpty);
                        }
                        else if (attr.type === 'Nested')
                        {
                            attributes = attr.attributes;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    break;
                }
            }
        }
        return attr;
    },
    /**
     * Return is entity instance of super entity or not
     * @param uri {String} entity uri
     * @param superUri {String} super entity uri
     * @returns {boolean}
     */
    isEntityInstanceOf: function (uri, superUri)
    {
        var entityType;
        if (uri === superUri)
        {
            return true;
        }
        entityType = this.entityTypes[uri];
        while (entityType && entityType.extendsTypeURI)
        {
            if (entityType.uri === superUri)
            {
                return true;
            }

            entityType = this.entityTypes[entityType.extendsTypeURI];

            if (entityType.uri === superUri)
            {
                return true;
            }
        }
        return false;
    },

    getSubAttributes: function(attrType)
    {
        var attributes = null;
        if (attrType.analyticsAttributes)
        {
            attributes = attrType.analyticsAttributes;
        }
        else if(attrType.type === 'Nested')
        {
            attributes = attrType.attributes;
        }
        else if(attrType.type === 'Reference')
        {
            var entityType = this.getEntityType(attrType.referencedEntityTypeURI),
                relType = this.getRelationType(attrType.relationshipTypeURI);

            attributes = attrType.referencedAttributeURIs.map(function (uri) {
                return this.findEntityAttributeByUri(entityType, uri) || this.findRelationAttributeByUri(relType, uri);
            }, this).filter((function (a) {
                return !!a;
            }));
        }
        return attributes;
    },

    isAnalyticAttribute: function(attrType){
        if (Ext.isObject(attrType)){
            attrType = attrType.uri;
        }
        return Ext.isString(attrType) && attrType.indexOf('/analyticsAttributes/') !== -1;
    }
});
