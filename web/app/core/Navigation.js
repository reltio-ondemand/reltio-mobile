/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Navigation', {
    singleton: true,
    requires: [
        'MobileUI.core.Services',
        'MobileUI.core.util.HashUtil'
    ],
    SEARCH_PERSPECTIVE: 'com.reltio.mobile.search.General',
    DEFAULT_PERSPECTIVE: 'com.reltio.plugins.entity.default.DefaultPerspective',
    FADE_ANIMATION: {type: 'fade', duration: 500},
    FADE_FAST_ANIMATION: {type: 'fade', duration: 100},
    BACK_ANIMATION: {type: 'slide', direction: 'right'},
    FORWARD_ANIMATION: {type: 'slide', direction: 'left'},
    /**@type {Object} @private*/
    animationState: null,
    /**@type {Object} @private*/
    defaultAnimationState: null,
    /**@type {Number} @private*/
    historyLength: 0,
    /**@type {Array} @private*/
    previous: [],
    /**@type {Boolean} @private*/
    backAction: false,
    constructor: function (config)
    {
        this.initConfig(config);
        this.historyLength = 0;
        this.animationState = {};
        this.defaultAnimationState = {};
    },
    /**
     * Method returns is back button allow
     * @returns {boolean}
     */
    isBackAllow: function ()
    {
        return  Ext.os.is.ios7 ? MobileUI.app.getHistory().getActions().length > 1 : this.historyLength > 0;
    },

    isBackAction: function()
    {
        return this.backAction;
    },
    /**
     * Method redirects forward
     * @param {String} url
     * @param save {Boolean?} should be url saved or not;
     */
    redirectForward: function (url, save)
    {
        this.setAnimation('default', MobileUI.core.Navigation.FORWARD_ANIMATION);
        this.redirectTo(url, save);
    },

    addToPrevious: function (url) {
        var last = this.previous.length > 0 ? this.previous[this.previous.length - 1] : null;
        if (last) {
            if (last !== url) {
                this.previous.push(url);
            }
        } else {
            this.previous.push(url);
        }
    },
    /**
     * Method redirect to url in param
     * @param {String} url
     * @param save {Boolean?} should be url saved or not;
     * @param isBackAction {Boolean?} is it back action or not
     */
    redirectTo: function (url, save, isBackAction)
    {
        var record, decoded,
            history = MobileUI.app.getHistory(),
            actions = history.getActions();

        if (!url) {
            var cm = MobileUI.core.Services.getConfigurationManager();
            var plugin = cm.getExtensionById('com.reltio.mobile.common.General');
            var dashboard = cm.getExtensionById('com.reltio.mobile.dashboard.General');
            var defaultPath = (dashboard && dashboard.items) ? 'dashboard/init' : 'search/init';
            url = (plugin && plugin.defaultRoute) || defaultPath;
        }

        this.backAction = isBackAction || false;
        if (this.previous.length === 0)
        {
            if (actions.length >= 1 && actions[actions.length - 1].config)
            {
                this.addToPrevious(actions[actions.length - 1].config.historyUrl);
            }
        }
        if (['feedback','search/typeahead'].indexOf(url) === -1)
        {
            if (this.previous.length > 2)
            {
                this.previous.shift();
                this.addToPrevious(url);
            }
            else
            {
                this.addToPrevious(url);
            }
        }
        this.historyLength++;
        if (save === false)
        {
            if (Ext.data && Ext.data.Model && url instanceof Ext.data.Model)
            {
                record = url;
                url = record.toUrl();
            }
            decoded = MobileUI.app.getRouter().recognize(url);
            if (decoded)
            {
                decoded.url = url;
                if (record)
                {
                    decoded.data = {};
                    decoded.data.record = record;
                }
                MobileUI.app.dispatch(decoded, save);
            }
        }
        else
        {
            MobileUI.app.redirectTo(url);
        }
    },

    /**
     * Method redirect to default view
     * @param save {Boolean?} should be url saved or not;
     * @param isBackAction {Boolean?} is it back action or not
     */
    redirectToDefault: function(save, isBackAction) {
        this.redirectTo(null, save, isBackAction);
    },
    /**
     * Method sets default animation
     * @param state {String} state
     * @param value {Object} value
     */
    setDefaultAnimationState: function (state, value)
    {
        this.defaultAnimationState[state] = value;
    },

    /**
     * Method returns default animation
     * @param state {String}
     * @returns {Object | null}
     */
    getDefaultAnimationState: function (state)
    {
        return this.defaultAnimationState[state] || null;
    },

    /**
     * Set once animation
     * @param state {String} state
     * @param value {Object}
     */
    setAnimation: function (state, value)
    {
        this.animationState[state] = value;
    },

    /**
     * Method changes the current view to previous
     * @param state {String?} state
     * @param value {Object?}
     */
    back: function (state, value)
    {
        if (Ext.isEmpty(state) || Ext.isEmpty(value))
        {
            this.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
        }
        else
        {
            this.setAnimation(state, value);
        }
        this.backAction = true;
        this.historyLength--;
        this.previous.pop();
        if (Ext.os.is.ios7)
        {
            MobileUI.app.getHistory().back();
            MobileUI.app.getHistory().getActions().pop();
        }
        else
        {
            window.history.back();
        }
    },

    stepBack: function (state, value)
    {
        if (Ext.isEmpty(state) || Ext.isEmpty(value))
        {
            this.setAnimation('default', MobileUI.core.Navigation.BACK_ANIMATION);
        }
        else
        {
            this.setAnimation(state, value);
        }

        if (!Ext.os.is.ios7 && !Ext.isEmpty(this.previous))
        {
            this.historyLength--;
        }
        if (Ext.isEmpty(this.previous)) {
            this.redirectToDefault(false, true);
        }
        else {
            this.redirectTo(this.previous[this.previous.length - 1], false, true);
        }
    },
    /**
     * Method returns the current animation for state
     * @param state {String} state
     * @returns {*|Object|null}
     */
    getAnimation: function (state)
    {
        var value = this.animationState[state];
        delete this.animationState[state];
        return value || this.getDefaultAnimationState(state);
    },
    /**
     * @private
     * Method removes the all not necessary info from mobile hash
     * @param record {String}
     * @returns {*}
     */
    clearRecord: function (record)
    {
        var index = record.indexOf('entities');
        if (index !== -1)
        {
            return record.substring(index);
        }
        return record;
    },
    /**
     * Method switches to full version
     */
    switchToFull: function ()
    {
        var settings = MobileUI.core.Services.getGeneralSettings(),
            uiPath = settings.ui.replace(settings.debugPath, ''),
            state = MobileUI.app.getRouter().recognize(MobileUI.app.getHistory().getToken()),
            param = null, url,
            configuration = MobileUI.core.Services.getConfigurationManager(),
            extension, rawParams, entityUri;
        if (Ext.isObject(state))
        {
            switch (state.action)
            {
                case 'processSearch':
                    extension = configuration.getExtensionById(MobileUI.core.Navigation.SEARCH_PERSPECTIVE);
                    if (extension)
                    {
                        param = {p: extension.desktopPerspective};
                    }
                    break;
                case 'processProfile':
                case 'processRelations':
                case 'processRelationsTree':
                case 'processAttributesFacet':
                case 'processPlacesFacet':

                    rawParams = !Ext.isEmpty(state.args) ? this.clearRecord(state.args[0]).split('$') : [];
                    if (rawParams.length >= 2)
                    {
                        entityUri = rawParams.slice(0, 2).join('/');
                        if (entityUri === MobileUI.core.session.Session.getEntityUri())
                        {
                            extension = configuration.findPerspective(MobileUI.core.session.Session.getEntity().type);
                            extension = configuration.getExtensionById(extension);
                            param = {
                                p: (extension && extension.desktopPerspective) ? extension.desktopPerspective : MobileUI.core.Navigation.DEFAULT_PERSPECTIVE,
                                e: rawParams.slice(0, 2).join('%2F')
                            };
                        }
                    }
                    break;
            }
        }
        if (!param)
        {
            param = {p: MobileUI.core.Navigation.DEFAULT_PERSPECTIVE};
        }
        url = uiPath + settings.servlet.replace('/', '') + '/' + MobileUI.core.Services.getTenantName() + '/#' + MobileUI.core.util.HashUtil.stateToHash(param);
        this.redirectToUrl(url);
    },
    /**
     * Method redirect current page to another url
     * @param url
     */
    redirectToUrl: function (url)
    {
        if (Ext.os.is.iOS)
        {
            window.location = url;
        }
        window.location.href = url;
    }
});
