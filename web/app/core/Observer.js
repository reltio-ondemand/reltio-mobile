/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Observer', {
    requires: ['Ext.util.HashMap'],
    config: {
        threadNames: [],
        listener: Ext.emptyFn,
        self: null
    },

    constructor: function (config)
    {
        this.initConfig(config);
        this.mapThreads = this.getThreadNames().slice();
        if (this.mapThreads.length === 0)
        {
            var listener = this.getListener();
            if (listener)
            {
                setTimeout(listener.bind(this.getSelf()), 0);
            }
        }
    },
    /**@type {Ext.util.HashMap} @private*/
    mapThreads: null,

    /**
     * Method checks the thread and call listener if all threads are done
     * @param threadName
     * @returns {boolean}
     */
    checkThread: function (threadName)
    {
        var index = this.mapThreads.indexOf(threadName);
        if (index !== -1) {
            this.mapThreads.splice(index, 1);
        }
        if (this.mapThreads.length === 0)
        {
            if (this.getListener())
            {
                this.getListener().call(this.getSelf());
            }
            return true;
        }
        return false;
    },

    forceReady: function () {
        this.mapThreads.splice(0, this.mapThreads.length);
        if (this.getListener()) {
            this.getListener().call(this.getSelf());
        }
    },

    statics: {
        /**
         * @param loader {Function(item, next())} makes request and calls next() when done
         * @param equals {Function(item, item)?} returns true if items are equals
         * @return {Function(item|[item])} add items to queue
         * @constructor
         */
        lazyLoader: function(loader, equals) {
            var pending = false;
            var items = [];

            function check() {
                if (!pending) {
                    var item = items.shift();
                    if (item) {
                        pending = true;
                        loader(item, function() {
                            pending = false;
                            check();
                        });
                    }
                }
            }

            return function(addedItems) {
                items = items.concat(
                    [].concat(addedItems)
                        .filter(function(item) {
                            if (equals) {
                                return !items.some(function(addedItem) {
                                    return equals(addedItem, item);
                                });
                            }
                            else {
                                return items.indexOf(item) === -1;
                            }
                        })
                );

                check();
            };
        }
    }
});
