/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.PermissionManager', {
    requires: ['MobileUI.core.security.SecurityService'],
    statics: {
        ADMIN_TENANT_PREFIX: 'ROLE_ADMIN_',
        ADMIN_CUSTOMER_PREFIX: 'ROLE_ADMIN_CUSTOMER_',
        ADMIN_ROLE: 'ROLE_ADMIN',
        /**
         * @type {MobileUI.core.security.SecurityService}
         */
        security: null,
        /**
         * Method returns the security serivce
         * @returns {MobileUI.core.security.SecurityService}
         */
        securityService: function()
        {
            if (!this.security)
            {
                this.security = new MobileUI.core.security.SecurityService();
            }
            return this.security;
        }
    }
});