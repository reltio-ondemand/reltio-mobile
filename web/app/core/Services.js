/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Services', {
    requires: ['MobileUI.core.util.Util',
        'MobileUI.core.configuration.ConfigurationManager',
        'MobileUI.core.search.SearchResultBuilder',
        'MobileUI.core.Logger'
    ],
    statics: {
        SERVICES_URL: 'services/',
        /**@type {String}*/
        tenantName: null,
        /**@type {String}*/
        environmentName: null,
        /**@type {String}*/
        environmentUrl: null,
        /**@type {Object}*/
        settings: null,
        /**@type {MobileUI.core.configuration.ConfigurationManager}*/
        configurationManager: null,
        /**@type {MobileUI.core.search.SearchResultBuilder}*/
        searchResultBuilder: null,
        /**@type {String} @private */
        state: null,
        /**
         * Method return configuration manager
         * @returns {MobileUI.core.configuration.ConfigurationManager}
         */
        getConfigurationManager: function ()
        {
            if (this.configurationManager === null)
            {
                this.configurationManager = Ext.create('MobileUI.core.configuration.ConfigurationManager');
            }
            return this.configurationManager;
        },

        /**
         * Method return configuration manager
         * @returns {MobileUI.core.search.SearchResultBuilder}
         */
        getSearchResultBuilder: function ()
        {
            if (this.searchResultBuilder === null)
            {
                this.searchResultBuilder = Ext.create('MobileUI.core.search.SearchResultBuilder');
            }
            return this.searchResultBuilder;
        },

        /**
         * Method returns tenant name based on url
         * If tenant name is empty the empty string will be returned
         * @returns {string}
         */
        getTenantName: function ()
        {
            if (this.tenantName)
            {
                return this.tenantName;
            }

            var locationPath = this.getLocationObject().pathname,
                pathArray = locationPath.split('/'),
                result = '',
                lastElement;

            // examples: /m, /m/,  /m/tenant, /m/tenant/,  /m/tenant/index.html
            // we agreed to fix the servlet path to contain only one element (/m)
            if (pathArray.length > 2)
            {
                lastElement = pathArray[pathArray.length - 1];
                if (lastElement === '') 
                {
                    if (pathArray.length > 3)
                    {
                        result = pathArray[pathArray.length - 2];
                    }
                }
                else if (lastElement.indexOf('.htm') !== -1)
                {
                    result = pathArray[pathArray.length - 2];
                }
                else
                {
                    result = lastElement;
                }
            }
            this.tenantName = result;
            return result;
        },
        getEnvironmentName: function() {
            if (!this.environmentName) {
                var ui = MobileUI.core.Services.getGeneralSettings().apiPath;
                if (ui) {
                    var protocol = ui.indexOf('://');
                    if (protocol !== -1) {
                        ui = ui.substr(protocol + 3);
                    }
                    this.environmentName = ui.split('.')[0];
                }
            }

            return this.environmentName;
        },
        getEnvironmentUrl: function() {
            if (!this.environmentUrl) {
                var apiUrl = MobileUI.core.Services.getGeneralSettings().apiPath;
                if (apiUrl) {
                    var apiUrlParts = apiUrl.split('://');
                    var protocol = apiUrl.indexOf('://') !== -1 ? (apiUrlParts[0] + '://') : '';
                    var apiPath = protocol ? apiUrlParts[1] : apiUrlParts[0];
                    this.environmentUrl = protocol + apiPath.split('/')[0];
                }
            }

            return this.environmentUrl;
        },
        /**
         * Method allows to check is tenant exists or not
         * @param tenantName {String} tenant name
         * @param success {Function?} success callback
         * @param failure {Function?} failure callback
         * @param self {Object?} self object
         */
        checkTenant: function (tenantName, success, failure, self)
        {
            MobileUI.core.session.Session.sendRequest('/configuration/roles', {tenant: tenantName}, null, success, failure, self);
        },

        /**
         * return the mobile settings
         * @returns {Object | null}
         */
        getGeneralSettings: function ()
        {
            return this.settings;
        },
        /**
         * return the api path
         * @return {String}
         */
        getApiPath: function ()
        {
            return this.settings ? this.settings.apiPath : '';
        },
        /**
         * Method updates the settings
         */
        updateSettings: function (callback, self)
        {
            var that = this;
            Ext.Ajax.request({
                url: 'configuration?scheme=' + window.location.protocol.replace(':', ''),
                success: function (xhr)
                {
                    that.settings = Ext.decode(xhr.responseText);
                    that.requestUISettings(callback, self);
                    MobileUI.core.Logger.init();
                },
                failure: function (xhr)
                {
                    MobileUI.core.Logger.log(xhr);
                },
                method: 'GET'
            });
        },
        /**
         * Method return the general ui settings
         * @protected
         * @param callback {Function} callback function
         * @param self {Object} self object
         */
        requestUISettings: function (callback, self)
        {
            var tenant = this.getTenantName() || '',
                url = '/configuration?tenant=' + encodeURIComponent(tenant);
            MobileUI.core.Services.asyncRequest(url, function (response) {
                    this.settings = Ext.Object.merge(this.settings, response);
                    callback.call(self);
                },
                function (xhr) {
                    MobileUI.core.Logger.log(xhr);
                },
                {
                    signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
                },
                this
            );
        },
        /**
         * Method returns the window.location object
         * @returns {object}
         */
        getLocationObject: function ()
        {
            return window.location;
        },

        /**
         * Method returns the full url to service
         * @param service {String} service name
         * @returns {string}
         */
        getFullUrl: function (service)
        {
            return MobileUI.core.Services.getGeneralSettings().ui + this.SERVICES_URL + service.replace(/^\//g, '');
        },
        /**
         * Method allows to send async request
         * @param service {String} service url
         * @param success {Function?} callback on success
         * @param failure {Function?} callback on failure
         * @param options {Object?} options object
         * @param self {Object?} self object
         */
        asyncRequest: function (service, success, failure, options, self)
        {
            var url;
            options = options || {};
            options.headers = options.headers || {};
            if (options.signer)
            {
                options.signer(options.headers);
            }
            url = MobileUI.core.Services.getGeneralSettings().ui + this.SERVICES_URL + service.replace(/^\//g, '');
            Ext.Ajax.request({
                url: url,
                headers: options.headers,
                success: function (xhr)
                {
                    if (success)
                    {
                        var result;
                        try
                        {
                            result = Ext.decode(xhr.responseText);
                        }
                        catch (e)
                        {
                            result = xhr.responseText;
                        }
                        success.call(self, result, xhr);
                    }
                },
                failure: function (xhr)
                {
                    if (failure)
                    {
                        failure.call(self, Ext.decode(xhr.responseText), xhr);
                    }
                },
                params: options.data,
                method: options.method || 'GET'
            });
        },
        /**
         * Method save the current url state
         * @param state {String} state url
         */
        setState: function (state)
        {
            this.state = state;
        },

        /**
         * Method returns saved state url
         * @returns {String|null}
         */
        getState: function ()
        {
            return this.state;
        },
        /**
         * Method sets the user preference
         * @param name {String} setting name
         * @param value {Object} value
         * @param tenant {String} tenant name
         * @param success {Function?} callback on success
         * @param failure {Function?} callback on failure
         * @param self {Object?} self object
         * @param counter {Number?} counter object
         */
        setUserPreference: function (name, value, tenant, success, failure, self, counter)
        {
            counter = counter || 0;
            MobileUI.core.session.Session.callMethodWhenTokenAvailable(
                function (name, value, tenant, success, failure, self, counter)
                {
                    var data = {
                        name: name,
                        environment: location.host,
                        tenant: tenant,
                        values: [Ext.JSON.encode(value)]
                    };

                    this.asyncRequest('/preferences', function (result)
                    {
                        if (success)
                        {
                            success.call(self || window, result);
                        }
                    }, function (error, xhr)
                    {
                        if (xhr && xhr.status === 401)
                        {
                            if (this.counter < 3)
                            {
                                MobileUI.core.session.Session.setExpired(0);
                                MobileUI.core.Services.setUserPreference(this.name, this.value, this.tenant, this.success, this.failure, this.self, this.counter + 1);
                                return;
                            }
                        }
                        MobileUI.core.Logger.log(error);
                        if (failure)
                        {
                            failure.call(self || window, error);
                        }
                    }, {
                        method: 'POST',
                        data: Ext.JSON.encode(data),
                        signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
                    }, {
                        name: name,
                        value: value,
                        tenant: tenant,
                        success: success,
                        failure: failure,
                        self: self,
                        counter: counter
                    });
                }, null, [name, value, tenant, success, failure, self, counter], this);
        },
        /**
         * Method returns the user preference
         * @param name {String} setting name
         * @param tenant {String} tenant name
         * @param success {Function?} callback on success
         * @param failure {Function?} callback on failure
         * @param self {Object?} self object
         * @param counter {Number?} counter
         */
        getUserPreference: function (name, tenant, success, failure, self, counter)
        {
            counter = counter || 0;
            MobileUI.core.session.Session.callMethodWhenTokenAvailable(
                function(name, tenant, success, failure, self, counter)
                {
                    this.asyncRequest('/preferences', function (result)
                        {
                            var value = null;
                            if (!result)
                            {
                                MobileUI.core.Logger.error('Preferences could not be retrieved');
                            }
                            else
                            {
                                var values = result && result['result'] && result['result'][0] ? result['result'][0].values : null;
                                if (Ext.isArray(values) && values.length > 0)
                                {
                                    value = Ext.JSON.decode(values[0]);
                                }
                            }
                            if (success)
                            {
                                success.call(self || window, value);
                            }
                        }, function (error, xhr)
                        {
                            if (xhr && xhr.status === 401)
                            {
                                if (this.counter < 3)
                                {
                                    MobileUI.core.session.Session.setExpired(0);
                                    MobileUI.core.Services.getUserPreference(this.name, this.tenant, this.success, this.failure, this.self, this.counter + 1);
                                    return;
                                }
                            }
                            MobileUI.core.Logger.log(error);
                            if (failure)
                            {
                                failure.call(self || window, error);
                            }
                        },
                        {
                            parse: 'json',
                            method: 'GET',
                            data: 'name=' + name + '&tenant=' + tenant + '&environment=' + location.host,
                            signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
                        }, {
                            name: name,
                            tenant: tenant,
                            success: success,
                            failure: failure,
                            self: self,
                            counter: counter
                        });
                },
                null,
                [name, tenant, success, failure, self, counter],
                this);
        },

        /**
         * @param fallbackUrl {String}
         * @return {String} 
         */
        getBorderlessImageUrl: function(fallbackUrl) {
            return MobileUI.core.Services.getFullUrl('images') + 
                '?def=' + encodeURIComponent(fallbackUrl);
        },

        /**
         * Upload images via 'image_uploader' backend service
         * @param images {Array} array of files
         * @param repeatCount {number} repeat count
         * @param success {Function} success function
         * @param failure {Function} failure function
         * @param self {Object} context object
         */
        uploadImages: function(images, repeatCount, success, failure, self){
            repeatCount = repeatCount === undefined ? 3 : repeatCount;
            var data = (images || []).reduce(function (formData, file) {
                formData.append('file', file);
                return formData;
            }, new FormData());
            failure = (failure || Ext.emptyFn).bind(this);
            MobileUI.core.session.Session.callMethodWhenTokenAvailable(MobileUI.core.Services.uploadImagesInternal, failure, [data, repeatCount, success, failure, self]);
        },

        uploadImagesInternal: function(formData, repeatCount, success, failure, self){
            var xhr = new XMLHttpRequest();
            xhr.onload = function(event){
                var result;
                try {
                    result = JSON.parse(event.target.responseText);
                } catch (e) {
                    failure.call(self,e);
                }

                if (result) {
                    if (event.target.status === 200) {
                        success.call(self,result);
                    } else {
                        failure.call(self, event);
                    }
                }
            };
            xhr.repeat = repeatCount;
            xhr.onerror = function (e) {
                if (xhr.repeat > 0) {
                    MobileUI.core.Services.uploadImagesInternal(formData, --xhr.repeat,  success, failure, self);
                } else {
                    failure.call(self,e);
                }
            };
            xhr.open('post', MobileUI.core.Services.getGeneralSettings().ui + MobileUI.core.Services.SERVICES_URL + 'image_uploader');
            xhr.setRequestHeader('Authorization', 'Bearer ' + MobileUI.core.session.Session.getAccessToken());

            xhr.send(formData);
        }
    }
});
