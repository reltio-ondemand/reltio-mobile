/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.SuiteManager', {
    xtype: 'suitemanager',
    singleton: true,
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.view.dialogs.RelationshipType',
        'MobileUI.view.dialogs.CreateEntity',
        'MobileUI.view.dialogs.ChooseEntity',
        'MobileUI.view.dialogs.NestedAttributeEditor',
        'MobileUI.view.dialogs.ReferenceAttributeEditor',
        'MobileUI.components.MessageBox'
    ],
    mixins: {
        obs: 'Ext.mixin.Observable'
    },
    dialogs: {},
    activeItem: null,
    counter: 0,
    suiteHistory: [],
    state: [],
    getCurrentState: function ()
    {
        return this.state.length > 0 ? this.state[this.state.length - 1] : null;
    },
    getStates: function ()
    {
        return this.state;
    },
    clearState: function ()
    {
        this.state = [];
    },
    putState: function (key, state)
    {
        this.state.push({key: key, state: state});
    },
    getState: function (key)
    {
        this.state.filter(function (item)
        {
            return item.key === key;
        });
    },
    popState: function ()
    {
        return this.state.pop();
    },
    init: function ()
    {

    },
    /**
     * Method adds dialog to suite manager
     * @param name {String} dialog name
     * @param panel {Object} panel
     */
    addDialog: function (name, panel)
    {
        if (this.hasDialog(name))
        {
            return;
        }
        this.dialogs[name] = panel;
    },
    
    removeDialog: function(name){
        delete this.dialogs[name];
    },

    /**
     * Returns true if dialof already exists
     * @param name {String} dialog name
     */
    hasDialog: function (name)
    {
        var dialog = this.dialogs[name];
        return !Ext.isEmpty(dialog) && !dialog.isDestroyed;
    },

    isDialog: function(dialog) {
        var name = dialog && dialog.getName && dialog.getName();
        var dialogs = this.dialogs;
        return name && Object.keys(this.dialogs).some(function(d) { return dialogs[d].getName() === name; });
    },

    /**
     * Shows dialog by it's name
     * @param name {String} name
     * @param dontIncreaseCounter {Boolean?} dont increase counter flag
     */
    showDialog: function (name, dontIncreaseCounter)
    {
        var dialog = this.dialogs[name],
            animation;
        if (dialog)
        {
            Ext.Viewport.add(dialog);
        }
        if (dontIncreaseCounter !== true)
        {
            this.suiteHistory.push(name);
        }
        animation = {
            duration: 500,
            type: 'fade'
        };
        this.activeItem = Ext.Viewport.getActiveItem();
        Ext.Viewport.getLayout().setAnimation(animation);
        Ext.Viewport.on({
            activeitemchange: 'onAfterAnimate',
            scope: {self:this, name: name},
            single: true,
            order: 'after'
        });
        Ext.Viewport.setActiveItem(dialog);
    },

    onAfterAnimate: function ()
    {
        this.self.fireEvent('dialogActivated', {name: this.name});
    },

    back: function ()
    {
        if(this.suiteHistory.length === 1) {
            this.hideDialogs(true);
        }
        else {
            this.suiteHistory.pop();
            var name = this.suiteHistory[this.suiteHistory.length - 1];
            this.showDialog(name, true);
        }
    },

    popDialogHistory: function() {
        this.suiteHistory.pop();
    },

    /**
     * Method hides all the dialogs
     */
    hideDialogs: function (noConfirmation, callback, self)
    {
        if (noConfirmation)
        {
            this.hideDialogsInternal();
        }
        else
        {
            MobileUI.components.MessageBox.show({
                title: i18n('Warning'),
                message: i18n('Are you sure you want to close the dialog?'),
                buttons: [{text: i18n('No'),  itemId: 'no'}, {text: i18n('Yes'), itemId: 'yes', ui: 'action'}],
                promptConfig: false,
                scope: this,
                fn: function (buttonId) {
                    if (buttonId === 'yes') {
                        this.hideDialogsInternal();
                        if (callback)
                        {
                            callback.apply(self);
                        }
                    }
                }
            });
        }

    },

    isAllowBackButton: function ()
    {
        return this.state.length > 0;
    },
    /**
     * Method hides all the dialogs
     */
    hideDialogsInternal: function ()
    {
        var name;
        for (name in this.dialogs)
        {
            if (this.dialogs.hasOwnProperty(name))
            {
                Ext.Viewport.remove(this.dialogs[name], true);
            }
        }
        this.dialogs = {};
        this.suiteHistory = [];
        this.clearState();
        this.fireEvent('hideDialogs');
        Ext.Viewport.setActiveItem(0);
    }
});
