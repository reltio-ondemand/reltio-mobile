/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.UriMapping', {

    constructor: function () {
        this.clearMapping();
    },

    mapping:null,
    newMapping: null,

    registerMapping: function(uri, parent){
        uri = (uri || '').replace(new RegExp('/attributes/','g'),'/');
        parent = (parent || '').replace(new RegExp('/attributes/','g'),'/');
        this.mapping[uri]  = parent;
    },
    register: function(uri, parent){
        this.newMapping[uri] = parent;
        this.registerMapping(uri, parent);
    },
    resolve: function(uri){
        var result = Object.keys(this.newMapping)
            .filter(function (mappingUri) {
                return uri.indexOf(mappingUri) === 0;
            })
            .map(function (mappingUri) {
                return MobileUI.core.util.Util.replace(uri, mappingUri, this.newMapping[mappingUri]);
            }, this);

        return Ext.isEmpty(result) ? uri : this.resolve(result[0]);
    },
    clearMapping: function(){
        this.mapping = {};
        this.newMapping = {};
    },

    resolveMapping: function(uri){
        uri = (uri || '').replace(new RegExp('/attributes/','g'),'/');

        var result = Object.keys(this.mapping)
            .filter(function (mappingUri) {
                return uri.indexOf(mappingUri) === 0;
            })
            .map(function (mappingUri) {
                return MobileUI.core.util.Util.replace(uri, mappingUri, this.mapping[mappingUri]);
            }, this);

        return Ext.isEmpty(result) ? uri : this.resolveMapping(result[0]);
    },



    resolveSubMapping: function (attributeUri) {
        var latest = function (arr) {
            return Ext.isEmpty(arr) ? null : arr[arr.length - 1];
        };
        if (!attributeUri) {
            return null;
        }
        var entityUri = latest(attributeUri.split('/'));
        var result = null;
        if (entityUri && this.mapping['entities/' + entityUri]) {
            result = this.mapping['entities/' + entityUri].split('/');
            result = MobileUI.core.util.Util.replace(attributeUri, entityUri, latest(result));
        }
        return result;
    }

});
