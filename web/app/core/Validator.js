/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.Validator', {
    requires: ['MobileUI.core.entity.EntityUtils', 'Ext.mixin.Observable'],
    mixins: {observable: 'Ext.mixin.Observable'},

    constructor: function (config) {
        this.initConfig(config);
        this.resetValidators();
    },
    refUriCompare: function(attrValueUri, uri){
        attrValueUri = (attrValueUri || '').replace(/entities/g,'');
        uri = (uri || '').replace(/entities/g,'');
        return uri.length > 0 && uri.indexOf(attrValueUri) > 0 && uri.indexOf(attrValueUri) === uri.length-attrValueUri.length;
    },
    findAttribute: function (uri, value, entityType, path) {

        var mapping = MobileUI.core.session.Session.getUriMapping();

        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                var attrValues = value[key];
                for (var i = 0; i < attrValues.length; i++) {
                    var attrValue = attrValues[i];
                    var attrName = attrValue.type ?
                        MobileUI.core.Metadata.findEntityAttributeByUri(null, attrValue.type) || MobileUI.core.Metadata.findRelationAttributeByUri(null, attrValue.type)
                        : MobileUI.core.Metadata.findEntityAttributeByName(entityType, key);
                    var label;
                    if (attrName && attrName.type === 'Nested') {
                        label = MobileUI.core.entity.EntityUtils.evaluateLabel(null, attrValue.value, attrName) || i18n('<No label>');
                    }
                    else {
                        label = attrValue.refEntity ? (attrValue.label || i18n('<No label>')) : '';
                    }
                    label = Ext.util.Format.htmlEncode(label || attrValue.label || attrValue.value);
                    if (mapping.resolveMapping(MobileUI.core.entity.EntityUtils.tmpUriFromApiFormat(attrValue.uri)) === mapping.resolveMapping(uri) || this.refUriCompare(attrValue.uri, uri) || mapping.resolveSubMapping(uri) === attrValue.uri) {
                        path.push({
                            label:label,
                            attrName: attrName ? attrName.label : key
                        });
                        return path;
                    } else {
                        var newPath = Ext.clone(path);
                        if (Ext.isObject(attrValue.value)) {
                            newPath.push({
                                label:label,
                                attrName: attrName ? attrName.label : key
                            });
                            var result = this.findAttribute(uri, attrValue.value, entityType, newPath);
                            if (result) {
                                return result;
                            }
                        }
                    }
                }
            }
        }
    },
    findPath: function (uri, entity) {
        return this.findAttribute(uri, entity.attributes, entity.type, []);
    },
    updateExternalValidationResults: function (record) {
        var ValidationCodes = MobileUI.core.session.MValidation.VALIDATION_CODES;
        switch (record.invalidType) {
            case ValidationCodes.CUSTOM:
            case ValidationCodes.INCORRECT_VALUE:
                if (record.objectUri) {
                    this.externalIncorrect[record.objectUri] = {
                        message: record.value,
                        type: record.invalidType
                    };
                } else if (record.attrType){
                    this.externalAttrTypeMessage[record.attrType.uri] = {
                        message: record.value,
                        type: record.invalidType
                    };
                }
                break;
            case ValidationCodes.MISSED:
                if (record.objectParentUri) {
                    this.externalIncorrect[record.objectParentUri] = {
                        message: record.value,
                        type: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE
                    };
                }
        }
    },
    collectHeaderErrors: function (uri) {
        var result = {};
        if (this.requiredTypes[uri]) {
            result.error = {
                message: i18n('Requires a value'),
                type: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED
            };
        }
        if (this.externalAttrTypeMessage[uri]){
            result.error = this.externalAttrTypeMessage[uri];
        }
        return Object.keys(result).length === 0 ? null : result;
    },
    collectAttributeErrors: function (uri, attrType) {
        var result = {};
        var mapping = MobileUI.core.session.Session.getUriMapping();
        if (attrType && this.requiredTypes[attrType.uri]) {
            result.error = this.requiredTypes[attrType.uri];
        }
        Object.keys(this.externalIncorrect).map(function(uri){
            return {old: uri, mapped: mapping.resolveMapping(uri)};
        }).filter(function(mappedUri){
            return mappedUri.mapped === mapping.resolveMapping(uri) || this.refUriCompare(uri, mappedUri.old) || mapping.resolveSubMapping(mappedUri.old) === uri;
        }, this).forEach(function(mappedUri){
            result.error = this.externalIncorrect[mappedUri.old];
        },this);

        if (attrType && this.externalAttrTypeMessage[attrType.uri] && !MobileUI.core.entity.EntityUtils.isComplex(attrType)){
            result.error = this.externalAttrTypeMessage[attrType.uri];
        }

        return Object.keys(result).length === 0 ? null : result;
    },
    validateRequired: function (attributes, records, entity) {
        var requiredAttributes = attributes.filter(function (attr) {
            return MobileUI.core.CardinalityChecker.isRequired(attr) && !attr.hidden;
        });
        requiredAttributes.forEach(function (attribute) {
            delete this.requiredTypes[attribute.uri];
        }, this);

        var firstRequired;
        var headers = {};
        var isNestedEmpty = function (recordUri) {
            var hasChild = false;
            var j;
            for (j = 0; j < entity.editedAttributes.length; j++) {
                if (entity.editedAttributes[j].parentUri === recordUri && (entity.editedAttributes[j] !== '')) {
                    hasChild = true;
                    break;
                }
            }
            for (j = 0; j < entity.attributes.length; j++) {
                if (entity.attributes[j].uri === recordUri) {
                    if (Object.keys(entity.attributes[j].value).length > 0) {
                        hasChild = true;
                    }
                    break;
                }
            }
            return !hasChild;
        };
        records.forEach(function (record) {
            if (record.get('userData')) {
                delete this.requiredAttributes[record.get('userData').editingUri];
            }
            var required = record.get('required');
            var isHeader = (record.get('type') === MobileUI.components.list.items.BaseListItem.types.header);
            if (isHeader) {
                headers[record.get('editor').uri] = record;
            }
            required = required && !isHeader;
            var label = record.get('label');
            if (required) {
                var type = record.get('editor').type;
                var excluded;
                if (type === 'String') {
                    excluded = (label || '').trim().length > 0;
                } else {
                    excluded = label;
                }
                if (excluded) {
                    var uri = record.get('editor').uri;
                    var toRemove = requiredAttributes
                        .filter(function (attr) {
                            return attr.uri === uri;
                        })
                        .filter(function (attr) {
                            return attr.type !== 'Nested' || attr.type === 'Nested' && !isNestedEmpty(record.get('userData').uri);
                        });
                    toRemove.forEach(function (attr) {
                        var index = requiredAttributes.indexOf(attr);
                        if (index > -1) {
                            requiredAttributes.splice(index, 1);
                        }
                    });

                }
                else {
                    firstRequired = firstRequired || record;
                    this.requiredAttributes[record.get('userData').editingUri] = true;
                }
            }
        }, this);

        requiredAttributes.forEach(function (attribute) {
            this.requiredTypes[attribute.uri] = true;
        }, this);
        return requiredAttributes;
    },

    resetValidators: function () {
        this.requiredAttributes = {};
        this.requiredTypes = {};
        this.externalIncorrect = {};
        this.externalAttrTypeMessage ={};
    }

});
