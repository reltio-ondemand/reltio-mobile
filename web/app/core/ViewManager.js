/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.ViewManager', {
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.view.Search',
        'MobileUI.view.SearchTypeAhead',
        'MobileUI.view.SearchFacet',
        'MobileUI.view.Profile',
        'MobileUI.view.Relations',
        'MobileUI.view.Feedback',
        'MobileUI.view.ErrorPage'
    ],
    mixins: {observable: 'Ext.mixin.Observable'},

    /**@type {MobileUI.components.Menu}
     * @private */
    menu: null,

    /**@type {String}
     * @private */
    prevViewName: null,
    /**
     * Method allows to show the view with specific xtype
     * @private
     * @param viewName {String} view name
     */
    showViewWithMenu: function (viewName)
    {
        if (this.menu === null)
        {
            this.menu = Ext.create('MobileUI.components.Menu');
            Ext.Viewport.element.on({
                tap: function ()
                {
                    var that = this;
                    if (that.menu.isShown())
                    {
                        Ext.Viewport.hideMenu('left');
                    }
                },
                scope: this
            });
        }
        Ext.Viewport.setMenu(this.menu, {
            side: 'left',
            reveal: true
        });
        Ext.Viewport.hideAllMenus();
        Ext.Viewport.on({
            activeitemchange: 'onAfterAnimate',
            scope: this,
            single: true,
            order: 'after'
        });

        Ext.Viewport.add([{
            xtype: viewName
        }]);
        this.updateAnimation();
        this.setActiveItem(viewName);
    },
    /**
     * Method sets menu is shown or not
     */
    isMenuShown: function ()
    {
        return this.menu.isShown();
    },
    initialize: function(){
        var onDocumentRootClick = this.onDocumentRootClick.bind(this);
        window.document.addEventListener('click', onDocumentRootClick);
        window.document.addEventListener('touchmove', onDocumentRootClick);
    },
    onDocumentRootClick: function (event) {
        this.fireEvent('rootClicked', event);
    },
    getMenu: function() {
        return this.menu;
    },
    /**
     * Method sets active item
     * @private
     * @param viewName {String} active item
     */
    setActiveItem: function (viewName)
    {
        if (this.prevViewName === viewName)
        {
            Ext.Viewport.setActiveItem(Ext.Viewport.getItems().length - 1);
        }
        else
        {
            Ext.Viewport.setActiveItem(viewName);
        }
        this.prevViewName = viewName;
    },

    /**
     * Method updates animation
     * @private
     */
    updateAnimation: function ()
    {
        var animation = MobileUI.core.Navigation.getAnimation('default') || {
                duration: 1,
                type: 'fade'
            };
        Ext.Viewport.getLayout().setAnimation(animation);
    },

    /**
     * Method removes all non active views
     * @private
     */
    onAfterAnimate: function ()
    {
        var itemsToRemove, activeId, i;
        activeId = Ext.Viewport.getActiveItem().getId();
        itemsToRemove = Ext.Viewport.getItems().items.filter(function (item)
        {
            return item.getId() !== activeId;
        }) || [];

        for (i = 0; i < itemsToRemove.length; i++)
        {
            Ext.Viewport.remove(itemsToRemove[i], true);
        }
        this.fireEvent('activated');
    },

    /**
     * Method allow to show the view with specific xtype
     * @private
     * @param viewName {String} view name
     */
    showViewWithoutMenu: function (viewName)
    {
        Ext.Viewport.add([{
            xtype: viewName
        }]);
        Ext.Viewport.on({
            activeitemchange: 'onAfterAnimate',
            scope: this,
            single: true,
            order: 'after'
        });
        this.updateAnimation();
        this.setActiveItem(viewName);
    },
    /**
     * Method return true if on the switch window the activate event will be fired
     * @returns {boolean}
     */
    waitForActivate: function ()
    {
        return Ext.Viewport.getItems().length > 1;
    },
    /**
     * @param parameters {{view: string, showMenu: boolean}}
     */
    show: function (parameters)
    {
        if (parameters)
        {
            var view = parameters.view + 'view';
            if (parameters.showMenu)
            {
                this.showViewWithMenu(view);
            }
            else
            {
                this.showViewWithoutMenu(view);
            }
        }
    },
    /**
     * Method returns device prefix
     * @return {String}
     */
    getDevicePrefix: function ()
    {
        return MobileUI.app.getCurrentProfile().getName().toLowerCase();
    }
});
