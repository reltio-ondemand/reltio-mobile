/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.configuration.ConfigurationManager', {

    constructor: function (config)
    {
        this.initConfig(config);
        this.plugins = [];
    },

    /**@type {Array}*/
    plugins: null,

    /**
     * @protected
     * @param configurationObject {object} configuration object
     */
    parseConfiguration: function (configurationObject)
    {
        var pluginNamesSet = {},
            temp = configurationObject,
            lastTempLength = temp.length,
            p, i, j, ready, config, oldConfig;

        while (temp.length > 0)
        {
            p = temp;
            temp = [];
            for (i = 0; i < p.length; i++)
            {
                config = p[i];
                ready = true;
                if (config.requirements !== undefined)
                {
                    for (j = 0; j < config.requirements.length; j++)
                    {
                        if (pluginNamesSet[config.requirements[j]] === undefined)
                        {
                            ready = false;
                            break;
                        }
                    }
                }
                if (ready)
                {
                    if (pluginNamesSet[config.id] !== undefined)
                    {
                        oldConfig = this.plugins[pluginNamesSet[config.id]];
                        if (this.versionCompare(oldConfig.version, config.version) > 0)
                        {
                            this.plugins.splice(pluginNamesSet[config.id], 1);
                            this.plugins.push(config);
                            pluginNamesSet[config.id] = this.plugins.length - 1;
                        }
                    }
                    else
                    {
                        this.plugins.push(config);
                        pluginNamesSet[config.id] = this.plugins.length - 1;
                    }
                }
                else
                {
                    temp.push(config);
                }
            }
            if (lastTempLength === temp.length)
            {
                break;
            }
            lastTempLength = temp.length;
        }

    },
    /**
     * Method allows to compare plugin version
     * @param plugin1 {Object} first plugin
     * @param plugin2 {Object} second plugin
     * @returns {*}
     */
    versionCompare: function (plugin1, plugin2)
    {
        var v1, v2, n, i, delta;
        if (plugin1.version === undefined && plugin2.version === undefined)
        {
            return 0;
        }
        if (plugin1.version === undefined && plugin2.version !== undefined)
        {
            return -1;
        }
        if (plugin1.version !== undefined && plugin2.version === undefined)
        {
            return 1;
        }

        v1 = plugin1.version.split('.');
        v2 = plugin2.version.split('.');
        n = Math.min(v1.length, v2.length);
        for (i = 0; i < n; i++)
        {
            delta = Number(v1[i]) < Number(v2[i]);
            if (delta !== 0)
            {
                return delta;
            }
        }
        return v1.length - v2.length;
    },

    /**
     * method resolves configuration from the servlet
     * @param tenant {String} tenant name
     * @param success {function?} listener on success
     * @param fail {function?} listener on fail
     * @param self {object?} the context
     */
    initConfiguration: function (tenant, success, fail, self)
    {
        MobileUI.core.session.Session.callMethodWhenTokenAvailable(
            function (tenant, success, fail, self)
        {
            MobileUI.core.Services.asyncRequest('/mobile?tenant=' + tenant, function (response) {
                    this.parseConfiguration(response.plugins);
                    if (response.username) {
                        MobileUI.core.session.Session.setUserName(response.username);
                    }
                    if (success) {
                        success.call(self, response.plugins);
                    }
                },
                function (xhr) {
                    if (fail) {
                        fail.call(self, xhr.responseText);
                    }
                },
                {
                    signer: Ext.Function.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
                },
                this
            );
        }, null, [tenant, success, fail, self], this);
    },

    /**
     * Method returns extension by id
     * Return null if extension not found
     * @param id
     * @returns {Object|null}
     */
    getExtensionById: function (id)
    {
        var plugins = this.plugins, i, j;

        for (i = 0; i < plugins.length; i++)
        {
            if (plugins[i].extensions)
            {
                for (j = 0; j < plugins[i].extensions.length; j++)
                {
                    if (id === plugins[i].extensions[j].id)
                    {
                        return plugins[i].extensions[j];
                    }
                }
            }
        }
        return null;
    },

    /**
     * Method returns extensions by point
     * Method returns empty array if extensions not found.
     * @param point {String} extension point
     * @returns {Array}
     */
    getExtensionsByPoint: function (point)
    {
        var extensions = [],
            plugins = this.plugins,
            i, j;

        for (i = 0; i < plugins.length; i++)
        {
            if (plugins[i].extensions)
            {
                for (j = 0; j < plugins[i].extensions.length; j++)
                {
                    if (point === plugins[i].extensions[j].point)
                    {
                        extensions.push(plugins[i].extensions[j]);
                    }
                }
            }
        }
        return extensions;
    },

    /**
     * Method finds perspective by entity type uri
     * @param entityTypeUri {String}
     * @returns {Object }
     */
    findPerspective: function(entityTypeUri)
    {
        var perspectives = this.getExtensionsByPoint('com.reltio.plugins.ui.perspective'),
            defaultPerspective, i;
        for(i = 0; i < perspectives.length; i++)
        {
            if(entityTypeUri === perspectives[i]['entityType'])
            {
                return perspectives[i].id;
            }
            else if('*' === perspectives[i]['entityType'])
            {
                defaultPerspective = perspectives[i].id;
            }
        }

        return defaultPerspective;
    }

});
