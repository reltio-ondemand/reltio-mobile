/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.entity.ChartUtils', {
    statics: {
        processChart: function (attributes, items) {
            var attribute, labelSource, valueSource, group, result;
            result = {};
            result.type = MobileUI.components.list.items.BaseListItem.types.chart;

            if (Ext.isArray(attributes) && attributes.length > 0) {
                attribute = attributes[0];
            }
            if (attribute) {
                labelSource = attribute.chartContent.firstDimension;
                valueSource = attribute.chartContent.values;
                group = attribute.chartContent.secondDimension;
                result.chartOptions = attribute.chartContent.options || {};
                if (group) {
                    result.chartOptions.dimension = '2';
                    result.chartData = this.processTwoDimensionChart(labelSource, valueSource, group, attribute.value);
                } else {
                    result.chartOptions.dimension = '1';
                    result.chartOptions.hideXAxis = true;
                    result.chartData = this.processOneDimensionChart(labelSource, valueSource, attribute.value);
                }

            }
            if (result.chartData === null) {
                MobileUI.core.Logger.log('Chart data is incorrect');
            } else {
                items.push(result);
            }
        },
        extractValue: function (item) {
            if (Ext.isArray(item) && item.length > 0) {
                return item[0].value;
            }
        },
        addData: function (result, label) {
            var isExists = function (item) {
                return item.label === label;
            };
            for (var key in result) {
                if (result.hasOwnProperty(key)) {
                    var group = result[key];
                    var exists = group.some(isExists);
                    if (!exists) {
                        group.push({label: label, value: 0});
                    }
                }
            }

        },
        normalize: function (result) {
            //we should think about more fast algorithm
            for (var key in result) {
                if (result.hasOwnProperty(key)) {
                    var group = result[key];
                    for (var i = 0; i < group.length; i++) {
                        this.addData(result, group[i].label);
                    }
                }
            }
            return result;
        },
        processTwoDimensionChart: function (labelSource, valueSource, group, values) {
            var result = {};
            var incorrectData = false;
            values.forEach(function (item) {
                var groupName = MobileUI.core.entity.ChartUtils.extractValue(item.value[group]);
                if (groupName){
                    var label = this.extractValue(item.value[labelSource]);
                    var value = this.extractValue(item.value[valueSource]);
                    if (Ext.isNumeric(+value) && value >=0 && label){
                        if (!result[groupName]) {
                            result[groupName] = [];
                        }
                        var exits = result[groupName].some(function(item){
                                return item.value === value && item.label === label;
                            });
                        if (!exits) {
                            result[groupName].push({label: label, value: value});
                        }
                    }
                }
            }, this);
            incorrectData = this.validateData(result);
            return !incorrectData ? this.normalize(result): null;
        },

        validateData: function(result){
            return Object.keys(result).length === 0 || Object.keys(result).reduce(function(prev, item){
                    return prev + result[item].length;
                },0) === 0;
        },

        processOneDimensionChart: function (labelSource, valueSource, values) {
            var incorrectData = false;
            var result = {first: []};
            values.forEach(function (item) {
                var label = this.extractValue(item.value[labelSource]);
                var value = this.extractValue(item.value[valueSource]);
                if (Ext.isNumeric(+value) && value >=0 && label){
                    result.first.push({label: label, value: value});
                }
            }, this);
            incorrectData = this.validateData(result);
            return !incorrectData ? result: null;
        }
    }
});
