/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.entity.EntityItemUtils', {
    statics: {
        numericTypes: ['Int', 'Integer', 'Long', 'Number', 'Float', 'Double'],

        /**
         * Method returns header
         * @param label {String} header label
         * @param attrType {Object?} attrType
         * @param uri {string} nested attribute uri
         * @returns {{type: string, label: (string)}}
         */
        createHeader: function (label, attrType, uri, editable)
        {
            var result = {
                type: MobileUI.components.list.items.BaseListItem.types.header,
                label: label || '',
                disableSwipe: true,
                editor: {uri: uri}
            };
            if (attrType && editable) {
                result['required'] = MobileUI.core.CardinalityChecker.isRequired(attrType);
                var message = MobileUI.core.CardinalityChecker.getMessage(attrType);
                if (message) {
                    result['cardinality'] = message;
                }
            }
            return result;
        },

        createAddButton: function(attributes) {
            return {
                type: MobileUI.components.list.items.BaseListItem.types.buttons,
                label: 'Add',
                avatar: 'resources/images/reltio/relations/add.svg',
                info: 'text-at-left add-attribute-button',
                userData: {
                    attributes: attributes,
                    type: 'addButton'
                },
                disableSwipe: true
            };
        },

        /**
         * Method creates the attribute
         * @param property {String} property name
         * @param attribute {Object} attribute object
         * @param isSubgroup {Boolean} is in subgroup
         * @param isLastInGroup {Boolean} is last in subgroup
         * @param propertyType {String?}
         * @param editable {Boolean?}
         * @param attrType {Object?}
         * @param deleteRestricted {Boolean?}
         * @param updateRestricted {Boolean?}
         * @returns {{property: *, type: string, label: *}}
         */
        createAttribute: function (property, attribute, isSubgroup, isLastInGroup, propertyType, editable, attrType, deleteRestricted, updateRestricted)
        {
            if (!editable && !updateRestricted && attrType.type === 'Nested') {
                propertyType = MobileUI.components.list.items.BaseListItem.types.infoProperty;
            }
            var label;
            var lookups;
            var lookup;
            propertyType = propertyType || MobileUI.components.list.items.BaseListItem.types.property;
            var value = attribute.value,
                obj = {property: property, type: propertyType, label: value, secondaryLabel: attribute.secondaryLabel || ''},
                info = attribute.info;
            if (isSubgroup)
            {
                obj.subgroup = isSubgroup;
            }
            if (isLastInGroup)
            {
                obj.lastInGroup = isLastInGroup;
            }
            if (attribute.info)
            {
                obj.userData = {uri: info.infoUrl, actionLabel: info.actionUrl};
                obj.info = !!info.infoUrl;
                obj.actionLabel = !!info.actionUrl;
                obj.parentUri = attribute.info.parentUri;
                if (propertyType === MobileUI.components.list.items.BaseListItem.types.entityInfo)
                {
                    obj.tags = info.tags;
                    obj.avatar = info.avatar;
                }
                if (editable || updateRestricted) {
                    obj.editor = attrType;
                }
            }
            if (editable) {
                obj.userData.editingUri = info.uri;
            }
            if (deleteRestricted) {
                obj.disableSwipe = true;
            }

            if (attrType.type === 'Date') {
                obj.localizeLabel = 'date';
            } else if (attrType.type === 'Boolean') {
                obj.localizeLabel = 'boolean';
            }
            else if (attrType.type === 'Timestamp') {
                obj.localizeLabel = 'timestamp';
            }
            else if (attrType.dependentLookupCode) {
                label = value;
                if (attribute.displayName) {
                    label = attribute.displayName;
                    if (label !== value) {
                        label = label + ' (' + value + ')';
                    }
                }
                obj.localizeLabel = label;
            }
            else if (attrType.lookupCode) {
                label = value;
                lookups = MobileUI.core.Lookups.getLookups(attrType.lookupCode);
                if (lookups) {
                    lookups = lookups.raw;
                }
                lookups = lookups || {};
                lookup = lookups[value];
                if (lookup) {
                    label = lookup.displayName || value;
                    if (label !== value) {
                        label = label + ' (' + value + ')';
                    }
                }
                obj.localizeLabel = label;
            }
            else if (attrType.values) {
                obj.localizeLabel = 'string';
            }
            else if (MobileUI.core.entity.EntityItemUtils.numericTypes.indexOf(attrType.type) !== -1) {
                obj.localizeLabel = 'number';
            }
            else if (attrType.type === 'Dollar') {
                obj.localizeLabel = 'dollar';
            }
            obj.required = editable && MobileUI.core.CardinalityChecker.isRequired(attrType) && !attrType.hidden;
            var cardinality = MobileUI.core.CardinalityChecker.getMessage(attrType);
            if (cardinality && editable){
                obj.cardinality = cardinality;
            }
            obj.groupName = attrType.name;
            return obj;
        },
        /**
         * Method creates subgroup
         * @param propertyName {String} name
         * @param attributes {{value:String, info: Object}}
         * @param lastInGroup {Boolean} is element before header
         * @param propertyType {string}
         * @param editable {Boolean?}
         * @param attrType {Object?}
         * @param deleteRestricted {Boolean?}
         * @param updateRestricted{Boolean?}
         * @returns {Array}
         */
        createSubgroup: function (propertyName, attributes, lastInGroup, propertyType, editable, attrType, deleteRestricted, updateRestricted)
        {
            var result = [], i, property, attribute, last;
            for (i = 0; i < attributes.length; i++)
            {
                if (propertyName !== null)
                {
                    property = (i === 0) ? propertyName : '';
                }
                else
                {
                    property = propertyName;
                }
                attribute = attributes[i];
                last = lastInGroup && (i + 1 === attributes.length);
                result.push(this.createAttribute(property, attribute,
                    i + 1 !== attributes.length, last, propertyType, editable, attrType, deleteRestricted, updateRestricted));
            }
            return result;
        },

        /**
         * Method extracts value from raw value
         * @private
         * @param rawValue {String} raw value
         * @param type {String}  attribute type
         * @param editable {Boolean}
         * @returns {{}}
         */
        extractValue: function (rawValue, type, editable)
        {
            var isComplex, obj, entityType;
            isComplex = type === 'Reference' || type === 'Nested';
            obj = {
                value: (isComplex ? rawValue.label : (rawValue.lookupCode || rawValue.value)) || '',
                info: {
                    infoUrl: isComplex ? rawValue.uri : null,
                    uri: rawValue.uri,
                    parentUri: rawValue.parentUri
                }
            };
            if (type === 'Image') {
                var value = rawValue.value || {};
                var path = 'UrlPreview';
                if (value.CdnUrlPreview) {
                    path = 'CdnUrlPreview';
                }
                obj.value = value[path] && value[path][0] && value[path][0].value || '';
            }
            if (type === 'DependentLookup') {
                obj.displayName = rawValue.displayName || rawValue.value;
            }
            if (type === 'Reference')
            {
                obj.secondaryLabel = rawValue.relationshipLabel || '';
                if (rawValue.refEntity)
                {
                    obj.info.actionUrl = rawValue.refEntity.objectURI;
                    var empty = Ext.isEmpty(obj.value) || Ext.isEmpty(obj.value.trim());
                    if (empty && Ext.isEmpty(obj.editor) && !Ext.isEmpty(obj.info.actionUrl))
                    {
                        obj.value = i18n('<No label>');
                    }
                    entityType = MobileUI.core.Metadata.getEntityType(rawValue.refEntity.type);
                    obj.info.tags = [{
                        color: entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR,
                        value: entityType.label
                    }];
                    obj.info.avatar = MobileUI.core.Services.getBorderlessImageUrl(MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
                }
            }

            if (editable) {
                obj.info.value = rawValue.value;
            }
            return obj;
        },
        /**
         * Method formats dollar field
         * @param value {String|number} values
         * @param allowEmpty {Boolean?} is result could be empty
         * @returns {*}
         */
        formatDollarField: function (value, allowEmpty) {
            if (allowEmpty && Ext.isEmpty(value)) {
                value = '';
            } else {
                value = i18n(Number(value), MobileUI.core.I18n.DOLLAR_FIELD);
            }
            return value;
        },

        /**
         * Method processes attribuites with multiple values
         * @public
         * @param attribute {Object} attribute object
         * @param attrType {Object} attribute type
         * @param isLastInGroup {Boolean} is attribute last in group
         * @param editable {Boolean?}
         * @param deleteRestricted {Boolean?}
         * @param updateRestricted {Boolean?}
         * @returns {Array}
         */
        processMultiValuesAttribute: function (attribute, attrType, isLastInGroup, editable, deleteRestricted, updateRestricted)
        {
            var type,
                property = attribute.showLabel !== false ? (attrType.label || '') : null,
                values, result;

            type = attrType.dependentLookupCode ? 'DependentLookup' : attrType.type;
            values = attribute.value.map(function (item)
            {
                return this.extractValue(item, type, editable);
            }, this);
            if (MobileUI.core.entity.EntityUtils.isComplex(type))
            {
                result = this.createComplexAttribute(property, values, isLastInGroup,
                    this.getPropertyType(attribute), editable, attrType, deleteRestricted, updateRestricted);
            }
            else
            {
                result = this.createSubgroup(property, values, isLastInGroup,
                    this.getPropertyType(attribute), editable, attrType, deleteRestricted, updateRestricted);
            }


            return result;
        },
        /**
         * Method processes on attribute
         * @public
         * @param attribute {Object} attribute object
         * @param attrType {Object} is attribute complex or not
         * @param isLastInGroup {Boolean} is attribute last in group
         * @param editable {Boolean?}
         * @param deleteRestricted {Boolean?}
         * @param updateRestricted {Boolean?}
         * @returns {{property: *, type: string, label: *}}
         */
        processOneValueAttribute: function (attribute, attrType, isLastInGroup, editable, deleteRestricted, updateRestricted)
        {
            var type,
                property, value, attr;
            type = attrType.dependentLookupCode ? 'DependentLookup' : attrType.type;

            property = attribute.showLabel !== false ? (attribute.attrType.label || '') : null;
            value = this.extractValue(Ext.isEmpty(attribute.value) ? '' : attribute.value[0] || '', type, editable);
            attr = this.createAttribute(property, value, null, isLastInGroup,
                this.getPropertyType(attribute), editable, attrType, deleteRestricted, updateRestricted);

            return attr;

        },

        /**
         * Method creates multiple attributes
         * @param propertyName {String} name
         * @param attributes {{value:String, info: Object}}
         * @param lastInGroup {Boolean} is element before header
         * @param propertyType {string}
         * @param editable {Boolean?}
         * @param attrType {Object?}
         * @param deleteRestricted {Boolean?}
         * @param updateRestricted {Boolean?}
         * @returns {Array}
         */
        createComplexAttribute: function (propertyName, attributes, lastInGroup, propertyType, editable, attrType, deleteRestricted, updateRestricted)
        {
            var result = [], i, attribute, last;
            for (i = 0; i < attributes.length; i++)
            {
                attribute = attributes[i];
                last = lastInGroup && (i + 1 === attributes.length);
                result.push(this.createAttribute(propertyName, attribute,
                    false, last, propertyType, editable, attrType, deleteRestricted, updateRestricted));
            }
            return result;
        },

        /**
         * @param attribute {Object}
         * @return {string}
         */
        getPropertyType: function (attribute)
        {
            var BaseListItem = MobileUI.components.list.items.BaseListItem,
                type = attribute.itemType || BaseListItem.types.property;

            if (['Image URL', 'Image'].indexOf(attribute.attrType.type) !== -1)
            {
                type = BaseListItem.types.imageLinkProperty;
            }
            else if (['URL', 'Blog URL', 'Email'].indexOf(attribute.attrType.type) !== -1)
            {
                type = BaseListItem.types.linkProperty;
            }
            else if (['Reference'].indexOf(attribute.attrType.type) !== -1)
            {
                type = BaseListItem.types.entityInfo;
            }
            return type;
        },

        getMultiLineInfo: function(record){
            var result = {};
            if(record && record.get('error') && record.get('error').message) {
                result.error = record.get('error').message;
            }
            if (record && record.get('cardinality')){
                result.cardinality = record.get('cardinality');
            }
            return Ext.isEmpty(Object.keys(result)) ? null: result;
        }
    }
});
