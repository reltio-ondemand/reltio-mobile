/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.entity.EntityUtils', {
    requires: ['MobileUI.core.Metadata', 'MobileUI.core.entity.EntityItemUtils', 'MobileUI.core.entity.ChartUtils'],
    statics: {
        getImageAttributeValue: function (entity, imgAttributeUrl) {
            var entityType,
                i, j, attrType, uri, values, source;

            if (!Ext.isEmpty(entity)) {
                entityType = MobileUI.core.Metadata.getEntityType(entity.type);
                for (i = 0; i < entityType.attributes.length; i++) {
                    attrType = entityType.attributes[i];
                    if (attrType.type === 'Image URL') {
                        uri = entityType.attributes[i].uri;
                        uri = uri.substring(uri.lastIndexOf('/') + 1);
                        if (entity.attributes[uri] && entity.attributes[uri].length > 0) {
                            values = entity.attributes[uri];
                            for (j = 0; j < values.length; j++) {
                                source = values[j].uri;
                                if (source === imgAttributeUrl) {
                                    return values[j].value;
                                }
                            }
                            return values[0].value;
                        }
                    }
                }
            }
            return null;
        },
        isImageAttribute: function (entityType, attrType) {
            return (entityType.imageAttributeURIs && entityType.imageAttributeURIs.length > 0 && entityType.imageAttributeURIs.some(function (type) {
                    return this.uri === type;
                }, attrType)) || attrType.type === 'Image URL';
        },

        getImageSources: function (entity) {
            var entityType = MobileUI.core.Metadata.getEntityType(entity.type),
                images = [],
                i, attrType, attributes, uri;

            var processAttributes = function (entity, uri) {
                return entity.attributes
                    .filter(function (attribute) {
                        return attribute.type === this.uri;
                    }, {uri: uri})
                    .map(function (attribute) {
                        return {
                            value: attribute.value,
                            uri: attribute.uri
                        };
                    })
                    .filter(function (item) {
                        return !Ext.isEmpty(item.value);
                    });
            };
            for (i = 0; i < entityType.attributes.length; i++) {
                attrType = entityType.attributes[i];
                if (this.isImageAttribute(entityType, attrType)) {
                    uri = entityType.attributes[i].uri;
                    attributes = processAttributes(entity, uri);
                    images.push.apply(images, attributes);
                }
            }
            return images;
        },

        getImageSourcesNew: function (entity) {
            var entityType = MobileUI.core.Metadata.getEntityType(entity.type),
                images = [];

            var processAttributes = function (entity, uri) {
                return Object.keys(entity.attributes)
                    .map(function (key) {
                        return entity.attributes[key];
                    })
                    .reduce(function(acc, item){
                        return acc.concat(item);
                    }, [])
                    .filter(function (attribute) {
                        return attribute.type === uri;
                    })
                    .map(function (attribute) {
                        return {
                            value: attribute.value,
                            uri: attribute.uri
                        };
                    })
                    .filter(function (item) {
                        return !Ext.isEmpty(item.value);
                    });
            };

            entityType.attributes
                .filter(function (attrType) {
                    return this.isImageAttribute(entityType, attrType);
                }, this)
                .forEach(function (attrType) {
                    images.push.apply(images, processAttributes(entity, attrType.uri));
                });

            return images;
        },

        getUrlThumbnail: function (imageAttrValue) {
            var val = (imageAttrValue.value.CdnUrlThumbnail || []).filter(function (val) {
                return val.ov !== false;
            })[0];

            return val && val.value;
        },

        makeReferenceAttribute: function (attrType, entityUri, refEntityUri) {
            var md = MobileUI.core.Metadata;
            var relation = md.getRelationType(attrType.relationshipTypeURI);
            var startEntityUri, endEntityUri;
            if (relation.direction === 'directed') {
                // bad case, because we cannot determine start/end object
                if ((relation.endObject.objectTypeURI === attrType.referencedEntityTypeURI) &&
                    (relation.startObject.objectTypeURI === attrType.referencedEntityTypeURI)) {
                    MobileUI.core.Logger.log('Cannot determine start/end object for relation ' + relation.uri);
                    startEntityUri = entityUri;
                    endEntityUri = refEntityUri;
                }
                else if (relation.startObject.objectTypeURI === attrType.referencedEntityTypeURI) {
                    startEntityUri = refEntityUri;
                    endEntityUri = entityUri;
                }
                else {
                    startEntityUri = entityUri;
                    endEntityUri = refEntityUri;
                }
            }
            else if (relation.direction === 'bidirectional') {
                // does not matter which one is start/end object
                startEntityUri = entityUri;
                endEntityUri = refEntityUri;
            }
            else {
                // cannot understand which object should be start object
                MobileUI.core.Logger.log('Unsupported relation direction ' + relation.direction);
                startEntityUri = entityUri;
                endEntityUri = refEntityUri;
            }
            var data = {
                type: relation.uri,
                startObject: {objectURI: startEntityUri},
                endObject: {objectURI: endEntityUri}
            };
            return data;
        },
        convertAttributes: function (attributesArray, uri, includeRef) {
            var result = {};
            attributesArray.filter(function (item) {
                return item.parentUri === uri;
            }).forEach(function (item) {
                if (!result[item.name]) {
                    result[item.name] = [];
                }
                if (Ext.isObject(item.value) && !item.refEntity) {
                    result[item.name].push({
                        value: this.convertAttributes(attributesArray, item.uri, includeRef),
                        ov: true,
                        uri: item.uri
                    });
                } else if (includeRef && item.refEntity) {
                    var refAttr = MobileUI.core.entity.EntityUtils.createEntityFromTemporary(MobileUI.core.entity.TemporaryEntity.getEntity(item.refRelation.objectURI));
                    var entity = MobileUI.core.entity.TemporaryEntity.getEntity(item.refEntity.objectURI);
                    var entityAttrs;
                    if (entity.original) {
                        entityAttrs = MobileUI.core.entity.EntityUtils.createEntityFromTemporary(entity).attributes;
                    } else {
                        entityAttrs = this.convertAttributes(MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced(item, entity), item.uri);
                    }
                    var structure = {};
                    var crosswalkValue = (item.refEntity.objectURI || '').split('/');
                    crosswalkValue = crosswalkValue.length > 0 ? crosswalkValue[crosswalkValue.length - 1] : '';
                    structure.refEntity = {
                        crosswalks: [{
                            type: 'configuration/sources/Reltio',
                            value: crosswalkValue
                        }]
                    };
                    MobileUI.core.util.Util.merge(refAttr.attributes, entityAttrs, null, true);
                    structure.value = entityAttrs;
                    result[item.name].push(structure);
                } else {
                    if (!Ext.isObject(item.value) && (!Ext.isArray(item.value) || item.value.length > 0)) {
                        result[item.name].push({
                            value: item.value,
                            type: item.type,
                            ov: true,
                            uri: item.uri
                        });
                    }
                }
            }, this);
            return result;
        },

        createEntityFromTemporary: function (entity, includeRef) {
            var attributes = entity.addedAttributes;
            var uris = attributes.map(function (item) {
                return item.uri;
            });
            entity.editedAttributes.forEach(function (item) {
                if (uris.indexOf(item.uri) === -1) {
                    attributes.push(item);
                }
            });
            return {
                attributes: this.convertAttributes(attributes, entity.uri, includeRef),
                type: entity.type
            };
        },
        createEditableEntityStructure: function (entityType, groupsConfig, policy, ignoreComplex) {
            groupsConfig = groupsConfig || [];
            var groupAttrTypes = [],
                entityTypeObject = Ext.isObject(entityType) ? entityType : MobileUI.core.Metadata.getEntityType(entityType),
                isGroupExists = function (item) {
                    return this.label === item.label;
                },
                configuration = MobileUI.core.Services.getConfigurationManager(),
                perspective = configuration.findPerspective(entityTypeObject.uri),
                extension = configuration.getExtensionById(perspective),
                hiddenAttributes = extension && extension.hiddenAttributes || [];

            groupsConfig.forEach(function (item) {
                groupAttrTypes.push({label: item.label, values: []});
            });
            entityTypeObject.attributes.forEach(function (attrType) {
                if (policy) {
                    if (!policy.call(this, attrType)) {
                        return;
                    }
                }
                if (hiddenAttributes.indexOf(attrType.uri) !== -1) {
                    return;
                }
                for (var i = 0; i < groupsConfig.length; i++) {
                    var content = groupsConfig[i].content;
                    for (var j = 0; j < content.length; j++) {
                        if ((content[j] === attrType.uri) || (content[j] === attrType.type)) {
                            var label = groupsConfig[i].label;
                            var group = groupAttrTypes.filter(isGroupExists, {label: label});
                            group[0].values.push(attrType);
                            return;
                        }
                    }
                }
                if (MobileUI.core.entity.EntityUtils.isComplex(attrType) && !ignoreComplex) {
                    groupAttrTypes.push({label: attrType.label, values: [attrType]});
                    return;
                }
                var other = groupAttrTypes.filter(isGroupExists, {label: 'Other'});
                if (other.length === 0) {
                    groupAttrTypes.push({label: 'Other', values: [attrType]});
                } else {
                    other[0].values.push(attrType);
                }
            });
            groupAttrTypes = groupAttrTypes.filter(function (item) {
                return item.values.length > 0;
            });
            return groupAttrTypes;
        },
        makeEditableEntity: function (entity) {
            entity = Ext.clone(entity);
            return {
                original: entity,
                uri: entity.uri,
                type: entity.type,
                attributes: this.expandEntityAttributes(entity.attributes, entity.uri),
                addedAttributes: [],
                deletedAttributes: [],
                editedAttributes: []
            };
        },
        expandEntityAttributes: function (attributes, parentUri) {
            var result = [];
            if (Ext.isObject(attributes)) {
                /*jshint loopfunc: true */
                for (var key in attributes) {
                    if (attributes.hasOwnProperty(key)) {
                        attributes[key].forEach(function (item) {
                            item.name = key;
                            item.parentUri = parentUri;
                            result.push(item);
                            if (Ext.isObject(item.value)) {
                                result = result.concat(this.expandEntityAttributes(item.value, item.uri));
                            }
                        }, this);
                    }
                }
                /*jshint loopfunc: true */

            } else {
                result = attributes;
            }
            return result;
        },
        /**
         * Method removes the attribute with uri from entity.
         * @param entity {Object} entity object
         * @param uri {String} attribute uri
         */
        removeAttribute: function (entity, uri) {
            if (entity && Ext.isArray(entity.attributes)) {

                entity.attributes = entity.attributes.map(function (item) {
                    if (item.uri.indexOf(uri) === 0) {
                        if (item.uri === uri) {
                            return {
                                attrType: item.attrType,
                                parentUri: item.parentUri,
                                uri: item.uri,
                                value: []
                            };
                        }
                        else {
                            item.uri = '$deleted$';
                            return item;
                        }
                    }
                    else {
                        return item;
                    }
                }).filter(function (item) {
                    return item.uri !== '$deleted$';
                });
            }
        },
        /**
         * Method processes attributes array and return values object
         * @param attributes {Array} attributes array
         * @param parentUri
         * @returns {{}}
         */
        processEntitiesAttributes: function (attributes, parentUri) {
            return attributes.filter(function (item) {
                return item.parentUri === parentUri;
            }).reduce(function (result, attribute) {
                var EntityUtils = MobileUI.core.entity.EntityUtils;
                var tmpResult;
                if (EntityUtils.isReference(attribute.attrType)) {
                    tmpResult = EntityUtils.processEntitiesAttributes(attributes, attribute.uri);
                    if (Object.keys(tmpResult).length > 0) {
                        result[attribute.attrType.name] = [{
                            value: {},
                            refEntity: attribute.refEntity,
                            refRelation: attribute.refRelation
                        }];
                        result[attribute.attrType.name][0].value = EntityUtils.processEntitiesAttributes(attributes, attribute.uri);
                    }
                }
                else if (EntityUtils.isComplex(attribute.attrType)) {
                    tmpResult = EntityUtils.processEntitiesAttributes(attributes, attribute.uri);
                    if (Object.keys(tmpResult).length > 0) {
                        result[attribute.attrType.name] = [{value: tmpResult}];
                    }
                }
                else {
                    if (!Ext.isEmpty(attribute.value)) {
                        var v = attribute.value;
                        if (Ext.isArray(v)) {
                            v = v[0];
                        }
                        result[attribute.attrType.name] = [{value: v}];
                    }
                }
                return result;
            }, {});
        },
        /**
         * Method processes entity and return entity in "savable" format
         * @param entity {Object} entity to save
         * @returns {{}}
         */
        prepareEntityBeforeSave: function (entity) {
            var result = {};
            result.attributes = {};
            if (entity.type) {
                result.type = entity.type;
            }
            if (entity.roles) {
                result.roles = entity.roles;
            }
            result.attributes = this.processEntitiesAttributes(entity.attributes, entity.uri);
            return result;
        },

        /**
         * Method collects the attributes array which should be presented in label
         * @param entityType {Object|String} entity type
         * @returns {*}
         */
        collectLabelAttributes: function (entityType) {
            entityType = Ext.isObject(entityType) ? entityType : MobileUI.core.Metadata.getEntityType(entityType);
            return entityType.attributes.filter(function (attrType) {
                return this.dataLabelPattern.indexOf('{' + attrType.name + '}') !== -1 ||
                    this.dataLabelPattern.indexOf('{' + attrType.name + '.') !== -1 ||
                    (this.secondaryLabelPattern || '' ).indexOf('{' + attrType.name + '}') !== -1 ||
                    (this.secondaryLabelPattern || '' ).indexOf('{' + attrType.name + '.') !== -1 ||
                    (this.businessCardAttributeURIs || []).indexOf(attrType.uri) !== -1;
            }, entityType);
        },

        /**
         * Method find label pattern in entity type
         * @param entityType {String} entity type uri
         * @returns {*}
         */
        findLabelPattern: function (entityType) {
            var entityTypes = MobileUI.core.Metadata.getEntityTypes(),
                key,
                labelPattern = null;

            for (key in entityTypes) {
                if (entityTypes.hasOwnProperty(key)) {
                    if (MobileUI.core.util.Util.endsWith(key, entityType)) {
                        labelPattern = entityTypes[key].dataLabelPattern || null;
                        break;
                    }
                }
            }
            return labelPattern;
        },
        /**
         * Method find secondary label pattern in entity type
         * @param entityType {String} entity type uri
         * @returns {*}
         */
        findSecondaryLabelPattern: function (entityType) {
            var entityTypes = MobileUI.core.Metadata.getEntityTypes(),
                key,
                labelPattern = null;

            for (key in entityTypes) {
                if (entityTypes.hasOwnProperty(key)) {
                    if (key.lastIndexOf(entityType) === (key.length - entityType.length)) {
                        labelPattern = entityTypes[key].secondaryLabelPattern || null;
                        break;
                    }
                }
            }
            return labelPattern;
        },

        /**
         * Method processes attributes
         * @param entity {Object} entity object
         * @param groupsConfig {Array?} groups config
         * @param hiddenAttributes {Array?} hiddenAttributes
         * @returns {Object}
         */
        processAttributes: function (entity, groupsConfig, hiddenAttributes) {
            var groups = {},
                entityType, i, j, groupItem, chartsAttribute, attrType, attr, key, group, chart = null, groupByType, groupByUri, other,
                filterOv = function (item) {
                    return item.ov !== false;
                };
            if (!entity) {
                return groups;
            }
            groupsConfig = Ext.isArray(groupsConfig) ? groupsConfig : [];
            hiddenAttributes = Ext.isArray(hiddenAttributes) ? hiddenAttributes : [];
            (groupsConfig).forEach(function (item) {
                this[item.label] = [];
            }, groups);
            chartsAttribute = groupsConfig.filter(function (item) {
                return Ext.isObject(item.chartContent);
            }).map(function (item) {
                return item.chartContent.attributeUri;
            });
            other = [];
            entityType = MobileUI.core.Metadata.getEntityType(entity.type);
            for (i = 0; i < entityType.attributes.length; i++) {
                attrType = entityType.attributes[i];
                if (hiddenAttributes.indexOf(attrType.uri) !== -1 && chartsAttribute.indexOf(attrType.uri) === -1) {
                    continue;
                }
                attr = entity.attributes[attrType.name];
                if (!attr) {
                    continue;
                }
                if (Ext.isArray(attr)) {
                    attr = attr.filter(filterOv);
                }
                attr = {value: attr, attrType: attrType, showLabel: true};
                for (j = 0; j < groupsConfig.length; j++) {
                    groupItem = groupsConfig[j];
                    if (Ext.isObject(groupItem.chartContent)) {
                        if (groupItem.chartContent.attributeUri === attrType.uri && attrType.type === 'Nested') {
                            if (chart === null) {
                                chart = [groupItem];
                            }
                            else {
                                chart.push(groupItem);
                            }
                        }
                    }
                    group = groupItem.content;
                    if (group && group.indexOf(attrType.uri) !== -1) {
                        groupByUri = groupItem.label;
                        break;
                    }
                    if (group && group.indexOf(attrType.type) !== -1) {
                        groupByType = groupItem.label;
                    }
                }
                if (chart) {
                    for (var k = 0; k < chart.length; k++) {
                        var item = chart[k];
                        var attribute = Ext.clone(attr);
                        attribute.chartContent = item.chartContent;
                        groups[item.label].push(attribute);
                    }
                    chart = null;
                }
                if (hiddenAttributes.indexOf(attrType.uri) !== -1) {
                    continue;
                }
                if (groupByUri) {
                    groups[groupByUri].push(attr);
                    groupByUri = null;
                }
                else if (groupByType) {
                    groups[groupByType].push(attr);
                    groupByType = null;
                }
                else if (this.isComplex(attrType)) {
                    key = attrType.label;
                    if (!groups[key]) {
                        groups[key] = [];
                    }
                    attr.showLabel = false;
                    groups[key].push(attr);
                }
                else {
                    other.push(attr);
                }
            }
            groups.Other = other;
            return groups;
        },

        /**
         * Method returns is attribute complec or not
         * @param type {{type:String}|String}
         */
        isComplex: function (type) {
            var checkString = Ext.isObject(type) ? type.type : type;
            return checkString === 'Reference' || checkString === 'Nested' || (Ext.isObject(type) && !!type.analyticsAttributes);
        },

        /**
         * Method returns is attribute reference or not
         * @param type {{type:String}|String}
         */
        isReference: function (type) {
            var checkString = Ext.isObject(type) ? type.type : type;
            return checkString === 'Reference';
        },

        /**
         * Method find attribute and collect it's name
         * @param attributesObject {Object} object which contains the attributes
         * @param attrUri {String} attribute uri to find
         * @param name {String?} attribute name
         * @param notRelation {Boolean?}
         * @returns {*}
         */
        findAttribute: function (attributesObject, attrUri, name, notRelation) {
            if (!notRelation && attrUri && attrUri.indexOf('relations/') === 0) {
                return this.findRefAttribute(attributesObject, attrUri);
            }

            var key, attributes, i, attribute;

            name = name || 'attributes';

            if (!Ext.isObject(attributesObject)) {
                return null;
            }
            for (key in attributesObject) {
                if (attributesObject.hasOwnProperty(key)) {
                    attributes = attributesObject[key];
                    for (i = 0; i < attributes.length; i++) {
                        attribute = attributes[i];
                        if (attribute.uri === attrUri) {
                            name = name + '.' + key;
                            return {name: name, attribute: attribute};
                        }
                        else if (attrUri.indexOf(attribute.uri) === 0) {
                            name = name + '.' + key;

                            return this.findAttribute(attribute.value, attrUri, name, notRelation);
                        }
                    }
                }
            }
            return null;
        },

        findRefAttribute: function (attributesObject, attrUri) {
            var refRelationUri = attrUri.split('/').slice(0, 2).join('/');

            for (var name in attributesObject) {
                if (attributesObject.hasOwnProperty(name)) {
                    var attributes = attributesObject[name];
                    for (var i = 0; i < attributes.length; i++) {
                        var attr = attributes[i];
                        if (attr.refRelation && attr.refRelation.objectURI === refRelationUri) {
                            if (attrUri === refRelationUri) {
                                return {attribute: attr};
                            }
                            return this.findAttribute(attr.refRelation.attributes, attrUri, null, true);
                        }
                        if (Ext.isObject(attr.value)) {
                            var childAttr = this.findRefAttribute(attr.value, attrUri);
                            if (childAttr) {
                                return childAttr;
                            }
                        }
                        if (attr.uri === attrUri) {
                            return attr;
                        }
                    }
                }
            }

            return null;
        },

        findAttributeByTypeUri: function (attributesObject, typeUri) {
            var key, attributes, i, attribute;
            var result = [];
            if (!Ext.isObject(attributesObject)) {
                return result;
            }

            for (key in attributesObject) {
                if (attributesObject.hasOwnProperty(key)) {
                    attributes = attributesObject[key];
                    for (i = 0; i < attributes.length; i++) {
                        attribute = attributes[i];
                        if (attribute.type === typeUri) {
                            result.push(attribute);
                        } else if (attribute.value) {
                            result = result.concat(this.findAttributeByTypeUri(attribute.value, typeUri));
                        }
                    }
                }
            }
            return result;
        },


        /**
         * Method collects path to uri
         * @param attributesObject {Object} attribtues map key->value
         * @param entityType {Object?} entity type
         * @param attrUri {String} attribute uri
         * @param name {String?} attribute name
         * @returns {*}
         */
        collectPath: function (attributesObject, entityType, attrUri, name) {
            var key, attributes, i, attribute, path, attributeDefinition, result = [];

            name = name || 'attributes';
            if (!Ext.isObject(attributesObject)) {
                return null;
            }
            for (key in attributesObject) {
                if (attributesObject.hasOwnProperty(key)) {
                    attributes = attributesObject[key];
                    for (i = 0; i < attributes.length; i++) {
                        attribute = attributes[i];
                        if (attribute.uri === attrUri) {
                            attributeDefinition = MobileUI.core.Metadata.findEntityAttributeByName(entityType, key) || MobileUI.core.Metadata.findAttributeByName(key);
                            path = (Ext.isObject(attributeDefinition) && attributeDefinition.label) ? attributeDefinition.label : key;
                            result.push(path);
                            return result;
                        }
                        else if (attrUri.indexOf(attribute.uri) === 0) {
                            name = name + '.' + key;
                            attributeDefinition = MobileUI.core.Metadata.findEntityAttributeByName(entityType, key) || MobileUI.core.Metadata.findAttributeByName(key);
                            path = (Ext.isObject(attributeDefinition) && attributeDefinition.label) ? attributeDefinition.label : key;
                            result.push(path);
                            entityType = (attribute.refEntity && attribute.refEntity.type) || entityType;
                            result = result.concat(this.collectPath(attribute.value, entityType, attrUri, name));
                            return result;
                        }
                    }
                }
            }
            return null;
        },
        /**
         * Method creates attribute model
         * @param attributes {Object} grouped attributes
         * @param editable {Boolean?}
         * @param groupAttrTypes {Object?}
         * @returns {Array}
         */
        createAttributesModel: function (attributes, editable, groupAttrTypes) {
            var result = [], key, attributesInCategory,
                ItemUtils = MobileUI.core.entity.EntityItemUtils,
                categoriesCount = Object.keys(attributes).length,
                showCategory;

            function visible(attrType) {
                return attrType.attrType ? !attrType.attrType.hidden : !attrType.hidden;
            }

            function canAdd(attrType) {
                return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
            }

            function containsChart(item) {
                return item.chartContent;
            }

            for (key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attributesInCategory = attributes[key];
                    showCategory = (categoriesCount > 1 || categoriesCount === 1 && key !== 'Other');

                    var items = [];
                    var isChartRecord = attributesInCategory.some(containsChart);
                    if (isChartRecord) {
                        MobileUI.core.entity.ChartUtils.processChart(attributesInCategory, items);
                        if (items.length === 0) {
                            attributesInCategory = [];
                        }
                    } else {
                        attributesInCategory = attributesInCategory.filter(visible);
                        attributesInCategory.reduce(this.createAttributeModel(editable), items);
                    }
                    if (editable) {
                        if (groupAttrTypes[key]) {
                            var attributesToAdd = groupAttrTypes[key].filter(visible).filter(canAdd);

                            if ((items.length > 0) || (attributesToAdd.length > 0)) {
                                var attr = (groupAttrTypes[key].length === 1) && groupAttrTypes[key][0];
                                result.push(ItemUtils.createHeader(key, attr, attr && attr.uri, editable));
                            }

                            if (attributesToAdd.length > 0) {
                                result.push(ItemUtils.createAddButton(attributesToAdd));
                            }
                        }
                    }
                    else {
                        if (showCategory && attributesInCategory.length > 0) {
                            result.push(ItemUtils.createHeader(key));
                        }
                    }
                    result.push.apply(result, items);
                }
            }
            return result;
        },

        /**
         * @param editable {Boolean}
         * @returns {Function}
         */
        createAttributeModel: function (editable) {
            /**
             * @param createdModels {Object[]}
             * @param attr {Object}
             * @param index {Number?} attributeIndex
             * @param attributes {Object[]?} attributes
             */
            return function (createdModels, attr, index, attributes) {
                var ItemUtils = MobileUI.core.entity.EntityItemUtils,
                    last = attributes && (index + 1 === attributes.length);

                if (MobileUI.core.PermissionManager.securityService().metadata.canRead(attr.attrType)) {
                    var updateRestricted = editable && !MobileUI.core.PermissionManager.securityService().metadata.canUpdateOrInitiate(attr.attrType);
                    var editableAttribute = editable && !updateRestricted;
                    var deleteRestricted = !MobileUI.core.PermissionManager.securityService().metadata.canDeleteOrInitiate(attr.attrType);
                    if (attr.value && attr.value.length > 1) {
                        var models = ItemUtils.processMultiValuesAttribute(attr, attr.attrType, last, editableAttribute, deleteRestricted, updateRestricted);
                        createdModels.push.apply(createdModels, models);
                    }
                    else {
                        var model = ItemUtils.processOneValueAttribute(attr, attr.attrType, last, editableAttribute, deleteRestricted, updateRestricted);
                        createdModels.push(model);
                    }
                }
                return createdModels;
            };
        },


        /**
         * Method gets label based on entity and label pattern
         * @param entity {Object} entity object
         * @param labelPattern {String} label pattern
         * @returns {string}
         */
        evaluateEntityLabel: function (entity, labelPattern) {
            var re = new RegExp('{[^}]*}', 'g'),
                arr = [], i, uri, attr,
                temp, label = '', end;
            while ((temp = re.exec(labelPattern)) !== null) {
                arr.push(temp[0]);
            }
            for (i = 0; i < arr.length; i++) {
                end = arr[i].indexOf('.') !== -1 ? arr[i].indexOf('.') : arr[i].length - 1;
                uri = arr[i].substring(1, end);
                attr = entity.attributes[uri];
                attr = (attr && attr[0]) ? attr[0] : null;
                if (attr) {
                    var val = (Ext.isString(attr.value) || Ext.isNumber(attr.value)) ? attr.value + '' : null;
                    label += val || attr.label || '';
                }
                if (label.length > 0 && label.charAt(label.length - 1) !== ' ') {
                    label += ' ';
                }
            }
            return label;
        },
        /**
         * Method gets label based on entity and label pattern
         * @param entity {Object} entity object
         * @param attrValue {Object} vaue
         * @param attrType {Object} type
         * @returns {string}
         */
        evaluateLabel: function (entity, attrValue, attrType) {
            if (attrType.type === 'Reference') {
                var labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern(attrType.referencedEntityTypeURI);
                if (labelPattern) {
                    return MobileUI.core.entity.EntityUtils.evaluateEntityLabel(
                        attrValue ? {attributes: attrValue} : entity, labelPattern);
                }
            }
            else if (attrType.type === 'Nested') {
                if (attrType.dataLabelPattern) {
                    return MobileUI.core.entity.EntityUtils.evaluateEntityLabel({attributes: attrValue || {}}, attrType.dataLabelPattern);
                }
            }
            return '';
        },

        /**
         * Method gets label based on attributes list and label pattern
         * @param attributes {Array} attributes list
         * @param labelPattern {String} label pattern
         * @returns {string}
         * @param customAttributesProcessing{Function?}
         */
        evaluateLabelFromAttributes: function (attributes, labelPattern, customAttributesProcessing) {
            var re = new RegExp('{[^}]*}', 'g'),
                attributeNames = [], temp;

            customAttributesProcessing = customAttributesProcessing || function (attribute) {
                    return (typeof attribute.value !== 'undefined' && !Ext.isObject(attribute.value[0])) ? attribute.value[0] : null;
                };

            while ((temp = re.exec(labelPattern)) !== null) {
                attributeNames.push(temp[0].substring(1, temp[0].length - 1));
            }

            var processLabel = function (attributeValues, attributeNames) {
                return attributeNames.map(function (attributeName) {
                    var label;
                    var names = attributeName.split('.');
                    var name = names.shift();

                    var attribute = null;
                    for (var i = 0; i < attributeValues.length; ++i) {
                        if (attributeValues[i].attrType && attributeValues[i].attrType.name === name) {
                            attribute = attributeValues[i];
                            break;
                        }
                    }

                    if (attribute) {

                        var value = !names.length ?
                            customAttributesProcessing.call(this, attribute) :
                            (attribute && attribute.value && attribute.value[0]);

                        if (Ext.isNumber(value)) {
                            value = String(value);
                        }

                        if (typeof value !== 'undefined' && attribute.attrType && attribute.attrType.lookupCode) {
                            if (!attribute.attrType.dependentLookupCode) {
                                var lookupLabel = value;
                                var lookups = MobileUI.core.Lookups.getLookups(attribute.attrType.lookupCode);
                                if (lookups) {
                                    lookups = lookups.raw;
                                }

                                lookups = lookups || {};
                                var lookup = lookups[value];
                                if (lookup) {
                                    lookupLabel = lookup.displayName || value;
                                }

                                value = lookupLabel;
                            } else {
                                value = attribute.displayName || value;
                            }
                        }

                        if (names.length > 0 && value) {
                            label = processLabel(value, [names.join('.')]).value;
                        }
                        else {
                            label = (value || attribute.label || '');
                        }
                    }
                    return {key: attributeName, value: label};
                }, this);
            };
            var values = processLabel(attributes, attributeNames);
            var empty = values.every(function (value) {
                return typeof value.value === 'undefined';
            });

            var label;
            if (empty) {
                label = '';
            } else {
                label = labelPattern;
                values.forEach(function (obj) {
                    label = label.replace('{' + obj.key + '}', obj.value || '');
                });
            }

            return MobileUI.core.entity.EntityUtils.processLabel(label);
        },

        /**
         * Method processes label and if it is empty return '<No label>'
         * @param label {String?} label
         * @returns {string}
         */
        processLabel: function (label) {
            return (Ext.isEmpty(label) || Ext.isEmpty(label.trim())) ? i18n('<No label>') : label;
        },

        flattenAttributes: function (attributes) {
            return Array.prototype.concat.apply([],
                Object.keys(attributes).map(function (group) {
                    return attributes[group];
                }));
        },

        cleanupNestedUris: function (attribute) {
            if (attribute.attrType && attribute.attrType.type === 'Nested') {
                var attrs = MobileUI.core.entity.EntityUtils.flattenAttributes(attribute.value);
                attrs.forEach(function (attr) {
                    delete attr.uri;
                    delete attr.parentUri;
                    MobileUI.core.entity.EntityUtils.cleanupNestedUris(attr);
                });
            }
        },

        LABEL: 'dataLabelPattern',
        SECONDARY_LABEL: 'secondaryLabelPattern',
        DEFAULT_PROPERTIES: {dataLabelPattern: 'label', secondaryLabelPattern: 'secondaryLabel'},

        buildPatternProperty: function (entity, propertyName) {
            //TODO: Pattern i18n
            /*var type = MobileUI.core.Metadata.getEntityType(entity.type);

             if (type[propertyName]) {
             return MobileUI.core.entity.EntityUtils.evaluateEntityLabel(entity, type[propertyName]);
             }*/

            return entity[MobileUI.core.entity.EntityUtils.DEFAULT_PROPERTIES[propertyName]];
        },

        validationPassed: function (result) {
            result = result || {};
            return Object.keys(result).reduce(function (prev, key) {
                    var counter = Ext.isArray(result[key]) ? result[key].length : 0;
                    return prev + counter;
                }, 0) === 0;
        },

        validationMessage: function (result) {
            result = result || {};
            var incorrectAttributes = {};
            var validationFailed = result.validationFailed;
            var ValidationCodes = MobileUI.core.session.MValidation.VALIDATION_CODES;
            (result.invalid || []).forEach(function (attrName) {
                incorrectAttributes[attrName] = [i18n('%1 has not valid value', attrName)];
            });
            var missed = (result.required || []);
            var custom = [];
            var missedExternal = [];
            if (validationFailed) {
                missedExternal = validationFailed.filter(function (item) {
                    return item.invalidType === ValidationCodes.MISSED;
                }).map(function (item) {
                    return item.attrType ? item.attrType.label : '';
                }).filter(function (item) {
                    return !Ext.isEmpty(item);
                });

                var others = validationFailed.filter(function (item) {
                    return item.invalidType !== ValidationCodes.MISSED;
                });
                var hasPath = function (item) {
                    return Ext.isArray(item.path) && !Ext.isEmpty(item.path);
                };
                var hasAttrType = function (item) {
                    return item.attrType;
                };
                var not = function (func) {
                    return function () {
                        return !func.apply(null, arguments);
                    };
                };
                var withPaths = others.filter(hasPath);
                custom = others.filter(not(hasPath));

                withPaths.forEach(function (item) {
                    var attrName = item.path[0].attrName;
                    if (!incorrectAttributes[attrName]) {
                        incorrectAttributes[attrName] = [];
                    }
                    var path = item.path.reduce(function (prev, item, index, array) {
                        prev += item.label + (index < array.length - 1 ? '-':'');
                        return prev;
                    }, '');

                    incorrectAttributes[attrName].push(path + ': ' + item.value);
                });
                custom.filter(hasAttrType).forEach(function (item) {
                    if (!incorrectAttributes[item.attrType.label]) {
                        incorrectAttributes[item.attrType.label] = [];
                    }
                    incorrectAttributes[item.attrType.label].push(item.value);
                });
                custom = custom.filter(not(hasAttrType));
            }
            Ext.Array.unique(missedExternal.concat(missed)).forEach(function (attrName) {
                if (!incorrectAttributes[attrName]) {
                    incorrectAttributes[attrName] = [i18n('%1 is required', attrName)];
                }
            });
            var customMessage = custom
                .map(function (item) {
                    return item.value;
                })
                .filter(function (item) {
                    return item;
                })
                .map(function(item){
                    return '<div class="validation-header">' + item + '</div>';
                })
                .join('');

            return Object.keys(incorrectAttributes)
                    .map(function (key) {
                        return {key: key, value: incorrectAttributes[key]};
                    })
                    .filter(function (item) {
                        return !Ext.isEmpty(item.value);
                    })
                    .reduce(function (prev, item) {
                        if (item.value.length > 1) {
                            prev = prev + '<div class="validation-header">' + item.key + ':</div>';
                            prev = prev + item.value.reduce(function (msg, value) {
                                    return msg + '<div class="validation-record-multiline">' + value + '</div>';
                                }, '');
                        } else {
                            prev = prev + '<div class="validation-header">' + item.key + ':</div>' +
                                '<div class="validation-record">' + item.value[0] + '</div>';
                        }
                        return prev;
                    }, '') + customMessage;
        },

        validationMessageNew: function (errors) {
            var incorrectAttributes = {},
                customMessage = '';

            function pushError(key, message) {
                key = Ext.util.Format.htmlEncode(key);
                if (!incorrectAttributes[key]) {
                    incorrectAttributes[key] = [];
                }

                if (incorrectAttributes[key].indexOf(message) === -1) {
                    incorrectAttributes[key].push(message);
                }
            }

            function getLabel(label) {
                return label === Ext.util.Format.htmlEncode(i18n('<No label>')) ? label : Ext.util.Format.htmlEncode(label);
            }
            errors.forEach(function (err) {
                if (Ext.isArray(err.path) && !Ext.isEmpty(err.path)) {
                    var path = err.path.reduce(function (prev, item, index, array) {
                        prev += getLabel(item.label) + (index < array.length - 1 ? '-' : '');
                        return prev;
                    }, '');

                    pushError(err.path[0].attrName, path + ': ' + Ext.util.Format.htmlEncode(err.value));
                } else if (err.attrType) {
                    pushError(err.attrType.label, Ext.util.Format.htmlEncode(err.value));
                } else {
                    customMessage += '<div class="validation-header">' + Ext.util.Format.htmlEncode(err.value) + '</div>';
                }
            });

            return Object.keys(incorrectAttributes)
                .map(function (key) {
                    return {key: key, value: incorrectAttributes[key]};
                })
                .filter(function (item) {
                    return !Ext.isEmpty(item.value);
                })
                .reduce(function (prev, item) {
                    if (item.value.length > 1) {
                        prev = prev + '<div class="validation-header">' + item.key + ':</div>';
                        prev = prev + item.value.reduce(function (msg, value) {
                                return msg + '<div class="validation-record-multiline">' + value + '</div>';
                            }, '');
                    } else {
                        prev = prev + '<div class="validation-header">' + item.key + ':</div>' +
                            '<div class="validation-record">' + item.value[0] + '</div>';
                    }
                    return prev;
                }, '') + customMessage;
        },

        getAttributeSource: function(attrType)
        {
            var isAnalytic = MobileUI.core.Metadata.isAnalyticAttribute(attrType);
            var attrSource = isAnalytic ? 'analyticsAttributes' : 'attributes';
            return attrSource;
        },

        TEMP_URI: 'uri$$',
        TEMP_URI_FOR_API: '1Tempo4rary',

        generateUri: function (baseUri, attrTypeName) {
            if (baseUri) {
                baseUri = baseUri.lastIndexOf('/') === baseUri.length - 1 ? baseUri : baseUri + '/';
            } else {
                baseUri = '';
            }

            var random = Math.floor((Math.random() * 1000) + 1);

            return baseUri + attrTypeName + '/' + MobileUI.core.entity.EntityUtils.TEMP_URI + new Date().getTime() + random;
        },

        isTemporaryUri: function (uri) {
            return uri && uri.indexOf(MobileUI.core.entity.EntityUtils.TEMP_URI) > -1;
        },

        tmpUriToApiFormat: function (uri) {
            return uri.replace(/uri\$\$/g, MobileUI.core.entity.EntityUtils.TEMP_URI_FOR_API);
        },

        tmpUriFromApiFormat: function (uri) {
            return uri.replace(new RegExp(MobileUI.core.entity.EntityUtils.TEMP_URI_FOR_API, 'g'), 'uri$$$$');
        },

        generateEmptyAttribute: function (attrTypes, parentUri, initialValue) {
            return (attrTypes || [])
                .filter(function (item) {
                    return !item.hidden && !MobileUI.core.entity.EntityUtils.isComplex(item);
                })
                .reduce(function (acc, attrType) {
                    var record = {};
                    var val = '';
                    if (attrType.type === 'String' &&
                        MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType) &&
                        !attrType.values && !attrType.lookupCode && initialValue) {
                        val = initialValue;
                        initialValue = '';
                    }
                    record[attrType.name] = [{
                        value: val,
                        uri: MobileUI.core.entity.EntityUtils.generateUri(parentUri, attrType.name),
                        type: attrType.uri
                    }];
                    return Ext.Object.merge(acc, record);
                }, {});
        },

        isEmptyAttribute: function (attributeValue) {
            return Ext.isObject(attributeValue) && !Object.keys(attributeValue).some(function (key) {
                return attributeValue[key].some(function (val) {
                    return val && (!Ext.isEmpty(val.value) || !Ext.isEmpty(val.lookupCode));
                });
            });
        }
    }
});