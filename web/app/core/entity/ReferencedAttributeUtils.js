/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.entity.ReferencedAttributeUtils', {
    requires: ['MobileUI.core.Metadata', 'MobileUI.core.entity.TemporaryEntity'],
    statics: {

        /**
         * Method creates the array of the attributes from referenced attribute
         * @param referencedAttribute {Object} referenced attribute type
         * @param referencedEntity {Object?} entity
         * @returns {Array}
         */
        createAttributesFromReferenced: function (referencedAttribute, referencedEntity)
        {
            if (Ext.isEmpty(referencedAttribute))
            {
                throw new Error('Illegal parameters. Method doesn\'t allow empty parameters');
            }

            var md = MobileUI.core.Metadata,
                refAttributeType = referencedAttribute.attrType,
                refEntityType = md.getEntityTypes()[refAttributeType.referencedEntityTypeURI],
                referencedAttributeUris = refAttributeType.referencedAttributeURIs,
                attributeObject, uri, n, obj, attributes,
                refEntityClone = Ext.clone(referencedEntity),
                extractFunction = function (item)
                {
                    return item.value;
                };

            attributes = [];
            for (n = 0; n < referencedAttributeUris.length; n++)
            {
                uri = referencedAttributeUris[n];
                if (uri.indexOf('configuration/relationTypes') === 0)
                {
                    attributeObject = MobileUI.core.Metadata.findRelationAttributeByUri(null, referencedAttributeUris[n]);
                }
                else
                {
                    attributeObject = MobileUI.core.Metadata.findEntityAttributeByUri(refEntityType, referencedAttributeUris[n]);
                }
                if (attributeObject !== null)
                {
                    obj = {
                        value: [],
                        name: attributeObject.name,
                        type: attributeObject.uri,
                        attrType: attributeObject,
                        uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(referencedAttribute.uri, attributeObject),
                        parentUri: referencedAttribute.uri
                    };
                    if (refEntityClone)
                    {
                        if (!Ext.isArray(refEntityClone.attributes[attributeObject.name]))
                        {
                            obj.value = [];
                        }
                        else
                        {
                            obj.value = (refEntityClone.attributes[attributeObject.name]).map(extractFunction);
                            obj.value = obj.value[0];
                            if (Ext.isObject(obj.value))
                            {
                                attributes = attributes.concat(this.createNestedAttribute(obj.value, obj.uri));
                            }
                        }
                        obj.refEntityUri = refEntityClone.uri;
                    }
                    attributes.push(obj);
                }
            }
            return attributes;
        },
        /**
         * @private
         * Method expands nested attribute to array
         * @param attributeValue {Object} attribute value from the entity
         * @param parentUri {String} parent attribute uri.
         * @returns {Array}
         */
        createNestedAttribute: function (attributeValue, parentUri)
        {
            var attribute, key, result = [], obj, attributeObject;
            if (Ext.isEmpty(attributeValue))
            {
                throw new Error('Illegal parameters. Method doesn\'t allow empty parameters');
            }

            for (key in attributeValue)
            {
                if (attributeValue.hasOwnProperty(key) && !Ext.isEmpty(attributeValue[key]))
                {
                    attribute = attributeValue[key][0];
                    attributeObject = MobileUI.core.Metadata.findEntityAttributeByUri(null, attribute.type);
                    if (attributeObject)
                    {
                        obj = {
                            value: attribute.value,
                            attrType: attributeObject,
                            name: attributeObject.name,
                            type: attributeObject.uri,
                            uri: MobileUI.core.entity.TemporaryEntity.createAttributeUri(parentUri, attributeObject),
                            parentUri: parentUri
                        };
                        result.push(obj);
                    }
                }
            }
            return result;
        },
        /**
         * Method add the reference attribute to entity
         * @param entity {Object} entity
         * @param referencedAttribute {Object} referenced attribute definition
         * @param referencedEntity {Object} source entity
         * @returns {Object}
         */
        fillReferenceAttribute: function (entity, referencedAttribute, referencedEntity)
        {
            if (Ext.isEmpty(entity) || Ext.isEmpty(referencedAttribute) || Ext.isEmpty(referencedEntity))
            {
                throw new Error('Illegal parameters. Method doesn\'t allow empty parameters');
            }

            var value, labelPattern, entityAttributes,
                attributes = this.createAttributesFromReferenced(referencedAttribute, referencedEntity);


            if (Ext.isArray(entity.attributes)) {
                entityAttributes = entity.attributes;
            }
            else {
                entityAttributes = Array.prototype.concat.apply([], Object.keys(entity.attributes).
                    map(function(key) { return entity.attributes[key]; }));
            }

            value = entityAttributes .filter(function(item) {
                return item.uri === referencedAttribute.uri;
            });
            value = Ext.isEmpty(value) ? null : value[0];

            if (value !== null)
            {
                value.refEntity = {
                    type: referencedEntity.type,
                    objectURI: referencedEntity.uri
                };
                var tmpRealtion = MobileUI.core.entity.TemporaryEntity.createNewEntity(referencedAttribute.attrType.relationshipTypeURI, null, {});
                value.refRelation = {
                    objectURI: tmpRealtion.uri
                };
                labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern(referencedAttribute.attrType.referencedEntityTypeURI);
                if (labelPattern !== null)
                {
                    value.label = !Ext.isEmpty(referencedEntity.label) ? referencedEntity.label :MobileUI.core.entity.EntityUtils.evaluateLabelFromAttributes(attributes, labelPattern);
                }
            }
            if (Ext.isArray(entity.attributes)) {
                entity.attributes = entity.attributes.concat(attributes);
            }
            return entity;
        }
    }
});
