/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.entity.TemporaryEntity', {
    requires: ['MobileUI.core.util.Util'],
    statics: {
        /**
         * @private
         * @type {Object}
         */
        temporaryEntities: {},
        states: {},
        /**
         * Method creates entity by entity type
         * @param entityType {Object|String} entity type
         * @param original {Object?}
         * @param attributesContainer {Object?}
         * @returns {{type: *, attributes: Array, uri: string}}
         */
        createNewEntity: function (entityType, original, attributesContainer, notSave, originalUri)
        {
            var uri = 'entities/__temp_' + MobileUI.core.util.Util.generateUUID(6),
                entity = original ? Ext.clone(original) : {
                    type: Ext.isObject(entityType) ? entityType.uri : entityType,
                    attributes: attributesContainer || []
                };
            entity.uri = originalUri || uri;
            var result = {
                original: entity,
                uri: entity.uri,
                type: entity.type,
                crosswalks: [],
                attributes: MobileUI.core.entity.EntityUtils.expandEntityAttributes(entity.attributes, entity.uri),
                addedAttributes: [],
                deletedAttributes: [],
                editedAttributes: []
            };
            if (!notSave){
                 MobileUI.core.entity.TemporaryEntity.temporaryEntities[entity.uri] = result;
            }
            return result;
        },

        /**
         * Method adds entity to the temporary entities
         * @param {Object} entity  entity object
         */
        addEntity: function (entity)
        {
            var uri = entity.uri;
            if (!MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri])
            {
                MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri] = entity;
            }
        },
        /**
         * Method creates unique attribute uri by old uri and attribute type
         * @param oldUri {String} old uri
         * @param attributeType {Object} attribute type object
         * @returns {string}
         */
        createAttributeUri: function (oldUri, attributeType)
        {
            if (!Ext.isObject(attributeType)){
                return oldUri;
            }
            return oldUri + (attributeType ? ( '/' + attributeType.name + '/' + MobileUI.core.util.Util.generateUUID(6)) : '');
        },
        /**
         * Method clears the entities temporary list
         */
        removeAllEntities: function ()
        {
            MobileUI.core.entity.TemporaryEntity.temporaryEntities = {};
            MobileUI.core.entity.TemporaryEntity.states ={};
        },
        removeEntity: function (uri)
        {
            delete MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri];
            delete MobileUI.core.entity.TemporaryEntity.states[uri];
        },
        /**
         * Method updates the entity in the list
         * if entity is not exists it add to the temporary list
         * @param entity {Object} entity object
         */
        updateEntity: function (entity)
        {
            var uri = entity.uri;
            MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri] = entity;
        },
        saveState: function (entity, state) {
            var uri = entity.uri;
            if (!this.states[uri])
            {
                this.states[uri] = [];
            }
            this.states[uri].push(state);
            return this.updateState(entity, state);
        },
        getCurrentState: function(entityUri)
        {
            var uri = entityUri;
            var state = null;
            if (this.states[uri])
            {
                var states = this.states[uri];
                state = states[states.length - 1];
            }
            return this.getEntity(uri, state);
        },
        updateState: function(entity, state) {
                var uri = !state || state ===entity.uri ? entity.uri: entity.uri + '/' + state;
                var result = Ext.clone(entity);
                MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri] = result;
                return result;
        },
        popState: function(uri){
            var state = this.states[uri].pop();
            var result = MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri + '/' + state];
            delete MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri + '/' + state];
            return result;
        },

        /**
         * Method clears all not saved states begining from state
         * @param entityUri {String} entity uri
         * @param state {String} state
         */
        clearStateFrom: function(entityUri, state){
            var uri = !state || state === entityUri ? entityUri: entityUri + '/' + state;
            var toFilter = [];
            var states = MobileUI.core.entity.TemporaryEntity.temporaryEntities;
            for(var key in states){
                if (states.hasOwnProperty(key)){
                    if (key.indexOf(uri) === 0 && key !== uri){
                        toFilter.push(key);
                        delete states[key];
                    }
                }
            }
            this.states[entityUri] = !Ext.isArray(this.states[entityUri]) ? this.states[entityUri] : this.states[entityUri].filter(function (item) {
                return !toFilter.some(function (inFilter) {
                    return inFilter.lastIndexOf(item) === inFilter.length - item.length;
                });
            });
        },
        /**
         * Method returns entity by uri
         * @param uri {String} entity uri
         * @param state {String?} state
         * @returns {*}
         */
        getEntity: function (uri, state)
        {
            state = state ? '/' + state : '';
            return MobileUI.core.entity.TemporaryEntity.temporaryEntities[uri + state] || null;
        },

        isTemp: function(uri) {
            return uri && (uri.indexOf('__temp_') !== -1);
        }
    }
});
