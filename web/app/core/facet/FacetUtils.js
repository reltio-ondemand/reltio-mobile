/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.facet.FacetUtils', {
    statics: {
        TREE_VIEW: 'com.reltio.plugins.entity.TreeView',
        TWO_HOPS_FACET: 'com.reltio.plugins.entity.TwoHopsFacet',
        ATTRIBUTES_FACET: 'com.reltio.plugins.entity.AttributesFacet',
        CUSTOM_FACET: 'com.reltio.plugins.ui.CustomActionView',
        PLACES_FACET: 'com.reltio.plugins.entity.PlacesFacet',
        /**
         * Method creates the predefined facets
         * @param attributes {Object} attributes
         * @returns {*|{attributesFacet, attributes}}
         * @param keepAttributes {Boolean} do not modify attributes
         * @param perspectiveConfig {MobileUI.core.configuration.ConfigurationManager} config
         */
        createPredefinedFacets: function (attributes, perspectiveConfig, keepAttributes)
        {
            var result, extension, label = 'Location';
            if (perspectiveConfig)
            {
                extension = perspectiveConfig.getExtensionById(MobileUI.core.facet.FacetUtils.PLACES_FACET);
                if (extension && Ext.isString(extension.label))
                {
                    label = extension.label;
                }
            }
            result = this.convertAttributesToFacets(label, 'resources/images/reltio/profile/place_sm.png', this.createFacetsUrl(MobileUI.core.facet.FacetUtils.PLACES_FACET), MobileUI.core.facet.FacetUtils.PLACES_FACET, attributes, this.makeFilterFunction('referencedEntityTypeURI', 'configuration/entityTypes/Location', function (a, b)
            {
                return b.indexOf(a) !== -1;
            }), keepAttributes);
            return result;
        },
        /**
         * Method converts attributes to facet
         * @param caption {String} caption
         * @param img {String} image url
         * @param url {String} action url
         * @param id {String} facet id
         * @param attributes {Object} attributes
         * @param filterFunction {Function} filter function
         * @param keepAttributes {Boolean} do not modify attributes
         * @returns {{attributesFacet: {attributes: Array, badgeText: *, caption: *, image: *, action: *}, attributes: *}}
         */
        convertAttributesToFacets: function (caption, img, url, id, attributes, filterFunction, keepAttributes)
        {
            var key, filtered = [], tmpFiltered, total;
            for (key in attributes)
            {
                if (attributes.hasOwnProperty(key))
                {
                    tmpFiltered = attributes[key].filter(filterFunction);
                    filtered = filtered.concat(tmpFiltered);
                    if (!keepAttributes) {
                        attributes[key] = Ext.Array.difference(attributes[key], tmpFiltered);
                    }
                }
            }
            total = filtered.reduce(function (previous, current)
            {
                if (Ext.isArray(current.value))
                {
                    previous += current.value.length;
                }
                return previous;
            }, 0);
            return {
                attributesFacet: {
                    attributes: filtered,
                    badgeText: total,
                    caption: caption,
                    id: id,
                    image: img,
                    defaultImage: img,
                    action: url
                },
                attributes: attributes
            };
        },
        /**
         * Method create the facet url
         * @param facetClass {String} facet class
         * @param id {String?} facet id
         * @returns {string}
         */
        createFacetsUrl: function (facetClass, id)
        {
            var uri = MobileUI.core.util.HashUtil.encodeUri(MobileUI.core.session.Session.getEntityUri());
            if (MobileUI.core.facet.FacetUtils.TREE_VIEW === facetClass)
            {
                return 'relationstree/' + id + '$' + uri;
            }
            else if (MobileUI.core.facet.FacetUtils.ATTRIBUTES_FACET === facetClass)
            {
                return 'attributesfacet/' + id + '$' + uri;
            }
            else if (MobileUI.core.facet.FacetUtils.CUSTOM_FACET === facetClass)
            {
                return 'customview/' + id + '$' + uri;
            }
            else if (MobileUI.core.facet.FacetUtils.PLACES_FACET === facetClass)
            {
                return 'placesfacet/' + uri;
            }
            else if (MobileUI.core.facet.FacetUtils.TWO_HOPS_FACET === facetClass)
            {
                return 'relations/twohopsconnection$' + id + '$' + uri;
            }
            else
            {
                return 'relations/' + id + '$' + uri;
            }
        },
        /**
         * Method creates the filter function
         * @param parameters {String} condition parameter
         * @param value {String} value
         * @param func {Function} filter function
         * @returns {Function}
         */
        makeFilterFunction: function (parameters, value, func)
        {
            return function (element)
            {
                if (element && element.attrType)
                {
                    return func.call(this, element.attrType[parameters], value);
                }
                return false;
            };
        }
    }
});
