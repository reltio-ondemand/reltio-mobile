/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.feedback.CustomProvider', {
    processIssue: function(issueInfo, environment, success, failure, self){
        issueInfo.files = (issueInfo.files ||[]).map(function(item){
            return item.url;
        });
        var data = {
            user: MobileUI.core.session.Session.getUserName(),
            recipient: issueInfo.options,
            subject: issueInfo.summary,
            priority: issueInfo.priority,
            collaborator_ids: issueInfo.ccs,
            body: 'Environment:\n'+environment.join('\n')+'\nDescription:\n'+issueInfo.description+ '\nSummary:\n'+issueInfo.summary,
            tenant: MobileUI.core.Services.getTenantName(),
            images: issueInfo.files
        };
        MobileUI.core.Services.asyncRequest('/mail', function (json)
            {
                if (success)
                {
                    var result = json.status === 'success' ? {title:'New ticket', text:'New ticket has been created'} :{title:'Error', text:'Can\'t send message'} ;
                    success.call(self, result);
                }
            }, function (error)
            {
                if (failure)
                {
                    failure.call(self, error);
                }
            }, {
                parse: 'json',
                method: 'POST',
                data: Ext.JSON.encode(data),
                signer: Ext.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            },
            this);
    }
});
