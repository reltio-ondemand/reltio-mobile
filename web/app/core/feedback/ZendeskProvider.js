/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.feedback.ZendeskProvider', {
    processIssue: function(issueInfo, environment, success, failure, self){
        //summary, description, environment, files, options,
        MobileUI.core.Services.asyncRequest('/zendesk/issue', function (json)
            {
                if (json.ticket && issueInfo.files.length > 0)
                {
                    this.attachFiles(json.ticket.id, json.ticket.requester_id, issueInfo.files, success, failure, self);
                }
                else {
                    if (success) {
                        var result;
                        if (json.error || !json.ticket || !json.ticket.id) {
                            result = {title: 'Error', text: ''};
                            if (Ext.isObject(json.error) && json.error.message) {
                                result.text = json.error.message;
                            }
                            else {
                                result.text = json.description || json.error || 'Unknown error';
                            }

                        } else {
                            result = {title: 'New ticket', text: 'New ticket has been created: #' + json.ticket.id};
                        }
                        success.call(self, result);
                    }
                }
            }, function (error)
            {
               if (failure)
               {
                   failure.call(self, error);
               }
            }, {
                parse: 'json',
                method: 'POST',
                data: 'summary=' + encodeURIComponent(issueInfo.summary) +
                '&description=' + encodeURIComponent(issueInfo.description) +
                '&tenant=' + MobileUI.core.Services.getTenantName() +
                '&steps_to_reproduce=' + encodeURIComponent(issueInfo.stepsToReproduce) +
                '&actual_results=' + encodeURIComponent(issueInfo.actualResults) +
                '&expected_results=' + encodeURIComponent(issueInfo.expectedResults) +
                '&due_date=' + encodeURIComponent(issueInfo.dueDate) +
                '&did_it_works='+ encodeURIComponent(issueInfo.didItWork)+
                '&business_impact=' + encodeURIComponent(issueInfo.businessImpact) +
                '&collaborator_ids=' + encodeURIComponent('['+issueInfo.ccs+']') +
                '&priority=' + encodeURIComponent(issueInfo.priority) +
                '&regarding=' + encodeURIComponent(issueInfo.options) +
                '&user=' + MobileUI.core.session.Session.getUser().full_name +
                '&environment=' + environment.join('\n'),
                signer: Ext.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            },
            this);
    },

    attachFiles: function (key, userZendeskId, files, success, failure, self)
    {
        if (MobileUI.core.session.Session.isExpired())
        {
            MobileUI.core.session.Session.refreshAccessToken(function ()
            {
                this.self.attachFiles(this.key, this.userZendeskId, this.files, this.success, this.failure, this.context);
            }, {self: this, key: key, userZendeskId: userZendeskId, files: files, success: success,failure:failure, context:self});
            return;
        }
        MobileUI.core.Logger.info('Tries to attach image');
        MobileUI.core.Services.asyncRequest('/zendesk/files', function ()
            {
                if (this.success)
                {
                    var result = {title:'New ticket', text:'New ticket has been created: #' + key};

                    this.success.call(this.context, result);
                }
            },
            function (error)
            {
                if (this.failure)
                {
                    this.failure.call(this.context,error);
                }
            },
            {
                parse: 'json',
                method: 'POST',
                data: 'issueId=' + key + '&userZendeskId=' + userZendeskId + '&user=' + MobileUI.core.session.Session.getUser().full_name + '&files=' + Ext.encode(files),
                signer: Ext.bind(MobileUI.core.session.Session.addSignHeader, MobileUI.core.session.Session)
            }, {self: this, key: key, userZendeskId: userZendeskId, files: files, success: success,failure:failure, context:self});
    }

});