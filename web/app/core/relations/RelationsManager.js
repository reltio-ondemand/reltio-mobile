/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.relations.RelationsManager', {
    mixins:{
        observable: 'Ext.mixin.Observable'
    },
    singleton: true,
    ATTR_START_DATE: {
        label: 'Start date',
        activeness: true,
        name: 'startDate',
        type: 'Date',
        uri: 'startDate'
    },
    ATTR_END_DATE: {
        label: 'End date',
        activeness: true,
        name: 'endDate',
        type: 'Date',
        uri: 'endDate'
    },
    state: null,
    baseEntityUri: null,
    /**
     * Method saves relation
     * @param state
     * @param entityUri
     * @param relationType
     * @param relationDirection
     * @param success
     * @param self
     */
    saveRelation: function (state, entityUri, relationType, relationDirection, success, self)
    {
        var startDate = null,
            endDate = null,
            object, inRelation, startEntityUri, endEntityUri, relationTypeUri, attributes = [],
            filter = function (item)
            {
                    if (item.uri === MobileUI.core.relations.RelationsManager.ATTR_START_DATE.uri)
                    {
                        startDate = MobileUI.core.I18n.parseDate(item.value, MobileUI.core.I18n.DATE_INTERNAL);
                        startDate = startDate && startDate.getTime();
                        return false;
                    }
                    if (item.uri === MobileUI.core.relations.RelationsManager.ATTR_END_DATE.uri)
                    {
                        endDate = MobileUI.core.I18n.parseDate(item.value, MobileUI.core.I18n.DATE_INTERNAL);
                        endDate = endDate && endDate.getTime();
                        return false;
                    }
                return true;
            };

        attributes = (state.attributes || []).filter(filter);
        object = MobileUI.core.entity.EntityUtils.processEntitiesAttributes(attributes, 'relation');
        inRelation = state.isInRelation !== undefined ? state.isInRelation : relationDirection === 'in';
        startEntityUri = inRelation ? state.entityUri : entityUri;
        endEntityUri = inRelation ? entityUri : state.entityUri;
        relationTypeUri = state.relationTypeChanged || relationType.uri;
        MobileUI.core.session.Session.addRelation(startEntityUri, endEntityUri, relationTypeUri, object, startDate, endDate, success, function (error)
        {
            MobileUI.core.Logger.error(error);
        }, self);

    },

    setBaseEntityUri: function (uri)
    {
        this.baseEntityUri = uri;
    },

    resetBaseEntityUri: function ()
    {
        this.baseEntityUri = null;
    },
    getBaseEntityUri: function ()
    {
        return this.baseEntityUri || MobileUI.core.session.Session.getEntityUri();
    },
    /**
     * Method fires events
     */
    fireSaveRelationEvent: function(connectedEntityUri) {
        this.fireEvent('createRelation', connectedEntityUri);
        MobileUI.core.relations.RelationsManager.setState({});
    },
    /**
     * Method fires before save relation.
     * @param connectedEntityUri
     */
    fireBeforeSaveRelationEvent: function(connectedEntityUri){
       return this.fireEvent('beforeCreateRelation', connectedEntityUri);
    },
    /**
     * Method set states
     * @param state {Object} state
     */
    setState: function (state)
    {
        this.state = state;
    },
    /**
     * Method returns state
     * @returns {Object}
     */
    getState: function ()
    {
        return this.state;
    }
});
