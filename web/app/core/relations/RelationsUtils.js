/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.relations.RelationsUtils', {
    requires: ['MobileUI.core.Metadata', 'MobileUI.core.entity.EntityItemUtils'],

    statics: {
        SINGLE_IN_RELATION: 1,
        SINGLE_OUT_RELATION: 2,
        MULTIPLE_RELATIONS: 3,
        direction: {
            IN: 'in',
            OUT: 'out'
        },
        /**
         * @private
         * @param arr
         * @param types
         * @returns {Array}
         */
        prepareRelations: function (arr, types)
        {
            var temp = Ext.Array.clone(arr),
                i, strType, key, obj, isKeyInTemp,
                checkTemp = function (t)
                {
                    return t === this.key || t.uri && t.uri === this.key;
                };

            for (i = 0; i < temp.length; i++)
            {
                if (Ext.isObject(temp[i]))
                {
                    temp[i] = Ext.clone(temp[i]);
                }

                strType = Ext.isString(temp[i]) ? temp[i] : temp[i].uri;
                if (strType.indexOf('!') === 0)
                {
                    strType = strType.substring(1);
                    if (Ext.isString(temp[i]))
                    {
                        temp[i] = strType;
                    }
                    else
                    {
                        temp[i].uri = strType;
                    }
                }
                else
                {

                    for (key in types)
                    {
                        if (types.hasOwnProperty(key))
                        {
                            isKeyInTemp = temp.some(checkTemp, {key: key});

                            if ((types[key].extendsTypeURI === strType || types[key].extendsTypeURI === strType) && !isKeyInTemp)
                            {
                                if (Ext.isString(temp[i]))
                                {
                                    temp.push(key);
                                }
                                else
                                {
                                    obj = Ext.clone(temp[i]);
                                    obj.uri = key;
                                    temp.push(obj);
                                }
                            }
                        }
                    }
                }
                if (!types[strType] || !MobileUI.core.PermissionManager.securityService().metadata.canRead(types[strType]))
                {
                    temp.splice(i, 1);
                    i--;
                }
            }
            return temp;
        },
        /**
         * @private
         * Method processes the in and out relations
         * @param inRelations {Array} in relations
         * @param outRelations {Array} out relations
         * @param relTypes   {Object} relation types
         * @param entityTypes {Array} entity types
         * @returns {*|Array}
         */
        processRelationsInternal: function (inRelations, outRelations, relTypes, entityTypes)
        {
            var temp, i, strType, rel;
            inRelations = inRelations || [];
            outRelations = outRelations || [];
            temp = [];

            for (i = 0; i < inRelations.length; i++)
            {
                strType = Ext.isString(inRelations[i]) ? inRelations[i] : inRelations[i].uri;
                rel = relTypes[strType];
                if (rel && temp.indexOf(rel.startObject.objectTypeURI) === -1)
                {
                    temp.push(rel.startObject.objectTypeURI);
                }
            }
            for (i = 0; i < outRelations.length; i++)
            {
                strType = Ext.isString(outRelations[i]) ? outRelations[i] : outRelations[i].uri;
                rel = relTypes[strType];
                if (rel && temp.indexOf(rel.endObject.objectTypeURI) === -1)
                {
                    temp.push(rel.endObject.objectTypeURI);
                }
            }
            temp = this.prepareRelations(temp, entityTypes);
            return temp;
        },

        /**
         * @private
         */
        processRelationContent: function(content, type)
        {
            if (content)
            {
                var md = MobileUI.core.Metadata,
                    entry = content,
                    relations = md.getRelationTypes(),
                    entityTypes = md.getEntityTypes(),
                    inRelations, outRelations, rels, found;

                while (entry)
                {
                    if (entry.inRelations)
                    {
                        entry.inRelations = this.prepareRelations(entry.inRelations, relations);
                    }
                    if (entry.outRelations)
                    {
                        entry.outRelations = this.prepareRelations(entry.outRelations, relations);
                    }

                    if (entry.entityTypes)
                    {
                        entry.entityTypes = this.prepareRelations(entry.entityTypes, entityTypes);
                    }
                    entry = entry.nextEntry;
                }

                if (!content.nextEntry && !content.entityTypes)
                {
                    content.entityTypes = this.processRelationsInternal(content.inRelations, content.outRelations, relations, entityTypes);
                }

                inRelations = content.inRelations || [];
                outRelations = content.outRelations || [];

                if (type && relations[type.name])
                {
                    found = inRelations.some(function (relation)
                    {
                        return relation === this.name || relation.uri && relation.uri === this.name;
                    }, type);

                    if (!found)
                    {
                        found = outRelations.some(function (relation)
                        {
                            return relation === this.name || relation.uri && relation.uri === this.name;
                        }, type);
                    }

                    if (!found)
                    {
                        rels = content[type.direction];
                        if (!rels)
                        {
                            rels = content[type.direction] = [];
                        }
                        rels.unshift(type.name);
                    }
                }
            }
        },

        /**
         * @public
         * Method processes the in and out relations
         * @param config {Object}
         * @returns {*|Array}
         */
        processRelations: function (config)
        {
            if (config)
            {
                this.processRelationContent(config.content, config.defaultConfigType);
                if (config.contentSecondLevel)
                {
                    this.processRelationContent(config.contentSecondLevel);
                }
            }
        },
        /**
         * @private
         * Method collects in relations
         * @param config {Object} config object
         * @returns {Array.<T>}
         */
        collectInRelations: function (config)
        {
            var md = MobileUI.core.Metadata;

            return (config.inRelations || [])
                .map(function (relation)
                {
                    return Ext.isObject(relation) ? relation.uri : relation;
                })
                .map(md.getRelationType, md)
                .filter(function (relationType)
                {
                    return relationType && relationType.startObject && relationType.startObject.directionalContext && relationType.startObject.directionalContext.length === 1;
                });
        },
        /**
         * @private
         * Method collects out relations
         * @param config {Object} config object
         * @returns {Array.<T>}
         */
        collectOutRelations: function (config)
        {
            var md = MobileUI.core.Metadata;
            return (config.outRelations || [])
                .map(function (relation)
                {
                    return Ext.isObject(relation) ? relation.uri : relation;
                }).map(md.getRelationType, md).filter(function (relationType)
                {
                    return relationType && relationType.endObject && relationType.endObject.directionalContext && relationType.endObject.directionalContext.length === 1;
                });
        },
        /**
         * @private
         * Method groups duplicate relations
         * @param items {Array}
         * @returns {*}
         */
        unionDuplicatedRelations: function (items)
        {
            var labelToItemsMap = {},
                label,
                arr,
                relationType,
                parseItems = function (item)
                {
                    relationType = item.relationType;
                    item.label = (item.label + ' (' + relationType.label + ')');
                };
            items.forEach(function (item)
            {
                var label = item.label;
                this[label] = this[label] || [];
                this[label].push(item);
            }, labelToItemsMap);

            for (label in labelToItemsMap)
            {
                if (labelToItemsMap.hasOwnProperty(label))
                {
                    arr = labelToItemsMap[label];
                    if (arr.length > 1)
                    {
                        arr.forEach(parseItems);
                    }
                }
            }

            items.sort(function (a, b)
            {
                var aLabel = a.label;
                var bLabel = b.label;
                return aLabel < bLabel ? -1 : aLabel === bLabel ? 0 : 1;
            });
            return items;
        },
        /**
         * Method create relations type select model
         * @param config {Object} relations config
         * @param relationType {Object?} relation type
         * @param direction {String?} direction
         * @returns {{items: *, selected: Object|null}}
         */
        createRelationTypeSelectModel: function (config, relationType, direction)
        {
            var inRelations,
                outRelations,
                selected,
                items;
            if (Ext.isEmpty(config))
            {
                return;
            }
            inRelations = this.collectInRelations(config);
            outRelations = this.collectOutRelations(config);
            selected = null;
            items = inRelations
                .map(function (relType)
                {
                    var label = relType.startObject.directionalContext[0].labelPattern,
                        item = {label: label, relationType: relType, isInRelation: true};
                    if (relationType === relType && direction === 'in')
                    {
                        selected = item;
                    }
                    return item;
                }, this)
                .concat(outRelations
                    .map(function (relType)
                    {
                        var label = relType.endObject.directionalContext[0].labelPattern,
                            item = {label: label, relationType: relType, isInRelation: false};
                        if (relationType === relType && direction === 'out')
                        {
                            selected = item;
                        }
                        return item;
                    }, this));

            items = this.unionDuplicatedRelations(items);
            return {items: items, selected: selected};
        },
        isCorrectRelationType: function (type, config)
        {
            var content = config.content;
            var RelationsUtils = MobileUI.core.relations.RelationsUtils,
                types = [RelationsUtils.SINGLE_IN_RELATION, RelationsUtils.SINGLE_OUT_RELATION, RelationsUtils.MULTIPLE_RELATIONS];
            var relType = Ext.isObject(type) ? type.result : type,
                isCorrectType = types.indexOf(relType) !== -1;
            if (isCorrectType && content) {
                if (relType !== RelationsUtils.MULTIPLE_RELATIONS) {
                    var entityType = MobileUI.controller.dialogs.RelationshipType.getEntityType(type, relType === RelationsUtils.SINGLE_IN_RELATION, content);
                    isCorrectType = MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) && (!config.allowOnlyCreateNew || MobileUI.core.PermissionManager.securityService().metadata.canCreate(entityType));
                } else {
                    isCorrectType = type.inRelations.filter(function(type){
                        var entityType = MobileUI.controller.dialogs.RelationshipType.getEntityType(type, true, content);
                        return MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) && (!config.allowOnlyCreateNew || MobileUI.core.PermissionManager.securityService().metadata.canCreate(entityType));
                    }).concat(type.outRelations.filter(function(type){
                        var entityType = MobileUI.controller.dialogs.RelationshipType.getEntityType(type, false, content);
                        return MobileUI.core.PermissionManager.securityService().metadata.canRead(entityType) && (!config.allowOnlyCreateNew || MobileUI.core.PermissionManager.securityService().metadata.canCreate(entityType));
                    })).length > 0;
                }
            }
            return isCorrectType;

        },
        /**
         * Method returns the content relations type
         * @param config {Object} relations config
         * @param relationType {Object?} relations type
         * @param direction {String?} direction
         * @returns {*}
         */
        getContentRelationsType: function (config, relationType, direction)
        {
            var inRelations = (config.inRelations || []).filter(function (relType)
                {
                    if (Ext.isObject(relType)){
                        relType = relType.uri;
                    }
                    return MobileUI.core.PermissionManager.securityService().metadata.canCreate(relType) || relationType && direction === 'in' && relationType.uri === relType;
                }, this),
                outRelations = (config.outRelations || []).filter(function (relType)
                {
                    if (Ext.isObject(relType)){
                        relType = relType.uri;
                    }
                    return MobileUI.core.PermissionManager.securityService().metadata.canCreate(relType) || relationType && direction === 'out' && relationType.uri === relType;
                }),
                relationsCount,
                result;

            relationsCount = inRelations.length + outRelations.length;

            if (relationsCount === 0)
            {
                result = null;
            }
            else if (relationsCount === 1)
            {
                result = inRelations.length > 0 ? MobileUI.core.relations.RelationsUtils.SINGLE_IN_RELATION : MobileUI.core.relations.RelationsUtils.SINGLE_OUT_RELATION;
            }
            else
            {
                result = MobileUI.core.relations.RelationsUtils.MULTIPLE_RELATIONS;
            }
            return {result: result, inRelations: inRelations, outRelations: outRelations};
        },
        processSpecialAttributes: function(relation, hiddenAttributes){
            var attrType, result = [];
            hiddenAttributes = hiddenAttributes || [];
            if (relation.startDate && hiddenAttributes.indexOf('startDate') === -1) {
                attrType = Ext.clone(MobileUI.core.relations.RelationsManager.ATTR_START_DATE);
                result.push({
                    value: [{
                        value: i18n(new Date(relation.startDate), MobileUI.core.I18n.DATE_INTERNAL)
                    }],
                    attrType: attrType,
                    showLabel: true
                });
            }
            if (relation.endDate && hiddenAttributes.indexOf('endDate') === -1) {
                attrType = Ext.clone(MobileUI.core.relations.RelationsManager.ATTR_END_DATE);
                result.push(
                    {
                        value: [{
                            value: i18n(new Date(relation.endDate), MobileUI.core.I18n.DATE_INTERNAL)
                        }],
                        attrType: attrType,
                        showLabel: true
                    });
            }
            return result;
        },
        isAttributeModelAvailableInEditMode: function(item) {
            return (item.type !== MobileUI.components.list.items.BaseListItem.types.header);
        },

        isAttributeModelAvailableInCreateMode: function(item) {
            return MobileUI.core.relations.RelationsUtils.isAttributeModelAvailableInEditMode(item) &&
                !(item.type === MobileUI.components.list.items.BaseListItem.types.buttons &&
                item.userData && item.userData.type === 'addButton');
        }
    }
});
