/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.sandbox.API', {
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.core.sandbox.HtmlSanitizeElements',
        'MobileUI.core.util.Util'
    ],
    mixins: {observable: 'Ext.mixin.Observable'},
    statics: {
        WORKFLOW: 'workflow',
        WORKFLOW_START_PROCESS_FROM_QUERY: 'workflow/startProcessFromQuery',
        API: 'api',
        GET_ENTITY_URI: 'getEntityUri',
        GET_ENTITY: 'getEntity',
        GET_API_PATH: 'getApiPath',
        WORKFLOW_GET_WORKFLOW_PATH: 'workflow/getWorkflowPath',
        GET_SEARCH_QUERY: 'getSearchQuery',
        GET_TENANT: 'getTenant',
        GET_PERSPECTIVE: 'getPerspective',
        SET_PERSPECTIVE: 'setPerspective',
        SET_ENTITY_URI: 'setEntityUri',
        ALERT: 'alert',
        CONFIRM: 'confirm',
        PROMPT: 'prompt',
        GET_CONFIGURATION: 'getConfiguration',
        GET_UI_CONFIGURATION: 'getUiConfiguration',
        OPEN_SEARCH: 'openSearch',
        WORKFLOW_CHECK_PERMISSION: 'workflow/checkPermission'
    },
    /**
     * init API method
     * @param permissions {Array} list of available API endpoints (reg-exp allowed)
     * @param config {Object} UI configuration
     */
    initApi: function (permissions, config) {
        this.permissions = permissions || [];
        this.config = config;
    },

    permissions: null,
    config: null,

    /**
     * Method which is allow to log the message.
     * @param log {String} message
     */
    log: function (log) {
        // eslint-disable-next-line no-console
        if (window.console && console.log){
            // eslint-disable-next-line no-console
            console.log(log);
        } else {
            this.info(log);
        }
    },
    /**
     * Method which is allow to change visibility state of control.
     * @param value {String} 'visible' | 'hidden' | 'excluded'
     */
    setVisibility: function (value) {
        this.fireEvent('changeVisibility', {visibility: value});
    },

    /**
     * the entry point of the api.
     * @typedef Message
     * @type {object}
     * @property {string} action - action identifier.
     * @property {object | string} params - the parameters.
     * @param params {Message} message from the sandbox;
     */
    process: function (params) {
        if (params) {
            if (params.action === 'log') {
                this.log(params.params);
            }
            else if (params.action === 'setVisibility') {
                this.setVisibility(params.params || 'visible');
            }
            else if (params.action === 'setHtml') {
                this.setHtml(params.params);
            }
            else if (params.action === 'setHeight') {
                var height = params.params || null;
                this.fireEvent('changeSize', {height: height});
            }
            else if (params.action === 'request') {
                this.processRequest(params.params.name, params);
            }
        }
    },
    fireDataEvent: function (id, name, result) {
        this.fireEvent('response', {
            action: 'response',
            id: id,
            name: name,
            result: result
        });
    },
    /**
     * Method which is allow to process request from the user custom code
     * @param name {String} the method name
     * @param paramObject {Object}
     */
    processRequest: function (name, paramObject) {
        var API = MobileUI.core.sandbox.API;
        var session = MobileUI.core.session.Session;
        if (session) {
            var md = MobileUI.core.Metadata;
            switch (name) {
                case API.GET_ENTITY_URI:
                    this.fireDataEvent(paramObject.params.id, name, session.getEntityUri());
                    break;
                case API.GET_ENTITY:
                    this.fireDataEvent(paramObject.params.id, name, session.getEntity());
                    break;
                case API.GET_API_PATH:
                    this.fireDataEvent(paramObject.params.id, name, session.getApiPath());
                    break;
                case API.GET_SEARCH_QUERY:
                    this.fireDataEvent(paramObject.params.id, name, session.getSearchEngine().getQuery());
                    break;
                case API.GET_TENANT:
                    this.fireDataEvent(paramObject.params.id, name, session.getTenant());
                    break;
                case API.SET_ENTITY_URI:
                    session.on('updateEntity', function () {
                        this.fireDataEvent(paramObject.params.id, name, session.getEntity());
                    }, this, {single: true});
                    session.initEntity(paramObject.params.entityUri);
                    break;
                case API.SET_PERSPECTIVE:
                    var to = !Ext.isEmpty(paramObject.params.perspective) ? paramObject.params.perspective : '';
                    to = to[0] === '#' ? to.substring(1) : to;
                    MobileUI.core.Navigation.redirectForward(to);
                    break;
                case API.GET_PERSPECTIVE:
                    var hash = MobileUI.core.Services.getLocationObject().hash;
                    hash = hash.length > 1 ? hash.substring(1) : '';
                    this.fireDataEvent(paramObject.params.id, name, hash);
                    break;
                case API.API:
                    this.sendApiRequest(paramObject.params);
                    break;
                case API.ALERT:
                    this.fireDataEvent(paramObject.params.id, name, window.alert(paramObject.params.text));
                    break;
                case API.CONFIRM:
                    this.fireDataEvent(paramObject.params.id, name, window.confirm(paramObject.params.text));
                    break;
                case API.PROMPT:
                    this.fireDataEvent(paramObject.params.id, name, window.prompt(paramObject.params.text, paramObject.params.defaultText || ''));
                    break;
                case API.GET_CONFIGURATION:
                    this.fireDataEvent(paramObject.params.id, name, md.getRawConfiguration());
                    break;
                case API.GET_UI_CONFIGURATION:
                    var config = Ext.clone(this.config);
                    if (config.permissions) {
                        delete config.permissions;
                    }
                    this.fireDataEvent(paramObject.params.id, name, config);
                    break;
                case API.OPEN_SEARCH:
                    this.openSearch(paramObject.params);
                    break;
            }
        }
    },

    checkParams: function (params, type) {
        if (!params.url) {
            this.fireDataEvent(params.id, type, {errorMessage: 'URL is empty'});
            return false;
        }

        var allowed = this.permissions.some(function (perm) {
            var parsed = MobileUI.core.util.Util.splitUri(this.url);
            var host = parsed.host;
            var protocolAndHost = parsed.protocol + '://' + parsed.host;
            perm = perm.replace(/\/*$/, '');
            return perm === this.url || host && host === perm || host && protocolAndHost === perm;
        }, params);

        if (!allowed || this.permissions.length === 0) {
            this.fireDataEvent(params.id, type, {errorMessage: 'Not enough permissions'});
            return false;
        }
        return true;
    },

    getOptions: function (params) {
        params = params || {};
        var options = {
            method: params.method || 'GET',
            tenant: params.tenant,
            headers: params.headers,
            data: params.data
        };
        for (var key in options) {
            if (options.hasOwnProperty(key) && (typeof options[key] === 'undefined' || options[key] === null)) {
                delete options[key];
            }
        }
        return options;
    },

    sendApiRequest: function (params) {
        var session = MobileUI.core.session.Session;
        var API = MobileUI.core.sandbox.API;
        if (!session) {
            return;
        }
        if (!this.checkParams(params, API.API)) {
            return;
        }
        var options = this.getOptions(params);

        session.sendRequest(params.url, [], options, function (json) {
            this.self.fireDataEvent(this.params.id, API.API, json);
        }, function (json) {
            this.self.fireDataEvent(this.params.id, API.API, json);
        },{self: this, params: params});
    },

    openSearch: function (params) {
        if (params.searchState) {
            MobileUI.core.search.SearchParametersManager.loadState(params.searchState);
        }
        MobileUI.core.Navigation.redirectForward('search/init');
    },

    setHtml: function (params) {
        if (window.html_sanitize) {
            MobileUI.core.sandbox.HtmlSanitizeElements.applyProperties();
            params.html = window.html_sanitize(params.html || '', function (url) {
                return url;
            }, function (id) {
                return id;
            });

            this.fireEvent('changeHtml', params);
        }
    }
});