/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.sandbox.HtmlSanitizeElements', {
    statics: {

        __SVG_ELEMENTS: ['animate', 'animateColor', 'animateMotion', 'animateTransform', 'mpath', 'set', 'circle', 'ellipse',
            'line', 'polygon', 'polyline', 'rect', 'defs', 'glyph', 'g', 'marker', 'mask', 'missing-glyph', 'pattern', 'svg',
            'switch', 'symbol', 'font', 'font-face', 'font-face-format', 'font-face-name', 'font-face-src', 'font-face-uri',
            'hkern', 'vkern', 'linearGradient', 'radialGradient', 'stop', 'circle', 'ellipse', 'image', 'line', 'path',
            'polygon', 'polyline', 'rect', 'text', 'use', 'feDistantLight', 'fePointLight', 'feSpotLight', 'circle', 'ellipse',
            'line', 'path', 'polygon', 'polyline', 'rect', 'defs', 'g', 'svg', 'symbol', 'use', 'altGlyph', 'altGlyphDef',
            'altGlyphItem', 'glyph', 'glyphRef', 'textPath', 'text', 'tref', 'tspan', 'altGlyph', 'textPath', 'tref',
            'tspan', 'clipPath', 'color-profile', 'cursor', 'filter', 'foreignObject', 'script', 'style', 'view'],

        __SVG_ATTRIBUTES: ['accent-height', 'accumulate', 'additive', 'alignment-baseline', 'allowReorder', 'alphabetic',
            'amplitude', 'arabic-form', 'ascent', 'attributeName', 'attributeType', 'autoReverse', 'azimuth', 'baseFrequency',
            'baseline-shift', 'baseProfile', 'bbox', 'begin', 'bias', 'by', 'calcMode', 'cap-height', 'class', 'clip',
            'clipPathUnits', 'clip-path', 'clip-rule', 'color', 'color-interpolation', 'color-interpolation-filters',
            'color-profile', 'color-rendering', 'contentScriptType', 'contentStyleType', 'cursor', 'cx', 'cy', 'd',
            'decelerate', 'descent', 'diffuseConstant', 'direction', 'display', 'divisor', 'dominant-baseline', 'dur',
            'dx', 'dy', 'edgeMode', 'elevation', 'enable-background', 'end', 'exponent', 'externalResourcesRequired',
            'fill', 'fill-opacity', 'fill-rule', 'filter', 'filterRes', 'filterUnits', 'flood-color', 'flood-opacity',
            'font-family', 'font-size', 'font-size-adjust', 'font-stretch', 'font-style', 'font-variant', 'font-weight',
            'format', 'from', 'fx', 'fy', 'g1', 'g2', 'glyph-name', 'glyph-orientation-horizontal', 'glyph-orientation-vertical',
            'glyphRef', 'gradientTransform', 'gradientUnits', 'hanging', 'height', 'horiz-adv-x', 'horiz-origin-x', 'id',
            'ideographic', 'image-rendering', 'in', 'in2', 'intercept', 'k', 'k1', 'k2', 'k3', 'k4', 'kernelMatrix',
            'kernelUnitLength', 'kerning', 'keyPoints', 'keySplines', 'keyTimes', 'lang', 'lengthAdjust', 'letter-spacing',
            'lighting-color', 'limitingConeAngle', 'local', 'marker-end', 'marker-mid', 'marker-start', 'markerHeight',
            'markerUnits', 'markerWidth', 'mask', 'maskContentUnits', 'maskUnits', 'mathematical', 'max', 'media', 'method',
            'min', 'mode', 'name', 'numOctaves', 'offset', 'opacity', 'operator', 'order', 'orient', 'orientation', 'origin',
            'overflow', 'overline-position', 'overline-thickness', 'panose-1', 'paint-order', 'path', 'pathLength',
            'patternContentUnits', 'patternTransform', 'patternUnits', 'pointer-events', 'points', 'pointsAtX', 'pointsAtY',
            'pointsAtZ', 'preserveAlpha', 'preserveAspectRatio', 'primitiveUnits', 'r', 'radius', 'refX', 'refY', 'rendering-intent',
            'repeatCount', 'repeatDur', 'requiredExtensions', 'requiredFeatures', 'restart', 'result', 'rotate', 'rx',
            'ry', 'scale', 'seed', 'shape-rendering', 'slope', 'spacing', 'specularConstant', 'specularExponent', 'speed',
            'spreadMethod', 'startOffset', 'stdDeviation', 'stemh', 'stemv', 'stitchTiles', 'stop-color', 'stop-opacity',
            'strikethrough-position', 'strikethrough-thickness', 'string', 'stroke', 'stroke-dasharray', 'stroke-dashoffset',
            'stroke-linecap', 'stroke-linejoin', 'stroke-miterlimit', 'stroke-opacity', 'stroke-width', 'style', 'surfaceScale',
            'systemLanguage', 'tableValues', 'target', 'targetX', 'targetY', 'text-anchor', 'text-decoration', 'text-rendering',
            'textLength', 'to', 'transform', 'type', 'u1', 'u2', 'underline-position', 'underline-thickness', 'unicode',
            'unicode-bidi', 'unicode-range', 'units-per-em', 'v-alphabetic', 'v-hanging', 'v-ideographic', 'v-mathematical',
            'values', 'version', 'vert-adv-y', 'vert-origin-x', 'vert-origin-y', 'viewBox', 'viewTarget', 'visibility',
            'width', 'widths', 'word-spacing', 'writing-mode', 'x', 'x-height', 'x1', 'x2', 'xChannelSelector', 'y', 'y1',
            'y2', 'yChannelSelector', 'z', 'zoomAndPan'],

        applyProperties: function() {
            if(window.html_sanitize)
            {
                window.html4.ATTRIBS['*::ui-actions'] = window.html4.atype.NONE;
                window.html4.ATTRIBS['*::ui-tooltip'] = window.html4.atype.NONE;
                window.html4.ATTRIBS['iframe::src'] = window.html4.atype.NONE;
                window.html4.ELEMENTS['style'] = window.html4.atype.NONE;
                window.html4.ELEMENTS['link'] = window.html4.atype.NONE;
                window.html4.ATTRIBS['link::href'] = window.html4.atype.NONE;
                window.html4.ATTRIBS['link::type'] = window.html4.atype.NONE;
                window.html4.ATTRIBS['link::rel'] = window.html4.atype.NONE;
                window.cssSchema['user-select'] = {
                    cssFns: [],
                    cssLitGroup: [['all', 'text', 'element', 'none']],
                    cssPropBits: 0
                };

                var HSE = MobileUI.core.sandbox.HtmlSanitizeElements;

                HSE.__SVG_ELEMENTS.forEach(function(s)
                {
                    window.html4.ELEMENTS[s] = window.html4.atype.NONE;
                });
                HSE.__SVG_ATTRIBUTES.forEach(function(s)
                {
                    window.html4.ATTRIBS['*::' + s] = window.html4.atype.NONE;
                });
            }
        }
    }
});