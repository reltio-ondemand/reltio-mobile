/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.sandbox.WebWorkerSandbox', {
    requires: ['MobileUI.core.sandbox.API','MobileUI.core.Logger'],
    /**@type {Worker} */
    worker: null,
    /**@type MobileUI.core.sandbox.API*/
    api: null,
    apiListenerId: null,
    initApi: function (permissions, config) {
        this.api = Ext.create('MobileUI.core.sandbox.API');
        this.api.initApi(permissions, config);
    },
    getApi: function(){
      return this.api;
    },
    include: function (files) {
        if (Ext.isArray(files) && files.length) {
            var location = MobileUI.core.Services.getLocationObject();
            var servlet = location.origin + location.pathname.substring(0, location.pathname.indexOf(('/' + MobileUI.core.Services.getTenantName())));
            var scriptText = 'importScripts(\'' + servlet + '/worker_api.js\');importScripts(\'' + files.join('\',\'') + '\');postMessage({action:\'loaded\'});';
            try {// Firefox
                var url = window.webkitURL || window.URL;
                this.worker = new Worker(url.createObjectURL(new Blob([scriptText], {type: 'application/javascript'})));
            }
            catch (browserNotSupportWindowUrl) {
                try {// Chrome
                    this.worker = new Worker('data:application/javascript,' + encodeURIComponent(scriptText));
                }
                catch (browserNotSupportWorkers) {
                    MobileUI.core.Logger.error('WebWorker is not support on this platform');
                }
            }
            if (this.worker) {
                var that = this;
                this.api.un('response', this.processResponse, this);
                this.api.on('response', this.processResponse, this);
                this.worker.addEventListener('message', function (e) {

                    if (e.data.action !== 'loaded') {
                        var data = JSON.parse(e.data);
                        that.api.process(data);
                    }
                });
            }
        }
    },
    processResponse: function (e) {
        this.worker.postMessage(e);
    },
    postMessage: function (data) {
        if (this.worker) {
            this.worker.postMessage(data);
        }
    }

});
