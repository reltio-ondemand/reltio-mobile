/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.search.SearchEngine', {
    requires: [
        'Ext.mixin.Observable'
    ],
    mixins: {observable: 'Ext.mixin.Observable'},
    statics: {
        FILTER_TYPES: {
            EQUALS: 'equals',
            NOT_EQUALS: 'not equals',
            EXISTS: 'exists',
            FULL_TEXT: 'fullText',
            HAS_ALL: 'hasAll',
            IN: 'in',
            NOT_IN: 'notIn',
            MISSING_VALUE: 'missing',
            RANGE: 'range',
            //NOT_IN_RANGE: 'not range',
            STARTS_WITH: 'startsWith',
            COUNT_EQUALS: 'count.equals',
            COUNT_NOT_EQUALS: 'count.not.equals',
            COUNT_LT: 'count.less',
            COUNT_LTE: 'count.less.or.equals',
            COUNT_GT: 'count.greater',
            COUNT_GTE: 'count.greater.or.equals',
            COUNT_RANGE: 'count.range'
        },

        /**
         * Escape value to use in a search request
         * @param value {String} text to escape
         * @param encodedForBody {Boolean?} optional. Encode in old style to use query in body of a request
         * @return {String}
         */
        escapeForSearch: function (value) {
            return value.replace(/./g, function (c) {
                switch (c) {
                    case '\\':
                        c = '\\\\';
                        break;
                    case '\'':
                        c = '\\\'';
                        break;
                    case '"':
                        c = '\\"';
                        break;
                }
                return encodeURIComponent(c);
            });
        },

        /**
         * @param params {Array} [{fieldName: fieldName, filter: filter, values: [value, ...]}, ...]
         * fieldName - attr name (attributes.FirstName) or String for specific search facets.
         * filter - String with filter function name (e.g. 'equals', 'startsWith', 'exists'...).
         * value - String, Boolean or Numeric value. Array for filter functions with more than 1 parameter
         */
        buildQuery: function(params) {
            var Filters = MobileUI.core.search.SearchEngine.FILTER_TYPES;
            var countFacets = [Filters.COUNT_EQUALS,
                Filters.COUNT_NOT_EQUALS,
                Filters.COUNT_LT,
                Filters.COUNT_LTE,
                Filters.COUNT_GT,
                Filters.COUNT_GTE,
                Filters.COUNT_RANGE];
            var query = params.map(function(p) {
                var result = null;
                if (p.fieldName && p.filter) {
                    if (p.values && p.values.length > 0) {
                        result = p.values.map(function(value) {
                            var subResult, fieldName, filterType;
                            if (this.filter === 'in') {
                                subResult = MobileUI.core.search.SearchEngine.buildQuery([
                                    {
                                        fieldName: this.fieldName,
                                        filter: 'equals',
                                        values: value
                                    }
                                ]);
                            }
                            else if (this.filter === 'hasAll' || this.filter === 'notIn') {
                                subResult = MobileUI.core.search.SearchEngine.buildQuery(value.map(function(v) {
                                    return {
                                        fieldName: this.fieldName,
                                        filter: (this.filter === 'hasAll') ? 'equals' : 'not equals',
                                        values: [v]
                                    };
                                }, this));
                                if (value.length > 1) {
                                    subResult = '(' + subResult + ')';
                                }
                            }
                            else if(countFacets.indexOf(this.filter) !== -1)
                            {
                                fieldName = this.fieldName+'.count';

                                if(this.filter === Filters.COUNT_EQUALS){
                                    filterType = 'equals';
                                }else if(this.filter === Filters.COUNT_NOT_EQUALS){
                                    filterType = 'not equals';
                                }else if(this.filter === Filters.COUNT_LT){
                                    filterType = 'lt';
                                }else if(this.filter === Filters.COUNT_LTE){
                                    filterType = 'lte';
                                }else if(this.filter === Filters.COUNT_GT){
                                    filterType = 'gt';
                                }else if(this.filter === Filters.COUNT_GTE){
                                    filterType = 'gte';
                                }else if(this.filter === Filters.COUNT_RANGE){
                                    filterType = 'range';
                                }

                                subResult = filterType + '(' + fieldName;
                                value = MobileUI.core.search.SearchEngine.processValue(value);
                                if (value !== '') {
                                    subResult += ',' + value;
                                }
                                subResult += ')';
                            }
                            else {
                                subResult = this.filter + '(' + this.fieldName;
                                value = MobileUI.core.search.SearchEngine.processValue(value);
                                if (value !== '') {
                                    subResult += ',' + value;
                                }

                                subResult += ')';
                            }
                            return subResult;
                        }, p).join(' or ');

                        if (p.values.length > 1) {
                            result = '(' + result + ')';
                        }
                    }
                    else if (p.subQuery)
                    {
                        result = p.subQuery;
                    }
                    else {
                        result = p.filter + '('+ p.fieldName + ')';
                    }
                }
                return result;
            });

            return query.filter(function(q1) {return !!q1;}).join(' and ');
        },
        /**
         * Method converts any js value to search value
         * @param value {*}
         * @returns {*}
         */
        processValue: function (value)
        {
            if (Ext.isString(value))
            {
                return '\'' + MobileUI.core.search.SearchEngine.escapeForSearch(value) + '\'';
            }
            else if (Ext.isArray(value))
            {
                return value.map(function (v)
                {
                    return MobileUI.core.search.SearchEngine.processValue(v);
                }).join(',');
            }
            else if (value === null)
            {
                return 'null';
            }
            else if (value !== undefined)
            {
                return value.toString();
            }
            else
            {
                return '';
            }
        }
    },
    /**@type {Array} @private */
    searchParams: null,
    /**@type {String} @private */
    query: null,
    /**
     * Method sets search parameters
     * E.g.[{fieldName: "attributes", filter: "containsWordStartingWith", values: ['test'], notForState: true}]
     * @param params {Array} parameter
     */
    setSearchParameters: function (params)
    {
        this.searchParams = params;
        this.query = MobileUI.core.search.SearchEngine.buildQuery(this.searchParams);
        this.fireEvent('changeSearchQuery', {query: this.query});
    },
    /**
     * Method returns search parameters
     * @returns {Array}
     */
    getSearchParameters: function ()
    {
        return this.searchParams;
    },
    /**
     * Method return query string
     * @returns {String}
     */
    getQuery: function ()
    {
        return this.query;
    },

    /**
     * Method return global filter url part
     * @returns {String|null}
     */
    getGlobalFilterPart: function ()
    {
        var globalFilter = MobileUI.core.search.SearchParametersManager.getGlobalFilter();
        if (globalFilter && (globalFilter.mute || (globalFilter.uris.length === 0))) {
            return null;
        }

        return globalFilter.uris
            .map(function(uri) { return MobileUI.core.session.Session.getCachedSavedSearch(uri);})
            .filter(function(savedSearch) { return !!savedSearch; }) //<-- all required saved searches should be cached
            .map(function(savedSearch) {
                var query = savedSearch.query || '';
                if (query.indexOf('filter=') === 0) {
                    query = query.substr('filter='.length);
                }
                return '(' + query + ')';
            })
            .join(' or ');
    },

    /**
     *
     * @param facets {Array} facets array
     * @param params {Array?} parameters see buildQuery
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object?} self object
     */
    facetSearch: function (facets, params, success, failure, self)
    {
        var options,
            uri, query, globalFilterPart,
            facetsInternal = facets.map(function (f)
            {
                var facet = {
                    fieldName: f.fieldName
                };
                if (f.pageSize)
                {
                    facet.pageSize = f.pageSize;
                    facet.pageNo = f.pageNo || 1;
                }
                if (f.includeFilter)
                {
                    facet.includeFilter = f.includeFilter;
                }

                facet.orderType = f.orderType || 'reversedCount';

                return facet;
            });

        query = '';
        if (params)
        {
            query = MobileUI.core.search.SearchEngine.buildQuery(params);
            globalFilterPart = this.getGlobalFilterPart();
            if (globalFilterPart)
            {
                query = query ? query + ' and (' + globalFilterPart + ')' : globalFilterPart;
            }
        }
        if (query)
        {
            query = 'filter=' + query;
        }

        options = {method: 'POST', data: facetsInternal};
        uri = MobileUI.core.session.MSearch.URL_SEARCH_FACETS;

        MobileUI.core.session.Session.sendRequest(uri, [query], options, success, failure, self);
    },

    totalSearch: function(dataTenants, params, success, failure, self)
    {
        var currentTenant = MobileUI.core.Services.getTenantName(),
            tenants = [currentTenant], query, context,globalFilterPart,observer;

        if (dataTenants)
        {
            tenants = tenants.concat(dataTenants);
        }

        tenants = Ext.Array.unique(tenants);
        context = {tenants: {}, success: success, failure: failure, self: self};

        observer =  Ext.create('MobileUI.core.Observer',{
            threadNames: tenants,
            listener: function () {
                this.success.call(self, this.tenants);
            },
            self: context
        });

        query = this.query || '';
        if (params)
        {
            query = MobileUI.core.search.SearchEngine.buildQuery(params);
        }
        globalFilterPart = this.getGlobalFilterPart();
        if (globalFilterPart) {
            if (query) {
                query += ' and (' + globalFilterPart + ')';
            }else {
                query = globalFilterPart;
            }
        }

        if(query)
        {
            query = 'filter=' + query;
        }

        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_TOTAL, [query], null, function(json) {
            this.observer.getSelf().tenants[this.tenant] = (json && json.total) ? json.total : 0;
            this.observer.checkThread(this.tenant);
        }, function(
            result){
            if (this.observer.getSelf().failure)
            {
                this.observer.getSelf().failure.call(this.observer.getSelf().self, result);
            }
        },{tenant: currentTenant, observer: observer});
    },


    /**
     * Method searchs enities
     * To use this method you should set parameters using the setSearchParameters method
     * @param max {Number} max result count
     * @param offset {Number} offset
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object?} self object
     */
    entitySearch: function (max, offset, success, failure, self)
    {
        var globalFilterPart = this.getGlobalFilterPart(),
            query = this.query;
        if (globalFilterPart)
        {
            if (query)
            {
                query += ' and (' + globalFilterPart + ')';
            }
            else
            {
                query = globalFilterPart;
            }
        }

        /* TODO: Pattern i18n
        if (!this.searchAttributesCache) {
            var types = MobileUI.core.Metadata.getEntityTypes();
            var template = /\{([^\}]*)\}/gi;
            var attibutesMap = Object.keys(types)
                .map(MobileUI.core.Metadata.getEntityType)
                .reduce(function(attrs, type) {
                    var attributes = ((type.dataLabelPattern || '') + (type.secondaryLabelPattern || '')).match(template);
                    attributes.forEach(function(attribute) {
                        attrs[attribute] = true;
                    });
                    return attrs;
                }, {});

            this.searchAttributesCache = Object.keys(attibutesMap)
                .map(function(attr) {
                    return ',attributes.' + attr.substring(1, attr.length - 1);
                })
                .join('');
        }

        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_PREFIX_PAGING, ['filter=' + query + '&', max, offset, this.searchAttributesCache], {}, success, failure, self);
        */

        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_PREFIX_PAGING, ['filter=' + query + '&', max, offset], null, success, failure, self);
    },

    totalEntitySearch: function(success, failure, self) {
        var globalFilterPart = this.getGlobalFilterPart(),
            query = this.query;
        //TODO we should remove this method and use the totalSearch
        if (globalFilterPart)
        {
            if (query)
            {
                query += ' and (' + globalFilterPart + ')';
            }
            else
            {
                query = globalFilterPart;
            }
        }
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_TOTAL, ['filter=' + query], null, success, failure, self);
    },

    searchAttributesCache: null
});
