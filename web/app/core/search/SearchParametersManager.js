/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.search.SearchParametersManager', {
    singleton: 'true',

    mixins: {
        observable: 'Ext.mixin.Observable'
    },

    /** @private */
    tags: [],
    /** @type {String} @private*/
    keyword: null,
    /**
     * @private
     * @type Array
     */
    advancedParameters: [],

    /** @private */
    savedSearch: {},

    facets: null,

    /** @private */
    globalFilter: null,

    /** @private @type {String}*/
    typeahead: null,

    KEYWORD_KEY: 'keyword',
    TYPEAHEAD_KEY: 'typeahead',
    FACETS_KEY: 'facets',
    ADVANCED_PARAMETERS: 'advancedParameters',
    SAVED_SEARCH: 'savedSearch',
    GLOBAL_FILTER: 'globalFilter',

    getFacets: function()
    {
        return Ext.clone(this.facets);
    },

    setFacets: function(facets)
    {
        this.facets = facets;
        MobileUI.core.util.Storage.setItem(this.FACETS_KEY, this.facets);
    },
    /**
     * Method loads all the search parameters from the storage
     */
    loadFromStorage: function ()
    {
        if (MobileUI.core.util.Storage.getItem('username'))
        {
            this.facets = MobileUI.core.util.Storage.getItem(this.FACETS_KEY) || null;
            this.keyword = MobileUI.core.util.Storage.getItem(this.KEYWORD_KEY);
            this.setTypeAhead(this.keyword);
            this.advancedParameters = MobileUI.core.util.Storage.getItem(this.ADVANCED_PARAMETERS) || [];
            this.savedSearch = MobileUI.core.util.Storage.getItem(this.SAVED_SEARCH) || {};

            if (!Ext.isArray(this.advancedParameters))
            {
                this.advancedParameters = [];
            }
        }
    },

    resetSearch: function() {
        MobileUI.core.search.SearchParametersManager.setAdvancedSearchParameters([]);
        MobileUI.core.search.SearchParametersManager.setSavedSearch(null);
        MobileUI.core.search.SearchParametersManager.setKeyword('');
        MobileUI.core.search.SearchParametersManager.setTypeAhead('');
        MobileUI.core.search.SearchParametersManager.setFacets(null);
    },

    /**
     * Set keyword value
     * @param keyword {String} keyword
     */
    setKeyword: function (keyword)
    {
        MobileUI.core.util.Storage.setItem(this.KEYWORD_KEY, keyword);
        this.keyword = keyword;
    },
    /**
     * return keyword
     * @returns {String|null}
     */
    getKeyword: function ()
    {
        return this.keyword;
    },

    /**
     * Set typeahead value
     * @param typeahead {String} typeahead
     */
    setTypeAhead: function (typeahead)
    {
        MobileUI.core.util.Storage.setItem(this.TYPEAHEAD_KEY, typeahead);
        this.typeahead = typeahead;
    },
    /**
     * return typeahead
     * @returns {String|null}
     */
    getTypeAhead: function ()
    {
        if (this.typeahead === null)
        {
            return this.keyword;
        }
        return this.typeahead;
    },

    /**
     * @param {Array} params
     */
    setAdvancedSearchParameters: function (params)
    {
        this.advancedParameters = params || [];
        MobileUI.core.util.Storage.setItem(this.ADVANCED_PARAMETERS, this.advancedParameters);
    },
    loadState: function(params){
        MobileUI.core.search.SearchParametersManager.setTypeAhead(params.keyword);
        MobileUI.core.search.SearchParametersManager.setKeyword(params.keyword);
        MobileUI.core.search.SearchParametersManager.setFacets(params.facets);
        MobileUI.core.search.SearchParametersManager.setAdvancedSearchParameters(params.params);
    },
    /**
     * @return {Array}
     */
    getAdvancedSearchParameters: function ()
    {
        return this.advancedParameters;
    },

    /**
     * @param keyword {string?}
     * @param advanced {[]?}
     * @param facets {[]?}
     * @return {Array}
     */
    buildSearchParameters: function (keyword, advanced, facets)
    {
        advanced = advanced || this.advancedParameters;
        facets = facets || this.facets;

        var facetsParameters = [];
        if (Object.keys(facets||{}).length > 0)
        {
            for (var key in facets){
                if (facets.hasOwnProperty(key))
                {
                    facetsParameters.push(facets[key]);
                }
            }
        }

        var params = advanced.concat(facetsParameters);
        if (keyword) {
            params.unshift({title: 'Search', labels: [keyword]});
        }
        return params;
    },
    /**
     * Method executes search
     * @param max {Number?} max result count
     * @param offset {Number?} offset
     * @param force {Boolean?} forced search
     */
    executeSearch: function (max, offset, force)
    {

        MobileUI.core.session.Session.performSearch(this.getKeyword(), max, offset, null, function (resp)
        {
            MobileUI.core.Logger.log(resp);
        }, this, force);
    },

    /**
     * @param {Object} savedSearch
     */
    setSavedSearch: function (savedSearch)
    {
        this.savedSearch = savedSearch || {};
        MobileUI.core.util.Storage.setItem(this.SAVED_SEARCH, this.savedSearch);
    },

    /**
     * @return {Object}
     */
    getSavedSearch: function ()
    {
        var facets = this.getFacets() || {},
            search = Ext.clone(this.savedSearch),
            i,tags,type, found;

        if (!search.uiState)
        {
            search.uiState = {};
        }
        if (!search.uiState.state)
        {
            search.uiState.state = {};
        }
        if (!search.uiState.state.model)
        {
            search.uiState.state.model = {};
        }
        if (!search.uiState.state.facets)
        {
            search.uiState.state.facets = [];
        }
        //TODO: refactor it
        tags = facets.tags;
        if (!Ext.isObject(tags))
        {
            if (search.uiState.state.model.tags)
            {
                delete search.uiState.state.model.tags;
            }
            for (i = 0; i < search.uiState.state.facets.length; i++)
            {
                if (search.uiState.state.facets[i].fieldName === 'tags')
                {
                    search.uiState.state.facets.splice(i, 1);
                    break;
                }
            }
        }
        else
        {
            search.uiState.state.model.tags = {
                filter: null,
                labels: facets.tags.labels
            };
            found = false;
            for (i = 0; !found && i < search.uiState.state.facets.length; i++)
            {
                if (search.uiState.state.facets[i].fieldName === 'tags')
                {
                    search.uiState.state.facets[i].values = facets.tags.values;
                    found = true;
                }
            }
            if (!found)
            {
                search.uiState.state.facets.push({
                    additional: false,
                    dataTenant: null,
                    facetClass: 'com.reltio.plugins.search.dev.SearchFacetWithCheckboxes',
                    fieldName: 'tags',
                    hideForDataTenant: false,
                    includeFilter: [],
                    isClosable: false,
                    isCollapsed: false,
                    pageSize: 7,
                    title: 'Tags',
                    typeAhead: true,
                    values: facets.tags.values
                });
            }
        }

        type = facets.type;
        if (!Ext.isObject(type))
        {
            if (search.uiState.state.model.type)
            {
                delete search.uiState.state.model.type;
            }
            for (i = 0; i < search.uiState.state.facets.length; i++)
            {
                if (search.uiState.state.facets[i].fieldName === 'type')
                {
                    search.uiState.state.facets.splice(i, 1);
                    break;
                }
            }
        }
        else
        {
            search.uiState.state.model.type = {
                filter: null,
                labels: facets.type.labels
            };
            found = false;
            for (i = 0; !found && i < search.uiState.state.facets.length; i++)
            {
                if (search.uiState.state.facets[i].fieldName === 'type')
                {
                    search.uiState.state.facets[i].values = facets.type.values;
                    found = true;
                }
            }
            if (!found)
            {
                search.uiState.state.facets.push({
                    additional: false,
                    dataTenant: null,
                    facetClass: 'com.reltio.plugins.search.dev.EntityTypeSearchFacet',
                    fieldName: 'type',
                    hideForDataTenant: false,
                    includeFilter: null,
                    isClosable: false,
                    isCollapsed: false,
                    pageSize: 100,
                    showAllEntityTypes: false,
                    title: 'Entity Types',
                    typeAhead: false,
                    values: facets.type.values
                });
            }
        }

        var keyword = this.getKeyword();
        search.uiState.state.model['$$keyword'] = {labels: [keyword], filter: null};
        search.uiState.state.keyword = keyword;
        search.query = 'filter=' + MobileUI.core.session.Session.getSearchEngine().getQuery();

        if (!search.uiState.state.advanced) {
            search.uiState.state.advanced = {};
        }

        search.uiState.state.advanced.fastSearchParams = this.advancedParameters;
        
        (this.advancedParameters || []).forEach(function (parameter) {
            search.uiState.state.model[parameter.fieldName] = {
                filter: parameter.filter,
                labels: parameter.labels
            };
        });

        return search;
    },

    initGlobalFilter: function(callback, context) {
        MobileUI.core.Services.getUserPreference(MobileUI.core.search.SearchParametersManager.GLOBAL_FILTER,
            MobileUI.core.Services.getTenantName(),
            function (filter) {
                filter = filter || {};
                var uris = filter.uris;
                if (!uris) {
                    uris = filter.uri ? [filter.uri] : [];
                }
                this.globalFilter = { uri: uris[0] || null, uris: uris, mute: filter.mute };
                this.fireEvent('globalFilterChanged', this.globalFilter);
                callback.call(context);
            },
            function(error) {
                MobileUI.core.Logger.log(error);
                this.globalFilter = { uri: null, uris: [], mute: false };
                this.fireEvent('globalFilterChanged', this.globalFilter);
                callback.call(context);
            }, this);
    },

    /**
     * @param {[String]} uris
     * @param {Boolean} mute
     */
    setGlobalFilter: function(uris, mute) {
        uris = uris || [];
        this.globalFilter = {uri: uris[0], uris: uris, mute: mute};

        MobileUI.core.Services.setUserPreference(this.GLOBAL_FILTER, this.globalFilter,
            MobileUI.core.Services.getTenantName(), Ext.emptyFn, function(error) {
                MobileUI.core.Logger.log(error);
            });

        this.fireEvent('globalFilterChanged', this.globalFilter);
    },

    setGlobalFilterMute: function(mute) {
        this.setGlobalFilter(this.globalFilter.uris, mute);
    },

    addGlobalFilterUrl: function(uri) {
        if (this.globalFilter.uris.indexOf(uri) === -1) {
            this.globalFilter.uris.push(uri);
            this.setGlobalFilter(this.globalFilter.uris, this.globalFilter.mute);
        }
    },

    removeGlobalFilterUrl: function(uri) {
        var index = this.globalFilter.uris.indexOf(uri);
        if (index !== -1) {
            this.globalFilter.uris.splice(index, 1);
            this.setGlobalFilter(this.globalFilter.uris, this.globalFilter.mute);
        }
    },

    /**
     * @public
     */
    getGlobalFilter: function() {
        return this.globalFilter;
    }
});
