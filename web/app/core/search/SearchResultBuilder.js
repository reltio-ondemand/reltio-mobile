/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.search.SearchResultBuilder', {
    requires: ['MobileUI.components.list.model.EListModel'],
    statics: {
        DEFAULT_BADGE_COLOR: '#99CCFF',
        CONNECTION_BADGE_COLOR: 'transparent',
        CONNECTION_BADGE_TEXT_COLOR: '#666'
    },
    /**
     * Method creates model item from the api response
     * Response example:
     * {
         *  label: 'Harry Potter',
         *  secondaryLabel: '1 Infinity Loop, Cupertino, CA, USA',
         *  tags: [{value:'HCO',color:dark}],
         *  avatar: 'resources/images/reltio/ava-test.png',
         *  type: 'entityInfo'
         * },
     * @param response {Array} api response
     * @param excludeAvatars {Boolean?} do not add avatar to the item
     * @return {Array}
     */
    buildContent: function (response, excludeAvatars)
    {
        var result = [];
        if (Ext.isArray(response))
        {
            result = response.map(function (item)
            {
                var convertedItem = {},
                    entityType = MobileUI.core.Metadata.getEntityType(item.type);
                var EntityUtils = MobileUI.core.entity.EntityUtils;
                convertedItem.label = MobileUI.core.entity.EntityUtils.processLabel(EntityUtils.buildPatternProperty(item, EntityUtils.LABEL));
                convertedItem.secondaryLabel = EntityUtils.buildPatternProperty(item, EntityUtils.SECONDARY_LABEL) || '';
                convertedItem.userData = {uri:item.uri };
                convertedItem.tags = [{
                    color: entityType.typeColor ? entityType.typeColor : MobileUI.core.search.SearchResultBuilder.DEFAULT_BADGE_COLOR,
                    value: Ext.util.Format.htmlEncode(entityType.label)
                }];
                convertedItem.type = MobileUI.components.list.items.BaseListItem.types.entityInfo;
                if (!excludeAvatars) {
                    convertedItem.avatar = item.defaultProfilePicValue;
                    convertedItem.defaultImage = MobileUI.core.Services.getBorderlessImageUrl(
                        MobileUI.core.session.Session.getAbsoluteImagePath(entityType.typeImage));
                }
                return convertedItem;
            }, this);
        }
        return result;
    },
    /**
     * Method parse response and return array
     * @param response
     * @returns {Array}
     * @param fieldName {String}
     */
    buildFacet: function (response, fieldName)
    {
        var facets, facet, data = [];
        if (response && Ext.isObject(response[fieldName]))
        {
            facets = response[fieldName];
            for (facet in facets)
            {
                if (facets.hasOwnProperty(facet))
                {
                    data.push({
                        type: MobileUI.components.list.items.BaseListItem.types.facetItem,
                        label: facet,
                        score: i18n(facets[facet])
                    });
                }
            }
        }
        return data;
    }
});
