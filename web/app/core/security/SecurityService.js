/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.security.SecurityService', {
    statics: {
        OPERATION: {
            READ: 'READ',
            INITIATE_CHANGE_REQUEST: 'INITIATE_CHANGE_REQUEST',
            UPDATE: 'UPDATE',
            'DELETE': 'DELETE',
            CREATE: 'CREATE'
        }
    },

    suggestedMode: false,
    metadata: {
        canRead: function (type) {
            return this.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.READ, type);
        },
        canUpdate: function (type) {
            return this.checkMetadataPermission([
                MobileUI.core.security.SecurityService.OPERATION.READ,
                MobileUI.core.security.SecurityService.OPERATION.UPDATE], type) && !this.isImageGalleryAttribute(type);
        },
        canDelete: function (type) {
            return this.checkMetadataPermission(MobileUI.core.security.SecurityService.OPERATION.DELETE, type) && !this.isImageGalleryAttribute(type);
        },
        canCreate: function (type) {
            return this.checkMetadataPermission([MobileUI.core.security.SecurityService.OPERATION.CREATE,
                MobileUI.core.security.SecurityService.OPERATION.READ], type) && !this.isImageGalleryAttribute(type);
        },
        canInitiateRequest: function (type) {
            return this.checkMetadataPermission([
                MobileUI.core.security.SecurityService.OPERATION.READ,
                MobileUI.core.security.SecurityService.OPERATION.INITIATE_CHANGE_REQUEST], type) && !this.isImageGalleryAttribute(type);
        },
        canUpdateOrInitiate: function (type) {
            return this.canUpdate(type) || this.canInitiateRequest(type) && !this.isImageGalleryAttribute(type);
        },
        canCreateOrInitiate: function (type) {
            return this.canCreate(type) || this.canInitiateRequest(type) && !this.isImageGalleryAttribute(type);
        },
        canDeleteOrInitiate: function (type) {
            return this.canDelete(type) || this.canInitiateRequest(type);
        },
        getAttributeType: function (type) {
            if (Ext.isString(type)) {
                var md = MobileUI.core.Metadata,
                    /*eslint no-useless-escape: "off"*/
                    objectType = type.match(/^(configuration\/(?:entity|relation)Types\/[^\/]*)/)[1],
                    isEntityType;
                if (objectType) {
                    isEntityType = objectType.indexOf('configuration/entityTypes/') === 0;

                    objectType = isEntityType ? md.getEntityType(objectType) : md.getRelationType(objectType);

                    if (type.indexOf('/attributes/') !== -1) {
                        type = isEntityType ? md.findEntityAttributeByUri(objectType, type)
                            : md.findRelationAttributeByUri(objectType, type);
                    } else {
                        type = objectType;
                    }
                }
            }
            return type;
        },
        isImageGalleryAttribute: function (type) {
            var attrType = this.getAttributeType(type);
            return attrType && attrType.type === 'Image';

        },
        /**
         *  Check specific permission for metadata type.
         *
         * @param permissionName {String|String[]} can be one or several from the OPERATION enum
         * @param type {String|Object} metadata type object or uri
         *
         * @return {Boolean}
         */
        checkMetadataPermission: function (permissionName, type) {
            type = this.getAttributeType(type);

            if (!type || !type.access) {
                return true;
            }
            permissionName = [].concat(permissionName);
            for (var i = 0; i < permissionName.length; i++) {
                if (type.access.indexOf(permissionName[i]) === -1) {
                    return false;
                }
            }
            return true;
        }
    },

    checkUserPermission: function (session, type, subResourceID) {
        var permissions = session.getPermissions().permissions;
        if (permissions) {
            var permParts = subResourceID.split(':');
            var name = permParts[1];
            var key = Object.keys(permissions).filter(function (key) {
                var keyParts = key.split(':');
                if (keyParts[0] === permParts[0]) {
                    var configName = keyParts[1];
                    return configName === '*' || name.indexOf(configName) === 0
                        || name.indexOf(configName.replace(/\.\*$/, '')) === 0;
                }
            })[0];

            var currentPermissions = permissions[key] || {};
            var hasTenants = function (type) {
                var tenants = currentPermissions[type];
                return Array.isArray(tenants) && tenants.some(function (tenant) {
                    return tenant.match('SuperUser:.*') || session.getTenantName() === tenant;
                })
            };

            if (Ext.isString(type)) {
                return hasTenants(type);
            } else if (Array.isArray(type)) {
                return type.every(hasTenants, this);
            }
        }
        return false;
    }

});
