/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MActivity', {

    requires: [],

    statics: {
        URL_ACTIVITIES: '/activities',
        URL_ACTIVITIES_PARTICIPANTS: '/activities/_participatedValues',
        URL_ENTITY_ACTIVITIES: '/{0}/_activities',
        URL_ACTIVITIES_TOTAL: '/activities/_total',
        URL_ENTITY_ACTIVITIES_TOTAL: '/{0}/_activities/_total',
        URL_ACTIVITY: '/{0}',
        URL_ACTIVITY_OPTIONS: '/activities/_options'
    },

    isIncognito: false,

    generateId: function()
    {
        /**
         * @return {string}
         */
        function id() {
            return (((1+Math.random())*65536)|0).toString(16).substring(1);
        }
        return id()+'-'+id()+'-'+id()+id();
    },

    /**
     * Specify the description at the Activity Log
     *
     * @param activityId {String} Activity ID
     * @param label {String} label
     * @param description {String?} description
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    describeActivity: function(activityId, label, description, success, failure, self) {
        if (!activityId) {
            activityId = this.generateId();
        }
        if (this.isIncognito) {
            if (success) {
                success.call(self);
            }
            return;
        }

        this.sendRequest(MobileUI.core.session.MActivity.URL_ACTIVITIES, [], {
            method: 'POST',
            data: {
                label: label || '',
                description: description || ''
            },
            headers: {
                ActivityID: activityId
            }
        }, success, failure, self);
    },

    /**
     * Request an Activity Log Record by ID from tenant
     *
     * @param activityId {String} Activity ID
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param itemsCursor {String?} itemsCursor
     * @param self {Object?} listener's context
     */
    requestActivity: function(activityId, itemsCursor, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ACTIVITY;
        if (itemsCursor) {
            url += '?itemsCursor=' + itemsCursor;
        }
        this.sendRequest(url, [activityId], null, success, failure, self);
    },

    /**
     * This operation performs a search for Activity Log Records - by type of activity (login, logout, search, view, etc) or by action (creates, edit, merge, delete, set a source update time etc).
     *
     * @param params {Array|String?} [{fieldName: fieldName, filter: filter, values: [value, ...]}, ...]
     * @param offset {Number?} Positive Integer value to identify starting what element in a result set should be returned in a response. Can be used to organize pagination in combination with 'max' parameter.
     * @param max {Number?} Positive Integer value to identify maximum number of entities to return in a response. Can be used to organize pagination in combination with 'offset' parameter.
     * @param sort {String?} Activity objects property that should be used for sorting. Can be used in combination with 'order' parameter to have reverse order.
     * @param order {String?} Order of sorting. Can be used in combination with 'sort' parameter to have reverse order. Possible values:
     *                            asc - the results will be shown in ascending order
     *                            desc - the results will be shown in descending order
     *                            Default sorting in by entity asc.
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    searchActivities: function(params, offset, max, sort, order, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ACTIVITIES + this.buildQuery(params, offset, max, sort, order);
        this.sendRequest(url, null, null, success, failure, self);
    },

    /**
     * This operation performs a search for Activity Log Records for certain entity - by type of activity (login, logout, search, view, etc) or by action (creates, edit, merge, delete, set a source update time etc).
     *
     * @param entityUri {String} - entity uri
     * @param params {Array|String?} [{fieldName: fieldName, filter: filter, values: [value, ...]}, ...]
     * @param offset {Number?} Positive Integer value to identify starting what element in a result set should be returned in a response. Can be used to organize pagination in combination with 'max' parameter.
     * @param max {Number?} Positive Integer value to identify maximum number of entities to return in a response. Can be used to organize pagination in combination with 'offset' parameter.
     * @param sort {String?} Activity objects property that should be used for sorting. Can be used in combination with 'order' parameter to have reverse order.
     * @param order {String?} Order of sorting. Can be used in combination with 'sort' parameter to have reverse order. Possible values:
     *                            asc - the results will be shown in ascending order
     *                            desc - the results will be shown in descending order
     *                            Default sorting in by entity asc.
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    searchEntityActivities: function(entityUri, params, offset, max, sort, order, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ENTITY_ACTIVITIES + this.buildQuery(params, offset, max, sort, order);
        this.sendRequest(url, [entityUri], null, success, failure, self, true);
    },

    /**
     * This operation performs a search for Activity Log Records - by type of activity (login, logout, search, view, etc) or by action (creates, edit, merge, delete, set a source update time etc).
     *
     * @param params {Array|String?} [{fieldName: fieldName, filter: filter, values: [value, ...]}, ...]
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    totalActivities: function(params, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ACTIVITIES_TOTAL + this.buildQuery(params);
        this.sendRequest(url, [], null, success, failure, self);
    },

    /**
     * This operation returns total activities for certain entity - by type of activity (login, logout, search, view, etc) or by action (creates, edit, merge, delete, set a source update time etc).
     *
     * @param entityUri {String} - entity uri
     * @param params {Array|String?} [{fieldName: fieldName, filter: filter, values: [value, ...]}, ...]
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    totalEntityActivities: function(entityUri, params, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ENTITY_ACTIVITIES_TOTAL + this.buildQuery(params);
        this.sendRequest(url, [entityUri], null, success, failure, self);
    },

    /**
     * This operation performs a search for Activity Log participants.
     *
     * @param params {Array|String?} same as for searchActivities
     * @param participantField {String} the field for which to retrieve participants
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    searchActivitiesParticipants: function(params, participantField, success, failure, self) {
        var url = MobileUI.core.session.MActivity.URL_ACTIVITIES_PARTICIPANTS + this.buildQuery(params, null, null, null, null, participantField);
        this.sendRequest(url, [], null, success, failure, self);
    },

    /**
     * Get activities options - it is used to get configuration for log activity functionality
     *
     * @param success {Function?} listener
     * @param failure {Function?} listener
     * @param self {Object?} listener's context
     */
    getActivitiesOptions: function(success, failure, self) {
        this.sendRequest(MobileUI.core.session.MActivity.URL_ACTIVITY_OPTIONS, [], {method: 'POST'},
            success, failure, self);
    },

    buildQuery: function(params, offset, max, sort, order, participantField) {
        var p = [];
        var filter = null;
        if (params) {
            filter = Ext.isString(params) ? params : '(' + MobileUI.core.search.SearchEngine.buildQuery(params) + ')';
        }
        if (filter) {
            if (filter.indexOf('filter=') !== 0) {
                filter = 'filter=' + filter;
            }
            p.push(filter);
        }
        if (offset !== null && offset !== undefined) {
            p.push('offset=' + offset);
        }
        if (max !== null && max !== undefined) {
            p.push('max=' + max);
        }
        if (sort !== null && sort !== undefined) {
            p.push('sort=' + sort);
        }
        if (order !== null && order !== undefined) {
            p.push('order=' + order);
        }
        if (participantField !== null && participantField !== undefined) {
            p.push('fieldName=' + participantField);
        }

        if (p.length > 0) {
            return '?' + p.join('&');
        }

        return '';
    }
});
