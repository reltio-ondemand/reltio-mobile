/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MDataProvider', {
    /**@type{Object} @private}*/
    requests: {},
    /**@type{String} @private}*/
    globalId: '',
    /**@type{String} @private}*/
    deviceName: '',
    /**
     * Method sets the active view id
     * @param viewId {String} view id
     */
    setActiveView: function (viewId)
    {
        this.globalId = viewId + MobileUI.core.util.Util.generateUUID();
    },
    /**
     * Method sets the device name
     * @param deviceName {String}
     */
    setDevice: function (deviceName)
    {
        this.deviceName = deviceName;
    },
    /**
     * Method sends request to API
     * @param url {String} url string
     * @param params {Array?} request url parameters
     * @param options {Object?} request options
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    sendRequest: function (url, params, options, success, failure, self)
    {
        this.callMethodWhenTokenAvailable(this.sendRequestInternal, function (resp)
        {
            MobileUI.core.Logger.log(resp);
        }, [url, params, options, success, failure, self], this);
    },
    /**
     * Method sends request to API
     * @param url {String} url string
     * @param params {Array?} request url parameters
     * @param options {Object?} request options
     * @return {Promise}
     */
    sendRequestAndReturnPromise: function (url, params, options) {
        var me = this;
        return new Promise(function (resolve, reject) {
            me.callMethodWhenTokenAvailable(me.sendRequestInternal, function (resp) {
                MobileUI.core.Logger.log(resp);
            }, [url, params, options, function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            }, me], me);
        });
    },

    /*@private*/
    sendRequestInternal: function (url, params, options, success, failure, self)
    {
        var args = {
                url: url,
                params: params,
                options: (options === null || options === undefined) ? options : Ext.clone(options),
                success: success,
                failure: failure,
                self: self
            },
            arrOpts = [],
            method = null,
            data = null,
            headers = {},
            tenant = MobileUI.core.Services.getTenantName(),
            needToSendData, key, id, methodId,
            isLoggedIn, scope,
            failureFn = function (response)
            {
                var arr = this.that.requests[this.methodId] || [],
                    i, json;
                delete this.that.requests[this.methodId];
                if (this.that.requests['old_' + this.methodId])
                {
                    arr = arr.concat(this.that.requests['old_' + this.methodId]);
                    delete this.that.requests['old_' + this.methodId];
                }
                if (response.status === 401)
                {
                    this.that.setExpired(null);
                    if (this.that.isLoggedIn() || this.that.isRefreshingToken())
                    {
                        this.that.requests['old_' + this.methodId] = arr;
                        this.that.sendRequest(this.args.url, this.args.params, this.args.options, this.args.success, this.args.failure, this.args.self);
                    }
                } else if (MobileUI.core.util.Util.isThrottlingError(response)) {
                    this.that.requests['old_' + this.methodId] = arr;
                    setTimeout(function () {
                        this.that.sendRequest(this.args.url, this.args.params, this.args.options, this.args.success, this.args.failure, this.args.self);
                    }.bind(this), 1000);
                }
                else
                {
                    try
                    {
                        json = Ext.decode(response.responseText);
                    }
                    catch (ex)
                    {
                        MobileUI.core.Logger.log(ex.message);
                    }
                    for (i = 0; i < arr.length; i++)
                    {
                        if (arr[i].failure)
                        {
                            arr[i].failure.call(arr[i].self || this.that, json);
                        }
                    }
                    if (this.args.failure)
                    {
                        this.args.failure.call(this.args.self || this.that, json);
                    }
                }
            },
            successFn = function (response)
            {
                var arr = this.that.requests[this.methodId] || [],
                    json = {}, i;

                delete this.that.requests[this.methodId];
                if (this.that.requests['old_' + this.methodId])
                {
                    arr = arr.concat(this.that.requests['old_' + this.methodId]);
                    delete this.that.requests['old_' + this.methodId];
                }
                try
                {
                    json = Ext.decode(response.responseText);
                }
                catch (ex)
                {
                    MobileUI.core.Logger.log(ex.message);
                }

                if (this.args.success)
                {
                    this.args.success.call(this.args.self || this.that, json);
                }

                if (Ext.isArray(arr))
                {
                    for (i = 0; i < arr.length; i++)
                    {
                        if (arr[i].success)
                        {
                            arr[i].success.call(arr[i].self || this.that, json);
                        }
                    }
                }

            };

        if (options !== null && options !== undefined)
        {
            for (key in options)
            {
                if (options.hasOwnProperty(key))
                {
                    if (key === 'method')
                    {
                        method = options[key];
                    }
                    else if (key === 'data')
                    {
                        data = options[key];
                    }
                    else if (key === 'headers')
                    {
                        headers = options[key];
                    }
                    else if (key === 'tenant')
                    {
                        tenant = options[key];
                    }
                    else
                    {
                        arrOpts.push(key + '=' + options[key]);
                    }
                }
            }
            delete options.method;
            delete options.data;
            delete options.headers;
        }

        if (data === null)
        {
            data = options;
        }
        needToSendData = method === 'POST' || method === 'PUT';
        url = this.prepareUrl(url, params, tenant, needToSendData, arrOpts, headers);
        method = method || 'GET';

        id = Ext.isObject(headers) ? MobileUI.core.util.Util.getHash(Ext.Object.getValues(headers).join(' ')) : '';
        methodId = method + url + id;

        if (needToSendData)
        {
            methodId += Math.random();
        }

        if (this.requests[methodId])
        {
            this.requests[methodId].push(args);
            return;
        }

        this.requests[methodId] = [];
        isLoggedIn = this.isLoggedIn();

        scope = {
            that: this,
            args: args,
            methodId: methodId,
            needToSendData: needToSendData
        };
        if (url.indexOf(MobileUI.core.Services.getApiPath()) !== -1)
        {
            headers['Source-System'] = 'Reltio';
        }
        if (isLoggedIn && (!headers['Authorization']))
        {
            headers['Authorization'] = 'Bearer ' + this.getAccessToken();
        }

        var forcedLocale = MobileUI.core.I18n.getForcedLocale();
        if(forcedLocale && !headers['Accept-Language']) {
            headers['Accept-Language'] = forcedLocale;
        }

        //if (MobileUI.core.Services.getGeneralSettings().apiPath.indexOf(window.location.host) !== -1)
        //{
        //    headers['clientSystemId'] = 'MobileUI' + this.deviceName;
        //    headers['globalId'] = this.globalId;
        //}
        if (needToSendData)
        {
            if (!headers['Content-Type'])
            {
                headers['Content-Type'] = 'application/json';
            }
        }
        if (method === 'GET' && (Ext.isEmpty(data) || (Ext.isObject(data) && Object.keys(data).length === 0))) {
            data = null;
        }
        data = data && (Ext.isString(data) ? data : Ext.encode(data));

        Ext.Ajax.request({
            url: url,
            method: method,
            headers: headers,
            params: data,
            scope: scope,
            success: successFn,
            failure: failureFn
        });
    },

    /**
     * Return the url
     * @private
     * @param url {String} initial url
     * @param params {Array|String|Number|Boolean?} params
     * @param tenant {String} tenant name
     * @param needToSendData {Boolean} if false all arrOpts will be added to url
     * @param arrOpts {Array} options array
     * @param headers {Object?} headers
     * @returns {String}
     */
    prepareUrl: function (url, params, tenant, needToSendData, arrOpts, headers)
    {
        var i;
        if (params !== undefined && params !== null)
        {
            if (!Ext.isArray(params))
            {
                params = [params];
            }

            for (i = 0; i < params.length; i++)
            {
                url = url.replace(new RegExp('\\{' + i + '\\}', 'g'), params[i]);
            }
        }

        if (url.indexOf('http:') !== 0 && url.indexOf('https:') !== 0)
        {
            url = '/' + (tenant || 'tenant') + url;
            url = MobileUI.core.Services.getApiPath() + '/api' + url;
            if (Ext.isObject(headers))
            {
                headers['xxx-client'] = 'true';
                headers['clientSystemId'] = 'Reltio Mobile UI';
                headers['globalId'] = 'Reltio Mobile UI';
            }
        }

        if (!needToSendData && arrOpts.length > 0)
        {
            url += (url.indexOf('?') === -1 ? '?' : '&') + arrOpts.join('&');
        }
        return url;
    },
    /**
     * Method returns the absolute image path
     * @param path {String} path to image
     * @returns {String}
     */
    getAbsoluteImagePath: function (path)
    {
        if (!path)
        {
            return '';
        }
        var result,
            settings = MobileUI.core.Services.getGeneralSettings();
        if (path.indexOf('http:') === 0 || path.indexOf('https:') === 0)
        {
            result = path;
        }
        else
        { //noinspection JSUnresolvedVariable
            result = (settings.imagePath || settings.apiPath) + '/' + path.replace(/^\/+/, '');
        }
        return result;
    }
});
