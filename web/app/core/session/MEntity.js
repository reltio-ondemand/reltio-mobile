/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MEntity', {
    statics: {
        URL_ENTITY: '/{0}',
        URL_ENTITY_OPTIONS: '/{0}?options=sendHidden,addRefAttrUriToCrosswalk',
        URL_ENTITIES: '/entities',
        URL_ENTITIES_DCR: '/entities?changeRequestId={0}',
        URL_ENTITIES_UPDATE: '/{0}?options=updateAttributeUpdateDates,preserveURIs,preserveHidden',
        URL_ENTITIES_UPDATE_DCR: '/{0}?options=updateAttributeUpdateDates,preserveURIs,preserveHidden&changeRequestId={1}',
        URL_SLICED_ENTITY: '/{0}/_slice?options=showDataProvider,sendHidden',
        URL_UPDATE_ATTRIBUTE_X: '/{0}',
        URL_INSERT_ATTRIBUTE: '/{0}',
        URL_DELETE_ATTRIBUTE_X: '/{0}',
        MARK_ENTITY: '/{0}/_markFavorite',
        UNMARK_ENTITY: '/{0}/_unmarkFavorite',
        URL_INSERT_DEFAULT_BY_ATTR_URL: '/{0}/_setDefaultProfilePicByAttributeValueURI',
        URL_DCR: '/changeRequests',
        /**
         * Method returns the uri from response
         * @param json
         * @returns {*}
         */
        parseUriFromServerResult: function (json)
        {
            var ret = null,
                result;
            if (json)
            {
                if (!Ext.isArray(json))
                {
                    ret = json.uri || null;
                }
                else if (json.length === 1)
                {
                    result = json[0];
                    if (!result.successful)
                    {
                        return result;
                    }
                    else
                    {
                        MobileUI.core.session.MEntity.fixRefEntity(result.object);
                        ret = result.object.uri;
                    }
                }
            }
            return ret;
        },
        /**
         * Fix the ref object
         * @param refObject
         */
        fixRefEntity: function (refObject)
        {
            if (refObject.refEntity && !Ext.isArray(refObject.refEntity))
            {
                refObject.refEntity = [refObject.refEntity];
            }
            if (refObject.refRelation && !Ext.isArray(refObject.refRelation))
            {
                refObject.refRelation = [refObject.refRelation];
            }
        }
    },
    currentEntity: null,
    modifiedEntity: null,
    editMode: null,

    setEditSubMode: function(editMode){
        this.editMode = editMode;
    },

    getEditSubMode: function(){
        return this.editMode;
    },

    isEntityError: function(json){
        return !json || json.error || json.errorMessage || Ext.isEmpty(json);
    },

    initEntity: function(entityUri, forceSwitch, includeHidden)
    {
        var oldEntityUri = this.entityUri;
        this.entityUri = entityUri;
        this.entity = null;
        var cancelObj = {cancel: false};
        this.fireEvent('beforeRequestEntity', cancelObj);
        if(cancelObj.cancel)
        {
            return;
        }

        var entityReceivied = function(json) {
            var me = this;
            if(this.isEntityError(json))
            {
                var errMsg = json ? (json.errorMessage || json.error) : null;
                var errCode = json ? (json.errorCode) : -1;
                MobileUI.core.Logger.error(errMsg || 'Could not request entity');
                this.fireDataEvent('entityRequestFailed', {uri: me.entityUri, message: errMsg, code: errCode});
                return;
            }

            me.entity = json;

            if (me.entityUri !== me.entity.uri)
            {
                me.entityUri = me.entity.uri;
            }

            me.fireEvent('beforeUpdateEntity', {forceSwitch: forceSwitch, oldEntityUri: oldEntityUri});
            var task = Ext.create('Ext.util.DelayedTask', function() {
                this.fireEvent('updateEntity');
            }, me);
            task.delay(150);
        };

        this.requestEntity(this.entityUri,  entityReceivied, function(){
            this.fireDataEvent('entityRequestFailed', {uri: this.entityUri});
        }, this, includeHidden);
    },

    /**
     * Method returns current entity
     * @returns {Object}
     */
    getEntity: function ()
    {
        return this.currentEntity;
    },

    /**
     * Method returns modified entity
     * @returns {Object}
     */
    getModifiedEntity: function ()
    {
        return this.modifiedEntity;
    },

    /**
     * Method sets current entity
     */
    setEntity: function (entity)
    {
        this.currentEntity = entity;
    },

    /**
     * Method returns entity uri
     * @returns {String|null}
     */
    getEntityUri: function ()
    {
        return this.currentEntity !== null ? this.currentEntity.uri : null;
    },

    resetCurrentEntity: function()
    {
        this.currentEntity = null;
    },

    /**
     * Method allows to get entity
     * @param entityUri {String} entity uri
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @param dontOverrideCurrentEntity {Boolean?} should we override current entity or not
     * @param includeHidden {Boolean?} should the response contains the hidden attributes
     */
    requestEntity: function (entityUri, success, failure, self, dontOverrideCurrentEntity, includeHidden)
    {
        var requestUri = includeHidden ? MobileUI.core.session.MEntity.URL_ENTITY_OPTIONS : MobileUI.core.session.MEntity.URL_ENTITY;
        MobileUI.core.session.Session.sendRequest(requestUri, entityUri, null, function (resp)
        {
            var that = this;
            if (resp && !dontOverrideCurrentEntity)
            {
                that.that.currentEntity = resp;
                that.that.modifiedEntity = Ext.clone(that.that.currentEntity);
            }
            if (that.success !== undefined && that.success !== null)
            {
                that.success.call(that.self, resp);
            }
        }, function (resp)
        {
            var that = this;
            if (that.failure !== undefined && that.failure !== null)
            {
                that.failure.call(that.self, resp);
            }
        }, {that: this, self: self, success: success, failure: failure});
    },

    requestSlicedEntity: function (entityUri, success, failure, self) {
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.URL_SLICED_ENTITY, entityUri, null, function (resp)
        {
            var that = this;
            if (that.success !== undefined && that.success !== null)
            {
                that.success.call(that.self, resp);
            }
        }, function (resp)
        {
            var that = this;
            if (that.failure !== undefined && that.failure !== null)
            {
                that.failure.call(that.self, resp);
            }
        }, {that: this, self: self, success: success, failure: failure});
    },

    overrideEntity: function (entity, success, failure, self, changeRequestId) {
        var type = 'entities';
        var slashIndex = (entity.uri || '').indexOf('/');
        if (slashIndex !== -1) {
            type = entity.uri.substr(0, slashIndex);
        }

        MobileUI.core.session.Session.sendRequest(
            changeRequestId ? MobileUI.core.session.MEntity.URL_ENTITIES_UPDATE_DCR : MobileUI.core.session.MEntity.URL_ENTITIES_UPDATE,
            changeRequestId ? [type, changeRequestId] : [type], {
            method: 'POST',
            data: [entity]
        }, success || Ext.emptyFn, failure || Ext.emptyFn, self);
    },

    findEntity: function(entityUri, callback, context) {
        var entity = MobileUI.core.entity.TemporaryEntity.getEntity(entityUri);
        if (entity) {
            callback.call(context, entity);
        }
        else {
            MobileUI.core.session.Session.requestEntity(entityUri, callback, function() {
                callback.call(this);
            }, context, true);
        }
    },
    /**
     * Method creates new entity
     * @param entity {Object} entity object
     * @param changeRequestId {string} ID of data change request to which new change item should be added
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @returns {Promise|null}
     */
    createEntity: function (entity, changeRequestId, success, failure, self) {
        function doRequest(success, failure) {
            MobileUI.core.session.Session.sendRequest(
                changeRequestId ? MobileUI.core.session.MEntity.URL_ENTITIES_DCR : MobileUI.core.session.MEntity.URL_ENTITIES,
                changeRequestId ? [changeRequestId] : [], {
                    method: 'POST',
                    data: entity && [entity]
                }, success || Ext.emptyFn, failure || success || Ext.emptyFn, self);
        }

        if (success === undefined && failure === undefined) {
            return new Promise(function (resolve, reject) {
                doRequest(resolve, reject);
            });
        } else {
            doRequest(success, failure);
        }
    },

    /**
     * Method saves entity changes
     * @param uri {String} entity uri
     * @param entity {Object} entity object
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    saveEntity: function(uri, entity, success, failure, self) {
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.URL_ENTITY, [uri], {
            method: 'PUT',
            data: entity
        }, success || Ext.emptyFn, failure || Ext.emptyFn, self);
    },

    updateAttribute: function(entityUri, attrUri, value, success, failure, self) {
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.URL_UPDATE_ATTRIBUTE_X, [attrUri], {
            method: 'PUT',
            value: value
        }, success || Ext.emptyFn, failure || Ext.emptyFn, self);
    },

    insertAttribute: function(attrUri, value, success, failure, self) {
        if (attrUri.indexOf('relations/') !== -1) { //TODO: ?
            MobileUI.core.session.Session.insertRelationAttribute(attrUri, value, success, failure, self);
        }
        else {
            var objToSend = [{value: value}];
            objToSend.method = 'POST';

            MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.URL_INSERT_ATTRIBUTE,
                [attrUri], objToSend,
                success || Ext.emptyFn, failure || Ext.emptyFn, self);
        }
    },

    deleteAttribute: function(attrUri, success, failure, self) {
        this.sendRequest(MobileUI.core.session.MEntity.URL_DELETE_ATTRIBUTE_X, [attrUri], {
            method: 'DELETE'
        }, success || Ext.emptyFn, failure || Ext.emptyFn, self);
    },

    /**
     * Method marks entity
     * @param entityUri {String} entity object
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    markEntity: function (entityUri, success, failure, self)
    {
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.MARK_ENTITY, entityUri, {
            method: 'PUT'
        }, function (resp)
        {
            var that = this;
            that.that.currentEntity = resp;
            if (Ext.isFunction(that.success))
            {
                that.success.call(that.self, resp);
            }
        }, function (resp)
        {
            var that = this;
            if (Ext.isFunction(that.failure))
            {
                that.failure.call(that.self, resp);
            }
        }, {that: this, self: self, success: success, failure: failure});
    },
    /**
     * Method unmarks entity
     * @param entityUri {String} entity object
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    unmarkEntity: function (entityUri, success, failure, self)
    {
        MobileUI.core.session.Session.sendRequest(MobileUI.core.session.MEntity.UNMARK_ENTITY, entityUri, {
            method: 'PUT'
        }, function (resp)
        {
            var that = this;
            that.that.currentEntity = resp;
            if (Ext.isFunction(that.success))
            {
                that.success.call(that.self, resp);
            }
        }, function (resp)
        {
            var that = this;
            if (Ext.isFunction(that.failure))
            {
                that.failure.call(that.self, resp);
            }
        }, {that: this, self: self, success: success, failure: failure});
    },

    /**
     * Method inserts default image by attribute url
     * @param entityUri {String} entity uri
     * @param attrUrl {String} attribute url
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    insertDefaultImageByAttributeURL: function (entityUri, attrUrl, success, failure, self) {
        this.sendRequest(MobileUI.core.session.MEntity.URL_INSERT_DEFAULT_BY_ATTR_URL, [entityUri], {
            method: 'PUT',
            data: attrUrl
        }, success,failure, self);
    },

    createDCR: function (success, failure, self) {
        this.sendRequest(MobileUI.core.session.MEntity.URL_DCR, [], {
            method: 'POST'
        }, success,failure, self);
    }
});
