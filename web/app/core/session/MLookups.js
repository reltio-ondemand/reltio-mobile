/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MLookups', {
    statics: {
        URL_LIST_LOOKUPS: '/lookups/list',
        URL_ARRAY_LOOKUPS: '/lookups/resolveList',

        URL_VALIDATE_LOOKUPS: '/lookups/validate'
    },
    resolveLookupValue: function (type, parents, filter, max, success, failure, self) {

        this.sendRequest(MobileUI.core.session.MLookups.URL_LIST_LOOKUPS, null, {
            method: 'POST',
            data: {
                parents: parents,
                type: type,
                max: max || 100,
                displayNamePrefix: filter || ''
            }
        }, success, failure, self);
    },

    resolveLookupsValue: function (array) {
        return this.sendRequestAndReturnPromise(MobileUI.core.session.MLookups.URL_ARRAY_LOOKUPS, null,
            {
                method: 'POST',
                data: array
            });
    },
    validateLookups: function (parents, codeValues, success, failure, self) {

        this.sendRequest(MobileUI.core.session.MLookups.URL_VALIDATE_LOOKUPS, null, {
            method: 'POST',
            data: {parents: parents, codeValues: codeValues}
        }, success, failure, self);
    }
});
