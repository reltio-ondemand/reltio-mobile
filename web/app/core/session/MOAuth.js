/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//noinspection JSUnusedGlobalSymbols
Ext.define('MobileUI.core.session.MOAuth', {
    statics: {
        URL_OAUTH_PROXY: '/oauth/proxy',
        URL_USER_PROFILE: '/oauth/users/{0}',
        URL_OAUTH_CHECK_TOKEN: '/oauth/checkToken?serviceId=MDM,export,Auth&tenantId='
    },
    /**@type {String} @private */
    accessToken: null,
    /**@type {String} @private */
    refreshToken: null,
    /**@type {Array} @private **/
    requestsWaitsForToken: null,
    /**@type {Boolean} @private **/
    refreshingToken: null,
    /**@type {Number} @private **/
    expired: 0,
    /**@type {Object} @private **/
    user: null,
    /**@type {String} @private **/
    userProfileEntityId: null,
    /**@type {Array} @private **/
    roles: null,
    /**@type {String} @private **/
    customer: null,
    /**@type {String} @private **/
    externalTokens: null,

    /**
     * Method allows to set the access token
     * @param accessToken {String} access token
     */
    setAccessToken: function (accessToken)
    {
        this.accessToken = accessToken;
    },
    /**
     * Method returns the saved access token or null
     * @returns {String|null}
     */
    getAccessToken: function ()
    {
        return this.accessToken;
    },

    /**
     * Method returns user object
     * @returns {Object}
     */
    getUser: function ()
    {
        return this.user;
    },

    /**
     * Method returns user roles
     * @returns {Array}
     */
    getRoles: function ()
    {
        return this.roles;
    },

    /**
     * Method returns uesr customer name
     * @returns {String}
     */
    getCustomerName: function ()
    {
        return this.customer;
    },

    /**
     * Return is user logged in or not
     * @returns {boolean}
     */
    isLoggedIn: function ()
    {
        return this.accessToken !== null;
    },
    /**
     * Method set the expired value
     * @param expired {Number} expired value
     */
    setExpired: function (expired)
    {
        this.expired = new Date().getTime() + expired * 1000;
        if (Ext.isNumber(this.expired))
        {
            MobileUI.core.util.Storage.setItem('expired', this.expired);
        }
    },
    /**
     * Return whether access token expired or not
     * @returns {Boolean}
     */
    isExpired: function ()
    {
        return this.expired - new Date().getTime() <= 60000;
    },

    /**
     * Method allows to set the refresh token
     * @param refreshToken {String} refresh token
     */
    setRefreshToken: function (refreshToken)
    {
        this.refreshToken = refreshToken;
    },
    /**
     * Return the saved refresh token or null
     * @returns {String|null}
     */
    getRefreshToken: function ()
    {
        return this.refreshToken;
    },
    /**
     * Method allows to auth user with credentials
     * @param username {String} user name
     * @param password {String} password
     * @param success  {function} success callback
     * @param fail {function} fail callback
     * @param self {Object} self object
     */
    login: function (username, password, success, fail, self)
    {
        var url = MobileUI.core.Services.getGeneralSettings().ui,
            that = this;
        url += 'services/oauth/token';

        Ext.Ajax.request({
            url: url,
            success: function (xhr)
            {
                var json = Ext.decode(xhr.responseText);
                that.user = {full_name: username};
                that.setAccessToken(json.access_token);
                that.setRefreshToken(json.refresh_token);
                that.setExpired(Number(json.expires_in));
                that.updateUserInStore(json, username);
                that.requestUserInfo();
                if (success)
                {
                    success.call(self, json);
                }
            },
            failure: function (xhr)
            {
                if (fail)
                {
                    fail.call(self, Ext.decode(xhr.responseText));
                }
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            params: {
                grant_type: 'password',
                username: username,
                password: password
            }
        });
    },

    /**
     * Method initialize session.
     * @param success {Function?} on success callback
     * @param failure {Function?} on failure callback
     * @param self {Object?} self object
     */
    initSession: function (success, failure, self)
    {
        var Services = MobileUI.core.Services,
            tenant = Services.getTenantName();
        Services.checkTenant(tenant, function ()
        {
            Services.getConfigurationManager().initConfiguration(tenant, function (response)
            {
                success.call(self, response);
            }, failure, self);
        }, failure, self);
    },

    /**
     * Method allows to update
     * @protected
     * @param userInfo {object} user info
     * @param username {String} user name
     */
    updateUserInStore: function (userInfo, username)
    {
        MobileUI.core.util.Storage.removeItem('username');
        MobileUI.core.util.Storage.removeItem('refresh_token');
        MobileUI.core.util.Storage.removeItem('access_token');
        MobileUI.core.util.Storage.removeItem('expired');
        MobileUI.core.util.Storage.removeItem('userdata');
        this.setUserName(username);
        MobileUI.core.util.Storage.setItem('refresh_token', userInfo.refresh_token);
        MobileUI.core.util.Storage.setItem('access_token', userInfo.access_token);

        if (userInfo.expires_in)
        {
            MobileUI.core.util.Storage.setItem('expired', this.expired);
        }
    },
    /**
     * Method allows to refresh access token
     * @param refreshToken {String} refresh tokn
     * @param success  {function} success callback
     * @param fail {function} fail callback
     * @param self {Object} self object
     */
    sendRefreshAccessTokenRequest: function (refreshToken, success, fail, self)
    {
        var url = MobileUI.core.Services.getGeneralSettings().ui;

        url += 'services/oauth/token';

        Ext.Ajax.request({
            url: url,
            success: function (xhr)
            {
                if (success)
                {
                    success.call(self, Ext.decode(xhr.responseText));
                }
            },
            failure: function (xhr)
            {
                if (fail)
                {
                    fail.call(self, Ext.decode(xhr.responseText));
                }
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            params: {
                grant_type: 'refresh_token',
                refresh_token: refreshToken
            }
        });
    },
    /**
     * Method check the captcha and send the link
     * @param username {String} user name
     * @param captcha {String} user entered captcha value
     * @param success  {function} success callback
     * @param fail {function} fail callback
     * @param self {Object} self object
     */
    resetPassword: function (username, captcha, success, fail, self)
    {
        var url = MobileUI.core.Services.getGeneralSettings().ui;
        url += 'ui/forgot';
        Ext.Ajax.request({
            url: url,
            success: function (xhr)
            {
                if (success)
                {
                    success.call(self, Ext.decode(xhr.responseText));
                }
            },
            failure: function (xhr)
            {
                if (fail)
                {
                    fail.call(self, Ext.decode(xhr.responseText));
                }
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            params: {
                username: username,
                mobileApp: window.location.pathname.split('/')[1],
                'g-recaptcha-response': captcha
            }
        });
    },
    /**
     * Method returns true if access token is in refreshing state
     * @returns {Boolean}
     */
    isRefreshingToken: function ()
    {
        return this.refreshingToken;
    },
    /**
     * Method waiting for the access token and execute success callback when the token refreshed
     * @param success {Function?} on success callback
     * @param failure {Function?} on failure callback
     * @param args {[Object]?} callback arguments
     * @param self {Object?} self object
     */
    callMethodWhenTokenAvailable: function (success, failure, args, self)
    {
        if (this.requestsWaitsForToken === null)
        {
            this.requestsWaitsForToken = [];
        }
        if (this.isRefreshingToken())
        {
            this.requestsWaitsForToken.push({success: success, failure: failure, args: args, self: self});
        }
        else if ((this.getAccessToken() === null) || this.isExpired())
        {
            if (this.getRefreshToken()) {
                this.requestsWaitsForToken.push({success: success, failure: failure, args: args, self: self});
                this.refreshAccessToken(function() {
                        var arr, i;
                        arr = this.requestsWaitsForToken;
                        this.requestsWaitsForToken = [];
                        for (i = 0; i < arr.length; i++) {
                            if (arr[i].success) {
                                arr[i].success.apply(arr[i].self || this, arr[i].args);
                            }
                        }
                    },
                    function() {
                        var arr, i;
                        arr = this.requestsWaitsForToken;
                        this.requestsWaitsForToken = [];
                        for (i = 0; i < arr.length; i++) {
                            if (arr[i].failure) {
                                arr[i].failure.apply(arr[i].self || this, arr[i].args);
                            }
                        }
                    }, this);
            }
            else {
                this.removeTokens();
                this.requestsWaitsForToken = [];
                MobileUI.core.Metadata.killMetadata();
                MobileUI.core.Navigation.redirectForward('login');
                window.location.reload();
                MobileUI.core.util.Storage.removeItem('username');
            }
        }
        else
        {
            if (success)
            {
                success.apply(self || this, args);
            }
        }
    },
    /**
     * Method return true if the access_token has been updated from the store
     * @returns {boolean}
     */
    checkAccessTokenFromStore: function ()
    {
        var accessToken = MobileUI.core.util.Storage.getItem('access_token'),
            refreshToken, username, expired;
        if (!accessToken || this.accessToken === accessToken)
        {
            return false;
        }

        refreshToken = MobileUI.core.util.Storage.getItem('refresh_token');
        username = MobileUI.core.util.Storage.getItem('username');
        expired = Number(MobileUI.core.util.Storage.getItem('expired'));

        if (expired - new Date().getTime() <= 60000)
        {
            return false;
        }

        this.setAccessToken(accessToken);
        this.setRefreshToken(refreshToken);
        this.setExpired(expired);
        this.setUserName(username);
        this.user = {full_name: username};
        this.fireEvent('inited');
        return true;
    },

    /**
     * Method removes all saved credentials
     */
    logout: function (callback, self) {
        this.removeTokens();
        callback.call(self);
    },

    removeTokens: function () {
        this.setAccessToken(null);
        this.setRefreshToken(null);
        this.setExpired(null);
        MobileUI.core.util.Storage.removeItem('username');
        MobileUI.core.util.Storage.removeItem('refresh_token');
        MobileUI.core.util.Storage.removeItem('access_token');
        MobileUI.core.util.Storage.removeItem('expired');
        MobileUI.core.util.Storage.removeItem('userdata');
    },

    /**
     * Method refresh access token
     * @param success {Function?} on success callback
     * @param failure  {Function?} on failure callback
     * @param self {Object?} self object
     */
    refreshAccessToken: function (success, failure, self)
    {
        var isLoggedIn, md, tempRefreshToken, username;

        if (this.getRefreshToken() === null)
        {
            this.setRefreshToken(MobileUI.core.util.Storage.getItem('refresh_token'));
        }
        if (!this.getRefreshToken())
        {
            if (failure)
            {
                failure.call(self || this, {error: 'No refresh token'});
            }
            return;
        }

        isLoggedIn = this.isLoggedIn();

        if (this.checkAccessTokenFromStore())
        {
            md = MobileUI.core.Metadata;
            if (!md.isInited() && MobileUI.core.I18n.isInited)
            {
                md.init();
            }
            if (success)
            {
                success.call(self || this, null);
            }
            return;
        }

        tempRefreshToken = this.getRefreshToken();
        username = MobileUI.core.util.Storage.getItem('username');

        this.setAccessToken(null);
        this.setRefreshToken(null);
        this.setExpired(null);
        MobileUI.core.util.Storage.removeItem('username');
        MobileUI.core.util.Storage.removeItem('refresh_token');
        MobileUI.core.util.Storage.removeItem('access_token');
        MobileUI.core.util.Storage.removeItem('expired');

        this.refreshingToken = true;
        this.sendRefreshAccessTokenRequest(tempRefreshToken, function (json)
        {
            this.self.refreshingToken = false;
            if (json.error)
            {
                if (this.l.failure)
                {
                    this.l.failure.call(this.l.self, json);
                }
                return;
            }
            json.refresh_token = json.refresh_token || tempRefreshToken;
            if (json.refresh_token)
            {
                this.self.setRefreshToken(json.refresh_token);
                this.self.setAccessToken(json.access_token);
                this.self.setExpired(new Date().getTime() + Number(json.expires_in) * 1000);
                MobileUI.core.util.Storage.setItem('refresh_token', json.refresh_token);
                this.self.setUserName(this.username);
                if (Ext.isEmpty(this.user))
                {
                    this.user = {full_name: username};
                }
                MobileUI.core.util.Storage.setItem('access_token', json.access_token);
                MobileUI.core.util.Storage.setItem('expired', this.self.expired);
                this.self.fireEvent('inited');
                this.self.requestUserInfo();
            }
            var md = MobileUI.core.Metadata;
            if (!md.isInited())
            {
                md.init();
            }
            if (this.l.success)
            {
                this.l.success.call(this.l.self || this, json);
            }

        }, function (resp)
        {
            if (failure)
            {
                this.l.failure.call(this.l.self, resp);
            }
        }, {
            self: this,
            l: {self: self, success: success, failure: failure},
            isLoggedIn: isLoggedIn,
            username: username
        });
    },
    
    getPermissions: function () {
        return this.permissions;
    },

    requestUserInfo: function (bForce)
    {
        if (bForce || !MobileUI.core.util.Storage.getItem('userdata'))
        {
            MobileUI.core.Services.asyncRequest(MobileUI.core.session.MOAuth.URL_OAUTH_CHECK_TOKEN + MobileUI.core.Services.getTenantName(),
                function (json)
                {
                    var that = this;
                    json = json.user;
                    that.userProfileEntityId = json.entity || null;
                    that.roles = json.roles || null;
                    that.customer = json.customer || null;
                    that.externalTokens = json.externalTokens || null;
                    that.permissions = json.userPermissions || {};
                    if (that.user) {
                        that.user.full_name = json.username;
                    } else {
                        that.user = {full_name: json.username};
                    }
                    that.setUserName(json.username);
                    MobileUI.core.util.Storage.setItem('userdata', Ext.encode({
                        roles: that.roles,
                        customer: that.customer,
                        entity: that.userProfileEntityId,
                        user: that.user
                    }));
                    that.fireEvent('inited');
                },
                null,

                {
                    method: 'POST',
                    data: 'token='+this.accessToken,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                },
                this);
        }
        else
        {
            var json = MobileUI.core.util.Storage.getItem('userdata');
            try
            {
                json = Ext.decode(json);
            }
            catch (e)
            {
                json = {};
            }
            this.userProfileEntityId = json.entity || null;
            this.roles = json.roles || null;
            this.customer = json.customer || null;
        }
    },
    /**
     * Restore password method
     * @param token {String} token
     * @param password {String} new password
     * @param success {Function?} success listener
     * @param fail {Function?} fail listener
     * @param self {Object?} self object
     */
    restorePassword: function (token, password, success, fail, self)
    {
        var url = MobileUI.core.Services.getGeneralSettings().ui;
        url += 'ui/forgot';

        Ext.Ajax.request({
            url: url,
            success: function (xhr)
            {
                if (success)
                {
                    success.call(self, Ext.decode(xhr.responseText));
                }
            },
            failure: function (xhr)
            {
                if (fail)
                {
                    fail.call(self, Ext.decode(xhr.responseText));
                }
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            params: {
                mobile: 'true',
                token: token,
                newpassword: password
            }
        });
    },
    /**
     * Method adds the auth header and return header object
     * @param headers {Object?} header object
     * @returns {Object}
     */
    addSignHeader: function (headers)
    {
        if (!Ext.isObject(headers))
        {
            headers = {};
        }
        headers['Authorization'] = 'Bearer ' + this.getAccessToken();
        return headers;
    }
});
