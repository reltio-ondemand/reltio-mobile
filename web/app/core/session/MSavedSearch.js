/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MSavedSearch', {

    requires: ['MobileUI.core.search.SearchParametersManager'],

    statics: {
        URL_SAVED_SEARCH: '/personal/{0}',

        URL_SAVED_SEARCH_FIND: '/personal/findSavedSearches?countResults={0}&findPublic={1}',

        URL_SAVED_SEARCH_DEFAULT_SUFFIX: 'savedSearches'
    },

    /** @private */
    cache: {},
    /**
     * @param {Number} max
     * @param {Number} offset
     * @param {Object} args (favoriteOnly, sortBy, sortOrder, includes, startsWith)
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    findSavedSearches: function(max, offset, args, success, failure, self) {
        var cache = this.cache;
        this.sendRequest(MobileUI.core.session.MSavedSearch.URL_SAVED_SEARCH_FIND,
            [!!args.countResults, !!args.findShared], {
                method: 'POST',
                data: {
                    offset: offset,
                    max: max,
                    favoriteOnly: args.favoriteOnly,
                    sortBy: args.sortBy,
                    sortOrder: args.sortOrder,
                    includes: args.includes,
                    startsWith: args.startsWith
                }
            },
            function(data) {
                var searches = ((data && data.result) || []).map(function(item) {
                    var globalFilterUris = MobileUI.core.search.SearchParametersManager.getGlobalFilter().uris,
                        fullName;

                    if (globalFilterUris.indexOf(item.uri) !== -1) {
                        item.isGlobalFilter = true;
                    }
                    fullName = MobileUI.core.session.Session.getUser() ? MobileUI.core.session.Session.getUser().full_name : '';
                    item.ownedByCurrentUser = (item.owner === fullName);

                    return {
                        type: MobileUI.components.list.items.BaseListItem.types.savedSearch,
                        label: item.name,
                        secondaryLabel: item.owner,
                        score: item.count === undefined ? '' : Number(item.count),
                        userData: item
                    };
                });
                searches.forEach(function(search) {
                    cache[search.userData.uri] = search.userData;
                });

                searches.
                    map(function(item) {
                        return {
                            item: item,
                            processed: MobileUI.controller.MSavedSearches.extractSavedSearchUIData(item.userData)
                        };
                    }).
                    filter(function(data) { return !!data.processed; }).
                    forEach(function(data) {
                        data.item.info = MobileUI.core.search.SearchParametersManager.buildSearchParameters(
                            data.processed.keyword, data.processed.params, data.processed.facets);
                    });

                success.call(self, searches);
            }, failure, self);
    },

    /**
     * @param {Object} savedSearch
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    createSavedSearch: function(savedSearch, success, failure, self) {
        this.cache[savedSearch.uri] = savedSearch;
        this.requestSavedSearches('POST', null, success, failure, self, savedSearch);
    },

    /**
     * @param {String} uri
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    getSavedSearch: function(uri, success, failure, self) {
        var cache = this.cache,
            savedSearch = cache[uri];
        if (savedSearch) {
            success.call(self, savedSearch);
        }
        else {
            this.requestSavedSearches('GET', uri, function(savedSearch) {
                cache[savedSearch.uri] = savedSearch;
                success.apply(this, arguments);
            }, failure, self);
        }
    },

    getCachedSavedSearch: function(uri) {
        return this.cache[uri];
    },

    /**
     * @param {String} uri
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    requestSavedSearch: function(uri, success, failure, self) {
        this.requestSavedSearches('GET', uri, success, failure, self);
    },

    /**
     * @param {String} uri
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    deleteSavedSearch: function(uri, success, failure, self) {
        delete this.cache[uri];
        this.requestSavedSearches('DELETE', uri, success, failure, self);
    },

    /**
     * @param {Object} savedSearch
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self]
     */
    updateSavedSearch: function(savedSearch, success, failure, self) {
        this.cache[savedSearch.uri] = savedSearch;
        this.requestSavedSearches('PUT', savedSearch.uri, success, failure, self, savedSearch);
    },

    /**
     * @private
     * @param {String} method for execute
     * @param {String} uri  or existing saved search
     * @param {Function} success callback function
     * @param {Function} failure callback function
     * @param {Object} [self] optional. Context of execution of the listener.
     * @param {Object} [savedSearch] data to send
     */
    requestSavedSearches: function(method, uri, success, failure, self, savedSearch) {
        savedSearch = savedSearch && {
            name: savedSearch.name,
            description: savedSearch.description || '',
            query: savedSearch.query,
            isFavorite: !!savedSearch.isFavorite,
            isPublic: !!savedSearch.isPublic,
            uiState: savedSearch.uiState || {}
        };

        if (method === 'POST') {
            savedSearch = [savedSearch];
        }

        var data = {
            method: method
        };

        if (savedSearch) {
            data.data = savedSearch;
        }

        this.sendRequest(MobileUI.core.session.MSavedSearch.URL_SAVED_SEARCH,
            [uri || MobileUI.core.session.MSavedSearch.URL_SAVED_SEARCH_DEFAULT_SUFFIX],
            data, success, failure, self);

    }
});
