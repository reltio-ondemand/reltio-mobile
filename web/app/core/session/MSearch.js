/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MSearch', {
    statics: {
        URL_SEARCH_TYPE_AHEAD: '/entities/_typeAheadSearch?{0}select=uri,label,type,roles,secondaryLabel,defaultProfilePicValue&scoreEnabled=true',
        URL_SEARCH_PREFIX_FULL: '/entities?{0}select=uri,label,type,roles,secondaryLabel,defaultProfilePicValue&scoreEnabled=true',
        URL_SEARCH_FACETS: '/entities/_facets?{0}',
        URL_SEARCH_PREFIX_PAGING_ATTRIBUTES: '/entities?{0}select=uri,label,type,attributes,secondaryLabel,defaultProfilePicValue&max={1}&offset={2}&scoreEnabled=true',
        /* TODO: Pattern i18n
        URL_SEARCH_PREFIX_PAGING: '/entities?{0}select=uri,type,defaultProfilePicValue,{3}&max={1}&offset={2}&scoreEnabled=true',*
         */
        URL_SEARCH_PREFIX_PAGING: '/entities?{0}select=uri,label,type,secondaryLabel,defaultProfilePicValue&max={1}&offset={2}&scoreEnabled=true',
        URL_SEARCH_TOTAL: '/entities/_total?{0}'
    },
    /**@type {Array} @private */
    previousSearchParams: null,
    /** @private */
    previousSearchMax: null,
    /** @private */
    previousSearchOffset: null,


    /**
     * Method processes query params
     * @private
     * @param params {Object} query params
     * @returns {string}
     */
    processPrefixParams: function (params)
    {
        var prefix = [],
            key, value, result;
        for (key in params)
        {
            if (params.hasOwnProperty(key))
            {
                value = params[key];
                if (value && value.length > 0 && value.trim().length > 0)
                {
                    if (key === 'value')
                    {
                        prefix.push('containsWordStartingWith(attributes,\'' + MobileUI.core.search.SearchEngine.escapeForSearch(value) + '\')');
                    }
                    else
                    {
                        prefix.push('containsWordStartingWith(attributes.' + key + ',\'' + MobileUI.core.search.SearchEngine.escapeForSearch(value) + '\')');
                    }
                }
            }
        }
        if (prefix.length > 0)
        {
            result = 'filter=' + prefix.join(' and ');
        }
        return result || '';

    },
    /**
     * Method generates search prefix
     * @private
     * @param params {Object}
     * @param entityType {Array|String?}
     * @param onlyThisEntityType {Boolean?}
     * @returns {string}
     */
    generateSearchPrefix: function (params, entityType, onlyThisEntityType)
    {
        var strQuery = '',
            defltQuery = 'filter=';
        params = params || [];
        var prefixQuery = this.processPrefixParams(params);
        var globalQueryPart = this.getSearchEngine().getGlobalFilterPart(),
            ignoreEntityType = false ;
        if(prefixQuery)
        {
            strQuery = prefixQuery;
        }
        else
        {
            strQuery = defltQuery;
        }
        if(globalQueryPart)
        {
            if(defltQuery === prefixQuery)
            {
                strQuery += globalQueryPart;
            }
            else
            {
                strQuery += ' and (' + globalQueryPart + ')';
            }
            if (globalQueryPart.indexOf('equals('+MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME) !== -1){
                ignoreEntityType = true;
            }
        }
        if (entityType && (onlyThisEntityType || !ignoreEntityType))
        {
            if (Ext.isArray(entityType))
            {
                if(entityType.length > 0 && Ext.isString(entityType[0])) {
                    entityType = [entityType.map(function(item) {return {type: MobileUI.components.facets.EntityTypeSearchFacet.FIELD_NAME, term: item};})];
                }
                var prefix = [];
                //Type is an array of arrays, so generate a set of "field(<type>, <term>)" constructions
                for (var i = 0; i < entityType.length; i++)
                {
                    var inner = entityType[i];
                    var typeArr = [];
                    for (var j = 0; j < inner.length; j++)
                    {
                        typeArr.push('equals(' + inner[j].type + ',\'' + inner[j].term + '\')');
                    }
                    var l = typeArr.length;
                    if (l === 1)
                    {
                        prefix.push(typeArr[0]);
                    }
                    else if (l > 1)
                    {
                        prefix.push('(' + typeArr.join(' or ') + ')');
                    }
                }
                entityType = prefix.join(' and ');
            }
            else
            {
                entityType = 'equals(type,"' + entityType + '")';
            }
            if (strQuery === defltQuery) {
                strQuery += entityType;
            }
            else {
                strQuery += ' and ' + entityType;
            }
        }
        return strQuery;
    },

    /**
     *
     * @param params {Object?} type ahead parameters
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    typeAheadSearch: function(params, success, failure, self)
    {
        var searchableTypes =  Object.keys(MobileUI.core.Metadata.getEntityTypes(function(item){
            return item.searchable !== false && !item['abstract'];
        }));
        this.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_TYPE_AHEAD, this.generateSearchPrefix(params, searchableTypes) + '&max=3&scoreEnabled=true&', null,
            function(data) {
                if (success) {
                    success.call(this, data, params);
                }
            }, failure, self);
    },

    /**
     *
     * @param params {Object?} type ahead parameters
     * @param type {String|Array} entityType
     * @param offset {Number} offset
     * @param count {Number} limit
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    chooseEntitySearch: function (params, type, offset, count, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_PREFIX_FULL,
            this.generateSearchPrefix(params, type, true) +
                '&offset=' + offset + '&' +
                '&max=' + count + '&', null,
            function(data) {
                if (success) {
                    success.call(this, data, params);
                }
            }, failure, self);
    },

    findEntitiesByUris: function(uris, success, failure, self) {
        this.sendRequest(MobileUI.core.session.MSearch.URL_SEARCH_PREFIX_FULL,
            'filter=' + MobileUI.core.search.SearchEngine.buildQuery(
                [{fieldName: 'uri', filter: 'in', values: uris}]), null,
            function(data) {
                if (success) {
                    success.call(this, data);
                }
            }, failure, self);
    },

    /**
     * Method allows to get all facets by field name
     * @param fieldName {String} fieldName
     * @param includeFilter {Array?} filters
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object?} self object
     */
    findFacet: function (fieldName, includeFilter, success, failure, self)
    {
        var facet = {fieldName: fieldName},
            params, advanced = MobileUI.core.search.SearchParametersManager.getAdvancedSearchParameters().map(function (item) {
                return item.fieldName;
            });

        params = this.getSearchEngine().getSearchParameters() || [];

        params = params.filter(function(item) {
            return fieldName !== item.fieldName && advanced.indexOf(item.fieldName) === -1;
        });
        if (Ext.isArray(includeFilter) && includeFilter.length > 0)
        {
            facet = {fieldName: fieldName, filter: 'containsWordStartingWith', values: includeFilter};
            params.push(facet);
        }
        this.getSearchEngine().facetSearch([facet], params, success, failure, self);
    },

    /**
     * Method performs the search
     * @param keyword {String} keyword
     * @param max {Number?} max search results item count
     * @param offset {Number?} offset
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object?} self object
     * @param force {Boolean?} is forced search
     */
    performSearch: function (keyword, max, offset, success, failure, self, force)
    {
        var additionalParams = MobileUI.core.search.SearchParametersManager.buildSearchParameters(),
            keywordParameter = [];
        max = max || 15;
        offset = offset || 0;
        if (!Ext.isEmpty(keyword))
        {
            keywordParameter = [{
                fieldName: 'attributes',
                filter: 'containsWordStartingWith',
                values: [keyword],
                notForState: true
            }];
        }
        additionalParams = additionalParams.concat(keywordParameter);
        if (!force &&
                MobileUI.core.util.Util.equals(additionalParams, this.previousSearchParams) &&
                this.previousSearchMax === max &&
                this.previousSearchOffset === offset &&
                additionalParams.length > 0) {
            this.processSearchResponse(success, self, null);
            return;
        }
        this.previousSearchParams = additionalParams;
        this.previousSearchMax = max;
        this.previousSearchOffset = offset;
        this.getSearchEngine().setSearchParameters(additionalParams);
        this.getSearchEngine().entitySearch(max, offset, function (resp)
        {
            this.processSearchResponse(success, self, resp);
        }, function(resp) {
            if (failure)
            {
                failure.call(self, resp);
            }
            this.fireEvent('searchComplete', []);
        }, this);

        this.getSearchEngine().totalEntitySearch(function (resp)
        {
            this.processTotalResponse(success, self, resp);
        }, function(resp) {
            if (failure)
            {
                failure.call(self, resp);
            }
            this.fireEvent('searchTotalComplete', {total:0});
        }, this);
    },
    /**
     * @protected
     */
    processSearchResponse: function (success, self, resp)
    {
        if (success)
        {
            success.call(self, resp);
        }
        this.fireEvent('searchComplete', resp);
    },

    processTotalResponse: function (success, self, resp)
    {
        if (success)
        {
            success.call(self, resp);
        }
        this.fireEvent('searchTotalComplete', resp);
    }

});
