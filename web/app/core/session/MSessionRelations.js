/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MSessionRelations', {
    statics: {
        URL_CONNECTIONS: '/{0}/_connections?limitCreditsConsumption=true',
        URL_REL_RATING: '/{0}/rate',
        URL_REL_GET: '/{0}',
        URL_REL_REMOVE: '/{0}',
        URL_REL_MODIFY: '/{0}',
        URL_REL_ADD: '/relations',
        URL_REL_UPDATE: '/{0}',
        URL_ENTITY_ACTIVENESS_DATE: '/{0}/activeness/{1}',
        URL_ENTITY_ACTIVENESS: '/{0}/activeness'
    },
    /**
     * Method prepares config request
     * @private
     * @param config {Object} request config object
     * @param filter {String?} filter/globalfilter
     * @param max {Number?} max response items
     * @param offset {Number?} query offset
     * @returns {*}
     */
    prepareRequestConfig: function (config, filter, max, offset)
    {
        var temp;
        offset = offset || 0;
        config = Ext.clone(config);
        temp = config;
        while (temp.nextEntry)
        {
            temp = temp.nextEntry;
        }
        if (max !== null || max !== undefined)
        {
            temp.max = max;
        }

        temp.offset = offset;
        if (filter)
        {
            temp.filter = filter.indexOf('containsWordStartingWith') !== 0? 'containsWordStartingWith(entity.label,\'' + filter + '\')':filter;
        }
        else if (temp.filter)
        {
            delete temp.filter;
        }

        var globalFilterPart = this.getSearchEngine().getGlobalFilterPart();
        if (globalFilterPart)
        {
            if (temp.filter)
            {
                temp.filter += ' and (' + globalFilterPart + ')';
            }
            else
            {
                temp.filter = globalFilterPart;
            }
        }

        if (config.sortOptions)
        {
            delete config.sortOptions;
        }
        return config;
    },
    /**
     * Method requests relations
     * @param config {Array} request config object
     * @param filter {String?} filter
     * @param max {Number?} max response items
     * @param entityUri {String?} entity uri if null the current uri will be used
     * @param offset {Number?} query offset
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    requestRelationsImmediately: function (config, filter, entityUri, max, offset, success, failure, self)
    {
        config = config.map(function (item)
        {
            return this.prepareRequestConfig(item, filter, max, offset);
        }, this);

        this.requestRelationsInternal(config, entityUri, success, failure, self);
    },
    /**
     * Method requests relations
     * @private
     * @param requests {Array} array requests
     * @param entityUri {String?} entity uri if null the current uri will be used
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    requestRelationsInternal: function (requests, entityUri, success, failure, self)
    {
        entityUri = entityUri || this.getEntityUri();
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_CONNECTIONS, [entityUri], {
            method: 'POST',
            data: requests
        }, success, failure, self);
    },
    /**
     * Method adds rating to the relation
     * @param relationUri {String} relation url
     * @param rating {String|Number} rating
     * @param comment {String?} comment
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    addRelationRating: function (relationUri, rating, comment, success, failure, self)
    {
        var data = {
            value: rating,
            comment: comment
        };
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_RATING, [relationUri], {
            data: data,
            method: 'POST'
        }, function (json)
        {
            if (success !== undefined && success !== null)
            {
                success.call(self, [json]);
            }
        }, function (error)
        {
            if (failure !== undefined && failure !== null)
            {
                failure.call(self, error);
            }
        }, this, true);
    },
    /**
     * Method removes rating from the relation
     * @param relationUri {String} relation url
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    removeRelationRating: function (relationUri, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_RATING, [relationUri], {
            method: 'DELETE'
        }, function (json)
        {
            if (success !== undefined && success !== null)
            {
                success.call(self, [json]);
            }
        }, function (error)
        {
            if (failure !== undefined && failure !== null)
            {
                failure.call(self, error);
            }
        }, this, true);
    },
    /**
     * Method gets the relation
     * @param relationUri {String} relation url
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    getRelation: function (relationUri, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_GET, [relationUri], {
            method: 'GET'
        }, function (json)
        {
            if (success !== undefined && success !== null)
            {
                success.call(self, [json]);
            }
        }, function (error)
        {
            if (failure !== undefined && failure !== null)
            {
                failure.call(self, error);
            }
        }, this, true);
    },
    /**
     * Method removes the relation
     * @param relationUri {String} relation uri
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    removeRelation: function (relationUri, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_REMOVE, [relationUri], {
                method: 'DELETE'
            },
            function (json)
            {
                if (success !== undefined && success !== null)
                {
                    success.call(self, [json]);
                }
            }, function (error)
            {
                if (failure !== undefined && failure !== null)
                {
                    failure.call(self, error);
                }
            }, this, true);
    },
    /**
     * @private
     * @param url {String} url
     * @param options {{crosswalkValue: String, sourceTable: String}} options
     * @returns {*}
     */
    prepareRelationUrl: function (url, options)
    {
        var result = url;
        if (Ext.isObject(options))
        {
            if (options.crosswalkValue)
            {
                result += '?crosswalkValue=' + options.crosswalkValue;
            }
            if (options.sourceTable)
            {
                result += result.indexOf('?') === -1 ? '?' : '&';
                result += 'crosswalkSourceTable=' + options.sourceTable;
            }
        }
        return result;
    },
    /**
     * Change the Relation attribute
     * @param attributeUri {String} attribute uri
     * @param newValue {{}|String} value
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @param options {{crosswalkValue: String, sourceTable: String, sourceSystem: String}?} options
     */
    changeRelationAttribute: function (attributeUri, newValue, success, failure, self, options)
    {
        var objToSend =
                [{
                    value: newValue
                }],
            url;
        objToSend.method = 'PUT';

        url = this.prepareRelationUrl(MobileUI.core.session.MSessionRelations.URL_REL_MODIFY, options);

        if (options && options.sourceSystem)
        {
            objToSend.headers = {'Source-System': options.sourceSystem};
        }

        this.sendRequest(url,
            [attributeUri], objToSend, function (json)
            {
                if (success !== undefined && success !== null)
                {
                    success.call(self, MobileUI.core.session.MEntity.parseUriFromServerResult(json), json);
                }
            }, failure, self);
    },
    /**
     * Method creates the relation attribute
     * @param relationUri {String} relation uri
     * @param attrName {String|Number} attribute name
     * @param newValue {String|Number} value
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @param options {{crosswalkValue: String, sourceTable: String, sourceSystem: String}?} options
     */
    createRelationAttribute: function (relationUri, attrName, newValue, success, failure, self, options)
    {
        this.insertRelationAttribute(relationUri + '/attributes/' + attrName, success, failure, self, options);
    },
    /**
     * Method inserts the relation attribute
     * @param relationUri {String} relation uri
     * @param newValue {String|Number} value
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @param options {{crosswalkValue: String, sourceTable: String, sourceSystem: String}?} options
     */
    insertRelationAttribute: function (relationUri, newValue, success, failure, self, options)
    {
        this.insertRelationAttributeValues(relationUri, [{value: newValue}], success, failure, self, options);
    },
    /**
     * Method inserts the relation attribute values
     * @param relationUri {String} relation uri
     * @param arrValues {Array} values
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     * @param options {{crosswalkValue: String, sourceTable: String, sourceSystem: String}?} options
     */
    insertRelationAttributeValues: function (relationUri, arrValues, success, failure, self, options)
    {
        var objToSend = {
            data: arrValues
        }, url;
        objToSend.method = 'POST';

        url = this.prepareRelationUrl(MobileUI.core.session.MSessionRelations.URL_REL_MODIFY, options);

        if (options && options.sourceSystem)
        {
            objToSend.headers = {'Source-System': options.sourceSystem};
        }

        this.sendRequest(url,
            [relationUri], objToSend, function (json)
            {
                if (success !== undefined && success !== null)
                {
                    success.call(self, MobileUI.core.session.MEntity.parseUriFromServerResult(json), json);
                }
            }, failure, self, true);
    },
    /**
     * Method creates new relation
     * @param startEntityUri {String} start entity uri
     * @param endEntityUri {String} end entity uri
     * @param relation  {String} relation type
     * @param attributesOpt {Object?} options
     * @param startDate {Number?} start date
     * @param endDate {Number?} end date
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    addRelation: function (startEntityUri, endEntityUri, relation, attributesOpt, startDate, endDate, success, failure, self)
    {
        var data = {
            'type': relation,
            'startObject': {objectURI: startEntityUri},
            'endObject': {objectURI: endEntityUri}
        };
        if (attributesOpt)
        {
            data.attributes = attributesOpt;
        }
        if (startDate)
        {
            data.startDate = startDate;
        }
        if (endDate)
        {
            data.endDate = endDate;
        }
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_ADD, null, {
            'data': [data],
            'method': 'POST'
        }, success, failure, self);
    },
    updateRelationAttributes: function(uri, attributes, success, failure, self) {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_UPDATE, uri+ '/attributes', {
            'data': [attributes],
            'method': 'PUT'
        }, success, failure, self);
    },

    insertReferencedAttribute : function( attrType, entityUri, refEntityUri, refEntityCw, attributes,
                                          success, failure, self) {
        var tmpRefEntity = MobileUI.core.entity.TemporaryEntity.getEntity(refEntityUri);
        if (tmpRefEntity && tmpRefEntity.original) {
            tmpRefEntity = MobileUI.core.entity.EntityUtils.createEntityFromTemporary(tmpRefEntity);
            this.createEntity(tmpRefEntity, null, function(resp) {
                MobileUI.core.session.Session.insertReferencedAttribute(
                    attrType, entityUri, resp[0].object.uri, refEntityCw, attributes,
                    success, failure, self);
            }, failure, self);
        }
        else
        {
            attributes = MobileUI.core.entity.TemporaryEntity.getEntity(attributes);
            // find startObject and endObject, based on relation configuration
            var data = MobileUI.core.entity.EntityUtils.makeReferenceAttribute(attrType, entityUri, refEntityUri);
            if (attributes) {
                data.attributes = MobileUI.core.entity.EntityUtils.createEntityFromTemporary(attributes).attributes;
            }
            this.sendRequest(MobileUI.core.session.MSessionRelations.URL_REL_ADD, null, {
                data : [data],
                method: 'POST'
            }, success, failure, self);
        }
    },
    removeActivenessAttribute: function(entityUri, attrName, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_ENTITY_ACTIVENESS_DATE, [entityUri, attrName], {
            method: 'PUT',
            data : null
        }, success, failure, self);

    },
    updateActivenessAttribute: function(entityUri, values, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MSessionRelations.URL_ENTITY_ACTIVENESS, [entityUri], {
            method: 'PUT',
            data : values
        }, success, failure, self);

    }
});
