/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MTree', {
    statics: {
        URL_TREE: '/{0}/_tree?graphTypeURIs={1}&limitCreditsConsumption=true&&select=total',
        URL_TREE_FETCH: '/{0}/_tree?graphTypeURIs={1}&limitCreditsConsumption=true&&select=label,secondaryLabel',
        URL_GRAPH: '/{0}/_hops'
    },
    /**
     * Method requests tree
     * @param type {Array} graph type
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    requestTree: function (type, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MTree.URL_TREE, [this.getEntityUri(), type.join(',')], null, success, failure, self);
    },
    /**
     * Method requests full tree
     * @param type {Array} graph type
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    getFullTree: function (type, success, failure, self)
    {
        this.sendRequest(MobileUI.core.session.MTree.URL_TREE_FETCH, [this.getEntityUri(), type.join(',')], null, success, failure, self);
    },
    /**
     * Method requests graph from the api
     * @param entityUri {String} entity uri
     * @param type {String?} graph type
     * @param deep {Number?} deep. Default deep = 2
     * @param limit {Number?} max items count in response
     * @param success {Function?} success callback
     * @param failure {Function?} failure callback
     * @param self {Object} self object
     */
    getGraph: function (entityUri, type, deep, limit, success, failure, self) {
        var params = {
                deep: deep !== null ? deep : 2,
                filterLastLevel: false,
                limitCreditsConsumption: true
            },
            lastIndexOf;

        if (!entityUri) {
            MobileUI.core.Logger.log('entityUri is undefined');
            return;
        }
        if (!Ext.isEmpty(type)) {
            lastIndexOf = type.lastIndexOf('/');
            if (lastIndexOf !== -1) {
                type = type.substring(lastIndexOf + 1);
            }

            params['graphTypeURIs'] = type;
        }
        if (!Ext.isEmpty(limit)) {
            params['max'] = limit;
        }
        var options = '?' + Object.keys(params).map(function (key) {
            return key + '=' + params[key];
        }).join('&');
        this.sendRequest(MobileUI.core.session.MTree.URL_GRAPH + options, entityUri, null, success, failure, self);
    }
});
