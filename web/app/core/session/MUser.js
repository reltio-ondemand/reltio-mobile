/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MUser', {
    requires: [
    'MobileUI.core.Logger'
    ],

    /**@type {String} @private */
    userName: null,

    /**
     * Method allows to set the user name
     * @param userName {String} access token
     */
    setUserName: function (userName)
    {
        this.userName = userName;
        MobileUI.core.util.Storage.setItem('username', userName);
    },
    /**
     * Return the user name
     * @returns {String}
     */
    getUserName: function ()
    {
        return this.userName || '';
    },

    getUserProfileEntity: function(callback, context) {
        var userData = MobileUI.core.util.Storage.getItem('userdata'),
            entitiesList = userData && userData.entity && userData.entity.uri,
            tenant = MobileUI.core.Services.getTenantName(),
            environment = MobileUI.core.Services.getEnvironmentName(),
            entityInfo, i, found;

        try {
            entitiesList = JSON.parse(entitiesList);
        }
        catch(err) {
            MobileUI.core.Logger.log(err);
        }

        if (entitiesList && Ext.isArray(entitiesList)) {
            for (i = 0; !found && i < entitiesList.length; i++) {
                entityInfo = entitiesList[i];
                if ((entityInfo.tenant === tenant) && (entityInfo.environment === environment)) {
                    found = entityInfo;
                }
            }
        }

        if (found) {
            MobileUI.core.session.Session.requestEntity(found.uri, callback, function () {
                callback.apply(context);
            }, context, false, true);
        }

        callback.call(context);

    }
});
