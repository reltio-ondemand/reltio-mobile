/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.MValidation', {
    statics: {
        VALIDATE_ENTITY_URL: '/validate/{0}/entity?options=preserveURIs',
        VALIDATE_CHANGES_URL: '/validate/{0}/entity/{1}',
        VALIDATION_CODES: {
            INCORRECT_VALUE: 'incorrectValue',
            MISSED: 'missed',
            CUSTOM: 'customError'
        }
    },

    processCreatedReferenceAttributes: function (entity) {
        var editedAttributes = Array.prototype.concat.apply([], entity.addedAttributes);

        entity.editedAttributes.forEach(function (attribute) {
            var exists = editedAttributes.some(function (item) {
                return item.uri && attribute.uri && attribute.uri === item.uri;
            });
            if (!exists) {
                editedAttributes.push(attribute);
            }
        });

        editedAttributes
            .filter(function (attr) {
                return MobileUI.core.entity.EntityUtils.isReference(attr.attrType) &&
                    MobileUI.core.entity.TemporaryEntity.isTemp(attr.refEntity.objectURI);
            })
            .forEach(function (attr) {
                var tmpEntity = this.copyEntityForValidation(MobileUI.core.entity.TemporaryEntity.getEntity(attr.refEntity.objectURI));
                this.processCreatedReferenceAttributes(tmpEntity);

                var result = {
                    roles: [],
                    type: tmpEntity.type,
                    uri: tmpEntity.uri,
                    attributes: {}
                };
                MobileUI.controller.MSuggestEdit.modifyEntity(result, tmpEntity, true);
                Object.keys(result.attributes).forEach(function (key) {
                    attr.value[key] = result.attributes[key];
                });
                if (attr.refEntity) {
                    attr.refEntity = {objectURI: attr.refEntity.objectURI};
                }
                entity.editedAttributes.filter(function (attribute) {
                    return attribute.uri === attr.uri;
                }).forEach(function (attribute) {
                    if (attribute.refEntity) {
                        attribute.refEntity = {objectURI: attribute.refEntity.objectURI};
                    }
                    Object.keys(result.attributes).forEach(function (key) {
                        attribute.value[key] = result.attributes[key];
                    });
                });
            }, this);

    },

    copyEntityForValidation: function (entity) {
        var newEntity = {};
        newEntity.uri = entity.uri;
        newEntity.type = entity.type;
        newEntity.addedAttributes = MobileUI.core.util.Util.cloneEntity(entity.addedAttributes);
        newEntity.attributes = MobileUI.core.util.Util.cloneEntity(entity.attributes);
        newEntity.deletedAttributes = MobileUI.core.util.Util.cloneEntity(entity.deletedAttributes);
        newEntity.editedAttributes = MobileUI.core.util.Util.cloneEntity(entity.editedAttributes);
        newEntity.original = entity.original;
        return newEntity;

    },

    prepareForValidation: function (entity) {
        var newEntity = this.copyEntityForValidation(entity);
        this.processCreatedReferenceAttributes(newEntity);
        return new Promise(function (resolve) {
            var isTemp = !!MobileUI.core.entity.TemporaryEntity.isTemp(newEntity.uri);
            if (isTemp) {
                var result = {
                    roles: [],
                    type: entity.type,
                    uri: entity.uri,
                    attributes: {}
                };
                MobileUI.controller.MSuggestEdit.modifyEntity(result, newEntity, true);
                resolve(result);
            }
            else {
                MobileUI.core.session.Session.requestSlicedEntity(newEntity.uri, function (resp) {
                    MobileUI.controller.MSuggestEdit.modifyEntity(resp, newEntity, true);
                    resolve(resp);
                }, function (err) {
                    MobileUI.core.Logger.error(err);
                    resolve({});
                }, this);
            }
        });
    },
    cleanAttributes: function (attributes) {
        /*jshint loopfunc: true */
        for (var a in attributes) {
            if (attributes.hasOwnProperty(a)) {
                if (Ext.isArray(attributes[a])) {
                    attributes[a] = attributes[a]
                        .filter(function (val) {
                            return !Ext.isEmpty(val.value) || val.refEntity;
                        });
                    attributes[a]
                        .filter(function (val) {
                            return Ext.isObject(val.value);
                        })
                        .forEach(function (val) {
                            this.cleanAttributes(val.value);
                        }, this);
                    if (!attributes[a].length) {
                        delete attributes[a];
                    }
                }
            }
        }
        /*jshint loopfunc: true */
    },
    validateEntity: function (entity) {
        var Services = MobileUI.core.Services,
            tenant = Services.getTenantName();

        var clonedEntity = MobileUI.core.util.Util.cloneEntity(entity);
        MobileUI.core.util.Util.forEach(clonedEntity, function (key, value, obj) {
            if (Ext.isString(value) && value.indexOf('__temp_') !== -1 && key.toString().toLowerCase().indexOf('uri') !== -1) {
                obj[key] = value.replace('__temp_', '1Tempo4rary');
            }
        }, this, true, ['editor', 'valueChangedContext']);
        this.cleanAttributes(clonedEntity.attributes);
        return this.sendValidationRequest(MobileUI.core.session.MValidation.VALIDATE_ENTITY_URL, [tenant], {
            method: 'POST',
            data: clonedEntity
        })
            .then(function (response) {
                if (Ext.isArray(response) && response.length > 0) {
                    entity = entity || this.getEntity();
                    return {validationFailed: response.map(this.createValidationResponseFunction(entity), this)};
                }
                return {};
            }.bind(this));
    },

    sendValidationRequest: function (url, params, data) {
        var me = this;
        return new Promise(function (resolve) {
            var servicePath = MobileUI.core.Services.getGeneralSettings().validationServicePath;
            if (servicePath) {
                MobileUI.core.session.Session.sendRequest(servicePath + url, params, data, function (result) {
                    resolve(result);
                }, function (error) {
                    MobileUI.core.Logger.log(error);
                    resolve({});
                }, me);
            } else {
                resolve({});
            }
        });
    },

    createValidationResponseFunction: function (entity) {
        var validator = MobileUI.core.session.Session.getValidator();
        return function (responseRecord) {
            var metadata = MobileUI.core.Metadata;
            var ValidationCodes = MobileUI.core.session.MValidation.VALIDATION_CODES;
            var record;

            var attrType = metadata.getEntityAttributeHierarchy(responseRecord.objectTypeUri, true);
            if (attrType) {
                attrType = attrType[attrType.length - 1];
            }

            if (responseRecord.errorType === 'MISSED') {
                record = {
                    invalidType: ValidationCodes.MISSED,
                    attrType: attrType,
                    objectUri: responseRecord.objectUri,
                    value: responseRecord.message,
                    parent: entity
                };
            } else if (responseRecord.errorType === 'INCORRECT' && Ext.isEmpty(responseRecord.objectTypeUri) ) {
                record = {
                    invalidType: ValidationCodes.CUSTOM,
                    objectUri: responseRecord.objectUri,
                    value: responseRecord.message
                };
            } else {
                record = {
                    invalidType: ValidationCodes.INCORRECT_VALUE,
                    value: responseRecord.message,
                    objectUri: responseRecord.objectUri,
                    attrType: attrType
                };
            }
            if (record.objectUri) {
                record.objectUri = MobileUI.core.entity.EntityUtils.tmpUriFromApiFormat(record.objectUri);
            }
            if (responseRecord.objectParentUri) {
                responseRecord.objectParentUri = MobileUI.core.entity.EntityUtils.tmpUriFromApiFormat(responseRecord.objectParentUri);
                record.objectParentUri = responseRecord.objectParentUri;
            }
            if (responseRecord.originalObjectUri) {
                record.path = validator.findPath(responseRecord.originalObjectUri, entity);
            } else if (record.objectUri) {
                record.path = validator.findPath(record.objectUri, entity);
            } else if (responseRecord.objectParentUri && responseRecord.objectParentUri !== entity.uri){
                record.path = validator.findPath(responseRecord.objectParentUri, entity);
            }
            validator.updateExternalValidationResults(record);
            return record;
        };
    }

});