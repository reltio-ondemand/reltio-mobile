/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.Session', {
    singleton: true,
    requires: [
        'Ext.mixin.Observable',
        'MobileUI.core.Logger',
        'MobileUI.core.util.Storage',
        'MobileUI.core.Services',
        'MobileUI.core.Metadata',
        'MobileUI.core.search.SearchEngine',
        'MobileUI.core.search.SearchParametersManager',
        'MobileUI.core.session.MSessionRelations',
        'MobileUI.core.session.MTree',
        'MobileUI.core.session.MEntity',
        'MobileUI.core.session.MOAuth',
        'MobileUI.core.session.MLookups',
        'MobileUI.core.session.newEditMode.MAttributesDiff',
        'MobileUI.core.session.newEditMode.MCumulativeUpdate',
        'MobileUI.core.session.newEditMode.MWorkflow',
        'MobileUI.core.session.newEditMode.MValidation',
        'MobileUI.core.session.newEditMode.MValidationService',
        'MobileUI.core.DependentLookupsManager',
        'MobileUI.core.CardinalityChecker',
        'MobileUI.core.Validator',
        'MobileUI.core.UriMapping'
    ],
    mixins: {
        oauth: 'MobileUI.core.session.MOAuth',
        entity: 'MobileUI.core.session.MEntity',
        user: 'MobileUI.core.session.MUser',
        dataprovider: 'MobileUI.core.session.MDataProvider',
        observable: 'Ext.mixin.Observable',
        search: 'MobileUI.core.session.MSearch',
        savedsearch: 'MobileUI.core.session.MSavedSearch',
        relations: 'MobileUI.core.session.MSessionRelations',
        tree: 'MobileUI.core.session.MTree',
        lookups: 'MobileUI.core.session.MLookups',
        activity: 'MobileUI.core.session.MActivity',
        validation: 'MobileUI.core.session.MValidation',
        attributesDiff: 'MobileUI.core.session.newEditMode.MAttributesDiff',
        cumulativeUpdate: 'MobileUI.core.session.newEditMode.MCumulativeUpdate',
        workflow: 'MobileUI.core.session.newEditMode.MWorkflow',
        newValidation: 'MobileUI.core.session.newEditMode.MValidation',
        validationService: 'MobileUI.core.session.newEditMode.MValidationService'
    },
    /**@type {MobileUI.core.search.SearchEngine} @private */
    searchEngine: null,
    /**@type {MobileUI.core.DependentLookupsManager} @private */
    lookupsManager: null,
    /**@type {MobileUI.core.UriMapping} @private */

    uriMapping: null,
    /**
     * @events 'searchComplete'
     * @events 'searchTotalComplete'
     */

    constructor: function (config)
    {
        this.initConfig(config);
        this.searchEngine = Ext.create('MobileUI.core.search.SearchEngine');
        this.lookupsManager = Ext.create('MobileUI.core.DependentLookupsManager');
        this.cardinalityChecker = Ext.create('MobileUI.core.CardinalityChecker');
        this.validator = Ext.create('MobileUI.core.Validator');
        this.uriMapping = Ext.create('MobileUI.core.UriMapping');
        MobileUI.core.search.SearchParametersManager.loadFromStorage();
    },

    /**
     * Method returns search engine
     * @returns {MobileUI.core.search.SearchEngine}
     */
    getSearchEngine: function ()
    {
        return this.searchEngine;
    },

    getLookupsManager: function(){
        return this.lookupsManager;
    },

    getCardinalityChecker: function(){
        return this.cardinalityChecker;
    },

    getValidator: function(){
        return this.validator;
    },
    getUriMapping: function(){
        return this.uriMapping;
    }
});
