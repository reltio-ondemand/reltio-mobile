/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.newEditMode.MAttributesDiff', {

    compareAttributes: function (a, b, keepUri) {
        return window.uiSDK.entity.AttributesDiff.compareAttributes(a, b, keepUri);
    },

    getEntityAttributesDiff: function (keepUri) {
        return this.compareAttributes(this.getEntity().attributes, this.getModifiedEntity().attributes, keepUri);
    },

    collectCrosswalksFromAttributes: function (attributes, crosswalks) {
        return window.uiSDK.entity.AttributesDiff.collectCrosswalksFromAttributes(attributes, crosswalks);
    },

    applyCrosswalks: function (diff, crosswalks) {
        return window.uiSDK.entity.AttributesDiff.applyCrosswalks(diff, crosswalks);
    },

    compareImages: function (a, b) {
        return window.uiSDK.entity.AttributesDiff.compareImages(a, b);
    },

    cleanEntityAttributes: function (attributes) {
        /*jshint loopfunc: true */
        for (var a in attributes) {
            if (attributes.hasOwnProperty(a)) {
                attributes[a] = attributes[a].filter(function (val) {
                    return val.value !== undefined && val.value !== '' || val.refEntity;
                });
                attributes[a].forEach(function (val) {
                    delete val.type;
                    delete val.uri;

                    if (Ext.isObject(val.value)) {
                        this.cleanEntityAttributes(val.value);
                    }
                }, this);
                if (!attributes[a].length) {
                    delete attributes[a];
                }
            }
        }
        /*jshint loopfunc: false */
    }
});
