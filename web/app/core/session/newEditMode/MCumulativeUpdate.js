/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.newEditMode.MCumulativeUpdate', {
    statics: {
        URL_CUMULATIVE_UPDATE: '/{0}/_update?options=sendHidden,updateAttributeUpdateDates'
    },

    dcrId: null,
    processedChanges: null,

    updateWithDCR: function (uri) {
        if (this.dcrId) {
            uri += (uri.indexOf('?') > 0 ? '&' : '?') + 'changeRequestId=' + this.dcrId;
        }
        return uri;
    },

    saveAllPendingChanges: function (entity) {
        this.dcrId = null;
        this.processedChanges = [];

        if (MobileUI.core.entity.EntityUtils.isTemporaryUri(entity.uri)) {
            return this.saveNewEntity(entity);
        } else {
            return this.saveAttributesDiff(entity);
        }
    },

    saveAttributesDiff: function (entity) {
        var attrDiff = this.getEntityAttributesDiff();

        var crosswalks = entity.crosswalks;
        crosswalks = this.collectCrosswalksFromAttributes(entity.attributes, crosswalks);
        attrDiff = this.applyCrosswalks(attrDiff, crosswalks);

        var otherDiff = this.compareImages(this.getEntity(), entity);
        var allDiff = attrDiff.concat(otherDiff);

        return this.saveAllTemporaryEntities(entity)
            .then(function (urisMap) {
                this.updateTemporaryUris(entity, urisMap);
                return this.cumulativeUpdate(entity.uri, allDiff);
            }.bind(this))
            .catch(function (err) {
                MobileUI.core.Logger.log(err);
            });
    },

    saveNewEntity: function (entity) {
        return this.saveAllTemporaryEntities(entity)
            .then(function (urisMap) {
                this.updateTemporaryUris(entity, urisMap);
                this.cleanEntityAttributes(entity.attributes);
                var clone = Ext.clone(entity);
                delete clone.uri;

                return this.createEntity(clone, this.dcrId);
            }.bind(this))
            .catch(function (err) {
                MobileUI.core.Logger.log(err);
            });
    },

    updateTemporaryUris: function (entity, urisMap) {
        for (var name in entity.attributes) {
            if (entity.attributes.hasOwnProperty(name)) {
                /*jshint loopfunc: true */
                entity.attributes[name].forEach(function (attrValue) {
                    if (attrValue.refEntity && MobileUI.core.entity.EntityUtils.isTemporaryUri(attrValue.refEntity.objectURI)) {
                        attrValue.refEntity.objectURI = urisMap[attrValue.refEntity.objectURI];
                    }
                });
                /*jshint loopfunc: false */
            }
        }
    },

    saveAllTemporaryEntities: function (entity) {
        var attributes = entity.attributes;
        var temporaryEntities = Object.keys(attributes)
            .map(function (key) {
                return attributes[key];
            })
            .reduce(function (flat, toFlatten) {
                return flat.concat(toFlatten);
            }, [])
            .map(function (attrValue) {
                if (attrValue.refEntity && MobileUI.core.entity.EntityUtils.isTemporaryUri(attrValue.refEntity.objectURI)) {
                    return attrValue.refEntity.objectURI;
                }
            })
            .filter(function (uri) {
                return uri;
            })
            .map(function (uri) {
                return MobileUI.core.entity.TemporaryEntity.getEntity(uri);
            });

        var map = {};

        if (temporaryEntities.length === 0) {
            return Promise.resolve(map);
        } else {
            var onSuccessSave = function (savedResult, i) {
                var entityUri;

                if (savedResult.changes) {
                    if (!this.dcrId) {
                        this.dcrId = savedResult.uri.split('/')[1];
                    }

                    entityUri = Object.keys(savedResult.changes).filter(function (uri) {
                        return this.processedChanges.indexOf(uri) === -1;
                    }, this)[0];

                    this.processedChanges.push(entityUri);
                } else {
                    entityUri = savedResult[0].object.uri;
                }

                map[temporaryEntities[i].uri] = entityUri;
            }.bind(this);

            return this.saveNewEntity(temporaryEntities[0])
                .then(function (result) {
                    onSuccessSave(result, 0);
                    return Promise.all(temporaryEntities.slice(1).map(this.saveNewEntity, this))
                        .then(function (savedEntities) {
                            savedEntities.forEach(function (saveResult, i) {
                                onSuccessSave(saveResult, i + 1);
                            }, this);
                            return map;
                        }.bind(this));
                }.bind(this))
                .catch(function (err) {
                    MobileUI.core.Logger.log(err);
                });
        }
    },

    cumulativeUpdate: function (objectUri, diff) {
        return new Promise(function (resolve, reject) {
            this.sendRequest(
                this.updateWithDCR(MobileUI.core.session.newEditMode.MCumulativeUpdate.URL_CUMULATIVE_UPDATE),
                [objectUri],
                {
                    method: 'POST',
                    data: diff
                },
                resolve,
                reject);
        }.bind(this));
    }

});
