/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.newEditMode.MValidation', {

    validateAll: function (entity) {
        return Promise.all([
            this.validateEmptyEntity(entity),
            this.validateRequired(entity),
            this.validateOnService(entity)
        ]).then(function (validationResponses) {
            return [].concat.apply([], validationResponses);
        }).catch(function (err) {
            MobileUI.core.Logger.log(err);
        });
    },

    validateRequired: function (entity) {
        var attrTypes = MobileUI.core.Metadata.getEntityType(entity.type).attributes;
        attrTypes = (attrTypes || []).filter(function (attrType) {
            return MobileUI.core.PermissionManager.securityService().metadata.canCreateOrInitiate(attrType);
        });
        var result = this.validateAttributes(entity.attributes, attrTypes, entity.uri);

        return Promise.resolve(result);
    },

    validateAttributes: function (attributes, attrTypes, parentUri) {
        return attrTypes
            .map(function (attrType) {
                var values = attributes[attrType.name] || [],
                    errors = [];

                var hasNonEmptyValues = values.some(function (val) {
                    return val.value && val.value.toString().trim() !== '' && !MobileUI.core.entity.EntityUtils.isEmptyAttribute(val.value);
                });
                if (attrType.required && !hasNonEmptyValues) {
                    errors.push({
                        invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED,
                        attrType: attrType,
                        value: i18n('Requires a value'),
                        objectParentUri: parentUri
                    });
                }

                if (attrType.type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.URL) {
                    errors = errors.concat(values.filter(function (val) {
                        return val.value && !MobileUI.core.util.Util.isLinkValid(val.value.toString());
                    }).map(function (val) {
                        return {
                            invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE,
                            attrType: attrType,
                            value: i18n('Value is incorrect'),
                            objectParentUri: parentUri,
                            objectUri: val.uri
                        };
                    }));
                } else if (attrType.type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.NUMBER || attrType.type === MobileUI.components.editablelist.ControlsFactory.attributeTypes.INT) {
                    errors = errors.concat(values.filter(function (val) {
                        return val.value && !MobileUI.core.util.Util.isValidInteger(val.value.toString());
                    }).map(function (val) {
                        return {
                            invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE,
                            attrType: attrType,
                            value: i18n('Value is incorrect'),
                            objectParentUri: parentUri,
                            objectUri: val.uri
                        };
                    }));
                }

                if (attrType.attributes) {
                    errors = errors.concat(values
                        .map(function (value) {
                            return this.validateAttributes(value.value, attrType.attributes, value.uri);
                        }, this)
                        .reduce(function (a, b) {
                            return a.concat(b);
                        }, []));
                }

                return errors;
            }, this)
            .reduce(function (a, b) {
                return a.concat(b);
            }, [])
            .filter(function (err) {
                return err;
            });
    },

    getEntityForDL: function (entity) {
        var result = Ext.clone(entity);
        var fixReferenceValue = function (attrValue) {
            if (!attrValue.refEntity) {
                return;
            }

            var uri = MobileUI.core.entity.EntityUtils.tmpUriFromApiFormat(attrValue.refEntity.objectURI);

            if (!MobileUI.core.entity.EntityUtils.isTemporaryUri(uri)) {
                return;
            }

            var tmpEntity = this.getEntityForDL(MobileUI.core.entity.TemporaryEntity.getEntity(uri));
            for (var k in tmpEntity.attributes) {
                if (tmpEntity.attributes.hasOwnProperty(k)) {
                    attrValue.value[k] = tmpEntity.attributes[k];
                }
            }
        };
        Ext.Object.getValues(result.attributes)
            .filter(function (attrValues) {
                return attrValues.length && attrValues[0].refEntity;
            })
            .reduce(function (a, b) {
                return a.concat(b);
            }, [])
            .forEach(fixReferenceValue, this);
        return result;
    },

    validateOnService: function (entity) {
        var entityType = MobileUI.core.Metadata.getEntityType(entity.type);

        if (MobileUI.core.entity.EntityUtils.isTemporaryUri(entity.uri)) {
            return this.validateNewEntity(this.getEntityForValidation(entity), entityType);
        } else {
            return this.validateAttributesDiff(entity.uri, entityType);
        }
    },

    validateEmptyEntity: function (entity) {
        var clone = Ext.clone(entity);
        this.cleanAttributesForValidation(clone.attributes);

        if (Object.keys(clone.attributes).length === 0) {
            return Promise.resolve([{
                invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE,
                value: i18n('Please fill out some fields')
            }]);
        } else {
            return Promise.resolve([]);
        }
    }
});
