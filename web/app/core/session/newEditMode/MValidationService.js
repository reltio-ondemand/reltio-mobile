/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.newEditMode.MValidationService', {
    statics: {
        VALIDATE_ENTITY_URL: '/validate/{0}/entity?options=preserveURIs,validateDataPermissions',
        VALIDATE_CHANGES_URL: '/validate/{0}/entity/{1}?options=preserveURIs,updateAttributeUpdateDates,validateDataPermissions',
        VALIDATE_ENTITY_URL_WITHOUT_DATA_PERMISSION: '/validate/{0}/entity?options=preserveURIs',
        VALIDATE_CHANGES_URL_WITHOUT_DATA_PERMISSION: '/validate/{0}/entity/{1}?options=preserveURIs,updateAttributeUpdateDates'
    },

    allowDataPermissionValidation: function(){
        var cm = MobileUI.core.Services.getConfigurationManager();
        var extension = cm.getExtensionById('com.reltio.plugins.ui.AllowDataPermissionValidation');
        return !extension || extension.isAllow;
    },
    validateAttributesDiff: function (uri, entityType) {
        var id = uri.split('/')[1],
            tenant = MobileUI.core.Services.getTenantName();
        var MVS = MobileUI.core.session.newEditMode.MValidationService;
        var canCreate = MobileUI.core.PermissionManager.securityService().metadata.canCreate(entityType);
        var validateDataPermissions = this.allowDataPermissionValidation() && canCreate;
        return this.sendValidationRequestWithCheck(
            validateDataPermissions ? MVS.VALIDATE_CHANGES_URL: MVS.VALIDATE_CHANGES_URL_WITHOUT_DATA_PERMISSION, [tenant, id],
            {
                method: 'POST',
                data: this.getAttributesDiff()
            },
            entityType,
            this.getModifiedEntity()
        );
    },

    validateNewEntity: function (entity, entityType) {
        var tenant = MobileUI.core.Services.getTenantName();
        var MVS = MobileUI.core.session.newEditMode.MValidationService;
        var canCreate = MobileUI.core.PermissionManager.securityService().metadata.canCreate(entityType);
        var validateDataPermissions = this.allowDataPermissionValidation() && canCreate;
        return this.sendValidationRequestWithCheck(
            validateDataPermissions ? MVS.VALIDATE_ENTITY_URL : MVS.VALIDATE_ENTITY_URL_WITHOUT_DATA_PERMISSION, [tenant],
            {
                method: 'POST',
                data: entity
            },
            entityType,
            entity
        );
    },

    sendValidationRequestWithCheck: function (url, params, data, entityType, entity) {
        return new Promise(function (resolve) {
            var servicePath = MobileUI.core.Services.getGeneralSettings().validationServicePath,
                validationEnabled = entityType.cardinality || (entityType.lifecycleActions && entityType.lifecycleActions.validate && entityType.lifecycleActions.validate.length),
                useDependentLookups = Object.keys(this.getLookupsManager().getCurrentLookupsHierarchy()).length > 0;

            if (servicePath && (validationEnabled || useDependentLookups)) {
                MobileUI.core.session.Session.sendRequest(servicePath + url, params, data, function (result) {
                    resolve(result.map(this.createValidationResponseFunction(entity), this));
                }, function (error) {
                    MobileUI.core.Logger.log(error);
                    resolve([]);
                }, this);
            } else {
                resolve([]);
            }
        }.bind(this));
    },

    getAttributesDiff: function () {
        var diff = Ext.clone(this.getEntityAttributesDiff(true)),
            entity = this.getModifiedEntity();

        var crosswalks = entity.crosswalks;
        crosswalks = this.collectCrosswalksFromAttributes(entity.attributes, crosswalks);

        diff = this.applyCrosswalks(diff, crosswalks);

        return diff.map(function (item) {
            if (Ext.isArray(item.newValue)) {
                item.newValue
                    .map(this.transformAttributeUri, this)
                    .forEach(this.fixReferenceAttrValue, this);
            }

            return item;
        }, this);
    },

    getEntityForValidation: function (entity) {
        var result = Ext.clone(entity);

        this.cleanAttributesForValidation(result.attributes);
        Ext.Object.getValues(result.attributes)
            .map(this.transformAttributeUri, this)
            .filter(function (attrValues) {
                return attrValues.length && attrValues[0].refEntity;
            })
            .reduce(function (a, b) {
                return a.concat(b);
            }, [])
            .forEach(this.fixReferenceAttrValue, this);
        result = this.transformAttributeUri(result);
        return result;
    },

    cleanAttributesForValidation: function (attributes) {
        for (var a in attributes) {
            if (attributes.hasOwnProperty(a)) {
                /*jshint loopfunc: true */
                attributes[a] = attributes[a].filter(function (val) {
                    return val.value !== undefined && val.value !== '' || val.refEntity;
                });
                attributes[a].forEach(function (val) {
                    if (Ext.isObject(val.value)) {
                        this.cleanAttributesForValidation(val.value);
                    }
                }, this);
                if (!attributes[a].length) {
                    delete attributes[a];
                }
                /*jshint loopfunc: false */
            }
        }
    },

    transformAttributeUri: function (attrValue) {
        if (Ext.isArray(attrValue)) {
            attrValue.forEach(this.transformAttributeUri, this);
        } else {
            if (attrValue.uri) {
                attrValue.uri = MobileUI.core.entity.EntityUtils.tmpUriToApiFormat(attrValue.uri);
            }

            if (attrValue.refEntity && attrValue.refEntity.objectURI) {
                attrValue.refEntity.objectURI = MobileUI.core.entity.EntityUtils.tmpUriToApiFormat(attrValue.refEntity.objectURI);
            }
            if (attrValue.refRelation && attrValue.refRelation.objectURI) {
                attrValue.refRelation.objectURI = MobileUI.core.entity.EntityUtils.tmpUriToApiFormat(attrValue.refRelation.objectURI);
            }

            if (Ext.isObject(attrValue.value)) {
                [].concat.apply([], Ext.Object.getValues(attrValue.value)).forEach(this.transformAttributeUri, this);
            }
        }

        return attrValue;
    },

    fixReferenceAttrValue: function (attrValue) {
        if (!attrValue.refEntity) {
            return;
        }

        var uri = MobileUI.core.entity.EntityUtils.tmpUriFromApiFormat(attrValue.refEntity.objectURI);

        if (!MobileUI.core.entity.EntityUtils.isTemporaryUri(uri)) {
            return;
        }

        var tmpEntity = this.getEntityForValidation(MobileUI.core.entity.TemporaryEntity.getEntity(uri));
        for (var k in tmpEntity.attributes) {
            if (tmpEntity.attributes.hasOwnProperty(k)) {
                attrValue.value[k] = tmpEntity.attributes[k];
            }
        }
    }
});