/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.session.newEditMode.MWorkflow', {
    statics: {
        URL_CHANGE_REQUEST_REVIEW: '/workflow/{0}/processInstances'
    },

    sendWorkflowRequest: function (url, params, options) {
        var workflowPath = MobileUI.core.Services.getGeneralSettings().workflowPath;
        var workflowUrl = workflowPath + url;

        return new Promise(function (resolve, reject) {
            if (workflowPath) {
                MobileUI.core.session.Session.sendRequest(workflowUrl, params, options, resolve, reject);
            } else {
                reject('workflowPath not specified');
            }
        });
    },

    sendChangeRequestToReview: function (changeRequestUri, entitiesUris, comment) {
        var options = {
            method: 'POST',
            data: {
                processType: 'dataChangeRequestReview',
                objectURIs: [changeRequestUri].concat(entitiesUris)
            },
            headers: {
                EnvironmentURL: MobileUI.core.Services.getEnvironmentUrl()
            }
        };

        if (comment && comment.trim()) {
            options.data.comment = comment;
        }

        return this.sendWorkflowRequest(
            MobileUI.core.session.newEditMode.MWorkflow.URL_CHANGE_REQUEST_REVIEW,
            [MobileUI.core.Services.getTenantName()],
            options);
    }
});