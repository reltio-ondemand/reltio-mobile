/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.Cookie', {
    statics: {
        /**
         * Returns the string value of a cookie.
         *
         * @param key {String} The key for the saved string value.
         * @return {null | String} Returns the saved string value, if the cookie
         *    contains a value for the key, <code>null</code> otherwise.
         */
        get: function (key) {
            var start = document.cookie.indexOf(key + '='),
                len = start + key.length + 1,
                end;

            if ((!start) && (key !== document.cookie.substring(0, key.length))) {
                return null;
            }

            if (start === -1) {
                return null;
            }

            end = document.cookie.indexOf(';', len);

            if (end === -1) {
                end = document.cookie.length;
            }

            return decodeURI(document.cookie.substring(len, end));
        },


        /**
         * Sets the string value of a cookie.
         *
         * @param key {String} The key for the string value.
         * @param value {String} The string value.
         * @param expires {Number?null} The expires in days starting from now,
         *    or <code>null</code> if the cookie should deleted after browser close.
         * @param path {String?null} Path value.
         * @param domain {String?null} Domain value.
         * @param secure {Boolean?null} Secure flag.
         * @return {void}
         */
        set: function (key, value, expires, path, domain, secure) {
            // Generate cookie
            var cookie = [key, '=', encodeURI(value)];

            if (expires) {
                var today = new Date();
                today.setTime(today.getTime());

                cookie.push(';expires=', new Date(today.getTime() + (expires * 1000 * 60 * 60 * 24)).toGMTString());
            }

            if (path) {
                cookie.push(';path=', path);
            }

            if (domain) {
                cookie.push(';domain=', domain);
            }

            if (secure) {
                cookie.push(';secure');
            }

            // Store cookie
            document.cookie = cookie.join('');
        },


        /**
         * Deletes the string value of a cookie.
         *
         * @param key {String} The key for the string value.
         * @param path {String?null} Path value.
         * @param domain {String?null} Domain value.
         * @return {void}
         */
        del: function (key, path, domain) {
            if (!MobileUI.core.util.Cookie.get(key)) {
                return;
            }
            
            // Generate cookie
            var cookie = [key, '='];
            if (path) {
                cookie.push('; path=', path);
            }
            if (domain) {
                cookie.push('; domain=', domain);
            }
            cookie.push('; expires=Thu, 01-Jan-1970 00:00:01 GMT');
            // Store cookie
            document.cookie = cookie.join('');
        }
    }
});