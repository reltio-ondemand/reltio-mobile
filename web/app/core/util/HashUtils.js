/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.HashUtil', {
    statics: {
        STATE_DIVIDER: '_',
        PARAMS_DIVIDER: '~',
        STATE_PERSPECTIVE: 'p',
        STATE_TABID: 'w',
        VALUE_SEPARATOR: '$',


        /**
         * Method parse the hash string and return state
         * @param hash {String} hash of the
         * @returns {{}}
         */
        stateFromHash: function (hash)
        {
            var state = {},
                pairs, pair, args,
                i, type, matcher,
                replaceSeparator;

            if (hash)
            {
                replaceSeparator = function (item)
                {
                    return item.replace(matcher, '/');
                };
                pairs = hash.split(this.STATE_DIVIDER);
                matcher = new RegExp('\\' + this.VALUE_SEPARATOR, 'g');
                for (i = 0; i < pairs.length; i++)
                {
                    pair = pairs[i];
                    args = pair.split(this.PARAMS_DIVIDER);
                    if (args.length > 0)
                    {
                        type = args[0];
                        args.splice(0, 1);
                        args = args.map(replaceSeparator);
                        state[type] = args;
                    }
                }
            }

            return state;
        },
        /**
         * Method converts the state to hash string
         * @param state {object} the map with state
         * @returns {string} hash string
         */
        stateToHash: function (state)
        {
            var stateString = '',
                type, args, params,
                i, separator, replaceSeparator;
            if (state)
            {
                separator = this.VALUE_SEPARATOR;
                replaceSeparator = function (item)
                {
                    return item.replace(/\//g, separator);
                };
                for (type in state)
                {
                    if (state.hasOwnProperty(type))
                    {
                        args = [type];
                        params = state[type];
                        if (typeof params === 'string')
                        {
                            params = [params];
                        }
                        params = params.map(replaceSeparator);

                        if (params && params.length > 0)
                        {
                            args = args.concat(params);
                        }

                        for (i = 0; i < args.length; i++)
                        {
                            if (args[i] === null)
                            {
                                args[i] = '';
                            }
                        }

                        if (stateString !== '')
                        {
                            stateString += this.STATE_DIVIDER;
                        }
                        stateString += args.join(this.PARAMS_DIVIDER);
                    }
                }
            }
            return stateString;
        },

        encodeUri: function(uri) {
            return uri.replace(/\//g, MobileUI.core.util.HashUtil.VALUE_SEPARATOR);
        }
    }
});
