/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.Storage', {
    statics: {
        /**
         * Method returns item from the storage
         * @param key {String} item key
         * @param isForSession {Boolean?} is for session
         * @returns {Object | null}
         */
        getItem: function (key, isForSession)
        {
            var value = null;
            if (window.localStorage)
            {
                if (isForSession === undefined)
                {
                    value = window.localStorage.getItem(key) || window.sessionStorage.getItem(key);
                }
                else if (isForSession)
                {
                    value = window.sessionStorage.getItem(key);
                }
                else
                {
                    value = window.localStorage.getItem(key);
                }
            }

            try
            {
                value = value && JSON.parse(value);
            }
            catch (err)
            {
                //Prevent app crashing when migrated from old saved format
            }
            return value;
        },

        /**
         * Method adds value to the storage
         * @param key {String} item key
         * @param value {Object} item value
         * @param forSession {Boolean?} is for session
         */
        setItem: function (key, value, forSession)
        {
            var storage;
            if (forSession === undefined)
            {
                forSession = false;
            }

            storage = forSession ? window.sessionStorage : window.localStorage;

            if (storage)
            {
                if (!Ext.isString(value))
                {
                    value = JSON.stringify(value);
                }
                try
                {
                    storage.setItem(key, value);
                }
                catch (e)
                {
                    storage = window.virtualStorage;
                    storage.setItem(key, value);
                }
            }

        },
        /**
         * Method removes item from the storage
         * @param key {String} item key
         */
        removeItem: function (key)
        {
            if (window.localStorage && window.sessionStorage)
            {
                window.localStorage.removeItem(key);
                window.sessionStorage.removeItem(key);
            }
        }
    }
});