/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.Util', {
    statics: {
        int64MaxValue: '9223372036854775807',
        int64MinValue: '-9223372036854775808',

        geopositionTimeout: 50,
        /**
         * Method convert array to object using the @separator as devider
         * E.g.:
         * var arr = ['key'='value','key1'='value1'];
         * the result will be: {key:value, key1:value1}
         * @param array {Array} array to convert
         * @param separator {String?} separator string, default value '='
         * @returns {Object}
         */
        convertArrayToObject: function (array, separator)
        {
            var i, item, result = {};
            separator = separator || '=';
            if (Ext.isArray(array))
            {
                for (i = 0; i < array.length; i++)
                {
                    item = array[i].split(separator);
                    if (item.length === 2)
                    {
                        result[item[0].trim()] = item[1].trim();
                    }
                }
            }
            return result;
        },
        /**
         * Method preloads url
         * @param url {String} url
         */
        preloadImage: function (url)
        {
            var image = new Image();
            image.src = url;
            return image;
        },
        /**
         * Method generates uuid
         * @param size {Number?} default size 4
         * @returns {string}
         */
        generateUUID: function (size)
        {
            var result = '',
                i,
                alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            size = size || 4;
            for (i = 0; i < size; i++)
            {
                result += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
            }
            return result;
        },

        /**
         * Method which is return hash of the string
         * @param str {String}
         * @returns {String}
         */
        getHash: function (str)
        {
            var i,
                chr,
                hash = 0;
            if (!str || str.length === 0)
            {
                return String(hash);
            }
            for (i = 0; i < str.length; i++)
            {
                chr = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0;
            }
            return String(hash);
        },
        /**
         * Method creates url from parameter
         * @param url {String} url string
         * @param mapParams {Object} key-value
         * @returns {String}
         */
        createRequestUrl: function (url, mapParams)
        {
            url = url || '';
            if (mapParams)
            {
                if (url.indexOf('?') === -1)
                {
                    url += '?';
                }
                else
                {
                    url += '&';
                }
                url += this.toParameter(mapParams);
            }
            return url;
        },
        /**
         * Serializes an object to URI parameters (also known as query string).
         * @param obj {Object} Object to serialize.
         * @param post {Boolean} Whether spaces should be encoded with '+'.
         * @return {String}
         */
        toParameter: function (obj, post)
        {
            var key,
                parts = [];

            for (key in obj)
            {
                if (obj.hasOwnProperty(key))
                {
                    var value = obj[key];
                    if (Ext.isArray(value))
                    {
                        for (var i = 0; i < value.length; i++)
                        {
                            this.toParameterPair(key, value[i], parts, post);
                        }
                    }
                    else
                    {
                        this.toParameterPair(key, value, parts, post);
                    }
                }
            }

            return parts.join('&');
        },
        /**
         * Encodes key/value to URI safe string and pushes to given array.
         * @private
         * @param key {String} Key.
         * @param value {String} Value.
         * @param parts {Array} Array to push to.
         * @param post {Boolean} Whether spaces should be encoded with '+'.
         */
        toParameterPair: function (key, value, parts, post)
        {
            var encode = window.encodeURIComponent;
            if (post)
            {
                parts.push(encode(key).replace(/%20/g, '+') + '=' +
                           encode(value).replace(/%20/g, '+'));
            }
            else
            {
                parts.push(encode(key) + '=' + encode(value));
            }
        },
        /**
         * Method merges two objects
         * @param parent {Object} parent object
         * @param child  {Object} child object
         * @param ignoreProperties {Array?} ignored properties
         * @param replace {Boolean?} replace property or not
         */
        merge: function (parent, child, ignoreProperties, replace)
        {
            parent = parent || {};
            child = child || {};
            for (var p in parent)
            {
                if (parent.hasOwnProperty(p))
                {

                    if (ignoreProperties && Ext.Array.contains(ignoreProperties, p))
                    {
                        continue;
                    }

                    if (replace || child[p] === undefined)
                    {
                        child[p] = Ext.clone(parent[p]);
                    }
                    else if (Ext.isObject(parent[p]) && Ext.isObject(child[p]))
                    {
                        MobileUI.core.util.Util.merge(parent[p], child[p]);
                    }
                }
            }
        },
        mergeWithArrayInternal: function(source, target){
            var destination = (target || []).slice();

            if(Ext.isArray(source)){
                destination = source.concat(target || []);
            }
            return destination;
        },

        mergeWithArray: function (source, target) {
            var destination = {};
            Object.keys(target).forEach(function (key) {
                destination[key] = Ext.clone(target[key]);
            });
            Object.keys(source).forEach(function (key) {
                if (!target[key]) {
                    destination[key] = Ext.clone(source[key]);
                } else {
                    destination[key] = MobileUI.core.util.Util.mergeWithArrayInternal(target[key], source[key]);
                }
            });
            return destination;
        },



        /**
         * Check whether the string ends with the given substring
         * @param fullstr {String} the string to search in
         * @param substr {String} the substring to look for
         * @return {Boolean} whether the string ends with the given substring
         */
        endsWith : function(fullstr, substr) {
            fullstr = fullstr || '';
            substr = substr || '';
            return fullstr.substring(fullstr.length - substr.length, fullstr.length) === substr;
        },
        /**
         * Method flatten given array
         * @param arr {Array} initial array of arrays
         * @return {Array} flatten array
         */
        flatten: function (arr) {
            var me = this;
            return arr.reduce(function (flat, toFlatten) {
                return flat.concat(Array.isArray(toFlatten) ? me.flatten(toFlatten) : toFlatten);
            }, []);
        },
        /**
         * Method check the equality
         * @param obj1 {*} first param
         * @param obj2 {*} second param
         * @returns {boolean}
         */
        equals: function (obj1, obj2)
        {

            if (obj1 === obj2 || (Ext.isEmpty(obj1) && Ext.isEmpty(obj2)))
            {
                return true;
            }

            if ((Ext.isEmpty(obj1) && !Ext.isEmpty(obj2)) || (!Ext.isEmpty(obj1) && Ext.isEmpty(obj2)))
            {
                return false;
            }


            if (typeof obj1 !== typeof obj2)
            {
                return false;
            }

            if (Ext.isArray(obj1))
            {
                if (obj1.length !== obj2.length)
                {
                    return false;
                }

                obj1 = Ext.Array.clone(obj1).sort();
                obj2 = Ext.Array.clone(obj2).sort();
                return obj1.every(function (item, index)
                {
                    return MobileUI.core.util.Util.equals(item, this[index]);
                }, obj2);
            }

            if (Ext.isObject(obj1))
            {
                if (Object.keys(obj1).length !== Object.keys(obj2).length)
                {
                    return false;
                }
                for (var key in obj1)
                {
                    if (obj1.hasOwnProperty(key))
                    {
                        if (!MobileUI.core.util.Util.equals(obj1[key], obj2[key]))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }

            return obj1 === obj2;
        },
        /**
         * Method converts the text to link
         * @param text {String} link test
         * @param toHtmlTag {Boolean?} to fullLink
         * @returns {*}
         */
        convertToLink: function (text, toHtmlTag)
        {
            var url1 = /(^|&lt;|\s)(www\..+?\..+?)(\s|&gt;|$)/g,
                url2 = /(^|&lt;|\s)(((https?|ftps?):\/\/|mailto:).+?)(\s|&gt;|$)/g,
                result = text.trim();
            if (result)
            {
                if (toHtmlTag)
                {
                    result = result
                        .replace(url1, '$1<a  target="_blank"  href="http://$2">$2</a>$3')
                        .replace(url2, '$1<a  target="_blank"  href="$2">$2</a>$5');
                }
                else
                {
                    result = result.replace(url1, '$2').replace(url2, '$2');
                }
            }
            return result;
        },

        isLinkValid: function(link) {
            return (/^((https?|ftps?|smb):\/\/|mailto:|data:|www).+/i).test(link);
        },

        /**
         * Method executes the func for each item in obj
         * @param obj
         * @param func
         * @param self
         * @param deep
         */
        forEach: function (obj, func, self, deep, ignored)
        {
            var i, key;
            if (!obj || !func)
            {
                return;
            }
            if (!self)
            {
                self = window;
            }
            ignored = ignored || [];
            deep = !!deep;
            if (Ext.isArray(obj))
            {
                for (i = 0; i < obj.length; i++)
                {
                    func.call(self, i, obj[i], obj);
                    if (deep)
                    {
                        MobileUI.core.util.Util.forEach(obj[i], func, self, deep, ignored);
                    }
                }
            }
            else if (Ext.isObject(obj))
            {
                for (key in obj)
                {
                    if (obj.hasOwnProperty(key))
                    {
                        func.call(self, key, obj[key], obj);
                        if (deep && ignored.indexOf(key) === -1)
                        {
                            MobileUI.core.util.Util.forEach(obj[key], func, self, deep, ignored);
                        }
                    }
                }
            }
        },

        blurField: function() {
            if (document.activeElement) {
                document.activeElement.blur();
            }
        },
        filter: function(obj, condition, context)
        {
            var result = {}, key;
            if (Ext.isObject(obj))
            {
                for (key in obj)
                {
                    if (obj.hasOwnProperty(key))
                    {
                        if (condition.call(context || window, obj[key], key))
                        {
                            result[key] = Ext.clone(obj[key]);
                        }
                    }
                }
            }
            return result;
        },
        some: function(obj, condition, context)
        {
            var key;
            if (Ext.isObject(obj))
            {
                for (key in obj)
                {
                    if (obj.hasOwnProperty(key))
                    {
                        if (condition.call(context || window, obj[key], key))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        },

        locateGeoposition: function(address, session, callback, self)
        {
            var Util = MobileUI.core.util.Util;
            if(!Util.geopositionQueue) {
                Util.geopositionQueue = [];
            }
            Util.geopositionQueue.push({address: address, callback: callback, self: self});

            if(!Util.geopositionTimer)
            {
                Util.locateGeopositionTimerCall(session);
            }
        },

        locateGeopositionTimerCall: function(session)
        {
            var Util = MobileUI.core.util.Util;
            Util.geopositionTimer = null;
            if(Util.geopositionQueue.length === 0) {
                return;
            }

            Util.geopositionTimer = setTimeout(function () {
                var geo = Util.geopositionQueue.shift();
                if (geo) {
                    Util.locateGeopositionInternal(geo.address, session, geo.callback, geo.self);
                }
            }, Util.geopositionTimeout);
        },

        locateGeopositionInternal: function(address, session, callback, self)
        {
            var Util = MobileUI.core.util.Util;
            if (!Util.geopositionServerSpecification) {
                var conf = MobileUI.core.Services.getConfigurationManager().getExtensionById('com.reltio.plugins.ui.GeoServerSpecification');
                Util.geopositionServerSpecification = (conf && conf.configuration) || 'maps.google.com';
            }

            if(Util.geopositionServerSpecification === 'maps.google.com')
            {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK)
                    {
                        if (results.length > 0)
                        {
                            var result = results[0];
                            if(result.geometry && result.geometry.location)
                            {
                                callback.call(self, null, result.geometry.location.lat(), result.geometry.location.lng());
                                Util.locateGeopositionTimerCall(session);

                                return;
                            }
                        }
                        status = 'Service returned empty coordinates';
                        MobileUI.core.Logger.warn('Geocode was not successful for the following reason: ' + status);
                        callback.call(self, status);
                        Util.geopositionTimeout = 50;
                    }
                    else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT)
                    {
                        Util.geopositionQueue.push({address: address, callback: callback, self: self});
                        Util.geopositionTimeout += 100;
                    }
                    else
                    {
                        MobileUI.core.Logger.warn('Geocode was not successful for the following reason: ' + status);
                        callback.call(self, status);
                        Util.geopositionTimeout = 50;
                    }

                    Util.locateGeopositionTimerCall(session);

                });
            }
            else if(Util.geopositionServerSpecification === 'saas.loqate.com')
            {
                MobileUI.core.Services.asyncRequest(
                    'loqate',
                    function(json)
                    {
                        if (json.status === 'OK')
                        {
                            if (json.results.length > 0)
                            {
                                var result = json.results[0],
                                    lat = result.Latitude,
                                    lng = result.Longitude;
                                if (!isNaN(lat) && !isNaN(lng))
                                {
                                    callback.call(self, null, lat, lng);
                                    Util.locateGeopositionTimerCall(session);

                                    return;
                                }
                            }
                            json.status = 'Service returned empty coordinates';
                        }

                        MobileUI.core.Logger.warn('Geocode was not successful for the following reason: ' + json.status);
                        callback.call(self, json.status);

                        Util.locateGeopositionTimerCall(session);
                    },callback,
                    {
                        method: 'POST',
                        data: {
                            addr: JSON.stringify(address),
                            user: MobileUI.core.session.Session.getUser().full_name
                        },
                        signer: MobileUI.core.session.Session
                    },
                    Util);
            }
            else {
                callback.call(self, 'Unsupported server specification: ' + Util.geopositionServerSpecification);
            }
        },

        splitUri: function (str) {
            var options = {
                key: ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'anchor'],
                query: {
                    name: 'queryKey',
                    parser: /(?:^|&)([^&=]*)=?([^&]*)/g
                },
                parser: {
                    /*eslint no-useless-escape: "off"*/
                    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/
                }
            };

            var match = options.parser['strict'].exec(str),
                uri = {},
                i = 14;

            while (i--) {
                uri[options.key[i]] = match[i] || '';
            }
            uri[options.query.name] = {};
            uri[options.key[12]].replace(options.query.parser, function (match, replacerTemplate, replacerTemplate2) {
                if (replacerTemplate) {
                    uri[options.query.name][replacerTemplate] = replacerTemplate2;
                }
            });
            return uri;
        },

        generateArithmeticSequences: function(length){
            return Array.apply(null, {length: length}).map(Number.call, Number);
        },

        cloneEntity: function(attributes, deprecatedFields){
            deprecatedFields = deprecatedFields || ['editor', 'valueChangedContext'];
            var i, destination, key;

            if (attributes === null || attributes === undefined) {
                return attributes;
            }

            var type = Object.prototype.toString.call(attributes);

            if (type === '[object Date]') {
                return Ext.clone(attributes);
            }

            if (type === '[object Array]') {
                i = attributes.length;
                destination = [];
                while (i--) {
                    destination[i] = MobileUI.core.util.Util.cloneEntity(attributes[i]);
                }
            }
            // Objects
            else if (type === '[object Object]' && attributes.constructor === Object) {
                destination = {};

                for (key in attributes) {
                    if (deprecatedFields.indexOf(key) === -1){
                        destination[key] = MobileUI.core.util.Util.cloneEntity(attributes[key]);
                    }
                }
            }

            return destination || attributes;

        },

        replace: function(source, what, str){
            if (source && source.indexOf(what) > -1) {
                var index = source.indexOf(what);
                return source.substring(0, index) + str + source.substring(index + what.length);
            }
            return source;
        },
        redirect: function(url){
            document.location.href = url;
        },
        supportSSO: function() {
            var oauthMethod = location.search.match(/[?&]oauthMethod=([^&]*)/i);
            oauthMethod = (oauthMethod && oauthMethod[1]);
            var settings = MobileUI.core.Services.getGeneralSettings();
            return oauthMethod !== 'reltio' && settings.sso && settings.sso.length > 0;
        },

        getCurrentUrl: function(){
            return document.location.toString();
        },

        getLogoutURL: function () {
            var settings = MobileUI.core.Services.getGeneralSettings();
            if (this.supportSSO()) {
                var ssoUri = MobileUI.core.util.Util.splitUri(settings.sso);
                var location = MobileUI.core.util.Util.getCurrentUrl().replace(/index\.html/, '');
                return ssoUri.protocol + '://' + ssoUri.authority + '/logout?redirect_uri=' + encodeURIComponent(location);
            }
        },

        getTenantNotExistsURL: function () {
            var settings = MobileUI.core.Services.getGeneralSettings();
            if (this.supportSSO()) {
                var ssoUri = MobileUI.core.util.Util.splitUri(settings.sso);
                var location = MobileUI.core.util.Util.getCurrentUrl().replace(/index\.html/, '');
                return ssoUri.protocol + '://' + ssoUri.authority + '/error?id=nonexistentTenant&client_id=mobile&tenant=' + MobileUI.core.Services.getTenantName() + '&redirect_uri=' + encodeURIComponent(location);
            }
        },

        isValidInteger: function (value) {
            function compareAbs(a, b) {
                if (a.length !== b.length) {
                    return a.length > b.length ? 1 : -1;
                }
                for (var i = a.length - 1; i >= 0; i--) {
                    if (a[i] !== b[i]) {
                        return a[i] > b[i] ? 1 : -1;
                    }
                }
                return 0;
            }
            var max = this.int64MaxValue,
                min = this.int64MinValue;
            var correct = value.indexOf('-') === 0 ? compareAbs(min.substring(1), value.substring(1)) >= 0 : compareAbs(max, value) >= 0;
            return correct && /^-?\d*$/.test(value);
        },

        isThrottlingError: function (response) {
            var status = response.status;
            if (status === 503) {
                var text = response.responseText;
                try {
                    return JSON.parse(text).errorMessage === 'quota_exceeded';
                } catch (e) {
                    return false;
                }
            }
            return false;
        }
    }
});
