/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.Validator', {
    statics: {
        types: {
            url: 'url',
            integer: 'integer',
            floatingPoint: 'floatingPoint',
            dollar: 'dollar'
        },

        isValid: function(value, valueType) {
            var valid = (value !== null) && (value !== undefined);

            switch (valueType) {
                case MobileUI.core.util.Validator.types.url:
                    valid = valid && (value === '') || MobileUI.core.util.Util.isLinkValid(value) ||
                        (value.toLowerCase().indexOf('www.') === 0);
                    break;
                case MobileUI.core.util.Validator.types.integer:
                    valid = valid && !isNaN(Number(value)) && (parseInt(value, 10).toString() === value);
                    break;
                case MobileUI.core.util.Validator.types.floatingPoint:
                    valid = valid && !isNaN(Number(value)) && (parseFloat(value).toString() === value);
                    break;
                case MobileUI.core.util.Validator.types.dollar:
                    if (valid) {
                        var format = i18n(MobileUI.core.I18n.DOLLAR_FIELD);
                        var parsed = MobileUI.core.I18n.parseNumber(value, MobileUI.core.I18n.DOLLAR_FIELD);

                        valid = valid && !isNaN(parsed);
                        if (valid && (format.digitsAfterDecimalSeparator !== undefined)) {
                            var pointPos = value.indexOf(format.decimalSeparator || '.');
                            valid = (pointPos === -1) || (pointPos >= value.length - 1 - (format.digitsAfterDecimalSeparator || 2));
                        }
                    }
                    break;
            }

            return valid;
        }
    }
});
