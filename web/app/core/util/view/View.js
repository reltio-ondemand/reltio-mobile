/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.core.util.view.View', {
    singleton: true,
    /**
     * @private
     */
    cache: [],

    createView: function (item)
    {

        var limit = 20,
            view, i,
            cache = this.cache;

        for (i = 0; i < cache.length; i++)
        {
            if (cache[i].viewName === item)
            {
                return cache[i];
            }
        }

        if (cache.length >= limit)
        {
            cache.forEach(function (view)
            {
                if (!view.isPainted())
                {
                    view.destroy();
                }
            });
            cache = cache.filter(function(view){
                return view.isPainted();
            });
        }

        view = Ext.create(item);
        view.viewName = item;
        cache.push(view);
        return view;
    }

});