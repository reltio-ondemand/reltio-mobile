/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.plugins.PullRefresh', {
    extend: 'Ext.Component',
    alias: 'plugin.reltiopullrefresh',
    requires: ['Ext.DateExtras'],
    config: {
        width: '100%',

        /**
         * @cfg {String} pullText The text that will be shown while you are pulling down.
         * @accessor
         */
        pullText: 'Pull down to refresh...',
        component: null,
        /**
         * @cfg {String} releaseText The text that will be shown after you have pulled down enough to show the release message.
         * @accessor
         */
        releaseText: 'Release to refresh...',

        /**
         * @cfg {String} loadingText The text that will be shown while the list is refreshing.
         * @accessor
         */
        loadingText: 'Loading...',

        /**
         * @cfg {String} loadedText The text that will be when data has been loaded.
         * @accessor
         */
        loadedText: 'Loaded.',

        /**
         * @cfg {Number} snappingAnimationDuration The duration for snapping back animation after the data has been refreshed
         * @accessor
         */
        snappingAnimationDuration: 300,
        /**
         * @cfg {Number} overpullSnapBackDuration The duration for snapping back when pulldown has been lowered further then its height.
         */
        overpullSnapBackDuration: 300,

        /**
         * @cfg {Ext.XTemplate/String/Array} pullTpl The template being used for the pull to refresh markup.
         * Will be passed a config object with properties state, message and updated
         *
         * @accessor
         */
        pullTpl: [
            '<div class="x-list-pullrefresh-arrow"></div>',
            '<div class="spinner"><div class="uil-default-css" style="-webkit-transform:scale(0.16)">' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(0deg) translate(0,-60px);transform:rotate(0deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(30deg) translate(0,-60px);transform:rotate(30deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(60deg) translate(0,-60px);transform:rotate(60deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(90deg) translate(0,-60px);transform:rotate(90deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(120deg) translate(0,-60px);transform:rotate(120deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(150deg) translate(0,-60px);transform:rotate(150deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(180deg) translate(0,-60px);transform:rotate(180deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(210deg) translate(0,-60px);transform:rotate(210deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(240deg) translate(0,-60px);transform:rotate(240deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(270deg) translate(0,-60px);transform:rotate(270deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(300deg) translate(0,-60px);transform:rotate(300deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '<div style="top:80px;left:93px;width:14px;height:40px;background:#666666;-webkit-transform:rotate(330deg) translate(0,-60px);transform:rotate(330deg) translate(0,-60px);border-radius:10px;position:absolute;"></div>' +
            '</div></div>',
            '<span class="x-loading-top"></span>',
            '<span class="x-loading-right"></span>',
            '<span class="x-loading-bottom"></span>',
            '<span class="x-loading-left"></span>',
            '</div>',
            //'<div class="x-list-pullrefresh-wrap">',
            //'<h3 class="x-list-pullrefresh-message">{message}</h3>',
            '</div>'
        ].join(''),

        translatable: true
    },

    // @private
    $state: 'pull',
    // @private
    getState: function ()
    {
        return this.$state;
    },
    // @private
    setState: function (value)
    {
        this.$state = value;
        this.updateView();
    },
    // @private
    $isSnappingBack: false,
    // @private
    getIsSnappingBack: function ()
    {
        return this.$isSnappingBack;
    },
    // @private
    setIsSnappingBack: function (value)
    {
        this.$isSnappingBack = value;
    },

    // @private
    init: function (component)
    {
        var me = this;

        me.setComponent(component);
        me.initScrollable();
    },

    getElementConfig: function ()
    {
        return {
            reference: 'element',
            classList: ['x-unsized'],
            children: [
                {
                    reference: 'innerElement',
                    className: Ext.baseCSSPrefix + 'list-pullrefresh'
                }
            ]
        };
    },

    // @private
    initScrollable: function ()
    {
        var me = this,
            autoRefresh,
            component = me.getComponent(),
            scrollable = component.getScrollable(),
            scroller;

        if (!scrollable)
        {
            return;
        }

        scroller = scrollable.getScroller();
        autoRefresh = !Ext.isFunction(component.onPullRefresh);
        scroller.setAutoRefresh(autoRefresh);

        me.lastUpdated = new Date();

        component.insert(0, me);

        scroller.on({
            scroll: me.onScrollChange,
            scope: me
        });

        this.updateView();
    },

    // @private
    applyPullTpl: function (config)
    {
        if (config instanceof Ext.XTemplate)
        {
            return config;
        }
        else
        {
            return new Ext.XTemplate(config);
        }
    },

    // @private
    updateComponent: function (newComponent, oldComponent)
    {
        var me = this;

        if (newComponent && newComponent !== oldComponent)
        {
            newComponent.on({
                order: 'after',
                scrollablechange: me.initScrollable,
                scope: me
            });
        }
        else if (oldComponent)
        {
            oldComponent.un({
                order: 'after',
                scrollablechange: me.initScrollable,
                scope: me
            });
        }
    },

    // @private
    getPullHeight: function ()
    {
        return this.innerElement.getHeight();
    },

    /**
     * @private
     * Attempts to load the newest data
     */
    fetchLatest: function ()
    {
        if (this.getComponent().onPullRefresh)
        {
            this.getComponent().onPullRefresh(this.onLatestFetched, this);
        }
        else if (this.getComponent().fireEvent)
        {
            this.getComponent().fireEvent('pullRefresh');
            this.onLatestFetched();
        }
        else
        {
            this.onLatestFetched();
        }
    },

    /**
     * @private
     * Called after fetchLatest has finished grabbing data. Matches any returned records against what is already in the
     * Store. If there is an overlap, updates the existing records with the new data and inserts the new items at the
     * front of the Store. If there is no overlap, insert the new records anyway and record that there's a break in the
     * timeline between the new and the old records.
     */
    onLatestFetched: function ()
    {
        this.setState('loaded');
        this.snapBack();
        //todo: remove when the update will be implemented
        this.onSnapBackEnd();
    },

    /**
     * Snaps the Component back to the top after a pullrefresh is complete
     * @param {Boolean=} force Force the snapback to occur regardless of state {optional}
     */
    snapBack: function (force)
    {

        if (this.getState() !== 'loaded' && force !== true)
        {
            return;
        }

        var component = this.getComponent(),
            scroller = component.getScrollable().getScroller();

        scroller.refresh();
        scroller.minPosition.y = 0;

        scroller.on({
            scrollend: this.onSnapBackEnd,
            single: true,
            scope: this
        });

        this.setIsSnappingBack(true);
        scroller.scrollTo(null, 0, {duration: this.getSnappingAnimationDuration()});
    },

    /**
     * @private
     * Called when PullRefresh has been snapped back to the top
     */
    onSnapBackEnd: function ()
    {
        this.setState('pull');
        this.setIsSnappingBack(false);
    },

    /**
     * @private
     * Called when the Scroller updates from the list
     * @param scroller
     * @param x
     * @param y
     */
    onScrollChange: function (scroller, x, y)
    {
        if (y <= 0)
        {
            var pullHeight = this.getPullHeight(),
                isSnappingBack = this.getIsSnappingBack();

            if (this.getState() === 'loaded' && !isSnappingBack)
            {
                this.snapBack();
            }

            if (this.getState() !== 'loading' && this.getState() !== 'loaded')
            {
                if (-y >= pullHeight + 10)
                {
                    this.setState('release');
                    scroller.getContainer().onBefore({
                        dragend: 'onScrollerDragEnd',
                        single: true,
                        scope: this
                    });
                }
                else if ((this.getState() === 'release') && (-y < pullHeight + 10))
                {
                    this.setState('pull');
                    scroller.getContainer().unBefore({
                        dragend: 'onScrollerDragEnd',
                        single: true,
                        scope: this
                    });
                }
            }
            if (this.getComponent().getStore)
            {
                this.getTranslatable().translate(0, -y);
            }
        }
    },

    /**
     * @private
     * Called when the user is done dragging, this listener is only added when the user has pulled far enough for a refresh
     */
    onScrollerDragEnd: function ()
    {
        if (this.getState() !== 'loading')
        {
            var component = this.getComponent(),
                scroller = component.getScrollable().getScroller(),
                translateable = scroller.getTranslatable();


            this.setState('loading');
            translateable.setEasingY({duration: this.getOverpullSnapBackDuration()});
            scroller.minPosition.y = -this.getPullHeight();
            scroller.on({
                scrollend: 'fetchLatest',
                single: true,
                scope: this
            });
        }
    },

    /**
     * @private
     * Updates the content based on the PullRefresh Template
     */
    updateView: function ()
    {
        var state = this.getState(),
            templateConfig = {state: state, updated: ''},
            stateFn = state.charAt(0).toUpperCase() + state.slice(1).toLowerCase(),
            fn = 'get' + stateFn + 'Text';

        if (this[fn] && Ext.isFunction(this[fn]))
        {
            templateConfig.message = this[fn].call(this);
        }

        this.innerElement.removeCls(['loaded', 'loading', 'release', 'pull'], Ext.baseCSSPrefix + 'list-pullrefresh');
        this.innerElement.addCls(this.getState(), Ext.baseCSSPrefix + 'list-pullrefresh');
        this.getPullTpl().overwrite(this.innerElement, templateConfig);
    }
});