/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.plugins.SwipeToDelete', {
    extend: 'Ext.Component',
    alias: 'plugin.swipetodelete',
    requires: ['Ext.Anim'],
    config: {
        animation: {duration: 250, easing: {type: 'ease-out'}},
        list: null,
        swipeRightButtons: null,
        swipeLeftButtons: [],
        minStep: 15,
        scrollTolerance: 5,
        openWhenPosition: 150,
        actionsBackground: '#ffffff',
        filterButtons: null,
        useMenu: false
    },
    panel: null,
    actions: null,
    scrolling: false,
    actualRecord: null,
    listScrollConfig: null,
    scrollableComponent: null,
    startX: null,
    init: function (list)
    {
        this.setList(list);
        if (!this.getSwipeRightButtons()) {
            this.setSwipeRightButtons([{
                label: i18n('Delete'),
                ui: 'reltio-swipe-delete',
                action: 'delete'
            }]);
        }
        if (!this.getUseMenu()) {
            list.on({
                itemtap: this.onTap,
                updatedata: this.onUpdateData,
                itemtouchmove: this.onTouchMove,
                itemtouchstart: this.onTouchStart,
                hide: this.removeButtons,
                scope: this
            });
            list.on('painted', function() {
                this.scrollableComponent = (list.getScrollable().getScroller().getDisabled() ? list.up('panel') : list) || list;
                this.scrollableComponent.getScrollable().getScroller().on({
                    scroll: this.onScrollStart,
                    scrollend: this.onScrollEnd,
                    scope: this
                });
            }, this, {single: true});
        }
    },
    /**
     * On tap listener
     * @returns {boolean}
     */
    onTap: function ()
    {
        var stop = this.panel !== null;
        this.removeButtons();

        if (stop)
        {
            return false;
        }
    },
    /**
     * On scroll end listener
     */
    onScrollEnd: function ()
    {
        this.scrolling = false;
    },
    /**
     * On Scroll start listener
     * @param scroller {Object} scroller
     * @param x {Number} x position
     * @param y {Number} y position
     */
    onScrollStart: function (scroller, x, y)
    {
        if (Math.abs(y) > this.getScrollTolerance()) {
            if (this.panel) {
                this.removeButtons(false);
            }
            this.scrolling = true;
        }
    },
    /**
     * Method return true if the direction aalow
     * @param direction {String} left or right
     * @returns {boolean}
     */
    isDirectionAllow: function (direction, record)
    {
        var filterButtonFn = this.getFilterButtons(),
            filterButtons = function(button) {
                return !record || !filterButtonFn || filterButtonFn(button.id, record);
            };
        return (direction === 'left' && !Ext.isEmpty(this.getSwipeRightButtons().filter(filterButtons))) ||
               (direction === 'right' && !Ext.isEmpty(this.getSwipeLeftButtons().filter(filterButtons)));
    },
    onTouchStart: function(list, index, target, record, e)
    {
      this.startX = e.pageX;
    },
    /**
     * On touch move listener
     * @param list {elist} list
     * @param index {Number} record index
     * @param target {Ext.Element} target element
     * @param record {Object} model record
     * @param e {Object} event
     * @param eOpts {Object} options
     * @returns {boolean}
     */
    onTouchMove: function (list, index, target, record, e)
    {
        var direction;
        if (this.scrolling || record.get('disableSwipe'))
        {
            return false;
        }
        if (this.startX > e.pageX)
        {
            direction = 'right';
        } else if (this.startX < e.pageX)
        {
            direction = 'left';
        }
        if (direction === 'right' && Ext.isEmpty(this.getSwipeRightButtons()) ||
            direction === 'left' && Ext.isEmpty(this.getSwipeLeftButtons()))
        {
             return false;
        }

        var input = target.element.down('input');
        if (input && input.dom)
        {
            return false;
        }

        var element = target.element.down('.x-innerhtml'),
            initialOffset = {x: 0, y: 0},
            that = this;

        if (this.panel && this.panel.getElement() !== element)
        {
            this.removeButtons();
        }

        if (!this.panel)
        {
            this.actualRecord = record;
            this.panel = new Ext.util.Draggable({
                element: element,
                constraint: false,
                direction: 'horizontal',
                listeners: {
                    dragstart: function (self)
                    {
                        if (self.getOffset().x === -1 * that.getOpenWhenPosition())
                        {
                            initialOffset = {x: -1 * that.getOpenWhenPosition(), y: 0};
                        }
                    },
                    drag:
                    {
                        fn: function (self, e, newX)
                        {
                            var dragDirection;
                            if (that.actions === null)
                            {
                                dragDirection = initialOffset.x > newX ? 'left' : 'right';

                                if (that.isDirectionAllow(dragDirection, record))
                                {
                                    that.actions = that.createButtonsContainer(target.element, dragDirection, record);
                                }
                                else {
                                    e.preventDefault();
                                    return false;
                                }
                            }
                            if (newX < 5 && Ext.isEmpty(that.getSwipeRightButtons()) ||
                                newX > 5 && Ext.isEmpty(that.getSwipeLeftButtons()))
                            {
                                e.preventDefault();
                                return false;
                            }
                            if (Math.min(initialOffset.x, newX) - Math.max(initialOffset.x, newX) > -1 * that.getMinStep())
                            {
                                self.setOffset(self.getOffset().x, 0);
                            }
                            else
                            {
                                list.getScrollable().getScroller().fireEvent('scrollend');
                                if (that.listScrollConfig === null)
                                {
                                    that.listScrollConfig = {
                                        direction: that.scrollableComponent.getScrollable().getScroller().getDirection(),
                                        disabled: that.scrollableComponent.getScrollable().getScroller().getDisabled()
                                    };
                                }
                                (that.scrollableComponent || list).setScrollable(false);
                            }
                        },
                        order: 'before'
                    },
                    dragend: function (self)
                    {
                        var direction = self.getOffset().x > 0 ? 'right': 'left';
                        if (!that.isDirectionAllow(direction))
                        {
                            self.setOffset(0, 0, that.getAnimation());
                            Ext.destroy(that.actions);
                            Ext.destroy(that.panel);
                            that.panel = null;
                            that.actions = null;
                            that.actualRecord = null;
                        }
                        else
                        {
                            if (self.getOffset().x < -1 * (that.getOpenWhenPosition() / 2))
                            {
                                self.setOffset(-1 * that.getOpenWhenPosition(), 0, that.getAnimation());
                            }
                            else if (self.getOffset().x > (that.getOpenWhenPosition() / 2))
                            {
                                self.setOffset(that.getOpenWhenPosition(), 0, that.getAnimation());
                            }
                            else
                            {
                                self.setOffset(0, 0, that.getAnimation());
                                Ext.destroy(that.actions);
                                Ext.destroy(that.panel);
                                that.panel = null;
                                that.actions = null;
                                that.actualRecord = null;
                            }
                        }
                        setTimeout(function ()
                        {
                            if (that.listScrollConfig !== null && that.scrollableComponent !== null){
                                that.scrollableComponent.setScrollable(that.listScrollConfig);
                            }
                        }, 250);
                    }
                }
            });
        }

    },
    /**
     * Method creates button container
     * @param element {Ext.Element}
     * @param direction {String} direction
     * @returns {*}
     */
    createButtonsContainer: function (element, direction, record)
    {
        var outer = Ext.DomHelper.insertFirst(element, '<div class="x-slide-action-outer" style="background: ' + this.getActionsBackground() + '; position:absolute; width: 100%; height: 100%"></div>', true),
            buttons, marginLeft,
            enabledButtons,
            filterButtonFn = this.getFilterButtons(),
            filterButtons = function(button) {
                return !filterButtonFn || filterButtonFn(button.id, record);
            },
            convertButtons = function (button)
            {
                var btn = Ext.create('Ext.Button', {
                    text: button.label || '',
                    cls: (button.ui || '') + ' reltio-swipe-button',
                    style: 'height: 99%;border: none;box-shadow: none;z-index: auto;',
                    flex: 1,
                    width: button.width || 150,
                    handler: function (btn, e)
                    {
                        var me = this.self;
                        e.preventDefault();
                        e.stopPropagation();
                        me.removeButtons(false);
                        me.getList().fireEvent(this.action, this.record);
                    },
                    scope: {self: this, action: button.action, record: this.actualRecord}
                });
                return btn;
            };
        if (direction === 'left')
        {
            enabledButtons = this.getSwipeRightButtons().filter(filterButtons);
            buttons = enabledButtons.map(convertButtons, this);
            marginLeft = 'auto';

            this.setOpenWhenPosition(Math.max(150,
                enabledButtons.reduce(function(width, button) {
                    return width + parseFloat(button.width || 0);
                }, 0)));
        }
        else
        {
            enabledButtons = this.getSwipeLeftButtons().filter(filterButtons);
            buttons = enabledButtons.map(convertButtons, this);
            marginLeft = '0';

            this.setOpenWhenPosition(Math.max(150,
                enabledButtons.reduce(function(width, button) {
                    return width + parseFloat(button.width || 0);
                }, 0)));
        }

        Ext.create('Ext.Panel', {
            layout: 'hbox',
            width: this.getOpenWhenPosition(),
            cls: 'x-slide-action-buttons-outer',
            renderTo: outer,
            style: 'margin-left: ' + marginLeft + ';height: 100%;',
            items: buttons
        });

        return outer;
    },
    /**
     * On update data listener
     */
    onUpdateData: function ()
    {
        if (this.panel)
        {
            this.removeButtons();
        }
    },
    /**
     * Method removes button with animation
     * @param timeout
     */
    removeButtons: function (timeout)
    {
        if (timeout === undefined)
        {
            timeout = true;
        }

        if (this.panel)
        {
            if (this.panel.getElement() === undefined)
            {
                this.panel = null;
                this.actualRecord = null;
                this.actions = null;
            }
            else
            {
                var panel = this.panel,
                    actions;
                panel.setOffset(0, 0, this.config.animation);
                actions = this.actions;
                setTimeout(function ()
                {
                    Ext.destroy(actions);
                    Ext.destroy(panel);
                }, timeout ? 500 : 0);
                this.panel = null;
                this.actualRecord = null;
                this.actions = null;
            }
        }
        if (this.getList() && !this.getList().getScrollable())
        {
            this.getList().getScroller().fireEvent('scrollend');
            if (this.listScrollConfig !== null && this.scrollableComponent !== null)
            {
                this.scrollableComponent.setScrollable(this.listScrollConfig);
            }
        }
    }
});
