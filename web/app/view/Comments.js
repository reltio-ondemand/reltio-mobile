/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Comments', {
    extend: 'Ext.form.Panel',
    xtype: 'commentsview',
    requires: [
        'Ext.form.FieldSet',
        'MobileUI.components.EditableList'
    ],
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('History')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        name: 'comments-closeButton',
                        text: i18n('Close'),
                        cls: 'text-button'
                    }
                ]
            },
            {
                xtype: 'elist',
                flex: 1,
                name: 'comments-list',
                defaultType: 'eDataItem',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ]);
    }
});
