/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.ComplexAttributes', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.EditableList',
        'MobileUI.components.Crumb',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.ReltioToolbar'
    ],
    config: {
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    xtype: 'complexview',
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: true,
                cls: 'reltio-profile',
                name: 'complexattributes',
                plugins: [
                    'reltiopullrefresh'
                ],
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'complexProfileBand'
                    },
                    {
                        xtype: 'crumb',
                        name: 'crumb'
                    },
                    {
                        xtype: 'elist',
                        scrollable: false,
                        height: 0,
                        itemId: 'complexAttributesList',
                        name: 'complexAttributesList',
                        allowFetch: true,
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
