/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.CustomView', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.ReltioToolbar',
        'MobileUI.components.EditableList',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.components.CustomViewComponent'
    ],
    xtype: 'customview',
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: 'vertical',
                name: 'customViewPanel',
                cls: 'reltio-profile',
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'customViewProfileBand'
                    },
                    {
                        xtype: 'customviewcomponent',
                        flex: 1
                    }
                ]
            }
        ]);
    }
});
