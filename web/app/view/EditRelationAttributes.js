/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.EditRelationAttributes', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.ReltioToolbar',
        'MobileUI.components.EditableList',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.plugins.SwipeToDelete'
    ],
    xtype: 'editrelationattributesview',
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        cls: 'text-button',
                        name: 'cancelEditRelation'

                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'editRelationTitle'
                    },
                    {
                        xtype: 'spacer'
                    },

                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'save-relation-button',
                        text: i18n('Save')
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                name: 'notificationToolbar',
                hidden: true,
                items: [

                    {
                        xtype: 'component',
                        name: 'notificationMessage',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        name: 'closeNotificationBtn',
                        cls: 'icon-button'
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: 'vertical',
                name: 'editRelationAttributes',
                cls: 'reltio-profile editrelationattributesdialog',
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'editProfileBand'
                    },
                    {
                        xtype: 'elist',
                        scrollable: false,
                        height: 0,
                        itemId: 'editRelationAttributesList',
                        name: 'editRelationAttributesList',
                        allowFetch: true,
                        plugins: [{type: 'swipetodelete', useMenu: true }],
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
