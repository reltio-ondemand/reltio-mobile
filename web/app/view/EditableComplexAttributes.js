/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.EditableComplexAttributes', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.EditableList',
        'MobileUI.components.Crumb',
        'MobileUI.components.FacetProfileBand'
    ],
    xtype: 'editablecomplexview',
    config: {
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'backToolbarBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Edit Attribute'),
                        name: 'headerToolbarLabel'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'text-button',
                        text: i18n('Done'),
                        name: 'doneToolbarBtn'
                    }

                ]
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: true,
                cls: 'reltio-profile',
                name: 'complexattributes',
                items: [
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    },
                    {
                        xtype: 'facetprofileband',
                        name: 'complexProfileBand'
                    },
                    {
                        xtype: 'crumb',
                        name: 'crumb'
                    },
                    {
                        xtype: 'elist',
                        scrollable: false,
                        height: 0,
                        itemId: 'complexAttributesList',
                        name: 'complexAttributesList',
                        plugins: [{type: 'swipetodelete', useMenu: true }],
                        allowFetch: true,
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
