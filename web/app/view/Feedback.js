/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Feedback', {
    extend: 'Ext.form.Panel',
    xtype: 'feedbackview',
    requires: [
        'MobileUI.components.FileUpload',
        'MobileUI.components.Selectbox',
        'MobileUI.components.Email'
    ],
    config: {
        layout: 'vbox',
        cls: 'feedback',
        scrollable: true,
        style: 'background-color: white',
        name: 'feedbackview-root',
        items: []
    },

    initialize: function () {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        xtype: 'button',
                        name: 'feedback-cancel',
                        cls: 'text-button',
                        text: i18n('Cancel')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Send Feedback')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'text-button',
                        name: 'feedback-send',
                        text: i18n('Send'),
                        disabled: true
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                name: 'feedback-notificationToolbar',
                hidden: true,
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                items: [
                    {
                        xtype: 'component',
                        name: 'feedback-notificationMessage',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        name: 'feedback-closeNotificationBtn',
                        cls: 'icon-button'
                    }
                ]
            },
            {
                xtype: 'reltio-selectbox',
                placeHolder: i18n('Reason'),
                name: 'reason',
                label: i18n('Regarding'),
                cls: ['feedback-field-input', 'required-field']
            },
            {
                xtype: 'component',
                name: 'reason-separator',
                cls: 'feedback-separator',
                height: '1px'
            },

            {
                xtype: 'reltio-emailfield',
                name: 'email',
                label: i18n('CCs (Emails)'),
                cls: ['feedback-field-input']
            },
            {
                xtype: 'component',
                cls: 'feedback-separator',
                height: '1px'
            },
            {
                xtype: 'textfield',
                label: i18n('Subject'),
                name: 'summary',
                cls: ['feedback-field-input', 'required-field']
            },
            {
                xtype: 'component',
                cls: 'feedback-separator',
                height: '1px'
            },

            {
                xtype: 'reltio-selectbox',
                label: i18n('Priority'),
                name: 'priority',
                options: [
                    {value: 'urgent', text: i18n('Urgent')},
                    {value: 'high', text: i18n('High')},
                    {value: 'normal', text: i18n('Normal')},
                    {value: 'low', text: i18n('Low')}
                ],
                cls: ['feedback-field-input', 'required-field']
            },
            {
                xtype: 'component',
                name: 'priority-separator',
                cls: 'feedback-separator',
                height: '1px'
            },
            {
                xtype: 'textareafield',
                scrollable: {direction: 'vertical', directionLock: true},
                name: 'description',
                label: i18n('Description'),
                cls: 'required-field',
                labelAlign: 'top',
                minHeight: Ext.os.is.Phone ? '100px' : '150px',
                clearIcon: false,
                maxRows: Ext.os.is.Phone ? 5 : 15,
                maxHeight: Ext.os.is.Phone ? '120px' : ''
            },
            {
                xtype: 'component',
                name: 'description-separator',
                cls: 'feedback-separator',
                height: '1px'
            },
            {
                label: i18n('Steps to reproduce'),
                labelAlign: 'top',
                xtype: 'textareafield',
                cls: 'required-field',
                scrollable: {direction: 'vertical', directionLock: true},
                name: 'stepstoreproduce',
                hidden: true,
                minHeight: Ext.os.is.Phone ? '100px' : '150px',
                clearIcon: false,
                maxRows: Ext.os.is.Phone ? 5 : 15,
                maxHeight: Ext.os.is.Phone ? '120px' : ''
            },
            {
                xtype: 'component',
                name: 'stepstoreproduce-separator',
                hidden: true,
                cls: 'feedback-separator',
                height: '1px'
            },

            {
                xtype: 'textareafield',
                scrollable: {direction: 'vertical', directionLock: true},
                name: 'actualResults',
                cls: 'required-field',
                label: i18n('Actual results'),
                labelAlign:'top',
                minHeight: Ext.os.is.Phone ? '100px' : '150px',
                hidden: true,
                clearIcon: false,
                maxRows: Ext.os.is.Phone ? 5 : 15,
                maxHeight: Ext.os.is.Phone ? '120px' : ''
            },
            {
                xtype: 'component',
                name: 'actualResults-separator',
                hidden: true,
                cls: 'feedback-separator',
                height: '1px'
            },

            {
                xtype: 'textareafield',
                scrollable: {direction: 'vertical', directionLock: true},
                name: 'expectedResults',
                minHeight: Ext.os.is.Phone ? '100px' : '150px',
                hidden: true,
                cls: 'required-field',
                label: i18n('Expected results'),
                labelAlign:'top',
                clearIcon: false,
                maxRows: Ext.os.is.Phone ? 5 : 15,
                maxHeight: Ext.os.is.Phone ? '120px' : ''
            },
            {
                xtype: 'component',
                name: 'expectedResults-separator',
                hidden: true,
                cls: 'feedback-separator',
                height: '1px'
            },
            {
                xtype: 'container',
                layout: 'hbox',
                name: 'didItWork',

                hidden: true,
                cls: 'feedback-boolean-container',
                items: [
                    {
                        xtype: 'label',
                        tag: 'span',
                        cls: 'required-field',
                        html: i18n('Did it work before?'),
                        flex: 1
                    },
                    {
                        xtype: 'togglefield',
                        name: 'didItWork-value'
                    }
                ]
            },
            {
                xtype: 'component',
                name: 'didItWork-separator',
                hidden: true,
                cls: 'feedback-separator',
                height: '1px'
            },
            {
                xtype: 'textfield',
                label: i18n('Business Impact'),
                name: 'businessImpact',
                hidden: true,
                cls: ['feedback-field-input', 'required-field']
            },
            {
                xtype: 'component',
                name: 'businessImpact-separator',
                cls: 'feedback-separator',
                hidden: true,
                height: '1px'
            },
            {
                label: i18n('Due Date'),
                name: 'duedate',
                cls: ['feedback-field-input', 'required-field'],
                hidden: true,
                xtype: 'textfield',
                component: {type: 'date'}
            },
            {xtype: 'spacer', flex: 1},
            {
                xtype: 'panel',
                name: 'imagepanel',
                cls: 'image-panel',
                scrollable: 'vertical',
                height: 110,
                hidden: true,
                items: []
            },
            {
                xtype: 'panel',
                cls: 'photo-panel',
                items: [
                    {
                        xtype: 'fileupload',
                        cls: 'fileupload',
                        icon: 'resources/images/reltio/feedback/photo.svg',
                        width: 35,
                        name: 'file'
                    }
                ]
            }
        ]);
    }
});
