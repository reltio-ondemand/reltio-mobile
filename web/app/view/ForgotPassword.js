/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.ForgotPassword', {
    extend: 'Ext.form.Panel',
    xtype: 'forgotpasswordview',
    requires: ['Ext.form.FieldSet', 'Ext.Label', 'Ext.Img','MobileUI.components.Recaptcha'],
    config: {
        cls: ['reltio-credentials','reltio-restore-info'],
        fullscreen: true,
        name: 'forgot-password',
        scrollable: 'vertical',
        layout: {
            type: 'vbox',
            align: 'center'
        },
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'spacer'
            },
            {
                xtype: 'image',
                width: 250,
                height: 80,
                cls: 'reltio-login-logo',
                src: 'resources/images/logo.png'
            },
            {
                height: 30
            },
            {
                xtype: 'container',
                width: 302,
                cls: 'restore-message',
                minHeight: 60,
                flex: 1,
                scrollable: {
                    direction: 'vertical',
                    directionLock: true
                },
                html: i18n('To reset your password, enter your username or e-mail address that you use to login to your account.')
            },
            {
                xtype: 'container',
                cls: 'reltio-form-fieldset',
                width: 302,
                items: [
                    {
                        xtype: 'textfield',
                        inputCls: 'reltio-input',
                        placeHolder: i18n('User Name or e-mail address'),
                        autoCorrect: false,
                        autoCapitalize : false,
                        itemId: 'userNameTextField',
                        name: 'userNameTextField-forgot',
                        clearIcon: true,
                        required: true
                    },

                    {
                        xtype: 'recaptcha',
                        cls: 'x-field-input'
                    }
                ]
            },
            {
                xtype: 'button',
                cls: 'submit-button',
                itemId: 'resetButton',
                name: 'resetButton',
                ui: 'plain',
                width: 302,
                height: 52,
                style: {
                    marginTop: '13px'
                },
                text: i18n('Reset password')
            },
            {
                xtype: 'button',
                cls: 'transparent-button',
                name: 'forgotpassword-cancelButton',
                ui: 'plain',
                width: 302,
                height: 52,
                style: {
                    marginTop: '10px'
                },
                text: i18n('Cancel')
            },
            {
                xtype: 'spacer'
            }
        ]);
    }
});
