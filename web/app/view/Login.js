/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'loginview',
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'MobileUI.core.Services'],
    config: {
        fullscreen: true,
        cls: 'reltio-credentials',
        scrollable: null,

        layout: {
            type: 'vbox',
            align: 'center'
        },
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'spacer'
            },
            {
                xtype: 'image',
                width: 250,
                height: 80,
                style: {
                    margin: '0 0 42px 0'
                },
                cls: 'reltio-login-logo',
                src: 'resources/images/logo.png'
            },
            {
                xtype: 'container',
                cls: 'reltio-form-fieldset',
                width: 290,
                height: 101,
                items: [
                    {
                        xtype: 'textfield',
                        inputCls: 'input-text',
                        autoCorrect: false,
                        autoCapitalize : false,
                        placeHolder: i18n('User Name'),
                        itemId: 'userNameTextField',
                        name: 'userNameTextField',
                        clearIcon: true,
                        required: true
                    },

                    {
                        xtype: 'passwordfield',
                        inputCls: 'input-text',
                        placeHolder: i18n('Password'),
                        itemId: 'passwordTextField',
                        name: 'passwordTextField',
                        clearIcon: true,
                        required: true
                    }

                ]
            },
            {
                xtype: 'button',
                cls: 'submit-button',
                itemId: 'logInButton',
                name: 'logInButton',
                ui: 'plain',
                width: 290,
                height: 52,
                style: {
                    marginTop: '13px'
                },
                text: i18n('Log In'),
                iconAlign: 'right',
                iconCls: 'reltio-icon-spinner-small'
            },
            {
                xtype: 'button',
                itemId: 'forgotPwd',
                cls: 'transparent-button',
                name: 'forgotPwdButton',
                ui: 'plain',
                width: 290,
                height: 52,
                style: {
                    marginTop: '10px'
                },
                text: i18n('Forgot your password?')
            },
            {
                xtype: 'button',
                itemId: 'policy',
                cls: 'transparent-button',
                name: 'policyButton',
                ui: 'plain',
                width: 290,
                height: 52,
                text: i18n('Privacy Policy')
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'container',
                width: '100%',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [
                    {
                        xtype: 'button',
                        itemId: 'feedback',
                        name: 'feedbackButton',
                        cls: 'transparent-button',
                        ui: 'plain',
                        //width: 150,
                        height: 52,
                        hidden: MobileUI.core.Services.getGeneralSettings().hideSendFeedback,
                        style: {
                            marginBottom: 0,
                            marginRight: 0,
                            paddingLeft: '16px',
                            paddingRight: '16px'
                        },
                        text: i18n('Send feedback')
                    }
                ]
            }
        ]);
    }
});
