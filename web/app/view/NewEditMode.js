/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.NewEditMode', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.ProfileFacets',
        'MobileUI.components.ProfileBand',
        'MobileUI.components.EditableList',
        'MobileUI.components.LoadMaskEx'
    ],
    xtype: 'neweditmodeview',
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'newEditModeCancelToolbarBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        name: 'title',
                        cls: 'header',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        text: i18n('Save'),
                        name: 'newEditModeSaveToolbarBtn',
                        cls: 'text-button'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                name: 'new-notificationToolbar',
                hidden: true,
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                items: [
                    {
                        xtype: 'component',
                        name: 'new-notificationMessage',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        name: 'new-closeNotificationBtn',
                        cls: 'icon-button'
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: 'vertical',
                name: 'editableProfile',
                cls: 'reltio-profile',
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'newEditModeProfileBand'
                    },
                    {
                        xtype: 'elist',
                        defaultType: 'reltioListItem',
                        scrollable: false,
                        height: 0,
                        itemId: 'newEditModeAttributesList',
                        name: 'newEditModeAttributesList',
                        plugins: [{type: 'swipetodelete', useMenu: true }],
                        store: {
                            data: [],
                            model: 'MobileUI.components.editablelist.Model'
                        }
                    }
                ]
            }
        ]);
    }
});
