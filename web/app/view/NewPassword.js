/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.NewPassword', {
    extend: 'Ext.form.Panel',
    xtype: 'newpasswordview',
    requires: ['Ext.Label', 'Ext.Img'],
    config: {
        cls: ['reltio-credentials', 'reltio-restore-info'],
        fullscreen: true,
        layout: {
            type: 'vbox',
            align: 'center'
        },
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'spacer'
            },
            {
                xtype: 'image',
                width: 250,
                height: 80,
                cls: 'reltio-login-logo',
                src: 'resources/images/logo.png'
            },
            {
                xtype: 'container',
                cls: 'reltio-form-fieldset',
                width: 290,
                height: 160,
                items: [
                    {
                        height: 50
                    },
                    {
                        xtype: 'passwordfield',
                        inputCls: 'reltio-input',
                        placeHolder: i18n('Enter new password'),
                        autoCorrect: false,
                        autoCapitalize: false,
                        itemId: 'newPasswordField',
                        name: 'newPasswordField',
                        clearIcon: true,
                        required: true
                    },
                    {
                        xtype: 'passwordfield',
                        inputCls: 'reltio-input',
                        placeHolder: i18n('Re-Enter new password'),
                        autoCorrect: false,
                        autoCapitalize: false,
                        itemId: 're-newPasswordField',
                        name: 're-newPasswordField',
                        clearIcon: true,
                        required: true
                    }
                ]
            },
            {
                xtype: 'button',
                cls: 'submit-button',
                itemId: 'back',
                name: 'submitButton',
                ui: 'plain',
                width: 290,
                height: 52,
                style: {
                    marginTop: '13px'
                },
                text: i18n('Reset password')
            },
            {
                xtype: 'spacer'
            }
        ]);
    }
});
