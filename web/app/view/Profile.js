/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Profile', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.ProfileFacets',
        'MobileUI.components.ProfileBand',
        'MobileUI.components.EditableList',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.components.ReltioToolbar'
    ],
    xtype: 'profileview',
    config: {
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: 'vertical',
                name: 'profile',
                cls: 'reltio-profile',
                plugins: [
                    'reltiopullrefresh'
                ],
                items: [
                    {
                        xtype: 'profileband',
                        name: 'profileBand',
                        hidden: Ext.os.is.Phone
                    },
                    {
                        xtype: 'facetprofileband',
                        name: 'facetProfileBand',
                        hidden: true
                    },
                    {
                        xtype: 'profilefacets',
                        name: 'profileFacets',
                        hidden: true
                    },
                    {
                        xtype: 'elist',
                        scrollable: false,
                        height: 0,
                        itemId: 'attributesList',
                        name: 'attributesList',
                        allowFetch: true,
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
