/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.RelationComplexAttributes', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.EditableList',
        'MobileUI.components.Crumb',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.ReltioToolbar'
    ],
    xtype: 'relationcomplexattributesview',
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        icon: 'resources/images/reltio/toolbar/menu.svg',
                        name: 'menuBtn',
                        cls: 'icon-button'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/back.svg',
                        name: 'backToolbarBtn',
                        cls: 'icon-button',
                        hidden: true
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'relationComplexTitle'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        text: i18n('Done'),
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: true,
                cls: 'reltio-profile',
                plugins: [
                    'reltiopullrefresh'
                ],
                name: 'relationcomplexattributesview',
                items: [
                    {
                        xtype: 'crumb',
                        name: 'relationComplexCrumb'
                    },
                    {
                        xtype: 'elist',
                        scrollable: false,
                        height: 0,
                        itemId: 'complexAttributesList',
                        name: 'relationComplexAttributesList',
                        allowFetch: true,
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
