/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Relations', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.RelationsFacet',
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.EditableList',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.plugins.SwipeToDelete',
        'MobileUI.components.ReltioToolbar'
    ],
    xtype: 'relationsview',
    config: {
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                name: 'relations',
                layout: 'vbox',
                scrollable: null,
                cls: 'reltio-profile',
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'relationsProfileBand'
                    },
                    {
                        xtype: 'relationsfacet',
                        name: 'relationsFacets',
                        hidden: true
                    },
                    {
                        xtype: 'elist',
                        scrollable: true,
                        flex: 1,
                        plugins: [
                            'reltiopullrefresh',
                            {
                                type: 'swipetodelete',
                                swipeRightButtons: [{
                                    label: i18n('Edit'),
                                    ui: 'reltio-swipe-change',
                                    action: 'editRelation',
                                    id: 'editRelation',
                                    width: 70
                                }, {
                                    label: i18n('Delete'),
                                    ui: 'reltio-swipe-delete',
                                    action: 'delete',
                                    id: 'delete',
                                    width: 70
                                }, {
                                    label: i18n('Accept relationship'),
                                    ui: 'reltio-swipe-edit',
                                    action: 'addRelation',
                                    id: 'addRelation',
                                    width: 70
                                }],
                                filterButtons: function(id, record) {
                                    var allowedElements = record.get('allowedSwipeElements');
                                    if (Ext.isArray(allowedElements)) {
                                        return allowedElements.indexOf(id) !== -1;
                                    }
                                    return true;
                                }
                            }
                        ],
                        defaultType: 'eDataItem',
                        itemId: 'relationsList',
                        name: 'relationsList',
                        allowDelete: true,
                        allowFetch: true,
                        scrollToTopOnRefresh: false,
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
