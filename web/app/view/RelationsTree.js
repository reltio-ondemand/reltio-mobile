/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.RelationsTree', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.components.FacetProfileBand',
        'MobileUI.components.EditableList',
        'MobileUI.components.RelationsTreeFacet',
        'MobileUI.components.ReltioTree',
        'MobileUI.components.LoadMaskEx',
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.ReltioToolbar'
    ],
    xtype: 'relationstreeview',
    config: {
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                name: 'relations-tree',
                cls: 'reltio-profile',
                items: [
                    {
                        xtype: 'facetprofileband',
                        name: 'relationsTreeProfileBand'
                    },
                    {
                        xtype: 'relationstreefacet',
                        name: 'relationsTreeFacet'
                    },
                    {
                        xtype: 'reltiotree',
                        name: 'relationsTree',
                        scrollToTopOnRefresh: false,
                        cls: 'treelist',
                        scrollable: 'vertical',
                        editing: false,
                        infinite: true,
                        variableHeights: true,
                        useHeaders: false,
                        allowFetch: false,
                        allowRefresh: false,
                        plugins: [
                            'reltiopullrefresh',
                            {
                                type: 'swipetodelete',
                                swipeRightButtons: [{
                                    label: i18n('Add Parent'),
                                    ui: 'reltio-swipe-change',
                                    action: 'addParent',
                                    id: 'addParent',
                                    width: 70
                                }, {
                                    label: i18n('Add Child'),
                                    ui: 'reltio-swipe-edit',
                                    action: 'addChild',
                                    id: 'addChild',
                                    width: 70
                                }, {
                                    label: i18n('Delete'),
                                    ui: 'reltio-swipe-delete',
                                    action: 'delete',
                                    id: 'delete',
                                    width: 70
                                }],
                                filterButtons: function(id, record) {
                                    switch (id) {
                                        case 'delete':
                                            return record.get('definition').allowDelete;
                                        case 'addParent':
                                            return record.get('definition').allowAddParent;
                                        case 'addChild':
                                            return record.get('definition').allowAddChild;
                                        default:
                                            return false;
                                    }
                                }
                            }
                        ],
                        flex: 1,
                        itemId: 'relationsTree',
                        store: {
                            data: [],
                            remoteSort: true,
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
