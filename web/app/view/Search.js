/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.Search', {
    extend: 'Ext.form.Panel',
    xtype: 'searchview',
    requires: [
        'MobileUI.plugins.PullRefresh',
        'MobileUI.components.ReltioToolbar',
        'Ext.field.Radio'
    ],
    config: {
        scrollable: null,
        cls: 'card searchview',
        fullscreen: true,
        layout: 'fit',
        style: 'background-color: white'
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                cls: 'reltio-search',
                items: [
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'button',
                                cls: 'filter-button',
                                text: i18n('Filter'),
                                minWidth: 65,
                                name: 'filterButton',
                                itemId: 'filterButton'
                            },
                            {
                                xtype: 'component',
                                cls: 'badge',
                                name: 'searchview-totalLabel'
                            },
                            {xtype: 'spacer'},
                            {
                                xtype: 'button',
                                cls: 'saved-searches-button',
                                text: i18n('Saved Searches'),
                                name: 'savedSearchesButton',
                                itemId: 'savedSearchesButton'
                            }
                        ]
                    },
                    {
                        xtype: 'searchfilter'
                    },
                    {
                        xtype: 'elist',
                        flex: 1,
                        itemId: 'foundList',
                        name: 'foundList',
                        plugins: [
                            'reltiopullrefresh'
                        ],
                        allowFetch: true,
                        scrollToTopOnRefresh: false,
                        masked: {
                            xtype: 'loadmask'
                        },
                        store: {
                            data: [],
                            model: 'MobileUI.components.list.model.EListModel'
                        }
                    }
                ]
            }
        ]);
    }
});
