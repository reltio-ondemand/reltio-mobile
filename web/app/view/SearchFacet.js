/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.SearchFacet', {
    extend: 'Ext.form.Panel',
    xtype: 'searchfacetview',
    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Password',
        'Ext.Label',
        'Ext.Img',
        'MobileUI.components.Menu',
        'MobileUI.components.EditableList'
    ],
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'vbox',
        cls: 'search-facet',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'search-facet-cancelButton',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Filter')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'search-facet-searchButton',
                        itemId: 'searchButton',
                        text: i18n('Done')
                    }
                ]
            },
            {
                xtype: 'component',
                name: 'searchfacetview-hint',
                cls: 'hint',
                html: i18n('Please refine search by selecting a type below'),
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'component',
                name: 'total'
            },
            {
                xtype: 'panel',
                flex: 1,
                layout: 'vbox',
                name: 'searchfacetview',
                scrollable: 'vertical',
                items: [
                    {
                        xtype: 'container',
                        name: 'searchfacetview-facets'
                    },
                    {
                        xtype: 'component',
                        html: '<div class="rl-item-separator" style="  margin: 0 10px"></div>'
                    },
                    {
                        xtype: 'container',
                        name: 'searchfacetview-advanced'
                    }
                ]
            }
        ]);
    }
});
