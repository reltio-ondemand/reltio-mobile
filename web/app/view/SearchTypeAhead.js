/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.SearchTypeAhead', {
    extend: 'Ext.form.Panel',
    xtype: 'searchtypeaheadview',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        layout: 'vbox',
        scrollable: null,
        cls: 'card',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'typeAheadResultsStorePhone',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        icon: 'resources/images/reltio/toolbar/menu.svg',
                        name: 'menuBtn',
                        cls: 'icon-button'
                    },
                    {
                        xtype: 'truesearchfield',
                        placeHolder: i18n('Search'),
                        name: 'searchtypeaheadview-search',
                        cls: 'search-field',
                        icon: 'resources/images/reltio/toolbar/search.svg',
                        flex: 1
                    },
                    {
                        text: i18n('Cancel'),
                        name: 'cancelBtn',
                        cls: 'text-button'
                    }
                ]
            },
            {
                xtype: 'button',
                icon: 'resources/images/reltio/relations/add.svg',
                name: 'addNewEntityBtn',
                text: '',
                cls: 'add-new-entity-btn'
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'typeAheadList',
                store: 'typeAheadResultsStorePhone'
            }
        ]);
    }
});
