/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dashboard.Dashboard', {
    extend: 'Ext.form.Panel',
    requires: [
        'MobileUI.plugins.PullRefresh'
    ],
    xtype: 'dashboardview',
    config: {
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        items: [
            {
                xtype: 'reltiotoolbar'
            },
            {
                xtype: 'panel',
                layout: 'vbox',
                scrollable: 'vertical',
                name: 'dashboardview-root',
                cls: 'dashboardview',
                items: [],
                pullRefresh: 'onPullRefresh'
            }
        ]
    }
});
