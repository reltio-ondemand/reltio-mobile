/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.ChooseAttributeType', {
    extend: 'Ext.form.Panel',
    xtype: 'chooseattrtype',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'coose-attribute-type',
        layout: 'vbox',
        scrollable: null,
        cls: 'card chooseattrtype',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'chooseattrtypeStore',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'chooseattrtype-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'chooseattrtype-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {

                        text: i18n('Done'),
                        name: 'chooseattrtype-doneBtn',
                        cls: 'text-button',
                        hidden: true
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                name: 'notificationToolbarChooseAttr',
                hidden: true,
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        name: 'notificationMessageChooseAttr',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'fake-icon-button'
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'chooseattrtype-search',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search')
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'chooseattrtype-list',
                store: 'chooseattrtypeStore'
            }
        ]);
    }
});
