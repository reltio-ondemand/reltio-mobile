/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.ChooseEntity', {
    extend: 'Ext.form.Panel',
    xtype: 'chooseentitydialog',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'choose-entity',
        layout: 'vbox',
        scrollable: null,
        cls: 'card chooseentitydialog',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'typeAheadResultsStorePhone',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'chooseentitydialog-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'chooseentitydialog-title',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'chooseentitydialog-search',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search')
            },
            {
                xtype: 'button',
                icon: 'resources/images/reltio/relations/add.svg',
                name: 'chooseentitydialog-addNewEntityBtn',
                text: '',
                cls: 'add-new-entity-btn'
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'chooseentitydialog-list',
                scrollToTopOnRefresh: false,
                store: 'typeAheadResultsStorePhone'
            }
        ]);
    }
});
