/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.CreateEntity', {
    extend: 'Ext.form.Panel',
    xtype: 'createentitydialog',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    config: {
        name: 'create-entity',
        layout: 'vbox',
        scrollable: true,
        cls: 'card createentitydialog',
        fullscreen: true,
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'close-create-entity',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title-create-entity',
                        html: '',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'save-new-entity-button',
                        text: i18n('Save')
                    }
                ]
            },
            {
                xtype: 'elist',
                editing: true,
                flex: 1,
                scrollable: false,
                height: 0,
                name: 'create-entity-list',
                cls: 'reltio-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ]);
    }
});
