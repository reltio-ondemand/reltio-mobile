/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.CreateRelation', {
    extend: 'Ext.form.Panel',
    xtype: 'createrelationdialog',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    config: {
        name: 'create-relation',
        layout: 'vbox',
        scrollable: true,
        cls: 'card createrelationdialog',
        fullscreen: true,
        items: []
    },
    initialize: function ()
    {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'close-create-relation',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title-create-relation',
                        html: '',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'create-relation-button',
                        text: i18n('Save')
                    }

                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                name: 'notificationToolbar',
                hidden: true,
                items: [

                    {
                        xtype: 'component',
                        name: 'notificationMessage',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        name: 'closeNotificationBtn',
                        cls: 'icon-button'
                    }
                ]
            },

            {
                xtype: 'elist',
                editing: true,
                scrollable: false,
                height: 0,
                name: 'create-relation-list-entity',
                cls: 'reltio-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                style: 'margin-bottom: 0'
            },
            {
                xtype: 'component',
                cls: 'rl-item-separator',
                style: 'margin: 0 10px',
                name: 'create-relation-lists-separator'
            },
            {
                xtype: 'elist',
                editing: true,
                scrollable: false,
                height: 0,
                name: 'create-relation-list',
                cls: 'reltio-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                style: 'margin-top: 0'
            }
        ]);
    }
});
