/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.EditSavedSearch', {
    extend: 'Ext.form.Panel',
    xtype: 'editsavedsearchdialog',
    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Password',
        'Ext.Label',
        'Ext.Img',
        'MobileUI.components.Menu',
        'MobileUI.components.EditableList'
    ],
    config: {
        name: 'edit-saved-searches-dialog',
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        cls: 'editsavedsearch',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        xtype: 'button',
                        name: 'editsavedsearch-cancelButton',
                        cls: 'text-button',
                        text: i18n('Cancel')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Saved Searches')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'editsavedsearch-saveButton',
                        text: i18n('Save')
                    }

                ]
            },
            {
                xtype: 'elist',
                name: 'editsavedsearch-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ]);
    }
});
