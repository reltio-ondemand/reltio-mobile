/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.GlobalFilter', {
    extend: 'Ext.form.Panel',
    xtype: 'globalfilterdialog',
    config: {
        name: 'global-filter-dialog',
        layout: 'vbox',
        scrollable: null,
        cls: 'card globalfilterdialog',
        fullscreen: true,
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        xtype: 'button',
                        name: 'globalfilterdialog-closeBtn',
                        cls: 'icon-button',
                        icon: 'resources/images/reltio/toolbar/back.svg'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Global Filter'),
                        flex: 1
                    },
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    }
                ]
            },
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox',
                    align: 'center'
                },
                cls: 'mute-toggle-panel',
                items: [
                    {
                        xtype: 'component',
                        html: i18n('Global Filter'),
                        cls: 'toggle-label'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'togglefield',
                        name: 'globalfilterdialog-toggle'
                    }
                ]
            },
            {
                xtype: 'panel',
                flex: 1,
                name: 'globalfilterdialog-filters',
                cls: 'globalfilterdialog-filters',
                scrollable: 'vertical'
            }
        ]);
    }
});
