/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.ReferenceAttributeEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'referenceattributeeditordialog',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    config: {
        name: 'reference-attribute-editor',
        layout: 'vbox',
        scrollable: true,
        cls: 'card popupEditor',
        fullscreen: true,
        items: []
    },
    initialize: function ()
    {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'close-reference-attribute-editor',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title-reference-attribute-editor',
                        html: '',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'save-reference-attribute-button',
                        disabled: true,
                        text: i18n('Done')
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                cls: 'notification-toolbar',
                layout: {
                    type: 'hbox',
                    align: 'top'
                },
                name: 'notificationToolbarRef',
                hidden: true,
                items: [
                    {
                        xtype: 'component',
                        name: 'notificationMessageRef',
                        cls: 'notification-message',
                        html: ''
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        icon: 'resources/images/reltio/toolbar/close.svg',
                        name: 'closeNotificationBtnRef',
                        cls: 'icon-button'
                    }
                ]
            },
            {
                xtype: 'elist',
                editing: true,
                flex: 1,
                scrollable: false,
                height: 0,
                cls: 'reltio-list',
                name: 'reference-attribute-editor-entity-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                style: 'margin-bottom: 0'
            },
            {
                xtype: 'component',
                cls: 'rl-item-separator',
                style: 'margin: 0 10px',
                name:  'reference-attribute-editor-list-separator'
            },
            {
                xtype: 'elist',
                editing: true,
                flex: 1,
                scrollable: false,
                height: 0,
                name: 'reference-attribute-editor-list',
                cls: 'reltio-list',
                plugins: [
                    {type: 'swipetodelete', useMenu: true }
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                style: 'margin-top: 0'
            }
        ]);
    }
});
