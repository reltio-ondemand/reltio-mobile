/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.RelationshipType', {
    extend: 'Ext.form.Panel',
    xtype: 'relationshiptypedialog',
    config: {
        name: 'relationship-type',
        layout: 'vbox',
        scrollable: null,
        cls: 'card relationshiptype',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'close-relation-type-dialog',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Choose Type'),
                        flex: 1
                    }
                   
                ]
            },
            {
                xtype: 'elist',
                editing: true,
                flex: 1,
                cls: 'reltio-list',
                name: 'relationshiptypedialog-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ]);
    }
});
