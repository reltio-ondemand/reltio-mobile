/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.SavedSearches', {
    extend: 'Ext.form.Panel',
    xtype: 'savedsearchesdialog',
    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Password',
        'Ext.Label',
        'Ext.Img',
        'MobileUI.components.Menu',
        'MobileUI.components.EditableList',
        'MobileUI.plugins.SwipeToDelete'
    ],
    config: {
        name: 'saved-searches-dialog',
        scrollable: null,
        fullscreen: true,
        layout: 'fit',
        cls: 'savedsearch',
        style: 'background-color: white',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        icon: 'resources/images/reltio/toolbar/menu.svg',
                        name: 'menuBtn',
                        cls: 'icon-button'
                    },
                    {
                        xtype: 'button',
                        name: 'savedsearches-backButton',
                        cls: 'icon-button',
                        icon: 'resources/images/reltio/toolbar/back.svg'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: i18n('Saved Searches')
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'fake-icon-button'
                    }
                ]
            },
            {
                xtype: 'component',
                name: 'savedsearches-hint',
                cls: 'hint',
                html: i18n('Swipe for global filter and edit control'),
                docked: 'top',
                hidden: true
            },
            {
                xtype: 'elist',
                flex: 1,
                name: 'savedsearches-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                allowFetch: true,
                scrollToTopOnRefresh: false,
                plugins: [
                    {
                        type: 'swipetodelete',
                        swipeRightButtons: [{
                            label: i18n('Add to Global Filter'),
                            ui: 'reltio-swipe-edit',
                            action: 'addToGlobalFilter',
                            id: 'addToGlobalFilter',
                            width: 100
                        }, {
                            label: i18n('Remove from Global Filter'),
                            ui: 'reltio-swipe-edit',
                            action: 'removeFromGlobalFilter',
                            id: 'removeFromGlobalFilter',
                            width: 100
                        }, {
                            label: i18n('Edit'),
                            ui: 'reltio-swipe-change',
                            action: 'editSearch',
                            id: 'editSearch',
                            width: 70
                        }, {
                            label: i18n('Delete'),
                            ui: 'reltio-swipe-delete',
                            action: 'delete',
                            id: 'delete',
                            width: 70
                        }],
                        filterButtons: function(id, record) {
                            if (id === 'delete') {
                                return record.get('userData').ownedByCurrentUser;
                            }
                            else if (id === 'editSearch') {
                                return record.get('userData').ownedByCurrentUser;
                            }
                            else if (id === 'addToGlobalFilter') {
                                return !record.get('userData').isGlobalFilter;
                            }
                            else if (id === 'removeFromGlobalFilter') {
                                return record.get('userData').isGlobalFilter;
                            }
                            return true;
                        }
                    }
                ]
            }
        ]);
    }
});
