/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.UploadImage', {
    extend: 'Ext.form.Panel',
    xtype: 'uploadimagedialog',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    config: {
        name: 'upload-image',
        layout: 'vbox',
        scrollable: true,
        cls: 'card uploadimage property-editor',
        fullscreen: true,
        items: []
    },
    initialize: function ()
    {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'uploadimagedialog-backToolbarBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title-upload-image',
                        html: i18n('Add image'),
                        flex: 1
                    },
                    {
                        name: 'upload-image-save',
                        cls: 'text-button',
                        text: i18n('Save')
                    }
                ]
            },
            {
                xtype: 'component',
                cls: 'label',
                html: i18n('Upload image by Url')
            },
            {
                xtype: 'textfield',
                name: 'upload-image-url',
                cls: 'property-editor-field',
                placeHolder: i18n('url')
            },
            {
                xtype: 'component',
                cls: 'label',
                html: i18n('or')
            },
            {
                xtype: 'fileupload',
                name: 'upload-image-take-photo',
                cls: 'save-new-entity-button',
                text: i18n('Take photo')
            }
        ]);
    }
});
