/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.dialogs.entities.SelectImage', {
    extend: 'Ext.form.Panel',
    xtype: 'selectimagedialog',
    requires: ['MobileUI.plugins.SwipeToDelete'],
    config: {
        name: 'select-image',
        layout: 'vbox',
        scrollable: true,
        cls: 'card selectimage',
        fullscreen: true,
        items: []
    },
    initialize: function ()
    {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'close-select-image',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        name: 'title-select-image',
                        html: i18n('Select image'),
                        flex: 1
                    },
                    {
                        name: 'selectimagedialog-backToolbarBtn',
                        text: i18n('Save'),
                        cls: 'text-button'
                    }
                ]
            },
            {
                xtype: 'elist',
                editing: true,
                scrollable: false,
                height: 0,
                name: 'select-image-list',
                cls: 'reltio-list',
                plugins: [
                    'swipetodelete'
                ],
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                },
                style: 'margin-bottom: 0'
            }
        ]);
    }
});
