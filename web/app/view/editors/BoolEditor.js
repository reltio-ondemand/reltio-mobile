/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.BoolEditor', {
    extend: 'Ext.form.Panel',
    requires: ['MobileUI.components.TripleToggle'],
    xtype: 'booleditor',
    config: {
        name: 'boolean-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card property-editor',
        fullscreen: true,
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'booleditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'booleditor-parent-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        docked: 'bottom',
                        cls: 'text-button',
                        name: 'booleditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                cls: 'property-editor-spacer'
            },
            {
                cls: 'property-editor-field',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'component',
                        html: '',
                        name: 'booleditor-title',
                        cls: 'inline-editor-caption'
                    },
                    {xtype: 'spacer'},
                    {
                        xtype: 'tripletoggle',
                        name: 'booleditor-field',
                        cls: 'tripletoggle'
                    }
                ]
            }
        ]);
    }
});
