/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.DateEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'dateeditor',
    requires: [],
    config: {
        name: 'date-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card property-editor',
        fullscreen: true,
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'dateeditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'dateeditor-parent-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'dateeditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                cls: 'property-editor-spacer'
            },
            {
                cls: 'property-editor-field',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'component',
                        html: '',
                        name: 'dateeditor-title',
                        cls: 'inline-editor-caption'
                    },
                    {xtype: 'spacer'},
                    {
                        xtype: 'textfield',
                        name: 'dateeditor-field',

                        inputCls: 'input-text',
                        clearIcon: false,
                        component: {type: 'date'}
                    }
                ]
            }
        ]);
    }
});
