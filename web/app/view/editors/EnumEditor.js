/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.EnumEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'enumeditor',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'enum-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card enumeditor',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'enumeditorStore',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'enumeditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'enumeditor-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'enumeditor-doneBtn',
                        text: i18n('Done')
                    }
                   
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'enumeditor-search',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search')
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'enumeditor-list',
                store: 'enumeditorStore'
            }
        ]);
    }
});
