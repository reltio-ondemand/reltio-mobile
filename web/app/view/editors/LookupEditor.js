/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.LookupEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'lookupeditor',
    requires: [
        'MobileUI.components.TrueSearchField'
    ],
    config: {
        name: 'lookup-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card lookupeditor',
        fullscreen: true,
        style: 'background-color: white',
        items: []
    },
    initialize: function() {
        this.callParent();
        Ext.create('Ext.data.Store', {
            storeId: 'lookupeditorStore',
            proxy: {
                type: 'memory'
            },
            model: 'MobileUI.components.list.model.EListModel'
        });
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'lookupeditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'lookupeditor-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        cls: 'text-button',
                        name: 'lookupeditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'lookupeditor-search',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search')
            },
            {
                flex: 1,
                xtype: 'elist',
                name: 'lookupeditor-list',
                store: 'lookupeditorStore'
            }
        ]);
    }
});
