/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.NumberEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'numbereditor',
    requires: ['MobileUI.components.list.editors.NumberEditor'],
    config: {
        name: 'number-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card property-editor',
        fullscreen: true,
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'numbereditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'numbereditor-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        docked: 'bottom',
                        cls: 'text-button',
                        name: 'numbereditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                cls: 'property-editor-spacer'
            },
            {
                xtype: 'truenumbereditor',
                name: 'numbereditor-field',
                cls: 'property-editor-field'
            },
            {
                xtype: 'component',
                name: 'numbereditor-invalidMarker',
                cls: 'property-editor-invalid',
                html: i18n('Value is incorrect'),
                hidden: true
            }
        ]);
    }
});
