/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.StringEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'stringeditor',
    requires: [],
    config: {
        name: 'string-editor',
        layout: 'vbox',
        scrollable: null,
        cls: 'card property-editor',
        fullscreen: true,
        items: []
    },
    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        text: i18n('Cancel'),
                        name: 'stringeditor-cancelBtn',
                        cls: 'text-button'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'stringeditor-title'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'stringeditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                cls: 'property-editor-spacer'
            },
            {
                xtype: 'textfield',
                name: 'stringeditor-field',
                cls: 'property-editor-field'
            },
            {
                xtype: 'component',
                name: 'stringeditor-invalidMarker',
                cls: 'property-editor-invalid',
                html: i18n('Value is incorrect'),
                hidden: true
            }
        ]);
    }
});
