/*
 * Copyright [2020] Reltio Inc., all rights reserved. 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Ext.define('MobileUI.view.editors.TypeaheadEditor', {
    extend: 'Ext.form.Panel',
    xtype: 'typeaheadeditor',
    requires: [
        'Ext.form.FieldSet',
        'Ext.form.Password',
        'Ext.Label',
        'Ext.Img',
        'MobileUI.components.Menu'
    ],
    config: {
        name: 'choose-facet-item',
        cls: 'card typeaheadeditor',
        scrollable: null,
        fullscreen: true,
        layout: 'vbox',
        style: 'background-color: white',
        items: []
    },

    initialize: function() {
        this.callParent();
        this.add([
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'reltio',
                items: [
                    {
                        xtype: 'button',
                        name: 'typeaheadeditor-cancelButton',
                        text: i18n('Cancel'),
                        cls: 'text-button'
                    },
                    {
                        xtype: 'component',
                        cls: 'header',
                        html: '',
                        name: 'typeaheadeditor-title',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        cls: 'text-button',
                        name: 'typeaheadeditor-doneBtn',
                        text: i18n('Done')
                    }
                ]
            },
            {
                xtype: 'truesearchfield',
                name: 'searchFacetItem',
                text: '',
                cls: 'search-field',
                placeHolder: i18n('Search')
            },
            {
                xtype: 'elist',
                editing: true,
                flex: 1,
                name: 'typeaheadeditor-list',
                store: {
                    data: [],
                    model: 'MobileUI.components.list.model.EListModel'
                }
            }
        ]);
    }
});
