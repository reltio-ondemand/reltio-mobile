module.exports = function(config) {
    config.set({
        frameworks: ['jasmine-ajax', 'jasmine'],
        basePath: '',
        files: [
            {pattern: 'bootstrap.json', included: false},
            {pattern: 'bootstrap*.js*', included: false},
            {pattern: 'app.js', watched: true,  served: true, included: false},
            {pattern: 'resources/**/*', included: false},
            {pattern: 'lib/**.*'},
            'touch/sencha-touch-all-debug.js',
            {pattern: 'touch/**/*', included: false},
            {pattern: 'app/**/*.js', watched: true,  served: true, included: false},
            'jasmine/lib/underscore.js',
            'jasmine/lib/jasmin-object-containing.js',
            'spec/setup.js',
            'spec/app/**/*.js'
        ],
        proxies: {
            '/bootstrap': '/base/bootstrap',
            '/app': '/base/app',
            '/lib': '/base/lib',
            '/jasmine': '/base/jasmine',
            '/touch': '/base/touch',
            '/resources': '/base/resources'
        }
    })
};