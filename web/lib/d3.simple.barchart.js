(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['d3'], factory)
    } else if (typeof module === 'object' && module.exports) {
        module.exports = function (d3) {
            BarChart = factory(d3);
            return BarChart
        }
    } else {
        root.BarChart = factory(root.d3)
    }
}(this, function (d3) {

        return function () {
            function i18n(data) {
                return window.i18n ? window.i18n.apply(null, arguments) : data;
            }

            function colorPalette(n) {
                var colors = ['#ffee91', '#ffdc0e', '#fec92f',
                    '#eab710', '#f69785', '#f57f27', '#fccde5', '#f8b6b8',
                    '#ff9acc', '#ee4272', '#b71137', '#c4c0e1', '#bd81be',
                    '#5f3577', '#9fd6bf', '#8accca', '#12959f'];
                return colors[(n * 3) % colors.length];
            }
            BarChart.prototype.xScale = function (label, i) {
                return this.xPositions[i].position + 30;
            };

            BarChart.prototype.xScaleAbsolute = function (uid) {
                return this.xPositions.filter(function (item) {
                    return item.uid === uid
                })[0];
            };

            BarChart.prototype.xScaleGroup = function (label, i) {
                return this.xGroupPositions[i];
            };

            function getOffsetRect(element) {
                if (!element) {
                    return null
                }
                var box = element.getBoundingClientRect(),
                    body = document.body,
                    docElem = document.documentElement,
                    scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop,
                    scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft,
                    clientTop = docElem.clientTop || body.clientTop || 0,
                    clientLeft = docElem.clientLeft || body.clientLeft || 0,
                    top, left;

                top = box.top + scrollTop - clientTop;
                left = box.left + scrollLeft - clientLeft;

                return {top: Math.round(top), left: Math.round(left)}
            }

            function BarChart(vis, options, painted) {
                if (typeof vis === 'string') {
                    this.element = document.getElementById(vis.replace(/^#/, ''));
                }
                this.xPositions = [];
                this.xGroupPositions = [];
                this.legendElementMaxHeight = 30;
                this.options = {
                    asyxOffset: 65,
                    barWidth: 32,
                    minWidth: 230,
                    width: 230,
                    minBarWidth: 32,
                    maxBarWidth: 100,
                    height: 250,
                    barInitPadding: 0,
                    gapBetweenBars: 3,
                    minGapBetweenBars: 3,
                    maxGapBetweenBars: 30,
                    gapBetweenGroup: 30,
                    minGapBetweenGroup: 30,
                    maxGapBetweenGroup: 100,
                    hideXAxis: false,
                    margin: {top: 15, right: 20, bottom: 30, left: 40},
                    data: []
                };
                this.tip = d3tip();
                if (options) {
                    for (var attrName in this.options) {
                        if (this.options.hasOwnProperty(attrName)) {
                            if (options[attrName] !== undefined) {
                                this.options[attrName] = options[attrName];
                            }
                        }
                    }
                    this.setData(this.options.data);
                }
                painted.call();

            }

            BarChart.prototype.setTransition = function (transition) {
                this.transformableSvg.attr('transform', 'translate(' + transition[0] + ',' + transition[1] + ')')
            };
            BarChart.prototype.getTransition = function () {
                return d3.transform(this.transformableSvg.attr('transform'))
            };
            BarChart.prototype.getInitialTransition = function () {
                return [this.options.margin.left - 35, this.options.margin.top];
            };
            BarChart.prototype.calculateConstants = function (data, minWidth) {
                var keys = Object.keys(data), width;
                var groupCount = keys.length || 1;
                var barsInGroup = data[keys[0]].length;
                var initialEstimatedWidth = groupCount * barsInGroup * this.options.minBarWidth;
                if (initialEstimatedWidth > minWidth) {
                    return;
                }
                var maxAvailableGroupWidth = (minWidth / groupCount) - this.options.minGapBetweenGroup;
                var minAvailableGroupWidth = (minWidth / groupCount) - this.options.maxGapBetweenGroup;
                var maxAvailableItemWidth = maxAvailableGroupWidth / barsInGroup - this.options.minGapBetweenBars;
                var minAvailableItemWidth = minAvailableGroupWidth / barsInGroup - this.options.maxGapBetweenGroup;
                var availableItemWidth = maxAvailableItemWidth;
                if (minAvailableItemWidth > this.options.minBarWidth) {
                    availableItemWidth = (maxAvailableItemWidth + minAvailableItemWidth) / 2;
                }
                if (availableItemWidth > this.options.minBarWidth) {
                    this.options.barWidth = Math.min(availableItemWidth, this.options.maxBarWidth);
                    this.options.gapBetweenBars = Math.min(Math.max(availableItemWidth - this.options.barWidth, this.options.minGapBetweenBars), this.options.maxGapBetweenBars);
                }
                width = (this.options.barWidth * barsInGroup + this.options.gapBetweenBars * (barsInGroup - 1)) * groupCount + (groupCount - 1) * this.options.gapBetweenGroup + this.options.asyxOffset;
                if (width < minWidth)
                {
                    this.options.barInitPadding = Math.floor((minWidth-width)/2)
                }
            };
            BarChart.prototype.calculateWidth = function (data) {
                var width, self = this;
                this.xGroupPositions = [self.options.barInitPadding];
                width = data.reduce(function (prevWidth, nextObject, index, array) {
                    var offset = 0, currentWidth;
                    if (index < array.length - 1) {
                        offset = self.options.gapBetweenGroup;
                    }
                    var sumWidth;
                    currentWidth = Object.keys(nextObject).filter(function (item) {
                        return item !== '$$groupLabel'
                    }).reduce(function (prev, next, index, array) {
                        var offset = 0;
                        if (index < array.length - 1) {
                            offset = self.options.gapBetweenBars;
                        }
                        var result = prev + self.options.barWidth + offset;
                        self.xPositions.push({
                            uid: nextObject[next].uid,
                            position: prev,
                            absolutePosition: prevWidth + prev
                        });
                        return result;
                    }, 0);
                    sumWidth = prevWidth + currentWidth
                        + offset;
                    if (index !== array.length - 1) {
                        self.xGroupPositions.push(sumWidth);
                    }
                    return sumWidth;
                }, self.options.barInitPadding);
                width += this.options.asyxOffset;
                return width;
            };

            BarChart.prototype.extractLabels = function (data) {
                return d3.set(Array.prototype.concat.apply([], data.map(function (item) {
                    return Object.keys((item));
                }))).values().filter(function (i) {
                    return i !== '$$groupLabel';
                });

            };
            BarChart.prototype.convertData = function (data) {
                var keys = Object.keys(data);
                var result = [];
                for (var i = 0; i < keys.length; i++) {
                    var group = data[keys[i]];
                    var obj = {};
                    var duplicates = {};
                    group.forEach(function (item) {
                        item.label = item.label.trim();
                        if (obj[item.label]) {
                            if (!duplicates[item.label]) {
                                duplicates[item.label] = 0;
                            }
                            duplicates[item.label] += 1;
                            item.label += Ext.String.repeat('$$$', duplicates[item.label]);
                        }
                        obj[item.label] = {
                            value: item.value,
                            color: item.color,
                            uid: Math.random().toFixed(4).slice(2)
                        };
                    });
                    obj.$$groupLabel = keys[i];
                    result.push(obj)
                }
                return result;
            };
            BarChart.prototype.wrap = function (text, width) {
                width = width - (this.options.gapBetweenGroup || 0);
                var me = this;
                text.each(function () {
                    var text = d3.select(this),
                        words = text.text().split(/\s+/).reverse(),
                        word,
                        line = [],
                        lineNumber = 0,
                        lineHeight = 1.1, // ems
                        y = text.attr('y'),
                        dy = parseFloat(text.attr('dy')),
                        tspan = text.text(null).append('tspan').attr('x', 0).attr('y', y).attr('dy', dy + 'em');
                    while (word = words.pop()) {
                        line.push(word);
                        tspan.text(line.join(' '));
                        if (tspan.node().getComputedTextLength() > width) {
                            line.pop();
                            tspan.text(line.join(' '));
                            line = [word];
                            tspan = text.append('tspan').attr('x', 0).attr('y', y).attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word);
                        }
                    }
                    me.legendElementMaxHeight = Math.max(text.node().getBBox().height, me.legendElementMaxHeight);
                });
            };
            BarChart.prototype.render = function () {
                var me = this;
                if (this.data) {
                    var data = this.convertData(this.data);
                    var availableWidth = Math.max(this.options.minWidth, this.options.width);
                    this.calculateConstants(this.data, availableWidth);
                    var maxWidth = this.calculateWidth(data),
                        width = Math.max(maxWidth,availableWidth),
                        height = this.options.height,
                        that = this;
                    this.width = maxWidth;
                    var format = function(i){
                        if (i>0 && i<1){
                            return d3.format('.2g')(i);
                        }else{
                            return d3.format('.2s')(i);
                        }
                    };
                    var x0 = d3.scale.ordinal()
                        .rangeRoundBands([0, Math.max(width,availableWidth)]);
                    var y = d3.scale.linear()
                        .range([height, 0]);
                    var xAxis = d3.svg.axis()
                            .scale(x0)
                            .orient('bottom')
                            .tickPadding(10)
                            .outerTickSize(0),
                        yAxis = d3.svg.axis()
                            .scale(y)
                            .ticks(4)
                            .tickSize(width)
                            .orient('right')
                            .tickFormat(format);

                    var clipPathId = this.element.id + '-clippath';
                    var fullWidth = width + this.options.margin.left + this.options.margin.right;
                    var fullHeight = height + this.options.margin.top + this.options.margin.bottom;
                    var svg = this.svg = d3.select(this.element).append('svg')
                        .attr('width', fullWidth)
                        .attr('height', fullHeight)
                        .call(this.tip);
                    this.mainClipPath = svg.append('svg:clipPath')
                        .attr('id', clipPathId)
                        .append('svg:rect')
                        .attr('x', 35)
                        .attr('y', 0)
                        .attr('width', fullWidth)
                        .attr('height', fullHeight);

                    var yPath = svg.append('g')
                        .attr('transform', 'translate(' + (this.options.margin.left - 35) + ',' + this.options.margin.top + ')');
                    svg = svg.append('g')
                        .attr('transform', 'translate(' + (this.options.margin.left - 35) + ',' + this.options.margin.top + ')')
                        .attr('clip-path', 'url(#' + clipPathId + ')');
                    svg = this.transformableSvg = svg.append('g')
                        .attr('transform', 'translate(' + (this.options.margin.left - 35) + ',' + this.options.margin.top + ')');
                    var labels = this.extractLabels(data);

                    data.forEach(function (d) {
                        d.values = labels.map(function (label) {
                            return {
                                label: label,
                                value: d[label] ? (+d[label].value || 0) : 0,
                                color: d[label] ? d[label].color : '',
                                uid: d[label] ? d[label].uid : ''
                            };
                        });
                    });
                    x0.domain(data.map(function (d) {
                        return d.$$groupLabel;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d3.max(d.values, function (d) {
                            return d.value;
                        });
                    })]);

                    var xAxisPath = svg.append('g')
                        .attr('class', 'x axis')
                        .attr('transform', 'translate(0,' + height + ')')
                        .call(xAxis);
                    xAxisPath.selectAll('.tick text').call(this.wrap.bind(this), x0.rangeBand());

                    var yAxisPath = yPath.append('g')
                        .attr('class', 'y axis')
                        .attr('transform', ' translate(35,15)')
                        .call(yAxis);
                    yAxisPath
                        .append('text')
                        .attr('transform', 'rotate(-90)')
                        .attr('y', 6)
                        .attr('dy', '.71em')
                        .style('text-anchor', 'end');

                    yAxisPath.selectAll('text')
                        .attr('x', -35)
                        .attr('dy', 4);
                    yAxisPath.selectAll('line').attr('transform', 'translate(-10,0)');

                    d3.select(yAxisPath.selectAll('.tick')[0][0]).attr('visibility', 'hidden');
                    if (this.options.hideXAxis || data.length <= 1) {
                        xAxisPath.selectAll('.tick').attr('visibility', 'hidden');
                    }
                    if (this.options.barInitPadding !== 0){
                        var ticks = xAxisPath.selectAll('.tick');
                        var ticksCount =0;
                        ticks.each(function(){
                            ticksCount++;
                        });
                        if (ticksCount > 1) {
                            var availableRangeWidth = me.xGroupPositions[1] - me.xGroupPositions[0]-me.options.gapBetweenGroup;
                            ticks.each(function (d, position) {
                                var translate = d3.transform(d3.select(this).attr('transform')).translate,
                                    translateX;
                                if (position === 0 || position === ticksCount - 1) {
                                    var tickWidth = d3.select(this).node().getBBox().width;
                                    var offset = (availableRangeWidth -tickWidth)/2+35;
                                    translateX = position === 0
                                        ? me.options.barInitPadding + offset
                                        : me.xGroupPositions[ticksCount -1] +offset ;
                                    d3.select(this).attr('transform', 'translate(' + translateX + ',' + translate[1] + ')');
                                }
                            })
                        }
                    }
                    var state = this.basePath = svg.selectAll('.value')
                        .data(data)
                        .enter().append('g')
                        .attr('class', 'g')
                        .attr('transform', function (d, i) {
                            return 'translate(' + me.xScaleGroup(d.$$groupLabel, i) + ',0)';
                        });
                    var enterSelection = state.selectAll('rect')
                        .data(function (d) {
                            return d.values;
                        })
                        .enter();
                    enterSelection.append('rect')
                        .attr('width', this.options.barWidth)
                        .attr('x', function (d, i, t) {
                            var xPos = me.xScale(d.label, i);
                            return xPos;
                        })
                        .attr('y', function (d) {
                            return y(d.value);
                        })
                        .attr('height', function (d) {
                            return height - y(d.value);
                        })
                        .style('fill', function (d, i, t) {

                            return d.color || colorPalette(i);
                        })
                        .on('click', function (d, i, t) {
                            var rect = getOffsetRect(that.element);
                            var xPosition = this.getBoundingClientRect().left + this.getBoundingClientRect().width / 2 - 10;
                            var yPosition = rect.top + y(d.value);
                            var label = d.label.replace(/\$\$\$/g, '');
                            that.tip.show(xPosition, yPosition, label, i18n(+d.value), [xPosition, height - y(d.value)], d.color || colorPalette(i), {
                                calculatePosition: function (x, y, centroid, content, before, after) {
                                    var size = content.node().getBoundingClientRect(),
                                        dx, dxDefault = 22;
                                    if (centroid) {
                                        if (centroid[0] - size.width > 0) {
                                            x = x - size.width + 17;
                                            dx = Math.floor(size.width - dxDefault);
                                            before.style('left', dx + 'px');
                                            after.style('left', dx + 1 + 'px');
                                        } else {
                                            before.style('left', '10px');
                                            after.style('left', '11px');
                                            x = x - 15;
                                        }
                                    }
                                    if (centroid[1] > height / 2) {
                                        y = y + size.height;
                                    } else {
                                        y = Math.max(y - size.height + 17, 0);
                                    }
                                    return {x: Math.floor(x), y: Math.floor(y)};
                                },
                                calculateArrow: function (coordinates) {
                                    if (coordinates[1] > height / 2) {
                                        return 'bottom';
                                    } else {
                                        return 'top';
                                    }
                                }
                            });
                        }
                    );
                    enterSelection.append('rect')
                        .attr('width', this.options.barWidth)
                        .attr('x', function (d, i, t) {
                            return me.xScale(d.label, i);
                        })
                        .attr('y', height - 1)
                        .attr('height', function (d) {
                            return 2;
                        })
                        .style('fill', function (d, i, t) {

                            return '#666666';
                        });
                }
                this.svg.attr('height', fullHeight + this.legendElementMaxHeight + 30);
                this.mainClipPath.attr('height', fullHeight + this.legendElementMaxHeight + 30);
            };
            BarChart.prototype.getWidth = function () {
                return parseFloat(this.svg.attr('width'));
            };
            BarChart.prototype.getHeight = function () {

            };
            BarChart.prototype.setData = function (data) {
                this.data = data;
                this.render();
            };
            BarChart.prototype.tip = this.tip;

            return BarChart;
        }();
    }
));