(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['d3'], factory)
    } else if (typeof module === 'object' && module.exports) {
        module.exports = function (d3) {
            Legend = factory(d3);
            return Legend
        }
    } else {
        root.Legend = factory(root.d3)
    }
}(this, function (d3) {

        return function () {
            var self, svg;

            function wrapLegendAttribute() {
                var selfElement = d3.select(this),
                    previousSibling = d3.select(this.previousSibling),
                    previousTextLength = previousSibling.node().getComputedTextLength(),
                    text = previousSibling.text(),
                    textLength = selfElement.node().getComputedTextLength(),
                    calculateAvailableSpace = function (textLength) {
                        return (self.options.width - self.options.legendRectSize - self.options.legendXSpacing - textLength - 2 * 2);
                    },
                    availaleSpace;
                if (!previousSibling) {
                    return;
                }
                availaleSpace = calculateAvailableSpace(textLength);
                while (previousTextLength > availaleSpace && text.length > 0) {
                    text = text.slice(0, -1);
                    previousSibling.text(text + '...');
                    previousTextLength = previousSibling.node().getComputedTextLength();
                }
            }

            function Legend(vis, options, painted) {
                if (typeof vis === 'string') {
                    this.element = document.getElementById(vis.replace(/^#/, ''));
                }


                self = this;
                this.options = {
                    width: 250,
                    margin: {top: 20, right: 20, bottom: 30, left: 40},
                    legendRectSize: 12,
                    legendYSpacing: 11,
                    legendXSpacing: 10,
                    data: []
                };
                if (options) {
                    for (var attrName in this.options) {
                        if (this.options.hasOwnProperty(attrName)) {
                            if (options[attrName] !== undefined) {
                                this.options[attrName] = options[attrName];
                            }
                        }
                    }
                    svg = d3.select(this.element)
                        .append('svg:svg')
                        .attr('width', this.options.width)
                    this.setData(this.options.data);
                }
                painted.call();

            }

            Legend.prototype.setData = function (data) {
                var that=this;
                var legend = svg.selectAll('.legend')
                    .data(data)
                    .enter()
                    .append('g')
                    .attr('class', 'legend')
                    .attr('transform', function (d, i) {
                        var legendItemHeight = that.options.legendRectSize + that.options.legendYSpacing;
                        var horizontal = 2 * that.options.legendRectSize;
                        var vertical = legendWidth + that.options.legendOffset + i * legendItemHeight;
                        return 'translate(' + horizontal + ',' + vertical + ')';
                    });

                legend.append('circle')
                    .attr('r', that.options.legendRectSize / 2)
                    .style('fill', function (d, i) {
                        return d.color || that.options.colorPalette(i);
                    })
                    .style('stroke', function (d, i) {
                        return d.color || that.options.colorPalette(i);
                    });
                legend.append('text')
                    .attr('x', that.options.legendRectSize + that.options.legendXSpacing)
                    .attr('y', 6)
                    .attr('overflow', 'hidden')
                    .attr('width', '10')
                    .attr('class', 'legend-attribute-name')
                    .text(function (d) {
                        return d.label;
                    });
                legend.append('text')
                    .attr('x', legendWidth)
                    .attr('y', 6)
                    .attr('overflow', 'hidden')
                    .attr('width', '10')
                    .attr('class', 'legend-attribute-value')
                    .attr('text-anchor', 'end')
                    .text(function (d) {
                        return d.value;
                    }).each(that.wrapLegendAttribute);
            };
            return Legend;
        }();

    }
));
