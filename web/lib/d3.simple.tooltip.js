(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['d3'], factory)
    } else if (typeof module === 'object' && module.exports) {
        module.exports = function (d3) {
            d3tip = factory(d3);
            return d3tip
        }
    } else {
        root.d3tip = factory(root.d3)
    }
}(this, function (d3) {

    return function () {
        var node = initNode(),
            content,
            colorNode,
            text,
            height,
            width,
            before,
            after,
            clickHandler,
            tipInfo;

        function tip(vis) {
            height = vis.attr('height');
            width = vis.attr('width');

            document.body.appendChild(node.node());
            getNodeEl().style({'display':'none', 'pointer-events': 'none'});
        }

        function initNode() {
            var node = d3.select('.ui-tooltip');
            if (node.empty()) {
                node = d3.select(document.createElement('div'));
            }
            node.html('');
            node.style({
                position: 'absolute',
                'pointer-events': clickHandler ? '' : 'none'
            });
            node.attr('class', 'ui-tooltip right');
            before = node.append('div');
            before.attr('class', 'before');
            content = node.append('div');
            colorNode = createColorNode(content);
            text = createTextNode(content);
            content.attr('class', 'ui-tooltip-content' + (clickHandler ? ' clickable' : ''));
            content.attr('beforePosition', '1px;');
            after = node.append('div');
            after.attr('class', 'after');

            if (clickHandler) {
                node.on('click', function() {
                    clickHandler(tipInfo);
                });
            }

            return node;
        }

        function createColorNode(node) {
            if (node) {
                var color = node.append('div');
                color.attr('class', 'ui-tooltip-color');
                return color;
            }
        }

        function createTextNode(node) {
            if (node) {
                var text = node.append('span');
                text.attr('class', 'ui-tooltip-text');
                return text;
            }
        }

        function getNodeEl() {
            if (node === null) {
                node = initNode();
                document.body.appendChild(node.node());
            }
            return d3.select(node.node());
        }

        function calculateArrow(centroid) {
            if (centroid[1] > 0) {
                return 'top';
            } else {
                return 'bottom';
            }
        }

        function calculatePosition(x, y, centroid, content) {
            var size = content.node().getBoundingClientRect(),
                dx, dxDefault = 22;
            if (centroid) {
                if (centroid[0] > 0) {
                    if (centroid[1] > 0) {
                        x = x - size.width + 5;
                        y = Math.max(y - size.height - 13, 0);
                        dx = Math.floor(size.width - dxDefault);
                        before.style('left', dx + 'px');
                        after.style('left', dx + 1 + 'px');
                    } else {
                        x = x - size.width + 5;
                        dx = Math.floor(size.width - dxDefault);
                        before.style('left', dx + 'px');
                        after.style('left', dx + 1 + 'px');
                    }
                } else {
                    if (centroid[1] > 0) {
                        x = x - dxDefault;
                        y = Math.max(y - size.height, 0);
                        before.style('left', '10px');
                        after.style('left', '11px');
                    } else {
                        x = x - dxDefault;
                        before.style('left', '10px');
                        after.style('left', '11px');
                    }
                }
            }
            return {x: Math.floor(x), y: Math.floor(y)};
        }
        tip.getNode = function(){
            return node;
        };
        tip.show = function (x, y, textStr, value, centroid, color, algorithm) {
            tipInfo = {label: textStr, value: value};
            setTimeout(function () {
                initNode();
                getNodeEl().style({display: ''});
                var html = textStr + ': ' + '<b>' + value + '</b>';
                if (clickHandler) {
                    html += '<span class="click-arrow"></span>';
                }

                text.html(html);
                var arrow = algorithm && algorithm.calculateArrow ? algorithm.calculateArrow(centroid) : calculateArrow(centroid);
                var position = algorithm && algorithm.calculatePosition ? algorithm.calculatePosition(x, y, centroid, content, before, after) : calculatePosition(x, y, centroid, content);
                node.style('top', position.y + 'px');
                node.style('left', position.x + 'px');
                node.attr('class', 'ui-tooltip ' + arrow);
                colorNode.style('background', color);
                tip.isShown = true;
            }, 200);

        };
        tip.hide = function () {
            if (!tip.isShown) {
                return;
            }
            getNodeEl().style({display: 'none'});
            tip.isShown = false;
            return tip
        };
        tip.setClickHandler = function(handler) {
            clickHandler = handler;
        };

        return tip;
    };

}));
