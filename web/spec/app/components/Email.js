/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.Email', function () {

    describe('basic render test', function () {
        it('should render element', function () {
            var email = Ext.create('MobileUI.components.Email');
            expect(email.getComponent().element.dom.innerHTML.indexOf('email')).not.toBe(-1);
        });

    });
    describe('multiple attribute test', function () {
        it('should pass multiple option to the element', function () {
            var email = Ext.create('MobileUI.components.Email',{
                multiple: true
            });
            expect(email.getComponent().element.dom.innerHTML.indexOf('multiple')).not.toBe(-1);
        });

    })
});