/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.MCustomAction', function () {

    it('should set html from file and set height', function () {
        var view = new MobileUI.components.CustomViewComponent();
        var config = {
            caption: 'Test facet',
            action: {
                files: ['script.js']
            }
        };

        var api = new MobileUI.core.sandbox.API();

        spyOn(view, 'createSandbox').and.returnValue({
            initApi: Ext.emptyFn,
            include: Ext.emptyFn,
            postMessage: Ext.emptyFn,
            getApi: function () {
                return api;
            }
        });

        view.setParameters(config);

        api.fireEvent('changeHtml', {
            html: '<h1>Title</h1>'
        });

        expect(view.getHtml()).toBe('<h1>Title</h1>');

        api.fireEvent('changeSize', {
            height: 420
        });

        expect(view.getHeight()).toBe(420);
        expect(view.getMaxHeight()).toBe(420);
        expect(view.getMinHeight()).toBe(420);
    });
});