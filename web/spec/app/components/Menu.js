/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.Menu', function () {
    var menu;
    beforeEach(function () {
        spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
            getExtensionById: function () {
                return {}
            }
        });
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({});
        menu = Ext.create('MobileUI.components.Menu');
    });

    it('should open separate dialog for choose entity type in case of phone', function () {
        var createProfileButton = menu.child('button[name=createProfile]');
        var onTap = createProfileButton.getConfig('handler');
        spyOn(Ext.os, 'is');
        spyOn(MobileUI.core.entity.TemporaryEntity, 'addEntity');
        spyOn(MobileUI.core.Navigation, 'redirectForward');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (entityType) {
            if (entityType === 'configuration/entityTypes/HCP') {
                return {
                    uri: 'configuration/entityTypes/HCP',
                    attributes: []
                }
            }
        });
        Ext.os.is.Phone = true;
        spyOn(MobileUI.controller.CreateEntity, 'showChooseEntityTypeDialog').and.callFake(function (shouldInitLookups, listener) {
            listener({uri: 'configuration/entityTypes/HCP'})
        });
        onTap(createProfileButton);
        expect(MobileUI.core.Navigation.redirectForward.calls.mostRecent().args[0]).toContain('edit/entities$uri$$');
        expect(MobileUI.core.entity.TemporaryEntity.addEntity.calls.mostRecent().args[0].type).toBe('configuration/entityTypes/HCP');
    });
});