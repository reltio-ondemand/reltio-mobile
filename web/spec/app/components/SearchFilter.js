/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.SearchFilter', function () {
    describe('basic tests', function () {
        it('should process xss in metadata correctly', function () {
            var SearchFilter = MobileUI.components.SearchFilter;
            var result = SearchFilter.generateHtml([{title: '<h1>title</h1>', labels:['<h1>label1</h1>']}]);
            expect(result).toBe('<div class="filter"><span class="title no-word">&lt;h1&gt;title&lt;/h1&gt;</span><span class="filter-word">:</span><span class="label">&lt;h1&gt;label1&lt;/h1&gt;</span></div>');
        });
    });
});