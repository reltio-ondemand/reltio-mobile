/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.Selectbox', function () {
    describe('basic render test', function () {
        it('should pass multiple option to the element', function () {
            var selectBox = Ext.create('MobileUI.components.Selectbox');
            expect(selectBox.element.classList.indexOf('reltio-selectbox')).not.toBe(-1);
            expect(selectBox.label.dom.innerText).toBe(selectBox.getLabel());
            expect(selectBox.options.dom).toBeDefined();
            expect(selectBox.img.dom).toBeDefined();
        });
    });
    describe('refresh test', function () {
        it('should create select option for each element in options', function () {
            var selectBox = Ext.create('MobileUI.components.Selectbox', {options: [
                {value: 'val1', text:'text1'},
                {value: 'val2', text:'text2'}
            ]});
            expect(selectBox.options.dom.childNodes.length).toBe(2);
            expect(selectBox.options.dom.childNodes[0].text).toBe('text1');
            expect(selectBox.options.dom.childNodes[0].value).toBe('val1');
        });
    });

    describe('change value test', function () {
        it('should fire change value event on setValue method invocation', function (done) {
            var selectBox = Ext.create('MobileUI.components.Selectbox', {options: [
                {value: 'val1', text:'text1'},
                {value: 'val2', text:'text2'}
            ]});
            selectBox.on('change', function(e){
                expect(e).toBe('val2');
                done()
            });
            selectBox.setValue('val2');
        });
    });
});