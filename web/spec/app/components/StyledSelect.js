/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.StyledSelect', function () {
    var editorImpl;
    beforeEach(function () {
        editorImpl = Ext.create('MobileUI.components.StyledSelect', {
            cls: 'rl-inline-editor',
            options:
                [{text: 'First Option', value: 'first'},
                    {text: 'Second Option', value: 'second'},
                    {text: 'Third Option', value: 'third'}],
            forceSelection: false,
            autoSelect: false,
            useHtml: true,
            defaultPhonePickerConfig: {
                cls: 'rl-inline-editor-list'
            },
            defaultTabletPickerConfig: {
                cls: 'rl-inline-editor-list'
            }
        });
    });

    it('should render all values', function () {
        var store = editorImpl.getTabletPicker().getAt(0).getStore();
        expect(store.getCount()).toBe(3);
    });

    it('should choose value correctly', function () {
        var record = editorImpl.getTabletPicker().getAt(0).getStore().getAt(1);
        editorImpl.onListItemSelect(null, record);
        editorImpl.onListTap();
        expect(editorImpl.getValue()).toBe('second');
    });

    it('should unselect value correctly', function () {
        var record = editorImpl.getTabletPicker().getAt(0).getStore().getAt(1);
        editorImpl.onListItemSelect(null, record);
        editorImpl.onListTap();
        expect(editorImpl.getValue()).toBe('second');
        editorImpl.onListItemSelect(null, record);
        editorImpl.onListTap();
        expect(editorImpl.getValue()).toBe(null);
    });
});