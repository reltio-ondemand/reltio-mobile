/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.TripleToggle', function () {
    it('should ignore tap if it is disable', function () {
        var tripleToggle = Ext.create('MobileUI.components.TripleToggle', {
            disabled: true
        });
        var fireEvent = spyOn(tripleToggle, 'fireEvent');
        tripleToggle.onTap();
        expect(fireEvent).not.toHaveBeenCalled();
    });
    it('should fire event if it is not disable', function () {
        var tripleToggle = Ext.create('MobileUI.components.TripleToggle');
        var fireEvent = spyOn(tripleToggle, 'fireEvent');
        tripleToggle.onTap();
        expect(fireEvent).toHaveBeenCalled();
    });
});