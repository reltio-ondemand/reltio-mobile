/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.charts.Chart', function () {

    it('should encode labels in tooltip', function () {
        var chart = Ext.create('MobileUI.components.charts.Chart');
        chart.setData({
            A: [{
                label: '<h1>xss</h1>',
                value: '42'
            }]
        });

        var barChart = chart.createChart('MobileUI.components.charts.BarChart');

        expect(barChart.data.A[0].label).toEqual('&lt;h1&gt;xss&lt;/h1&gt;');
    });
});