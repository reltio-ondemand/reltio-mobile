/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.MShowEditor', function () {
    Ext.define('TestControl', {
        requires: ['MobileUI.components.editablelist.MShowEditor'],
        mixins: {
            showEditor: 'MobileUI.components.editablelist.MShowEditor'
        }
    });

    beforeEach(function () {
        spyOn(Ext.Viewport, 'remove');
        spyOn(Ext.Viewport, 'setActiveItem');
        MobileUI.core.SuiteManager.hideDialogs(true);
    });

    it('should use unique name for each dialog', function () {
        var editor = Ext.create('TestControl');

        editor.showEditor('MobileUI.components.editablelist.editors.NestedAttributeEditor', Ext.emptyFn);
        editor.showEditor('MobileUI.components.editablelist.editors.NestedAttributeEditor', Ext.emptyFn);

        expect(Object.keys(MobileUI.core.SuiteManager.dialogs).length).toBe(2);
    });

    it('should remove dialog on close', function () {
        var editor = Ext.create('TestControl');

        var nested = editor.showEditor('MobileUI.components.editablelist.editors.NestedAttributeEditor', Ext.emptyFn);

        expect(Object.keys(MobileUI.core.SuiteManager.dialogs).length).toBe(1);

        nested.fireEvent('cancel');

        expect(Object.keys(MobileUI.core.SuiteManager.dialogs).length).toBe(0);
    });
});