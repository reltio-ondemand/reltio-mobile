/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.MTransaction', function () {
    var controller;
    beforeEach(function () {
        controller = Ext.create('MobileUI.controller.NewEditMode', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });
    });

    it('should copy value on attribute add in mutator', function (done) {
        var originalObject = {};
        spyOn(window.uiSDK.entity.AttributesEditor, 'addAttribute').and.callFake(function(source, attrType, uri, value){
            expect(value).not.toBe(originalObject);
            done();
        });
        expect(originalObject).toBe(originalObject);
        controller.mutator({},{
            type: MobileUI.components.editablelist.MTransactions.eventTypes.add,
            payload: {
                value: originalObject
            }
        })
    });

    it('should copy value on attribute edit in mutator', function (done) {
        var originalObject = {};
        spyOn(window.uiSDK.entity.AttributesEditor, 'editAttribute').and.callFake(function(source, attrType, uri, value){
            expect(value).not.toBe(originalObject);
            done();
        });
        expect(originalObject).toBe(originalObject);
        controller.mutator({},{
            type: MobileUI.components.editablelist.MTransactions.eventTypes.edit,
            payload: {
                value: originalObject
            }
        })
    });

    it('should update relationship label', function () {
        var attributes = {
            Address: [{
                uri: 'address'
            }]
        };

        controller.mutator(attributes,{
            type: MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel,
            payload: {
                uri: 'address',
                label: 'label',
                relationshipLabel: 'relationshipLabel'
            }
        });

        expect(attributes.Address[0].label).toBe('label');
        expect(attributes.Address[0].relationshipLabel).toBe('relationshipLabel');
    });
});