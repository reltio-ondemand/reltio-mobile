/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.MAttributesPanel', function () {
    var controller;
    var getHeader = function (models) {
        return getHeaders(models)[0];
    };
    var getHeaders = function (models) {
        return models.filter(function (item) {
            return item.type === 'header';
        })
    };
    var getAddButtons = function (models) {
        return models.filter(function (item) {
            return item.type === 'addButton';
        })
    };
    var getAttributes = function (models) {
        return models.filter(function (item) {
            return item.type === 'attribute';
        })
    };
    var getReferenceAttributes = function (models) {
        return models.filter(function (item) {
            return item.type === MobileUI.components.editablelist.items.ListItem.types.referenceAttribute;
        });
    };
    var hasError = function (record) {
        return record && record.error && record.error.value === 'ERROR'
    };

    beforeEach(function () {
        controller = Ext.create('MobileUI.controller.NewEditMode', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });
        controller.launch();
    });

    it('should fill autopopulated dependent lookups', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {
                CountryCode: [{
                    value: ''
                }]
            }
        });

        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(true);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(true);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isTouched').and.returnValue(false);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'getParents').and.returnValue(null);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'getValues').and.callFake(function (lookupCode, editingUri, attrTypeUri, filter, success, failure, self) {
            success.call(self, {
                codeValues: {
                    Country: {
                        RU: {
                            displayName: 'Russia'
                        }
                    }
                }
            })
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'CountryCode',
            dependentLookupCode: 'Country',
            lookupCode: 'Country'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(models.length).toBe(3);
            expect(models[2].attrValue.lookupCode).toBe('RU');
            expect(models[2].attrValue.value).toBe('Russia');
            done();
        });
    });

    it('should show required attribute', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(false);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(false);
        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'CountryCode',
            type: 'String',
            required: true
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(getAttributes(models).length).toBe(1);
            done();
        });
    });

    it('shouldn\'t show metadata driven hidden complex attribute', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(false);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(false);
        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [
            {
                name: 'Nested',
                type: 'Nested',
                label: 'Nested',
                hidden: true
            },
            {
                name: 'Reference',
                type: 'Reference',
                label: 'Reference',
                hidden: false
            }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(getHeaders(models).length).toBe(1);
            expect(getHeader(models).label).toBe('Reference');
            expect(getAttributes(models).length).toBe(0);
            done();
        });
    });

    it('shouldn\'t show configuration driven hidden complex attribute', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(false);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(false);
        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [
            {
                name: 'Nested',
                type: 'Nested',
                label: 'Nested',
                uri: 'test/Nested',
                hidden: false
            },
            {
                name: 'Reference',
                type: 'Reference',
                label: 'Reference',
                uri: 'test/Reference',
                hidden: false
            }];

        controller.getModelsFromSource(entityType, attrTypes, {
            hiddenAttributes: ['test/Reference']
        }).then(function (models) {
            expect(getHeaders(models).length).toBe(1);
            expect(getHeader(models).label).toBe('Nested');
            expect(getAttributes(models).length).toBe(0);
            done();
        });
    });

    it('should set placeholder for empty lookup if it has empty parent and keep label if it has value', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {
                Child2: [{
                    value: 'Child',
                    lookupCode: 'C'
                }],
                Child1: [{
                    value: ''
                }],
                Parent: [{
                    value: ''
                }]
            }
        });

        spyOn(MobileUI.core.Metadata, 'getEntityTypes').and.returnValue({});
        spyOn(MobileUI.core.Metadata, 'getRelationTypes').and.returnValue({});
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(true);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(true);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isTouched').and.returnValue(false);
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'getParents').and.returnValue([{
            getInfo: function () {
                return {
                    values: [],
                    uri: 'uri',
                    dependentLookupCode: 'Parent'
                };
            }
        }]);
        spyOn(MobileUI.core.Metadata, 'findEntityAttributeByUri').and.returnValue({
            label: 'Parent'
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Parent',
            dependentLookupCode: 'Parent'
        }, {
            name: 'Child1',
            dependentLookupCode: 'Child1'
        }, {
            name: 'Child2',
            dependentLookupCode: 'Child12'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(models.length).toBe(5);

            expect(models[3].label).toBe('Please select value for Parent');
            expect(models[3].disabled).toBe(true);

            expect(models[4].label).toBe('Child (C)');
            expect(models[4].disabled).toBe(true);
            done();
        });
    });

    it('should fill autopopulated lookup', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {
                CountryCode: [{
                    value: ''
                }]
            }
        });

        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            isSupportAutoPopulate: function () {
                return true;
            },
            isTouched: function () {
                return false;
            }
        });

        spyOn(MobileUI.core.Lookups, 'getLookups').and.returnValue({
            tree: {
                children: [{
                    displayName: 'Russia',
                    name: 'RU'
                }]
            }
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'CountryCode',
            lookupCode: 'Country'
        }];

        spyOn(controller, 'onListChangeValue').and.callFake(function (data) {
            expect(data.value).toBe('RU');
            expect(data.attrType).toBe(attrTypes[0]);
        });

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(models.length).toBe(3);
            expect(models[2].values.length).toBe(1);
            expect(models[2].attrValue.value).toBe('RU');
            done();
        });
    });

    it('should return "required" and "message" for attribute with cardinality', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'CountryCode',
            type: 'String',
            cardinality: {
                minValue: 1,
                maxValue: 3
            }
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(models.length).toBe(3);
            var isRequired = function (attribute) {
                return attribute.required;
            };
            var hasCardinalityMessage = function (attribute) {
                return attribute.message === 'This attribute can have minimum 1 value and maximum 3 values';
            };
            var requiredAttributes = getAttributes(models).filter(isRequired);
            var withCardinalityMessage = getAttributes(models).filter(hasCardinalityMessage);
            expect(requiredAttributes.length).toBe(1);
            expect(withCardinalityMessage.length).toBe(1);
            done();
        });
    });

    it('should clear attribute for empty dependent lookup selected', function () {
        var attrType = {
            uri: 'attrTypeUri'
        };

        spyOn(controller, 'executeCommandInCurrentBranch').and.callFake(function (type, payload) {
            expect(type).toBe(MobileUI.components.editablelist.MTransactions.eventTypes.edit);
            expect(payload.uri).toBe('uri');
            expect(payload.value.value).toBe('');
            expect(payload.value.lookupCode).toBe('');
        });

        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'removeDependentLookupValue').and.callFake(function (attrTypeUri, uri) {
            expect(attrTypeUri).toBe('attrTypeUri');
            expect(uri).toBe('uri');
        });

        controller.updateDependentLookup(attrType, 'uri', null);
    });

    it('should return "required" and "message" for header with cardinality', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Address',
            label: 'Address',
            type: 'Reference',
            cardinality: {
                minValue: 1,
                maxValue: 3
            }
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(models.length).toBe(2);
            var header = getHeader(models);
            expect(header.required).toBe(true);
            expect(header.message).toBe('This attribute can have minimum 1 value and maximum 3 values');
            done();
        });
    });

    it('should return error for header', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri123',
            value: {}
        });

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Address',
            label: 'Address',
            type: 'Nested'
        }];

        controller.errors = [{
            attrType: attrTypes[0],
            value: 'ERROR'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            var attributesWithError = getAttributes(models).some(hasError);
            expect(getHeader(models).error.value).toBe('ERROR');
            expect(attributesWithError).toBe(false);
            done();
        });
    });

    it('should return error for reference attribute', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri123',
            value: {
                Address: [
                    {
                        uri: 'entities/uri123/attributes/Address/456'
                    }
                ]
            }
        });

        controller.errors = [{
            objectUri: 'entities/uri123/attributes/Address/456/Type/678',
            value: 'ERROR'
        }];

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Address',
            type: 'Nested'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            expect(getAttributes(models)[0].error.value).toBe('ERROR');
            done();
        });
    });

    it('should not add complex required attribute on delete', function (done) {
        spyOn(MobileUI.core.CardinalityChecker, 'isRequired').and.returnValue(true);
        var attrValue = {
            uri: 'nestedUri', value: {
                TestNested: [{
                    String: [{
                        uri: 'test'
                    }]
                }]
            }
        };
        spyOn(controller, 'getSource').and.returnValue(attrValue);
        spyOn(controller, 'executeCommandInCurrentBranch').and.callFake(function (action) {
            expect(action).toBe('remove');
            done();
        });
        var attrType = {
            name: 'TestNested',
            type: 'Nested'
        };

        controller.onListDeleteAttributeInternal(attrType, 'nestedUri');
    });
    it('should not fail on delete for empty attribute', function () {
        spyOn(MobileUI.core.CardinalityChecker, 'isRequired').and.returnValue(true);
        spyOn(controller, 'getSource').and.returnValue({value: {}});
        spyOn(controller, 'executeCommandInCurrentBranch');

        controller.onListDeleteAttributeInternal({name: 'test'}, '');
    });

    it('should clear all values values for reference point to temporary entity', function () {
        var record = {
            get: function (key) {
                if (key === 'attrType') {
                    return {
                        type: 'Reference'
                    }
                } else if (key === 'attrValue') {
                    return {
                        refEntity: {
                            objectURI: 'uri$$refEntityUri'
                        }
                    }
                }
            }
        };
        var lookupManager = {
            removeValuesForAttribute: Ext.emptyFn
        };

        var removeValuesForAttribute = spyOn(lookupManager, 'removeValuesForAttribute');
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue(lookupManager);

        spyOn(MobileUI.core.Metadata, 'getSubAttributes').and.returnValue([]);
        spyOn(controller, 'onListDeleteAttributeInternal');
        spyOn(controller, 'refreshList');
        var onRemove = spyOn(controller, 'actualizeLookupsOnRemove');
        controller.onListDeleteAttribute(record);
        expect(removeValuesForAttribute.calls.mostRecent().args[0]).toBe('uri$$refEntityUri');
        expect(onRemove.calls.mostRecent().args[1].refEntity.objectURI).toBe('uri$$refEntityUri');
    });

    it('shouldn\'t clear values for reference pointed to existing entity', function () {
        var record = {
            get: function (key) {
                if (key === 'attrType') {
                    return {
                        type: 'Reference'
                    }
                } else if (key === 'attrValue') {
                    return {
                        refEntity: {
                            objectURI: 'realUri'
                        }
                    }
                }
            }
        };
        var lookupManager = {
            removeValuesForAttribute: Ext.emptyFn
        };

        var removeValuesForAttribute = spyOn(lookupManager, 'removeValuesForAttribute');
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue(lookupManager);

        spyOn(MobileUI.core.Metadata, 'getSubAttributes').and.returnValue([]);
        spyOn(controller, 'onListDeleteAttributeInternal');
        spyOn(controller, 'refreshList');
        var onRemove = spyOn(controller, 'actualizeLookupsOnRemove');
        controller.onListDeleteAttribute(record);
        expect(removeValuesForAttribute).not.toHaveBeenCalled();
        expect(onRemove.calls.mostRecent().args[1].refEntity.objectURI).toBe('realUri');
    });

    it('should actualize lookups on complex attribute removing', function () {
        var record = {
            get: function (key) {
                if (key === 'attrType') {
                    return {
                        type: 'Nested'
                    }
                } else if (key === 'attrValue') {
                    return {
                        refEntity: {
                            objectURI: 'realUri'
                        }
                    }
                }
            }
        };

        spyOn(MobileUI.core.Metadata, 'getSubAttributes').and.returnValue([]);
        spyOn(controller, 'onListDeleteAttributeInternal');
        spyOn(controller, 'refreshList');
        var onRemove = spyOn(controller, 'actualizeLookupsOnRemove');
        controller.onListDeleteAttribute(record);
        expect(onRemove.calls.mostRecent().args[1].refEntity.objectURI).toBe('realUri');
    });

    it('should remove all children dependent lookups values for parent on lookups hierarchy update', function () {
        var lookupManager = {
            removeValuesForAttribute: Ext.emptyFn
        };
        var removeValuesForAttribute = spyOn(lookupManager, 'removeValuesForAttribute');
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue(lookupManager);
        controller.updateLookupsHierarchy([], {uri: 'someComplexAttr'});
        expect(removeValuesForAttribute.calls.mostRecent().args[0]).toBe('someComplexAttr');
    });

    it('should restore lookup codes from temporary entity as well', function () {
        var lookupsStructure = {};
        var lookupManager = {
            removeValuesForAttribute: function (uri) {
                Object.keys(lookupsStructure)
                    .filter(function (key) {
                        return key.indexOf(uri) === 0;
                    })
                    .forEach(function (key) {
                        delete lookupsStructure[key];
                    });
            },
            updateLookupsHierarchy: function (attrType, payload) {
                lookupsStructure[payload.uri] = payload.lookupCode;
            }
        };

        lookupsStructure['attributes/1/lookup1'] = 'CODE_1';
        lookupsStructure['attributes/1/lookup2'] = 'CODE_2';
        lookupsStructure['attributes/1/lookup3'] = 'CODE_3';
        lookupsStructure['attributes/2/lookup1'] = 'CODE_1';

        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue(lookupManager);

        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue({
            uri: '42',
            attributes: {
                CountryCode: [{
                    uri: 'CountryCode/1',
                    value: 'RU',
                    lookupCode: 'RU'
                }]
            }
        });

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                name: 'CountryCode',
                dependentLookupCode: 'COUNTRY_CODE'
            }]
        });

        var attrValue = {
            uri: 'attributes/1',
            refEntity: {
                type: 'HCP',
                uri: '42'
            }
        };

        controller.updateLookupsHierarchy([], attrValue);

        expect(Object.keys(lookupsStructure).length).toBe(2);
        expect(lookupsStructure['attributes/2/lookup1']).toBe('CODE_1');
        expect(lookupsStructure['CountryCode/1']).toBe('RU');
    });

    it('should remove children dependent lookups on complex attribute remove', function () {
        var lookupManager = {
            removeDependentLookupValue: Ext.emptyFn
        };
        var removeValuesForAttribute = spyOn(lookupManager, 'removeDependentLookupValue');
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue(lookupManager);
        var attrTypes = [
            {
                type: 'Nested',
                name: 'Nested',
                attributes: [
                    {type: 'String', name: 'InnerLookup', dependentLookupCode: 'IL'}
                ]
            },
            {type: 'String', name: 'Lookup', dependentLookupCode: 'L'}
        ];
        var attrValue = {
            value: {
                Lookup: [
                    {
                        uri: 'complex/attributes/Lookup'
                    }
                ],
                Nested: [{
                    uri: 'complex/attributes/Nested',
                    value: {
                        InnerLookup: [
                            {
                                uri: 'complex/attributes/Nested/InnerLookup'
                            }
                        ]
                    }
                }]
            }
        };
        controller.actualizeLookupsOnRemove(attrTypes, attrValue);
        expect(removeValuesForAttribute.calls.count()).toBe(2);
        expect(removeValuesForAttribute.calls.mostRecent().args[1]).toBe('complex/attributes/Lookup');
        expect(removeValuesForAttribute.calls.first().args[1]).toBe('complex/attributes/Nested/InnerLookup');
    });
    it('should return error for attribute based on parent uri', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri123',
            value: {
                Address: [
                    {
                        uri: 'entities/uri123/attributes/Address/456'
                    }
                ]
            }
        });

        controller.errors = [{
            objectParentUri: 'entities/uri123/attributes/Address/456',
            value: 'ERROR',
            invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE
        }];

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Address',
            type: 'Nested'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            var attributesWithError = getAttributes(models).some(hasError);
            expect(attributesWithError).toBe(true);
            done();
        });
    });

    it('shouldn\'t return error for attribute based on attribute type uri for complex attributes', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri123',
            value: {
                Address: [
                    {
                        uri: 'entities/uri123/attributes/Address/456'
                    }
                ]
            }
        });
        var attrType = {
            name: 'Address',
            label: 'Address',
            type: 'Nested'
        };

        controller.errors = [{
            objectParentUri: 'entities/uri123',
            value: 'ERROR',
            attrType: attrType,
            invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE
        }];

        controller.getModelsFromSource({
            label: 'test',
            typeIcon: 'icon'
        }, [attrType]).then(function (models) {
            var attributesWithError = getAttributes(models).some(hasError);
            expect(getHeader(models).error.value).toBe('ERROR');
            expect(attributesWithError).toBe(false);
            done();
        });
    });
    it('should return error for attribute based on attribute type uri for simple attributes', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri123',
            value: {
                Simple: [
                    {
                        uri: 'entities/uri123/attributes/Simple/456'
                    }
                ]
            }
        });
        var attrType = {
            name: 'Simple',
            type: 'String'
        };

        controller.errors = [{
            objectParentUri: 'entities/uri123',
            value: 'ERROR',
            attrType: attrType,
            invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.INCORRECT_VALUE
        }];

        controller.getModelsFromSource({
            label: 'test',
            typeIcon: 'icon'
        }, [attrType]).then(function (models) {
            var attributesWithError = getAttributes(models).some(hasError);
            expect(getHeader(models).error.value).toBe('ERROR');
            expect(attributesWithError).toBe(true);
            done();
        });
    });

    it('should add empty attribute reference structure on attribute add', function (done) {
        var refAttrTypes = [{
            name: 'String', type: 'String'
        }];
        var editor = {
            init: Ext.emptyFn,
            on: Ext.emptyFn,
            executeCommandInCurrentBranch: function (action, payload) {
                expect(Object.keys(payload.value).length).toBe(1);
                expect(payload.value.String[0].value).toBe('');
                done();
            },
            getAttrTypes: function () {
                return refAttrTypes
            },
            initState: Ext.emptyFn
        };
        spyOn(controller, 'showEditor').and.callFake(function (name, cb) {
            cb.call(controller, editor)
        });
        controller.onListAddAttribute({
            record: {
                get: function () {
                    return [
                        {
                            name: 'Reference',
                            type: 'Reference',
                            attributes: refAttrTypes
                        }
                    ]
                }
            }
        });
    });

    it('should add empty attribute nested structure on attribute add', function (done) {
        var refAttrTypes = [{
            name: 'String', type: 'String'
        }];
        var editor = {
            init: Ext.emptyFn,
            on: Ext.emptyFn,
            executeCommandInCurrentBranch: function (action, payload) {
                expect(Object.keys(payload.value).length).toBe(1);
                expect(payload.value.String[0].value).toBe('');
                done();
            },
            getAttrTypes: function () {
                return refAttrTypes
            },
            refreshList: Ext.emptyFn
        };
        spyOn(controller, 'showEditor').and.callFake(function (name, cb) {
            cb.call(controller, editor)
        });
        controller.onListAddAttribute({
            record: {
                get: function () {
                    return [
                        {
                            name: 'Nested',
                            type: 'Nested',
                            attributes: refAttrTypes
                        }
                    ]
                }
            }
        });
    });
    it('should add empty simple attribute for one attribute', function (done) {
        spyOn(controller, 'executeCommandInCurrentBranch').and.callFake(function (action, payload) {
            expect(payload.value).toBe('');
            expect(payload.attrType.name).toBe('String');
            setTimeout(done, 10);
        });
        spyOn(controller, 'showEditor').and.callFake(function (name, cb) {
            cb.call(controller, null)
        });
        spyOn(controller, 'refreshList');
        controller.onListAddAttribute({
            record: {
                get: function () {
                    return [
                        {
                            name: 'String', type: 'String'
                        }
                    ]
                }
            }
        });
    });
    it('should add to attr types attribute which user can create', function () {
        var attrTypes = [
            {
                name: 'String',
                access: ['CREATE', 'UPDATE', 'READ']
            },
            {
                name: 'Blob',
                access: ['READ']
            }
        ];
        var result = controller.createHeader('label', attrTypes);
        expect(result[1].attrTypes.length).toBe(1);
        expect(result[1].attrTypes[0].name).toBe('String');
    });
    it('shouldn\'t add \'add button\' if no attr types', function () {
        var attrTypes = [];
        var result = controller.createHeader('label', attrTypes);
        expect(result.length).toBe(1);
    });

    it('should disable attributes based on metadata permission', function () {
        var attrTypes = [
            {
                name: 'String',
                access: ['CREATE', 'UPDATE', 'READ']
            },
            {
                name: 'Blob',
                access: ['CREATE', 'UPDATE', 'READ', 'DELETE']
            },
            {
                name: 'Reference',
                type: 'Reference',
                access: ['READ']
            }
        ];
        controller.attrValues = {
            String: [{
                access: ['READ'],
                ov: true,
                value: 'test'
            }],
            Blob: [{
                access: ['READ'],
                ov: true,
                value: 'test'
            }],
            Reference: [{
                access: ['READ'],
                ov: true,
                value: 'test'
            }]
        };
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({});
        spyOn(MobileUI.core.Services, 'getBorderlessImageUrl').and.returnValue({});
        var content = controller.createContent(attrTypes[0]);
        expect(content[0].disableSwipe).toBe(true);
        expect(content[0].editable).toBe(true);
        content = controller.createContent(attrTypes[1]);
        expect(content[0].disableSwipe).toBe(false);
        expect(content[0].editable).toBe(true);
        content = controller.createContent(attrTypes[2]);
        expect(content[0].disableSwipe).toBe(true);
        expect(content[0].editable).toBe(false);
    });

    it('should set empty value for the nulled lookup value', function (done) {
        var data = {
            attrType: {
                lookupCode: '1'
            },
            value: null
        };
        spyOn(controller, 'getDoneButton');
        spyOn(controller, 'executeCommandInCurrentBranch').and.callFake(function (type, payload) {
            expect(payload.value.value).toBe('');
            expect(payload.value.lookupCode).toBe('');
            done();
        });
        controller.onListChangeValue(data);

    });

    it('should return label with lookup code and value if they are not the same.', function () {
        var attrValue = {
            lookupCode: 'CA',
            value: 'California'
        };
        expect(controller.getLabelForDependentLookup(attrValue)).toBe('California (CA)');
    });

    it('should return label only if value and lookup code are the same.', function () {
        var attrValue = {
            lookupCode: 'CA',
            value: 'CA'
        };
        expect(controller.getLabelForDependentLookup(attrValue)).toBe('CA');
    });
    describe('create group tests', function(){
        beforeEach(function(){
            controller = Ext.create('MobileUI.controller.NewEditMode', {
                application: Ext.create('Ext.app.Application', {name: 'Test'})
            });
            controller.launch();
            spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isInited').and.returnValue(false);
            spyOn(controller, 'updateDependentLookup');
            spyOn(MobileUI.core.session.Session.getLookupsManager(), 'isSupportAutoPopulate').and.returnValue(false);
        });
        it('should create group for complex attr type with attributes and with create rights', function (done) {
            spyOn(controller, 'getSource').and.returnValue({
                uri: 'entities/uri$$123',
                value: {
                    Nested: [
                        {
                            label: 'test',
                            uri: 'entities/uri$$123/Nested/123'
                        }
                    ]
                }
            });

            var entityType = {
                label: 'test',
                typeIcon: 'icon'
            };

            var attrTypes = [
                {
                    name: 'Nested',
                    type: 'Nested',
                    label: 'Nested',
                    access: ['READ','CREATE','UPDATE','DELETE']
                }];

            controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
                expect(getAddButtons(models).length).toBe(1);
                expect(getHeader(models).label).toBe('Nested');
                expect(getAttributes(models).length).toBe(1);
                done();
            });
        });
        it('should create group for complex attr type without attributes and with create rights', function (done) {
            spyOn(controller, 'getSource').and.returnValue({
                uri: 'entities/uri$$123',
                value: {
                }
            });

            var entityType = {
                label: 'test',
                typeIcon: 'icon'
            };

            var attrTypes = [
                {
                    name: 'Nested',
                    type: 'Nested',
                    label: 'Nested',
                    access: ['READ','CREATE','UPDATE','DELETE']
                }];

            controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
                expect(getAddButtons(models).length).toBe(1);
                expect(getHeader(models).label).toBe('Nested');
                expect(getAttributes(models).length).toBe(0);
                done();
            });
        });
        it('should create group for simple attributes inside group and move other attribute to other section', function (done) {
            spyOn(controller, 'getSource').and.returnValue({
                uri: 'entities/uri$$123',
                value: {
                    Simple1: [
                        {
                            label: 'simple1',
                            uri: 'entities/uri$$123/Simple1/123'
                        }
                    ],
                    Simple2: [
                        {
                            label: 'simple2',
                            uri: 'entities/uri$$123/Simple2/123'
                        }
                    ]
                }
            });

            var entityType = {
                label: 'test',
                typeIcon: 'icon'
            };

            var attrTypes = [
                {
                    name: 'Simple1',
                    type: 'String',
                    label: 'Simple1',
                    access: ['READ','CREATE','UPDATE','DELETE'],
                    uri: 'entityTypes/test/attributes/Simple1'
                },
                {
                    name: 'Simple2',
                    type: 'String',
                    label: 'Simple2',
                    access: ['READ','CREATE','UPDATE','DELETE'],
                    uri: 'entityTypes/test/attributes/Simple2'
                }];

            controller.getModelsFromSource(entityType, attrTypes, {
                attributes: [{
                    label: 'Simple1',
                    content: ['entityTypes/test/attributes/Simple1']
                }]
            }).then(function (models) {
                expect(getHeaders(models).map(function(model){
                    return model.label
                })).toEqual(['Simple1', 'Other']);
                expect(getAddButtons(models).length).toBe(2);
                expect(getAttributes(models).length).toBe(2);
                done();
            });
        });
        it('should\'t show empty groups', function (done) {
            spyOn(controller, 'getSource').and.returnValue({
                uri: 'entities/uri$$123',
                value: {
                    Simple1: [
                        {
                            label: 'simple1',
                            uri: 'entities/uri$$123/Simple1/123'
                        }
                    ]
                }
            });

            var entityType = {
                label: 'test',
                typeIcon: 'icon'
            };

            var attrTypes = [
                {
                    name: 'Simple1',
                    type: 'String',
                    label: 'Simple1',
                    access: ['READ','CREATE','UPDATE','DELETE'],
                    uri: 'entityTypes/test/attributes/Simple1'
                },
                {
                    name: 'Simple2',
                    type: 'String',
                    label: 'Simple2',
                    access: ['READ'],
                    uri: 'entityTypes/test/attributes/Simple2'
                }];

            controller.getModelsFromSource(entityType, attrTypes, {
                attributes: [{
                    label: 'Simple1',
                    content: ['entityTypes/test/attributes/Simple1']
                }]
            }).then(function (models) {
                expect(getHeaders(models).map(function(model){
                    return model.label
                })).toEqual(['Simple1']);
                expect(getAddButtons(models).length).toBe(1);
                expect(getAttributes(models).length).toBe(1);
                done();
            });
        });

    });
    it('should flatten tree correctly', function(){
       var lookup = {
           name: 'test',
           children: [{
               name: 'test.child1'
           }, {
               name: 'test.child2'
           }]
       };
       var tree = [];
        controller.flattenTree(tree, lookup);
        expect(tree.map(function(item){return item.text})).toEqual([ 'test', 'test.child1', 'test.child2' ]);
    });

    it('should show attributes with only CREATE & READ permission in create mode', function() {
        var attrType = 
        {
            name: 'Simple1',
            type: 'String',
            label: 'Simple1',
            access: ['CREATE', 'READ'],
            uri: 'entityTypes/test/attributes/Simple1'
        };

        controller.attrValues = {
            Simple1: [{
                uri: 'uri$$1',
                value: ''
            }]
        };

        var content = controller.createContent(attrType, attrType, 'uri$$123');
        expect(content.length).toBe(1);
    });

    it('should not show attributes with only CREATE permission in create mode', function() {
        var attrType = 
        {
            name: 'Simple1',
            type: 'String',
            label: 'Simple1',
            access: ['CREATE'],  // read permission lacking
            uri: 'entityTypes/test/attributes/Simple1'
        };

        controller.attrValues = {
            Simple1: [{
                uri: 'uri$$1',
                value: ''
            }]
        };

        var content = controller.createContent(attrType, attrType, 'uri$$123');
        expect(content.length).toBe(0);
    });

    it('should hide attributes without CREATE permission for create mode', function () {
        var attrType = {
            label: 'Nested',
            type: 'Nested',
            attributes: [{
                name: 'String1',
                type: 'String',
                label: 'String1',
                access: ['CREATE', 'UPDATE', 'READ']
            }, {
                name: 'String2',
                type: 'String',
                label: 'String2',
                access: ['READ']
            }]
        };

        controller.attrValues = {
            String1: [{
                uri: 'uri$$1',
                value: ''
            }],
            String2: [{
                uri: 'uri$$2',
                value: ''
            }]
        };

        var content = controller.createContent(attrType.attributes[0], attrType, 'uri$$123');
        expect(content.length).toBe(1);
        content = controller.createContent(attrType.attributes[1], attrType, 'uri$$123');
        expect(content.length).toBe(0);
    });

    it('Attribute permissions should take into account parent permission', function () {
        var attrType = {
            label: 'Nested',
            type: 'Nested',
            access: ['READ', 'CREATE'],
            attributes: [{
                name: 'String1',
                type: 'String',
                label: 'String1',
                access: ['CREATE', 'UPDATE', 'READ']
            }, {
                name: 'String2',
                type: 'String',
                label: 'String2',
                access: ['READ']
            }]
        };

        var header = controller.createHeader('label', attrType.attributes, attrType);
        expect(getAddButtons(header).length).toBe(1);
    });

    it('Should hide add button if parent is null', function () {
        var attrType = {
            label: 'Nested',
            type: 'Nested',
            access: ['READ'],
            attributes: [{
                name: 'String1',
                type: 'String',
                label: 'String1',
                access: ['CREATE', 'UPDATE', 'READ']
            }, {
                name: 'String2',
                type: 'String',
                label: 'String2',
                access: ['READ']
            }]
        };

        var header = controller.createHeader('label', attrType.attributes, attrType);
        expect(getAddButtons(header).length).toBe(0);
    });

    it('should encode referenced entity type label', function (done) {
        spyOn(controller, 'updateDependentLookup');
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {
                Reference: [{
                    label: 'test',
                    uri: 'entities/uri$$123/Simple1/123'
                }]
            }
        });
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            label: 'Address<h1>xss</h1>'
        });
        spyOn(MobileUI.core.Services, 'getBorderlessImageUrl').and.returnValue('url');

        var entityType = {
            label: 'test',
            typeIcon: 'icon'
        };

        var attrTypes = [{
            name: 'Reference',
            type: 'Reference',
            label: 'Reference'
        }];

        controller.getModelsFromSource(entityType, attrTypes).then(function (models) {
            var attribute = getReferenceAttributes(models)[0];
            expect(attribute.typeBadge.value).toBe('Address&lt;h1&gt;xss&lt;/h1&gt;');
            done();
        });
    });

    it('should return header for nested inside reference and no header for reference itself', function (done) {
        spyOn(controller, 'getSource').and.returnValue({
            uri: 'entities/uri$$123',
            value: {}
        });

        var referenceAttrType = {
            label: 'Address',
            type: 'Reference'
        };

        var attrTypes = [{
            type: 'Nested',
            label: 'Nested',
            attributes: [{
                type: 'String'
            }]
        }];

        controller.getModelsFromSource(referenceAttrType, attrTypes).then(function (models) {
            expect(getHeaders(models).length).toBe(1);
            var header = getHeader(models);
            expect(header.label).toBe('Nested');
            done();
        });
    });
});