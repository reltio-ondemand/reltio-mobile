/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var getRequired = function (model) {
    return model.filter(function (item) {
        return item.get('required')
    });
};
describe('MobileUI.components.editablelist.editors.ChooseAttributeType', function () {

    describe('choose attribute dialog test', function () {
        it('should add required field in the model for the required attributes', function () {
            var chooseAttribute = Ext.create('MobileUI.components.editablelist.editors.ChooseAttributeType');
            chooseAttribute.init([{label: 'Normal'},
                {label: 'Required', required: true}]);
            var model = chooseAttribute.getList().getStore().getData();
            expect(getRequired(model).length).toBe(1);
            expect(getRequired(model)[0].get('label')).toBe('Required')
        });
        it('should add required field in the model for the minCardinality=1 attributes', function () {
            var chooseAttribute = Ext.create('MobileUI.components.editablelist.editors.ChooseAttributeType');
            chooseAttribute.init([{label: 'Normal'},
                {label: 'Cardinality', cardinality: {minValue: 1}}]);
            var model = chooseAttribute.getList().getStore().getData();
            expect(getRequired(model).length).toBe(1);
            expect(getRequired(model)[0].get('label')).toBe('Cardinality')
        });
    });
});