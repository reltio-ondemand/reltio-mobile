/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.CreateEntityCmp', function () {
    var getHeader = function (models) {
        return models.filter(function (item) {
            return item.type === 'header';
        })[0]
    };
    var getAttributes = function (models) {
        return models.filter(function (item) {
            return item.type === 'attribute';
        })
    };
    var hasError = function (record) {
        return record && record.error && record.error.value === 'ERROR'
    };

    it('should show data based on temp entity', function (done) {
        var entityType = {
            attributes: [
                {
                    name: 'String',
                    type: 'String'
                }
            ]
        };
        var source = {
            uri: 'entities/uri$$temp',
            value: {
                String: [
                    {
                        uri: 'entities/uri$$temp/attributes/S1',
                        value: '1'
                    }
                ]
            }
        };
        var createEntityCmp = Ext.create('MobileUI.components.editablelist.editors.CreateEntityCmp');
        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue(source);
        spyOn(createEntityCmp, 'initTransaction');
        spyOn(createEntityCmp, 'createTransaction');
        spyOn(createEntityCmp, 'updateListHeight');
        spyOn(createEntityCmp, 'getSource').and.returnValue(source);
        var store = {
            clearData: Ext.emptyFn,
            add: function (results) {
                expect(getHeader(results)).toBeUndefined();
                expect(getAttributes(results).length).toBe(1);
                setTimeout(done,1000);
            }
        };

        spyOn(createEntityCmp, 'getList').and.returnValue({
            getStore: function () {
                return store
            },
            refresh: Ext.emptyFn
        });

        createEntityCmp.init(entityType, source.uri);
    });
    it('should show external errors correctly', function (done) {
        var entityType = {
            attributes: [
                {
                    name: 'String',
                    type: 'String'
                }
            ]
        };
        var source = {
            uri: 'entities/uri$$temp',
            value: {
                String: [
                    {
                        uri: 'entities/uri$$temp/attributes/String/S1',
                        value: '1'
                    }
                ]
            }
        };
        var errors = [{
            objectUri: 'entities/uri123/attributes/Address/456/String/S1',
            value: 'ERROR'
        }];

        var createEntityCmp = Ext.create('MobileUI.components.editablelist.editors.CreateEntityCmp');
        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue(source);
        spyOn(createEntityCmp, 'initTransaction');
        spyOn(createEntityCmp, 'createTransaction');
        spyOn(createEntityCmp, 'updateListHeight');
        spyOn(createEntityCmp, 'getSource').and.returnValue(source);
        var store = {
            clearData: Ext.emptyFn,
            add: function (results) {
                expect(getHeader(results)).toBeUndefined();
                expect(getAttributes(results).filter(hasError).length).toBe(1);
                setTimeout(done,1000);
            }
        };

        spyOn(createEntityCmp, 'getList').and.returnValue({
            getStore: function () {
                return store
            },
            refresh: Ext.emptyFn
        });

        createEntityCmp.init(entityType, source.uri, 'entities/uri123/attributes/Address/456', errors);
    });
    it('should show DL error correctly', function (done) {
        var entityType = {
            attributes: [
                {
                    name: 'String',
                    type: 'String'
                }
            ]
        };
        var source = {
            uri: 'entities/uri$$temp',
            value: {
                String: [
                    {
                        uri: 'entities/uri$$temp/attributes/String/S1',
                        value: '1'
                    }
                ]
            }
        };
        var errors = [{
            objectUri: 'entities/uri123/attributes/Address/456/attributes/String/S1',
            value: 'ERROR'
        }];

        var createEntityCmp = Ext.create('MobileUI.components.editablelist.editors.CreateEntityCmp');
        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue(source);
        spyOn(createEntityCmp, 'initTransaction');
        spyOn(createEntityCmp, 'createTransaction');
        spyOn(createEntityCmp, 'updateListHeight');
        spyOn(createEntityCmp, 'getSource').and.returnValue(source);
        var store = {
            clearData: Ext.emptyFn,
            add: function (results) {
                expect(getHeader(results)).toBeUndefined();
                expect(getAttributes(results).filter(hasError).length).toBe(1);
                setTimeout(done,1000);
            }
        };

        spyOn(createEntityCmp, 'getList').and.returnValue({
            getStore: function () {
                return store
            },
            refresh: Ext.emptyFn
        });

        createEntityCmp.init(entityType, source.uri, 'entities/uri123/attributes/Address/456', errors);
    });
});