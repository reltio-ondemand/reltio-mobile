/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.DependentLookupValues', function () {

    it('should deselect item correctly', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.DependentLookupValues');
        var response = {
            codeValues: {
                'test': {
                    'value1': {
                        lookupCode: 'value1',
                        displayName: 'displayName1'
                    },
                    'value2': {
                        lookupCode: 'value2',
                        displayName: 'displayName2'
                    }
                }
            }
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getValues: function (code, valueUri, typeUri, filter, cb, option, self) {
                cb.call(self, response)
            }
        });
        var attrType = {dependentLookupCode: 'test'};
        editor.init(attrType, {lookupCode: 'value2'});
        expect(editor.selectedRecord).not.toBeNull();
        editor.onListSelectItem({
            record: {
                get: function () {
                    return {
                        lookupCode: 'value2',
                        displayName: 'displayName2'
                    }
                }
            }
        });
        expect(editor.selectedRecord).toBeNull();
    });

    it('should set title correctly', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.DependentLookupValues');
        spyOn(editor, 'getLookups');
        var setTitle = spyOn(editor, 'setTitle');
        editor.init({label: 'test'}, {lookupCode: 'LC'});
        expect(setTitle.calls.mostRecent().args[0]).toBe('test');
    });

});