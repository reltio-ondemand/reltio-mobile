/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.EntitySelector', function () {

    it('should collect referenceAttributeURI entity attributes only', function (done) {
        var selector = Ext.create('MobileUI.components.editablelist.editors.EntitySelector');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [
                {
                    name: 'inReference',
                    uri: 'uri/inReference'
                },
                {
                    name: 'notInReference',
                    uri: 'uri/notInReference'
                }
            ]
        });
        selector.on('done', function (entityType) {
            expect(Object.keys(entityType.attributes).length).toBe(1);
            expect(entityType.attributes['inReference']).toBeDefined();
            done();
        });
        var attrType = {
            referencedAttributeURIs: ['uri/inReference']
        };
        selector.init(attrType);
        selector.onAddNewEntityTap();
    });

    it('should escape entity type label', function () {
        var selector = Ext.create('MobileUI.components.editablelist.editors.EntitySelector');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            label: '<h1>xss</h1>'
        });
        var attrType = {
            referencedAttributeURIs: ['uri/inReference']
        };
        selector.init(attrType);

        expect(selector.getTitle().getHtml()).toBe('Add &lt;h1&gt;xss&lt;/h1&gt;');
    });

    it('should restore typeahead state', function () {
        MobileUI.core.search.SearchParametersManager.setTypeAhead('old');

        var selector = Ext.create('MobileUI.components.editablelist.editors.EntitySelector');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            label: '123'
        });
        var attrType = {
            referencedAttributeURIs: ['uri/inReference']
        };
        selector.init(attrType);

        spyOn(selector, 'doSearch');
        spyOn(selector, 'getSearchField').and.returnValue({
            getValue: function () {
                return 'new';
            }
        });

        selector.onKeyUp();

        expect(MobileUI.core.search.SearchParametersManager.getTypeAhead()).toBe('new');

        selector.fireEvent('cancel');

        expect(MobileUI.core.search.SearchParametersManager.getTypeAhead()).toBe('old');
    });
});