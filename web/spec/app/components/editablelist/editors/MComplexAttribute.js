/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.MComplexAttribute', function () {

    it('should update reference attribute label', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        editor.attrType = {
            type: 'Reference',
            relationshipLabelPattern: '{Name}'
        };
        editor.attrValue = {
            uri: '123'
        };
        editor.branch = {
            save: Ext.emptyFn
        };

        spyOn(editor, 'getAttrTypes');
        spyOn(MobileUI.core.session.Session, 'validateAttributes').and.returnValue([]);
        spyOn(editor, 'getSource').and.returnValue({
            label: 'Test',
            value: {
                Name: [{
                    value: 'Name'
                }]
            }
        });

        spyOn(editor, 'executeCommandInCurrentBranch').and.callFake(function (type, payload) {
            expect(type).toBe(MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel);
            expect(payload.label).toBe('Test');
            expect(payload.relationshipLabel).toBe('Name ');
        });

        editor.onSaveButtonTap();
    });

    it('should show correct title for editing attribute procedure', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        spyOn(editor, 'initTransaction');
        spyOn(editor, 'getHeader').and.returnValue({
            setHtml: function (title) {
                expect(title).toBe('Edit attr type label');
                done();
            }
        });
        var attrType = {
            label: 'attr type label'
        };
        var value = {
            uri: '123',
            value: {
                existingValue: []
            }
        };
        editor.init(attrType, value);
    });

    it('should show correct title for creating new attribute procedure', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        spyOn(editor, 'initTransaction');
        spyOn(editor, 'getHeader').and.returnValue({
            setHtml: function (title) {
                expect(title).toBe('Create attr type label&lt;h1&gt;xss&lt;/h1&gt;');
                done();
            }
        });
        var attrType = {
            label: 'attr type label<h1>xss</h1>'
        };
        var value = {
            uri: '123'
        };
        editor.init(attrType, value);
    });

    it('should pass parentUri to validation', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        editor.attrType = {
            type: 'Reference',
            relationshipLabelPattern: '{Name}'
        };
        editor.attrValue = {
            uri: '123'
        };
        editor.branch = {
            save: Ext.emptyFn
        };

        spyOn(editor, 'getAttrTypes');
        var spy = jasmine.createSpy('spy');

        spyOn(MobileUI.core.session.Session, 'validateAttributes').and.callFake(function(value, attrTypes, parentUri){
            expect(parentUri).toBe(editor.attrValue.uri);
            spy();
            return [{}]
        });
        spyOn(editor, 'getSource').and.returnValue({
            label: 'Test',
            value: {
                Name: [{
                    value: 'Name'
                }]
            }
        });
        spyOn(editor, 'showValidationErrors');
        spyOn(editor, 'refreshList');
        editor.onSaveButtonTap();
        expect(spy).toHaveBeenCalled();
    });
    it('should disable save button during save process for complex editors', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        editor.attrType = {
            type: 'Reference',
            relationshipLabelPattern: '{Name}'
        };
        editor.attrValue = {
            uri: '123'
        };
        editor.branch = {
            save: Ext.emptyFn
        };

        spyOn(editor, 'getAttrTypes');
        spyOn(MobileUI.core.session.Session, 'validateAttributes').and.returnValue([]);
        spyOn(editor, 'getSource').and.returnValue({
            label: 'Test',
            value: {
                Name: [{
                    value: 'Name'
                }]
            }
        });
        var doneButton = {
            setDisabled: function(){}
        };
        var setDisabled = spyOn(doneButton, 'setDisabled');
        spyOn(editor, 'getDoneButton').and.returnValue(doneButton);
        spyOn(editor, 'executeCommandInCurrentBranch');

        editor.onSaveButtonTap();
        expect(setDisabled).toHaveBeenCalledWith(true);
    });

    it('should disable cancel button after tap', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        editor.branch = {
            remove: Ext.emptyFn
        };
        editor.attrType = {
            type: 'Reference',
            referencedAttributeURIs: []
        };

        editor.onCancelButtonTap();

        expect(editor.getCancel().isDisabled()).toBe(true);
    });
});