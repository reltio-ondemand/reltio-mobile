/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.NestedAttributeEditor', function () {

    describe('done behaviour test', function () {
        it('should be enabled on data change', function () {
            var nestedEditor = Ext.create('MobileUI.components.editablelist.editors.NestedAttributeEditor');
            spyOn(nestedEditor, 'getDoneButton').and.returnValue({
                state: true,
                isDisabled: function () {
                    return this.state
                },
                setDisabled: function (state) {
                    this.state = state
                }
            });
            expect(nestedEditor.getDoneButton().isDisabled()).toBe(true);
            spyOn(nestedEditor, 'executeCommandInCurrentBranch');
            nestedEditor.onListChangeValue({attrType: {type: 'String'}});
            expect(nestedEditor.getDoneButton().isDisabled()).toBe(false);
        });
    });
});