/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.ReferenceAttributeEditor', function () {

    it('should send attributes for validation', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var attrType = {
                referencedAttributeURIs: [
                    'configuration/relationTypes/A/attributes/B'
                ]
            },
            branch = {
                branch: function () {
                    return {save: Ext.emptyFn}
                }
            };

        editor.init(attrType, {}, branch);

        spyOn(editor, 'getSource').and.returnValue({
            value: 42
        });

        spyOn(editor, 'executeCommandInCurrentBranch');

        spyOn(MobileUI.core.Metadata, 'findRelationAttributeByUri').and.returnValue({
            name: 'name',
            uri: 'uri'
        });

        spyOn(MobileUI.core.session.Session, 'validateAttributes').and.callFake(function (attributes, attrTypes) {
            expect(attrTypes.length).toBe(1);
            done();
            return [];
        });

        editor.onSaveButtonTap();
    });
    it('should build correct uris for validation request', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var branch = {
            branch: Ext.emptyFn
        };
        editor.attrValue = {
            uri: 'refUri'
        };
        editor.initTransaction('test/parentUri', branch);
        spyOn(editor, 'getSource').and.returnValue({});
        spyOn(editor, 'showEditor').and.returnValue({
            on: function (action, cb) {
                if (action === 'done') {
                    cb.call(editor, {
                        uri: 'entityUri',
                        type: 'entityType'
                    })
                }
            }
        });
        spyOn(editor, 'refreshList');
        spyOn(editor, 'refreshEntitySelector');
        spyOn(editor, 'executeCommandInCurrentBranch').and.callFake(function (action, payload) {
            expect(payload.value.refRelation.objectURI).toBe('relation/parentUri');
            done();
        });
        editor.showEntitySelector()
    });
    it('should show correct label and entity creator in case of temporary entity', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var branch = {
            branch: Ext.emptyFn
        };
        editor.attrValue = {
            uri: 'refUri'
        };
        editor.attrType = {
            label: 'Address'
        };
        spyOn(MobileUI.core.entity.EntityUtils, 'isTemporaryUri').and.returnValue(true);
        spyOn(editor, 'getSource').and.returnValue({
            refEntity: {objectURI: 'entities/uri$$test'},
            label: 'entityLabel'
        });
        spyOn(editor, 'initEntityCreator');
        editor.initTransaction('test/parentUri', branch);
        editor.refreshEntitySelector();
        expect(editor.getEntityCreator().isHidden()).toBe(false);
        var model = editor.getEntitySelectorComponent().getStore().getData();
        var entityRecord = model.filter(function (item) {
            return item.get('type') === 'entityInfo'
        })[0];
        expect(entityRecord.get('label')).toBe('New entity will be created');
    });

    it('should register mapping for referenced entity', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var branch = {
            branch: Ext.emptyFn
        };
        editor.attrValue = {
            uri: 'refUri'
        };
        editor.attrType = {
            label: 'Address'
        };
        spyOn(MobileUI.core.entity.EntityUtils, 'isTemporaryUri').and.returnValue(true);
        spyOn(editor, 'getSource').and.returnValue({
            refEntity: {objectURI: 'entities/uri$$test'},
            label: 'entityLabel',
            uri: editor.attrValue.uri
        });
        spyOn(editor, 'initEntityCreator');
        spyOn(MobileUI.core.session.Session, 'getUriMapping').and.returnValue({
            register: function (entityUri, refAttrUri) {
                expect(entityUri).toBe('entities/uri$$test');
                expect(refAttrUri).toBe('refUri');
                done();
            }
        });
        editor.initTransaction('test/parentUri', branch);

        editor.refreshEntitySelector();

    });

    it('should show entity label and no entity creator in case of existed entity', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var branch = {
            branch: Ext.emptyFn
        };
        editor.attrValue = {
            uri: 'refUri'
        };
        editor.attrType = {
            label: 'Address'
        };
        spyOn(MobileUI.core.entity.EntityUtils, 'isTemporaryUri').and.returnValue(false);
        spyOn(editor, 'getSource').and.returnValue({
            refEntity: {objectURI: 'entities/wrealEntity'},
            label: 'entityLabel'
        });
        spyOn(editor, 'initEntityCreator');
        editor.initTransaction('test/parentUri', branch);
        editor.refreshEntitySelector();
        expect(editor.getEntityCreator().isHidden()).toBe(true);
        var model = editor.getEntitySelectorComponent().getStore().getData();
        var entityRecord = model.filter(function (item) {
            return item.get('type') === 'entityInfo'
        })[0];
        expect(entityRecord.get('label')).toBe('entityLabel');
    });
    it('should validate both relation attributes and entity attribute on save', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        spyOn(editor, 'getSource').and.returnValue({
            value: {}
        });
        spyOn(editor, 'getAttrTypes').and.returnValue([]);
        spyOn(editor, 'isEntityCreatorExists').and.returnValue(true);
        spyOn(editor, 'showValidationErrors');
        spyOn(editor, 'refreshList');
        spyOn(editor, 'getEntityCreator').and.returnValue({
            validate: function () {
                return ['errorFromCreator']
            }
        });
        editor.onReferenceSaveButtonTap();
        expect(editor.errors.indexOf('errorFromCreator')).not.toBe(-1);
    });

    it('should call save in entity creator and update label', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        var entityCreator = {
            onSaveAction: Ext.emptyFn,
            getEntity: function () {
                return {
                    label: 'entityLabel'
                }
            }
        };
        var saveAction = spyOn(entityCreator, 'onSaveAction');
        editor.attrValue = {};
        spyOn(editor, 'onSaveButtonTap');
        spyOn(editor, 'validate').and.returnValue([]);
        spyOn(editor, 'isEntityCreatorExists').and.returnValue(true);
        spyOn(editor, 'getEntityCreator').and.returnValue(entityCreator);
        spyOn(editor, 'executeCommandInCurrentBranch').and.callFake(function (action, payload) {
            expect(saveAction.calls.count()).toBe(1);
            expect(action).toBe(MobileUI.components.editablelist.MTransactions.eventTypes.updateLabel);
            expect(payload.label).toBe('entityLabel');
            done();
        });
        editor.onReferenceSaveButtonTap();
    });
    it('should hide entity selector before the reference attribute editor', function (done) {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({});
        spyOn(editor, 'getSource').and.returnValue({
            value: {}
        });
        editor.attrType = {
            label: 'Address'
        };

        var results = [];

        function checkResults(name) {
            results.push(name);
            if (results.length === 2) {
                expect(results[0]).toBe('closeSelector');
                done();
            }
        }

        spyOn(editor, 'cancel').and.callFake(function () {
            checkResults('closeReference');
        });
        spyOn(editor, 'closeEditor').and.callFake(function () {
            checkResults('closeSelector');
        });
        var selector = editor.showEntitySelector();
        selector.fireEvent('cancel');

    });

    it('should filter buttons based on permission', function () {
        var editor = Ext.create('MobileUI.components.editablelist.editors.ReferenceAttributeEditor');
        editor.attrType = {
            label: 'Nested',
            type: 'Nested',
            access: ['READ', 'CREATE'],
            attributes: []
        };
        expect(editor.child('elist[name=entity-selector]').getPlugins()[0].getFilterButtons()()).toBeTruthy();
        editor.attrType = {
            label: 'Nested',
            type: 'Nested',
            access: ['READ'],
            attributes: []
        };
        expect(editor.child('elist[name=entity-selector]').getPlugins()[0].getFilterButtons()()).toBeFalsy();
    });
});