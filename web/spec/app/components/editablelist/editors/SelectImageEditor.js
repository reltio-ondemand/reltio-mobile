/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.SelectImageEditor', function () {
    var getImageRecords = function (model) {
        return model.filter(function (item) {
            return item.get('type') === 'imageItem'
        });
    };
    var hasImage = function (image) {
        return function (item) {
            return item.get('avatar') === image;
        }
    };
    var entityType = {
        attributes: [
            {
                uri: 'test/String',
                name: 'String',
                type: 'String'
            },
            {
                uri: 'test/ImageStringUrl',
                name: 'ImageStringUrl',
                type: 'String'
            },
            {
                uri: 'test/ImageUrl',
                name: 'Image Url',
                type: 'Image URL'
            }
        ],
        imageAttributeURIs: ['test/ImageStringUrl']
    };
    it('should show all images attributes from entity in list', function () {
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(entityType);
        var entity = {
            attributes: {
                String: [{
                    value: 'testString',
                    type: 'test/String',
                    uri: 'entities/test/attributes/String/1'
                }],
                ImageStringUrl: [{
                    value: 'https://reltio.com/ImageStringUrl',
                    type: 'test/ImageStringUrl',
                    uri: 'entities/test/attributes/ImageStringUrl/1'
                }],
                ImageUrl: [{
                    value: 'https://reltio.com/ImageUrl',
                    type: 'test/ImageUrl',
                    uri: 'entities/test/attributes/ImageUrl/1'
                }]
            }
        };
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            imagePath: 'https://reltio.com/'
        });
        var selectImage = Ext.create('MobileUI.components.editablelist.editors.SelectImageEditor');
        var branch = {
            branch: function () {
                return {
                    save: Ext.emptyFn, getCurrentProjection: function () {
                        return entity.attributes
                    }
                }
            }
        };
        selectImage.init(entity, branch);
        selectImage.refreshList();

        var model = selectImage.getList().getStore().getData();
        var imageRecords = getImageRecords(model);
        expect(imageRecords.length).toBe(2);
        expect(imageRecords.filter(hasImage('https://reltio.com/ImageUrl')).length).toBe(1);
        expect(imageRecords.filter(hasImage('https://reltio.com/ImageStringUrl')).length).toBe(1);
    });
    describe('interaction tests', function () {
        var value = {
            value: 'https://reltio.com/ImageUrl.jpg',
            uri: 'entities/test/attributes/ImageUrl/1'
        };
        var anotherValue = {
            value: 'https://reltio.com/ImageUrl2',
            uri: 'entities/test/attributes/ImageUrl/12'
        };
        var entity = {
            attributes: {
                ImageUrl: [value, anotherValue]
            },
            crosswalks: [{attributes:[anotherValue.uri], createDate:''}],
            defaultProfilePic: 'entities/test/attributes/ImageUrl/1'
        };
        var selectImage;

        beforeEach(function () {
            spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
                imagePath: 'https://reltio.com/'
            });
            selectImage = Ext.create('MobileUI.components.editablelist.editors.SelectImageEditor');

            var branch = {
                branch: function () {
                    return {
                        save: Ext.emptyFn,
                        getCurrentProjection: function () {
                            return entity.attributes
                        }
                    }
                }
            };
            spyOn(MobileUI.core.entity.EntityUtils, 'getImageSourcesNew').and.returnValue([
                value, anotherValue
            ]);

            selectImage.init(entity, branch);
        });

        it('should show default profile pic as selected', function () {
            selectImage.refreshList();

            var model = selectImage.getList().getStore().getData();
            var imageRecords = getImageRecords(model);
            var isSelected = function (item) {
                return item.get('state') === 'selected';
            };
            var record = imageRecords.filter(isSelected);
            expect(record.length).toBe(1);
        });

        it('should allow to change default profile pic', function () {
            selectImage.refreshList();
            selectImage.onListSelectItem({
                record: {get: function(key){
                    return key === 'uri' && anotherValue.uri
                }}
            });

            var model = selectImage.getList().getStore().getData();
            var imageRecords = getImageRecords(model);

            var isSelected = function (item) {
                return item.get('state') === 'selected';
            };
            var record = imageRecords.filter(isSelected);
            expect(record[0].get('avatar')).toBe(anotherValue.value);
        });

        it('should add attribute to first image attribute type', function (done) {
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(entityType);
            var newImageUrl = 'http:/reltio.com/newImage';
            spyOn(selectImage,'showEditor').and.returnValue({
                on: function(action, cb,self){
                    cb.call(self, newImageUrl);
                }
            });
            spyOn(selectImage, 'executeCommandInCurrentBranch').and.callFake(function(action, payload){
                expect(payload.attrType.uri).toBe(entityType.attributes[1].uri);
                expect(payload.value).toBe(newImageUrl);
                done();
            });
            selectImage.onListAddAttribute();
        });

        it('should remove attribute correctly', function (done) {
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(entityType);
            spyOn(selectImage, 'executeCommandInCurrentBranch').and.callFake(function(action, payload){
                expect(payload.attrType.uri).toBe(entityType.attributes[1].uri);
                expect(payload.uri).toBe(anotherValue.uri);
                done();
            });
            selectImage.onListDeleteAttribute(
                {
                    get: function (key) {
                        return key === 'uri' && anotherValue.uri
                    }
                }
            );
        });
    });
});