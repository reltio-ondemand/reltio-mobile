/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.editors.UploadImage', function () {

    it('should set url to uploaded file in the text field', function () {
        var uploadImage = Ext.create('MobileUI.components.editablelist.editors.UploadImage');
        uploadImage.initUploader();
        expect(uploadImage.getUrlText().getValue()).toBe('');
        uploadImage.onImageUploaded({url:'http/test.reltio.com'});
        expect(uploadImage.getUrlText().getValue()).toBe('http/test.reltio.com');
    });
    it('should show an error on save if image is not set', function (done) {
        var uploadImage = Ext.create('MobileUI.components.editablelist.editors.UploadImage');
        uploadImage.initUploader();
        spyOn(MobileUI.components.MessageBox, 'alert').and.callFake(function(title){
            expect(title).toBeDefined();
            done();
        });
        uploadImage.onSaveButtonTap();
    });

    it('should fire done event with url on save', function (done) {
        var uploadImage = Ext.create('MobileUI.components.editablelist.editors.UploadImage');
        uploadImage.initUploader();
        uploadImage.onImageUploaded({url:'http/test.reltio.com'});
        uploadImage.on('done',function(url){
            expect(url).toBe('http/test.reltio.com');
            done();
        });
        uploadImage.onSaveButtonTap();
    });
});