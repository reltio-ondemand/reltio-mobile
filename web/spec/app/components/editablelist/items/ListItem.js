/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.items.ListItem', function () {

    it('should return multiline with no error if it is not passed', function () {
        var record = {
            get: function (key) {
                if (key === 'message') {
                    return 'message';
                }
            }
        };
        var multiline = Ext.create('MobileUI.components.editablelist.items.ListItem').getMultiline(record);

        expect(multiline.error).toBe(null);
        expect(multiline.message).toBe('message');
    });

    it('should keep MISSED error message for parent attribute', function () {
        var record = {
            get: function (key) {
                if (key === 'error') {
                    return {
                        invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED,
                        attrType: {name: 'String', uri: 'string'},
                        value: 'Error'
                    };
                } else if (key === 'attrType') {
                    return {name: 'Nested', uri: 'nested'};
                }
            }
        };
        var multiline = Ext.create('MobileUI.components.editablelist.items.ListItem').getMultiline(record);

        expect(multiline.error).toBe('Error');
    });

    it('should skip MISSED error message', function () {
        var record = {
            get: function (key) {
                if (key === 'error') {
                    return {
                        invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED,
                        attrType: {uri: '1'},
                        value: 'Error'
                    };
                } else if (key === 'attrType') {
                    return {uri: '1'};
                }
            }
        };
        var multiline = Ext.create('MobileUI.components.editablelist.items.ListItem').getMultiline(record);

        expect(multiline.error).toBeNull();
    });

    it('should render error if no label and set error class', function (done) {
        var record = {
            get: function (key) {
                if (key === 'error') {
                    return {
                        invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED,
                        attrType: {name: 'String'},
                        value: 'Error'
                    };
                } else if (key === 'attrType') {
                    return {name: 'Nested'};
                }
            }
        };
        spyOn(MobileUI.components.editablelist.items.MAttributeEListItem.templates.t,'apply').and.callFake(function(params){
            expect(params.label).toBe('Error');
            expect(params.placeHolderCls).toBe('rl-error');
            done()
        });
        Ext.create('MobileUI.components.editablelist.items.ListItem').attributeLayout(record);
    });

    it('should render placeholder if no label and item is disabled', function (done) {
        var record = {
            get: function (key) {
                if (key === 'disabled'){
                    return true;
                }
                if (key === 'label'){
                    return 'Placeholder';
                }
                if (key === 'error') {
                    return {
                        invalidType: MobileUI.core.session.MValidation.VALIDATION_CODES.MISSED,
                        attrType: {name: 'String'},
                        value: 'Error'
                    };
                } else if (key === 'attrType') {
                    return {name: 'Nested'};
                }
            }
        };
        spyOn(MobileUI.components.editablelist.items.MAttributeEListItem.templates.t,'apply').and.callFake(function(params){
            expect(params.label).toBe('Placeholder');
            expect(params.placeHolderCls).toBe('reltio-disabled-placeholder');
            done()
        });
        Ext.create('MobileUI.components.editablelist.items.ListItem').attributeLayout(record);
    });
    it('should disable editor if editable if false', function () {
        var record = {
            get: function (key) {
                if (key === 'editable'){
                    return false;
                } else if (key === 'attrType') {
                    return {name: 'String', type: 'String'};
                } else if (key === 'attrValue') {
                    return {value: 'test'};
                }
            }
        };
        var editor = {
            on: Ext.emptyFn,
            set: Ext.emptyFn,
            setDisabled: Ext.emptyFn,
            setRenderTo: Ext.emptyFn
        };
        var setDisabled = spyOn(editor,'setDisabled');
        var listItems = Ext.create('MobileUI.components.editablelist.items.ListItem');
        spyOn(listItems, 'getRecord').and.returnValue(record);
        listItems.element = {
            down:function(){
                return {
                    setHtml: Ext.emptyFn
                }
            }
        };
        spyOn(MobileUI.components.editablelist.ControlsFactory, 'createControl').and.returnValue(editor);
        listItems.attributeAfterLayout();
        expect(setDisabled.calls.mostRecent().args[0]).toBe(true);
    });

    describe('simple lookup editor tests', function () {
        var record, editor, listItems,defaultRecord;
        beforeEach(function () {
            defaultRecord = {
                attrType: {name: 'String', type: 'String', lookupCode: 'Test'},
            };
            record = {
                get: function (key) {
                    return defaultRecord[key]
                }
            };
            editor = {
                on: Ext.emptyFn,
                set: Ext.emptyFn,
                setDisabled: Ext.emptyFn,
                setRenderTo: Ext.emptyFn
            };
            listItems = Ext.create('MobileUI.components.editablelist.items.ListItem');
            spyOn(listItems, 'getRecord').and.returnValue(record);
            listItems.element = {
                down: function () {
                    return {
                        setHtml: Ext.emptyFn
                    }
                }
            };
            spyOn(MobileUI.components.editablelist.ControlsFactory, 'createControl').and.returnValue(editor);
        });
        it('should set value from lookupCode first', function () {
            var set = spyOn(editor, 'set');
            defaultRecord.attrValue = {value: 'value', lookupCode: 'code'};
            listItems.attributeAfterLayout();
            expect(set.calls.mostRecent().args[1]).toBe('code');
        });
        it('should set value from attribute value if lookupCode is not find', function () {
            var set = spyOn(editor, 'set');
            defaultRecord.attrValue = {value: 'value'};
            listItems.attributeAfterLayout();
            expect(set.calls.mostRecent().args[1]).toBe('value');
        });
    });

});