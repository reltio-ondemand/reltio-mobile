/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.editablelist.items.MAttributeEListItem', function () {
    it('should escape attrType label and value', function () {
        var listItem = new MobileUI.components.editablelist.items.ListItem(),
            record = new MobileUI.components.editablelist.Model();

        record.set('attrType', {label: '<h1>xss</h1>'});
        record.set('label', '<h2>xss</h2>');

        var html = listItem.attributeLayout(record);
        expect(html).toContain('&lt;h1&gt;xss&lt;/h1&gt;');
        expect(html).toContain('&lt;h2&gt;xss&lt;/h2&gt;');
    });

    it('should prepend http:// prefix to url', function (done) {
        var listItem = new MobileUI.components.editablelist.items.ListItem(),
            record = new MobileUI.components.editablelist.Model();

        record.set('attrType', {
            label: 'Website',
            type: 'URL'
        });
        record.set('attrValue', {
            value: ''
        });

        spyOn(listItem, 'getRecord').and.returnValue(record);

        listItem.element = {
            down: function () {
                return this;
            },
            setHtml: Ext.emptyFn
        };

        var fakeEditor = {
            set: function (name, value) {
                expect(name).toBe('value');
                expect(value).toBe('http://www.reltio.com');
                done();
            }
        };

        spyOn(MobileUI.components.editablelist.ControlsFactory, 'createControl').and.returnValue({
            set: Ext.emptyFn,
            on: function (type, callback) {
                if (type === 'change') {
                    callback.call(this, fakeEditor, 'www.reltio.com');
                }
            },
            setRenderTo: Ext.emptyFn
        });

        listItem.attributeAfterLayout();
    });

    it('should display CdnUrlPreview for image attributes', function () {
        var listItem = new MobileUI.components.editablelist.items.ListItem(),
            record = new MobileUI.components.editablelist.Model();

        record.set('attrType', {
            label: 'Image',
            type: 'Image'
        });
        record.set('attrValue', {
            value: {
                CdnUrlPreview: [{
                    value: 'link'
                }]
            }
        });

        var html = listItem.attributeLayout(record);
        expect(html).toContain('<img src="link"');
        expect(html).not.toContain('rl-edit-button');
    });
});