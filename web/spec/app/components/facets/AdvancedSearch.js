/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.facets.AdvancedSearch', function () {

    it('should expand faced for deep nested', function () {
        var facet = Ext.create('MobileUI.components.facets.AdvancedSearch');
        var attributes = [{
            caption: 'SecondLevelNested',
            attributeType: 'Nested',
            uri: 'configuration/entityTypes/HCP/attributes/FirstLevelNested/attributes/SecondLevelNested'
        }];
        var state = [{
            fieldName: 'attributes.FirstLevelNested.SecondLevelNested.ThirdLevelNested.FirstLevelNestedSub5',
            uri: 'configuration/entityTypes/HCP/attributes/FirstLevelNested/attributes/SecondLevelNested/attributes/ThirdLevelNested/attributes/FirstLevelNestedSub5',
            values: ['test']
        }];

        expect(facet.checkCollapsedState(attributes, state)).toBeFalsy();
    });
});