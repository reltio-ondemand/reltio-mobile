/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.facets.EntityTypeSearchFacet', function () {

    it('should search only for searchable entity types', function (done) {
        MobileUI.core.Metadata.entityTypes = {
            'searchable': {},
            'nonSearchable': {
                searchable: false
            }
        };

        var facet = Ext.create('MobileUI.components.facets.EntityTypeSearchFacet');
        facet.initialize();

        facet.on('valueChanged', function (value) {
            expect(value.nonSearchable).toBeUndefined();
            expect(value.searchable).toBeDefined();
            done();
        }, this);

        facet.setParameters({});
    });
});