/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.facets.advanced.MetadataManager', function () {

    it('processMetadata should use attributes order as is', function () {
        MobileUI.core.Metadata.entityTypes = {
            HCP: {
                attributes: [{
                    type: 'Nested',
                    uri: 'nested',
                    attributes: [{
                        type: 'String',
                        label: 'B_should_be_first',
                        uri: 'b'
                    }, {
                        type: 'String',
                        label: 'A_should_be_second',
                        uri: 'a'
                    }]
                }]
            }
        };

        var manager = Ext.create('MobileUI.components.facets.advanced.MetadataManager');

        manager.processMetadata();

        expect(manager.fields['HCP']['nested'][0].children[0].caption).toBe('B_should_be_first');
        expect(manager.fields['HCP']['nested'][0].children[1].caption).toBe('A_should_be_second');
    });
});