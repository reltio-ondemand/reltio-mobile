/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.list.editors.ControlsFactory', function () {
    it('should create editors with steps 1 for int and long editors', function () {
        var ControlsFactory = MobileUI.components.editablelist.ControlsFactory;
        var intControl = ControlsFactory.createControl(ControlsFactory.attributeTypes.INT);
        expect(intControl.getStepValue()).toBe(1);
        var longControl = ControlsFactory.createControl(ControlsFactory.attributeTypes.LONG);
        expect(longControl.getStepValue()).toBe(1)
    })
});