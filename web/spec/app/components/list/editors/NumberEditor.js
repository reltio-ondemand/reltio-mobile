/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.list.editors.NumberEditor', function () {
    it('RP-TC-553:Validation. Check Float type', function () {
        var editor = Ext.create('MobileUI.components.list.editors.NumberEditor');

        //UI does not allow to type letters
        editor.setValue('abc');
        expect(editor.getValue()).toBe(null);

        //UI does not allow to type special characters
        editor.setValue('!@#$%^');
        expect(editor.getValue()).toBe(null);

        //Try to input number with floating point
        //Value is accepted
        editor.setValue('3.14');
        expect(editor.getValue()).toBe('3.14');

        //Clear value - tap on "x" on the right side of the field
        //Value should be cleared.
        editor.doClearIconTap(editor);
        expect(editor.getValue()).toBe(null);
    });
});