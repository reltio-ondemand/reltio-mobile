/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.list.editors.TimestampEditor', function () {
    it('should reset date/time if it is incorrect', function () {
        var item = Ext.create('MobileUI.components.list.editors.TimestampEditor');
        item.refresh(1505420841088);
        var changed = spyOn(item, 'changed');
        item.down('component[name=date]').fireEvent('change', 'editor', '1');
        expect(changed.calls.mostRecent().args[0].indexOf('1970-01-01')).toBe(0);
    });
    it('shouldn\'t allow to change info if editor is disabled', function () {
        var item = Ext.create('MobileUI.components.list.editors.TimestampEditor');
        item.setDisabled(true);
        item.refresh(1505420841088);
        var editableComponents = [
            'MobileUI.components.list.editors.NumberEditor',
            'Ext.field.Text',
            'MobileUI.components.TripleToggle'
        ];
        var allDisabled = item.getItems().items
            .filter(function (editor) {
                return editableComponents.indexOf(Ext.getClassName(editor)) !== -1;
            })
            .every(function (editor) {
                return editor.getDisabled();
            });

        expect(allDisabled).toBe(true);
    });
    it('should allow to change info by default', function () {
        var item = Ext.create('MobileUI.components.list.editors.TimestampEditor');
        item.refresh(1505420841088);
        var editableComponents = [
            'MobileUI.components.list.editors.NumberEditor',
            'Ext.field.Text',
            'MobileUI.components.TripleToggle'
        ];
        var allEnabled = item.getItems().items
            .filter(function (editor) {
                return editableComponents.indexOf(Ext.getClassName(editor)) !== -1;
            })
            .every(function (editor) {
                return !editor.getDisabled();
            });

        expect(allEnabled).toBe(true);
    });
});