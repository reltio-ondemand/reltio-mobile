/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.components.list.items.PropertyListItem', function () {
    Ext.define('components.list.items.TestPropertyListItem', {
        requires: ['MobileUI.components.list.items.MPropertyListItem'],
        mixins: {
            properyListItem: 'MobileUI.components.list.items.MPropertyListItem'
        },
        registerHandler: Ext.emptyFn,
        convertText: Ext.emptyFn,
        getRecord: Ext.emptyFn,
        element: {
            down: Ext.emptyFn
        }
    });

    describe('placeholder test', function () {
        it('should use placeholder if parent dependent is not filled', function () {
            var item = Ext.create('components.list.items.TestPropertyListItem');
            spyOn(item, 'getPlaceholderText').and.returnValue('placeholder');
            spyOn(item, 'inlineEditor').and.returnValue(false);
            var record = {
                get: function (key) {
                    if (key === 'editor') {
                        return {
                            dependentLookupCode: 'test'
                        }
                    }
                }
            };
            item.propertyLayout(record);
            expect(item.placeHolder).toBeTruthy();
        });
        it('should override requires a value cover if  placeholder defined', function () {
            var item = Ext.create('components.list.items.TestPropertyListItem');
            item.placeHolder = true;
            var record = {
                get: function (key) {
                    if (key === 'editor') {
                        return {
                            dependentLookupCode: 'test'
                        }
                    }
                }
            };
            spyOn(item, 'getRecord').and.returnValue(record);
            spyOn(item.element, 'down').and.returnValue({});
            spyOn(MobileUI.core.session.Session.getValidator(), 'collectHeaderErrors').and.returnValue(true);
            spyOn(MobileUI.core.CardinalityChecker, 'isRequired').and.returnValue(true);

            item.propertyAfterLayout();
            expect(item.placeHolder).toBeTruthy();
        });
        it('should show cover if placeholder not defined', function () {
            var item = Ext.create('components.list.items.TestPropertyListItem');
            var record = {
                get: function (key) {
                    if (key === 'editor') {
                        return {
                            dependentLookupCode: 'test'
                        }
                    }
                }
            };
            spyOn(item, 'getRecord').and.returnValue(record);
            var coverElementParent = {appendChild:Ext.emptyFn, style: {}};
            spyOn(coverElementParent, 'appendChild');
            spyOn(item.element, 'down').and.returnValue(coverElementParent);
            spyOn(MobileUI.core.session.Session.getValidator(), 'collectHeaderErrors').and.returnValue(true);
            spyOn(MobileUI.core.CardinalityChecker, 'isRequired').and.returnValue(true);
            item.propertyAfterLayout();
            expect(coverElementParent.appendChild).toHaveBeenCalled();
        });
    })
});