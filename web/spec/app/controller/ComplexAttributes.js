/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MProfile', function () {
    Ext.define('controller.Profile.TestComplex', {
        mixins: {
            conn: 'MobileUI.controller.MComplexAttributes'
        }
    });
    describe('processEntityRequest method test', function () {
        it('should remove ignored attributes from model', function () {
            var complex = Ext.create('controller.Profile.TestComplex');

            var result = complex.processNestedAttribute({
                    attributes: [{
                        name: 'Test'
                    }]
                },
                {
                    name: 'attributes.Parent',
                    attribute: {
                        label: '',
                        ov: true,
                        value: {
                            Test: [{
                                ov: false,
                                value: 'Test'
                            }]
                        }
                    }
                });
            expect(result).toEqual([]);
        });
        it('should process attributes from model', function () {
            var complex = Ext.create('controller.Profile.TestComplex');
            var result = complex.processNestedAttribute({
                    attributes: [{
                        name: 'Test'
                    }]
                },
                {
                    name: 'attributes.Parent',
                    attribute: {
                        label: '',
                        ov: true,
                        value: {
                            Test: [{
                                ov: true,
                                value: 'Test'
                            }]
                        }
                    }
                });
            expect(result[0].value[0].value).toEqual('Test');
        })
    });
});