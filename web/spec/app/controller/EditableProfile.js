/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MEditableProfile', function ()
{
    Ext.define('controller.EditableProfile.TestEditableProfile', {
        requires: ['MobileUI.controller.MEditableProfile'],
        mixins: {
            editableProfile: 'MobileUI.controller.MEditableProfile'
        },
        saveDone:Ext.emptyFn
    });
    describe('editEntity method test', function(){
        it('should clear validation errors on invoke', function(){
            var editableProfile = Ext.create('controller.EditableProfile.TestEditableProfile');
            var validator = MobileUI.core.session.Session.getValidator();
            spyOn(validator, 'resetValidators');
            spyOn(MobileUI.core.Navigation, 'redirectForward');
            editableProfile.editEntity([1,2,3,4]);
            expect(validator.resetValidators).toHaveBeenCalled();

        })
    });
    describe('processEditEntity method test', function(){
        it('shouldn\'t process empty entities', function(){
            var editableProfile = Ext.create('controller.EditableProfile.TestEditableProfile');
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(null);
            spyOn(editableProfile, 'getProfileConfiguration').and.returnValue(null);
            editableProfile.processEditEntity(null);
            expect(editableProfile.getProfileConfiguration).not.toHaveBeenCalled();
        })
    })
    describe('sendChangeRequestReview method test', function(){
        var Services;

        beforeEach(function () {
            Services = MobileUI.core.Services;
        });

        afterEach(function() {
            Services.settings = null;
        });

        it('should send DCR comment', function(){
            Services.settings = {
                workflowPath: 'testWorkflowPath'
            };

            var editableProfile = Ext.create('controller.EditableProfile.TestEditableProfile');

            editableProfile.changeRequestComment = 'test comment'

            var sendRequestSpy = spyOn(MobileUI.core.session.Session, 'sendRequest').and.returnValue(null);
            editableProfile.sendChangeRequestReview();
            expect(sendRequestSpy.calls.mostRecent().args[2].data.comment).toBe('test comment');
        })

        it('shouldn\'t send empty DCR comment', function(){
            Services.settings = {
                workflowPath: 'testWorkflowPath'
            };

            var editableProfile = Ext.create('controller.EditableProfile.TestEditableProfile');

            editableProfile.changeRequestComment = '   ';

            var sendRequestSpy = spyOn(MobileUI.core.session.Session, 'sendRequest').and.returnValue(null);
            editableProfile.sendChangeRequestReview();
            expect(sendRequestSpy.calls.mostRecent().args[2].data.comment).not.toBeDefined();
        })

        it('should be called in onAfterValidation method', function(done){
            Services.settings = {
                workflowPath: 'testWorkflowPath'
            };

            var editableProfile = Ext.create('controller.EditableProfile.TestEditableProfile');

            spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue({
                suggestedMode: true
            })

            spyOn(MobileUI.core.session.Session, 'createDCR').and.callFake(function(successCb){
                successCb.call(editableProfile, {uri: 'changeRequest/testId'})
            })

            spyOn(editableProfile, 'saveEntity').and.callFake(function(entityForSave, cb, scope){
                cb.call(scope, true)
            })

            spyOn(editableProfile, 'showDCRCommentDialog').and.returnValue(Promise.resolve())
            spyOn(editableProfile, 'sendChangeRequestReview').and.callFake(done)

            editableProfile.onAfterValidation()
        })
    })
});
