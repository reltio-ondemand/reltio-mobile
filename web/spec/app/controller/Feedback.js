/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.Feedback', function () {
    var controller, view;
    beforeEach(function () {
        view = Ext.create('MobileUI.view.Feedback', {
            renderTo: Ext.getBody()
        });

        controller = Ext.create('MobileUI.controller.Feedback', {application: Ext.create('Ext.app.Application', {name: 'Test'})});
        controller.launch();
        controller.onPainted();
    });
    afterEach(function () {
        view.destroy();
        controller.destroy();
    });

    describe('onFeedbackSend method test', function () {
        it('should send data only from the visible fields', function () {
            spyOn(controller, 'createIssue');
            controller.getDescription().setValue('description');
            controller.getPriority().setValue('priority');
            controller.getStepsToReproduce().setValue('steps');
            controller.getActualResults().setValue('actualResults');
            controller.getExpectedResults().setValue('expectedResults');
            controller.onFeedbackSend();
            expect(controller.createIssue).toHaveBeenCalled();
            var args = controller.createIssue.calls.mostRecent().args.map(function (item, index) {
                return {index: index, item: item}
            });
            var emptyCount = args.filter(function (item) {
                return Ext.isEmpty(item.item);
            });
            expect(emptyCount.length).toBe(5);
        });
    });

    describe('validate method test', function () {
        it('should add invalid cls for each incorrect item', function () {
            controller.validate();
            expect(controller.getSummary().getCls().indexOf('invalid-item')).not.toBe(-1);
        });
    });
    describe('validate email test', function () {
        it('should pass valid email', function () {
            expect(controller.validateEmail('test@test.com')).toBeTruthy();
        });
        it('should pass several valid emails', function () {
            expect(controller.validateEmail('test@test.com,test1@test.com')).toBeTruthy();
        });
        it('should pass for empty email', function () {
            expect(controller.validateEmail('')).toBeTruthy();
        });
        it('should fail for incorrect email', function () {
            expect(controller.validateEmail('11')).toBeFalsy();
        });
    });
    describe('initGroup method test', function () {
        it('should add invalid cls for each incorrect item', function () {
            expect(controller.group[MobileUI.controller.Feedback.ZENDESK_REPORT].length).toBe(11);
            expect(controller.group[MobileUI.controller.Feedback.ZENDESK_ASK].length).toBe(0);
            expect(controller.group[MobileUI.controller.Feedback.ZENDESK_REQUEST].length).toBe(1);
            expect(controller.group[MobileUI.controller.Feedback.ZENDESK_SUGGEST].length).toBe(1);
        });
    });
    describe('getDefaultFeedbackOptions method test', function () {
        it('should translate options', function () {
            spyOn(window, 'i18n').and.callFake(function (item) {
                return 'tr:' + item;
            });
            var nontranslatedOptions = controller.getDefaultFeedbackOptions()
                .map(function (option) {
                    return option.text
                })
                .filter(function (option) {
                    return option.indexOf('tr:') !== 0
                });
            expect(nontranslatedOptions.length).toBe(0)
        });
    });

    it('should display correct fields based on issue type', function () {
        expect(controller.getReason().isHidden()).toBeFalsy();
        expect(controller.getCcs().isHidden()).toBeFalsy();
        expect(controller.getSummary().isHidden()).toBeFalsy();
        expect(controller.getPriority().isHidden()).toBeFalsy();
        expect(controller.getDescription().isHidden()).toBeFalsy();
        expect(controller.getStepsToReproduce().isHidden()).toBeFalsy();
        expect(controller.getActualResults().isHidden()).toBeFalsy();
        expect(controller.getExpectedResults().isHidden()).toBeFalsy();
        expect(controller.getDidItWork().isHidden()).toBeFalsy();
        expect(controller.getBusinessImpact().isHidden()).toBeFalsy();

        controller.onReasonChanged(MobileUI.controller.Feedback.ZENDESK_ASK);
        expect(controller.getReason().isHidden()).toBeFalsy();
        expect(controller.getCcs().isHidden()).toBeFalsy();
        expect(controller.getSummary().isHidden()).toBeFalsy();
        expect(controller.getPriority().isHidden()).toBeFalsy();
        expect(controller.getDescription().isHidden()).toBeFalsy();

        expect(controller.getStepsToReproduce().isHidden()).toBeTruthy();
        expect(controller.getActualResults().isHidden()).toBeTruthy();
        expect(controller.getExpectedResults().isHidden()).toBeTruthy();
        expect(controller.getDidItWork().isHidden()).toBeTruthy();
        expect(controller.getBusinessImpact().isHidden()).toBeTruthy();

        controller.onReasonChanged(MobileUI.controller.Feedback.ZENDESK_REQUEST);
        expect(controller.getReason().isHidden()).toBeFalsy();
        expect(controller.getCcs().isHidden()).toBeFalsy();
        expect(controller.getSummary().isHidden()).toBeFalsy();
        expect(controller.getPriority().isHidden()).toBeFalsy();
        expect(controller.getDescription().isHidden()).toBeFalsy();

        expect(controller.getStepsToReproduce().isHidden()).toBeTruthy();
        expect(controller.getActualResults().isHidden()).toBeTruthy();
        expect(controller.getExpectedResults().isHidden()).toBeTruthy();
        expect(controller.getDidItWork().isHidden()).toBeTruthy();
        expect(controller.getBusinessImpact().isHidden()).toBeTruthy();

        controller.onReasonChanged(MobileUI.controller.Feedback.ZENDESK_SUGGEST);
        expect(controller.getReason().isHidden()).toBeFalsy();
        expect(controller.getCcs().isHidden()).toBeFalsy();
        expect(controller.getSummary().isHidden()).toBeFalsy();
        expect(controller.getPriority().isHidden()).toBeFalsy();
        expect(controller.getDescription().isHidden()).toBeFalsy();
        expect(controller.getBusinessImpact().isHidden()).toBeFalsy();

        expect(controller.getStepsToReproduce().isHidden()).toBeTruthy();
        expect(controller.getActualResults().isHidden()).toBeTruthy();
        expect(controller.getExpectedResults().isHidden()).toBeTruthy();
        expect(controller.getDidItWork().isHidden()).toBeTruthy();
    });

    it('should show validation message for required fields', function () {
        controller.onFeedbackSend();
        expect(controller.getNotificationMessage().element.dom.textContent).toBe('Incorrect fields: Description, Subject, Steps to reproduce, Actual results, Expected results, Business Impact');

        controller.getSummary().setValue('Summary');
        controller.getActualResults().setValue('Actual Results');

        controller.onFeedbackSend();
        expect(controller.getNotificationMessage().element.dom.textContent).toBe('Incorrect fields: Description, Steps to reproduce, Expected results, Business Impact');
    });
});