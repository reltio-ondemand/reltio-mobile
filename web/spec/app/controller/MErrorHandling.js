/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MErrorHandling', function () {
    Ext.define('controller.Panel', {
        requires: ['MobileUI.controller.MErrorHandling'],
        mixins: {
            errorHandling: 'MobileUI.controller.MErrorHandling'
        }
    });
    describe('error handling method', function () {
        it('should create correct record type and escape html message', function (done) {
            var errorHandlingPanel = Ext.create('controller.Panel');
            var clearData = jasmine.createSpy('spy');
            var store = {
                clearData: clearData,
                add: function(record){
                    expect(record.type).toBe(MobileUI.components.list.items.BaseListItem.types.custom);
                    expect(record.editor()).toBe('&lt;xss/&gt;');
                    done();
                }
            };
            errorHandlingPanel.showErrorMessage(store, '<xss/>');
        })
    });
});
