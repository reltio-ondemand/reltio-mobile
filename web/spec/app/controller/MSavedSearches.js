/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MSavedSearches', function () {
    Ext.define('controller.SavedSearches.TestSavedSearch', {
        requires: ['MobileUI.controller.MSavedSearches'],
        mixins: {
            savedSearches: 'MobileUI.controller.MSavedSearches'
        }
    });
    describe('showSavedSearch method test', function () {
        it('should clear all dialogs in case of redirecting on search screen', function () {
            var savedSearches = Ext.create('controller.SavedSearches.TestSavedSearch');
            spyOn(MobileUI.core.session.Session, 'getSavedSearch').and.callFake(function (uri, fn, ctx) {
                fn.call(ctx)
            });
            spyOn(MobileUI.controller.MSavedSearches, 'extractSavedSearchUIData').and.returnValue({});
            spyOn(MobileUI.core.SuiteManager, 'hideDialogs');
            spyOn(MobileUI.core.Navigation, 'redirectForward');
            savedSearches.showSavedSearch('uri');
            expect(MobileUI.core.SuiteManager.hideDialogs).toHaveBeenCalled();
            MobileUI.core.search.SearchParametersManager.resetSearch();
        })
    });
});
