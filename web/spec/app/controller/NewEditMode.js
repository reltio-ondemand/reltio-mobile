/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.NewEditMode', function () {
    var controller;
    beforeEach(function () {
        controller = Ext.create('MobileUI.controller.NewEditMode', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });
        spyOn(controller, 'getSaveToolbarBtn').and.returnValue({
            state: false,
            isDisabled: function () {
                return this.state
            },
            setDisabled: function (state) {
                this.state = state
            }
        });
        spyOn(controller, 'getPanel').and.returnValue({
            setMasked: Ext.emptyFn
        });
        controller.launch();
    });

    it('should return to entity profile after create new entity', function (done) {
        controller.entityUri = 'entities/123';
        controller.createMode = true;

        spyOn(MobileUI.core.Navigation, 'redirectForward').and.callFake(function (url) {
            expect(url).toEqual('profile/entities$123');
            done();
        });

        controller.returnToViewMode();
    });

    it('should go back on return to view mode if allowed', function () {
        controller.createMode = false;

        spyOn(MobileUI.core.Navigation, 'isBackAllow').and.returnValue(true);
        var backSpy = spyOn(MobileUI.core.Navigation, 'back');
        var redirectSpy = spyOn(MobileUI.core.Navigation, 'redirectForward');

        controller.returnToViewMode();

        expect(backSpy.calls.count()).toBe(1);
        expect(redirectSpy.calls.count()).toBe(0);
    });

    it('should redirect to dashboard if no temporary entity found by uri', function (done) {
        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue(null);

        spyOn(MobileUI.core.Navigation, 'redirectForward').and.callFake(function (url) {
            expect(url).toEqual('dashboard/init');
            done();
        });

        controller.entryPoint(['entities', 'uri', '', '456']); //temporary uri with $$
    });

    it('should clear errors on request entity', function () {
        spyOn(MobileUI.core.Metadata, 'getEntityType');
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'initWithEntityType');
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'fillValuesFromEntity');
        spyOn(MobileUI.core.session.Session.getLookupsManager(), 'cleanTouchedList');
        spyOn(MobileUI.core.session.Session, 'getModifiedEntity').and.returnValue({});
        spyOn(controller, 'createTransaction');
        spyOn(controller, 'initTransaction');
        spyOn(controller, 'refreshList');
        spyOn(controller, 'initProfileBand');

        var entity = {};

        controller.onSuccessRequestEntity(entity);

        controller.errors = [{type: 'MISSED'}];

        controller.onSuccessRequestEntity(entity);

        expect(controller.errors).toBeNull();
    });

    it('should set createMode correctly', function () {
        spyOn(controller, 'onSuccessRequestEntity');
        spyOn(MobileUI.core.session.Session, 'requestEntity');

        controller.entryPoint(['entities', 'uri', '', '456']); //temporary uri with $$
        expect(controller.createMode).toBe(true);

        controller.entryPoint(['entities', '123']);
        expect(controller.createMode).toBe(false);
    });

    it('should disable save button for creation mode only', function () {
        spyOn(controller, 'onSuccessRequestEntity');
        spyOn(MobileUI.core.session.Session, 'requestEntity');

        controller.entryPoint(['entities', 'uri', '', '456']); //temporary uri with $$
        expect(controller.getDoneButton().isDisabled()).toBe(true);

        controller.entryPoint(['entities', '123']);
        expect(controller.getDoneButton().isDisabled()).toBe(false);
    });

    it('should enable save button when data is changed', function () {
        spyOn(controller, 'onSuccessRequestEntity');
        spyOn(MobileUI.core.session.Session, 'requestEntity');

        controller.entryPoint(['entities', 'uri', '', '456']); //temporary uri with $$
        expect(controller.getDoneButton().isDisabled()).toBe(true);

        controller.entryPoint(['entities', '123']);
        expect(controller.getDoneButton().isDisabled()).toBe(false);
    });

    it('should change title based on mode', function () {
        spyOn(controller, 'onSuccessRequestEntity');
        spyOn(MobileUI.core.session.Session, 'requestEntity');
        spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function(type){
            return {
                'configuration/entityTypes/HCO': {
                    access: ['READ'],
                    label: 'HCO'
                },
                'configuration/entityTypes/HCP': {
                    access: ['READ', 'CREATE', 'UPDATE', 'DELETE'],
                    label: 'HCP'
                }
            }[type];
        });
        controller.entryPoint(['entities', 'uri', '', '456']); //temporary uri with $$
        expect(controller.getTitleText({
            type: 'configuration/entityTypes/HCO'
        })).toBe('Suggest new HCO');
        expect(controller.getTitleText({
            type: 'configuration/entityTypes/HCP'
        })).toBe('Create HCP');

        controller.entryPoint(['entities', '123']);
        expect(controller.getTitleText({
            type: 'configuration/entityTypes/HCP'
        })).toBe('Edit Profile');
    });

    it('should show confirm for cancel button', function () {
        var spy = spyOn(MobileUI.components.MessageBox, 'show');
        controller.onCancelToolbarButtonTap();

        expect(spy.calls.count()).toBe(1);
    });

    it('should reset entity after save', function () {
        controller.branch = {save: Ext.emptyFn};
        controller.transaction = {
            getCurrentProjection: function () {
                return {
                    Name: [{
                        value: 'test'
                    }]
                }
            }
        };
        spyOn(controller, 'getCurrentEntity').and.returnValue({});
        spyOn(MobileUI.core.session.Session, 'validateAll').and.callFake(function () {
            return Promise.resolve([]);
        });
        spyOn(MobileUI.core.session.Session, 'saveAllPendingChanges').and.callFake(function () {
            return Promise.resolve({uri: '42'});
        });
        var spy = spyOn(MobileUI.core.session.Session, 'resetCurrentEntity');

        controller.onSaveButtonTap();

        spyOn(controller, 'returnToViewMode').and.callFake(function () {
            expect(spy.calls.count()).toBe(1);
        });
    });

    describe('integration tests', function () {

        it('RP-TC-6304', function (done) {

            var entity = {
                type: 'configuration/entityTypes/HCP',
                uri: 'entities/1',
                attributes: {
                    CallingName: [{
                        uri: 'entities/1/attributes/CallingName/2',
                        value: 'CallingName'
                    }],
                    MiddleName: [{
                        uri: 'entities/1/attributes/MiddleName/3',
                        value: 'Test'
                    }],
                    LastName: [{
                        uri: 'entities/1/attributes/LastName/4',
                        value: 'Test'
                    }]
                },
                crosswalks: [{
                    type: 'configuration/sources/Reltio',
                    value: '123',
                    attributes: [
                        'entities/1/attributes/CallingName/2',
                        'entities/1/attributes/MiddleName/3',
                        'entities/1/attributes/LastName/4'
                    ]
                }]
            };

            var attrTypes = {
                CallingName: {
                    name: 'CallingName',
                    label: 'CallingName',
                    required: true,
                    type: 'String',
                    uri: 'configuration/entityTypes/HCP/attributes/CallingName'
                },
                MiddleName: {
                    name: 'MiddleName',
                    label: 'MiddleName',
                    type: 'String',
                    uri: 'configuration/entityTypes/HCP/attributes/MiddleName'
                },
                LastName: {
                    name: 'LastName',
                    label: 'LastName',
                    type: 'String',
                    uri: 'configuration/entityTypes/HCP/attributes/LastName',
                    cardinality: {
                        maxValue: 1
                    }
                }
            };

            var validateRequestExpectSpy = jasmine.createSpy('spy');

            function mockRequests() {
                spyOn(MobileUI.core.session.Session, 'sendRequestAndReturnPromise').and.callFake(function (url) {
                    if (url === MobileUI.core.Metadata.URL_CONFIGURATION) {
                        return Promise.resolve({
                            sources: [],
                            survivorshipStrategies: [],
                            entityTypes: [{
                                uri: 'configuration/entityTypes/HCP',
                                attributes: [
                                    attrTypes.CallingName,
                                    attrTypes.MiddleName,
                                    attrTypes.LastName
                                ]
                            }]
                        });
                    }
                });

                spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success, failure, self) {
                    var data = {};
                    if (url === MobileUI.core.session.MEntity.URL_ENTITY_OPTIONS) {
                        data = entity;
                    } else if (url === 'localhost' + MobileUI.core.session.newEditMode.MValidationService.VALIDATE_CHANGES_URL) {
                        validateRequestExpectSpy();
                        expect(options.data).toEqual([{
                            type: 'DELETE_ATTRIBUTE',
                            uri: 'entities/1/attributes/CallingName/2',
                            crosswalk: {
                                type: 'configuration/sources/Reltio',
                                value: '123'
                            }
                        }, {
                            type: 'UPDATE_ATTRIBUTE',
                            uri: 'entities/1/attributes/MiddleName/3',
                            newValue: {'value': 'Middle'},
                            crosswalk: {
                                type: 'configuration/sources/Reltio',
                                value: '123'
                            }
                        }]);

                        data = [{
                            errorType: 'INCORRECT',
                            objectUri: 'entities/1/attributes/MiddleName/3',
                            objectTypeUri: 'configuration/entityTypes/HCP/attributes/MiddleName',
                            objectParentUri: '',
                            message: 'Middle Name cannot contains Middle'
                        }];
                    }

                    success.call(self, data);
                });

                spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
                    ui: 'localhost',
                    validationServicePath: 'localhost'
                });
            }

            mockRequests();

            MobileUI.core.Metadata.init().then(function () {
                Ext.create('MobileUI.view.NewEditMode', {
                    renderTo: Ext.getBody()
                });
                var controller = Ext.create('MobileUI.controller.NewEditMode', {
                        application: Ext.create('Ext.app.Application', {name: 'Test'})
                    });

                controller.entryPoint(['entities', '1']);

                var record = new MobileUI.components.list.model.EListModel();
                record.set('attrValue', entity.attributes.CallingName[0]);
                record.set('attrType', attrTypes.CallingName);
                controller.onListDeleteAttribute(record);

                controller.onListChangeValue({
                    attrType: attrTypes.MiddleName,
                    uri: 'entities/1/attributes/MiddleName/3',
                    value: 'Middle'
                });

                controller.onSaveButtonTap();

                var task = Ext.create('Ext.util.DelayedTask', function () {
                    expect(controller.getNotificationMessage().getHtml()).toBe('' +
                        '<div class="validation-header">CallingName:</div><div class="validation-record">Requires a value</div>' +
                        '<div class="validation-header">MiddleName:</div><div class="validation-record">Middle: Middle Name cannot contains Middle</div>');

                    expect(validateRequestExpectSpy).toHaveBeenCalled();

                    done();
                }, this);
                task.delay(100);
            });
        });
    });
});