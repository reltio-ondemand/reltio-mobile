/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MProfileConfiguration', function ()
{
    Ext.define('controller.ProfileConfiguration.TestProfileConfiguration', {
        requires: ['MobileUI.controller.MProfileConfiguration'],
        mixins: {
            profileConfiguration: 'MobileUI.controller.MProfileConfiguration'
        }
    });

    beforeEach(function ()
    {
        jasmine.Ajax.install();
        MobileUI.core.Services.settings = {};
        MobileUI.core.Services.settings.ui = '';
        MobileUI.core.session.Session.callMethodWhenTokenAvailable = function(success, failure, args, self){
            success.apply(self || MobileUI.core.session.Session, args);
        };
        var manager = MobileUI.core.Services.getConfigurationManager();
        jasmine.Ajax.stubRequest(new RegExp('^services/mobile')).andReturn({responseText: window.mocked.getMobileConfiguration()});
        manager.initConfiguration('a5');
    });

    afterEach(function ()
    {
        jasmine.Ajax.uninstall();
    });
    it('Should return the correct profile configuration for the entityType', function ()
        {
            var profileConf = Ext.create('controller.ProfileConfiguration.TestProfileConfiguration'),
                hcpType = 'configuration/entityTypes/HCP';
            expect(profileConf.getProfileConfiguration(hcpType)).not.toBe(null);
            expect(profileConf.getProfileConfiguration(hcpType).entityType).toBe(hcpType);
        }
    );
    it('Should return null for the nonexistent entity type', function ()
        {
            var profileConf = Ext.create('controller.ProfileConfiguration.TestProfileConfiguration'),
                nonExistentType = 'configuration/entityTypes/NONExist';
            expect(profileConf.getProfileConfiguration(nonExistentType)).toBe(null);
        }
    );
});