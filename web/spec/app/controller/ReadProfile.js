/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.MProfile', function () {
    Ext.define('controller.Profile.TestProfile', {
        requires: ['MobileUI.controller.MProfile', 'MobileUI.controller.MProfileConfiguration'],
        mixins: {
            profile: 'MobileUI.controller.MProfile',
            band: 'MobileUI.controller.MProfileBand',
            conf: 'MobileUI.controller.MProfileConfiguration',
            conn: 'MobileUI.controller.MProfileFacets'
        },
        getProfileBand: function () {
            return {
                show: Ext.emptyFn
            }
        }
    });
    var getButtons = function (model) {
        return model.getRange(0, 99).filter(function (item) {
            return item.get('type') === MobileUI.components.list.items.BaseListItem.types.buttons;
        });
    };
    var getAllAttributes = function (model) {
        return model.getRange(0, 99).filter(function (item) {
            return item.get('type') === MobileUI.components.list.items.BaseListItem.types.property || item.get('type') === MobileUI.components.list.items.BaseListItem.types.infoProperty;
        });
    };
    describe('processEntityRequest method test', function () {
        it('should remove ignored attributes from model', function () {
            var profile = Ext.create('controller.Profile.TestProfile');
            spyOn(profile, 'fillBand');
            spyOn(profile, 'trackActivity');
            spyOn(profile, 'fillAttributesList');
            spyOn(profile, 'processConnections');
            spyOn(profile, 'prepareAttributes').and.returnValue({
                    'Profile Conf': [{
                        value: [{
                            nonEmpty: ''
                        }],
                        showLabel: true
                    }, {
                        value: [],
                        showLabel: true
                    }]
                }
            );
            profile.processEntityRequest({});
            expect(profile.processConnections).toHaveBeenCalled();
            expect(profile.processConnections.calls.mostRecent().args[1]['Profile Conf'].length).toBe(1);

        })
    });
    var getMetadata = function (access) {
        return {
            access: access,
            attributes: [
                {
                    label: 'Simple',
                    access: access,
                    name: 'Simple',
                    type: 'String',
                    uri: 'configuration/entityTypes/HCP/attributes/Simple'
                },
                {
                    label: 'Nested',
                    name: 'Nested',
                    access: access,
                    type: 'Nested',
                    uri: 'configuration/entityTypes/HCP/attributes/Nested',
                    attributes: [
                        {
                            label: 'SimpleInsideNested',
                            name: 'SimpleInsideNested',
                            type: 'String',
                            access: access,
                            uri: 'configuration/entityTypes/HCP/attributes/Nested/attributes/SimpleInsideNested'
                        }
                    ]
                }
            ]
        }
    };
    var entity = {
        uri: 'entities/123',
        attributes: {
            Simple: [{
                value: 'Simple',
                type: 'configuration/entityTypes/HCP/attributes/Simple',
                ov: true,
                uri: 'entities/123/attributes/Simple/456'
            }],
            Nested: [{
                label: 'Simple In Nested',
                type: 'configuration/entityTypes/HCP/attributes/Nested',
                ov: true,
                uri: 'entities/123/attributes/Nested/456',
                value: {
                    SimpleInsideNested: [{
                        ov: true,
                        value: 'Simple In Nested',
                        type: 'configuration/entityTypes/HCP/attributes/Nested/attributes/SimpleInsideNested',
                        uri: 'entities/123/attributes/Nested/456/attributes/SimpleInsideNested/789'
                    }]
                }
            }]
        }
    };
    describe('RP-TC-569(2). Read mode profile', function () {
        it('should show attributes from the entity', function () {
            var profile = Ext.create('controller.Profile.TestProfile');
            spyOn(profile, 'fillBand');
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(getMetadata(['READ']));
            spyOn(profile, 'fillAttributesList');
            spyOn(profile, 'processConnections').and.callFake(function (type, attributes) {
                return attributes;
            });
            spyOn(profile, 'trackActivity');
            profile.processEntityRequest(entity);
            var model = profile.fillAttributesList.calls.mostRecent().args[0];
            expect(profile.processConnections).toHaveBeenCalled();
            expect(model.Other[0].value[0].value).toBe('Simple');
            expect(model.Nested[0].value[0].label).toBe('Simple In Nested');
            expect(model.Nested[0].value[0].value.SimpleInsideNested[0].value).toBe('Simple In Nested');
        })
    });
    describe('RP-TC-1041(3,4,5,6,7,8):Metadata security. User has CREATE READ UPDATE RIGHTS', function () {
        it('should show editors in case of read/update from the entity', function () {
            var profile = Ext.create('controller.Profile.TestProfile');
            spyOn(profile, 'fillBand');
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue(getMetadata(['READ', 'UPDATE', 'DELETE']));
            spyOn(profile, 'updateListHeight');
            spyOn(profile, 'processConnections').and.callFake(function (type, attributes) {
                return attributes;
            });
            spyOn(profile, 'trackActivity');
            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            profile.getAttributesList = function () {
                return {
                    getStore: function () {
                        return store;
                    },
                    refresh: Ext.emptyFn
                }
            };
            profile.getPanel = function () {
                return {setMasked: Ext.emptyFn}
            };
            profile.processEntityRequest(entity);
            var buttons = getButtons(store.getData());
            expect(buttons[0].get('label')).toBe('Edit profile');
            expect(getAllAttributes(store.getData()).length).toBe(2);
            var label = getAllAttributes(store.getData()).map(function (item) {
                return item.get('label')
            });
            expect(label).toEqual(['Simple In Nested', 'Simple']);
        })
    });
});