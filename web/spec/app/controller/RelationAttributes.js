/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.RelationAttributes', function ()
{
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.RelationAttributes', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: function () {
            }
        });
        controller.launch();
    });
    var getAllAttributes = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'property'
        });
    };
    var hasEditButton = function (model) {
        return model.filter(function (item) {
            return item.get('type') === 'buttons';
        }).length > 0;
    };
    describe('RP-TC-1475. Readonly rights in relationship', function(){
        var metadata = {
            entityTypes: {
                'configuration/entityTypes/HCP': {
                    access: ['READ'],
                    attributes: [
                        {
                            label: 'Simple',
                            access: ['READ'],
                            name: 'Simple',
                            type: 'String',
                            uri: 'configuration/entityTypes/HCP/attributes/Simple'
                        },
                        {
                            label: 'Nested',
                            name: 'Nested',
                            access: ['READ'],
                            type: 'Nested',
                            uri: 'configuration/entityTypes/HCP/attributes/Nested',
                            attributes: [
                                {
                                    label: 'SimpleInsideNested',
                                    name: 'SimpleInsideNested',
                                    type: 'String',
                                    access: ['READ'],
                                    uri: 'configuration/entityTypes/HCP/attributes/Nested/attributes/SimpleInsideNested'
                                }
                            ]
                        }
                    ]
                }
            },
            relationTypes: {
                'configuration/relationTypes/HasHealthCareRole': {
                    access: ['READ'],
                    startObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    endObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    attributes: [
                        {
                            label: 'Simple',
                            access: ['READ'],
                            name: 'Simple',
                            type: 'String',
                            uri: 'configuration/relationTypes/HasHealthCareRole/attributes/Simple'
                        }
                    ],
                    typeColor: '#556655',
                    uri: 'configuration/relationTypes/HasHealthCareRole'
                }
            }
        };
        it('RP-TC-1475(3,5,9) should show all relation attributes as readonly if it has read only rights', function () {
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            controller.parsedParams = {};
            controller.entity = {};
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getTitle').and.returnValue({
                setHtml: Ext.emptyFn
            });
            spyOn(controller, 'updateListHeight');

            spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
                getExtensionById: Ext.emptyFn
            });

            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(controller, 'getRelationAttributesList').and.returnValue({
                getStore: function () {
                    return store;
                },
                refresh: Ext.emptyFn
            });

            controller.onRelationsResponse([{
                type: 'configuration/relationTypes/HasHealthCareRole',
                startObject: {},
                endObject: {},
                attributes: {
                    Simple: [{
                        value: 'Simple',
                        type: 'configuration/relationTypes/HCP/HasHealthCareRole/attributes/Simple',
                        ov: true,
                        uri: 'relations/123/attributes/Simple/456'
                    }]
                }
            }]);

            var data = controller.getRelationAttributesList().getStore().getData();
            expect(getAllAttributes(data).length).toBe(1);
            expect(hasEditButton(data));
        });
    });
});