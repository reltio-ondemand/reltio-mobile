/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.Relations', function () {
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.Relations', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: function () {
            }
        });
        controller.launch();
    });
    var isEditable = function (record) {
        return record.get('allowedSwipeElements').indexOf('editRelation') !== -1;
    };
    var isRemovable = function (record) {
        return record.get('allowedSwipeElements').indexOf('delete') !== -1;
    };
    var getErrorItems = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'custom'
        });
    };
    var hasAddButton = function (model) {
        return model.getRange(0, 99).filter(function (item) {
            return item.get('type') === 'facetItem';
        }).length > 0;
    };

    var hasVoteButton = function (record) {
        return record.get('showVote')
    };
    var getAllRelationships = function (model) {
        return model.getRange(0, 99).filter(function (item) {
            return item.get('type') === 'relationsInfo'
        });
    };
    var createMetadata = function (entityAccess, relationAccess) {
        return {
            entityTypes: {
                'configuration/entityTypes/HCP': {
                    access: entityAccess,
                    attributes: [
                        {
                            label: 'Simple',
                            access: entityAccess,
                            name: 'Simple',
                            type: 'String',
                            uri: 'configuration/entityTypes/HCP/attributes/Simple'
                        },
                        {
                            label: 'Nested',
                            name: 'Nested',
                            access: entityAccess,
                            type: 'Nested',
                            uri: 'configuration/entityTypes/HCP/attributes/Nested',
                            attributes: [
                                {
                                    label: 'SimpleInsideNested',
                                    name: 'SimpleInsideNested',
                                    type: 'String',
                                    access: entityAccess,
                                    uri: 'configuration/entityTypes/HCP/attributes/Nested/attributes/SimpleInsideNested'
                                }
                            ]
                        }
                    ]
                }
            },
            relationTypes: {
                'configuration/relationTypes/HasHealthCareRole': {
                    access: relationAccess,
                    startObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    endObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    typeColor: '#556655',
                    uri: 'configuration/relationTypes/HasHealthCareRole'
                }
            }
        }
    };
    describe('RP-TC-1475. Readonly rights in relationship', function () {
        var metadata = createMetadata(['READ'], ['READ']);
        it('RP-TC-1475(7,8) should show all items as readonly if it has read only rights', function () {
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getRelationsFacet').and.returnValue({
                setFilter: Ext.emptyFn,
                setFilterEnabled: Ext.emptyFn
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(controller, 'getRelationsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn
            });
            controller.relationConfig = [{
                sortOrder: 'asc',
                type: 'rating',
                content: {
                    entityTypes: ['configuration/entityTypes/HCP'],
                    outRelations: [{uri: 'configuration/relationTypes/HasHealthCareRole'}]
                }
            }];

            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            controller.onRelationsResponse([{
                connections: [
                    {
                        entity: {
                            entityLabel: 'test',
                            entityType: 'configuration/entityTypes/HCP',
                            entityUri: 'entities/123'
                        },
                        relation: {
                            relationUri: 'relations/123',
                            relationType: 'configuration/relationTypes/HasHealthCareRole',
                            relationLabel: 'label',
                            direction: 'out',
                            ratings: []
                        }
                    }
                ],
                index: 0,
                total: 1
            }]);

            var allItems = getAllRelationships(store.getData());
            expect(allItems.filter(isEditable).length).toBe(0);
            expect(allItems.filter(isRemovable).length).toBe(0);
            expect(allItems.filter(hasVoteButton).length).toBe(0);
            expect(hasAddButton(store.getData())).toBeFalsy();

        });
    });
    describe('RP-TC-1041:Metadata security. User has CREATE READ UPDATE RIGHTS', function () {
        it('RP-TC-1041(9,10,11,12) should show rate control and Add Member control', function () {
            var metadata = createMetadata(['CREATE', 'UPDATE', 'READ'], ['CREATE', 'UPDATE', 'READ']);
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getRelationsFacet').and.returnValue({
                setFilter: Ext.emptyFn,
                setFilterEnabled: Ext.emptyFn
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(controller, 'getRelationsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn
            });
            controller.relationConfig = [{
                sortOrder: 'asc',
                type: 'rating',
                content: {
                    entityTypes: ['configuration/entityTypes/HCP'],
                    outRelations: [{uri: 'configuration/relationTypes/HasHealthCareRole'}]
                }
            }];

            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            controller.onRelationsResponse([{
                connections: [
                    {
                        entity: {
                            entityLabel: 'test',
                            entityType: 'configuration/entityTypes/HCP',
                            entityUri: 'entities/123'
                        },
                        relation: {
                            relationUri: 'relations/123',
                            relationType: 'configuration/relationTypes/HasHealthCareRole',
                            relationLabel: 'label',
                            direction: 'out',
                            ratings: []
                        }
                    }
                ],
                index: 0,
                total: 1
            }]);

            var allItems = getAllRelationships(store.getData());
            expect(allItems.filter(isEditable).length).toBe(1);
            expect(allItems.filter(isRemovable).length).toBe(0);
            expect(allItems.filter(hasVoteButton).length).toBe(1);
            expect(hasAddButton(store.getData())).toBeTruthy();
        });
    });
    describe('RP-TC-1063:Metadata security. User with CRU role on relation and RU on entities (8,9)', function () {
        it('shouldn\'t add the add member button if a user can\'t create entity type and allowOnlyCreateNew: true', function () {
            var metadata = createMetadata(['UPDATE', 'READ'], ['CREATE', 'UPDATE', 'READ']);
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            controller.relationConfig = [{
                content: {
                    outRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}],
                    entityTypes: ['configuration/entityTypes/HCP'],
                    inRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}]
                },
                allowOnlyCreateNew: true
            }];
            var store = {
                clearData: Ext.emptyFn,
                add: jasmine.createSpy('add')
            };
            controller.initRelationsList(store);
            expect(store.add.calls.count()).toBe(0)
        })
    });

    describe('throttling ui cases', function () {
        var store;
        beforeEach(function () {
            controller.relationConfig = [{
                content: {
                    outRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}],
                    entityTypes: ['configuration/entityTypes/HCP']
                },
                allowOnlyCreateNew: true
            }];
            store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(controller, 'getRelationsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn
            });
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
        });
        it('should show a message in case of error', function () {
            spyOn(MobileUI.core.session.Session, 'requestRelationsImmediately').and.callFake(function(config, filter, entityUri, max, offset, success, failure, self){
                failure.call(self, {errorDetailMessage: 'error'});
            });
            controller.fillRelation();
            var errorItems = getErrorItems(store.getData());
            expect(errorItems.length).toBe(1);
            expect(errorItems[0].get('editor')()).toBe('error');
        });

    })
});