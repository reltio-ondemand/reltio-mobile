/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.RelationsTree', function () {
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.RelationsTree', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: function () {
            }
        });
        controller.launch();
    });
    var getAllItems = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'relationsTree'
        });
    };
    var getErrorItems = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'custom'
        });
    };
    var getItemsByLabel = function (model, label) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'relationsTree' && item.get('label') === label;
        });
    };
    var hasAddParent = function(item){
        return item.get('allowAddParent')
    };
    var hasAddChild = function(item){
        return item.get('allowAddChild')
    };
    var hasDelete = function(item){
        return item.get('allowDelete')
    };
    var metadata = {
        entityTypes: {
            'configuration/entityTypes/HCP': {
                access: ['READ'],
                attributes: [
                    {
                        label: 'Simple',
                        access: ['READ'],
                        name: 'Simple',
                        type: 'String',
                        uri: 'configuration/entityTypes/HCP/attributes/Simple'
                    }
                ]
            }
        },
        graphTypes :{
            'myGraph':{
                relationshipTypeURIs: ['configuration/relationTypes/HasHealthCareRole']
            }
        },
        relationTypes: {
            'configuration/relationTypes/HasHealthCareRole': {
                access: ['READ'],
                startObject: {
                    objectTypeURI: 'configuration/entityTypes/HCP'
                },
                endObject: {
                    objectTypeURI: 'configuration/entityTypes/HCP'
                },
                typeColor: '#556655',
                uri: 'configuration/relationTypes/HasHealthCareRole'
            }
        }
    };
    describe('RP-TC-1475. Readonly rights in relationship', function () {

        var response = {
            root: {
                total: 1,
                entity: {
                    label: 'root',
                    type: 'configuration/entityTypes/HCP',
                    uri: 'entities/123'
                },
                relation: {
                    direction: 'directed',
                    type: 'configuration/relationTypes/HasHealthCareRole',
                    uri: 'relations/123'
                },
                children: [
                            {
                                entity: {
                                    label: 'firstLevelChild',
                                    type: 'configuration/entityTypes/HCP',
                                    uri: 'entities/345'
                                },
                                relation: {
                                    direction: 'directed',
                                    type: 'configuration/relationTypes/HasHealthCareRole',
                                    uri: 'relations/345'
                                },
                                children: [
                                    {
                                        entity: {
                                            label: 'secondLevelChild',
                                            type: 'configuration/entityTypes/HCP',
                                            uri: 'entities/567'
                                        },
                                        relation: {
                                            direction: 'directed',
                                            type: 'configuration/relationTypes/HasHealthCareRole',
                                            uri: 'relations/567'
                                        },
                                        children: [],
                                        parent: 'entities/345',
                                        total: 1

                                    }
                                ],
                                parent: 'entities/123',
                                total: 1

                            }
                        ]

            },
            successful: true
        };
        it('RP-TC-2174:Metadata security. User has READ ONLY RIGHTS. Tree Facet', function () {
            controller.parsedParams = {};
            spyOn(controller,'getRelationsTreeFacet').and.returnValue({
                    setFacet: Ext.emptyFn
                }
            );
            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getRelationsTree').and.returnValue({
                getStore: function () {
                    return store;
                },
                on: Ext.emptyFn,
                fetchDone: Ext.emptyFn,
                getScrollable: Ext.emptyFn
            });

            spyOn(MobileUI.core.Services.getConfigurationManager(), 'getExtensionById').and.returnValue({
                graph: {
                    type: 'myGraph'
                }
            });
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getGraphType').and.callFake(function (uri) {
                return metadata.graphTypes[uri];
            });
            spyOn(controller,'fillBand');
            spyOn(MobileUI.core.session.Session, 'getFullTree').and.callFake(function(requestsContent, success, error, context){
                success.call(context, response);
            });
            controller.processRelationsTree({uri:123});
            expect(getAllItems(store.getData()).length).toBe(3);
            var rootItem = getItemsByLabel(store.getData(), 'root')[0];
            expect(hasAddChild(rootItem)).toBeFalsy();
            expect(hasAddParent(rootItem)).toBeFalsy();
            expect(hasDelete(rootItem)).toBeFalsy();
            var firstLevelChild = getItemsByLabel(store.getData(), 'firstLevelChild')[0];
            expect(hasAddChild(firstLevelChild)).toBeFalsy();
            expect(hasAddParent(firstLevelChild)).toBeFalsy();
            expect(hasDelete(firstLevelChild)).toBeFalsy();
            var secondLevelChild = getItemsByLabel(store.getData(), 'secondLevelChild')[0];
            expect(hasAddChild(secondLevelChild)).toBeFalsy();
            expect(hasAddParent(secondLevelChild)).toBeFalsy();
            expect(hasDelete(secondLevelChild)).toBeFalsy();
        });
    });

    describe('throttling ui cases', function () {
        var store;
        beforeEach(function () {
            store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(controller,'getRelationsTreeFacet').and.returnValue({
                    setFacet: Ext.emptyFn
                }
            );
            spyOn(controller,'fillBand');
            controller.parsedParams = {};
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(MobileUI.core.Services.getConfigurationManager(), 'getExtensionById').and.returnValue({
                graph: {
                    type: 'myGraph'
                }
            });
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getGraphType').and.callFake(function (uri) {
                return metadata.graphTypes[uri];
            });
            spyOn(controller, 'getRelationsTree').and.returnValue({
                getStore: function () {
                    return store;
                },
                on: Ext.emptyFn,
                fetchDone: Ext.emptyFn,
                getScrollable: Ext.emptyFn
            });
        });
        it('should show a message in case of error on tree loading', function () {
            spyOn(MobileUI.core.session.Session, 'getFullTree').and.callFake(function(requestsContent, success, failure, self){
                failure.call(self, {errorDetailMessage: 'error'});
            });
            controller.processRelationsTree({uri:123});
            var errorItems = getErrorItems(store.getData());
            expect(errorItems.length).toBe(1);
            expect(errorItems[0].get('editor')()).toBe('error');
        });
        it('should show a message in case of error on pending loading', function () {
            spyOn(MobileUI.core.session.Session, 'getFullTree').and.callFake(function(requestsContent, success, failure, self){
                success.call(self, {
                    successful: true,
                    root: {
                        total: 0
                    }
                });
            });
            spyOn(MobileUI.core.session.Session, 'getGraph').and.callFake(function(entityUri, type, deep, limit, success, failure, self){
                failure.call(self, {errorDetailMessage: 'error'});
            });
            spyOn(controller, 'createTree');
            controller.processRelationsTree({uri:123});
            controller.pending('nodeId', Ext.emptyFn, 0, null);
            var errorItems = getErrorItems(store.getData());
            expect(errorItems.length).toBe(1);
            expect(errorItems[0].get('editor')()).toBe('error');
        });
    })
});