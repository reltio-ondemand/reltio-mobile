/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.RelationshipType', function () {
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.dialogs.RelationshipType', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: Ext.emptyFn,
            setEntityTypeFilter: Ext.emptyFn
        });
        controller.launch();
    });

    describe('RP-TC-1063:Metadata security. User with CRU role on relation and RU on entities (5,6,7)', function () {
        var createMetadata = function (entityAccess, relationAccess) {
            return {
                entityTypes: {
                    'configuration/entityTypes/HCP': {
                        access: entityAccess || ['CREATE','UPDATE','READ'],
                        attributes: [
                            {
                                label: 'Simple',
                                access: entityAccess || ['CREATE','UPDATE','READ'],
                                name: 'Simple',
                                type: 'String',
                                uri: 'configuration/entityTypes/HCP/attributes/Simple'
                            }
                        ]
                    }
                },
                relationTypes: {
                    'configuration/relationTypes/HasHealthCareRole': {
                        access: relationAccess || ['CREATE','UPDATE','READ'],
                        startObject: {
                            objectTypeURI: 'configuration/entityTypes/HCP'
                        },
                        endObject: {
                            objectTypeURI: 'configuration/entityTypes/HCP'
                        },
                        typeColor: '#556655',
                        uri: 'configuration/relationTypes/HasHealthCareRole'
                    }
                }
            }
        };
        beforeEach(function () {
            spyOn(MobileUI.core.SuiteManager, 'hideDialogs');
            spyOn(MobileUI.core.relations.RelationsManager, 'setState');
        });


        it('should show choose relation dialog in case of multiple relations', function () {
            var config = {
                outRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}],
                entityTypes: ['configuration/entityTypes/HCP'],
                inRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}]
            };
            var metadata = createMetadata();
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.SuiteManager, 'hasDialog');
            spyOn(MobileUI.core.SuiteManager, 'showDialog');
            spyOn(MobileUI.core.SuiteManager, 'clearState');
            spyOn(MobileUI.core.SuiteManager, 'putState');
            spyOn(controller, 'fillList');
            controller.showRelationshipTypeDialog(config, {});
            expect(controller.fillList).toHaveBeenCalled();
        });
        it('should show select entity dialog in case of single relation', function () {
            var config = {
                entityTypes: ['configuration/entityTypes/HCP'],
                inRelations: [{label: '', uri: 'configuration/relationTypes/HasHealthCareRole'}]
            };
            var metadata = createMetadata();
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            spyOn(MobileUI.core.SuiteManager, 'hasDialog');
            spyOn(MobileUI.core.SuiteManager, 'showDialog');
            spyOn(MobileUI.core.SuiteManager, 'clearState');
            spyOn(MobileUI.core.SuiteManager, 'putState');
            controller.showRelationshipTypeDialog(config, {});
            expect(MobileUI.core.SuiteManager.showDialog.calls.mostRecent().args[0]).toBe('chooseEntity');
        });
    });
});