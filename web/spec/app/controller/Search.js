/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.Search', function () {

    it('should search entities on global filter change', function (done) {
        spyOn(MobileUI.core.Services, 'setUserPreference');
        spyOn(MobileUI.core.Metadata, 'on');

        var application = Ext.create('Ext.app.Application', {name: 'Test'});
        spyOn(application, 'getController').and.returnValue({
            on: function (name, callback, self) {
                callback.call(self);
            }
        });

        var view = Ext.create('MobileUI.view.Search', {
            renderTo: Ext.getBody()
        });

        spyOn(Ext.Viewport, 'getActiveItem').and.returnValue(view);

        var controller = Ext.create('MobileUI.controller.Search', {
            application: application
        });

        spyOn(controller, 'onShow');

        controller.launch();

        MobileUI.core.session.Session.cache['1'] = {query: 'saved-search-query'};

        var spy = spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params) {
            if (url === MobileUI.core.session.MSearch.URL_SEARCH_TOTAL) {
                if (spy.calls.count() === 2) {
                    expect(params[0]).toBe('filter=(saved-search-query)', 'query should include global filter if it is enabled');
                }
                else if (spy.calls.count() === 6) {
                    expect(params[0]).toBe('filter=', 'query should be empty if global filter is disabled');
                    done();
                }
            }
        });

        MobileUI.core.search.SearchParametersManager.setGlobalFilter(['1'], false);

        MobileUI.core.search.SearchParametersManager.setGlobalFilter(['1'], true);
    });
});