/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.TwoHopsConnections', function ()
{
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.TwoHopsConnections', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: function () {
            }
        });
        controller.launch();
    });
    var getErrorItems = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'custom'
        });
    };
    var getAllRelationships = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('type') === 'twoHopsInfo'
        });
    };
    var hasAddButton = function (model) {
        return model.getRange(0,99).filter(function (item) {
            return item.get('label') === 'Add member'
        }).length > 0;
    };
    describe('RP-TC-1475. Readonly rights in relationship', function(){
        var healthRelation = [{
            connections: [
                {
                    entity: {
                        entityLabel: 'test',
                        entityType: 'configuration/entityTypes/HCP',
                        entityUri: 'entities/123'
                    },
                    relation: {
                        relationUri: 'relations/123',
                        relationType: 'configuration/relationTypes/HasHealthCareRole',
                        relationLabel: 'label',
                        direction: 'out',
                        ratings: []
                    }
                }
            ],
            index: 0,
            total: 1
        }];
        var addressRelation = [{
            connections: [
                {
                    entity: {
                        entityLabel: 'address',
                        entityType: 'configuration/entityTypes/Location',
                        entityUri: 'entities/abc'
                    },
                    relation: {
                        relationUri: 'relations/cde',
                        relationType: 'configuration/relationTypes/HasAddress',
                        relationLabel: 'label',
                        direction: 'out',
                        ratings: []
                    }
                }
            ],
            index: 0,
            total: 1
        }];
        var metadata = {
            entityTypes: {
                'configuration/entityTypes/HCP': {
                    access: ['READ'],
                    attributes: [
                        {
                            label: 'Simple',
                            access: ['READ'],
                            name: 'Simple',
                            type: 'String',
                            uri: 'configuration/entityTypes/HCP/attributes/Simple'
                        },
                        {
                            label: 'Nested',
                            name: 'Nested',
                            access: ['READ'],
                            type: 'Nested',
                            uri: 'configuration/entityTypes/HCP/attributes/Nested',
                            attributes: [
                                {
                                    label: 'SimpleInsideNested',
                                    name: 'SimpleInsideNested',
                                    type: 'String',
                                    access: ['READ'],
                                    uri: 'configuration/entityTypes/HCP/attributes/Nested/attributes/SimpleInsideNested'
                                }
                            ]
                        }
                    ]
                },
                'configuration/entityTypes/Location': {
                    access: ['READ'],
                    attributes: [
                        {
                            label: 'SimpleAddress',
                            access: ['READ'],
                            name: 'SimpleAddress',
                            type: 'String',
                            uri: 'configuration/entityTypes/Location/attributes/SimpleAddress'
                        }
                    ]
                }
            },
            relationTypes: {
                'configuration/relationTypes/HasHealthCareRole': {
                    access: ['READ'],
                    startObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    endObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    attributes: [
                        {
                            label: 'Simple',
                            access: ['READ'],
                            name: 'Simple',
                            type: 'String',
                            uri: 'configuration/relationTypes/HasHealthCareRole/attributes/Simple'
                        }
                    ],
                    typeColor: '#556655',
                    uri: 'configuration/relationTypes/HasHealthCareRole'
                },
                'configuration/relationTypes/HasAddress': {
                    access: ['READ'],
                    startObject: {
                        objectTypeURI: 'configuration/entityTypes/HCP'
                    },
                    endObject: {
                        objectTypeURI: 'configuration/entityTypes/Location'
                    },
                    typeColor: '#556655',
                    uri: 'configuration/relationTypes/HasAddress'
                }
            }
        };
        it('RP-TC-1475(6) should show all relation attributes as readonly on all the attributes if it has read only rights', function () {
            spyOn(MobileUI.core.Metadata, 'getRelationType').and.callFake(function (uri) {
                return metadata.relationTypes[uri];
            });
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.callFake(function (uri) {
                return metadata.entityTypes[uri];
            });
            controller.configuration = [{
                content: {
                    outRelations: [{uri: 'configuration/relationTypes/HasHealthCareRole'}]
                },
                contentSecondLevel:{
                    outRelations: [{uri: 'configuration/relationTypes/HasAddress'}]
                }
            }];
            var store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getTwoHopsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn,
                getScrollable: Ext.emptyFn
            });
            spyOn(controller, 'getTwoHopsFacet').and.returnValue({
                setFacet: Ext.emptyFn,
                setFilter: Ext.emptyFn,
                setFilterEnabled: Ext.emptyFn
            });
            spyOn(MobileUI.core.session.Session, 'requestRelationsImmediately').and.callFake(function(config,filter, entityUri, max, offset, success, failure, context){
                if(config[0].outRelations[0].uri === 'configuration/relationTypes/HasHealthCareRole'){
                    success.call(context, healthRelation);
                } else {
                    success.call(context, addressRelation);
                }
            });
            controller.fillRelation('uri', 1, 0);
            var relation = store.getAt(store.findBy(function (i) {
                return i.get('type') === 'twoHopsInfo';
            }));
            controller.onExpand({record:relation});
            var relations = getAllRelationships(store.getData());
            expect(relations.length).toBe(2);
            expect(hasAddButton(store.getData())).toBeFalsy();
        });
    });

    describe('RP-TC-801. Check second-level filter', function () {
        var store;

        beforeEach(function () {
            spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
                uris: []
            });

            spyOn(MobileUI.core.relations.RelationsUtils, 'isCorrectRelationType');
            spyOn(MobileUI.core.relations.RelationsUtils, 'getContentRelationsType');


            var record = new MobileUI.components.list.model.EListModel({
                label: 'FirstLevel',
                actionLabel: 'relations/1'
            });
            store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            store.add(record);


            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getTwoHopsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn,
                getScrollable: Ext.emptyFn
            });
            spyOn(controller, 'onParentDelete');
            controller.tmpStore = [];
            controller.configuration = [{
                content: {
                    outRelations: [{uri: 'configuration/relationTypes/HasHealthCareRole'}]
                },
                contentSecondLevel: {
                    outRelations: [{uri: 'configuration/relationTypes/HasAddress'}]
                }
            }];
        });

        it('Step 2. Filter second level relations', function () {
            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success, failure, self) {
                expect(options.data[0].filter).toBe('containsWordStartingWith(entity.label,\'test\')', 'search request should contain "test" search string');

                var response = [{
                    returnObjects: false,
                    returnDates: false,
                    index: 0,
                    total: 0
                }];
                success.call(self, response);
            });

            var item = {
                getValue: function () {
                    return 'test';
                },
                element: {
                    parent: function () {
                        return {
                            dom: {
                                getAttribute: function () {
                                    return 'relations/1';
                                }
                            }
                        };
                    }
                }
            };
            controller.onInnerFilter(item);

            expect(store.getAllCount()).toBe(1, 'first level relation should exists despite the empty relations in response');
            expect(store.getAt(0).get('label')).toBe('FirstLevel');
        });

        it('Step 3. Clear filter', function (done) {
            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options) {
                expect(options.data[0].filter).toBeUndefined('filter should be empty');
                done();
            });

            var item = {
                getValue: function () {
                    return '';
                },
                element: {
                    parent: function () {
                        return {
                            dom: {
                                getAttribute: function () {
                                    return 'relations/1';
                                }
                            }
                        };
                    }
                }
            };

            controller.onInnerFilter(item);
        });
    });

    describe('throttling ui cases', function () {
        var store;
        beforeEach(function () {
            spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
                uris: []
            });

            spyOn(MobileUI.core.relations.RelationsUtils, 'isCorrectRelationType');
            spyOn(MobileUI.core.relations.RelationsUtils, 'getContentRelationsType');
            store = Ext.create('Ext.data.Store', {
                storeId: 'test',
                data: [],
                model: 'MobileUI.components.list.model.EListModel'
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl');
            spyOn(controller, 'getPanel').and.returnValue({
                setMasked: Ext.emptyFn
            });
            spyOn(controller, 'getTwoHopsList').and.returnValue({
                getStore: function () {
                    return store;
                },
                fetchDone: Ext.emptyFn,
                getScrollable: Ext.emptyFn
            });

            controller.configuration = [{
                content: {
                    outRelations: [{uri: 'configuration/relationTypes/HasHealthCareRole'}]
                },
                contentSecondLevel:{
                    outRelations: [{uri: 'configuration/relationTypes/HasAddress'}]
                }
            }];
        });
        it('should show a message in case of error on first level loading', function () {
            spyOn(MobileUI.core.session.Session, 'requestRelationsImmediately').and.callFake(function(config, filter, entityUri, max, offset, success, failure, self){
                failure.call(self, {errorDetailMessage: 'error'});
            });
            controller.fillRelation('uri', 1, 0);
            var errorItems = getErrorItems(store.getData());
            expect(errorItems.length).toBe(1);
            expect(errorItems[0].get('editor')()).toBe('error');

        });
        it('should show a message in case of error on second levelt loading', function () {
            spyOn(MobileUI.core.session.Session, 'requestRelationsImmediately').and.callFake(function(config, filter, entityUri, max, offset, success, failure, self){
                failure.call(self, {errorDetailMessage: 'error'});
            });
            controller.pending('uri', Ext.emptyFn, 1, 0);
            var errorItems = getErrorItems(store.getData());
            expect(errorItems.length).toBe(1);
            expect(errorItems[0].get('editor')()).toBe('error');
        });
    })
});