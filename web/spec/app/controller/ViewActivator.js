/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.ViewActivator', function () {
    var app, controller;
    beforeEach(function () {
        app = Ext.create('Ext.app.Application', {name: 'Menu'});
        controller = Ext.create('MobileUI.controller.ViewActivator', {application: app});
        spyOn(controller.getApplication(), 'getController').and.returnValue({
            on: function () {
            }
        });
        controller.launch();
    });
    describe('beforeLogin method test', function () {
        it('should redirect to tenant not found in case of error ', function(){
            var session = MobileUI.core.session.Session;
            var util  = MobileUI.core.util.Util;
            spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({sso: 'http://test'});
            spyOn(util, 'supportSSO').and.returnValue(true);
            spyOn(util, 'redirect');
            spyOn(session, 'checkAccessTokenFromStore').and.returnValue();
            spyOn(session, 'getAccessToken').and.returnValue('access_token');
            spyOn(session, 'initSession').and.callFake(function (success, failure, context) {
                failure.call(context, {errorCode: 309});
            });
            controller.beforeLogin(Ext.emptyFn);
            expect(util.redirect.calls.mostRecent().args[0]).toContain('id=nonexistentTenant');
        })
    });
});