/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.dialogs.CreateRelation', function ()
{

    it('Shouldn\'t run required entity attributes in case relation editing', function ()
        {
            var saveButton = {};
            saveButton.setDisabled = Ext.emptyFn;
            spyOn(saveButton, 'setDisabled');
            var relationController = MobileUI.app.getController('dialogs.CreateRelation');
            spyOn(relationController, 'getSaveButton').and.returnValue(saveButton);
            spyOn(relationController, 'shouldValidateEntity').and.returnValue(false);
            spyOn(relationController, 'validateLookups').and.returnValue({then: Ext.emptyFn});
            spyOn(Promise, 'all').and.returnValue({then: Ext.emptyFn});
            relationController.onSaveTap();
            var args = Promise.all.calls.mostRecent().args[0];
            expect(args.length).toEqual(1);
        }
    );
    it('Should run required entity attributes in case relation creation', function ()
        {
            var saveButton = {};
            saveButton.setDisabled = Ext.emptyFn;
            spyOn(saveButton, 'setDisabled');
            var relationController = MobileUI.app.getController('dialogs.CreateRelation');
            spyOn(relationController, 'getSaveButton').and.returnValue(saveButton);
            spyOn(relationController, 'shouldValidateEntity').and.returnValue(true);
            spyOn(relationController, 'validateLookups').and.returnValue({then: Ext.emptyFn});
            spyOn(MobileUI.core.entity.EntityUtils, 'collectLabelAttributes');
            var old = relationController.referenceEntity;
            relationController.referenceEntity = {};
            spyOn(relationController, 'allRequiredAttributesFilled').and.returnValue({then: Ext.emptyFn});
            spyOn(Promise, 'all').and.returnValue({then: Ext.emptyFn});
            relationController.onSaveTap();
            var args = Promise.all.calls.mostRecent().args[0];
            relationController.referenceEntity = old;
            expect(args.length).toEqual(2);
        }
    );
    it('Should disable save button', function (done)
        {
            var saveButton = {};
            saveButton.setDisabled = Ext.emptyFn;
            var setDisabledFunc = spyOn(saveButton, 'setDisabled');
            var relationController = MobileUI.app.getController('dialogs.CreateRelation');
            spyOn(relationController, 'shouldValidateEntity');
            spyOn(relationController, 'onAfterAllValidations');
            spyOn(relationController, 'getSaveButton').and.returnValue(saveButton);
            spyOn(relationController, 'validateLookups').and.returnValue(Promise.resolve({}));
            relationController.referenceEntity = {};
            relationController.onSaveTap();
            setTimeout(function(){
                expect(setDisabledFunc.calls.count()).toBe(1);
                expect(setDisabledFunc.calls.mostRecent().args[0]).toBe(true);
                done();
            }, 10);
        }
    );
});