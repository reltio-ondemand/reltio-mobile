/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.dialogs.EditSavedSearch', function () {
    var controller;
    beforeEach(function () {
        controller = Ext.create('MobileUI.controller.dialogs.EditSavedSearch', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });

        controller.launch();
    });

    it('Should always show Public toggle', function () {
        spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
            uris: []
        });

        MobileUI.controller.dialogs.EditSavedSearch.show();

        var publicToggleButton = controller.getPropertiesList().getStore().getData().getRange(0, 99).filter(function (item) {
            return item.get('label') === 'Public';
        })[0];

        expect(publicToggleButton).toBeDefined();
    });
});