/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.dialogs.GlobalFilter', function () {
    it('should mute global filter on toggle button', function () {
        spyOn(MobileUI.core.Services, 'setUserPreference');

        MobileUI.core.search.SearchParametersManager.setGlobalFilter(['1'], true);

        Ext.create('MobileUI.view.dialogs.GlobalFilter', {
            renderTo: Ext.getBody()
        });

        var controller = Ext.create('MobileUI.controller.dialogs.GlobalFilter', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });

        var toggle = controller.getToggle();

        toggle.setValue(1);
        expect(MobileUI.core.search.SearchParametersManager.getGlobalFilter().mute).toBeFalsy();

        toggle.setValue(0);
        expect(MobileUI.core.search.SearchParametersManager.getGlobalFilter().mute).toBeTruthy();
    });
});