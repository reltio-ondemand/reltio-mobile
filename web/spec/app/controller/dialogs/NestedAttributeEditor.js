/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.dialogs.NestedAttributeEditor', function () {
    describe('get required attributes method test', function () {
        var entity = {
            attributes: []
        };
        it('Should return null if no required attributes', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            var nestedAttrType = {
                attributes: [
                    {
                        name: 'Boolean',
                        type: 'Boolean',
                        hidden: false,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/Boolean',
                        required: false
                    }
                ]
            };
            spyOn(nestedEditor, 'getProfileConfiguration').and.returnValue(null);
            expect(nestedEditor.getRequiredAttributes(nestedAttrType, entity, 'test').length).toBe(0);
        });

        it('Should return required attributes if it is not hidden', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            var nestedAttrType = {
                attributes: [
                    {
                        name: 'Boolean',
                        type: 'Boolean',
                        hidden: false,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/Boolean',
                        required: true
                    },
                    {
                        name: 'String',
                        type: 'String',
                        hidden: false,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/String',
                        required: false
                    }
                ]
            };
            spyOn(nestedEditor, 'getProfileConfiguration').and.returnValue(null);
            var result = nestedEditor.getRequiredAttributes(nestedAttrType, entity, 'test');
            expect(result.length).toBe(1);
            expect(result[0].name).toBe('Boolean');
        });

        it('Should filter required attributes if they are hidden', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            var nestedAttrType = {
                attributes: [
                    {
                        name: 'Boolean',
                        type: 'Boolean',
                        hidden: true,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/Boolean',
                        required: true
                    },
                    {
                        name: 'String',
                        type: 'String',
                        hidden: false,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/String',
                        required: true
                    }
                ]
            };
            spyOn(nestedEditor, 'getProfileConfiguration').and.returnValue(
                {
                    hiddenAttributes: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/String'
                });
            var result = nestedEditor.getRequiredAttributes(nestedAttrType, entity, 'test');
            expect(result.length).toBe(0);
        });

        it('Shouldn\'t add attributes if they are presented in entity', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            spyOn(nestedEditor, 'getProfileConfiguration').and.returnValue(null);
            var nestedAttrType = {
                attributes: [
                    {
                        name: 'Boolean',
                        type: 'Boolean',
                        hidden: false,
                        uri: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/Boolean',
                        required: true
                    }
                ]
            };
            var entity = {
                attributes: [{
                    name: 'Boolean',
                    type: 'configuration/entityTypes/EntityType/attributes/Nested/attributes/Boolean',
                    value: false
                }]
            };
            var result = nestedEditor.getRequiredAttributes(nestedAttrType, entity, 'test');
            expect(result.length).toBe(0);
        });
    });

    describe('cancel functionality test', function () {
        it('should return to previous state if it is available', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            spyOn(MobileUI.core.SuiteManager, 'popState');
            spyOn( MobileUI.core.session.Session.getLookupsManager(), 'cancelState');
            spyOn(MobileUI.core.SuiteManager, 'getCurrentState').and.returnValue({state: 'test'});
            spyOn(nestedEditor, 'processState');
            nestedEditor.onCancelButtonTap();
            expect(nestedEditor.processState).toHaveBeenCalledWith('test');
        });

        it('should hide dialogs if state is not available', function () {
            var nestedEditor = MobileUI.app.getController('dialogs.NestedAttributeEditor');
            spyOn(MobileUI.core.SuiteManager, 'popState');
            spyOn(MobileUI.core.SuiteManager, 'hideDialogs');
            spyOn( MobileUI.core.session.Session.getLookupsManager(), 'cancelState');
            spyOn(MobileUI.core.SuiteManager, 'getCurrentState').and.returnValue(null);
            spyOn(nestedEditor, 'processState');
            nestedEditor.onCancelButtonTap();
            expect(MobileUI.core.SuiteManager.hideDialogs).toHaveBeenCalledWith(true);
        })
    })
});