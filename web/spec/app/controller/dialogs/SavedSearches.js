/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.controller.dialogs.SavedSearches', function () {
    it('should set correct buttons for saved searched based on user and global filter option', function () {
        spyOn(MobileUI.core.Services, 'setUserPreference');
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success, failure, self) {
            var data = {};
            if (url === MobileUI.core.session.MSavedSearch.URL_SAVED_SEARCH_FIND) {
                data = {
                    result: [{
                        uri: '1',
                        owner: 'user'
                    }, {
                        uri: '2',
                        owner: 'somebody'
                    }]
                };
            }

            success.call(self, data);
        });

        spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
            uris: [
                '1'
            ]
        });

        spyOn(MobileUI.core.session.Session, 'getUser').and.returnValue({
            full_name: 'user'
        });

        Ext.create('MobileUI.view.dialogs.SavedSearches', {
            renderTo: Ext.getBody()
        });

        var controller = Ext.create('MobileUI.controller.dialogs.SavedSearches', {
            application: Ext.create('Ext.app.Application', {name: 'Test'})
        });

        controller.refreshList();

        var items = controller.getSearchesList().getStore().getData().getRange(0, 99);

        expect(items[0].get('userData').isGlobalFilter).toBe(true);
        expect(items[0].get('userData').ownedByCurrentUser).toBe(true);

        expect(items[1].get('userData').isGlobalFilter).toBeFalsy();
        expect(items[1].get('userData').ownedByCurrentUser).toBeFalsy();
    });
});