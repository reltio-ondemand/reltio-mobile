/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.DependentLookupManager', function ()
{
    var dependentLookupManager;
    beforeEach(function (done) {
        dependentLookupManager = Ext.create('MobileUI.core.DependentLookupsManager');
        done();
    });
    describe('autopopulate lookups tests', function ()
    {

        it('shouldn\'t fail if isTouched method was invoked before touch', function(){
            expect(dependentLookupManager.isTouched('testuri')).toBeFalsy();
        })
    });
    describe('fill values from entities correctly', function ()
    {
        it('should ignore empty values from entity', function(){
            var info =  {
                uri: 'entity/HCO/attributes/test',
                values: []
            };
            var hierarchy = {
                getInfo: function(){
                    return info;
                },
                getChildren: function(){
                    return [];
                }
            };
            spyOn(dependentLookupManager, 'getCurrentLookupsHierarchy').and.returnValue(hierarchy);
            var entity = {
                attributes:
                    {String:[{
                        type: 'entity/HCO/attributes/test',
                        value: ''
                    }]}

            };
            dependentLookupManager.fillValuesFromEntity(entity);
            expect(hierarchy.getInfo().values.length).toBe(0);
        });

        it('should populate hierarchy by values from entity', function(){
            var info =  {
                uri: 'entity/HCO/attributes/test',
                values: []
            };
            var hierarchy = {
                getInfo: function(){
                    return info;
                },
                getChildren: function(){
                    return [];
                }
            };
            spyOn(dependentLookupManager, 'getCurrentLookupsHierarchy').and.returnValue(hierarchy);
            var entity = {
                attributes:
                    {String:[{
                        type: 'entity/HCO/attributes/test',
                        value: 'test',
                        lookupCode: 'test'
                    }]}

            };
            dependentLookupManager.fillValuesFromEntity(entity);
            expect(hierarchy.getInfo().values[0].value).toBe('test');
        });
    });

});