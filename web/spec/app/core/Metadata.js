/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.Metadata', function () {

    describe('getEntityAttributeHierarchy', function () {

        it('should find attribute from referenced entity', function () {
            MobileUI.core.Metadata.parseMetadata({
                entityTypes: [{
                    uri: 'configuration/entityTypes/HCP',
                    attributes: [{
                        name: 'Address',
                        type: 'Reference',
                        referencedAttributeURIs: [
                            'configuration/entityTypes/Location/attributes/AddressLine2'
                        ]
                    }]
                }, {
                    uri: 'configuration/entityTypes/Location',
                    attributes: [{
                        name: 'AddressLine2',
                        type: 'String',
                        uri: 'configuration/entityTypes/Location/attributes/AddressLine2'
                    }]
                }],
                sources: [],
                survivorshipStrategies: []
            });

            var hierarchy = MobileUI.core.Metadata.getEntityAttributeHierarchy('configuration/entityTypes/HCP/attributes/Address/attributes/AddressLine2', true);

            expect(hierarchy.length).toBe(3);
            expect(hierarchy[2].uri).toBe('configuration/entityTypes/Location/attributes/AddressLine2');
        });
    });
});