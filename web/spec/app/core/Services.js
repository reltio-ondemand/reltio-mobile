/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.Services', function ()
{
    describe('getTenantName method', function ()
    {
        var Services;
        beforeEach(function ()
        {
            Services = MobileUI.core.Services;
        });
        it('should be a function', function ()
        {
            expect(typeof Services.getTenantName).toEqual('function');
        });

        it('should return a string', function ()
        {
            expect(typeof Services.getTenantName()).toEqual('string');
        });

        it('should return the last element in pathname as tenant name', function ()
        {
            Services.tenantName = null;
            spyOn(Services, 'getLocationObject').and.callFake(function ()
            {
                return {pathname: '/MobileUI/a5/'};
            });
            expect(Services.getTenantName()).toEqual('a5');
        });

        it('should return empty tenant name if pathname has only one element and slash', function()
        {
            Services.tenantName = null;
            spyOn(Services, 'getLocationObject').and.callFake(function ()
            {
                return {pathname: '/MobileUI/'};
            });
            expect(Services.getTenantName()).toEqual('');
        });
        it('should return empty tenant name if pathname has only one element', function()
        {
            Services.tenantName = null;
            spyOn(Services, 'getLocationObject').and.callFake(function ()
            {
                return {pathname: '/MobileUI'};
            });
            expect(Services.getTenantName()).toEqual('');
        });

        it('should return the element before last in pathname as tenant name if path contains .htm', function ()
        {
            Services.tenantName = null;
            spyOn(Services, 'getLocationObject').and.callFake(function ()
            {
                return {pathname: '/MobileUI/a5/index.html'};
            });
            expect(Services.getTenantName()).toEqual('a5');
        });

    });

    describe('get configuration manager test', function ()
    {
        var Services;
        beforeEach(function ()
        {
            Services = MobileUI.core.Services;
        });
        it('should be a function', function ()
        {
            expect(typeof Services.getConfigurationManager).toEqual('function');
        });

        it('should return object', function ()
        {
            expect(typeof Services.getConfigurationManager()).not.toBe(null);
        });
    });

    describe('get search result builder test', function ()
    {
        var Services;
        beforeEach(function ()
        {
            Services = MobileUI.core.Services;
        });
        it('should be a function', function ()
        {
            expect(typeof Services.getSearchResultBuilder).toEqual('function');
        });

        it('should return object', function ()
        {
            expect(typeof Services.getSearchResultBuilder()).not.toBe(null);
        });
    });

    describe('check tenant test', function ()
    {
        var Services;
        beforeEach(function ()
        {
            Services = MobileUI.core.Services;
        });
        it('should be a function', function ()
        {
            expect(typeof Services.checkTenant).toEqual('function');
        });

        it('should send request with object which contains  tenant name', function ()
        {
            spyOn(MobileUI.core.session.Session, 'sendRequest');
            var tenant = 'test';
            Services.checkTenant(tenant, null, null, null);

            expect(MobileUI.core.session.Session.sendRequest).
                toHaveBeenCalledWith('/configuration/roles', {tenant: 'test'}, null, null, null, null)
        });
    });

    describe('getEnvironmentUrl method', function ()
    {
        var Services;
        beforeEach(function ()
        {
            Services = MobileUI.core.Services;
        });
        afterEach(function(){
            Services.settings = null;
            Services.environmentUrl = null;
        });

        it('should return full environment url string from remote url', function ()
        {
            Services.settings = {
                apiPath: 'https://tst-01.reltio.com/reltio'
            };

            expect(Services.getEnvironmentUrl()).toEqual('https://tst-01.reltio.com');
        });

        it('should return full environment url string from local url', function ()
        {
            Services.settings = {
                apiPath: 'localhost:8888/reltio'
            };

            expect(Services.getEnvironmentUrl()).toEqual('localhost:8888');
        });
    });
    describe('requestUISettings tests', function(){
        it('should encode tenant name on get request', function () {
            spyOn(MobileUI.core.Services, 'asyncRequest');
            spyOn(MobileUI.core.Services, 'getTenantName').and.returnValue('uitest\'\'');
            MobileUI.core.Services.requestUISettings();
            expect(MobileUI.core.Services.asyncRequest.calls.mostRecent().args[0]).toBe('/configuration?tenant=uitest\'\'');
        })
    })
});