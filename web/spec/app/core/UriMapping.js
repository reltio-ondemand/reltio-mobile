/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.UriMapping', function () {

    describe('main use case tests', function(){
        it('should resolve mapping', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            mapping.registerMapping('__temp__123/attributes/ee','attributes/Address/xxx1');
            var result = mapping.resolveMapping('__temp__123/attributes/ee/123/123/attributes');
            expect(result).toBe('attributes/Address/xxx1/123/123/attributes');
        });
        it('shouldn\'t broke not mapped url', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            var result = mapping.resolveMapping('__temp__123/test/ee/123/123/test');
            expect(result).toBe('__temp__123/test/ee/123/123/test');
        });
        it('shouldn\'t map after mapping clear', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            mapping.registerMapping('__temp__123/attributes/ee','attributes/Address/xxx1');
            var result = mapping.resolveMapping('__temp__123/attributes/ee/123/123/attributes');
            expect(result).toBe('attributes/Address/xxx1/123/123/attributes');
            mapping.clearMapping();
            result = mapping.resolveMapping('__temp__123/attributes/ee/123/123/attributes');
            expect(result).toBe('__temp__123/ee/123/123/attributes');
        });
        it('should resolve uri for reference attributes', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            mapping.registerMapping('entities/TTXgNWY','attributes/Address/xxx1');
            var result = mapping.resolveSubMapping('entity/attributes/Address/TTXgNWY');
            expect(result).toBe('entity/attributes/Address/xxx1');
        });
        it('shouldn\'t resolve uri for non existence reference attributes', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            var result = mapping.resolveSubMapping('entity/attributes/Address/TTXgNWY');
            expect(result).toBe(null);
        });
        it('should work correctly for a new uri format', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            mapping.registerMapping('entity/uri$$123', 'uri$$345');
            var result = mapping.resolveMapping('entity/uri$$123/Test');
            expect(result).toBe('uri$$345/Test');
        });
        it('simple mapping shouldn\'t remove attributes', function(){
            var mapping = Ext.create('MobileUI.core.UriMapping');
            mapping.register('entity/uri$$123', 'entity/uri$$345/attributes/1');
            var result = mapping.resolve('entity/uri$$123/Test');
            expect(result).toBe('entity/uri$$345/attributes/1/Test');
        });
    })
});