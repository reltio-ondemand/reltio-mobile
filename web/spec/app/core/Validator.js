/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.Validator', function () {

    describe('find attribute tests', function () {
        beforeEach(function () {
            jasmine.addMatchers(window.JasmineObjectMatchers);
        });

        it('should get correct label in path for reference attribute with existing entity', function () {
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByName').and.returnValue('Reference');
            var value1 = {
                'Reference': [
                    {name: name, uri: 'testUri', refEntity: 'testRefEntityUri', label: ''}
                ]
            };
            var value2 = {
                'Reference': [
                    {name: name, uri: 'testUri', refEntity: 'testRefEntityUri', label: 'label'}
                ]
            };
            var validator = Ext.create('MobileUI.core.Validator');
            expect(validator.findAttribute('testUri', value1, 'entityType', [])[0].label).toBe('&lt;No label&gt;');
            expect(validator.findAttribute('testUri', value2, 'entityType', [])[0].label).toBe('label');
        });

        it('should get correct label in path for nested attribute', function () {

            var attrType = {
                type: 'Nested',
                label: 'Nested'
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByName').and.returnValue(attrType);
            spyOn(MobileUI.core.entity.EntityUtils, 'evaluateLabel');
            var value = {
                'Nested': [
                    {name: name, uri: 'testUri', value: {'Simple': []}}
                ]
            };
            var validator = Ext.create('MobileUI.core.Validator');
            validator.findAttribute('testUri', value, 'entityType', []);
            expect(MobileUI.core.entity.EntityUtils.evaluateLabel).toHaveBeenCalledWith(null, {'Simple': []}, attrType);
        });

        it('should return no label for no label attributes', function () {

            var attrType = {
                type: 'Nested',
                label: 'Label'
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByName').and.returnValue(attrType);
            spyOn(MobileUI.core.entity.EntityUtils, 'evaluateLabel');
            var value = {
                'Nested': [
                    {name: name, uri: 'testUri', value: {'Simple': []}}
                ]
            };
            var validator = Ext.create('MobileUI.core.Validator');
            var result = validator.findAttribute('testUri', value, 'entityType', []);
            expect(result[0].label).toEqual('&lt;No label&gt;');
        });
        it('should return label for attributes with label', function () {

            var attrType = {
                type: 'Simple',
                label: 'Simple'
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByName').and.returnValue(attrType);
            var value = {
                'Simple': [
                    {name: 'name', uri: 'testUri', value: 'label'}
                ]
            };
            var validator = Ext.create('MobileUI.core.Validator');
            var result = validator.findAttribute('testUri', value, 'entityType', []);
            expect(result[0].label).toEqual('label');
        });
        it('should find attributes based on mapping', function () {
            var value = {
                'Phone': [
                    {
                        'ov': true,
                        'type': 'configuration/entityTypes/HCP/attributes/Phone',
                        'uri': 'entities/__temp_aVjE5h/attributes/Phone/SCQa4H',
                        'value': {
                            'LocalNumber': [
                                {
                                    'ov': true,
                                    'type': 'configuration/entityTypes/HCP/attributes/Phone/attributes/LocalNumber',
                                    'uri': 'entities/__temp_aVjE5h/attributes/Phone/SCQa4H/LocalNumber/6s1fb2',
                                    'value': 'Test'
                                }
                            ]
                        }
                    }
                ]
            };
            spyOn(MobileUI.core.session.Session, 'getUriMapping').and.returnValue({
                resolveMapping:function(uri){
                    return uri.replace('entities/__temp_aVjE5h', 'entities/xxx/Address/xxx')
                },
                resolveSubMapping:function(){
                    return null;
                }
            });
            var attrType = {
                type: 'Simple',
                label: 'Local Number'
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByUri').and.returnValue(attrType);
            var validator = Ext.create('MobileUI.core.Validator');
            var result = validator.findAttribute('entities/xxx/Address/xxx/attributes/Phone/SCQa4H/LocalNumber/6s1fb2',value, null, []);
            expect(result.length).toBe(2);
            expect(result[1].attrName).toBe('Local Number');
            expect(result[1].label).toBe('Test');
        });

        it('should construct path for reference attributes', function () {
            var entity = {
                attributes: {
                    Affiliations: [{
                        value: {
                            TypeCode: [{
                                value: 'national health care provider',
                                uri: 'entities/1Tempo4rary_4/attributes/TypeCode/1Tempo4rary_3'
                            }]
                        },
                        refEntity: {
                            'objectURI': 'entities/1Tempo4rary_4'
                        },
                        label: 'label',
                        uri: 'entities/1Tempo4rary_1/attributes/Affiliations/1Tempo4rary_2'
                    }]
                },
                type: 'configuration/entityTypes/HCP'
            };

            MobileUI.core.session.Session.getUriMapping().register('entities/uri$$_4', 'entities/uri$$_1/attributes/Affiliations/uri$$_2');

            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByUri');
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByName');

            var validator = Ext.create('MobileUI.core.Validator');
            var path = validator.findPath('entities/uri$$_1/attributes/Affiliations/uri$$_2/TypeCode/uri$$_3', entity);

            expect(path.length).toBe(2);
        });
    });

    describe('refCompare method tests', function(){
        it('should return true if attribute uri is ended with entity attribute uri', function(){
            var validator = Ext.create('MobileUI.core.Validator');
            expect(validator.refUriCompare('/attr/123', '/entities/111/attr/123')).toBeTruthy();
        });

        it('should return false if attribute is not part of uri', function(){
            var validator = Ext.create('MobileUI.core.Validator');
            expect(validator.refUriCompare('/attr/124', '/entities/111/attr/123')).toBeFalsy();
        });

    });

    describe('updateExternalValidationResults method tests', function(){
        beforeEach(function ()
        {
            jasmine.addMatchers(window.JasmineObjectMatchers);
        });
        it('should process missed error type and add it to validation', function(){
            var validator = Ext.create('MobileUI.core.Validator');
            validator.updateExternalValidationResults({
                invalidType:'missed',
                objectParentUri: 'test',
                value: 'test'
            });

            expect(validator.collectAttributeErrors('test').error).toContainValues({
                message:'test',
                type: 'incorrectValue'
            });
        });


    })
});