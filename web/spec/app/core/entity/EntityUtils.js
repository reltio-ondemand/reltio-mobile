/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.entity.EntityUtils', function () {
    var metadata = null,
        setUp = function (done) {
            jasmine.Ajax.install();
            MobileUI.core.Services.tenantName = 'a5';
            MobileUI.core.Services.settings = {};
            MobileUI.core.Services.settings.apiPath = '';
            metadata = MobileUI.core.Metadata;
            jasmine.Ajax.stubRequest(new RegExp('^/api/a5/configuration')).andReturn({responseText: window.mocked.getMetadata()});
            metadata.on('init', function () {
                done();
            });
            metadata.init();
        },
        tearDown = function (done) {
            jasmine.Ajax.uninstall();
            done();
        };
    beforeAll(setUp, 1000);
    afterAll(tearDown);
    describe('processAttributes method', function () {
        it('Should group the entity response', function () {
                var groupConfig = [{
                        'content': ['configuration/entityTypes/HCP/attributes/FirstName', 'configuration/entityTypes/HCP/attributes/LastName', 'configuration/entityTypes/HCP/attributes/Gender'],
                        'label': 'Personality'
                    }, {
                        'content': ['Video URL', 'Image URL'],
                        'label': 'Media'
                    }],
                    attributes = MobileUI.core.entity.EntityUtils.processAttributes(window.mocked.getTestEntity(), groupConfig);
                expect(Object.keys(attributes).length).toBe(6);
                expect(attributes['Other'].length).toBe(4);
            }
        );

        it('Should group all simple items in Other group in case of no group config', function () {
                var attributes = MobileUI.core.entity.EntityUtils.processAttributes(window.mocked.getTestEntity());
                expect(Object.keys(attributes).length).toBe(4);
                expect(attributes['Other'].length).toBe(5);
            }
        );

        it('Should filter items by url', function () {
                var before = MobileUI.core.entity.EntityUtils.processAttributes(window.mocked.getTestEntity(), null),
                    filtered;
                expect(before).not.toBe(null);
                expect(before['Other']).not.toBe(null);
                expect(before['Other'].length).toBe(5);
                filtered = MobileUI.core.entity.EntityUtils.processAttributes(window.mocked.getTestEntity(), null, ['configuration/entityTypes/HCO/attributes/ImageLinks']);
                expect(filtered).not.toBe(null);
                expect(filtered['Other']).not.toBe(null);
                expect(filtered['Other'].length).toBe(4);
            }
        );

        it('Should process incorrect params correctly', function () {
                var emptyAttributes = MobileUI.core.entity.EntityUtils.processAttributes(null);
                expect(Object.keys(emptyAttributes).length).toBe(0);
            }
        );
    });
    describe('getImageAttributeValue method', function () {

        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.getImageAttributeValue).not.toBe(undefined)
        });
        it('Should return image attributes value', function () {
            var imageAttribute = MobileUI.core.entity.EntityUtils.getImageAttributeValue(window.mocked.getTestEntity(), 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf');
            expect(imageAttribute).not.toBe(null);
            expect(imageAttribute).toBe('http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Alta_Bates_Summit_Medical_Center.jpg/275px-Alta_Bates_Summit_Medical_Center.jpg');
        });
        it('Should return null entity is not set', function () {
            var imageAttribute = MobileUI.core.entity.EntityUtils.getImageAttributeValue(null, '');
            expect(imageAttribute).toBe(null);
        });
        it('Should return first image attribute if url is not set', function () {
            var imageAttribute = MobileUI.core.entity.EntityUtils.getImageAttributeValue(window.mocked.getTestEntity(), null);
            expect(imageAttribute).not.toBe(null);
        })
    });

    describe('removeAttribute method', function () {
        var testEntity = {
            attributes: [
                {
                    id: 1,
                    uri: 'test1'
                },
                {
                    id: 2,
                    uri: 'test1/child1'
                },
                {
                    id: 3,
                    uri: 'test1/child2'
                },
                {
                    id: 4,
                    uri: 'test2'
                },
                {
                    id: 5,
                    uri: 'test2/child2'
                }
            ]
        };
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.removeAttribute).not.toBe(undefined)
        });
        it('Shouldn\'t change something in case of incorrect data', function () {
            var test = [];
            MobileUI.core.entity.EntityUtils.removeAttribute(test);
            expect(test).toBe(test);
        });
        it('Should replace current attribute and remove all childs', function () {
            var entity = Ext.clone(testEntity);
            MobileUI.core.entity.EntityUtils.removeAttribute(entity, 'test1');
            expect(entity.attributes.length).toBe(3);
        });

        it('Should replace current attribute with new empty definition', function () {
            var uri = 'test1';
            var entity = Ext.clone(testEntity);
            MobileUI.core.entity.EntityUtils.removeAttribute(entity, uri);
            var item = entity.attributes.filter(function (item) {
                return item.uri === uri;
            });
            item = item.length > 0 ? item[0] : {};

            expect(item.value).not.toBe(undefined);
            expect(item.value.length).toBe(0);
        });
        it('Shouldn\'t affect other attributes', function () {
            var uri = 'test1';
            var entity = Ext.clone(testEntity);
            MobileUI.core.entity.EntityUtils.removeAttribute(entity, uri);
            var item = entity.attributes.filter(function (item) {
                return item.uri.indexOf('test2') !== -1;
            });
            expect(item).not.toBe(undefined);
            expect(item.length).toBe(2);
        });
    });

    describe('processEntitiesAttributes method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.processEntitiesAttributes).not.toBe(undefined)
        });
        it('Should return empty object if parent not found', function () {
            var attributes = Ext.clone(window.mocked.getAttributes());
            var result = MobileUI.core.entity.EntityUtils.processEntitiesAttributes(attributes, 'not_existing_parent');
            expect(Object.keys(result).length).toBe(0);
        });
        it('Should convert array to entity values object where the attributes with empty values shouldn\'t exists', function () {
            var attributes = Ext.clone(window.mocked.getAttributes());
            var result = MobileUI.core.entity.EntityUtils.processEntitiesAttributes(attributes, 'entities/$temp_Vl0D1w');
            expect(Object.keys(result).length).toBe(1);
        });
        it('Should creates the nested values', function () {
            var attributes = Ext.clone(window.mocked.getAttributes());
            var result = MobileUI.core.entity.EntityUtils.processEntitiesAttributes(attributes, 'entities/$temp_Vl0D1w');
            result = result.Address[0].value;
            expect(result.Zip).not.toBe(undefined);
        });
    });

    describe('prepareEntityBeforeSave method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.prepareEntityBeforeSave).not.toBe(undefined)
        });

        it('Should create entity for save', function () {
            var entity = {};
            entity.attributes = Ext.clone(window.mocked.getAttributes());
            entity.uri = 'entities/$temp_Vl0D1w';
            var result = MobileUI.core.entity.EntityUtils.prepareEntityBeforeSave(entity);
            expect(Object.keys(result.attributes).length).toBe(1);
        });
        it('Should add types to result entity', function () {
            var entity = {};
            entity.attributes = Ext.clone(window.mocked.getAttributes());
            entity.type = 'type';
            entity.uri = 'entities/$temp_Vl0D1w';
            var result = MobileUI.core.entity.EntityUtils.prepareEntityBeforeSave(entity);
            expect(result.type).toBe('type');
        });
        it('Should add roles to result entity', function () {
            var entity = {};
            entity.attributes = Ext.clone(window.mocked.getAttributes());
            entity.roles = 'roles';
            entity.uri = 'entities/$temp_Vl0D1w';
            var result = MobileUI.core.entity.EntityUtils.prepareEntityBeforeSave(entity);
            expect(result.roles).toBe('roles');
        });
    });

    describe('collectLabelAttributes method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.collectLabelAttributes).not.toBe(undefined)
        });

        it('Should collect attributes', function () {
            var entityType = MobileUI.core.Metadata.getEntityType('configuration/entityTypes/Location');
            var expected = (entityType.dataLabelPattern || '').split('}').concat((entityType.secondaryLabelPattern || '').split('}')).concat(entityType.businessCardAttributeURIs || []).filter(function (item) {
                return item.trim().length > 0
            });
            var result = MobileUI.core.entity.EntityUtils.collectLabelAttributes('configuration/entityTypes/Location');
            expect(expected.length).toBe(result.length);
        });

        it('Should collect attributes base on entity type object', function () {
            var entityType = MobileUI.core.Metadata.getEntityType('configuration/entityTypes/Location');
            var expected = (entityType.dataLabelPattern || '').split('}').concat((entityType.secondaryLabelPattern || '').split('}')).concat(entityType.businessCardAttributeURIs || []).filter(function (item) {
                return item.trim().length > 0
            });
            var result = MobileUI.core.entity.EntityUtils.collectLabelAttributes(entityType);
            expect(expected.length).toBe(result.length);
        });

    });

    describe('findLabelPattern method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.findLabelPattern).not.toBe(undefined)
        });

        it('Should return null if entity type not found', function () {
            var result = MobileUI.core.entity.EntityUtils.findLabelPattern('not_existing');
            expect(result).toBe(null);
        });

        it('Should return label pattern if entity type found', function () {
            var result = MobileUI.core.entity.EntityUtils.findLabelPattern('configuration/entityTypes/Location');
            expect(result).not.toBe(null);
        });
        it('Should return correct label pattern', function () {
            //https://reltio.jira.com/browse/RP-11653
            var result = MobileUI.core.entity.EntityUtils.findLabelPattern('configuration/entityTypes/Location');
            var entity = MobileUI.core.Metadata.getEntityType('configuration/entityTypes/Location');
            expect(result).toBe(entity.dataLabelPattern);
        });


    });
    describe('findSecondLabelPattern method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.findSecondaryLabelPattern).not.toBe(undefined)
        });

        it('Should return null if entity type not found', function () {
            var result = MobileUI.core.entity.EntityUtils.findSecondaryLabelPattern('not_existing');
            expect(result).toBe(null);
        });

        it('Should return label pattern if entity type found', function () {
            var result = MobileUI.core.entity.EntityUtils.findSecondaryLabelPattern('configuration/entityTypes/HCP');
            expect(result).not.toBe(null);
        });
    });

    describe('isComplex method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.isComplex).not.toBe(undefined)
        });

        it('Should return false in case of incorrect data', function () {
            expect(MobileUI.core.entity.EntityUtils.isComplex(null)).toBe(false);
        });

        it('Should return true in case of Reference attribute type data', function () {
            var attrType = {
                type: 'Reference'
            };
            expect(MobileUI.core.entity.EntityUtils.isComplex(attrType)).toBe(true);
        });
        it('Should return true in case of Nested attribute type data', function () {
            var attrType = {
                type: 'Nested'
            };
            expect(MobileUI.core.entity.EntityUtils.isComplex(attrType)).toBe(true);
        });
        it('Should verify the string type names', function () {
            expect(MobileUI.core.entity.EntityUtils.isComplex('Nested')).toBe(true);
            expect(MobileUI.core.entity.EntityUtils.isComplex('Reference')).toBe(true);
        });
    });
    describe('isReference method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.isReference).not.toBe(undefined)
        });

        it('Should return false in case of incorrect data', function () {
            expect(MobileUI.core.entity.EntityUtils.isReference(null)).toBe(false);
        });

        it('Should return true in case of Reference attribute type data', function () {
            var attrType = {
                type: 'Reference'
            };
            expect(MobileUI.core.entity.EntityUtils.isReference(attrType)).toBe(true);
        });

        it('Should verify the string type names', function () {
            expect(MobileUI.core.entity.EntityUtils.isComplex('Reference')).toBe(true);
        });
    });

    describe('findAttribute method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.findAttribute).not.toBe(undefined)
        });
        it('Should return null in case of incorrect arguments', function () {
            expect(MobileUI.core.entity.EntityUtils.findAttribute(null)).toBe(null)
        });
        it('Should return null if attribute not found', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.findAttribute(attributes, 'not_existing_uri')).toBe(null)
        });
        it('Should return not null if attribute found', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.findAttribute(attributes, 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ')).not.toBe(null);
        });
        it('Should create complex name in case of complex attribute', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.findAttribute(attributes, 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ').name).toBe('attributes.Zip.Zip5');
        });
        it('Should create simple name in case of simple attribute', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.findAttribute(attributes, 'entities/5upheAV/attributes/City/1nrLHJw3').name).toBe('attributes.City');
        });
        it('Should take into account the name attribute', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.findAttribute(attributes, 'entities/5upheAV/attributes/City/1nrLHJw3', 'test').name).toBe('test.City');
        });
    });

    describe('collectPath method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.collectPath).not.toBe(undefined)
        });
        it('Should return null in case of incorrect arguments', function () {
            expect(MobileUI.core.entity.EntityUtils.collectPath(null)).toBe(null)
        });
        it('Should return null if attribute not found', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.collectPath(attributes, null, 'not_existing_uri')).toBe(null)
        });
        it('Should return not null if attribute found', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            expect(MobileUI.core.entity.EntityUtils.collectPath(attributes, 'configuration/entityTypes/Location', 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ')).not.toBe(null);
        });
        it('Should return correct path for nested attributes', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            var result = MobileUI.core.entity.EntityUtils.collectPath(attributes, 'configuration/entityTypes/Location', 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ');
            expect(result).toEqual(jasmine.arrayContaining(['Zip', 'Zip5']));
        });
        it('Should return correct path for simple attributes', function () {
            var attributes = window.mocked.getLocationEntity().attributes;
            var result = MobileUI.core.entity.EntityUtils.collectPath(attributes, 'configuration/entityTypes/Location', 'entities/5upheAV/attributes/Zip/1nrLHOCJ');
            expect(result).toEqual(jasmine.arrayContaining(['Zip']));
        });
    });

    describe('createAttributesModel method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.createAttributesModel).not.toBe(undefined)
        });
        it('Should return not null if attributes are correct', function () {
            var groupedAttributes = window.mocked.getGroupedAttributes();
            expect(MobileUI.core.entity.EntityUtils.createAttributesModel(groupedAttributes)).not.toBe(null);
        });
        it('Should create header as first element', function () {
            var groupedAttributes = window.mocked.getGroupedAttributes();
            var result = MobileUI.core.entity.EntityUtils.createAttributesModel(groupedAttributes);
            expect(result[0].type).toBe('header');
        });

        it('Should create header for each key', function () {
            var groupedAttributes = window.mocked.getGroupedAttributes();
            var expectedHeaders = Object.keys(groupedAttributes);
            var result = MobileUI.core.entity.EntityUtils.createAttributesModel(groupedAttributes).filter(function (item) {
                return item.type === 'header';
            }).map(function (item) {
                return item.label;
            });
            expect(result).toEqual(jasmine.arrayContaining(expectedHeaders));
        });
        it('Should create header for each group', function () {
            var groupedAttributes = window.mocked.getGroupedAttributes();
            var expectedHeaders = Object.keys(groupedAttributes);
            var result = MobileUI.core.entity.EntityUtils.createAttributesModel(groupedAttributes).filter(function (item) {
                return item.type === 'header';
            }).map(function (item) {
                return item.label;
            });
            expect(result).toEqual(jasmine.arrayContaining(expectedHeaders));
        });

        it('Should create one line for each value in group', function () {
            var groupedAttributes = window.mocked.getGroupedAttributes();
            var result = MobileUI.core.entity.EntityUtils.createAttributesModel(groupedAttributes);
            var expected = [];
            for (var i = 1, len; len = result.length, i < len; i++) {
                if (result[i].type === 'header')
                    break;
                expected.push(result[i]);
            }
            expect(expected.length).toBe(groupedAttributes['Personal Details'].length);
        });

    });

    describe('evaluateEntityLabel method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.evaluateEntityLabel).not.toBe(undefined)
        });

        it('Should return empty string if the label pattern not defined', function () {
            var result = MobileUI.core.entity.EntityUtils.evaluateEntityLabel(window.mocked.getLocationEntity(), null);
            expect(result).toBe('');
        });

        it('Should return label', function () {
            var labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern('configuration/entityTypes/Location');
            var result = MobileUI.core.entity.EntityUtils.evaluateEntityLabel(window.mocked.getLocationEntity(), labelPattern);
            expect(result).toBe('Something Place 5712 Test.City_5712 Test.ZIP5.5712- ');
        });
    });

    describe('evaluateLabel method', function () {
        it('Should be a function', function () {
            expect(MobileUI.core.entity.EntityUtils.evaluateLabel).not.toBe(undefined)
        });

        it('Should return empty string if the label pattern not defined', function () {
            var result = MobileUI.core.entity.EntityUtils.evaluateLabel(window.mocked.getLocationEntity(), null, window.mocked.getReferencedAttribute());
            expect(result).toBe('');
        });

        it('Should return label', function () {
            var labelPattern = MobileUI.core.entity.EntityUtils.findLabelPattern('configuration/entityTypes/Location');
            var result = MobileUI.core.entity.EntityUtils.evaluateEntityLabel(window.mocked.getLocationEntity(), labelPattern);
            expect(result).toBe('Something Place 5712 Test.City_5712 Test.ZIP5.5712- ');
        });
    });

    describe('validationMessage method', function () {
        it('Should return correct error in case of custom message', function () {
            var error = {
                validationFailed: [
                    {
                        value: 'custom1'
                    }
                ]
            };

            var errors = {
                validationFailed: [
                    {
                        value: 'custom1'
                    },
                    {
                        value: 'custom2'
                    }
                ]
            };
            expect(MobileUI.core.entity.EntityUtils.validationMessage(error)).toBe('<div class="validation-header">custom1</div>');
            expect(MobileUI.core.entity.EntityUtils.validationMessage(errors)).toBe('<div class="validation-header">custom1</div><div class="validation-header">custom2</div>');
        });
    });

    describe('validationMessageNew method', function () {
        it('Should use attribute label for message', function () {
            var errors = [{
                attrType: {label: 'Test'},
                value: 'Error'
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe('<div class="validation-header">Test:</div><div class="validation-record">Error</div>');
        });

        it('Should return custom message for errors with no attrType', function () {
            var errors = [{
                value: 'Error1'
            }, {
                value: 'Error2'
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe('<div class="validation-header">Error1</div><div class="validation-header">Error2</div>');
        });

        it('Should construct message based on path', function () {
            var errors = [{
                invalidType: 'incorrectValue',
                value: 'INCORRECT!',
                path: [{
                    label: 'Speciality=Cardiology',
                    attrName: 'Speciality'
                }, {
                    label: 'Cardiology',
                    attrName: 'Type'
                }]
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe(
                    '<div class="validation-header">Speciality:</div>' +
                    '<div class="validation-record">Speciality=Cardiology-Cardiology: INCORRECT!</div>');
        });

        it('Should process labels correctly', function () {
            var errors = [{
                invalidType: 'incorrectValue',
                value: 'INCORRECT!',
                path: [{
                    label: 'Speciality=Cardiology',
                    attrName: 'Speciality'
                }, {
                    label: Ext.util.Format.htmlEncode('<No label>'),
                    attrName: 'Type'
                }]
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe(
                    '<div class="validation-header">Speciality:</div>' +
                    '<div class="validation-record">Speciality=Cardiology-&lt;No label&gt;: INCORRECT!</div>');
        });

        it('Should combine errors for same attribute', function () {
            var errors = [{
                attrType: {label: 'Test'},
                value: 'Error1'
            }, {
                attrType: {label: 'Test'},
                value: 'Error2'
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe('<div class="validation-header">Test:</div><div class="validation-record-multiline">Error1</div><div class="validation-record-multiline">Error2</div>');
        });

        it('Should display unique errors only', function () {
            var errors = [{
                attrType: {label: 'Test'},
                value: 'Error1'
            }, {
                attrType: {label: 'Test'},
                value: 'Error2<h1>xss</h1>'
            }, {
                attrType: {label: 'Test'},
                value: 'Error2<h1>xss</h1>'
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe('<div class="validation-header">Test:</div><div class="validation-record-multiline">Error1</div><div class="validation-record-multiline">Error2&lt;h1&gt;xss&lt;/h1&gt;</div>');
        });

        it('Should escape error message', function () {
            var errors = [{
                invalidType: 'incorrectValue',
                value: '<h1>xss</h1>',
                path: [{
                    label: 'Speciality=Cardiology',
                    attrName: '<h1>Speciality</h1>'
                }, {
                    label: '<h1>Cardiology</h1>',
                    attrName: '<h1>Type</h1>'
                }]
            }];

            expect(MobileUI.core.entity.EntityUtils.validationMessageNew(errors))
                .toBe(
                    '<div class="validation-header">&lt;h1&gt;Speciality&lt;/h1&gt;:</div>' +
                    '<div class="validation-record">Speciality=Cardiology-&lt;h1&gt;Cardiology&lt;/h1&gt;: &lt;h1&gt;xss&lt;/h1&gt;</div>');
        });
    });

    describe('generate empty attribute test', function () {
        it('Should generate empty values for simple attribute', function () {
            var attrTypes = [
                {type: 'String', name: 'String'},
                {type: 'Nested', name: 'Nested'},
                {type: 'Reference', name: 'Reference'}
            ];
            var parentUri = 'test/';
            var result = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, parentUri);
            expect(Object.keys(result).length).toBe(1);
            var stringAttribute = result.String[0];
            expect(stringAttribute.value).toBe('');
            expect(stringAttribute.uri.indexOf('test/String')).toBe(0);
        });
        it('Should fill first string value', function () {
            var attrTypes = [
                {type: 'String', name: 'StringL', lookupCode: '1'},
                {type: 'String', name: 'StringE', values: ['v1']},
                {type: 'String', name: 'StringRestricted', access: ['READ']},
                {type: 'String', name: 'String'}
            ];
            var parentUri = 'test/';
            var result = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, parentUri, 'test');
            expect(Object.keys(result).length).toBe(4);
            var stringAttribute = result.String[0];
            expect(stringAttribute.value).toBe('test');
            expect(stringAttribute.uri.indexOf('test/String')).toBe(0);
        });
    });
    describe('isEmpty attribute test', function () {
        it('Should return true for empty attribute', function () {
            var attrTypes = [
                {type: 'String', name: 'String'},
                {type: 'Nested', name: 'Nested'},
                {type: 'Reference', name: 'Reference'}
            ];
            var parentUri = 'test/';
            var result = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, parentUri);
            expect(MobileUI.core.entity.EntityUtils.isEmptyAttribute(result)).toBe(true);
        });
        it('Should return false for non-empty attribute', function () {
            var attrTypes = [
                {type: 'String', name: 'StringL', lookupCode: '1'},
                {type: 'String', name: 'StringE', values: ['v1']},
                {type: 'String', name: 'String'}
            ];
            var parentUri = 'test/';
            var result = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, parentUri, 'test');
            expect(MobileUI.core.entity.EntityUtils.isEmptyAttribute(result)).toBe(false);

        });
        it('Should return false for non-empty lookup attribute', function () {
            var attrTypes = [
                {type: 'String', name: 'StringL', lookupCode: '1'}
            ];
            var parentUri = 'test/';
            var result = MobileUI.core.entity.EntityUtils.generateEmptyAttribute(attrTypes, parentUri);
            result.StringL[0].lookupCode = 'Test';
            expect(MobileUI.core.entity.EntityUtils.isEmptyAttribute(result)).toBe(false);

        });
    });

    describe('getUrlThumbnail method', function () {
        it('should return thumbnail url from first ov value', function () {
            var value = {
                value: {
                    CdnUrlThumbnail: [{
                        ov: false,
                        value: '1'
                    }, {
                        value: 'link'
                    }]
                }
            };
            expect(MobileUI.core.entity.EntityUtils.getUrlThumbnail(value)).toBe('link');
        });
    });

});