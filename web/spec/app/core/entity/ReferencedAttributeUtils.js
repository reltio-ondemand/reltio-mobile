/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.entity.ReferenceAttributeUtils', function ()
{
    var metadata = null,
        tearDown = function (done)
        {
            jasmine.Ajax.uninstall();
            done();
        };
    beforeAll(function (done)
    {
        jasmine.Ajax.install();
        MobileUI.core.Services.tenantName = 'a5';
        MobileUI.core.Services.settings = {};
        MobileUI.core.Services.settings.apiPath = '';
        metadata = MobileUI.core.Metadata;
        jasmine.Ajax.stubRequest(new RegExp('^/api/a5/configuration')).andReturn({responseText: window.mocked.getMetadata()});
        metadata.on('init', function ()
        {
            Ext.require('MobileUI.core.entity.ReferencedAttributeUtils', function ()
            {
                done();
            });
        });
        metadata.init();
    }, 1000);
    afterAll(tearDown);
    describe('createAttributesFromReferenced method', function ()
    {
        it('should be a function', function ()
        {
            expect(typeof MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced).toEqual('function');
        });

        it('should return error in case of incorrect arguments', function (done)
        {
            try
            {
                MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced();
            }
            catch (ex)
            {
                expect(ex.name).toEqual('Error');
                done();
            }
        });
        it('should creates the non-empty attributes array from referenced attribute', function ()
        {
            var referenced = window.mocked.getReferencedAttribute();
            var attributes = MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced(referenced);
            expect(attributes.length).toBeGreaterThan(0);
        });
        it('should return array where all elements has one parent and it\'s parent is referenced attribute if there is no referenced entity', function ()
        {
            var referenced = window.mocked.getReferencedAttribute();
            var attributes = MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced(referenced);
            expect(attributes.filter(function (attribute)
            {
                return attribute.parentUri !== referenced.uri;
            }).length).toEqual(0);
        });

        it('should fill the reference attributes from referenced entity', function ()
        {
            var referenced = window.mocked.getReferencedAttribute();
            var referencedEntity = window.mocked.getLocationEntity();
            var typeUriWithValues = [];
            for (var i in referencedEntity.attributes)
            {
                if (referencedEntity.attributes.hasOwnProperty(i))
                {
                    referencedEntity.attributes[i].forEach(function (item)
                    {
                        typeUriWithValues.push(item.type);
                    });
                }
            }
            var attributes = MobileUI.core.entity.ReferencedAttributeUtils.createAttributesFromReferenced(referenced, referencedEntity);
            var filledAttributes = attributes.filter(function (attribute)
            {
                return attribute.value.length > 0 && !Ext.isObject(attribute.value[0]);
            });
            expect(filledAttributes.some(function(item){
                return typeUriWithValues.indexOf(item.attrType.uri) !== -1;
            })).toEqual(true);
        });
    });
    describe('createNestedAttribute method', function ()
    {
        it('should be a function', function ()
        {
            expect(typeof MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute).toEqual('function');
        });

        it('should return error in case of incorrect arguments', function (done)
        {
            try
            {
                MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute();
            }
            catch (ex)
            {
                expect(ex.name).toEqual('Error');
                done();
            }
        });

        it('should return error in case of incorrect arguments', function (done)
        {
            try
            {
                MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute();
            }
            catch (ex)
            {
                expect(ex.name).toEqual('Error');
                done();
            }
        });

        it('should return the non-empty attribute array if nested attribute type found', function ()
        {
            var nestedAttribute = {
                'Zip5': [{
                    'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                    'ov': true,
                    'value': 'Test.ZIP5.7091',
                    'uri': 'entities/5uppl6x/attributes/Zip/1nrPraF9/Zip5/1nrPreVP'
                }]
            };
            var nested = MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute(nestedAttribute,'parent');
            expect(nested.length).toBeGreaterThan(0);
        });

        it('should return the empty attribute array if nested attribute type found', function ()
        {
            var nested = MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute({},'parent');
            expect(nested.length).toEqual(0);
        });
        it('should return the attribute object in array if nested attribute type found', function ()
        {
            var nestedAttribute = {
                'Zip5': [{
                    'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                    'ov': true,
                    'value': 'Test.ZIP5.7091',
                    'uri': 'entities/5uppl6x/attributes/Zip/1nrPraF9/Zip5/1nrPreVP'
                }]
            };
            var nested = MobileUI.core.entity.ReferencedAttributeUtils.createNestedAttribute(nestedAttribute,'parent');
            expect(nested[0].attrType.uri).toEqual(nestedAttribute.Zip5[0].type);
        });
    });
    describe('fillReferenceAttribute method', function ()
    {
        it('should be a function', function ()
        {
            expect(typeof MobileUI.core.entity.ReferencedAttributeUtils.fillReferenceAttribute).toEqual('function');
        });

        it('should return error in case of incorrect arguments', function (done)
        {
            try
            {
                MobileUI.core.entity.ReferencedAttributeUtils.fillReferenceAttribute();
            }
            catch (ex)
            {
                expect(ex.name).toEqual('Error');
                done();
            }
        });
        it('should creates the non-empty attributes array from referenced attribute', function ()
        {
            var referenced = window.mocked.getReferencedAttribute();
            var attributes = MobileUI.core.entity.ReferencedAttributeUtils.fillReferenceAttribute({attributes:[]},referenced, {attributes:[]}).attributes;
            expect(attributes.length).toBeGreaterThan(0);
        });
        it('should return original object with filled attribute', function ()
        {
            var referenced = window.mocked.getReferencedAttribute();
            var attributes = MobileUI.core.entity.ReferencedAttributeUtils.fillReferenceAttribute({attributes:[{
             uri: 'entities/$temp_7YHVF3/attributes/Address/zLUVty'
            }]},referenced,{attributes:[]}).attributes;
            attributes = attributes.filter(function(item){
                return item.uri === 'entities/$temp_7YHVF3/attributes/Address/zLUVty';
            });
            expect(attributes.length).toBeGreaterThan(0);
        });

    });
});