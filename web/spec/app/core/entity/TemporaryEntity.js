/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.entity.TemporaryEntity', function ()
{
    /*eslint no-unused-vars: "off"*/
    var TemporaryEntity,
        setUp = function (done)
        {
            Ext.require('MobileUI.core.entity.TemporaryEntity', function ()
            {
                TemporaryEntity = MobileUI.core.entity.TemporaryEntity;
                done();
            });
        },tearDown = function (done)
        {
            MobileUI.core.entity.TemporaryEntity.removeAllEntities();
            done();
        };
    beforeAll(setUp, 1000);
    afterAll(tearDown);
    describe('createNewEntity method', function ()
    {
        it('should create entity from string with unique uri', function ()
        {
            var first = MobileUI.core.entity.TemporaryEntity.createNewEntity('testType'),
                second = MobileUI.core.entity.TemporaryEntity.createNewEntity({uri: 'testType'});
            expect(first.uri).not.toBe(second.uri);
        });
        it('should create the same entity type from string and from object', function ()
        {
            var first = MobileUI.core.entity.TemporaryEntity.createNewEntity('testType'),
                second = MobileUI.core.entity.TemporaryEntity.createNewEntity({uri: 'testType'});
            expect(first.type).toBe(second.type);
        });
    });
    describe('addEntity method', function ()
    {
        var uri = 'testUri',
            testEntity = {type: 'entityType', attributes: [], uri: uri},
            anotherEntityWithSameUri = {type: 'test', attributes: [], uri: uri};
        it('should add entity if it is not in temporary list', function ()
        {
            MobileUI.core.entity.TemporaryEntity.addEntity(testEntity);
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri).type).toBe('entityType');
        });
        it('should not add entity with the same uri', function ()
        {
            MobileUI.core.entity.TemporaryEntity.addEntity(testEntity);
            MobileUI.core.entity.TemporaryEntity.addEntity(anotherEntityWithSameUri);
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri).type).toBe('entityType');
        });
    });
    describe('create attribute uri', function ()
    {
        var oldUri = 'testUri';
        it('should return old uri if there is no attribute type', function ()
        {
            expect(MobileUI.core.entity.TemporaryEntity.createAttributeUri(oldUri, null)).toBe(oldUri);
        });
        it('should compose the new uri using the following rule oldUri + / + attributeName + / + uniqueId', function ()
        {
            expect(MobileUI.core.entity.TemporaryEntity.createAttributeUri(oldUri, {name:'Test'})).not.toBe(oldUri);
            expect(MobileUI.core.entity.TemporaryEntity.createAttributeUri(oldUri, {name:'Test$$'})).toContain('Test$$');
        });
        it('should compose unique uri', function ()
        {
            expect(MobileUI.core.entity.TemporaryEntity.createAttributeUri(oldUri, {name:'Test'})).not.toBe(
                MobileUI.core.entity.TemporaryEntity.createAttributeUri(oldUri, {name:'Test'})
            );
        });
    });
    describe('update entity', function ()
    {
        var uri = 'testUri',
            testEntity = {type: 'entityType', attributes: [], uri: uri},
            anotherEntityWithSameUri = {type: 'test', attributes: [], uri: uri};
        it('should add entity to temporary list', function ()
        {
            MobileUI.core.entity.TemporaryEntity.updateEntity(testEntity);
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri).type).toBe('entityType');
        });
        it('should update entity with the same uri', function ()
        {
            MobileUI.core.entity.TemporaryEntity.addEntity(testEntity);
            MobileUI.core.entity.TemporaryEntity.updateEntity(anotherEntityWithSameUri);
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri).type).toBe('test');
        });
    });
    describe('remove all entities', function ()
    {
        var uri = 'testUri',
            testEntity = {type: 'entityType', attributes: [], uri: uri};
        it('should remove all the entities', function ()
        {
            MobileUI.core.entity.TemporaryEntity.removeAllEntities();
            MobileUI.core.entity.TemporaryEntity.addEntity(testEntity);
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri).type).toBe('entityType');
            MobileUI.core.entity.TemporaryEntity.removeAllEntities();
            expect(MobileUI.core.entity.TemporaryEntity.getEntity(uri)).toBeNull();
        });
    });
});