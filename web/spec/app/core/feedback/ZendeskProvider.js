/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.feedback.ZendeskProvider', function () {
    describe('Process issue test', function () {
        var provider = Ext.create('MobileUI.core.feedback.ZendeskProvider');
        beforeEach(function () {
            jasmine.Ajax.install();

            spyOn(MobileUI.core.session.Session, 'callMethodWhenTokenAvailable').and.callFake(function (success, failure, args, self) {
                success.apply(self || MobileUI.core.session.Session, args);
            });
            spyOn(MobileUI.core.session.Session, 'getUser').and.returnValue({
                full_name: 'test'
            });

        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });
        it('should make correct request', function () {
            jasmine.Ajax.stubRequest(new RegExp('^/zendesk/issue')).andReturn({
                responseText: JSON.stringify(
                    {ticket: {id: 1}}
                )
            });
            var issueInfo = {
                summary: 'summary',
                description: 'description',
                stepsToReproduce: 'stepsToReproduce',
                actualResults: 'actualResults',
                expectedResults: 'expectedResults',
                didItWork: 'didItWork',
                businessImpact: 'businessImpact',
                dueDate: 'dueDate',
                ccs: 'ccs',
                files: [],
                priority: 'priority',
                options: 'regarding'
            };
            provider.processIssue(issueInfo, ['environment']);
            var requestParams = jasmine.Ajax.requests.mostRecent().params;
            expect(requestParams).toContain('summary=summary');
            expect(requestParams).toContain('description=description');
            expect(requestParams).toContain('steps_to_reproduce=stepsToReproduce');
            expect(requestParams).toContain('actual_results=actualResults');
            expect(requestParams).toContain('expected_results=expectedResults');
            expect(requestParams).toContain('due_date=dueDate');
            expect(requestParams).toContain('did_it_works=didItWork');
            expect(requestParams).toContain('business_impact=businessImpact');
            expect(requestParams).toContain('collaborator_ids=%5Bccs%5D');
            expect(requestParams).toContain('priority=priority');
            expect(requestParams).toContain('regarding=regarding');
            expect(requestParams).toContain('user=test');
            expect(requestParams).toContain('environment=environment');
        });

        it('should process errors correctly', function (done) {
            jasmine.Ajax.stubRequest(new RegExp('zendesk/issue')).andReturn({
                responseText: JSON.stringify({
                    error: 'error'
                })
            });
            var issueInfo = {
                summary: 'summary',
                description: 'description',
                stepsToReproduce: 'stepsToReproduce',
                actualResults: 'actualResults',
                expectedResults: 'expectedResults',
                didItWork: 'didItWork',
                businessImpact: 'businessImpact',
                dueDate: 'dueDate',
                ccs: 'ccs',
                files: [],
                priority: 'priority',
                options: 'regarding'
            };
            provider.processIssue(issueInfo, ['environment'], function(result){
                expect(result.text).toEqual('error');
                done();
            });
        });

        it('should process server errors correctly', function (done) {
            jasmine.Ajax.stubRequest(new RegExp('zendesk/issue'));
            var issueInfo = {
                summary: 'summary',
                description: 'description',
                stepsToReproduce: 'stepsToReproduce',
                actualResults: 'actualResults',
                expectedResults: 'expectedResults',
                didItWork: 'didItWork',
                businessImpact: 'businessImpact',
                dueDate: 'dueDate',
                ccs: 'ccs',
                files: [],
                priority: 'priority',
                options: 'regarding'
            };
            provider.processIssue(issueInfo, ['environment'], null, function(result){
                expect(result.error).toEqual('error');
                done();
            });
            var request = jasmine.Ajax.requests.mostRecent();
            request.respondWith({ status: 500, responseText: JSON.stringify({error:'error'}) });
        });
    })


});