/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.i18n', function() {

    describe('i18n', function() {
        it('should be function', function() {
            expect(typeof i18n).toEqual('function');
        });
    });

    describe('i18n initialization', function () {
        var config = {
            configuration: {
                translations: {
                    'en_US': 'http://en-us.reltio.com',
                    'ru_RU': 'http://ru-RU.reltio.com',
                    'de_DE': 'http://de-DE.reltio.com'
                }
            }
        };
        var dict = {
            'http://en-us.reltio.com': {
                test: 'test'
            },
            'http://de-DE.reltio.com': {
                test: 'deTest'
            },
            'http://ru-RU.reltio.com': {
                test: 'ruTest'
            }
        };
        var i18;
        beforeEach(function () {
            i18 = MobileUI.core.I18n;
            spyOn(Ext.Ajax, 'request').and.callFake(function (params) {
                params.success.call(this, {
                    responseText: JSON.stringify(dict[params.url])
                })
            });
            spyOn(moment, 'locale');
        });
        afterEach(function () {
            MobileUI.core.I18n.isInited = false;
            MobileUI.core.I18n.isInitedInsecure = false;
        });
        it('should download language for different region correctly', function (done) {
            spyOn(i18, 'detectLanguage').and.returnValue('en-US');
            spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
                getExtensionById: function () {
                    return config;
                }
            });
            i18.init(function () {
                expect(i18.dictionaries[0].test).toEqual('test');
                done();
            })
        });
        it('should download language without region correctly', function (done) {
            spyOn(i18, 'detectLanguage').and.returnValue('ru');
            spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
                getExtensionById: function () {
                    return config;
                }
            });
            i18.init(function () {
                expect(i18.dictionaries[0].test).toEqual('ruTest');
                done();
            })
        });
        it('should download language for non-found region correctly', function (done) {
            spyOn(i18, 'detectLanguage').and.returnValue('de-CH');
            spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
                getExtensionById: function () {
                    return config;
                }
            });
            i18.init(function () {
                expect(i18.dictionaries[0].test).toEqual('deTest');
                done();
            })
        });
    });

    describe('String translator', function() {
        it('should return same string if not found in dictionaries', function() {
            expect(i18n('N/A')).toEqual('N/A');
        });

        it('should return translated string from dictionary', function() {
            MobileUI.core.I18n.dictionaries = [{'a': 'A'}];
            expect(i18n('a')).toEqual('A');
        });

        it('should return translated string from dictionary with minimum index', function() {
            MobileUI.core.I18n.dictionaries = [{'b': 'A'}, {'a': 'B'}, {'a': 'C'}];
            expect(i18n('a')).toEqual('B');
        });
    });

    describe('Date translator', function() {
        it('should contain default translation', function() {
            expect(i18n(new Date(2015, 10, 2))).toEqual('11/2/2015');
        });

        it('should contain default short translation', function() {
            expect(i18n(new Date(2015, 10, 2), MobileUI.core.I18n.DATE_SHORT)).toEqual('11/2/2015');
        });

        it('should contain default long translation', function() {
            expect(i18n(new Date(2015, 10, 2, 10, 20), MobileUI.core.I18n.DATE_LONG)).toEqual('November 2, 2015 10:20 AM');
        });

        it('should translate format from dictionary', function() {
            MobileUI.core.I18n.dictionaries = [{__customDate: 'DD.MM.YYYY'}];
            expect(i18n(new Date(2015, 10, 2), '__customDate')).toEqual('02.11.2015');
        });
    });

    describe('Number translator', function() {
        it('should contain default translation', function() {
            expect(i18n(2546.2445)).toEqual('2,546.2445');
        });

        it('should format with specified decimal separator', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {decimalSeparator: ':'}}];
            expect(i18n(2546.2445, '__customNumber')).toEqual('2546:2445');
        });

        it('should format with specified group separator', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {groupSeparator: '`'}}];
            expect(i18n(2546.2445, '__customNumber')).toEqual('2`546.2445');
        });

        it('should format with specified group size', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {groupSize: 2, groupSeparator: ' '}}];
            expect(i18n(2546.2445, '__customNumber')).toEqual('25 46.2445');
        });

        it('should format with specified decimal part length', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {digitsAfterDecimalSeparator: 2}}];
            expect(i18n(2546.2445, '__customNumber')).toEqual('2546.24');
        });

        it('should format integers', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {digitsAfterDecimalSeparator: 0}}];
            expect(i18n(2546.2445, '__customNumber')).toEqual('2546');
        });
    });

    describe('Number parser', function() {
        it('should parse localized number', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {
                groupSize: 2,
                decimalSeparator: '/',
                groupSeparator: ' ',
                prefix: '>>',
                suffix: '<<'
            }}];
            expect(MobileUI.core.I18n.parseNumber('>>6 32 48/987<<', '__customNumber')).toEqual(63248.987);
        });

        it('should return NaN if failed to parse', function() {
            MobileUI.core.I18n.dictionaries = [{__customNumber: {
                groupSize: 2,
                decimalSeparator: '/',
                groupSeparator: ' ',
                prefix: '>>',
                suffix: '<<'
            }}];
            expect(isNaN(MobileUI.core.I18n.parseNumber('63248^987', '__customNumber'))).toEqual(true);
        });
    });
});
