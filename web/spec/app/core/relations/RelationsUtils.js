/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.relations.RelationsUtils', function () {
    it('Shouldn\'t return non readable relation types', function () {
        var config = ['configuration/relationTypes/canread', 'configuration/relationTypes/cantread'];
        var relationTypes = {
            'configuration/relationTypes/canread': {
                access: ['READ'],
                uri: 'configuration/relationTypes/canread'
            },
            'configuration/relationTypes/cantread': {
                access: [],
                uri: 'configuration/relationTypes/cantread'
            }
        };
        var result = MobileUI.core.relations.RelationsUtils.prepareRelations(config, relationTypes);
        expect(result.length).toBe(1);
        expect(result[0]).toBe('configuration/relationTypes/canread');
    });
});