/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.sandbox.API', function () {
    var configuration = {
            point: 'com.reltio.plugins.ui.view',
            id: 'com.reltio.plugins.entity.BackupButton',
            'class': 'com.reltio.plugins.ui.CustomActionButtonView',
            label: 'Backup Tenant Data',
            height: 26,
            width: 200,
            action: {
                permissions: ['https://tst-01.reltio.com'],
                files: ['https://dl.dropboxusercontent.com/s/1varxdlsjm8abzv/backupTenant.js']
            }
        },
        permissions = configuration.action.permissions;
    describe('initAPI method', function () {
        it('Should be defined and init the sandbox api', function () {
                var api = Ext.create('MobileUI.core.sandbox.API');
                expect(api.initApi).toBeDefined();
                api.initApi(permissions, configuration);
                expect(api.permissions).toEqual(permissions);
            }
        );
    });

    describe('process method', function () {
        it('Should be defined and process actions correctly', function () {
            var logAction,
                api = Ext.create('MobileUI.core.sandbox.API');
            expect(api.process).toBeDefined();
            spyOn(api, 'log');
            logAction = {action: 'log', params: 'message'};
            api.process(logAction);
            expect(api.log).toHaveBeenCalledWith('message');
        });
        it('Should be defined and process visibility correctly', function () {
            var logAction,
                api = Ext.create('MobileUI.core.sandbox.API');
            spyOn(api, 'setVisibility');
            logAction = {action: 'setVisibility'};
            api.process(logAction);
            expect(api.setVisibility).toHaveBeenCalledWith('visible');
        });
        it('Should be defined and process visibility correctly', function () {
            var logAction,
                api = Ext.create('MobileUI.core.sandbox.API');
            spyOn(api, 'setHtml');
            logAction = {action: 'setHtml', params: 'html'};
            api.process(logAction);
            expect(api.setHtml).toHaveBeenCalledWith('html');
        });
        it('Should be defined and process visibility correctly', function () {
            var logAction,
                api = Ext.create('MobileUI.core.sandbox.API');
            spyOn(api, 'processRequest');
            logAction = {action: 'request', params: {name: 'test', test: 'test'}};
            api.process(logAction);
            expect(api.processRequest).toHaveBeenCalledWith('test', {action: 'request', params: {name: 'test', test: 'test'}});

        });
    });

    describe('checkParams method', function () {
        var api;
        beforeEach(function(){
            api = Ext.create('MobileUI.core.sandbox.API');
            spyOn(api, 'fireDataEvent');
            api.initApi(permissions, configuration);
        });
        it('Should be defined', function () {
            expect(api.checkParams).toBeDefined();
        });
        it('Should return false and fire event in case of incorrect params', function () {
            expect(api.checkParams({id:'test'},'type')).toBeFalsy();
            expect(api.fireDataEvent).toHaveBeenCalledWith('test', 'type', {errorMessage: 'URL is empty'})
        });
        it('Should return false and fire event in case of not enough permissions', function () {
            expect(api.checkParams({id:'test', url:'http://idev.reltio.com'},'type')).toBeFalsy();
            expect(api.fireDataEvent).toHaveBeenCalledWith('test', 'type', {errorMessage: 'Not enough permissions'})
        });
        it('Should return true in case of enough permissions', function () {
            expect(api.checkParams({id:'test', url:'https://tst-01.reltio.com/test'},'type')).toBeTruthy();
        });
    });

    describe('getOptions method', function(){
        var api;
        beforeEach(function(){
            api = Ext.create('MobileUI.core.sandbox.API');
            api.initApi(permissions, configuration);
        });
        it('Should be defined', function(){
            expect(api.getOptions).toBeDefined();
        });
        it('Should return GET method type by default', function(){
            expect(api.getOptions().method).toBe('GET');
        });
        it('Should remove empty fields from the result', function(){
            var options = api.getOptions({method:'POST', tenant: null});
            expect(options.tenant).toBeUndefined();
        });

        it('Should return 4 maximum fields', function(){
            var options = api.getOptions({method:'POST', tenant: 'a5', headers: {}, data:'', someOther:'test'});
            expect(options.method).toBe('POST');
            expect(options.tenant).toBe('a5');
            expect(options.headers).toEqual({});
            expect(options.data).toBe('');
            expect(options.someOther).toBeUndefined();
        });
    })


});