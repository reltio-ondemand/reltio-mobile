/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.search.SearchResultBuilder', function () {
    describe('RP-TC-569.1', function () {
        var content = [
            {
                defaultProfilePic:
                    'entities/123/attributes/ImageLinks/456',
                defaultProfilePicValue:
                    'https://reltio-images/cool-pic.svg',
                label:
                    'Label',
                secondaryLabel:
                    'Secondary label',
                type:
                    'configuration/entityTypes/HCP',
                uri:
                    'entities/123'
            }
        ];
        beforeEach(function () {
            spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
                label: 'HCP<h1>xss</h1>',
                uri: 'configuration/entityTypes/HCP',
                typeColor: '#555555'
            });
            spyOn(MobileUI.core.Services, 'getBorderlessImageUrl')
        });
        it('should have primary label', function () {
            var resultsBuilder = Ext.create(MobileUI.core.search.SearchResultBuilder);
            expect(resultsBuilder.buildContent(content)[0].label).toBe('Label');
        });

        it('should have secondary label', function () {
            var resultsBuilder = Ext.create(MobileUI.core.search.SearchResultBuilder);
            expect(resultsBuilder.buildContent(content)[0].secondaryLabel).toBe('Secondary label');
        });

        it('should have icon', function () {
            var resultsBuilder = Ext.create(MobileUI.core.search.SearchResultBuilder);
            expect(resultsBuilder.buildContent(content)[0].avatar).toBe('https://reltio-images/cool-pic.svg');
        });

        it('should have entity type', function () {
            var resultsBuilder = Ext.create(MobileUI.core.search.SearchResultBuilder);
            expect(resultsBuilder.buildContent(content)[0].tags[0]).toEqual({ color: '#555555', value: 'HCP&lt;h1&gt;xss&lt;/h1&gt;' });
        });

    })
});