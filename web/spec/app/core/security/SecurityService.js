/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.security.SecurityService', function () {
    describe('checkUserPermission method', function () {
        it('should return correct permissions based on user object', function () {
            var security = Ext.create('MobileUI.core.security.SecurityService');
            var session = {
                getTenantName: function () {
                    return 'tenant';
                },
                getPermissions: function () {
                    return {
                        permissions: {
                            'Ba:na.na.*': {'READ': ['1', '2', '3', 'tenant']},
                            'Cu:cum.ber.*': {'CREATE': ['1', '2', '3']},
                            'Nut:*': {'EXECUTE': ['1', '2', '3', 'tenant']},
                            'On:ni.on.*': {'WRITE': ['1', '2', '3', 'tenant'], 'READ': ['1', '2', '3', 'tenant']},
                            'Сorn:*': {'READ': ['SuperUser:*']}
                        }
                    }
                }
            };
            expect(security.checkUserPermission(session, 'READ', 'Ba:na.na.mama')).toBeTruthy();
            expect(security.checkUserPermission(session, 'WRITE', 'Ba:na.na.mama')).toBeFalsy();
            expect(security.checkUserPermission(session, 'WRITE', 'Ba:na.*')).toBeFalsy();
            expect(security.checkUserPermission(session, 'CREATE', 'Cu:cum.ber.1')).toBeFalsy();
            expect(security.checkUserPermission(session, 'EXECUTE', 'Nut:tel.lla')).toBeTruthy();
            expect(security.checkUserPermission(session, ['WRITE', 'READ'], 'On:ni.on')).toBeTruthy();
            expect(security.checkUserPermission(session, 'READ', 'Сorn:1.2.3')).toBeTruthy();
            expect(security.checkUserPermission(session, 'READ', 'Potato:1.2.3')).toBeFalsy();
        });

        it('should work for hierarchies', function () {
            var security = Ext.create('MobileUI.core.security.SecurityService');
            var session = {
                getTenantName: function () {
                    return 'tenant';
                },
                getPermissions: function () {
                    return {
                        permissions: {
                            'MDM:*': {
                                'READ': [
                                    'tenant'
                                ]
                            }
                        }
                    }
                }
            };
            expect(security.checkUserPermission(session, 'READ', 'MDM:ui.config')).toBeTruthy();
        });


        it('should work for complex hierarchies', function () {
            var security = Ext.create('MobileUI.core.security.SecurityService');
            var createSession = function(tenant) {
                return {
                    getTenantName: function () {
                        return tenant;
                    },
                    getPermissions: function () {
                        return {
                            permissions: {
                                'MDM:ui.*': {
                                    'CREATE': [
                                        'tenant'
                                    ]
                                },
                                'MDM:api.*': {
                                    'READ': [
                                        'tenant'
                                    ]
                                },
                                'MDM:dtss.*': {
                                    'READ': [
                                        'SuperUser:*'
                                    ],
                                    'CREATE': [
                                        'SuperUser:allTenants'
                                    ]
                                }
                            }
                        }
                    }
                }};
            expect(security.checkUserPermission(createSession('tenant'), 'CREATE', 'MDM:api.test.config')).toBeFalsy();
            expect(security.checkUserPermission(createSession('tenant'), 'CREATE', 'MDM:ui.test.config')).toBeTruthy();
            expect(security.checkUserPermission(createSession('tenant2'), 'CREATE', 'MDM:ui.test.config')).toBeFalsy();
            expect(security.checkUserPermission(createSession('tenant2'), 'READ', 'MDM:dtss.config')).toBeTruthy();
            expect(security.checkUserPermission(createSession('tenant2'), 'CREATE', 'MDM:dtss.config')).toBeTruthy();
        });

    });
});