/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MDataProvider', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
    });
    describe('sendRequest tests', function () {
        it('should send new request in case of throttling error', function (done) {
            var attempt = 0;
            spyOn(Ext.Ajax, 'request').and.callFake(function (params) {
                if (attempt === 0) {
                    attempt++;
                    params.failure.call(params.scope, {
                        status: 503,
                        responseText: '{"severity": "Error", "errorMessage": "quota_exceeded"}'
                    })
                } else {
                    params.success.call(params.scope, {
                        responseText: JSON.stringify({status: 'ok'})
                    })
                }
            });
            spyOn(session,'callMethodWhenTokenAvailable').and.callFake(function(success, failure, args, self) {
                return success.apply(self, args);
            });
            session.sendRequestInternal('/test', [], {
                method: 'POST',
                data: 'test'
            }, function (res) {
                expect(res.status).toBe('ok');
                done();
            }, Ext.emptyFn(),session);

        });

        it('should set ui headers for each request', function (done) {
            spyOn(Ext.Ajax,'request').and.callFake(function(options) {
                expect(options.headers['xxx-client']).toBe('true');
                expect(options.headers['clientSystemId']).toBe('Reltio Mobile UI');
                expect(options.headers['globalId']).toBe('Reltio Mobile UI');

                done();
            });
            session.sendRequestInternal('/test');
        });
    });

});