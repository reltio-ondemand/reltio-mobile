/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MEntity', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
    });
    describe('request entity tests', function () {
        it('should send correct request to API', function (done) {
            session.setAccessToken('accessToken');
            session.setRefreshToken('refreshToken');
            spyOn(session,'sendRequest').and.callFake(function(uri){
                expect(uri).toBe('/{0}?options=sendHidden,addRefAttrUriToCrosswalk');
                done();
            });
            session.requestEntity('123', Ext.emptyFn, Ext.emptyFn, null, false, true);
        });
    });

});