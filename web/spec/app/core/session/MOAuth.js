/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MOAuth', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
    });
    describe('Auth tests', function () {
        it('should send access token only on logout', function (done) {
            session.setAccessToken('accessToken');
            session.setRefreshToken('refreshToken');

            var context = {};

            session.logout(function () {
                expect(session.getAccessToken()).toBe(null);
                expect(session.getRefreshToken()).toBe(null);
                expect(this).toBe(context);
                done();
            }, context);
        });
        it('should fill permissions from user info ', function (done) {
            spyOn(MobileUI.core.Services, 'getTenantName').and.returnValue('tenant');
            spyOn(MobileUI.core.Services, 'asyncRequest').and.callFake(function (uri, success, failure, options, context) {
                if (uri.indexOf('/oauth/checkToken?serviceId=MDM,export,Auth&tenantId=tenant') !== -1){
                    success.call(context, {user:{userPermissions: 'userPermissions'}});
                    expect(session.getPermissions()).toBe('userPermissions');
                    done();
                }
            });
            session.requestUserInfo(true);

        });
    });

});