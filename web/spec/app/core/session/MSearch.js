/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MSearch', function () {

    var searchStringWithSpecialCharacters = 'abc \\ _ + - : > , \' & " xyz';
    var escapedSearchStringWithSpecialCharacters = '\'abc%20%5C%5C%20_%20%2B%20-%20%3A%20%3E%20%2C%20%5C\'%20%26%20%5C%22%20xyz\'';

    describe('typeAheadSearch', function () {

        it('should search only for searchable entity types', function (done) {
            MobileUI.core.Metadata.entityTypes = {
                'searchable': {},
                'nonSearchable': {
                    searchable: false
                }
            };

            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params) {
                expect(params.indexOf('equals(type,\'searchable\')')).toBeGreaterThan(0);
                expect(params.indexOf('equals(type,\'nonSearchable\')')).toBe(-1);
                done();
            });

            spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
                uris: []
            });

            MobileUI.core.session.Session.typeAheadSearch({value: 'test'}, null, null, this);
        });

        it('should escape characters', function (done) {
            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params) {
                var escapedValue = params.substring(params.indexOf(',') + 1, params.indexOf(')'));
                expect(escapedValue).toBe(escapedSearchStringWithSpecialCharacters);
                done();
            });

            spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
                uris: []
            });

            MobileUI.core.session.Session.typeAheadSearch({value: searchStringWithSpecialCharacters}, null, null, this);
        });

    });

    describe('performSearch', function () {

        it('should escape for /entities and /_total requests', function () {
            function checkSearchString(str) {
                var escapedValue = str.substring(str.indexOf(',') + 1, str.indexOf(')'));
                expect(escapedValue).toBe(escapedSearchStringWithSpecialCharacters);
            }

            var spy = spyOn(MobileUI.core.session.Session, 'sendRequest');

            spyOn(MobileUI.core.search.SearchParametersManager, 'getGlobalFilter').and.returnValue({
                uris: []
            });

            MobileUI.core.session.Session.performSearch(searchStringWithSpecialCharacters);

            expect(spy.calls.count()).toBe(2);

            expect(spy.calls.all()[0].args[0]).toBe(MobileUI.core.session.MSearch.URL_SEARCH_PREFIX_PAGING);
            expect(spy.calls.all()[1].args[0]).toBe(MobileUI.core.session.MSearch.URL_SEARCH_TOTAL);

            checkSearchString(spy.calls.all()[0].args[1][0]);
            checkSearchString(spy.calls.all()[1].args[1][0]);
        });
    });
});