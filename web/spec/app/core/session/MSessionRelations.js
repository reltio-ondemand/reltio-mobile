/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MSessionRelations', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
    });
    describe('add relation method tests', function () {
        it('should call success callback with correct context', function(done){
            var res = {
                uri: 'entityUri'
            };
            var self = {};
            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function(url, params, options, success, failure, self){
                success.call(self || MobileUI.core.session.Session, res)
            });
            session.addRelation('startEntityUri', 'endEntityUri', {}, null, null, null, function(){
                expect(this).toBe(self);
                done();
            }, Ext.emptyFn, self);
        });
    });
    describe('request relations method', function () {
        it('should request relations correctly', function(done){
            var max = 1;
            var offset = 1;
            spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function(url){
                expect(url).toBe('/{0}/_connections?limitCreditsConsumption=true');
                done()
            });
            MobileUI.core.session.Session.requestRelationsImmediately([], 'filter', 'entityUri', max, offset);
        });
    });

});