/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MTree', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
    });
    describe('request graph tests', function () {
        it('should send correct request to API for getGraph method', function (done) {
            var limit = 3;
            var deep = 2;
            spyOn(session, 'sendRequest').and.callFake(function (uri,entityUri, params) {
                expect(uri).toBe('/{0}/_hops?deep=2&filterLastLevel=false&limitCreditsConsumption=true&graphTypeURIs=type&max=3');
                expect(entityUri).toBe('123');
                expect(params).toBe(null);
                done();
            });
            session.getGraph('123', 'type', deep, limit, Ext.emptyFn, null, false, true);
        });

        it('should send correct request to API for getFullTree method', function (done) {
            var entityUri = 'uri';
            spyOn(session, 'sendRequest').and.callFake(function (uri, options, params) {
                expect(uri).toBe('/{0}/_tree?graphTypeURIs={1}&limitCreditsConsumption=true&&select=label,secondaryLabel');
                expect(params).toBe(null);
                expect(options[0]).toBe(entityUri);

                done();
            });
            spyOn(session, 'getEntityUri').and.returnValue(entityUri);
            session.getFullTree(['type'], Ext.emptyFn,  Ext.emptyFn, null);
        });
        it('should send correct request to API for requestTree method', function (done) {
            var entityUri = 'uri';
            spyOn(session, 'sendRequest').and.callFake(function (uri, options, params) {
                expect(uri).toBe('/{0}/_tree?graphTypeURIs={1}&limitCreditsConsumption=true&&select=total');
                expect(params).toBe(null);
                expect(options[0]).toBe(entityUri);
                done();
            });
            spyOn(session, 'getEntityUri').and.returnValue(entityUri);
            session.requestTree(['type'], Ext.emptyFn,  Ext.emptyFn, null);
        });
    });

});