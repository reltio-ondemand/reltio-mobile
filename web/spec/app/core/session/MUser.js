/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MUser', function () {
    it('should send correct request to API', function (done) {
        var session = MobileUI.core.session.Session;

        spyOn(MobileUI.core.util.Storage, 'getItem').and.returnValue({
           entity: {
               uri: '[{"environment":"tst-01","tenant":"kono","uri":"entities/j5U298N","url":"https://tst-01.reltio.com/reltio/api/TfTXeTgzX3pwlbf/entities/j5U298N"}]'
           }
        });
        spyOn(MobileUI.core.Services, 'getTenantName').and.returnValue('kono');
        spyOn(MobileUI.core.Services, 'getEnvironmentName').and.returnValue('tst-01');

        spyOn(session, 'sendRequest').and.callFake(function (url) {
            expect(url).toContain('sendHidden');
            done();
        });

        session.getUserProfileEntity(Ext.emptyFn);
    });
});