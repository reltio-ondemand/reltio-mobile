/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.MValidation', function () {
    var session;
    beforeEach(function () {
        session = MobileUI.core.session.Session;
        jasmine.addMatchers(window.JasmineObjectMatchers);
    });
    describe('Validation request for complex data', function () {

        it(' should contain tmp entity as part of the value', function (done) {
            var entity = {
                'original': {
                    'type': 'configuration/entityTypes/HCA',
                    'attributes': {},
                    'uri': 'entities/__temp_EIFoNb'
                },
                'uri': 'entities/__temp_EIFoNb',
                'type': 'configuration/entityTypes/HCA',
                'crosswalks': [],
                'attributes': {},
                'addedAttributes': [
                    {
                        'value': {},
                        'type': 'configuration/entityTypes/HCA/attributes/Address',
                        'attrType': {
                            'label': 'Address',
                            'name': 'Address',
                            'type': 'Reference',
                            'hidden': false,
                            'important': false,
                            'system': false,
                            'searchable': true,
                            'relationshipLabelPattern': '{AddressType}, {Status}, Primary - {PrimaryAddressIndicator}',
                            'attributeOrdering': {
                                'fieldURI': 'configuration/entityTypes/HCA/attributes/Address/attributes/PrimaryAddressIndicator',
                                'orderType': 'DESC',
                                'orderingStrategy': 'FieldBased'
                            },
                            'uri': 'configuration/entityTypes/HCA/attributes/Address',
                            'referencedAttributeURIs': [
                                'configuration/relationTypes/HasAddress/attributes/AddressType',
                                'configuration/relationTypes/HasAddress/attributes/Status',
                                'configuration/relationTypes/HasAddress/attributes/PrimaryAddressIndicator',
                                'configuration/entityTypes/Location/attributes/AddressLine1',
                                'configuration/entityTypes/Location/attributes/AddressLine2',
                                'configuration/entityTypes/Location/attributes/AddressLine3',
                                'configuration/entityTypes/Location/attributes/City',
                                'configuration/entityTypes/Location/attributes/CityTierName',
                                'configuration/entityTypes/Location/attributes/District',
                                'configuration/entityTypes/Location/attributes/Province',
                                'configuration/entityTypes/Location/attributes/Region',
                                'configuration/entityTypes/Location/attributes/PostalCode',
                                'configuration/relationTypes/HasAddress/attributes/InternalPostCode',
                                'configuration/entityTypes/Location/attributes/Country',
                                'configuration/entityTypes/Location/attributes/MiniBrickCode',
                                'configuration/relationTypes/HasAddress/attributes/Communication',
                                'configuration/relationTypes/HasAddress/attributes/AdditionalAttributes',
                                'configuration/relationTypes/HasAddress/attributes/LEGACY_MDM_CUST_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/LEGACY_MDM_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/SubDivisionCode'
                            ],
                            'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                            'relationshipTypeURI': 'configuration/relationTypes/HasAddress',
                            'access': [
                                'UNMERGE',
                                'ACCEPT_CHANGE_REQUEST',
                                'CREATE',
                                'DELETE',
                                'INITIATE_CHANGE_REQUEST',
                                'MERGE',
                                'UPDATE',
                                'READ'
                            ]
                        },
                        'ov': true,
                        'name': 'Address',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'objectURI': 'entities/__temp_BxOZZr'
                        },
                        'refRelation': {
                            'objectURI': 'entities/__temp_SbdxbZ'
                        },
                        'uri': 'entities/__temp_EIFoNb/Address/OjRTlp',
                        'parentUri': 'entities/__temp_EIFoNb',
                        'relationshipLabel': 'Business, , Primary - ',
                        'objectURI': 'entities/__temp_BxOZZr',
                        'relationURI': 'entities/__temp_SbdxbZ',
                        'label': 'te   '
                    }
                ],
                'deletedAttributes': [],
                'editedAttributes': [
                    {
                        'name': 'CountryCode',
                        'type': 'configuration/entityTypes/HCA/attributes/CountryCode',
                        'value': 'DE',
                        'ov': true,
                        'parentUri': 'entities/__temp_EIFoNb',
                        'uri': 'entities/__temp_EIFoNb/attributes/CountryCode/scGR7j',
                        'lookupCode': 'DE',
                        'displayName': 'Germany',
                        'edited': true
                    },
                    {
                        'value': {},
                        'type': 'configuration/entityTypes/HCA/attributes/Address',
                        'attrType': {
                            'label': 'Address',
                            'name': 'Address',
                            'type': 'Reference',
                            'hidden': false,
                            'important': false,
                            'system': false,
                            'searchable': true,
                            'relationshipLabelPattern': '{AddressType}, {Status}, Primary - {PrimaryAddressIndicator}',
                            'attributeOrdering': {
                                'fieldURI': 'configuration/entityTypes/HCA/attributes/Address/attributes/PrimaryAddressIndicator',
                                'orderType': 'DESC',
                                'orderingStrategy': 'FieldBased'
                            },
                            'uri': 'configuration/entityTypes/HCA/attributes/Address',
                            'referencedAttributeURIs': [
                                'configuration/relationTypes/HasAddress/attributes/AddressType',
                                'configuration/relationTypes/HasAddress/attributes/Status',
                                'configuration/relationTypes/HasAddress/attributes/PrimaryAddressIndicator',
                                'configuration/entityTypes/Location/attributes/AddressLine1',
                                'configuration/entityTypes/Location/attributes/AddressLine2',
                                'configuration/entityTypes/Location/attributes/AddressLine3',
                                'configuration/entityTypes/Location/attributes/City',
                                'configuration/entityTypes/Location/attributes/CityTierName',
                                'configuration/entityTypes/Location/attributes/District',
                                'configuration/entityTypes/Location/attributes/Province',
                                'configuration/entityTypes/Location/attributes/Region',
                                'configuration/entityTypes/Location/attributes/PostalCode',
                                'configuration/relationTypes/HasAddress/attributes/InternalPostCode',
                                'configuration/entityTypes/Location/attributes/Country',
                                'configuration/entityTypes/Location/attributes/MiniBrickCode',
                                'configuration/relationTypes/HasAddress/attributes/Communication',
                                'configuration/relationTypes/HasAddress/attributes/AdditionalAttributes',
                                'configuration/relationTypes/HasAddress/attributes/LEGACY_MDM_CUST_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/LEGACY_MDM_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/SubDivisionCode'
                            ],
                            'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                            'relationshipTypeURI': 'configuration/relationTypes/HasAddress',
                            'access': [
                                'UNMERGE',
                                'ACCEPT_CHANGE_REQUEST',
                                'CREATE',
                                'DELETE',
                                'INITIATE_CHANGE_REQUEST',
                                'MERGE',
                                'UPDATE',
                                'READ'
                            ]
                        },
                        'ov': true,
                        'name': 'Address',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'objectURI': 'entities/__temp_BxOZZr'
                        },
                        'refRelation': {
                            'objectURI': 'entities/__temp_SbdxbZ'
                        },
                        'uri': 'entities/__temp_EIFoNb/Address/OjRTlp',
                        'parentUri': 'entities/__temp_EIFoNb',
                        'relationshipLabel': 'Business, , Primary - ',
                        'objectURI': 'entities/__temp_BxOZZr',
                        'relationURI': 'entities/__temp_SbdxbZ',
                        'label': 'te   '
                    }
                ],
                'suggestedMode': false
            };
            var temporaryEntity = {
                'original': {
                    'type': 'configuration/entityTypes/Location',
                    'attributes': [],
                    'uri': 'entities/__temp_BxOZZr'
                },
                'uri': 'entities/__temp_BxOZZr',
                'type': 'configuration/entityTypes/Location',
                'crosswalks': [],
                'attributes': [
                    {
                        'value': 'te',
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'attrType': {
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'system': false,
                            'required': true,
                            'attributeOrdering': {
                                'orderType': 'ASC',
                                'orderingStrategy': 'LUD'
                            },
                            'uri': 'configuration/entityTypes/Location/attributes/AddressLine1',
                            'access': [
                                'UNMERGE',
                                'ACCEPT_CHANGE_REQUEST',
                                'CREATE',
                                'DELETE',
                                'INITIATE_CHANGE_REQUEST',
                                'MERGE',
                                'UPDATE',
                                'READ'
                            ]
                        },
                        'ov': true,
                        'name': 'AddressLine1',
                        'uri': 'entities/__temp_BxOZZr/AddressLine1/LIAMpk',
                        'parentUri': 'entities/__temp_BxOZZr'
                    }
                ],
                'addedAttributes': [
                    {
                        'value': 'te',
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'attrType': {
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'system': false,
                            'required': true,
                            'attributeOrdering': {
                                'orderType': 'ASC',
                                'orderingStrategy': 'LUD'
                            },
                            'uri': 'configuration/entityTypes/Location/attributes/AddressLine1',
                            'access': [
                                'UNMERGE',
                                'ACCEPT_CHANGE_REQUEST',
                                'CREATE',
                                'DELETE',
                                'INITIATE_CHANGE_REQUEST',
                                'MERGE',
                                'UPDATE',
                                'READ'
                            ]
                        },
                        'ov': true,
                        'name': 'AddressLine1',
                        'uri': 'entities/__temp_BxOZZr/AddressLine1/LIAMpk',
                        'parentUri': 'entities/__temp_BxOZZr'
                    }
                ],
                'deletedAttributes': [],
                'editedAttributes': [
                    {
                        'value': 'te',
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'attrType': {
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'system': false,
                            'required': true,
                            'attributeOrdering': {
                                'orderType': 'ASC',
                                'orderingStrategy': 'LUD'
                            },
                            'uri': 'configuration/entityTypes/Location/attributes/AddressLine1',
                            'access': [
                                'UNMERGE',
                                'ACCEPT_CHANGE_REQUEST',
                                'CREATE',
                                'DELETE',
                                'INITIATE_CHANGE_REQUEST',
                                'MERGE',
                                'UPDATE',
                                'READ'
                            ]
                        },
                        'ov': true,
                        'name': 'AddressLine1',
                        'uri': 'entities/__temp_BxOZZr/AddressLine1/LIAMpk',
                        'parentUri': 'entities/__temp_BxOZZr'
                    }
                ],
                'suggestedMode': false
            };
            spyOn(MobileUI.core.entity.TemporaryEntity,'isTemp').and.returnValue(true);
            spyOn(MobileUI.core.entity.TemporaryEntity,'getEntity').and.returnValue(temporaryEntity);
            session.prepareForValidation(entity).then(function(result){
                var value = result.attributes.Address[0].value;

                expect(Object.keys(value).length).toBe(1);
                expect(value.AddressLine1[0].value).toBe('te');
                done();
            });
        });

        it(' should invoke modify in validation mode always', function (done) {
            var entity = {
                'original': {
                    'type': 'configuration/entityTypes/HCA',
                    'attributes': {},
                    'uri': 'entities/__temp_EIFoNb'
                },
                'uri': 'entities/__temp_EIFoNb',
                'type': 'configuration/entityTypes/HCA',
                'crosswalks': [],
                'attributes': {},
                'addedAttributes': [
                    {
                        'value': {},
                        'type': 'configuration/entityTypes/HCA/attributes/Address',
                        'attrType': {
                            'label': 'Address',
                            'name': 'Address',
                            'type': 'Reference',
                            'uri': 'configuration/entityTypes/HCA/attributes/Address',
                            'referencedAttributeURIs': [
                                'configuration/relationTypes/HasAddress/attributes/AddressType',
                                'configuration/relationTypes/HasAddress/attributes/Status',
                                'configuration/relationTypes/HasAddress/attributes/PrimaryAddressIndicator',
                                'configuration/entityTypes/Location/attributes/AddressLine1',
                                'configuration/entityTypes/Location/attributes/AddressLine2',
                                'configuration/entityTypes/Location/attributes/AddressLine3',
                                'configuration/entityTypes/Location/attributes/City',
                                'configuration/entityTypes/Location/attributes/CityTierName',
                                'configuration/entityTypes/Location/attributes/District',
                                'configuration/entityTypes/Location/attributes/Province',
                                'configuration/entityTypes/Location/attributes/Region',
                                'configuration/entityTypes/Location/attributes/PostalCode',
                                'configuration/relationTypes/HasAddress/attributes/InternalPostCode',
                                'configuration/entityTypes/Location/attributes/Country',
                                'configuration/entityTypes/Location/attributes/MiniBrickCode',
                                'configuration/relationTypes/HasAddress/attributes/Communication',
                                'configuration/relationTypes/HasAddress/attributes/AdditionalAttributes',
                                'configuration/relationTypes/HasAddress/attributes/LEGACY_MDM_CUST_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/LEGACY_MDM_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/SubDivisionCode'
                            ]

                        },
                        'name': 'Address',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'objectURI': 'entities/__temp_BxOZZr'
                        },
                        'refRelation': {
                            'objectURI': 'entities/__temp_SbdxbZ'
                        },
                        'uri': 'entities/__temp_EIFoNb/Address/OjRTlp',
                        'parentUri': 'entities/__temp_EIFoNb',
                        'objectURI': 'entities/__temp_BxOZZr',
                        'relationURI': 'entities/__temp_SbdxbZ'
                    }
                ],
                'deletedAttributes': [],
                'editedAttributes': [
                    {
                        'name': 'CountryCode',
                        'type': 'configuration/entityTypes/HCA/attributes/CountryCode',
                        'value': 'DE',
                        'parentUri': 'entities/__temp_EIFoNb',
                        'uri': 'entities/__temp_EIFoNb/attributes/CountryCode/scGR7j',
                        'lookupCode': 'DE'
                    },
                    {
                        'value': {},
                        'type': 'configuration/entityTypes/HCA/attributes/Address',
                        'attrType': {
                            'label': 'Address',
                            'name': 'Address',
                            'type': 'Reference',
                            'uri': 'configuration/entityTypes/HCA/attributes/Address',
                            'referencedAttributeURIs': [
                                'configuration/relationTypes/HasAddress/attributes/AddressType',
                                'configuration/relationTypes/HasAddress/attributes/Status',
                                'configuration/relationTypes/HasAddress/attributes/PrimaryAddressIndicator',
                                'configuration/entityTypes/Location/attributes/AddressLine1',
                                'configuration/entityTypes/Location/attributes/AddressLine2',
                                'configuration/entityTypes/Location/attributes/AddressLine3',
                                'configuration/entityTypes/Location/attributes/City',
                                'configuration/entityTypes/Location/attributes/CityTierName',
                                'configuration/entityTypes/Location/attributes/District',
                                'configuration/entityTypes/Location/attributes/Province',
                                'configuration/entityTypes/Location/attributes/Region',
                                'configuration/entityTypes/Location/attributes/PostalCode',
                                'configuration/relationTypes/HasAddress/attributes/InternalPostCode',
                                'configuration/entityTypes/Location/attributes/Country',
                                'configuration/entityTypes/Location/attributes/MiniBrickCode',
                                'configuration/relationTypes/HasAddress/attributes/Communication',
                                'configuration/relationTypes/HasAddress/attributes/AdditionalAttributes',
                                'configuration/relationTypes/HasAddress/attributes/LEGACY_MDM_CUST_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/LEGACY_MDM_ADDR_ROWID',
                                'configuration/entityTypes/Location/attributes/SubDivisionCode'
                            ]

                        },
                        'name': 'Address',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'objectURI': 'entities/__temp_BxOZZr'
                        },
                        'refRelation': {
                            'objectURI': 'entities/__temp_SbdxbZ'
                        },
                        'uri': 'entities/__temp_EIFoNb/Address/OjRTlp',
                        'parentUri': 'entities/__temp_EIFoNb',
                        'objectURI': 'entities/__temp_BxOZZr',
                        'relationURI': 'entities/__temp_SbdxbZ'
                    }
                ],
                'suggestedMode': false
            };
            var temporaryEntity = {
                'original': {
                    'type': 'configuration/entityTypes/Location',
                    'attributes': [],
                    'uri': 'entities/__temp_BxOZZr'
                },
                'uri': 'entities/__temp_BxOZZr',
                'type': 'configuration/entityTypes/Location',
                'crosswalks': [],
                'attributes': [
                    {
                        'value': 'te',
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'attrType': {
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'type': 'String',
                            'uri': 'configuration/entityTypes/Location/attributes/AddressLine1'
                        },
                        'name': 'AddressLine1',
                        'uri': 'entities/__temp_BxOZZr/AddressLine1/LIAMpk',
                        'parentUri': 'entities/__temp_BxOZZr'
                    }
                ],
                'addedAttributes': [
                    {
                        'value': 'te',
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'attrType': {
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'type': 'String',
                            'uri': 'configuration/entityTypes/Location/attributes/AddressLine1'
                        },
                        'name': 'AddressLine1',
                        'uri': 'entities/__temp_BxOZZr/AddressLine1/LIAMpk',
                        'parentUri': 'entities/__temp_BxOZZr'
                    }
                ],
                'deletedAttributes': [],
                'editedAttributes': [
                ],
                'suggestedMode': false
            };
            spyOn(MobileUI.core.entity.TemporaryEntity,'isTemp').and.returnValue(true);
            spyOn(MobileUI.core.entity.TemporaryEntity,'getEntity').and.returnValue(temporaryEntity);
            spyOn(MobileUI.controller.MSuggestEdit,'modifyEntity');
            session.prepareForValidation(entity).then(function(){
                expect(MobileUI.controller.MSuggestEdit.modifyEntity.calls.mostRecent().args[2]).toBeTruthy();
                done();
            })
        });

    });
    describe('Validation entity test', function () {

        it(' shouldn\'t change the original entity', function () {
            var entity = {
                'original': {
                    'type': 'configuration/entityTypes/HCA',
                    'attributes': {},
                    'uri': 'entities/__temp_EIFoNb'
                },
                'uri': 'entities/__temp_EIFoNb'
            };
            spyOn(session, 'sendValidationRequest').and.returnValue({then:Ext.emptyFn});
            session.validateEntity(entity);
            expect(session.sendValidationRequest).toHaveBeenCalled();
            expect(entity.uri).toBe('entities/__temp_EIFoNb');
        });

        it(' should change temp uris in sent entity', function () {
            var entity = {
                'original': {
                    'type': 'configuration/entityTypes/HCA',
                    'attributes': {},
                    'uri': 'entities/__temp_EIFoNb'
                },
                'uri': 'entities/__temp_EIFoNb'
            };
            spyOn(session, 'sendValidationRequest').and.returnValue({then:Ext.emptyFn});
            session.validateEntity(entity);
            entity = session.sendValidationRequest.calls.mostRecent().args[2].data;
            expect(entity.original.uri.indexOf('__temp_')).toEqual(-1);
            expect(entity.uri.indexOf('__temp_')).toEqual(-1);
        });
    });

    describe('clean attributes tests', function(){
        var attributes = {
            simpleReqLookup: [{
                value: 'TheOne',
                uri: 'uri1',
                type: 'type1'
            }],
            simpleLookup: [{
                value: '',
                uri: 'uri2',
                type: 'type2'
            }],
            Address: [{
                uri: 'referencedUri',
                refEntity: [{
                    type: 'refEntityType',
                    objectURI: 'entities/uri'
                }],
                refRelation: [{
                    type: 'relationType',
                    objectURI: 'relations/uri'
                }]
            }],
            Zip: [{
                value: {
                    simpleLookup: [{
                        value: '',
                        uri: 'uri3',
                        type: 'type4'
                    }],
                    simpleReqLookup: [{
                        value: 'TheOne',
                        uri: 'uri5',
                        type: 'type5'
                    }, {
                        value: '',
                        uri: 'uri6',
                        type: 'type6'
                    }]
                },
                uri: 'zipuri',
                type: 'ziptype'
            }]
        };

        var expected = {
            simpleReqLookup: [{
                value: 'TheOne'
            }],
            Address: [{
                refEntity: {
                    type: 'refEntityType',
                    objectURI: 'entities/uri'
                },
                refRelation: {
                    type: 'relationType',
                    objectURI: 'relations/uri'
                }
            }],
            Zip: [{
                value: {
                    simpleReqLookup: [{
                        value: 'TheOne'
                    }]
                }
            }]
        };

        it('should remove all empty attributes from entity', function(){

            session.cleanAttributes(attributes);
            expect(attributes).toContainKeys(expected);
        })

    });

    describe('create validation response function', function () {

        it(' should use calculate path for objectParentUri if it is defined', function () {
            var attrType = {
                type: 'CountryCode',
                label: 'CountryCode'
            };
            var entity = {
                    'type': 'configuration/entityTypes/HCP',
                    'attributes': {
                        CountryCode: [{
                            type: 'configuration/entityTypes/HCP/attributes/CountryCode',
                            ov: true,
                            value: 'Italy',
                            lookupCode: 'IT',
                            lookupRawValue: 'IT',
                            uri: 'entities/uri$$EIFoNb/attributes/CountryCode/tcSJi5wd'
                        }]
                    },
                    'uri': 'entities/uri$$EIFoNb'

            };
            var response = {
                errorType: 'INCORRECT',
                message: 'Attribute of type configuration/entityTypes/HCP/attributes/AZCustomerID must have at least 1 ov value(s), but it has 0',
                objectParentUri: 'entities/1Tempo4raryEIFoNb/attributes/CountryCode/tcSJi5wd' ,
                objectTypeUri: 'configuration/entityTypes/HCP/attributes/CountryCode',
                objectUri: null
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByUri').and.returnValue(attrType);
            var func = session.createValidationResponseFunction(entity);
            var result = func(response);
            expect(result.path.length).toEqual(1);
            expect(result.path[0].attrName).toEqual('CountryCode');
        });

        it(' should add objectParentUri and message to response for missed', function () {
            var attrType = {
                type: 'CountryCode',
                label: 'CountryCode'
            };
            var entity = {
                'type': 'configuration/entityTypes/HCP',
                'attributes': {
                    CountryCode: [{
                        type: 'configuration/entityTypes/HCP/attributes/CountryCode',
                        ov: true,
                        value: 'Italy',
                        lookupCode: 'IT',
                        lookupRawValue: 'IT',
                        uri: 'entities/uri$$EIFoNb/attributes/CountryCode/tcSJi5wd'
                    }]
                },
                'uri': 'entities/uri$$EIFoNb'

            };
            var response = {
                errorType: 'MISSED',
                message: 'Attribute of type configuration/entityTypes/HCP/attributes/AZCustomerID must have at least 1 ov value(s), but it has 0',
                objectParentUri: 'entities/1Tempo4raryEIFoNb/attributes/CountryCode/tcSJi5wd' ,
                objectTypeUri: 'configuration/entityTypes/HCP/attributes/CountryCode',
                objectUri: null
            };
            spyOn(MobileUI.core.Metadata, 'findEntityAttributeByUri').and.returnValue(attrType);
            var func = session.createValidationResponseFunction(entity);
            var result = func(response);
            expect(result.objectParentUri).toEqual('entities/uri$$EIFoNb/attributes/CountryCode/tcSJi5wd');
            expect(result.value).toEqual('Attribute of type configuration/entityTypes/HCP/attributes/AZCustomerID must have at least 1 ov value(s), but it has 0');
        });
    });
});