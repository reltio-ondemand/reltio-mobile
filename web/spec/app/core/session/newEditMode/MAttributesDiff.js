/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.newEditMode.MAttributesDiff', function () {
    it('should clean attributes', function () {
        var attributes = {
            simpleReqLookup: [{
                value: 'TheOne',
                uri: 'uri1',
                type: 'type1'
            }],
            simpleLookup: [{
                value: '',
                uri: 'uri2',
                type: 'type2'
            }],
            Address: [{
                uri: 'referencedUri',
                refEntity: {
                    type: 'refEntityType',
                    objectURI: 'entities/uri'
                },
                refRelation: {
                    type: 'relationType',
                    objectURI: 'relations/uri'
                }
            }],
            Zip: [{
                value: {
                    simpleLookup: [{
                        value: '',
                        uri: 'uri3',
                        type: 'type4'
                    }],
                    simpleReqLookup: [{
                        value: 'TheOne',
                        uri: 'uri5',
                        type: 'type5'
                    }, {
                        value: '',
                        uri: 'uri6',
                        type: 'type6'
                    }]
                },
                uri: 'zipuri',
                type: 'ziptype'
            }]
        };

        var expected = {
            simpleReqLookup: [{
                value: 'TheOne'
            }],
            Address: [{
                refEntity: {
                    type: 'refEntityType',
                    objectURI: 'entities/uri'
                },
                refRelation: {
                    type: 'relationType',
                    objectURI: 'relations/uri'
                }
            }],
            Zip: [{
                value: {
                    simpleReqLookup: [{
                        value: 'TheOne'
                    }]
                }
            }]
        };

        MobileUI.core.session.Session.cleanEntityAttributes(attributes);

        expect(attributes).toEqual(expected);
    });
});