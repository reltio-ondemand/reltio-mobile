/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.newEditMode.MCumulativeUpdate', function () {

    beforeEach(function () {
        MobileUI.core.entity.TemporaryEntity.removeAllEntities();
    });

    it('should use the same changeRequestId for all save requests', function (done) {
        var newAttribute = {
            value: {},
            refEntity: {
                objectURI: 'entities/uri$$123',
                type: 'configuration/entityTypes/Location'
            },
            refRelation: {
                crosswalks: [{
                    type: 'configuration/sources/Reltio'
                }]
            }
        };

        var diff = [{
            type: 'INSERT_ATTRIBUTE',
            uri: 'entities/42/attributes/Address',
            newValue: [
                newAttribute
            ]
        }];

        var modifiedEntity = {
            uri: 'entities/myEntity',
            attributes: {
                Address: [
                    newAttribute
                ]
            }
        };

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$123',
            attributes: {
                City: [
                    {
                        value: 'test'
                    }
                ]
            }
        });

        var response = {
            changes: {
                'entities/456': {}
            },
            uri: 'dataChangeRequest/DCR'
        };

        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success) {
            expect(url).toContain('changeRequestId=DCR');
            expect(url).toContain('_update?options=sendHidden,updateAttributeUpdateDates');
            success();
        });
        spyOn(MobileUI.core.session.Session, 'getEntityAttributesDiff');
        spyOn(MobileUI.core.session.Session, 'collectCrosswalksFromAttributes');
        spyOn(MobileUI.core.session.Session, 'compareImages').and.returnValue([]);
        spyOn(MobileUI.core.session.Session, 'applyCrosswalks').and.returnValue(diff);
        spyOn(MobileUI.core.session.Session, 'createEntity').and.returnValue(Promise.resolve(response));

        MobileUI.core.session.Session.saveAllPendingChanges(modifiedEntity).then(function () {
            done();
        });
    });

    it('should save all dependent entities if create new entity', function (done) {
        var temporaryEntity = {
            uri: 'entities/uri$$main',
            attributes: {
                Address: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$123',
                            type: 'configuration/entityTypes/Location'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        };

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$123',
            attributes: {
                City: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$789',
                            type: 'configuration/entityTypes/City'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        });

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$789',
            attributes: {
                Name: [
                    {
                        value: 'test'
                    }
                ]
            }
        });

        var spy = spyOn(MobileUI.core.session.Session, 'createEntity').and.callFake(function (entity) {
            if (spy.calls.count() === 1) {
                return Promise.resolve([{object: {uri: 'entities/non-temporary-city'}}]);
            } else if (spy.calls.count() === 2) {
                expect(entity.attributes.City[0].refEntity.objectURI).toBe('entities/non-temporary-city');
                return Promise.resolve([{object: {uri: 'entities/non-temporary-location'}}]);
            } else if (spy.calls.count() === 3) {
                expect(entity.attributes.Address[0].refEntity.objectURI).toBe('entities/non-temporary-location');
                return Promise.resolve([{object: {uri: 'entities/qwerty'}}]);
            }
        });

        MobileUI.core.session.Session.saveAllPendingChanges(temporaryEntity).then(function () {
            expect(spy.calls.count()).toBe(3);
            done();
        });
    });

    it('should save several dependent entities of one new entity', function (done) {
        var temporaryEntity = {
            uri: 'entities/uri$$main',
            attributes: {
                Address: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$123',
                            type: 'configuration/entityTypes/Location'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ],
                City: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$789',
                            type: 'configuration/entityTypes/City'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        };

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$123',
            attributes: {
                Name: [
                    {
                        value: 'testCity'
                    }
                ]
            }
        });

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$789',
            attributes: {
                Name: [
                    {
                        value: 'test'
                    }
                ]
            }
        });

        var spy = spyOn(MobileUI.core.session.Session, 'createEntity').and.callFake(function () {
            if (spy.calls.count() === 1) {
                return Promise.resolve([{object: {uri: 'entities/non-temporary-city'}}]);
            } else if (spy.calls.count() === 2) {
                return Promise.resolve([{object: {uri: 'entities/non-temporary-location'}}]);
            } else if (spy.calls.count() === 3) {
                return Promise.resolve([{object: {uri: 'entities/qwerty'}}]);
            }
        });

        MobileUI.core.session.Session.saveAllPendingChanges(temporaryEntity).then(function () {
            expect(spy.calls.count()).toBe(3);
            done();
        });
    });

    it('should not send temporary entity uri', function (done) {
        var entity = {
            uri: 'entities/uri$$123',
            attributes: {
                Name: [
                    {
                        value: 'test'
                    }
                ]
            }
        };

        spyOn(MobileUI.core.session.Session, 'createEntity').and.callFake(function (entity) {
            expect(entity.uri).toBeUndefined();
            return Promise.resolve([{object: {uri: 'entities/non-temporary-city'}}]);
        });

        MobileUI.core.session.Session.saveAllPendingChanges(entity).then(done);
    });

    it('should set entity uri from DCR correctly', function (done) {
        var hcp = {
            uri: 'entities/uri$$main',
            attributes: {
                Affiliations: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$1',
                            type: 'configuration/entityTypes/HCA'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        };

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$1',
            attributes: {
                Address: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$2',
                            type: 'configuration/entityTypes/Location'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        });

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$2',
            attributes: {}
        });

        var spy = spyOn(MobileUI.core.session.Session, 'createEntity').and.callFake(function (data) {
            if (spy.calls.count() === 1) {
                return Promise.resolve({
                    changes: {
                        locationUri: {}
                    },
                    uri: 'changeRequests/123'
                });
            } else if (spy.calls.count() === 2) {
                expect(data.attributes.Address[0].refEntity.objectURI).toBe('locationUri');
                return Promise.resolve({
                    changes: {
                        locationUri: {},
                        hcaUri: {}
                    }
                });
            } else if (spy.calls.count() === 3) {
                expect(data.attributes.Affiliations[0].refEntity.objectURI).toBe('hcaUri');
                done();
            }
        });

        MobileUI.core.session.Session.saveAllPendingChanges(hcp);
    });

    it('should use changeRequestId from first saved entity for all requests', function (done) {
        var firstAddress = {
            value: {},
            refEntity: {
                objectURI: 'entities/uri$$1',
                type: 'configuration/entityTypes/Location'
            },
            refRelation: {
                crosswalks: [{
                    type: 'configuration/sources/Reltio'
                }]
            }
        };
        var secondAddress = {
            value: {},
            refEntity: {
                objectURI: 'entities/uri$$2',
                type: 'configuration/entityTypes/Location'
            },
            refRelation: {
                crosswalks: [{
                    type: 'configuration/sources/Reltio'
                }]
            }
        };

        var diff = [{
            type: 'INSERT_ATTRIBUTE',
            uri: 'entities/42/attributes/Address',
            newValue: [
                firstAddress,
                secondAddress
            ]
        }];

        var modifiedEntity = {
            uri: 'entities/myEntity',
            attributes: {
                Address: [
                    firstAddress,
                    secondAddress
                ]
            }
        };

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$1',
            attributes: {
                City: [
                    {
                        value: '1'
                    }
                ]
            }
        });

        MobileUI.core.entity.TemporaryEntity.addEntity({
            uri: 'entities/uri$$2',
            attributes: {
                City: [
                    {
                        value: '2'
                    }
                ]
            }
        });

        spyOn(MobileUI.core.session.Session, 'getEntityAttributesDiff');
        spyOn(MobileUI.core.session.Session, 'collectCrosswalksFromAttributes');
        spyOn(MobileUI.core.session.Session, 'compareImages').and.returnValue([]);
        spyOn(MobileUI.core.session.Session, 'applyCrosswalks').and.returnValue(diff);
        spyOn(MobileUI.core.session.Session, 'createEntity').and.callFake(function (entity, changeRequestId) {
            if (entity.attributes.City[0].value === '1') {
                expect(changeRequestId).toBeNull();
                return Promise.resolve({
                    changes: {
                        'entities/1': {}
                    },
                    uri: 'dataChangeRequest/DCR'
                });
            } else if (entity.attributes.City[0].value === '2') {
                expect(changeRequestId).toBe('DCR');
                return Promise.resolve({
                    changes: {
                        'entities/1': {},
                        'entities/2': {}
                    },
                    uri: 'dataChangeRequest/DCR'
                });
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options) {
            expect(options.data[0].newValue[0].refEntity.objectURI).toBe('entities/1');
            expect(options.data[0].newValue[1].refEntity.objectURI).toBe('entities/2');
            expect(url).toContain('changeRequestId=DCR');
            done();
        });

        MobileUI.core.session.Session.saveAllPendingChanges(modifiedEntity);
    });
});