/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.newEditMode.MValidation', function () {

    it('should validate required simple attribute', function (done) {
        var entity = {
            attributes: {
                String: [{
                    value: ''
                }]
            }
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                required: true,
                name: 'String'
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            done();
        });
    });

    it('should ignore required attributes which user can\'t add', function (done) {
        var entity = {
            attributes: {
                String: [{
                    value: ''
                }]
            }
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                access: ['READ'],
                required: true,
                name: 'String'
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(0);
            done();
        });
    });

    it('should validate required inside nested attributes', function (done) {
        var entity = {
            attributes: {
                Nested: [{
                    value: {
                        String: [{
                            value: ''
                        }]
                    }
                }]
            }
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                name: 'Nested',
                attributes: [{
                    required: true,
                    name: 'String'
                }]
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            done();
        });
    });

    it('should return error for required attribute with space sybmol', function (done) {
        var entity = {
            attributes: {
                String: [{
                    value: '   '
                }]
            }
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                required: true,
                name: 'String'
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            done();
        });
    });
    it('should add parent uri in response', function (done) {
        var entity = {
            attributes: {
                String: [{
                    value: '   '
                }]
            },
            uri: 'test'
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                required: true,
                name: 'String'
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            expect(errors[0].objectParentUri).toBe('test');
            done()
        });
    });

    it('return correct entity for DL validation', function () {
        var temporaryEntity = {
            uri: 'entities/uri$$main',
            attributes: {
                Address: [
                    {
                        value: {},
                        refEntity: {
                            objectURI: 'entities/uri$$123',
                            type: 'configuration/entityTypes/Location'
                        },
                        refRelation: {
                            crosswalks: [{
                                type: 'configuration/sources/Reltio'
                            }]
                        }
                    }
                ]
            }
        };

        spyOn(MobileUI.core.entity.TemporaryEntity, 'getEntity').and.returnValue({
            uri: 'entities/uri$$123',
            attributes: {
                City: [
                    {
                        value: 'test'
                    }
                ]
            }
        });

        var entityForDL = MobileUI.core.session.Session.getEntityForDL(temporaryEntity);
        expect(entityForDL.attributes.Address[0].value.City[0].value).toBe('test')
    });

    it('shouldn\'t allow to save empty nested', function (done) {
        var entity = {
            attributes: {
                Nested: [{
                    value: {
                        String: [{
                            value: ''
                        }]
                    }
                }]
            }
        };

        spyOn(MobileUI.core.Metadata, 'getEntityType').and.returnValue({
            attributes: [{
                name: 'Nested',
                required: true,
                attributes: [{
                    name: 'String'
                }]
            }]
        });

        MobileUI.core.session.Session.validateRequired(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            done();
        });
    });

    it('shouldn\'t allow to save empty entity', function (done) {
        var entity = {
            attributes: {
                String: [{
                    value: ''
                }]
            }
        };

        MobileUI.core.session.Session.validateEmptyEntity(entity).then(function (errors) {
            expect(errors.length).toBe(1);
            done();
        });
    });

    it('should validate web urls', function () {
        var attributes = {
            WebUrl: [{
                value: '',
                uri: 'empty'
            },
                {
                    value: 'http://reltio.com',
                    uri: 'correct'
                },
                {
                    value: '@@reltio@com',
                    uri: 'incorrect'
                }]
        };
        var attrTypes = [{
            name: 'WebUrl',
            type: 'URL'
        }];
        var errors = MobileUI.core.session.Session.validateAttributes(attributes, attrTypes, 'parentUri');
        expect(errors.length).toBe(1);
        expect(errors[0].objectUri).toBe('incorrect');
    });

    it('should validate int', function () {
        var attributes = {
            Int: [{
                value: '9223372036854775808',
                uri: '1'
            }]
        };
        var attrTypes = [{
            name: 'Int',
            type: 'Int'
        }];

        var errors = MobileUI.core.session.Session.validateAttributes(attributes, attrTypes, 'parentUri');
        expect(errors.length).toBe(1);
    });
});