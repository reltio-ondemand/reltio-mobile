/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.session.newEditMode.MValidationService', function () {
    var stubConfigurationManager = function(config){
          spyOn(MobileUI.core.Services, 'getConfigurationManager').and.returnValue({
              getExtensionById: function(id){
                  if (id === 'com.reltio.plugins.ui.AllowDataPermissionValidation'){
                      return config;
                  }
              }
          })
    };

    it('should validate entity with cardinality', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        var entityType = {
            cardinality: true
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });

        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function () {
            // sendRequest should be called for entityType with cardinality
            done();
        });

        MobileUI.core.session.Session.sendValidationRequestWithCheck('url', [], null, entityType);
    });

    it('should use temporary entity for validation response generation in case of new entity creation', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success, failure, self) {
            success.call(self, [{type: 'INCORRECT'}]);
        });

        var temporaryEntity = {
                uri: 'entities/uri$$123'
            },
            entityType = {
                cardinality: true
            };

        spyOn(MobileUI.core.session.Session, 'createValidationResponseFunction').and.callFake(function (entity) {
            expect(entity.uri).toBe('entities/uri$$123');
            done();
            return Ext.emptyFn;
        });

        MobileUI.core.session.Session.validateNewEntity(temporaryEntity, entityType);
    });

    it('should use modified entity for validation response generation in case of editing existing entity', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (url, params, options, success, failure, self) {
            success.call(self, [{type: 'INCORRECT'}]);
        });

        var modifiedEntity = {
                uri: 'entities/123'
            },
            entityType = {
                cardinality: true
            };

        spyOn(MobileUI.core.session.Session, 'getModifiedEntity').and.returnValue(modifiedEntity);
        spyOn(MobileUI.core.session.Session, 'getAttributesDiff').and.returnValue([]);

        spyOn(MobileUI.core.session.Session, 'createValidationResponseFunction').and.callFake(function (entity) {
            expect(entity.uri).toBe('entities/123');
            done();
            return Ext.emptyFn;
        });

        MobileUI.core.session.Session.validateAttributesDiff(modifiedEntity.uri, entityType);
    });

    it('should modify entity uri to validation service format', function () {
        var entity = {
            uri: 'entities/uri$$123123'
        };
        stubConfigurationManager();
        var entityForValidation = MobileUI.core.session.Session.getEntityForValidation(entity);
        expect(entityForValidation.uri).toBe('entities/1Tempo4rary123123');
    });

    it('should send validation request if dependent lookups enabled', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });

        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function () {
            done();
        });

        MobileUI.core.session.Session.sendValidationRequestWithCheck('url', [], null, {});
    });

    it('should use correct urls in case of cumulative update', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        var entityType = {
            cardinality: true,
            access: ['CREATE', 'READ']
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'getAttributesDiff');
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity/{1}?options=preserveURIs,updateAttributeUpdateDates,validateDataPermissions');
            done();
        });

        MobileUI.core.session.Session.validateAttributesDiff('url',  entityType);
    });
    it('should use correct urls in case of override', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        var entityType = {
            cardinality: true,
            access: ['CREATE', 'READ']
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity?options=preserveURIs,validateDataPermissions');
            done();
        });

        MobileUI.core.session.Session.validateNewEntity({uri:'url'},  entityType);
    });

    it('should use correct urls in case of override and disabled setting', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager({isAllow:false});
        var entityType = {
            cardinality: true,
            access: ['CREATE', 'READ']
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity?options=preserveURIs');
            done();
        });

        MobileUI.core.session.Session.validateNewEntity({uri:'url'},  entityType);
    });

    it('should use correct urls in case of cumulative update and disabled setting', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        var entityType = {
            cardinality: true,
            access: ['CREATE', 'READ']
        };
        stubConfigurationManager({isAllow:false});
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'getAttributesDiff');
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity/{1}?options=preserveURIs,updateAttributeUpdateDates');
            done();
        });

        MobileUI.core.session.Session.validateAttributesDiff('url',  entityType);
    });

    it('should use correct urls in case of cumulative update and initiate change request', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        var entityType = {
            cardinality: true,
            access: ['INITIATE_CHANGE_REQUEST', 'READ']
        };
        stubConfigurationManager();
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'getAttributesDiff');
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity/{1}?options=preserveURIs,updateAttributeUpdateDates');
            done();
        });

        MobileUI.core.session.Session.validateAttributesDiff('url',  entityType);
    });

    it('should use correct urls in case of override and initiate change request ', function (done) {
        spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
            validationServicePath: 'path'
        });
        stubConfigurationManager();
        var entityType = {
            cardinality: true,
            access: ['INITIATE_CHANGE_REQUEST', 'READ']
        };
        spyOn(MobileUI.core.session.Session, 'getLookupsManager').and.returnValue({
            getCurrentLookupsHierarchy: function () {
                return {getInfo: Ext.emptyFn}
            }
        });
        spyOn(MobileUI.core.session.Session, 'sendRequest').and.callFake(function (uri) {
            expect(uri).toBe('path/validate/{0}/entity?options=preserveURIs');
            done();
        });

        MobileUI.core.session.Session.validateNewEntity({uri:'url'},  entityType);
    });
});