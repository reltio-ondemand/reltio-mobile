/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.util.HashUtil', function ()
{
    describe('stateFromHash method', function ()
    {
        var HashUtils;
        beforeEach(function ()
        {
            HashUtils = MobileUI.core.util.HashUtil;
            jasmine.addMatchers(window.JasmineObjectMatchers);
        });
        it('should be a function', function ()
        {
            expect(typeof HashUtils.stateFromHash).toEqual('function');
        });

        it('should return an object', function ()
        {
            expect(typeof HashUtils.stateFromHash()).toEqual('object');
        });
        it('should process full hash string correctly', function ()
        {
            expect(HashUtils.stateFromHash('p~com.reltio.plugins.entity.HCPPerspective_w~29y_e~entities%2F1Pg1pIeI')).toContainValues({
                p: 'com.reltio.plugins.entity.HCPPerspective',
                w: '29y',
                e: 'entities%2F1Pg1pIeI'
            });
        });
        it('should process shot hash string correctly', function ()
        {
            expect(HashUtils.stateFromHash('p~com.reltio.plugins.entity.HCPPerspective')).toContainValues({
                p: 'com.reltio.plugins.entity.HCPPerspective'
            });
        });
        it('should process hash with / correctly', function ()
        {
            expect(HashUtils.stateFromHash('p~com.reltio.plugins.entity.$HCPPerspective')).toContainValues({
                p: 'com.reltio.plugins.entity./HCPPerspective'
            });
        });
    });

    describe('stateToHash method', function ()
    {
        var HashUtils;
        beforeEach(function ()
        {
            HashUtils = MobileUI.core.util.HashUtil;
        });
        it('should be a function', function ()
        {
            expect(typeof HashUtils.stateToHash).toEqual('function');
        });

        it('should return a string', function ()
        {
            expect(typeof HashUtils.stateToHash()).toEqual('string');
        });
        it('should return empty string if the parameter is null', function ()
        {
            expect(HashUtils.stateToHash(null)).toEqual('');
        });
        it('should process hash correctly', function ()
        {
            expect(HashUtils.stateToHash({p: 'com.reltio.plugins.entity.HCPPerspective'}))
                .toEqual('p~com.reltio.plugins.entity.HCPPerspective');
        });

        it('should process hash with arrays values correctly', function ()
        {
            expect(HashUtils.stateToHash({p: ['com.reltio.plugins.entity.HCPPerspective', 'com.reltio.plugins.entity.HCOPerspective']}))
                .toEqual('p~com.reltio.plugins.entity.HCPPerspective~com.reltio.plugins.entity.HCOPerspective');
        });

        it('should process complex hash correctly', function ()
        {
            expect(HashUtils.stateToHash({
                p: 'com.reltio.plugins.entity.HCPPerspective',
                w: '29y',
                e: 'entities%2F1Pg1pIeI'
            })).toEqual('p~com.reltio.plugins.entity.HCPPerspective_w~29y_e~entities%2F1Pg1pIeI');
        });
    });
});