/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.util.Util', function () {
    var Util;
    beforeEach(function (done) {
        Util = MobileUI.core.util.Util;
        jasmine.addMatchers(window.JasmineObjectMatchers);
        done();
    });

    describe('isLinkValid method', function () {
        it('Should be defined', function () {
            expect(Util.isLinkValid).toBeDefined();
        });
        it('Should return true in case of valid link', function () {
            expect(Util.isLinkValid('http://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('https://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('mailto:ere@reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('www.ere.reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('http://www.ere.reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('ftp://reltio.com')).toBeTruthy();
        });
        it('Should return false in case of invalid link', function () {
            expect(Util.isLinkValid('htt://reltio.com')).toBeFalsy();
            expect(Util.isLinkValid('http//reltio.com')).toBeFalsy();
        });
    });
    describe('convertArrayToObject method verification', function () {
        it('Should be defined', function () {
            expect(Util.convertArrayToObject).toBeDefined();
        });
        it('Should return empty object in case of incorrect arguments', function () {
            expect(Util.convertArrayToObject()).toEqual({});
        });
        it('Should split array by separator', function () {
            var array = ['key1:123', 'key2:12', 'key:value'];
            var result = Util.convertArrayToObject(array, ':');
            expect(result.key1).toEqual('123');
            expect(result.key2).toEqual('12');
            expect(result.key).toEqual('value');
        });
        it('Should split array by default separator in case of no separator', function () {
            var array = ['key1=123', 'key2=12'];
            var result = Util.convertArrayToObject(array);
            expect(result.key1).toEqual('123');
            expect(result.key2).toEqual('12');
        });
    });

    describe('preloadImage method verification', function () {
        it('Should be defined', function () {
            expect(Util.preloadImage).toBeDefined();
        });
        it('Should creates Image and set src from url', function () {
            expect(Util.preloadImage('http://reltio.com').src).toEqual('http://reltio.com/');
        });
    });

    describe('generateUUID method verification', function () {
        it('Should be defined', function () {
            expect(Util.generateUUID).toBeDefined();
        });
        it('Should creates unique id with default length equals 4', function () {
            expect(Util.generateUUID().length).toEqual(4);
        });

        it('Should creates unique id. Size should be taken from the parameter', function () {
            //actually this test should work. But sometimes it could not work
            expect(Util.generateUUID(10).length).toEqual(10);
        });
    });

    describe('getHash method verification', function () {
        it('Should be defined', function () {
            expect(Util.getHash).toBeDefined();
        });
        it('Should return has String based on hash algorithm', function () {
            expect(Util.getHash('aaa')).toEqual('96321');
        });
        it('Hashes for same string should be the same', function () {
            expect(Util.getHash('aaa')).toEqual(Util.getHash('aaa'));
        });
        it('Should return \'0\' in case of empty string', function () {
            expect(Util.getHash()).toEqual('0');
            expect(Util.getHash('')).toEqual('0');
            expect(Util.getHash(null)).toEqual('0');
            expect(Util.getHash(undefined)).toEqual('0');
        });
    });

    describe('toParameterPair method verification', function () {
        it('Should be defined', function () {
            expect(Util.toParameterPair).toBeDefined();
        });
        it('Should convert parameters to query pair', function () {
            var arr = [];
            Util.toParameterPair('key', 'val', arr);
            expect(arr[0]).toEqual('key=val');
        });
        it('Should add result to array', function () {
            var arr = ['aaa'];
            Util.toParameterPair('key key+1', 'val val', arr);
            expect(arr.length).toEqual(2);
        });
        it('Should encode keys and values', function () {
            var arr = [];
            Util.toParameterPair('key key+1', 'val val', arr);
            expect(arr[0]).toEqual('key%20key%2B1=val%20val');
        });
        it('Should replace spaces to plus for post requests', function () {
            var arr = [];
            Util.toParameterPair('key key+1', 'val val', arr, true);
            expect(arr[0]).toEqual('key+key%2B1=val+val');
        });
    });
    describe('toParameter method verification', function () {
        it('Should be defined', function () {
            expect(Util.toParameter).toBeDefined();
        });
        it('Should be return empty string in case of no params', function () {
            expect(Util.toParameter()).toEqual('');
        });
        it('Should return correct string from params', function () {
            var obj = {test: 'test', key: 'value'};
            expect(Util.toParameter(obj)).toEqual('test=test&key=value');
        });
    });
    describe('createRequestUrl method verification', function () {
        it('Should be defined', function () {
            expect(Util.createRequestUrl).toBeDefined();
        });
        it('Should be create url based on params', function () {
            expect(Util.createRequestUrl('http://reltio.com', {
                key: 'value',
                key2: 'value2'
            })).toEqual('http://reltio.com?key=value&key2=value2');
        });
    });

    describe('merge method verification', function () {
        it('Should be defined', function () {
            expect(Util.merge).toBeDefined();
        });
        it('Should merge parent to child', function () {
            var parent = {a: 'a'},
                child = {b: 'b'};
            Util.merge(parent, child);
            expect(child).toEqual({a: 'a', b: 'b'});
        });
        it('Shouldn\'t replace childs attribute by parent values by default', function () {
            var parent = {a: 'a', c: 'c'},
                child = {b: 'b', a: 'a1'};
            Util.merge(parent, child);
            expect(child).toEqual({a: 'a1', b: 'b', c: 'c'});
        });
        it('Should replace childs attribute by parent values in case of option', function () {
            var parent = {a: 'a', c: 'c'},
                child = {b: 'b', a: 'a1'};
            Util.merge(parent, child, null, true);
            expect(child).toEqual({a: 'a', b: 'b', c: 'c'});
        });
        it('Should ignores properties which are in ignore properties array', function () {
            var parent = {a: 'a', c: 'c'},
                child = {b: 'b'};
            Util.merge(parent, child, ['c']);
            expect(child).toEqual({a: 'a', b: 'b'});
        });
        it('Should deeply merge items', function () {
            var parent = {a: {a: 'a'}},
                child = {a: {b: 'b'}};
            Util.merge(parent, child);
            expect(child).toEqual({a: {a: 'a', b: 'b'}});
        });
    });

    describe('endsWith method verification', function () {
        it('Should be defined', function () {
            expect(Util.endsWith).toBeDefined();
        });
        it('Should return true if string ends with another string', function () {
            expect(Util.endsWith('testa', 'ta')).toBeTruthy();
        });
        it('Should return true in case of empty strings', function () {
            expect(Util.endsWith('', '')).toBeTruthy();
        });
        it('Should return false if string doesn\'t end with another string', function () {
            expect(Util.endsWith('testa', 'st')).toBeFalsy();
        });
    });

    describe('equals method verification', function () {
        it('Should be defined', function () {
            expect(Util.equals).toBeDefined();
        });
        it('Should return true in case the same or null objects', function () {
            var a = {};
            expect(Util.equals(null, null)).toBeTruthy();
            expect(Util.equals(undefined, undefined)).toBeTruthy();
            expect(Util.equals(a, a)).toBeTruthy();
        });
        it('Should return false if one object null and another is non empty', function () {
            var a = {};
            expect(Util.equals(a, null)).toBeFalsy();
            expect(Util.equals(a, undefined)).toBeFalsy();
        });
        it('Should return false if objects has not the same types', function () {
            var a = 1;
            var b = String('1');
            expect(Util.equals(a, b)).toBeFalsy();
        });
        it('Should check equality of the array based on theirs content', function () {
            var a = [1, 2, 3],
                c = [1, 2, 3],
                b = ['1', '2', '3'];
            expect(Util.equals(a, b)).toBeFalsy();
            expect(Util.equals(a, c)).toBeTruthy();
        });
        it('Should check equality of the objects deeply', function () {
            var a = {a: [1, 2, 3], b: 1},
                c = {a: [1, 2, 3], b: 1},
                b = {a: ['1', '2', '3'], b: 1};
            expect(Util.equals(a, b)).toBeFalsy();
            expect(Util.equals(a, c)).toBeTruthy();
        });
    });

    describe('convertToLink method verification', function () {
        it('Should be defined', function () {
            expect(Util.convertToLink).toBeDefined();
        });
        it('Should convert link correctly', function () {
            expect(Util.convertToLink('http://reltio.com')).toBe('http://reltio.com');
        });
        it('Should convert to html tag correctly', function () {
            expect(Util.convertToLink('http://reltio.com', true)).toBe('<a  target="_blank"  href="http://reltio.com">http://reltio.com</a>');
        });
    });

    describe('isLinkValid method verification', function () {
        it('Should be defined', function () {
            expect(Util.isLinkValid).toBeDefined();
        });
        it('Should return true if link starts with http/ftp/mailto', function () {
            expect(Util.isLinkValid('http://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('https://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('ftp://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('ftps://reltio.com')).toBeTruthy();
            expect(Util.isLinkValid('mailto://reltio.com')).toBeTruthy();
        });
        it('Should return false in other cases', function () {
            expect(Util.isLinkValid('htt://reltio.com')).toBeFalsy();
        });
    });
    describe('forEach method verification', function () {
        it('Should be defined', function () {
            expect(Util.forEach).toBeDefined();
        });
        it('Should invoke callback func with self context and params from object', function (done) {
            var context = {};
            Util.forEach([1], function (index, param) {
                expect(context).toEqual(this);
                expect(param).toEqual(1);
                done();
            }, context);
        });
        it('Should invoke callback for each key', function () {
            var context = {};
            var func = function () {
            };
            var o = {};
            o.func = func;
            spyOn(o, 'func');
            Util.forEach([1, 2, 3], o.func, context);
            expect(o.func.calls.count()).toBe(3);
        });
        it('Should work for arrays as well as for objects', function () {
            var context = {};
            var func = function () {
            };
            var o = {};
            o.func = func;
            spyOn(o, 'func');
            Util.forEach({a:1, b:2, c:3}, o.func, context);
            expect(o.func.calls.count()).toBe(3);
        });
        it('Should execute forEach deeply if the parameter provided', function () {
            var context = {};
            var func = function () {
            };
            var o = {};
            o.func = func;
            spyOn(o, 'func');
            Util.forEach([{a:1, b:2, c:3}], o.func, context, true);
            expect(o.func.calls.count()).toBe(4);
        });
        it('Should use global context if context is not provided', function (done) {
            Util.forEach([1], function () {
                expect(window).toEqual(this);
                done();
            });
        });
        it('Should exit in case non iterable argument', function () {
            var context = {};
            var func = function () {
            };
            var o = {};
            o.func = func;
            spyOn(o, 'func');
            Util.forEach('test', o.func, context, true);
            expect(o.func.calls.count()).toBe(0);
        });
    });
    describe('blurField method verification', function () {
        it('Should be defined', function () {
            expect(Util.blurField).toBeDefined();
        });
    });
    describe('filter method verification', function () {
        it('Should be defined', function () {
            expect(Util.filter).toBeDefined();
        });
        it('Should filter object values by condition', function () {
            var a = {a:1, b: 2};
            var result = Util.filter(a, function(value){
                return value > 1
            });
            expect(result).toEqual({b:2});
        });
        it('Should filter object keys by condition', function () {
            var a = {a:1, b: 2};
            var result = Util.filter(a, function(value, key){
                return key !== 'a';
            });
            expect(result).toEqual({b:2});
        });
        it('Should ignore non object params', function () {
            var result = Util.filter([1,2], function(){
                return true;
            });
            expect(result).toEqual({});
        });
    });
    describe('some method verification', function () {
        it('Should be defined', function () {
            expect(Util.some).toBeDefined();
        });
        it('Should return true in case true condition', function () {
            var a = {a:1, b: 2};
            var result = Util.some(a, function(value){
                return value > 1
            });
            expect(result).toBeTruthy();
        });
        it('Should be able to check condition by key', function () {
            var a = {a:1, b: 2};
            var result = Util.some(a, function(value, key){
                return key !== 'a';
            });
            expect(result).toBeTruthy();
        });
        it('Should return false in case of false condition', function () {
            var result = Util.some([1,2], function(value, key){
                return key === 'c';
            });
            expect(result).toBeFalsy();
        });
        it('Should ignore non object params', function () {
            var result = Util.some([1,2], function(){
                return true;
            });
            expect(result).toBeFalsy();
        });
    });
    describe('split uri method verification', function () {
        it('Should be defined', function () {
            expect(Util.splitUri).toBeDefined();
        });

        it('Should create an object from uri', function () {
            var uri = 'http://www.reltio.com:80/reltio/api?test=test#hash';
            var obj = Util.splitUri(uri);
            expect(obj.protocol).toEqual('http');
            expect(obj.host).toEqual('www.reltio.com');
            expect(obj.port).toEqual('80');
            expect(obj.relative).toEqual('/reltio/api?test=test#hash');
            expect(obj.anchor).toEqual('hash');
        });
    });
    describe('replace test', function () {
        it('Should replace string correctly', function () {
            var replace = Util.replace;
            expect(replace('test', 'test', 'abc')).toBe('abc');
            expect(replace('test1', 'test', 'abc')).toBe('abc1');
            expect(replace('1test1', 'test', 'abc')).toBe('1abc1');
            expect(replace('1test', 'test', 'abc')).toBe('1abc');
        });
        it('Shouldn\'t fail for not found string', function () {
            var replace = Util.replace;
            expect(replace('test', 'abc', '123')).toBe('test');
        });
        it('Should replace first entrance', function () {
            var replace = Util.replace;
            expect(replace('testtest', 'test', '123')).toBe('123test');
        });
    });

    describe('sso tests', function () {
        it('Should return support sso true if sso is supported', function () {
            spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
                sso: 'test'
            });
            expect(Util.supportSSO()).toBe(true);
        });
        it('Should return correct url on logout', function () {
            spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
                sso: 'https://login.reltio.com/'
            });
            spyOn(Util, 'getCurrentUrl').and.returnValue('http://tst-01.reltio.com/m/tenant');
            expect(Util.getLogoutURL()).toBe('https://login.reltio.com/logout?redirect_uri=http%3A%2F%2Ftst-01.reltio.com%2Fm%2Ftenant');
        });
        it('Should return tenant not exists url', function () {
            spyOn(MobileUI.core.Services, 'getGeneralSettings').and.returnValue({
                sso: 'https://login.reltio.com/'
            });
            spyOn(MobileUI.core.Services, 'getTenantName').and.returnValue('');
            spyOn(Util, 'getCurrentUrl').and.returnValue('http://tst-01.reltio.com/m/tenant');
            expect(Util.getTenantNotExistsURL()).toBe('https://login.reltio.com/error?id=nonexistentTenant&client_id=mobile&tenant=&redirect_uri=http%3A%2F%2Ftst-01.reltio.com%2Fm%2Ftenant');
        });
    });

    describe('isValidInteger', function () {
        it('smoke test', function () {
            expect(Util.isValidInteger('123')).toBeTruthy();
            expect(Util.isValidInteger('-123')).toBeTruthy();
            expect(Util.isValidInteger('9223372036854775807')).toBeTruthy();
            expect(Util.isValidInteger('-9223372036854775808')).toBeTruthy();

            expect(Util.isValidInteger('-922337203685.4775808')).toBeFalsy();
            expect(Util.isValidInteger('-1.d4775808')).toBeFalsy();
            expect(Util.isValidInteger('9223372036854775808')).toBeFalsy();
            expect(Util.isValidInteger('-9223372036854775809')).toBeFalsy();
            expect(Util.isValidInteger('-11111111111111111111111111')).toBeFalsy();
        });
    });

    describe('isThrottlingError', function () {
        it('should return true in case of throttling error', function () {
            expect(Util.isThrottlingError({
                status: 503,
                responseText: JSON.stringify({errorMessage: 'quota_exceeded'})
            })).toBeTruthy();
        });
        it('should return false in all other cases', function () {
            expect(Util.isThrottlingError({
                status: 503,
                responseText: JSON.stringify({errorMessage: 'anotherError'})
            })).toBeFalsy();
            expect(Util.isThrottlingError({
                status: 503,
                responseText: 'someText'
            })).toBeFalsy();
            expect(Util.isThrottlingError({
                status: 200,
                responseText: 'someText'
            })).toBeFalsy();
        });
    });
});