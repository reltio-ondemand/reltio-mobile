/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

describe('MobileUI.core.util.Validator', function ()
{
    var Validator;
    beforeEach(function ()
    {
        Validator = MobileUI.core.util.Validator;
        jasmine.addMatchers(window.JasmineObjectMatchers);
    });

    describe('isValid method', function ()
    {
        it('Should be a function', function ()
        {
            expect(typeof Validator.isValid).toEqual('function');
        });
        it('Should return false in case empty arguments', function ()
        {
            expect(Validator.isValid()).toBeFalsy();
            expect(Validator.isValid(null)).toBeFalsy();
        });
        it('Should validate url type correctly', function ()
        {
            expect(Validator.isValid('', Validator.types.url)).toBeTruthy();
            expect(Validator.isValid('www.reltio.com', Validator.types.url)).toBeTruthy();
            expect(Validator.isValid('http://www.reltio.com', Validator.types.url)).toBeTruthy();
            expect(Validator.isValid('http:w.reltio.com', Validator.types.url)).toBeFalsy();
        });
        it('Should validate integer type correctly', function ()
        {
            expect(Validator.isValid('', Validator.types.integer)).toBeFalsy();
            expect(Validator.isValid('1', Validator.types.integer)).toBeTruthy();
            expect(Validator.isValid('1.5', Validator.types.integer)).toBeFalsy();
        });
        it('Should validate floatingPoint type correctly', function ()
        {
            expect(Validator.isValid('', Validator.types.floatingPoint)).toBeFalsy();
            expect(Validator.isValid('1', Validator.types.floatingPoint)).toBeTruthy();
            expect(Validator.isValid('1.5', Validator.types.floatingPoint)).toBeTruthy();
        });
        it('Should validate dollar type correctly', function ()
        {
            spyOn(MobileUI.core.I18n, 'parseNumber').and.callFake(function ()
            {
                return 12.5;
            });
            expect(Validator.isValid('12.5', Validator.types.dollar)).toBeTruthy();
            expect(MobileUI.core.I18n.parseNumber).toHaveBeenCalled();
        });

    });
});