/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var getMobileConfiguration = function () {
    return JSON.stringify({
        'plugins': [{
            'id': 'com.reltio.plugins.location',
            'providerName': 'Reltio.com',
            'name': 'Location View',
            'extensions': [{
                'id': 'com.reltio.plugins.entity.org.LocationPerspective',
                'point': 'com.reltio.plugins.ui.perspective',
                'views': ['com.reltio.plugins.entity.loc.PeopleView', 'com.reltio.plugins.entity.loc.OrganizationsView'],
                'class': 'com.reltio.plugins.ui.Perspective',
                'entityType': 'configuration/entityTypes/Location',
                'desktopPerspective': 'com.reltio.plugins.entity.loc.LocationPerspective'
            }, {
                'content': {
                    'inRelations': ['configuration/relationTypes/HasAddress'],
                    'entityTypes': ['configuration/entityTypes/HCP', 'configuration/entityTypes/Individual']
                },
                'id': 'com.reltio.plugins.entity.loc.PeopleView',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'HCP Affiliations'
            }, {
                'content': {
                    'inRelations': ['configuration/relationTypes/HasAddress'],
                    'entityTypes': ['configuration/entityTypes/HCO', 'configuration/entityTypes/Organization']
                },
                'id': 'com.reltio.plugins.entity.loc.OrganizationsView',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'HCO Affiliations'
            }],
            'version': '1.0.0'
        }, {
            'id': 'com.reltio.plugins.organization',
            'providerName': 'Reltio.com',
            'name': 'Organization View',
            'extensions': [{
                'hiddenAttributes': ['configuration/entityTypes/HCO/attributes/Identifiers'],
                'id': 'com.reltio.plugins.entity.org.OrganizationPerspective',
                'point': 'com.reltio.plugins.ui.perspective',
                'views': ['com.reltio.plugins.entity.org.amv2.HCPs_Rating', 'com.reltio.plugins.entity.org.amv2.Administration', 'com.reltio.plugins.entity.org.amv2.IPAContracts', 'com.reltio.plugins.entity.org.amv2.LegalTreeView', 'com.reltio.plugins.entity.org.Media'],
                'class': 'com.reltio.plugins.ui.Perspective',
                'entityType': 'configuration/entityTypes/HCO',
                'desktopPerspective': 'com.reltio.plugins.entity.org.HCOPerspective'
            }, {
                'content': {
                    'inRelations': ['configuration/relationTypes/HasHealthCareRole'],
                    'entityTypes': ['configuration/entityTypes/HCP']
                },
                'id': 'com.reltio.plugins.entity.org.amv2.HCPs_Rating',
                'point': 'com.reltio.plugins.ui.view',
                'sortOptions': {'consolidatedRating': 'Consolidated Rating'},
                'sortOrder': 'asc',
                'class': 'com.reltio.plugins.entity.RatingListView',
                'caption': 'HCPs affiliated with this HCO',
                'type': 'rating'
            }, {
                'id': 'com.reltio.plugins.entity.org.Media',
                'point': 'com.reltio.plugins.ui.view',
                'image': 'https://s3.amazonaws.com/reltio.images/mobile/media_sm.png',
                'class': 'com.reltio.plugins.entity.AttributesFacet',
                'caption': 'Media',
                'attributes': {'type': ['Video URL', 'Image URL']}
            }, {
                'content': {
                    'inRelations': ['configuration/relationTypes/HasAdminRole'],
                    'entityTypes': ['configuration/entityTypes/HCP']
                },
                'id': 'com.reltio.plugins.entity.org.amv2.Administration',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'Administrators'
            }, {
                'content': {
                    'inRelations': ['configuration/relationTypes/IPAContract'],
                    'entityTypes': ['configuration/entityTypes/HCO']
                },
                'id': 'com.reltio.plugins.entity.org.amv2.IPAContracts',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'IPA Contracts'
            }, {
                'id': 'com.reltio.plugins.entity.org.amv2.LegalTreeView',
                'point': 'com.reltio.plugins.ui.view',
                'graph': {'limit': 300, 'layout': 25, 'type': 'configuration/graphTypes/CompanyHierarchy', 'deep': 2},
                'image': 'https://s3.amazonaws.com/reltio.images/mobile/tree_sm.png',
                'class': 'com.reltio.plugins.entity.TreeView',
                'caption': 'Legal Entities Hierarchy'
            }],
            'version': '1.0.0'
        }, {
            'id': 'com.reltio.plugins.search',
            'providerName': 'Reltio.com',
            'name': 'Individual View',
            'extensions': [{
                'id': 'com.reltio.mobile.search.General',
                'point': 'com.reltio.mobile.search',
                'desktopPerspective': 'com.reltio.plugins.search.SearchPerspective'
            }],
            'version': '1.0.0'
        }, {
            'id': 'com.reltio.plugins.individual',
            'providerName': 'Reltio.com',
            'name': 'Individual View',
            'extensions': [{
                'id': 'com.reltio.plugins.entity.org.IndividualPerspective',
                'point': 'com.reltio.plugins.ui.perspective',
                'views': ['com.reltio.plugins.entity.amv2.HCOs_Rating', 'com.reltio.plugins.entity.amv2.Administration', 'com.reltio.plugins.entity.amv2.ProfessionalAssociations', 'com.reltio.plugins.entity.amv2.ACOs'],
                'class': 'com.reltio.plugins.ui.Perspective',
                'attributes': [{
                    'content': ['configuration/entityTypes/HCP/attributes/FirstName', 'configuration/entityTypes/HCP/attributes/LastName', 'configuration/entityTypes/HCP/attributes/Gender'],
                    'label': 'Personality'
                }, {'content': ['Video URL', 'Image URL'], 'label': 'Media'}],
                'entityType': 'configuration/entityTypes/HCP',
                'desktopPerspective': 'com.reltio.plugins.entity.HCPPerspective'
            }, {
                'content': {
                    'entityTypes': ['configuration/entityTypes/HCO'],
                    'outRelations': ['configuration/relationTypes/HasHealthCareRole']
                },
                'id': 'com.reltio.plugins.entity.amv2.HCOs_Rating',
                'point': 'com.reltio.plugins.ui.view',
                'sortOptions': {'consolidatedRating': 'Consolidated Rating'},
                'sortOrder': 'asc',
                'class': 'com.reltio.plugins.entity.RatingListView',
                'caption': 'HCOs this HCP is affiliated with',
                'type': 'rating'
            }, {
                'content': {
                    'entityTypes': ['configuration/entityTypes/HCO'],
                    'outRelations': ['configuration/relationTypes/HasAdminRole']
                },
                'id': 'com.reltio.plugins.entity.amv2.Administration',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'Administrator of'
            }, {
                'content': {
                    'entityTypes': ['configuration/entityTypes/ProfessionalAssociation'],
                    'outRelations': ['configuration/relationTypes/ProfAssocMember']
                },
                'id': 'com.reltio.plugins.entity.amv2.ProfessionalAssociations',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'Professional Associations'
            }, {
                'content': {
                    'entityTypes': ['configuration/entityTypes/ACO'],
                    'outRelations': ['configuration/relationTypes/ACOMember']
                },
                'id': 'com.reltio.plugins.entity.amv2.ACOs',
                'point': 'com.reltio.plugins.ui.view',
                'class': 'com.reltio.plugins.entity.ListView',
                'caption': 'ACOs'
            }],
            'version': '1.0.0'
        }]
    });
};

var getTestEntity = function () {
    return {
        'uri': 'entities/07nTyz3',
        'type': 'configuration/entityTypes/HCO',
        'createdBy': 'dtarasenko',
        'createdTime': 1424094269654,
        'updatedBy': 'dtarasenko',
        'updatedTime': 1424094269654,
        'attributes': {
            'Name': [{
                'type': 'configuration/entityTypes/HCO/attributes/Name',
                'ov': true,
                'value': 'Alta Bates Summit Medical Center Alta Bates Campus1',
                'pin': true,
                'uri': 'entities/07nTyz3/attributes/Name/HTYnlOz'
            }, {
                'type': 'configuration/entityTypes/HCO/attributes/Name',
                'ov': false,
                'value': 'Alta Bates Summit Medical Center Alta Bates Campus',
                'uri': 'entities/07nTyz3/attributes/Name/HTYnpfF'
            }],
            'Phone': [{
                'label': '(510) 204-4450 Fax',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Type',
                        'ov': true,
                        'value': 'Fax',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp'
                    }],
                    'Number': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Number',
                        'ov': true,
                        'value': '(510) 204-4450',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5'
                    }],
                    'ValidationStatus': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/ValidationStatus',
                        'ov': true,
                        'value': 'VALID',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL'
                    }],
                    'Active': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Active',
                        'ov': true,
                        'value': 'true',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Phone/HTYqa9Z'
            }, {
                'label': '(510) 204-4444 Business',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Type',
                        'ov': true,
                        'value': 'Business',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X'
                    }],
                    'Number': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Number',
                        'ov': true,
                        'value': '(510) 204-4444',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn'
                    }],
                    'ValidationStatus': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/ValidationStatus',
                        'ov': true,
                        'value': 'VALID',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3'
                    }],
                    'Active': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Active',
                        'ov': true,
                        'value': 'true',
                        'uri': 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Phone/HTYqEqH'
            }],
            'WebsiteURL': [{
                'type': 'configuration/entityTypes/HCO/attributes/WebsiteURL',
                'ov': true,
                'value': 'http://www.yelp.com/biz/alta-bates-summit-medical-center-summit-campus-in-oakland-oakland',
                'uri': 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7'
            }, {
                'type': 'configuration/entityTypes/HCO/attributes/WebsiteURL',
                'ov': true,
                'value': 'www.altabatessummit.org',
                'uri': 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr'
            }],
            'Status': [{
                'type': 'configuration/entityTypes/HCO/attributes/Status',
                'ov': true,
                'value': 'Operating',
                'uri': 'entities/07nTyz3/attributes/Status/HTYr8Fd'
            }, {
                'type': 'configuration/entityTypes/HCO/attributes/Status',
                'ov': true,
                'value': 'Active',
                'uri': 'entities/07nTyz3/attributes/Status/HTYr3zN'
            }],
            'HealthSystemName': [{
                'type': 'configuration/entityTypes/HCO/attributes/HealthSystemName',
                'ov': true,
                'value': 'Sutter',
                'uri': 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt'
            }],
            'ImageLinks': [{
                'type': 'configuration/entityTypes/HCO/attributes/ImageLinks',
                'ov': true,
                'value': 'http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Alta_Bates_Summit_Medical_Center.jpg/275px-Alta_Bates_Summit_Medical_Center.jpg',
                'uri': 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf'
            }, {
                'type': 'configuration/entityTypes/HCO/attributes/ImageLinks',
                'ov': true,
                'value': 'https://doximity-res.cloudinary.com/image/upload/t_public_profile_photo_320x320/v1376071969/l1pnydlfnn4hlwiqkqzc.jpg',
                'uri': 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P'
            }, {
                'type': 'configuration/entityTypes/HCO/attributes/ImageLinks',
                'ov': true,
                'value': 'http://s3-media3.fl.yelpcdn.com/bphoto/9WRrLzzFZMqn_F-cnlFJpw/l.jpg',
                'uri': 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9'
            }],
            'Identifiers': [{
                'label': 'DDD 94705202 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'DDD',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '94705202',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrt8T'
            }, {
                'label': 'DDD 94705200 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'DDD',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '94705200',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrgLh'
            }, {
                'label': 'DDD 94705201 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'DDD',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '94705201',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYrTYv'
            }, {
                'label': 'DEA AA1362499 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'DEA',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': 'AA1362499',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYs5vF'
            }, {
                'label': 'HIN GJ48GG700HOHH ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'HIN',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': 'GJ48GG700HOHH',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsIi1'
            }, {
                'label': 'IMS ORG ID INS00006234 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'IMS ORG ID',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': 'INS00006234',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsVUn'
            }, {
                'label': 'NPI 1922140771 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'NPI',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '1922140771',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYt7r7'
            }, {
                'label': 'NPI 1013906221 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'NPI',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '1013906221',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsv4L'
            }, {
                'label': 'NPI 1316088024 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'NPI',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': '1316088024',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'
            }, {
                'label': 'PHS DSH050305 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'PHS',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': 'DSH050305',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtKdt'
            }, {
                'label': 'PHS HM146 ',
                'value': {
                    'Type': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                        'ov': true,
                        'value': 'PHS',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv'
                    }],
                    'ID': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                        'ov': true,
                        'value': 'HM146',
                        'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Identifiers/HTYtXQf'
            }],
            'Address': [{
                'label': '2450 Ashby Ave Berkeley CA 94705',
                'relationshipLabel': 'Business',
                'value': {
                    'AddressType': [{
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'Business',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/AddressType/HTZJBEv'
                    }],
                    'AddressLine1': [{
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': true,
                        'value': '2450 Ashby Ave',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/AddressLine1/HTZIYsb'
                    }],
                    'Premise': [{
                        'type': 'configuration/entityTypes/Location/attributes/Premise',
                        'ov': true,
                        'value': '2450',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/Premise/HTZJ2iP'
                    }],
                    'Street': [{
                        'type': 'configuration/entityTypes/Location/attributes/Street',
                        'ov': true,
                        'value': 'Ashby Ave',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/Street/HTZIhP7'
                    }],
                    'City': [{
                        'type': 'configuration/entityTypes/Location/attributes/City',
                        'ov': true,
                        'value': 'Berkeley',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/City/HTZIDZJ'
                    }],
                    'StateProvince': [{
                        'type': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'ov': true,
                        'value': 'CA',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/StateProvince/HTZIHpZ'
                    }],
                    'Zip': [{
                        'label': '94705-',
                        'value': {
                            'Zip5': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                'ov': true,
                                'value': '94705',
                                'uri': 'entities/07nTyz3/attributes/Address/03saadV/Zip/HTZIQM5/Zip5/HTZIUcL'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/Zip/HTZIQM5'
                    }],
                    'Country': [{
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': true,
                        'value': 'United States',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/Country/HTZId8r'
                    }],
                    'GeoLocation': [{
                        'label': '37.855920, -122.257300',
                        'value': {
                            'Latitude': [{
                                'type': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                                'ov': true,
                                'value': '37.855920',
                                'uri': 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/Latitude/HTZIuBt'
                            }],
                            'Longitude': [{
                                'type': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude',
                                'ov': true,
                                'value': '-122.257300',
                                'uri': 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/Longitude/HTZIyS9'
                            }],
                            'GeoAccuracy': [{
                                'type': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/GeoAccuracy',
                                'ov': true,
                                'value': 'P4',
                                'uri': 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/GeoAccuracy/HTZIpvd'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN'
                    }],
                    'SubAdministrativeArea': [{
                        'type': 'configuration/entityTypes/Location/attributes/SubAdministrativeArea',
                        'ov': true,
                        'value': 'Alameda',
                        'uri': 'entities/07nTyz3/attributes/Address/03saadV/SubAdministrativeArea/HTZIM5p'
                    }]
                },
                'ov': true,
                'uri': 'entities/07nTyz3/attributes/Address/03saadV',
                'refEntity': {
                    'crosswalks': [{
                        'type': 'configuration/sources/MOCA',
                        'value': '698e83d0991a4518f4281d70f7d1ffcf',
                        'createDate': '2015-02-16T13:44:29.654Z',
                        'updateDate': '2015-02-16T13:44:29.654Z',
                        'uri': 'entities/07nTyz3/crosswalks/07nUwef.HTYnyBl',
                        'attributeURIs': ['entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/Longitude/HTZIyS9', 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/GeoAccuracy/HTZIpvd', 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN/Latitude/HTZIuBt', 'entities/07nTyz3/attributes/Address/03saadV/AddressLine1/HTZIYsb', 'entities/07nTyz3/attributes/Address/03saadV/Zip/HTZIQM5', 'entities/07nTyz3/attributes/Address/03saadV/Street/HTZIhP7', 'entities/07nTyz3/attributes/Address/03saadV/Country/HTZId8r', 'entities/07nTyz3/attributes/Address/03saadV/SubAdministrativeArea/HTZIM5p', 'entities/07nTyz3/attributes/Address/03saadV/StateProvince/HTZIHpZ', 'entities/07nTyz3/attributes/Address/03saadV/Zip/HTZIQM5/Zip5/HTZIUcL', 'entities/07nTyz3/attributes/Address/03saadV/City/HTZIDZJ', 'entities/07nTyz3/attributes/Address/03saadV', 'entities/07nTyz3/attributes/Address/03saadV/GeoLocation/HTZIlfN', 'entities/07nTyz3/attributes/Address/03saadV/Premise/HTZJ2iP']
                    }], 'type': 'configuration/entityTypes/Location', 'objectURI': 'entities/07nUwef'
                },
                'refRelation': {
                    'crosswalks': [{
                        'type': 'configuration/sources/MOCA',
                        'value': 'R1002469950C1',
                        'createDate': '2015-02-16T13:44:29.654Z',
                        'updateDate': '2015-02-16T13:44:29.654Z',
                        'uri': 'entities/07nTyz3/crosswalks/03saadV.HTYo2S1',
                        'attributeURIs': ['entities/07nTyz3/attributes/Address/03saadV/AddressType/HTZJBEv', 'entities/07nTyz3/attributes/Address/03saadV']
                    }], 'objectURI': 'relations/03saadV'
                },
                'startObjectCrosswalks': [{
                    'uri': 'entities/07nTyz3/crosswalks/HTZNNEf',
                    'type': 'configuration/sources/Reltio',
                    'value': '07nTyz3',
                    'reltioLoadDate': '2015-03-03T09:09:23.709Z',
                    'createDate': '2015-03-03T09:09:23.709Z',
                    'updateDate': '2015-03-03T09:09:23.709Z',
                    'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
                    'singleAttributeUpdateDates': {}
                }, {
                    'uri': 'entities/07nTyz3/crosswalks/HTYtkDR',
                    'type': 'configuration/sources/HCOS',
                    'value': 'INS00006234',
                    'reltioLoadDate': '2015-03-03T09:09:23.709Z',
                    'createDate': '2015-03-03T09:09:23.709Z',
                    'updateDate': '2015-03-03T09:09:23.709Z',
                    'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
                    'singleAttributeUpdateDates': {}
                }]
            }, {
                'label': '2450 Ashby Avenue Alta Bates Campus Berkeley CA 94705',
                'relationshipLabel': 'Business, BestVisit, Mailing, Home',
                'value': {
                    'AddressType': [{
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'Business',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZK4eH'
                    }, {
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'BestVisit',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZJrrV'
                    }, {
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'Mailing',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZK0O1'
                    }, {
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'Home',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZJw7l'
                    }],
                    'AddressRank': [{
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                        'ov': true,
                        'value': '2',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressRank/HTZJnbF'
                    }],
                    'AddressLine1': [{
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': true,
                        'value': '2450 Ashby Avenue Alta Bates Campus',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/AddressLine1/HTZJaoT'
                    }],
                    'City': [{
                        'type': 'configuration/entityTypes/Location/attributes/City',
                        'ov': true,
                        'value': 'Berkeley',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/City/HTZJJlR'
                    }],
                    'StateProvince': [{
                        'type': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'ov': true,
                        'value': 'CA',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/StateProvince/HTZJO1h'
                    }],
                    'Zip': [{
                        'label': '94705-',
                        'value': {
                            'Zip5': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                'ov': true,
                                'value': '94705',
                                'uri': 'entities/07nTyz3/attributes/Address/03saetl/Zip/HTZJSHx/Zip5/HTZJWYD'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/Zip/HTZJSHx'
                    }],
                    'Country': [{
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': true,
                        'value': 'United States',
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/Country/HTZJf4j'
                    }],
                    'Phone': [{
                        'label': ' Mobile',
                        'value': {
                            'Type': [{
                                'type': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Type',
                                'ov': true,
                                'value': 'Mobile',
                                'uri': 'entities/07nTyz3/attributes/Address/03saetl/Phone/HTZK8uX/Type/HTZKDAn'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/07nTyz3/attributes/Address/03saetl/Phone/HTZK8uX'
                    }]
                },
                'ov': false,
                'uri': 'entities/07nTyz3/attributes/Address/03saetl',
                'refEntity': {
                    'crosswalks': [{
                        'type': 'configuration/sources/HCOS',
                        'value': '8b6e1f56c4e2fba909a42ab639daf5eb',
                        'createDate': '2015-02-16T13:44:29.654Z',
                        'updateDate': '2015-02-16T13:44:29.654Z',
                        'uri': 'entities/07nTyz3/crosswalks/07nV0uv.HTYp8e9',
                        'attributeURIs': ['entities/07nTyz3/attributes/Address/03saetl/AddressLine1/HTZJaoT', 'entities/07nTyz3/attributes/Address/03saetl/Zip/HTZJSHx', 'entities/07nTyz3/attributes/Address/03saetl/Zip/HTZJSHx/Zip5/HTZJWYD', 'entities/07nTyz3/attributes/Address/03saetl/Country/HTZJf4j', 'entities/07nTyz3/attributes/Address/03saetl/City/HTZJJlR', 'entities/07nTyz3/attributes/Address/03saetl', 'entities/07nTyz3/attributes/Address/03saetl/StateProvince/HTZJO1h']
                    }], 'type': 'configuration/entityTypes/Location', 'objectURI': 'entities/07nV0uv'
                },
                'refRelation': {
                    'crosswalks': [{
                        'type': 'configuration/sources/HCOS',
                        'value': 'RINS00006234',
                        'createDate': '2015-02-16T13:44:29.654Z',
                        'updateDate': '2015-02-16T13:44:29.654Z',
                        'uri': 'entities/07nTyz3/crosswalks/03saetl.HTYpHAf',
                        'attributeURIs': ['entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZK4eH', 'entities/07nTyz3/attributes/Address/03saetl/Phone/HTZK8uX', 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZJw7l', 'entities/07nTyz3/attributes/Address/03saetl/Phone/HTZK8uX/Type/HTZKDAn', 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZJrrV', 'entities/07nTyz3/attributes/Address/03saetl/AddressRank/HTZJnbF', 'entities/07nTyz3/attributes/Address/03saetl/AddressType/HTZK0O1', 'entities/07nTyz3/attributes/Address/03saetl']
                    }], 'objectURI': 'relations/03saetl'
                },
                'startObjectCrosswalks': [{
                    'uri': 'entities/07nTyz3/crosswalks/HTZNNEf',
                    'type': 'configuration/sources/Reltio',
                    'value': '07nTyz3',
                    'reltioLoadDate': '2015-03-03T09:09:23.709Z',
                    'createDate': '2015-03-03T09:09:23.709Z',
                    'updateDate': '2015-03-03T09:09:23.709Z',
                    'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
                    'singleAttributeUpdateDates': {}
                }, {
                    'uri': 'entities/07nTyz3/crosswalks/HTYtkDR',
                    'type': 'configuration/sources/HCOS',
                    'value': 'INS00006234',
                    'reltioLoadDate': '2015-03-03T09:09:23.709Z',
                    'createDate': '2015-03-03T09:09:23.709Z',
                    'updateDate': '2015-03-03T09:09:23.709Z',
                    'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
                    'singleAttributeUpdateDates': {}
                }]
            }]
        },
        'roles': ['configuration/roles/MailOrderPharmacy', 'configuration/roles/Hospital', 'configuration/roles/VeteransAdmin', 'configuration/roles/SPPH', 'configuration/roles/Pharmacy'],
        'crosswalks': [{
            'uri': 'entities/07nTyz3/crosswalks/HTZNNEf',
            'type': 'configuration/sources/Reltio',
            'value': '07nTyz3',
            'reltioLoadDate': '2015-02-16T13:44:29.654Z',
            'createDate': '2015-02-16T13:44:29.654Z',
            'updateDate': '2015-02-16T13:44:29.654Z',
            'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
            'singleAttributeUpdateDates': {}
        }, {
            'uri': 'entities/07nTyz3/crosswalks/HTYtkDR',
            'type': 'configuration/sources/HCOS',
            'value': 'INS00006234',
            'reltioLoadDate': '2015-02-16T13:44:29.654Z',
            'createDate': '2015-02-16T13:44:29.654Z',
            'updateDate': '2015-02-16T13:44:29.654Z',
            'attributes': ['entities/07nTyz3/attributes/Phone/HTYqEqH', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/ID/HTYs1ez', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/ID/HTYtTAP', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1', 'entities/07nTyz3/attributes/WebsiteURL/HTYqzj7', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/Type/HTYrkbx', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T/Type/HTYrxOj', 'entities/07nTyz3/attributes/Name/HTYnlOz', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Active/HTYqrCb', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh/ID/HTYrosD', 'entities/07nTyz3/attributes/Status/HTYr3zN', 'entities/07nTyz3/attributes/ImageLinks/HTYrL2P', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/ID/HTYt3ar', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/ID/HTYse1J', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/ID/HTYtfxB', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/Type/HTYrXpB', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt/Type/HTYtOu9', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/Type/HTYsMyH', 'entities/07nTyz3/attributes/Identifiers/HTYtXQf/Type/HTYtbgv', 'entities/07nTyz3/attributes/Identifiers/HTYrt8T', 'entities/07nTyz3/attributes/Identifiers/HTYsVUn/Type/HTYsZl3', 'entities/07nTyz3/attributes/HealthSystemName/HTYrCVt', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/Type/HTYsmXp', 'entities/07nTyz3/attributes/Phone/HTYqa9Z', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L', 'entities/07nTyz3/attributes/Identifiers/HTYrgLh', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Number/HTYqig5', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/Type/HTYsABV', 'entities/07nTyz3/attributes/Identifiers/HTYs5vF/ID/HTYsERl', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Number/HTYqNMn', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/Type/HTYqePp', 'entities/07nTyz3/attributes/Identifiers/HTYtKdt', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/ID/HTYtGNd', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Active/HTYqVtJ', 'entities/07nTyz3/attributes/Phone/HTYqEqH/ValidationStatus/HTYqRd3', 'entities/07nTyz3/attributes/Identifiers/HTYsv4L/Type/HTYszKb', 'entities/07nTyz3/attributes/Identifiers/HTYt7r7/Type/HTYtC7N', 'entities/07nTyz3/attributes/Phone/HTYqa9Z/ValidationStatus/HTYqmwL', 'entities/07nTyz3/attributes/Status/HTYr8Fd', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ/ID/HTYsqo5', 'entities/07nTyz3/attributes/Name/HTYnpfF', 'entities/07nTyz3/attributes/Phone/HTYqEqH/Type/HTYqJ6X', 'entities/07nTyz3/attributes/Identifiers/HTYrTYv/ID/HTYrc5R', 'entities/07nTyz3/attributes/Identifiers/HTYsIi1/ID/HTYsREX', 'entities/07nTyz3/attributes/WebsiteURL/HTYqvSr', 'entities/07nTyz3/attributes/ImageLinks/HTYrPIf', 'entities/07nTyz3/attributes/ImageLinks/HTYrGm9', 'entities/07nTyz3/attributes/Identifiers/HTYsiHZ'],
            'singleAttributeUpdateDates': {}
        }],
        'defaultProfilePic': 'entities/07nTyz3/attributes/ImageLinks/3Gz',
        'tags': ['gf test', 'KOL'],
        'consolidatedRating': 5.0,
        'label': 'Alta Bates Summit Medical Center Alta Bates Campus1',
        'secondaryLabel': '2450 Ashby Ave Berkeley CA 94705'
    }
};
var getReferencedAttribute = function () {
    return {
        attrType: {
            uri: 'configuration/entityTypes/HCO/attributes/Address',
            'label': 'Address',
            'name': 'Address',
            'type': 'Reference',
            'hidden': false,
            'important': false,
            'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/POBox', 'configuration/entityTypes/Location/attributes/Building', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active', 'configuration/relationTypes/HasAddress/attributes/CRMSONRowID', 'configuration/relationTypes/HasAddress/attributes/InclInTerrAssign', 'configuration/relationTypes/HasAddress/attributes/Sample', 'configuration/relationTypes/HasAddress/attributes/KeyContact', 'configuration/relationTypes/HasAddress/attributes/Alignment', 'configuration/relationTypes/HasAddress/attributes/RecordType', 'configuration/relationTypes/HasAddress/attributes/ExternalID', 'configuration/relationTypes/HasAddress/attributes/MobileID', 'configuration/relationTypes/HasAddress/attributes/Source'],
            'relationshipLabelPattern': 'rank - {AddressRank}',
            'attributeOrdering': {
                'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                'orderType': 'ASC',
                'orderingStrategy': 'FieldBased'
            },
            'referencedEntityTypeURI': 'configuration/entityTypes/Location',
            'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
        }, 'value': [], 'parentUri': 'entities/$temp_7YHVF3', 'uri': 'entities/$temp_7YHVF3/attributes/Address/zLUVty'
    }
};
var getLocationEntity = function () {
    return {
        'uri': 'entities/5upheAV',
        'type': 'configuration/entityTypes/Location',
        'createdBy': 'iplyac2',
        'createdTime': 1431532073096,
        'updatedBy': 'iplyac2',
        'updatedTime': 1431532073096,
        'attributes': {
            'AddressLine1': [{
                'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                'ov': true,
                'value': 'Something Place 5712',
                'uri': 'entities/5upheAV/attributes/AddressLine1/1nrLHFfn'
            }],
            'City': [{
                'type': 'configuration/entityTypes/Location/attributes/City',
                'ov': true,
                'value': 'Test.City_5712',
                'uri': 'entities/5upheAV/attributes/City/1nrLHJw3'
            }],
            'Zip': [{
                'label': 'Test.ZIP5.5712-',
                'value': {
                    'Zip5': [{
                        'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                        'ov': true,
                        'value': 'Test.ZIP5.5712',
                        'uri': 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ'
                    }]
                },
                'ov': true,
                'uri': 'entities/5upheAV/attributes/Zip/1nrLHOCJ'
            }]
        },
        'crosswalks': [{
            'uri': 'entities/5upheAV/crosswalks/1nrLHBPX',
            'type': 'configuration/sources/Reltio',
            'value': '5upheAV',
            'reltioLoadDate': '2015-05-13T15:47:53.096Z',
            'createDate': '2015-05-13T15:47:53.096Z',
            'updateDate': '2015-05-13T15:47:53.096Z',
            'attributes': ['entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ', 'entities/5upheAV/attributes/City/1nrLHJw3', 'entities/5upheAV/attributes/Zip/1nrLHOCJ', 'entities/5upheAV/attributes/AddressLine1/1nrLHFfn'],
            'singleAttributeUpdateDates': {}
        }],
        'consolidatedRating': 5,
        'label': 'Something Place 5712 Test.City_5712  Test.ZIP5.5712',
        'secondaryLabel': ''
    }
};
var getAttributes = function () {
    return [{
        'attrType': {
            'uri': 'configuration/entityTypes/HCO/attributes/Name',
            'label': 'Name',
            'name': 'Name',
            'description': 'Name',
            'type': 'String',
            'hidden': false,
            'important': false,
            'searchable': true,
            'attributeOrdering': {'orderingStrategy': 'LUD'}
        }, 'value': [], 'parentUri': 'entities/$temp_Vl0D1w', 'uri': 'entities/$temp_Vl0D1w/attributes/Name/dVpD3j'
    }, {
        'attrType': {
            'uri': 'configuration/entityTypes/HCO/attributes/Address',
            'label': 'Address',
            'name': 'Address',
            'type': 'Reference',
            'hidden': false,
            'important': false,
            'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/POBox', 'configuration/entityTypes/Location/attributes/Building', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active', 'configuration/relationTypes/HasAddress/attributes/CRMSONRowID', 'configuration/relationTypes/HasAddress/attributes/InclInTerrAssign', 'configuration/relationTypes/HasAddress/attributes/Sample', 'configuration/relationTypes/HasAddress/attributes/KeyContact', 'configuration/relationTypes/HasAddress/attributes/Alignment', 'configuration/relationTypes/HasAddress/attributes/RecordType', 'configuration/relationTypes/HasAddress/attributes/ExternalID', 'configuration/relationTypes/HasAddress/attributes/MobileID', 'configuration/relationTypes/HasAddress/attributes/Source'],
            'relationshipLabelPattern': 'rank - {AddressRank}',
            'attributeOrdering': {
                'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                'orderType': 'ASC',
                'orderingStrategy': 'FieldBased'
            },
            'referencedEntityTypeURI': 'configuration/entityTypes/Location',
            'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
        },
        'parentUri': 'entities/$temp_Vl0D1w',
        'uri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC',
        'value': [],
        'refEntity': {
            'crosswalks': [{'type': 'configuration/sources/Reltio'}],
            'type': 'configuration/entityTypes/Location',
            'objectURI': 'entities/5upheAV'
        },
        'refRelation': {'crosswalks': [{'type': 'configuration/sources/Reltio'}], 'objectURI': 'entities/5upheAV'},
        'label': 'Something Place 5712 Test.City_5712  Test.ZIP5.5712'
    }, {
        'value': [],
        'attrType': {
            'uri': 'configuration/relationTypes/HasAddress/attributes/AddressType',
            'label': 'Address Type',
            'name': 'AddressType',
            'type': 'String',
            'lookupCode': 'ADDR_TYPE',
            'hidden': false,
            'important': false,
            'attributeOrdering': {'orderingStrategy': 'LUD'}
        },
        'uri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC/AddressType/unVlxI',
        'parentUri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC',
        'refEntityUri': 'entities/5upheAV'
    }, {
        'value': ['Test.ZIP5.5712'],
        'attrType': {
            'uri': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
            'label': 'Zip5',
            'name': 'Zip5',
            'type': 'String',
            'hidden': false,
            'important': false,
            'searchable': false,
            'attributeOrdering': {'orderingStrategy': 'LUD'}
        },
        'uri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC/Zip/SmUnvh/Zip5/JBiOB5',
        'parentUri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC/Zip/SmUnvh'
    }, {
        'value': [{
            'Zip5': [{
                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                'ov': true,
                'value': 'Test.ZIP5.5712',
                'uri': 'entities/5upheAV/attributes/Zip/1nrLHOCJ/Zip5/1nrLHSSZ'
            }]
        }],
        'attrType': {
            'uri': 'configuration/entityTypes/Location/attributes/Zip',
            'label': 'Zip',
            'name': 'Zip',
            'type': 'Nested',
            'hidden': false,
            'important': false,
            'dataLabelPattern': '{Zip5}-{Zip4}',
            'attributeOrdering': {'orderingStrategy': 'LUD'},
            'attributes': [{
                'uri': 'configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode',
                'label': 'Postal Code',
                'name': 'PostalCode',
                'type': 'String',
                'hidden': false,
                'important': false,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            }, {
                'uri': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                'label': 'Zip5',
                'name': 'Zip5',
                'type': 'String',
                'hidden': false,
                'important': false,
                'searchable': false,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            }, {
                'uri': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4',
                'label': 'Zip4',
                'name': 'Zip4',
                'type': 'String',
                'hidden': false,
                'important': false,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            }]
        },
        'uri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC/Zip/SmUnvh',
        'parentUri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC',
        'refEntityUri': 'entities/5upheAV'
    }, {
        'value': [],
        'attrType': {
            'uri': 'configuration/relationTypes/HasAddress/attributes/ExternalID',
            'label': 'External ID',
            'name': 'ExternalID',
            'description': '',
            'type': 'String',
            'hidden': false,
            'important': false,
            'faceted': true,
            'attributeOrdering': {'orderingStrategy': 'LUD'}
        },
        'uri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC/ExternalID/S7m0YR',
        'parentUri': 'entities/$temp_Vl0D1w/attributes/Address/K9DYPC',
        'refEntityUri': 'entities/5upheAV'
    }]
};
var getMetadata = function () {
    return JSON.stringify({
            'uri': 'configuration',
            'label': '',
            'description': 'abbvie.v18.9-pilot-copy',
            'schemaVersion': '1',
            'referenceConfigurationURI': 'configuration/_vertical/life-sciences',
            'entityTypes': [{
                'uri': 'configuration/entityTypes/HCP',
                'label': 'HCP',
                'description': 'Health care provider',
                'geoLocationAttributes': [{
                    'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                    'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                }],
                'dataLabelPattern': '{FirstName} {LastName}, {Credentials}',
                'secondaryLabelPattern': '{Address}',
                'typeColor': '#99CCFF',
                'survivorshipGroups': [{
                    'uri': 'configuration/entityTypes/HCP/survivorshipGroups/default',
                    'default': false,
                    'mapping': [{
                        'attribute': 'configuration/entityTypes/HCP/attributes/Degree',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Education',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/License',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FirstName',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/MiddleName',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/LastName',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Name',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Prefix',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Address',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Phone',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Gender',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoB',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCity',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthState',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCountry',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoD',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Specialities',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecHCP',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecDescHCP',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SuffixName',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StandardJobTitle',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfType',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Title',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Credentials',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Identifiers',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/affiliatedwith',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Status',
                        'sourcesUriOrder': ['configuration/sources/IREP', 'configuration/sources/HCOS'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Employment',
                        'survivorshipStrategy': 'Aggregation'
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/survivorshipGroups/survivorshipGroupsZZT0Kdu',
                    'label': 'Marketing',
                    'description': '',
                    'default': false,
                    'mapping': [{
                        'attribute': 'configuration/entityTypes/HCP/attributes/Degree',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Education',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/License',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FirstName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/MiddleName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/LastName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Name',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Prefix',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Address',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Phone',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Gender',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoB',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCity',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthState',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCountry',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoD',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Specialities',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecHCP',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecDescHCP',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SuffixName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StandardJobTitle',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfType',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Title',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Credentials',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Identifiers',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/affiliatedwith',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Status',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Employment',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ImsXponentID',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Administrator',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Research',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ClinicalTrials',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DEABusinessActivity',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StatusReasonCode',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Group',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/TaxID',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StatusUpdateDate',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SSN',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SSNLast4',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ME',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/NPI',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/UPIN',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Sanction',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Certificates',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Credential',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/MajorProfessionalActivity',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PresentEmployment',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/TypeOfPractice',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PresumedDead',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Degrees',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Speaker',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Email',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PreferredName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Nickname',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DoB',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DoD',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/KaiserProvider',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Solo',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/WebsiteURL',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ImageLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DocumentLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/VideoLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Description',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerFirstName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerLastName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerMiddleName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerSuffixName',
                        'survivorshipStrategy': 'LUD'
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/survivorshipGroups/survivorshipGroupsZZT0OuA',
                    'label': 'Sales',
                    'description': '',
                    'default': true,
                    'mapping': [{
                        'attribute': 'configuration/entityTypes/HCP/attributes/Degree',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Education',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/License',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FirstName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/MiddleName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/LastName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Name',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Prefix',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Address',
                        'sourcesUriOrder': ['configuration/sources/Reltio', 'configuration/sources/ReltioCleanser', 'configuration/sources/AMS', 'configuration/sources/AMA', 'configuration/sources/AOA', 'configuration/sources/IMSDDD', 'configuration/sources/LNKD', 'configuration/sources/FB', 'configuration/sources/IMSPLAN', 'configuration/sources/IREP', 'configuration/sources/MOCA', 'configuration/sources/DEA', 'configuration/sources/HMS', 'configuration/sources/DT_PA', 'configuration/sources/ABV_MABI', 'configuration/sources/RPA', 'configuration/sources/TWITTER', 'configuration/sources/Veeva', 'configuration/sources/HCOS', 'configuration/sources/AHA'],
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Phone',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Gender',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoB',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCity',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthState',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/BirthCountry',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/YoD',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Specialities',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecHCP',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/AbvBestSpecDescHCP',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SuffixName',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StandardJobTitle',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfType',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Title',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Credentials',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Identifiers',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/affiliatedwith',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Status',
                        'survivorshipStrategy': 'SRC_SYS'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Employment',
                        'survivorshipStrategy': 'Aggregation'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ImsXponentID',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Administrator',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Research',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ClinicalTrials',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DEABusinessActivity',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StatusReasonCode',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Group',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/TaxID',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/StatusUpdateDate',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SSN',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/SSNLast4',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ME',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/NPI',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/UPIN',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Sanction',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Certificates',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Credential',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/MajorProfessionalActivity',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PresentEmployment',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/TypeOfPractice',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PresumedDead',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Degrees',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Speaker',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Email',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/PreferredName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Nickname',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DoB',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DoD',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/KaiserProvider',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Solo',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/WebsiteURL',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/ImageLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/DocumentLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/VideoLinks',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/Description',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerFirstName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerLastName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerMiddleName',
                        'survivorshipStrategy': 'LUD'
                    }, {
                        'attribute': 'configuration/entityTypes/HCP/attributes/FormerSuffixName',
                        'survivorshipStrategy': 'LUD'
                    }]
                }],
                'matchGroups': [{
                    'uri': 'configuration/entityTypes/HCP/matchGroups/PersonByIMSID',
                    'label': 'HCP by IMS ID (#0)',
                    'type': 'automatic',
                    'rule': {
                        'and': {
                            'exact': ['configuration/entityTypes/HCP/attributes/Identifiers/attributes/ID'],
                            'equals': [{
                                'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type',
                                'value': 'IMS ID'
                            }]
                        }, 'matchTokenClass': 'com.reltio.match.token.ExactMatchToken'
                    },
                    'matchServiceClass': 'com.reltio.businesslogic.match.providers.internal.InternalMatchService'
                }],
                'extendsTypeURI': 'configuration/entityTypes/Individual',
                'cleanseConfig': {
                    'infos': [{
                        'uri': 'configuration/entityTypes/Individual/cleanse/infos/default',
                        'useInCleansing': true,
                        'sequence': [{
                            'chain': [{
                                'cleanseFunction': 'InitialsCleanser',
                                'proceedOnSuccess': true,
                                'proceedOnFailure': true,
                                'mapping': {
                                    'inputMapping': [{
                                        'attribute': 'configuration/entityTypes/Individual/attributes/MiddleName',
                                        'cleanseAttribute': 'MiddleName',
                                        'mandatory': true,
                                        'allValues': false
                                    }],
                                    'outputMapping': [{
                                        'attribute': 'configuration/entityTypes/Individual/attributes/MiddleInitial',
                                        'cleanseAttribute': 'MiddleName',
                                        'mandatory': true,
                                        'allValues': false
                                    }]
                                }
                            }]
                        }]
                    }]
                },
                'attributes': [{
                    'uri': 'configuration/entityTypes/HCP/attributes/FirstName',
                    'label': 'First Name',
                    'name': 'FirstName',
                    'description': 'First Name',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/LastName',
                    'label': 'Last Name',
                    'name': 'LastName',
                    'description': 'Last Name',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': false,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/MiddleName',
                    'label': 'Middle Name',
                    'name': 'MiddleName',
                    'description': 'Middle Name',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Address',
                    'label': 'Address',
                    'name': 'Address',
                    'type': 'Reference',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active'],
                    'relationshipLabelPattern': '{AddressType}',
                    'attributeOrdering': {
                        'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                        'orderType': 'ASC',
                        'orderingStrategy': 'FieldBased'
                    },
                    'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                    'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/affiliatedwith',
                    'label': 'Affiliated with',
                    'name': 'affiliatedwith',
                    'type': 'Reference',
                    'hidden': true,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'referencedAttributeURIs': ['configuration/entityTypes/HCO/attributes/Name', 'configuration/entityTypes/HCO/attributes/HealthSystemName'],
                    'relationshipLabelPattern': '{Name} - {HealthSystemName}',
                    'attributeOrdering': {'orderingStrategy': 'LUD'},
                    'referencedEntityTypeURI': 'configuration/entityTypes/HCO',
                    'relationshipTypeURI': 'configuration/relationTypes/affiliatedwith'
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Gender',
                    'label': 'Gender',
                    'name': 'Gender',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'GENDER_CD',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                    'label': 'Prof Designation',
                    'name': 'ProfDesignation',
                    'description': 'String',
                    'type': 'String',
                    'lookupCode': 'PROF_DESG',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/AbvBestSpecHCP',
                    'label': 'ABS Code',
                    'name': 'AbvBestSpecHCP',
                    'description': 'String',
                    'type': 'String',
                    'lookupCode': 'ABV_HCP_BST_SPEC_CODE',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/AbvBestSpecDescHCP',
                    'label': 'ABS Desc',
                    'name': 'AbvBestSpecDescHCP',
                    'description': 'String',
                    'type': 'String',
                    'lookupCode': 'ABV_HCP_BST_SPEC_DESC',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ImsXponentID',
                    'label': 'Bridge ID',
                    'name': 'ImsXponentID',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Specialities',
                    'label': 'Speciality',
                    'name': 'Specialities',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                    'faceted': false,
                    'searchable': false,
                    'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Specialities/attributes/SpecialtyType',
                        'label': 'Specialty Type',
                        'name': 'SpecialtyType',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'SPEC_TYPE',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Specialities/attributes/Specialty',
                        'label': 'Specialty',
                        'name': 'Specialty',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'SPEC',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Specialities/attributes/Desc',
                        'label': 'Description',
                        'name': 'Desc',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Specialities/attributes/Group',
                        'label': 'Group',
                        'name': 'Group',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Specialities/attributes/SourceCD',
                        'label': 'Source CD',
                        'name': 'SourceCD',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ProfType',
                    'label': 'Prof Type',
                    'name': 'ProfType',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'PROF_TYPE',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/StandardJobTitle',
                    'label': 'Prescriber Type',
                    'name': 'StandardJobTitle',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences',
                    'label': 'Privacy Prefs',
                    'name': 'PrivacyPreferences',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': 'Contact Opt Out: {AMANoContact}',
                    'singleValue': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/AMANoContact',
                        'label': 'Contact Opt Out',
                        'name': 'AMANoContact',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/PDRP',
                        'label': 'PDRP',
                        'name': 'PDRP',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/PDRPDate',
                        'label': 'PDRP Date',
                        'name': 'PDRPDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/OptOut',
                        'label': 'Opt Out',
                        'name': 'OptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/OptOutStartDate',
                        'label': 'Opt Out Start Date',
                        'name': 'OptOutStartDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'searchable': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/AllowedToContact',
                        'label': 'Allowed To Contact',
                        'name': 'AllowedToContact',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/PhoneOptOut',
                        'label': 'Phone Opt Out',
                        'name': 'PhoneOptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/EmailOptOut',
                        'label': 'Email Opt Out',
                        'name': 'EmailOptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/FaxOptOut',
                        'label': 'Fax Opt Out',
                        'name': 'FaxOptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/TextMessageOptOut',
                        'label': 'Text Message Opt Out',
                        'name': 'TextMessageOptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/PrivacyPreferences/attributes/MailOptOut',
                        'label': 'Mail Opt Out',
                        'name': 'MailOptOut',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Prefix',
                    'label': 'Prefix',
                    'name': 'Prefix',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/SuffixName',
                    'label': 'Suffix Name',
                    'name': 'SuffixName',
                    'description': 'Generation Suffix',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Name',
                    'label': 'Full Name',
                    'name': 'Name',
                    'description': 'Name',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Credentials',
                    'label': 'Credential',
                    'name': 'Credentials',
                    'description': '',
                    'type': 'String',
                    'lookupCode': '',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Employment',
                    'label': 'Employment',
                    'name': 'Employment',
                    'type': 'Reference',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'referencedAttributeURIs': ['configuration/entityTypes/Organization/attributes/Name', 'configuration/relationTypes/Employment/attributes/Title', 'configuration/relationTypes/Employment/attributes/Summary', 'configuration/relationTypes/Employment/attributes/IsCurrent'],
                    'relationshipLabelPattern': '{Title}',
                    'attributeOrdering': {'orderingStrategy': 'LUD'},
                    'referencedEntityTypeURI': 'configuration/entityTypes/Organization',
                    'relationshipTypeURI': 'configuration/relationTypes/Employment'
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Administrator',
                    'label': 'Administrator',
                    'name': 'Administrator',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Research',
                    'label': 'Research',
                    'name': 'Research',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ClinicalTrials',
                    'label': 'Clinical Trials',
                    'name': 'ClinicalTrials',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/DEABusinessActivity',
                    'label': 'DEA Business Activity',
                    'name': 'DEABusinessActivity',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/StatusReasonCode',
                    'label': 'StatusReasonCode',
                    'name': 'StatusReasonCode',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'StatusReasonCode',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Group',
                    'label': 'Group',
                    'name': 'Group',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/TaxID',
                    'label': 'Tax ID',
                    'name': 'TaxID',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/StatusUpdateDate',
                    'label': 'Status Update Date',
                    'name': 'StatusUpdateDate',
                    'description': '',
                    'type': 'Date',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/SSN',
                    'label': 'SSN',
                    'name': 'SSN',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/SSNLast4',
                    'label': 'SSN Last 4',
                    'name': 'SSNLast4',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ME',
                    'label': 'ME',
                    'name': 'ME',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/NPI',
                    'label': 'NPI',
                    'name': 'NPI',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/UPIN',
                    'label': 'UPIN',
                    'name': 'UPIN',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Sanction',
                    'label': 'Sanction',
                    'name': 'Sanction',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{SanctionId}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/SanctionId',
                        'label': 'SanctionId',
                        'name': 'SanctionId',
                        'description': 'Court sanction Id for any case.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/ActionCode',
                        'label': 'ActionCode',
                        'name': 'ActionCode',
                        'description': 'Court sanction code for a case',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/ActionDescription',
                        'label': 'Action Description',
                        'name': 'ActionDescription',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/BoardCode',
                        'label': 'Board Code',
                        'name': 'BoardCode',
                        'description': 'Court case board id',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/BoardDesc',
                        'label': 'Board Desc',
                        'name': 'BoardDesc',
                        'description': 'court case board description ',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/ActionDate',
                        'label': 'Action Date',
                        'name': 'ActionDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/SanctionPeriodStartDate',
                        'label': 'Sanction Paeriod Start Date',
                        'name': 'SanctionPeriodStartDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/SanctionPeriodEndDate',
                        'label': 'Sanction Period End Date',
                        'name': 'SanctionPeriodEndDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/MonthDuration',
                        'label': 'Month Duration',
                        'name': 'MonthDuration',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/FineAmount',
                        'label': 'Fine Amount',
                        'name': 'FineAmount',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/OffenseCode',
                        'label': 'Offense Code',
                        'name': 'OffenseCode',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/OffenseDescription',
                        'label': 'Offense Description',
                        'name': 'OffenseDescription',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Sanction/attributes/OffenseDate',
                        'label': 'Offense Date',
                        'name': 'OffenseDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Certificates',
                    'label': 'Certificates',
                    'name': 'Certificates',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{Name} - {CertificateId}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/CertificateId',
                        'label': 'Certificate Id',
                        'name': 'CertificateId',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/BoardId',
                        'label': 'Board Id',
                        'name': 'BoardId',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/BoardName',
                        'label': 'Board Name',
                        'name': 'BoardName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/InternalHCPStatus',
                        'label': 'Internal HCP Status',
                        'name': 'InternalHCPStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/InternalHCPInactiveReasonCode',
                        'label': 'Internal HCP Inactive Reason Code',
                        'name': 'InternalHCPInactiveReasonCode',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/InternalSamplingStatus',
                        'label': 'Internal Sampling Status',
                        'name': 'InternalSamplingStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Certificates/attributes/PVSEligibilty',
                        'label': 'PVS Eligibilty',
                        'name': 'PVSEligibilty',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Credential',
                    'label': 'Credential',
                    'name': 'Credential',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{Credential}',
                    'matchFieldURIs': ['configuration/entityTypes/Individual/attributes/Credential/attributes/Credential'],
                    'attributeOrdering': {
                        'fieldURI': 'configuration/entityTypes/HCP/attributes/Credential/attributes/Rank',
                        'orderType': 'ASC',
                        'orderingStrategy': 'FieldBased'
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Credential/attributes/Rank',
                        'label': 'Rank',
                        'name': 'Rank',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Credential/attributes/Credential',
                        'label': 'Credential',
                        'name': 'Credential',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'CRED',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/BirthCountry',
                    'label': 'Birth Country',
                    'name': 'BirthCountry',
                    'description': 'Birth Country',
                    'type': 'String',
                    'lookupCode': 'COUNTRY_CD',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/MajorProfessionalActivity',
                    'label': 'Major Prof Activity',
                    'name': 'MajorProfessionalActivity',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'MPA_CD',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/PresentEmployment',
                    'label': 'Present Employ',
                    'name': 'PresentEmployment',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'PE_CD',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/TypeOfPractice',
                    'label': 'Type Of Practice',
                    'name': 'TypeOfPractice',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'TOP_CD',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/PresumedDead',
                    'label': 'Presumed Dead',
                    'name': 'PresumedDead',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': true,
                    'important': false,
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/License',
                    'label': 'License',
                    'name': 'License',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{BoardCode} {Number}, state {State}',
                    'matchFieldURIs': ['configuration/entityTypes/HCP/attributes/License/attributes/State', 'configuration/entityTypes/HCP/attributes/License/attributes/Number'],
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/Category',
                        'label': 'Category',
                        'name': 'Category',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/Number',
                        'label': 'Number',
                        'name': 'Number',
                        'description': 'State License INTEGER. A unique license INTEGER is listed for each license the physician holds. There is no standard format syntax. Format examples: 18986, 4301079019, BX1464089. There is also no limit to the INTEGER of licenses a physician can hold in a state. Example: A physician can have an inactive resident license plus unlimited active licenses. Residents can have as many as four licenses since some states issue licenses every year',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/BoardCode',
                        'label': 'Board Code',
                        'name': 'BoardCode',
                        'description': 'State License Board Code. For AMA The board code will always be AMA',
                        'type': 'String',
                        'lookupCode': 'STLIC_BRD_CD_LOV',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/State',
                        'label': 'State',
                        'name': 'State',
                        'description': 'State License State. Two character field. USPS standard abbreviations.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/Degree',
                        'label': 'Degree',
                        'name': 'Degree',
                        'description': 'State License Degree. A physician may hold more than one license in a given state. However, not more than one MD or more than one DO license in the same state.',
                        'type': 'String',
                        'lookupCode': 'DEGREE',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': 'State License Type. U = Unlimited there is no restriction on the physician to practice medicine; L = Limited implies restrictions of some sort. For example, the physician may practice only in a given county, admit patients only to particular hospitals, or practice under the supervision of a physician with a license in state or private hospitals or other settings;  T = Temporary issued to a physician temporarily practicing in an underserved area outside his/her state of licensure. Also granted between board meetings when new licenses are issued. Time span for a temporary license varies from state to state. Temporary licenses typically expire 6-9 months from the date they are issued; R = Resident License granted to a physician in graduate medical education (e.g., residency training).',
                        'type': 'String',
                        'lookupCode': 'ST_LIC_TYPE',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': 'State License Status. A = Active.  Physician is licensed to practice within the state; I = Inactive. If the physician has not reregistered a state license OR if the license has been suspended or revoked by the state board;  X = unknown. If the state has not provided current information Note: Some state boards issue inactive licenses to physicians who want to maintain licensure in the state although they are currently practicing in another state.',
                        'type': 'String',
                        'lookupCode': 'ST_LIC_STATUS',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/ExpiryDate',
                        'label': 'Expiry Date',
                        'name': 'ExpiryDate',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/ExpirationDate',
                        'label': 'Expiration Date',
                        'name': 'ExpirationDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/IssueDate',
                        'label': 'Issue Date',
                        'name': 'IssueDate',
                        'description': 'State License Issue Date',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/BrdDate',
                        'label': 'Board Date',
                        'name': 'BrdDate',
                        'description': 'State License as of date or pull date. The as of date (or stamp date) is the date the current license file is provided to the Database Licensees.',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/SampleEligibility',
                        'label': 'Sample Eligibility',
                        'name': 'SampleEligibility',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/License/attributes/SourceCD',
                        'label': 'Source CD',
                        'name': 'SourceCD',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Degrees',
                    'label': 'Degree',
                    'name': 'Degrees',
                    'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{Degree}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Degrees/attributes/Degree',
                        'label': 'Degree',
                        'name': 'Degree',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'lookupCode': 'DEGREE',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Degrees/attributes/BestDegree',
                        'label': 'Best Degree',
                        'name': 'BestDegree',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Speaker',
                    'label': 'Speaker',
                    'name': 'Speaker',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{Manufacturer} (speaker - {IsSpeaker})',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Speaker/attributes/IsSpeaker',
                        'label': 'Is Speaker',
                        'name': 'IsSpeaker',
                        'description': '',
                        'type': 'Boolean',
                        'lookupCode': '',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Speaker/attributes/Manufacturer',
                        'label': 'Manufacturer',
                        'name': 'Manufacturer',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Speaker/attributes/Brand',
                        'label': 'Brand',
                        'name': 'Brand',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Email',
                    'label': 'Email',
                    'name': 'Email',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{Email} {Type}',
                    'attributeOrdering': {
                        'fieldURI': 'configuration/entityTypes/HCP/attributes/Email/attributes/Rank',
                        'orderType': 'ASC',
                        'orderingStrategy': 'FieldBased'
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'values': ['Home', 'Work', 'Other'],
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Domain',
                        'label': 'Domain',
                        'name': 'Domain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/DomainType',
                        'label': 'Domain Type',
                        'name': 'DomainType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Username',
                        'label': 'Username',
                        'name': 'Username',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Rank',
                        'label': 'Rank',
                        'name': 'Rank',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/ValidationStatus',
                        'label': 'Validation Status',
                        'name': 'ValidationStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/Active',
                        'label': 'Active',
                        'name': 'Active',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Email/attributes/SourceCD',
                        'label': 'Source CD',
                        'name': 'SourceCD',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Education',
                    'label': 'Education',
                    'name': 'Education',
                    'type': 'Nested',
                    'hidden': true,
                    'important': false,
                    'dataLabelPattern': '{SchoolName} {Degree}, {EducationEndYear}',
                    'faceted': false,
                    'searchable': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/SchoolName',
                        'label': 'School Name',
                        'name': 'SchoolName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/Degree',
                        'label': 'Degree',
                        'name': 'Degree',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/YearOfGraduation',
                        'label': 'Year of graduation',
                        'name': 'YearOfGraduation',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/MyTimeStamp',
                        'label': 'My Timestamp',
                        'name': 'MyTimeStamp',
                        'description': '',
                        'type': 'TimeStamp',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/Graduated',
                        'label': 'Graduated',
                        'name': 'Graduated',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/GPA',
                        'label': 'GPA',
                        'name': 'GPA',
                        'type': 'Number',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/YearsInProgram',
                        'label': 'Years In Program',
                        'name': 'YearsInProgram',
                        'description': 'Year in Grad Training Program, Year in training in current program',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/StartYear',
                        'label': 'Start Year',
                        'name': 'StartYear',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/FieldofStudy',
                        'label': 'Field of Study',
                        'name': 'FieldofStudy',
                        'description': 'Specialty Focus or Specialty Training',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/Eligibility',
                        'label': 'Eligibility',
                        'name': 'Eligibility',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Education/attributes/EducationType',
                        'label': 'Type',
                        'name': 'EducationType',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/PreferredName',
                    'label': 'Preferred Name',
                    'name': 'PreferredName',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Nickname',
                    'label': 'Nickname',
                    'name': 'Nickname',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/DoB',
                    'label': 'Date of Birth',
                    'name': 'DoB',
                    'description': 'Date of Birth',
                    'type': 'Date',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/YoB',
                    'label': 'Year of Birth',
                    'name': 'YoB',
                    'description': 'Birth Year',
                    'type': 'Int',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/BirthCity',
                    'label': 'Birth City',
                    'name': 'BirthCity',
                    'description': 'Birth City',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/BirthState',
                    'label': 'Birth State',
                    'name': 'BirthState',
                    'description': 'Birth State',
                    'type': 'String',
                    'lookupCode': 'STATE_CODE',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/DoD',
                    'label': 'Date of Death',
                    'name': 'DoD',
                    'description': '',
                    'type': 'Date',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/YoD',
                    'label': 'Year of Death',
                    'name': 'YoD',
                    'description': '',
                    'type': 'Int',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/KaiserProvider',
                    'label': 'Kaiser Provider',
                    'name': 'KaiserProvider',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Solo',
                    'label': 'Sole Practitioner',
                    'name': 'Solo',
                    'description': '',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Phone',
                    'label': 'Phone',
                    'name': 'Phone',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{Number} {Type}',
                    'searchable': true,
                    'attributeOrdering': {
                        'fieldURI': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Rank',
                        'orderType': 'ASC',
                        'orderingStrategy': 'FieldBased'
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Number',
                        'label': 'Number',
                        'name': 'Number',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Extension',
                        'label': 'Extension',
                        'name': 'Extension',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Rank',
                        'label': 'Rank',
                        'name': 'Rank',
                        'description': '',
                        'type': 'Int',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/CountryCode',
                        'label': 'Country Code',
                        'name': 'CountryCode',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/AreaCode',
                        'label': 'Area Code',
                        'name': 'AreaCode',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/LocalNumber',
                        'label': 'Local Number',
                        'name': 'LocalNumber',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/ValidationStatus',
                        'label': 'Validation Status',
                        'name': 'ValidationStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/LineType',
                        'label': 'Line Type',
                        'name': 'LineType',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/FormatMask',
                        'label': 'Format Mask',
                        'name': 'FormatMask',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/DigitCount',
                        'label': 'Digit Count',
                        'name': 'DigitCount',
                        'description': '',
                        'type': 'Int',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/GeoArea',
                        'label': 'Geo Area',
                        'name': 'GeoArea',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/GeoCountry',
                        'label': 'Geo Country',
                        'name': 'GeoCountry',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Phone/attributes/Active',
                        'label': 'Active',
                        'name': 'Active',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/WebsiteURL',
                    'label': 'Website URL',
                    'name': 'WebsiteURL',
                    'type': 'URL',
                    'hidden': false,
                    'important': false,
                    'faceted': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Status',
                    'label': 'Status',
                    'name': 'Status',
                    'description': '',
                    'type': 'String',
                    'lookupCode': 'PROVIDER_STATUS_CD',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'values': ['New', 'Active', 'Deceased', 'Moved', 'Retired'],
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/ImageLinks',
                    'label': 'Image Link',
                    'name': 'ImageLinks',
                    'type': 'Image URL',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/DocumentLinks',
                    'label': 'Document Link',
                    'name': 'DocumentLinks',
                    'type': 'URL',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/VideoLinks',
                    'label': 'Video Link',
                    'name': 'VideoLinks',
                    'type': 'URL',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Description',
                    'label': 'Description',
                    'name': 'Description',
                    'description': '',
                    'type': 'Blob',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/FormerFirstName',
                    'label': 'Former First Name',
                    'name': 'FormerFirstName',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/FormerLastName',
                    'label': 'Former Last Name',
                    'name': 'FormerLastName',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/FormerMiddleName',
                    'label': 'Former Middle Name',
                    'name': 'FormerMiddleName',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/FormerSuffixName',
                    'label': 'Former Suffix Name',
                    'name': 'FormerSuffixName',
                    'description': '',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Identifiers',
                    'label': 'Identifiers',
                    'name': 'Identifiers',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{Type} {ID} {Status}',
                    'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                    'attributeOrdering': {
                        'fieldURI': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type',
                        'orderType': 'ASC',
                        'orderingStrategy': 'FieldBased'
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/ID',
                        'label': 'ID',
                        'name': 'ID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/DeactivationReasonCode',
                        'label': 'Deactivation Reason Code',
                        'name': 'DeactivationReasonCode',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/DeactivationDate',
                        'label': 'Deactivation Date',
                        'name': 'DeactivationDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/ReactivationDate',
                        'label': 'Reactivation Date',
                        'name': 'ReactivationDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/GSASanction',
                    'label': 'GSA Sanction',
                    'name': 'GSASanction',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{Agency} {ActionDate}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/SanctionId',
                        'label': 'Sanction ID',
                        'name': 'SanctionId',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/FirstName',
                        'label': 'First Name',
                        'name': 'FirstName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/MiddleName',
                        'label': 'Middle Name',
                        'name': 'MiddleName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/LastName',
                        'label': 'Last Name',
                        'name': 'LastName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/SuffixName',
                        'label': 'Suffix Name',
                        'name': 'SuffixName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/City',
                        'label': 'City',
                        'name': 'City',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/State',
                        'label': 'State',
                        'name': 'State',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/Zip',
                        'label': 'Zip',
                        'name': 'Zip',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/ActionDate',
                        'label': 'Action Date',
                        'name': 'ActionDate',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/TermDate',
                        'label': 'Term Date',
                        'name': 'TermDate',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/Agency',
                        'label': 'Agency',
                        'name': 'Agency',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/GSASanction/attributes/Confidence',
                        'label': 'Confidence',
                        'name': 'Confidence',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/MiddleInitial',
                    'label': 'Middle Initial',
                    'name': 'MiddleInitial',
                    'description': 'Middle Initial. This attribute is populated from Middle Name',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy',
                    'label': 'Taxonomy',
                    'name': 'Taxonomy',
                    'description': '',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{Taxonomy}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'TAXONOMY_CD',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'TAXONOMY_TYPE',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/ProviderType',
                        'label': 'Provider Type',
                        'name': 'ProviderType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/Classification',
                        'label': 'Classification',
                        'name': 'Classification',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/Specialization',
                        'label': 'Specialization',
                        'name': 'Specialization',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCP/attributes/Taxonomy/attributes/Priority',
                        'label': 'Priority',
                        'name': 'Priority',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'TAXONOMY_PRIORITY',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }],
                'defaultFacetedAttributes': ['configuration/entityTypes/HCP/attributes/affiliatedwith', 'configuration/entityTypes/HCP/attributes/Credentials', 'configuration/entityTypes/HCP/attributes/Address'],
                'defaultSearchAttributes': ['configuration/entityTypes/HCP/attributes/FirstName', 'configuration/entityTypes/HCP/attributes/LastName', 'configuration/entityTypes/HCP/attributes/affiliatedwith', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5'],
                'abstract': false,
                'entityTypeRoleURIs': ['configuration/roles/Physician', 'configuration/roles/PhysicianAssistant', 'configuration/roles/BillingManager', 'configuration/roles/OfficeStaff', 'configuration/roles/NurseCaseManager', 'configuration/roles/NursePractitioner', 'configuration/roles/PracticeManager', 'configuration/roles/PurchasingManager', 'configuration/roles/RegisteredNurse', 'configuration/roles/Administration', 'configuration/roles/CRA', 'configuration/roles/DirectorofNursing', 'configuration/roles/DirectorofPharmacy', 'configuration/roles/Fellows', 'configuration/roles/LabDirectors', 'configuration/roles/MedicalAssistant', 'configuration/roles/OfficeManager', 'configuration/roles/Other', 'configuration/roles/Pharmacist', 'configuration/roles/PharmacyTechnician', 'configuration/roles/Resident', 'configuration/roles/SocialWorker', 'configuration/roles/StaffDeveloper', 'configuration/roles/Technologist', 'configuration/roles/Therapist', 'configuration/roles/CaseManager'],
                'businessCardAttributeURIs': ['configuration/entityTypes/HCP/attributes/Employment'],
                'imageAttributeURIs': ['configuration/entityTypes/HCP/attributes/ImageLinks']
            },
                {
                    'uri': 'configuration/entityTypes/Organization',
                    'label': 'Organization',
                    'description': 'Organization entity type details',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'typeColor': '#80D557',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Organization/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Organization/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Organization/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Organization/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'typeIcon': 'images/base_type/org.png',
                    'typeImage': 'images/defaultImage/no-org.png',
                    'typeGraphIcon': 'images/graphIcon/organization-icon.png',
                    'entitySmartLogic': 'Organization',
                    'extendsTypeURI': 'configuration/entityTypes/Party',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Organization/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Ticker/attributes/Symbol',
                            'label': 'Symbol',
                            'name': 'Symbol',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Ticker/attributes/StockExchange',
                            'label': 'Stock Exchange',
                            'name': 'StockExchange',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Organization/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Organization/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': true,
                    'entityTypeRoleURIs': ['configuration/roles/Payer'],
                    'imageAttributeURIs': ['configuration/entityTypes/Organization/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/Company',
                    'label': 'Company',
                    'description': 'Company',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'searchable': false,
                    'typeColor': '#80D557',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Company/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Company/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Company/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Company/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Company/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Ticker/attributes/Symbol',
                            'label': 'Symbol',
                            'name': 'Symbol',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Ticker/attributes/StockExchange',
                            'label': 'Stock Exchange',
                            'name': 'StockExchange',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Company/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Company/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Company/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Company/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': true,
                    'imageAttributeURIs': ['configuration/entityTypes/Company/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/HCO',
                    'label': 'HCO',
                    'description': 'Health care provider',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{Address}',
                    'typeColor': '#80D557',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/HCO/survivorshipGroups/default',
                        'default': true,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/HCO/attributes/LegalBusinessName',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'Frequency'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/DoingBusinessAsName',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/HealthSystemName',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Name',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'Frequency'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Status',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Address',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Phone',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'LUD'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ABVType',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ABVSubType',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ABVABS',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ABVABSDesc',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/OwnershipStatus',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/CompanyType',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/AnnualStatistics',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ResidentCount',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Formulary',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/EMedicalRecord',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/EPrescribe',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/PayPerform',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/WebsiteURL',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/GenFirst',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ORSurgeries',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/TotalProcedures',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/ResidentProgram',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/SrepAccess',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/TotalSurgeries',
                            'sourcesUriOrder': ['configuration/sources/MOCA', 'configuration/sources/HCOS'],
                            'survivorshipStrategy': 'SRC_SYS'
                        }, {
                            'attribute': 'configuration/entityTypes/HCO/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/HCO/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/LegalBusinessName',
                        'label': 'Legal Business Name',
                        'name': 'LegalBusinessName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/HealthSystemName',
                        'label': 'Health System Name',
                        'name': 'HealthSystemName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ABVType',
                        'label': 'Type',
                        'name': 'ABVType',
                        'description': 'String',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ABVSubType',
                        'label': 'SubType',
                        'name': 'ABVSubType',
                        'description': 'String',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ABVABS',
                        'label': 'ABS Code',
                        'name': 'ABVABS',
                        'description': 'String',
                        'type': 'String',
                        'lookupCode': 'ABV_HCO_BST_SPEC_CODE',
                        'hidden': true,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ABVABSDesc',
                        'label': 'ABS Desc',
                        'name': 'ABVABSDesc',
                        'description': 'String',
                        'type': 'String',
                        'lookupCode': 'ABV_BST_SPEC_DESC',
                        'hidden': true,
                        'important': false,
                        'faceted': false,
                        'searchable': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/BedRange',
                        'label': 'Bed Range',
                        'name': 'BedRange',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TotalProcedures',
                        'label': 'Total Procedures',
                        'name': 'TotalProcedures',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TotalSurgeries',
                        'label': 'Total Surgeries',
                        'name': 'TotalSurgeries',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ORSurgeries',
                        'label': 'OR Surgeries',
                        'name': 'ORSurgeries',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ResidentProgram',
                        'label': 'Resident Program',
                        'name': 'ResidentProgram',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ResidentCount',
                        'label': 'Resident Count',
                        'name': 'ResidentCount',
                        'description': '',
                        'type': 'Number',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/GenFirst',
                        'label': 'Gen First',
                        'name': 'GenFirst',
                        'description': 'String',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Formulary',
                        'label': 'Formulary',
                        'name': 'Formulary',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/EMedicalRecord',
                        'label': 'e-Medical Record',
                        'name': 'EMedicalRecord',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/EPrescribe',
                        'label': 'e-Prescribe',
                        'name': 'EPrescribe',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/PayPerform',
                        'label': 'Pay Perform',
                        'name': 'PayPerform',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/SrepAccess',
                        'label': 'SREP Access',
                        'name': 'SrepAccess',
                        'description': 'String',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds',
                        'label': 'Total License Beds',
                        'name': 'TotalLicenseBeds',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{TotalLicenseBeds}',
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/TotalLicenseBeds',
                            'label': 'Total License Beds',
                            'name': 'TotalLicenseBeds',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Burn',
                            'label': 'Burn',
                            'name': 'Burn',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/CCU',
                            'label': 'CCU',
                            'name': 'CCU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/DetoxICU',
                            'label': 'Detox ICU',
                            'name': 'DetoxICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/ICU',
                            'label': 'ICU',
                            'name': 'ICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/MedSurg',
                            'label': 'MedSurg',
                            'name': 'MedSurg',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/NeoNatalICU',
                            'label': 'NeoNatal ICU',
                            'name': 'NeoNatalICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Nursery',
                            'label': 'Nursery',
                            'name': 'Nursery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/NursingFacility',
                            'label': 'Nursing Facility',
                            'name': 'NursingFacility',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Other',
                            'label': 'Other',
                            'name': 'Other',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/PediatricICU',
                            'label': 'Pediatric ICU',
                            'name': 'PediatricICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/PrematureICU',
                            'label': 'Premature ICU',
                            'name': 'PrematureICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Psychiatric',
                            'label': 'Psychiatric',
                            'name': 'Psychiatric',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/PsychICU',
                            'label': 'Psych ICU',
                            'name': 'PsychICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Rehabilitation',
                            'label': 'Rehabilitation',
                            'name': 'Rehabilitation',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/SICU',
                            'label': 'SICU',
                            'name': 'SICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/SkilledNursing',
                            'label': 'Skilled Nursing',
                            'name': 'SkilledNursing',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/SpecialCare',
                            'label': 'Special Care',
                            'name': 'SpecialCare',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/TraumaICU',
                            'label': 'Trauma ICU',
                            'name': 'TraumaICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/DevelopmentalDisability',
                            'label': 'Developmental Disability',
                            'name': 'DevelopmentalDisability',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/AssistedLiving',
                            'label': 'Assisted Living',
                            'name': 'AssistedLiving',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Hospice',
                            'label': 'Hospice',
                            'name': 'Hospice',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalLicenseBeds/attributes/Recovery',
                            'label': 'Recovery',
                            'name': 'Recovery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds',
                        'label': 'Total Census Beds',
                        'name': 'TotalCensusBeds',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{TotalCensusBeds}',
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/TotalCensusBeds',
                            'label': 'Total Census Beds',
                            'name': 'TotalCensusBeds',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Burn',
                            'label': 'Burn',
                            'name': 'Burn',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/CCU',
                            'label': 'CCU',
                            'name': 'CCU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/DetoxICU',
                            'label': 'Detox ICU',
                            'name': 'DetoxICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/ICU',
                            'label': 'ICU',
                            'name': 'ICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/MedSurg',
                            'label': 'MedSurg',
                            'name': 'MedSurg',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/NeoNatalICU',
                            'label': 'NeoNatal ICU',
                            'name': 'NeoNatalICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Nursery',
                            'label': 'Nursery',
                            'name': 'Nursery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/NursingFacility',
                            'label': 'Nursing Facility',
                            'name': 'NursingFacility',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Other',
                            'label': 'Other',
                            'name': 'Other',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/PediatricICU',
                            'label': 'Pediatric ICU',
                            'name': 'PediatricICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/PrematureICU',
                            'label': 'Premature ICU',
                            'name': 'PrematureICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Psychiatric',
                            'label': 'Psychiatric',
                            'name': 'Psychiatric',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/PsychICU',
                            'label': 'Psych ICU',
                            'name': 'PsychICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Rehabilitation',
                            'label': 'Rehabilitation',
                            'name': 'Rehabilitation',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/SICU',
                            'label': 'SICU',
                            'name': 'SICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/SkilledNursing',
                            'label': 'Skilled Nursing',
                            'name': 'SkilledNursing',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/SpecialCare',
                            'label': 'Special Care',
                            'name': 'SpecialCare',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/TraumaICU',
                            'label': 'Trauma ICU',
                            'name': 'TraumaICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/DevelopmentalDisability',
                            'label': 'Developmental Disability',
                            'name': 'DevelopmentalDisability',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/AssistedLiving',
                            'label': 'Assisted Living',
                            'name': 'AssistedLiving',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Hospice',
                            'label': 'Hospice',
                            'name': 'Hospice',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalCensusBeds/attributes/Recovery',
                            'label': 'Recovery',
                            'name': 'Recovery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds',
                        'label': 'Total Staffed Beds',
                        'name': 'TotalStaffedBeds',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{TotalStaffedBeds}',
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/TotalStaffedBeds',
                            'label': 'Total Staffed Beds',
                            'name': 'TotalStaffedBeds',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Burn',
                            'label': 'Burn',
                            'name': 'Burn',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/CCU',
                            'label': 'CCU',
                            'name': 'CCU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/DetoxICU',
                            'label': 'Detox ICU',
                            'name': 'DetoxICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/ICU',
                            'label': 'ICU',
                            'name': 'ICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/MedSurg',
                            'label': 'MedSurg',
                            'name': 'MedSurg',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/NeoNatalICU',
                            'label': 'NeoNatal ICU',
                            'name': 'NeoNatalICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Nursery',
                            'label': 'Nursery',
                            'name': 'Nursery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/NursingFacility',
                            'label': 'Nursing Facility',
                            'name': 'NursingFacility',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Other',
                            'label': 'Other',
                            'name': 'Other',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/PediatricICU',
                            'label': 'Pediatric ICU',
                            'name': 'PediatricICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/PrematureICU',
                            'label': 'Premature ICU',
                            'name': 'PrematureICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Psychiatric',
                            'label': 'Psychiatric',
                            'name': 'Psychiatric',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/PsychICU',
                            'label': 'Psych ICU',
                            'name': 'PsychICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Rehabilitation',
                            'label': 'Rehabilitation',
                            'name': 'Rehabilitation',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/SICU',
                            'label': 'SICU',
                            'name': 'SICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/SkilledNursing',
                            'label': 'Skilled Nursing',
                            'name': 'SkilledNursing',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/SpecialCare',
                            'label': 'Special Care',
                            'name': 'SpecialCare',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/TraumaICU',
                            'label': 'Trauma ICU',
                            'name': 'TraumaICU',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/DevelopmentalDisability',
                            'label': 'Developmental Disability',
                            'name': 'DevelopmentalDisability',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/AssistedLiving',
                            'label': 'Assisted Living',
                            'name': 'AssistedLiving',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Hospice',
                            'label': 'Hospice',
                            'name': 'Hospice',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/TotalStaffedBeds/attributes/Recovery',
                            'label': 'Recovery',
                            'name': 'Recovery',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics',
                        'label': 'Annual Statistics',
                        'name': 'AnnualStatistics',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': 'Year-Specific Info {Year}',
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/Year',
                            'label': 'Year',
                            'name': 'Year',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/InpatientRevenue',
                            'label': 'Inpatient Revenue',
                            'name': 'InpatientRevenue',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/OutpatientRevenue',
                            'label': 'Outpatient Revenue',
                            'name': 'OutpatientRevenue',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/TotalPatientRevenue',
                            'label': 'TotalPatient Revenue',
                            'name': 'TotalPatientRevenue',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/NetPatientRevenue',
                            'label': 'NetPatient Revenue',
                            'name': 'NetPatientRevenue',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/OperatingExpenses',
                            'label': 'Operating Expenses',
                            'name': 'OperatingExpenses',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/OtherExpenses',
                            'label': 'Other Expenses',
                            'name': 'OtherExpenses',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/TotalExpenses',
                            'label': 'Total Expenses',
                            'name': 'TotalExpenses',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/SupplyCost',
                            'label': 'Supply Cost',
                            'name': 'SupplyCost',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/DrugCost',
                            'label': 'Drug Cost',
                            'name': 'DrugCost',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/TotalSalaries',
                            'label': 'Total Salaries',
                            'name': 'TotalSalaries',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/NetIncomePerPatient',
                            'label': 'NetIncome PerPatient',
                            'name': 'NetIncomePerPatient',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/NetIncome',
                            'label': 'Net Income',
                            'name': 'NetIncome',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/CostPerBed',
                            'label': 'Cost Per Bed',
                            'name': 'CostPerBed',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/TotalAssets',
                            'label': 'Total Assets',
                            'name': 'TotalAssets',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/AnnualStatistics/attributes/NumberOfFTEs',
                            'label': 'Number Of FTEs',
                            'name': 'NumberOfFTEs',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Site',
                        'label': 'Site',
                        'name': 'Site',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'type': 'Int',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/GroupPractice',
                        'label': 'Group Practice',
                        'name': 'GroupPractice',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/VADOD',
                        'label': 'VA/DOD',
                        'name': 'VADOD',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/GPOMembership',
                        'label': 'GPO Membership',
                        'name': 'GPOMembership',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Academic',
                        'label': 'Academic',
                        'name': 'Academic',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/AcceptMedicare',
                        'label': 'Accept Medicare',
                        'name': 'AcceptMedicare',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/AcceptMedicaid',
                        'label': 'Accept Medicaid',
                        'name': 'AcceptMedicaid',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/PercentMedicare',
                        'label': 'Percent Medicare',
                        'name': 'PercentMedicare',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/PercentMedicaid',
                        'label': 'Percent Medicaid',
                        'name': 'PercentMedicaid',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ParentSatellite',
                        'label': 'Parent Satellite',
                        'name': 'ParentSatellite',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/NPI',
                        'label': 'NPI',
                        'name': 'NPI',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/HiN',
                        'label': 'HIN',
                        'name': 'HiN',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/DDD',
                        'label': 'DDD Number',
                        'name': 'DDD',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/PrimaryGPO',
                        'label': 'Primary GPO',
                        'name': 'PrimaryGPO',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Fiscal',
                        'label': 'Fiscal',
                        'name': 'Fiscal',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ClassofTrade',
                        'label': 'Class of Trade',
                        'name': 'ClassofTrade',
                        'description': 'DO NOT USE this attribute - use ClassofTradeN instead',
                        'type': 'String',
                        'lookupCode': 'CLASS_OF_TRADE',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/GroupPracticeID',
                        'label': 'Group Practice ID',
                        'name': 'GroupPracticeID',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/BusinessEntityType',
                        'label': 'Business Entity Type',
                        'name': 'BusinessEntityType',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/MktSegmentCode',
                        'label': 'Mkt Segment Code',
                        'name': 'MktSegmentCode',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/ClassofTradeN',
                        'label': 'IMS COT',
                        'name': 'ClassofTradeN',
                        'description': '',
                        'type': 'Nested',
                        'hidden': true,
                        'important': false,
                        'dataLabelPattern': '{Classification}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/ClassofTradeN/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/ClassofTradeN/attributes/FacilityType',
                            'label': 'Facility Type',
                            'name': 'FacilityType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/ClassofTradeN/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'type': 'Nested',
                        'hidden': true,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Ticker/attributes/Symbol',
                            'label': 'Symbol',
                            'name': 'Symbol',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Ticker/attributes/StockExchange',
                            'label': 'Stock Exchange',
                            'name': 'StockExchange',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Specialty',
                        'label': 'Specialty',
                        'name': 'Specialty',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Nested',
                        'hidden': true,
                        'important': false,
                        'dataLabelPattern': '{Specialty}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialty/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialty/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': true,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/HCO/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/CMSCoveredForTeaching',
                        'label': 'CMSCoveredForTeaching',
                        'name': 'CMSCoveredForTeaching',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion',
                        'label': 'GSA Exclusion',
                        'name': 'GSAExclusion',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{OrganizationName} {ActionDate}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/SanctionId',
                            'label': 'Sanction ID',
                            'name': 'SanctionId',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/OrganizationName',
                            'label': 'Organization Name',
                            'name': 'OrganizationName',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/AddressLine1',
                            'label': 'Address Line 1',
                            'name': 'AddressLine1',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/AddressLine2',
                            'label': 'Address Line 2',
                            'name': 'AddressLine2',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/City',
                            'label': 'City',
                            'name': 'City',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/State',
                            'label': 'State',
                            'name': 'State',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/Zip',
                            'label': 'Zip',
                            'name': 'Zip',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/ActionDate',
                            'label': 'Action Date',
                            'name': 'ActionDate',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/TermDate',
                            'label': 'Term Date',
                            'name': 'TermDate',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/Agency',
                            'label': 'Agency',
                            'name': 'Agency',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/GSAExclusion/attributes/Confidence',
                            'label': 'Confidence',
                            'name': 'Confidence',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion',
                        'label': 'OIG Exclusion',
                        'name': 'OIGExclusion',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{ActionDate} {ActionDescription}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/SanctionId',
                            'label': 'Sanction ID',
                            'name': 'SanctionId',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/ActionCode',
                            'label': 'Action Code',
                            'name': 'ActionCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/ActionDescription',
                            'label': 'Action Description',
                            'name': 'ActionDescription',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/BoardCode',
                            'label': 'Board Code',
                            'name': 'BoardCode',
                            'description': 'Court case board id',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/BoardDesc',
                            'label': 'Board Desc',
                            'name': 'BoardDesc',
                            'description': 'court case board description ',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/ActionDate',
                            'label': 'Offense Date',
                            'name': 'ActionDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/OffenseCode',
                            'label': 'Offense Code',
                            'name': 'OffenseCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/OIGExclusion/attributes/OffenseDescription',
                            'label': 'Offense Description',
                            'name': 'OffenseDescription',
                            'description': '',
                            'type': 'Blob',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/HCO/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'defaultFacetedAttributes': ['configuration/entityTypes/HCO/attributes/HealthSystemName', 'configuration/entityTypes/HCO/attributes/ABVType', 'configuration/entityTypes/HCO/attributes/ABVSubType', 'configuration/entityTypes/HCO/attributes/Address'],
                    'defaultSearchAttributes': ['configuration/entityTypes/HCO/attributes/Name', 'configuration/entityTypes/HCO/attributes/HealthSystemName', 'configuration/entityTypes/HCO/attributes/ABVType', 'configuration/entityTypes/HCO/attributes/ABVSubType', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5'],
                    'abstract': false,
                    'entityTypeRoleURIs': ['configuration/roles/Home', 'configuration/roles/Hospital', 'configuration/roles/LongTermCare', 'configuration/roles/MailOrderPharmacy', 'configuration/roles/PBM', 'configuration/roles/Pharmacy', 'configuration/roles/PHSCE', 'configuration/roles/SPPH', 'configuration/roles/VeteransAdmin', 'configuration/roles/Wholesaler', 'configuration/roles/TeachingHospital', 'configuration/roles/GroupPurchasingOrg', 'configuration/roles/AccountableCareOrg', 'configuration/roles/GroupPractice', 'configuration/roles/ManagedCareOrg', 'configuration/roles/IDN', 'configuration/roles/QualifiedRecipient', 'configuration/roles/Hospice', 'configuration/roles/ER', 'configuration/roles/Lab', 'configuration/roles/PrivatePractice', 'configuration/roles/Surgicenter', 'configuration/roles/Department', 'configuration/roles/ACTFSP', 'configuration/roles/BoardandCare', 'configuration/roles/CMHC', 'configuration/roles/HospitalOutpatient', 'configuration/roles/IMD', 'configuration/roles/Military', 'configuration/roles/Non-RetailPharmacy', 'configuration/roles/PartialProgram', 'configuration/roles/PhysicianOffice', 'configuration/roles/SkilledNursingFacility', 'configuration/roles/Clinic', 'configuration/roles/Office', 'configuration/roles/Other', 'configuration/roles/StateHospital'],
                    'imageAttributeURIs': ['configuration/entityTypes/HCO/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/Individual',
                    'label': 'Individual',
                    'description': 'Entity type representing person entity',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{FirstName} {LastName}, {Credentials}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'typeColor': '#99CCFF',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Individual/survivorshipGroups/default',
                        'default': true,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Individual/attributes/Employment',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Individual/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Individual/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Individual/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'typeIcon': 'images/base_type/Individual.png',
                    'typeImage': 'images/defaultImage/no-photo.png',
                    'typeGraphIcon': 'images/graphIcon/person-icon.png',
                    'entitySmartLogic': 'Individual',
                    'extendsTypeURI': 'configuration/entityTypes/Party',
                    'cleanseConfig': {
                        'infos': [{
                            'uri': 'configuration/entityTypes/Individual/cleanse/infos/default',
                            'useInCleansing': true,
                            'sequence': [{
                                'chain': [{
                                    'cleanseFunction': 'InitialsCleanser',
                                    'proceedOnSuccess': true,
                                    'proceedOnFailure': true,
                                    'mapping': {
                                        'inputMapping': [{
                                            'attribute': 'configuration/entityTypes/Individual/attributes/MiddleName',
                                            'cleanseAttribute': 'MiddleName',
                                            'mandatory': true,
                                            'allValues': false
                                        }],
                                        'outputMapping': [{
                                            'attribute': 'configuration/entityTypes/Individual/attributes/MiddleInitial',
                                            'cleanseAttribute': 'MiddleName',
                                            'mandatory': true,
                                            'allValues': false
                                        }]
                                    }
                                }]
                            }]
                        }]
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Individual/attributes/Credentials',
                        'label': 'Credential',
                        'name': 'Credentials',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'CRED',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Credential',
                        'label': 'Credential',
                        'name': 'Credential',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Credential}',
                        'matchFieldURIs': ['configuration/entityTypes/Individual/attributes/Credential/attributes/Credential'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Individual/attributes/Credential/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Credential/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Credential/attributes/Credential',
                            'label': 'Credential',
                            'name': 'Credential',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'CRED',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Prefix',
                        'label': 'Prefix',
                        'name': 'Prefix',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/FormerFirstName',
                        'label': 'Former First Name',
                        'name': 'FormerFirstName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/FormerLastName',
                        'label': 'Former Last Name',
                        'name': 'FormerLastName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/FormerMiddleName',
                        'label': 'Former Middle Name',
                        'name': 'FormerMiddleName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/FormerSuffixName',
                        'label': 'Former Suffix Name',
                        'name': 'FormerSuffixName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Name',
                        'label': 'Full Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/LastName',
                        'label': 'Last Name',
                        'name': 'LastName',
                        'description': 'Last Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/FirstName',
                        'label': 'First Name',
                        'name': 'FirstName',
                        'description': 'First Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/MiddleName',
                        'label': 'Middle Name',
                        'name': 'MiddleName',
                        'description': 'Middle Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/SuffixName',
                        'label': 'Suffix Name',
                        'name': 'SuffixName',
                        'description': 'Generation Suffix',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/PreferredName',
                        'label': 'Preferred Name',
                        'name': 'PreferredName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Nickname',
                        'label': 'Nickname',
                        'name': 'Nickname',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Title',
                        'label': 'Title',
                        'name': 'Title',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Gender',
                        'label': 'Gender',
                        'name': 'Gender',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/SSN',
                        'label': 'SSN',
                        'name': 'SSN',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/SSNLast4',
                        'label': 'SSN Last 4',
                        'name': 'SSNLast4',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/DoB',
                        'label': 'Date of Birth',
                        'name': 'DoB',
                        'description': 'Date of Birth',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/YoB',
                        'label': 'Year of Birth',
                        'name': 'YoB',
                        'description': 'Birth Year',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/BirthCity',
                        'label': 'Birth City',
                        'name': 'BirthCity',
                        'description': 'Birth City',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/BirthState',
                        'label': 'Birth State',
                        'name': 'BirthState',
                        'description': 'Birth State',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/BirthCountry',
                        'label': 'Birth Country',
                        'name': 'BirthCountry',
                        'description': 'Birth Country',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/DoD',
                        'label': 'Date of Death',
                        'name': 'DoD',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/YoD',
                        'label': 'Year of Death',
                        'name': 'YoD',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Individual/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences',
                        'label': 'Privacy Prefs',
                        'name': 'PrivacyPreferences',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': 'Phone: {PhoneOptOut}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/OptOut',
                            'label': 'Opt Out',
                            'name': 'OptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/OptOutStartDate',
                            'label': 'Opt Out Start Date',
                            'name': 'OptOutStartDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/AllowedToContact',
                            'label': 'Allowed To Contact',
                            'name': 'AllowedToContact',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/PhoneOptOut',
                            'label': 'Phone Opt Out',
                            'name': 'PhoneOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/EmailOptOut',
                            'label': 'Email Opt Out',
                            'name': 'EmailOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/FaxOptOut',
                            'label': 'Fax Opt Out',
                            'name': 'FaxOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/TextMessageOptOut',
                            'label': 'Text Message Opt Out',
                            'name': 'TextMessageOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/PrivacyPreferences/attributes/MailOptOut',
                            'label': 'Mail Opt Out',
                            'name': 'MailOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Education',
                        'label': 'Education',
                        'name': 'Education',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SchoolName} {Degree}, {EducationEndYear}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/SchoolName',
                            'label': 'School Name',
                            'name': 'SchoolName',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/Degree',
                            'label': 'Degree',
                            'name': 'Degree',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/YearOfGraduation',
                            'label': 'Year of graduation',
                            'name': 'YearOfGraduation',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/Graduated',
                            'label': 'Graduated',
                            'name': 'Graduated',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/GPA',
                            'label': 'GPA',
                            'name': 'GPA',
                            'type': 'Number',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/YearsInProgram',
                            'label': 'Years In Program',
                            'name': 'YearsInProgram',
                            'description': 'Year in Grad Training Program, Year in training in current program',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/StartYear',
                            'label': 'Start Year',
                            'name': 'StartYear',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/EndYear',
                            'label': 'End Year',
                            'name': 'EndYear',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/FieldofStudy',
                            'label': 'Field of Study',
                            'name': 'FieldofStudy',
                            'description': 'Specialty Focus or Specialty Training',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/Eligibility',
                            'label': 'Eligibility',
                            'name': 'Eligibility',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Education/attributes/EducationType',
                            'label': 'Type',
                            'name': 'EducationType',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Employment',
                        'label': 'Employment',
                        'name': 'Employment',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/entityTypes/Organization/attributes/Name', 'configuration/relationTypes/Employment/attributes/Title', 'configuration/relationTypes/Employment/attributes/Summary', 'configuration/relationTypes/Employment/attributes/IsCurrent'],
                        'relationshipLabelPattern': '{Title}',
                        'attributeOrdering': {'orderingStrategy': 'LUD'},
                        'referencedEntityTypeURI': 'configuration/entityTypes/Organization',
                        'relationshipTypeURI': 'configuration/relationTypes/Employment'
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/affiliatedwith',
                        'label': 'Affiliated with',
                        'name': 'affiliatedwith',
                        'type': 'Reference',
                        'hidden': true,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/entityTypes/Organization/attributes/Name', 'configuration/relationTypes/affiliatedwith/attributes/Title', 'configuration/relationTypes/affiliatedwith/attributes/PrefOrActive', 'configuration/relationTypes/affiliatedwith/attributes/Rank'],
                        'relationshipLabelPattern': '{Title}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/affiliatedwith/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Organization',
                        'relationshipTypeURI': 'configuration/relationTypes/affiliatedwith'
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['New', 'Active', 'Deceased', 'Moved', 'Retired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/PresumedDead',
                        'label': 'Presumed Dead',
                        'name': 'PresumedDead',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/MiddleInitial',
                        'label': 'Middle Initial',
                        'name': 'MiddleInitial',
                        'description': 'Middle Initial. This attribute is populated from Middle Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Individual/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': true,
                    'entityTypeRoleURIs': ['configuration/roles/Employee', 'configuration/roles/Patient'],
                    'imageAttributeURIs': ['configuration/entityTypes/Individual/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/Location',
                    'label': 'Location',
                    'description': 'Location',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{AddressLine1} {City} {StateProvince} {Zip.Zip5}',
                    'dataTooltipPattern': 'Tooltip {VerificationStatus}',
                    'searchable': false,
                    'typeColor': '#FF9900',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Location/survivorshipGroups/default',
                        'default': true,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Location/attributes/AddressLine1',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }, {
                            'attribute': 'configuration/entityTypes/Location/attributes/AddressLine2',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }, {
                            'attribute': 'configuration/entityTypes/Location/attributes/City',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }, {
                            'attribute': 'configuration/entityTypes/Location/attributes/State',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }, {
                            'attribute': 'configuration/entityTypes/Location/attributes/Country',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }, {
                            'attribute': 'configuration/entityTypes/Location/attributes/Zip',
                            'survivorshipStrategy': 'CleanserWinsStrategy'
                        }]
                    }],
                    'typeIcon': 'images/base_type/house.png',
                    'typeImage': 'images/defaultImage/no-loc.png',
                    'typeGraphIcon': 'images/graphIcon/location-icon.png',
                    'matchGroups': [{
                        'uri': 'configuration/entityTypes/Location/matchGroups/AutoMatch',
                        'label': 'Automatic on load Addresses match',
                        'type': 'automatic',
                        'useOvOnly': 'true',
                        'rule': {
                            'exact': ['configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5', 'configuration/entityTypes/Location/attributes/Country'],
                            'exactOrAllNull': ['configuration/entityTypes/Location/attributes/AddressLine2'],
                            'ignoreInToken': ['configuration/entityTypes/Location/attributes/AddressLine2']
                        }
                    }],
                    'cleanseConfig': {
                        'mappings': [{
                            'uri': 'configuration/entityTypes/Location/cleanse/mappings/address',
                            'outputMapping': [{
                                'attribute': 'configuration/entityTypes/Location/attributes/VerificationStatus',
                                'cleanseAttribute': 'VerificationStatus',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/VerificationStatusDetails',
                                'cleanseAttribute': 'VerificationStatusDetails',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/StateProvince',
                                'cleanseAttribute': 'AdministrativeArea',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/City',
                                'cleanseAttribute': 'Locality',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/SuperAdministrativeArea',
                                'cleanseAttribute': 'SuperAdministrativeArea',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/DoubleDependentLocality',
                                'cleanseAttribute': 'DoubleDependentLocality',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Neighborhood',
                                'cleanseAttribute': 'DependentLocality',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/DependentThoroughfare',
                                'cleanseAttribute': 'DependentThoroughfare',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Country',
                                'cleanseAttribute': 'CountryName',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                'cleanseAttribute': 'PostalCodePrimary',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4',
                                'cleanseAttribute': 'PostalCodeSecondary',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                                'cleanseAttribute': 'Latitude',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude',
                                'cleanseAttribute': 'Longitude',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/GeoAccuracy',
                                'cleanseAttribute': 'GeoAccuracy',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/POBox',
                                'cleanseAttribute': 'PostBox',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Telephone',
                                'cleanseAttribute': 'Telephone',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/SubAdministrativeArea',
                                'cleanseAttribute': 'SubAdministrativeArea',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Building',
                                'cleanseAttribute': 'Building',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/SubBuilding',
                                'cleanseAttribute': 'SubBuilding',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Organization',
                                'cleanseAttribute': 'Organization',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/DeliveryAddress1',
                                'cleanseAttribute': 'DeliveryAddress1',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/AddressLine1',
                                'cleanseAttribute': 'DeliveryAddress1',
                                'mandatory': true,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/AddressLine2',
                                'cleanseAttribute': 'DeliveryAddress2',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/ISO3166-2',
                                'cleanseAttribute': 'ISO3166-2',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/ISO3166-3',
                                'cleanseAttribute': 'ISO3166-3',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/DeliveryAddress',
                                'cleanseAttribute': 'DeliveryAddress',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/PremiseNumber',
                                'cleanseAttribute': 'PremiseNumber',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/ISO3166-N',
                                'cleanseAttribute': 'ISO3166-N',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/Unmatched',
                                'cleanseAttribute': 'Unmatched',
                                'mandatory': false,
                                'allValues': false
                            }, {
                                'attribute': 'configuration/entityTypes/Location/attributes/AVC',
                                'cleanseAttribute': 'AVC',
                                'mandatory': false,
                                'allValues': false
                            }]
                        }],
                        'infos': [{
                            'uri': 'configuration/entityTypes/Location/cleanse/infos/default',
                            'useInCleansing': true,
                            'sequence': [{
                                'chain': [{
                                    'cleanseFunction': 'Loqate',
                                    'resultingValuesSourceTypeUri': 'configuration/sources/ReltioCleanser',
                                    'proceedOnSuccess': true,
                                    'proceedOnFailure': false,
                                    'mapping': {
                                        'inputMapping': [{
                                            'attribute': 'configuration/entityTypes/Location/attributes/AddressInput',
                                            'cleanseAttribute': 'Address',
                                            'mandatory': true,
                                            'allValues': false
                                        }],
                                        'outputMappingRef': 'configuration/entityTypes/Location/cleanse/mappings/address/outputMapping'
                                    }
                                }]
                            }]
                        }, {
                            'uri': 'configuration/entityTypes/Location/cleanse/infos/other',
                            'useInCleansing': true,
                            'sequence': [{
                                'chain': [{
                                    'cleanseFunction': 'Loqate',
                                    'resultingValuesSourceTypeUri': 'configuration/sources/ReltioCleanser',
                                    'proceedOnSuccess': true,
                                    'proceedOnFailure': false,
                                    'mapping': {
                                        'inputMapping': [{
                                            'attribute': 'configuration/entityTypes/Location/attributes/AddressLine1',
                                            'cleanseAttribute': 'Address1',
                                            'mandatory': true,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/AddressLine2',
                                            'cleanseAttribute': 'Address2',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Country',
                                            'cleanseAttribute': 'Country',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/SuperAdministrativeArea',
                                            'cleanseAttribute': 'SuperAdministrativeArea',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/StateProvince',
                                            'cleanseAttribute': 'AdministrativeArea',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/SubAdministrativeArea',
                                            'cleanseAttribute': 'SubAdministrativeArea',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/City',
                                            'cleanseAttribute': 'Locality',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Neighborhood',
                                            'cleanseAttribute': 'DependentLocality',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/DoubleDependentLocality',
                                            'cleanseAttribute': 'DoubleDependentLocality',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Street',
                                            'cleanseAttribute': 'Thoroughfare',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/DependentThoroughfare',
                                            'cleanseAttribute': 'DependentThoroughfare',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Building',
                                            'cleanseAttribute': 'Building',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/SubBuilding',
                                            'cleanseAttribute': 'SubBuilding',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Premise',
                                            'cleanseAttribute': 'Premise',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                            'cleanseAttribute': 'PostalCode',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/POBox',
                                            'cleanseAttribute': 'PostBox',
                                            'mandatory': false,
                                            'allValues': false
                                        }, {
                                            'attribute': 'configuration/entityTypes/Location/attributes/Organization',
                                            'cleanseAttribute': 'Organization',
                                            'mandatory': false,
                                            'allValues': false
                                        }],
                                        'outputMappingRef': 'configuration/entityTypes/Location/cleanse/mappings/address/outputMapping'
                                    }
                                }]
                            }]
                        }]
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Location/attributes/City',
                        'label': 'City',
                        'name': 'City',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'label': 'State',
                        'name': 'StateProvince',
                        'type': 'String',
                        'lookupCode': 'STATE_CODE',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/SubAdministrativeArea',
                        'label': 'County',
                        'name': 'SubAdministrativeArea',
                        'description': 'This field holds the smallest geographic data element within a country. For instance, USA County.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Zip',
                        'label': 'Zip',
                        'name': 'Zip',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Zip5}-{Zip4}',
                        'faceted': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                            'label': 'Zip5',
                            'name': 'Zip5',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/AddressInput',
                        'label': 'Address Input',
                        'name': 'AddressInput',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'label': 'Address Line 1',
                        'name': 'AddressLine1',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/AddressLine2',
                        'label': 'Address Line 2',
                        'name': 'AddressLine2',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Country',
                        'label': 'Country',
                        'name': 'Country',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/CountryName',
                        'label': 'Country Name',
                        'name': 'CountryName',
                        'description': 'DO NOT USE this field - use Country instead',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/ISO3166-2',
                        'label': 'ISO3166-2',
                        'name': 'ISO3166-2',
                        'description': 'This field holds the ISO 3166 2-character country code.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/ISO3166-3',
                        'label': 'ISO3166-3',
                        'name': 'ISO3166-3',
                        'description': 'This field holds the ISO 3166 3-character country code.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/ISO3166-N',
                        'label': 'ISO3166-N',
                        'name': 'ISO3166-N',
                        'description': 'This field holds the ISO 3166 N-digit numeric country code.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/SuperAdministrativeArea',
                        'label': 'Super Admin Area',
                        'name': 'SuperAdministrativeArea',
                        'description': 'This field holds the largest geographic data element within a country.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/AdministrativeArea',
                        'label': 'Admin Area',
                        'name': 'AdministrativeArea',
                        'description': 'This field holds the most common geographic data element within a country. For instance, USA State, and Canadian Province.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Street',
                        'label': 'Street',
                        'name': 'Street',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Unit',
                        'label': 'Unit',
                        'name': 'Unit',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{UnitName}-{UnitValue}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Location/attributes/Unit/attributes/UnitName',
                            'label': 'UnitName',
                            'name': 'UnitName',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Location/attributes/Unit/attributes/UnitValue',
                            'label': 'UnitValue',
                            'name': 'UnitValue',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Floor',
                        'label': 'Floor',
                        'name': 'Floor',
                        'description': 'N/A',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Building',
                        'label': 'Building',
                        'name': 'Building',
                        'description': 'N/A',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/POBox',
                        'label': 'PO Box',
                        'name': 'POBox',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Neighborhood',
                        'label': 'Neighborhood',
                        'name': 'Neighborhood',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Locality',
                        'label': 'Locality',
                        'name': 'Locality',
                        'description': 'This field holds the most common population center data element within a country. For instance, USA City, Canadian Municipality.',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/GeoLocation',
                        'label': 'GeoLocation',
                        'name': 'GeoLocation',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Latitude}, {Longitude}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                            'label': 'Latitude',
                            'name': 'Latitude',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude',
                            'label': 'Longitude',
                            'name': 'Longitude',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/GeoAccuracy',
                            'label': 'GeoAccuracy',
                            'name': 'GeoAccuracy',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/CBSACode',
                        'label': 'Core Based Statistical Area',
                        'name': 'CBSACode',
                        'description': 'Core Based Statistical Area',
                        'type': 'String',
                        'lookupCode': 'CBSA_CD',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/FIPSCountyCode',
                        'label': 'FIPS County Code',
                        'name': 'FIPSCountyCode',
                        'description': 'FIPS county Code',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/FIPSStateCode',
                        'label': 'FIPS State Code',
                        'name': 'FIPSStateCode',
                        'description': 'FIPS State Code',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Multiunit',
                        'label': 'Multiunit',
                        'name': 'Multiunit',
                        'description': 'MULTIUNIT. Y = Building has multiple units; N = Building does not have multiple units or has a mailroom; U = USPS does not have information for this location; H = AMS has flagged this location to contain a hospital.',
                        'type': 'String',
                        'lookupCode': 'MULTIUNIT_CD',
                        'hidden': false,
                        'important': false,
                        'values': ['Y', 'N', 'U', 'H'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/OrgUnit',
                        'label': 'Org Unit',
                        'name': 'OrgUnit',
                        'description': 'ORG_UNIT',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Dept',
                        'label': 'Dept',
                        'name': 'Dept',
                        'description': 'N/A',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/CASSFlag',
                        'label': 'CASS Flag',
                        'name': 'CASSFlag',
                        'description': 'CASS flag',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/CongressCode',
                        'label': 'Congress Code',
                        'name': 'CongressCode',
                        'description': 'Congress Code',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/CMRAFlag',
                        'label': 'CMRA Flag',
                        'name': 'CMRAFlag',
                        'description': 'CMRA Flag',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DPCFlag',
                        'label': 'DPC Flag',
                        'name': 'DPCFlag',
                        'description': 'DPC flag',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['A', 'B', 'E', 'G', 'R', 'S'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/StreetPOFlag',
                        'label': 'Street/PO',
                        'name': 'StreetPOFlag',
                        'description': 'Flag for PO vs. street address',
                        'type': 'String',
                        'lookupCode': 'STREET_TYPE_CD',
                        'hidden': false,
                        'important': false,
                        'values': ['P', 'S'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/InvalidUnit',
                        'label': 'Invalid Unit',
                        'name': 'InvalidUnit',
                        'description': 'Invalid Unit Flag',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/BuildingFirmName',
                        'label': 'Building Firm Name',
                        'name': 'BuildingFirmName',
                        'description': 'Name of the building or firm as maintained by the US Postal Service',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DPV',
                        'label': 'Delivery Point Validation',
                        'name': 'DPV',
                        'description': 'USPS delivery point validation. R = Range Check; C = Clerk; F = Formally Valid; V = DPV Valid',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['R', 'C', 'F', 'V'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/RDI',
                        'label': 'Residential Delivery Flag',
                        'name': 'RDI',
                        'description': 'USPS residential delivery flag. Y = Residential; N = Business; U = Unknown; = Non of the above',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Y', 'N', 'U', ''],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/VerificationStatus',
                        'label': 'Verification Status',
                        'name': 'VerificationStatus',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/VerificationStatusDetails',
                        'label': 'Verification Status Details',
                        'name': 'VerificationStatusDetails',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/AVC',
                        'label': 'AVC',
                        'name': 'AVC',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/PostalCodeExtension',
                        'label': 'Postal Code Extension',
                        'name': 'PostalCodeExtension',
                        'description': 'DO NOT USE this field - use Zip/zip4 instead',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/SubBuilding',
                        'label': 'SubBuilding',
                        'name': 'SubBuilding',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DoubleDependentLocality',
                        'label': 'Double Dependent Locality',
                        'name': 'DoubleDependentLocality',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DependentThoroughfare',
                        'label': 'Dependent Thoroughfare',
                        'name': 'DependentThoroughfare',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Organization',
                        'label': 'Organization',
                        'name': 'Organization',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Premise',
                        'label': 'Premise',
                        'name': 'Premise',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/PremiseNumber',
                        'label': 'PremiseNumber',
                        'name': 'PremiseNumber',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DeliveryAddress',
                        'label': 'Delivery Address',
                        'name': 'DeliveryAddress',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/DeliveryAddress1',
                        'label': 'Delivery Address #1',
                        'name': 'DeliveryAddress1',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Location/attributes/Unmatched',
                        'label': 'Leftover',
                        'name': 'Unmatched',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }],
                    'surrogateCrosswalks': [{
                        'enforce': true,
                        'source': 'configuration/sources/HCOS',
                        'attributes': ['configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/SubBuilding', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4']
                    }, {
                        'enforce': true,
                        'source': 'configuration/sources/MOCA',
                        'attributes': ['configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/SubBuilding', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4']
                    }, {
                        'enforce': true,
                        'source': 'configuration/sources/IREP',
                        'attributes': ['configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/SubBuilding', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4']
                    }, {
                        'enforce': true,
                        'source': 'configuration/sources/RPA',
                        'attributes': ['configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/SubBuilding', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5', 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip4']
                    }],
                    'abstract': false
                },
                {
                    'uri': 'configuration/entityTypes/ACO',
                    'label': 'ACO',
                    'description': 'ACO',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'searchable': false,
                    'typeColor': '#80D557',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/ACO/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/ACO/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/ACO/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/ACO/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/ACO/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Ticker/attributes/Symbol',
                            'label': 'Symbol',
                            'name': 'Symbol',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Ticker/attributes/StockExchange',
                            'label': 'Stock Exchange',
                            'name': 'StockExchange',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ACO/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ACO/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': false,
                    'imageAttributeURIs': ['configuration/entityTypes/ACO/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/IDN',
                    'label': 'IDN',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'dataTooltipPattern': '',
                    'typeColor': '#AB88B1',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/IDN/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/IDN/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/IDN/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/IDN/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'typeIcon': 'images/base_type/house.png',
                    'typeImage': 'images/defaultImage/no-loc.png',
                    'typeGraphIcon': 'images/graphIcon/location-icon.png',
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/IDN/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/SubType',
                        'label': 'SubType',
                        'name': 'SubType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': true,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active', 'configuration/relationTypes/HasAddress/attributes/RankConfidence', 'configuration/relationTypes/HasAddress/attributes/AddressBestVisit', 'configuration/relationTypes/HasAddress/attributes/CDS'],
                        'relationshipLabelPattern': '{AddressType}, {AddressRank} ',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/URL',
                        'label': 'Website URL',
                        'name': 'URL',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/FiscalYearEnd',
                        'label': 'Fiscal Year End',
                        'name': 'FiscalYearEnd',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/ForProfit',
                        'label': 'For Profit',
                        'name': 'ForProfit',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/IDN/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/IDN/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/IDN/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'imageAttributeURIs': ['configuration/entityTypes/IDN/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/Party',
                    'label': 'Party',
                    'description': 'Organization entity type details',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Party/survivorshipGroups/default',
                        'default': true,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Party/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Party/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Party/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Party/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Party/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Party/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Party/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Party/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': true,
                    'businessCardAttributeURIs': ['configuration/entityTypes/Party/attributes/Address'],
                    'imageAttributeURIs': ['configuration/entityTypes/Party/attributes/ImageLinks']
                },
                {
                    'uri': 'configuration/entityTypes/Contact',
                    'label': 'Contact',
                    'description': 'Contact',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{FirstName} {LastName}, {Credentials}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'searchable': false,
                    'typeColor': '#99CCFF',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/Contact/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/Contact/attributes/Employment',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Contact/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Contact/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/Contact/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'extendsTypeURI': 'configuration/entityTypes/Individual',
                    'cleanseConfig': {
                        'infos': [{
                            'uri': 'configuration/entityTypes/Individual/cleanse/infos/default',
                            'useInCleansing': true,
                            'sequence': [{
                                'chain': [{
                                    'cleanseFunction': 'InitialsCleanser',
                                    'proceedOnSuccess': true,
                                    'proceedOnFailure': true,
                                    'mapping': {
                                        'inputMapping': [{
                                            'attribute': 'configuration/entityTypes/Individual/attributes/MiddleName',
                                            'cleanseAttribute': 'MiddleName',
                                            'mandatory': true,
                                            'allValues': false
                                        }],
                                        'outputMapping': [{
                                            'attribute': 'configuration/entityTypes/Individual/attributes/MiddleInitial',
                                            'cleanseAttribute': 'MiddleName',
                                            'mandatory': true,
                                            'allValues': false
                                        }]
                                    }
                                }]
                            }]
                        }]
                    },
                    'attributes': [{
                        'uri': 'configuration/entityTypes/Contact/attributes/Credentials',
                        'label': 'Credential',
                        'name': 'Credentials',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'CRED',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Credential',
                        'label': 'Credential',
                        'name': 'Credential',
                        'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Credential}',
                        'matchFieldURIs': ['configuration/entityTypes/Individual/attributes/Credential/attributes/Credential'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Contact/attributes/Credential/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Credential/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Credential/attributes/Credential',
                            'label': 'Credential',
                            'name': 'Credential',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'CRED',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Prefix',
                        'label': 'Prefix',
                        'name': 'Prefix',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/FormerFirstName',
                        'label': 'Former First Name',
                        'name': 'FormerFirstName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/FormerLastName',
                        'label': 'Former Last Name',
                        'name': 'FormerLastName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/FormerMiddleName',
                        'label': 'Former Middle Name',
                        'name': 'FormerMiddleName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/FormerSuffixName',
                        'label': 'Former Suffix Name',
                        'name': 'FormerSuffixName',
                        'description': '',
                        'type': 'String',
                        'hidden': true,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Name',
                        'label': 'Full Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/LastName',
                        'label': 'Last Name',
                        'name': 'LastName',
                        'description': 'Last Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/FirstName',
                        'label': 'First Name',
                        'name': 'FirstName',
                        'description': 'First Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/MiddleName',
                        'label': 'Middle Name',
                        'name': 'MiddleName',
                        'description': 'Middle Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/SuffixName',
                        'label': 'Suffix Name',
                        'name': 'SuffixName',
                        'description': 'Generation Suffix',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/PreferredName',
                        'label': 'Preferred Name',
                        'name': 'PreferredName',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Nickname',
                        'label': 'Nickname',
                        'name': 'Nickname',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Title',
                        'label': 'Title',
                        'name': 'Title',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Gender',
                        'label': 'Gender',
                        'name': 'Gender',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/SSN',
                        'label': 'SSN',
                        'name': 'SSN',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/SSNLast4',
                        'label': 'SSN Last 4',
                        'name': 'SSNLast4',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/DoB',
                        'label': 'Date of Birth',
                        'name': 'DoB',
                        'description': 'Date of Birth',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/YoB',
                        'label': 'Year of Birth',
                        'name': 'YoB',
                        'description': 'Birth Year',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/BirthCity',
                        'label': 'Birth City',
                        'name': 'BirthCity',
                        'description': 'Birth City',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/BirthState',
                        'label': 'Birth State',
                        'name': 'BirthState',
                        'description': 'Birth State',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/BirthCountry',
                        'label': 'Birth Country',
                        'name': 'BirthCountry',
                        'description': 'Birth Country',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/DoD',
                        'label': 'Date of Death',
                        'name': 'DoD',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/YoD',
                        'label': 'Year of Death',
                        'name': 'YoD',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Contact/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences',
                        'label': 'Privacy Prefs',
                        'name': 'PrivacyPreferences',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': 'Phone: {PhoneOptOut}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/OptOut',
                            'label': 'Opt Out',
                            'name': 'OptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/OptOutStartDate',
                            'label': 'Opt Out Start Date',
                            'name': 'OptOutStartDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/AllowedToContact',
                            'label': 'Allowed To Contact',
                            'name': 'AllowedToContact',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/PhoneOptOut',
                            'label': 'Phone Opt Out',
                            'name': 'PhoneOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/EmailOptOut',
                            'label': 'Email Opt Out',
                            'name': 'EmailOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/FaxOptOut',
                            'label': 'Fax Opt Out',
                            'name': 'FaxOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/TextMessageOptOut',
                            'label': 'Text Message Opt Out',
                            'name': 'TextMessageOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/PrivacyPreferences/attributes/MailOptOut',
                            'label': 'Mail Opt Out',
                            'name': 'MailOptOut',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Education',
                        'label': 'Education',
                        'name': 'Education',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SchoolName} {Degree}, {EducationEndYear}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/SchoolName',
                            'label': 'School Name',
                            'name': 'SchoolName',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/Degree',
                            'label': 'Degree',
                            'name': 'Degree',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/YearOfGraduation',
                            'label': 'Year of graduation',
                            'name': 'YearOfGraduation',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/Graduated',
                            'label': 'Graduated',
                            'name': 'Graduated',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/GPA',
                            'label': 'GPA',
                            'name': 'GPA',
                            'type': 'Number',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/YearsInProgram',
                            'label': 'Years In Program',
                            'name': 'YearsInProgram',
                            'description': 'Year in Grad Training Program, Year in training in current program',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/StartYear',
                            'label': 'Start Year',
                            'name': 'StartYear',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/EndYear',
                            'label': 'End Year',
                            'name': 'EndYear',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/FieldofStudy',
                            'label': 'Field of Study',
                            'name': 'FieldofStudy',
                            'description': 'Specialty Focus or Specialty Training',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/Eligibility',
                            'label': 'Eligibility',
                            'name': 'Eligibility',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Education/attributes/EducationType',
                            'label': 'Type',
                            'name': 'EducationType',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Employment',
                        'label': 'Employment',
                        'name': 'Employment',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/entityTypes/Organization/attributes/Name', 'configuration/relationTypes/Employment/attributes/Title', 'configuration/relationTypes/Employment/attributes/Summary', 'configuration/relationTypes/Employment/attributes/IsCurrent'],
                        'relationshipLabelPattern': '{Title}',
                        'attributeOrdering': {'orderingStrategy': 'LUD'},
                        'referencedEntityTypeURI': 'configuration/entityTypes/Organization',
                        'relationshipTypeURI': 'configuration/relationTypes/Employment'
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/affiliatedwith',
                        'label': 'Affiliated with',
                        'name': 'affiliatedwith',
                        'type': 'Reference',
                        'hidden': true,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/entityTypes/Organization/attributes/Name', 'configuration/relationTypes/affiliatedwith/attributes/Title', 'configuration/relationTypes/affiliatedwith/attributes/PrefOrActive', 'configuration/relationTypes/affiliatedwith/attributes/Rank'],
                        'relationshipLabelPattern': '{Title}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/affiliatedwith/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Organization',
                        'relationshipTypeURI': 'configuration/relationTypes/affiliatedwith'
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['New', 'Active', 'Deceased', 'Moved', 'Retired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/PresumedDead',
                        'label': 'Presumed Dead',
                        'name': 'PresumedDead',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/MiddleInitial',
                        'label': 'Middle Initial',
                        'name': 'MiddleInitial',
                        'description': 'Middle Initial. This attribute is populated from Middle Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/Contact/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': true,
                    'imageAttributeURIs': ['configuration/entityTypes/Contact/attributes/ImageLinks']
                }, {
                    'uri': 'configuration/entityTypes/GPO',
                    'label': 'GPO',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'dataTooltipPattern': '',
                    'typeColor': '#AB8800',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/GPO/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/GPO/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/GPO/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/GPO/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'typeIcon': 'images/base_type/house.png',
                    'typeImage': 'images/defaultImage/no-loc.png',
                    'typeGraphIcon': 'images/graphIcon/location-icon.png',
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/GPO/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/SubType',
                        'label': 'SubType',
                        'name': 'SubType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'faceted': false,
                        'searchable': true,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/entityTypes/Location/attributes/SubAdministrativeArea', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/DEA', 'configuration/relationTypes/HasAddress/attributes/OfficeInformation', 'configuration/relationTypes/HasAddress/attributes/Active', 'configuration/relationTypes/HasAddress/attributes/RankConfidence', 'configuration/relationTypes/HasAddress/attributes/AddressBestVisit', 'configuration/relationTypes/HasAddress/attributes/CDS'],
                        'relationshipLabelPattern': '{AddressType}, {AddressRank} ',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/URL',
                        'label': 'Website URL',
                        'name': 'URL',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/FiscalYearEnd',
                        'label': 'Fiscal Year End',
                        'name': 'FiscalYearEnd',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/ForProfit',
                        'label': 'For Profit',
                        'name': 'ForProfit',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/GPO/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/GPO/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/GPO/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'imageAttributeURIs': ['configuration/entityTypes/GPO/attributes/ImageLinks']
                }, {
                    'uri': 'configuration/entityTypes/ProfessionalAssociation',
                    'label': 'Professional Association',
                    'description': 'ProfessionalAssociation',
                    'geoLocationAttributes': [{
                        'latitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Latitude',
                        'longitude': 'configuration/entityTypes/Location/attributes/GeoLocation/attributes/Longitude'
                    }],
                    'dataLabelPattern': '{Name}',
                    'secondaryLabelPattern': '{businessCardAttributes}',
                    'typeColor': '#80D557',
                    'survivorshipGroups': [{
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/survivorshipGroups/default',
                        'default': false,
                        'mapping': [{
                            'attribute': 'configuration/entityTypes/ProfessionalAssociation/attributes/Address',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers',
                            'survivorshipStrategy': 'Aggregation'
                        }, {
                            'attribute': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities',
                            'survivorshipStrategy': 'Aggregation'
                        }]
                    }],
                    'matchGroups': [{
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/matchGroups/ExactNameAcronym',
                        'label': 'Exact Name Acronym',
                        'type': 'automatic',
                        'rule': {
                            'and': {
                                'exact': ['configuration/entityTypes/ProfessionalAssociation/attributes/Name'],
                                'exactOrAllNull': ['configuration/entityTypes/ProfessionalAssociation/attributes/Acronym']
                            }, 'matchTokenClass': 'com.reltio.match.token.ExactMatchToken'
                        },
                        'matchServiceClass': 'com.reltio.businesslogic.match.providers.internal.InternalMatchService'
                    }],
                    'extendsTypeURI': 'configuration/entityTypes/Organization',
                    'attributes': [{
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Acronym',
                        'label': 'Acronym',
                        'name': 'Acronym',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Name',
                        'label': 'Name',
                        'name': 'Name',
                        'description': 'Name',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/DoingBusinessAsName',
                        'label': 'Doing Business As Name',
                        'name': 'DoingBusinessAsName',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/CompanyType',
                        'label': 'Company Type',
                        'name': 'CompanyType',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/CUSIP',
                        'label': 'CUSIP',
                        'name': 'CUSIP',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Sector',
                        'label': 'Sector',
                        'name': 'Sector',
                        'description': 'Sector',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Public', 'Private'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Industry',
                        'label': 'Industry',
                        'name': 'Industry',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/FoundedYear',
                        'label': 'Founded Year',
                        'name': 'FoundedYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/EndYear',
                        'label': 'End Year',
                        'name': 'EndYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/IPOYear',
                        'label': 'IPO Year',
                        'name': 'IPOYear',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Ticker',
                        'label': 'Ticker',
                        'name': 'Ticker',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Ticker/attributes/Symbol',
                            'label': 'Symbol',
                            'name': 'Symbol',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Ticker/attributes/StockExchange',
                            'label': 'Stock Exchange',
                            'name': 'StockExchange',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/LegalDomicile',
                        'label': 'Domicile State',
                        'name': 'LegalDomicile',
                        'description': 'State of Legal Domicile',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/EmailDomain',
                        'label': 'Email Domain',
                        'name': 'EmailDomain',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'values': ['Operating', 'Operating Subsidiary', 'Reorganizing', 'Out of Business', 'Acquired'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/OwnershipStatus',
                        'label': 'Ownership Status',
                        'name': 'OwnershipStatus',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'faceted': true,
                        'searchable': true,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone',
                        'label': 'Phone',
                        'name': 'Phone',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Number} {Type}',
                        'searchable': true,
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Number',
                            'label': 'Number',
                            'name': 'Number',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Extension',
                            'label': 'Extension',
                            'name': 'Extension',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/CountryCode',
                            'label': 'Country Code',
                            'name': 'CountryCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/AreaCode',
                            'label': 'Area Code',
                            'name': 'AreaCode',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/LocalNumber',
                            'label': 'Local Number',
                            'name': 'LocalNumber',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/LineType',
                            'label': 'Line Type',
                            'name': 'LineType',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/FormatMask',
                            'label': 'Format Mask',
                            'name': 'FormatMask',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/DigitCount',
                            'label': 'Digit Count',
                            'name': 'DigitCount',
                            'description': '',
                            'type': 'Int',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/GeoArea',
                            'label': 'Geo Area',
                            'name': 'GeoArea',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/GeoCountry',
                            'label': 'Geo Country',
                            'name': 'GeoCountry',
                            'description': '',
                            'type': 'String',
                            'hidden': true,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Phone/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities',
                        'label': 'Speciality',
                        'name': 'Specialities',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{SpecialtyType} - {Specialty}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Specialities/attributes/SpecialtyType', 'configuration/entityTypes/Party/attributes/Specialities/attributes/Specialty'],
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities/attributes/SpecialtyType',
                            'label': 'Specialty Type',
                            'name': 'SpecialtyType',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities/attributes/Specialty',
                            'label': 'Specialty',
                            'name': 'Specialty',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'SPEC',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities/attributes/Desc',
                            'label': 'Description',
                            'name': 'Desc',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities/attributes/Group',
                            'label': 'Group',
                            'name': 'Group',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Specialities/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/DEABusinessActivity',
                        'label': 'DEA Business Activity',
                        'name': 'DEABusinessActivity',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/ImageLinks',
                        'label': 'Image Link',
                        'name': 'ImageLinks',
                        'type': 'Image URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/VideoLinks',
                        'label': 'Video Link',
                        'name': 'VideoLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/DocumentLinks',
                        'label': 'Document Link',
                        'name': 'DocumentLinks',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/WebsiteURL',
                        'label': 'Website URL',
                        'name': 'WebsiteURL',
                        'type': 'URL',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers',
                        'label': 'Identifiers',
                        'name': 'Identifiers',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Type} {ID} {Status}',
                        'matchFieldURIs': ['configuration/entityTypes/Party/attributes/Identifiers/attributes/Type', 'configuration/entityTypes/Party/attributes/Identifiers/attributes/ID'],
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/Type',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/ID',
                            'label': 'ID',
                            'name': 'ID',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/Status',
                            'label': 'Status',
                            'name': 'Status',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/DeactivationReasonCode',
                            'label': 'Deactivation Reason Code',
                            'name': 'DeactivationReasonCode',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/DeactivationDate',
                            'label': 'Deactivation Date',
                            'name': 'DeactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Identifiers/attributes/ReactivationDate',
                            'label': 'Reactivation Date',
                            'name': 'ReactivationDate',
                            'description': '',
                            'type': 'Date',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email',
                        'label': 'Email',
                        'name': 'Email',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Email} {Type}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Rank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'values': ['Home', 'Work', 'Other'],
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Email',
                            'label': 'Email',
                            'name': 'Email',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Domain',
                            'label': 'Domain',
                            'name': 'Domain',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/DomainType',
                            'label': 'Domain Type',
                            'name': 'DomainType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Username',
                            'label': 'Username',
                            'name': 'Username',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Rank',
                            'label': 'Rank',
                            'name': 'Rank',
                            'description': '',
                            'type': 'Int',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/ValidationStatus',
                            'label': 'Validation Status',
                            'name': 'ValidationStatus',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/Active',
                            'label': 'Active',
                            'name': 'Active',
                            'description': '',
                            'type': 'Boolean',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Email/attributes/SourceCD',
                            'label': 'Source CD',
                            'name': 'SourceCD',
                            'description': 'DO NOT USE THIS ATTRIBUTE - will be deprecated',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Address',
                        'label': 'Address',
                        'name': 'Address',
                        'type': 'Reference',
                        'hidden': false,
                        'important': false,
                        'referencedAttributeURIs': ['configuration/relationTypes/HasAddress/attributes/AddressType', 'configuration/relationTypes/HasAddress/attributes/CareOf', 'configuration/relationTypes/HasAddress/attributes/Primary', 'configuration/relationTypes/HasAddress/attributes/AddressRank', 'configuration/entityTypes/Location/attributes/AddressLine1', 'configuration/entityTypes/Location/attributes/AddressLine2', 'configuration/entityTypes/Location/attributes/City', 'configuration/entityTypes/Location/attributes/Premise', 'configuration/entityTypes/Location/attributes/Street', 'configuration/entityTypes/Location/attributes/StateProvince', 'configuration/entityTypes/Location/attributes/Zip', 'configuration/entityTypes/Location/attributes/Country', 'configuration/entityTypes/Location/attributes/GeoLocation', 'configuration/entityTypes/Location/attributes/AddressInput', 'configuration/relationTypes/HasAddress/attributes/Phone', 'configuration/relationTypes/HasAddress/attributes/Active'],
                        'relationshipLabelPattern': '{AddressType}',
                        'attributeOrdering': {
                            'fieldURI': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                            'orderType': 'ASC',
                            'orderingStrategy': 'FieldBased'
                        },
                        'referencedEntityTypeURI': 'configuration/entityTypes/Location',
                        'relationshipTypeURI': 'configuration/relationTypes/HasAddress'
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/TaxID',
                        'label': 'Tax ID',
                        'name': 'TaxID',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Description',
                        'label': 'Description',
                        'name': 'Description',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/StatusUpdateDate',
                        'label': 'Status Update Date',
                        'name': 'StatusUpdateDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/StatusReasonCode',
                        'label': 'StatusReasonCode',
                        'name': 'StatusReasonCode',
                        'description': '',
                        'type': 'String',
                        'lookupCode': 'StatusReasonCode',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy',
                        'label': 'Taxonomy',
                        'name': 'Taxonomy',
                        'description': '',
                        'type': 'Nested',
                        'hidden': false,
                        'important': false,
                        'dataLabelPattern': '{Taxonomy}',
                        'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                        'attributes': [{
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/Taxonomy',
                            'label': 'Taxonomy',
                            'name': 'Taxonomy',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_CD',
                            'hidden': false,
                            'important': false,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/Type',
                            'label': 'Type',
                            'name': 'Type',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_TYPE',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/ProviderType',
                            'label': 'Provider Type',
                            'name': 'ProviderType',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/Classification',
                            'label': 'Classification',
                            'name': 'Classification',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/Specialization',
                            'label': 'Specialization',
                            'name': 'Specialization',
                            'description': '',
                            'type': 'String',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }, {
                            'uri': 'configuration/entityTypes/ProfessionalAssociation/attributes/Taxonomy/attributes/Priority',
                            'label': 'Priority',
                            'name': 'Priority',
                            'description': '',
                            'type': 'String',
                            'lookupCode': 'TAXONOMY_PRIORITY',
                            'hidden': false,
                            'important': false,
                            'faceted': true,
                            'searchable': true,
                            'attributeOrdering': {'orderingStrategy': 'LUD'}
                        }]
                    }],
                    'abstract': false,
                    'imageAttributeURIs': ['configuration/entityTypes/ProfessionalAssociation/attributes/ImageLinks']
                }],
            'roles': [{
                'uri': 'configuration/roles/Therapist',
                'label': 'Therapist',
                'description': ''
            }, {
                'uri': 'configuration/roles/Physician',
                'label': 'Physician',
                'description': ''
            }, {
                'uri': 'configuration/roles/Technologist',
                'label': 'Technologist',
                'description': ''
            }, {
                'uri': 'configuration/roles/CMHC',
                'label': 'CMHC',
                'description': ''
            }, {
                'uri': 'configuration/roles/ACTFSP',
                'label': 'ACT/FSP',
                'description': ''
            }, {
                'uri': 'configuration/roles/Vendor',
                'label': 'Vendor',
                'description': ''
            }, {
                'uri': 'configuration/roles/Fellows',
                'label': 'Fellows',
                'description': ''
            }, {
                'uri': 'configuration/roles/PBM',
                'label': 'PBM',
                'description': ''
            }, {
                'uri': 'configuration/roles/Employee',
                'label': 'Employee',
                'description': ''
            }, {
                'uri': 'configuration/roles/Payer',
                'label': 'Payer',
                'description': ''
            }, {
                'uri': 'configuration/roles/Supplier',
                'label': 'Supplier',
                'description': ''
            }, {
                'uri': 'configuration/roles/CRA',
                'label': 'CRA',
                'description': ''
            }, {
                'uri': 'configuration/roles/Home',
                'label': 'Home',
                'description': ''
            }, {
                'uri': 'configuration/roles/PurchasingManager',
                'label': 'Purchasing Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/SPPH',
                'label': 'SPPH',
                'description': ''
            }, {
                'uri': 'configuration/roles/MedicalAssistant',
                'label': 'Medical Assistant',
                'description': ''
            }, {
                'uri': 'configuration/roles/OfficeStaff',
                'label': 'Office Staff',
                'description': ''
            }, {
                'uri': 'configuration/roles/GroupPractice',
                'label': 'Group Practice',
                'description': ''
            }, {
                'uri': 'configuration/roles/DirectorofNursing',
                'label': 'Director of Nursing',
                'description': ''
            }, {
                'uri': 'configuration/roles/PharmacyTechnician',
                'label': 'Pharmacy Technician',
                'description': ''
            }, {
                'uri': 'configuration/roles/Department',
                'label': 'Department',
                'description': ''
            }, {
                'uri': 'configuration/roles/IDN',
                'label': 'IDN',
                'description': ''
            }, {
                'uri': 'configuration/roles/HospitalOutpatient',
                'label': 'Hospital Outpatient',
                'description': ''
            }, {
                'uri': 'configuration/roles/Hospital',
                'label': 'Hospital',
                'description': ''
            }, {
                'uri': 'configuration/roles/Prospect',
                'label': 'Prospect',
                'description': 'Individuals and Organizations can be Clients. Prospects are potential clients, who can be converted to customers of a product.'
            }, {
                'uri': 'configuration/roles/IMD',
                'label': 'IMD',
                'description': ''
            }, {
                'uri': 'configuration/roles/LongTermCare',
                'label': 'Long Term Care',
                'description': ''
            }, {
                'uri': 'configuration/roles/Client',
                'label': 'Client',
                'description': ''
            }, {
                'uri': 'configuration/roles/SocialWorker',
                'label': 'Social Worker',
                'description': ''
            }, {
                'uri': 'configuration/roles/CaseManager',
                'label': 'Case Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/NurseCaseManager',
                'label': 'Nurse Case Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/BoardandCare',
                'label': 'Board and Care',
                'description': ''
            }, {
                'uri': 'configuration/roles/PracticeManager',
                'label': 'Practice Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/MailOrderPharmacy',
                'label': 'Mail Order Pharmacy',
                'description': ''
            }, {
                'uri': 'configuration/roles/Surgicenter',
                'label': 'Surgicenter',
                'description': ''
            }, {
                'uri': 'configuration/roles/Resident',
                'label': 'Resident',
                'description': ''
            }, {
                'uri': 'configuration/roles/Patient',
                'label': 'Patient',
                'description': ''
            }, {
                'uri': 'configuration/roles/LabDirectors',
                'label': 'Lab Directors',
                'description': ''
            }, {
                'uri': 'configuration/roles/BillingManager',
                'label': 'Billing Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/Military',
                'label': 'Military',
                'description': ''
            }, {
                'uri': 'configuration/roles/Hospice',
                'label': 'Hospice',
                'description': ''
            }, {
                'uri': 'configuration/roles/Partner',
                'label': 'Partner',
                'description': ''
            }, {
                'uri': 'configuration/roles/OfficeManager',
                'label': 'Office Manager',
                'description': ''
            }, {
                'uri': 'configuration/roles/TeachingHospital',
                'label': 'Teaching Hospital',
                'description': ''
            }, {
                'uri': 'configuration/roles/RegisteredNurse',
                'label': 'Registered Nurse',
                'description': ''
            }, {
                'uri': 'configuration/roles/Office',
                'label': 'Office',
                'description': ''
            }, {
                'uri': 'configuration/roles/ManagedCareOrg',
                'label': 'Managed Care Org',
                'description': ''
            }, {
                'uri': 'configuration/roles/AccountableCareOrg',
                'label': 'Accountable Care Org',
                'description': ''
            }, {
                'uri': 'configuration/roles/PhysicianOffice',
                'label': 'Physician Office',
                'description': ''
            }, {
                'uri': 'configuration/roles/Non-RetailPharmacy',
                'label': 'Non-Retail Pharmacy',
                'description': ''
            }, {
                'uri': 'configuration/roles/StaffDeveloper',
                'label': 'Staff Developer',
                'description': ''
            }, {
                'uri': 'configuration/roles/QualifiedRecipient',
                'label': 'Qualified Recipient',
                'description': ''
            }, {
                'uri': 'configuration/roles/ER',
                'label': 'ER',
                'description': ''
            }, {
                'uri': 'configuration/roles/NursePractitioner',
                'label': 'Nurse Practitioner',
                'description': ''
            }, {
                'uri': 'configuration/roles/VeteransAdmin',
                'label': 'Veterans Admin',
                'description': ''
            }, {
                'uri': 'configuration/roles/Lab',
                'label': 'Lab',
                'description': ''
            }, {
                'uri': 'configuration/roles/Clinic',
                'label': 'Clinic',
                'description': ''
            }, {
                'uri': 'configuration/roles/PHSCE',
                'label': 'Public Health Service Covered Entity',
                'description': ''
            }, {
                'uri': 'configuration/roles/Pharmacy',
                'label': 'Pharmacy',
                'description': ''
            }, {
                'uri': 'configuration/roles/PhysicianAssistant',
                'label': 'Physician Assistant',
                'description': ''
            }, {
                'uri': 'configuration/roles/Wholesaler',
                'label': 'Wholesaler',
                'description': ''
            }, {
                'uri': 'configuration/roles/SkilledNursingFacility',
                'label': 'Skilled Nursing Facility',
                'description': ''
            }, {
                'uri': 'configuration/roles/Administration',
                'label': 'Administration',
                'description': ''
            }, {
                'uri': 'configuration/roles/Other',
                'label': 'Other',
                'description': ''
            }, {
                'uri': 'configuration/roles/Pharmacist',
                'label': 'Pharmacist',
                'description': ''
            }, {
                'uri': 'configuration/roles/DirectorofPharmacy',
                'label': 'Director of Pharmacy',
                'description': ''
            }, {
                'uri': 'configuration/roles/PrivatePractice',
                'label': 'Private Practice',
                'description': ''
            }, {
                'uri': 'configuration/roles/PartialProgram',
                'label': 'Partial Program',
                'description': ''
            }, {
                'uri': 'configuration/roles/StateHospital',
                'label': 'State Hospital',
                'description': ''
            }, {
                'uri': 'configuration/roles/GroupPurchasingOrg',
                'label': 'Group Purchasing Org',
                'description': ''
            }, {'uri': 'configuration/roles/Customer', 'label': 'Customer', 'description': ''}],
            'groupTypes': [{
                'uri': 'configuration/groupTypes/Test',
                'label': 'Path Household',
                'description': 'Households loaded from PATH',
                'dataLabelPattern': '[primaryMember]({LastName}) Household',
                'dataTooltipPattern': '[primaryMember]({LastName}) Household',
                'hasPrimaryMember': true,
                'multiplePrimaryMembers': false,
                'limitMemberToOneGroupInstance': true,
                'groupElements': [{
                    'uri': 'configuration/groupTypes/PathHHGroupType/groupElements/1',
                    'entityTypeURIs': ['configuration/entityTypes/Individual']
                }],
                'memberTypes': [{
                    'uri': 'configuration/groupTypes/PathHHGroupType/memberTypes/Member',
                    'label': 'Member',
                    'primaryMember': false
                }, {
                    'uri': 'configuration/groupTypes/PathHHGroupType/memberTypes/PrimaryMember',
                    'label': 'Primary Member',
                    'primaryMember': true
                }],
                'type': 'manual',
                'attributes': [{
                    'uri': 'configuration/groupTypes/Test/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/groupTypes/Test/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/groupTypes/Test/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderingStrategy': 'LUD'}
                }]
            }],
            'interactionTypes': [{
                'uri': 'configuration/interactionTypes/Email',
                'label': 'Email',
                'memberTypes': [{
                    'uri': 'configuration/interactionTypes/Email/memberTypes/From',
                    'label': 'From',
                    'minOccurs': 2,
                    'name': 'From',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                }, {
                    'uri': 'configuration/interactionTypes/Email/memberTypes/CC',
                    'label': 'CC',
                    'name': 'CC',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                }, {
                    'uri': 'configuration/interactionTypes/Email/memberTypes/To',
                    'label': 'To',
                    'name': 'To',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                }],
                'attributes': [{
                    'uri': 'configuration/interactionTypes/Email/attributes/Subject',
                    'label': 'Subject',
                    'name': 'Subject',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/interactionTypes/Email/attributes/WordsCount',
                    'label': 'Subject',
                    'name': 'Subject',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/interactionTypes/Email/attributes/Location',
                    'label': 'Location',
                    'name': 'Location',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }]
            }, {
                'uri': 'configuration/interactionTypes/Prescriptions',
                'label': 'Prescriptions',
                'memberTypes': [{
                    'uri': 'configuration/interactionTypes/Prescriptions/memberTypes/Manf',
                    'label': 'Manf',
                    'name': 'Manf',
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                }, {
                    'uri': 'configuration/interactionTypes/Prescriptions/memberTypes/HCP',
                    'label': 'HCP',
                    'name': 'HCP',
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                }, {
                    'uri': 'configuration/interactionTypes/Prescriptions/memberTypes/Address',
                    'label': 'Address',
                    'name': 'Address',
                    'objectTypeURI': 'configuration/entityTypes/Location'
                }, {
                    'uri': 'configuration/interactionTypes/Prescriptions/memberTypes/Pharmacy',
                    'label': 'Pharmacy',
                    'name': 'Pharmacy',
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                }],
                'attributes': [{
                    'uri': 'configuration/interactionTypes/Prescriptions/attributes/Medication',
                    'label': 'Medication',
                    'name': 'Medication',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/interactionTypes/Prescriptions/attributes/Strength',
                    'label': 'Strength',
                    'name': 'Strength',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/interactionTypes/Prescriptions/attributes/Dispense',
                    'label': 'Dispense',
                    'name': 'Dispense',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }]
            }],
            'graphTypes': [{
                'uri': 'configuration/graphTypes/PharmaCompanyHierarchy',
                'label': 'Pharma Company Hierarchy',
                'limitMemberToOneGraphInstance': false,
                'type': 'logical',
                'layout': 'hierarchy',
                'allowCycles': false,
                'relationshipTypeURIs': ['configuration/relationTypes/PharmacyProvider', 'configuration/relationTypes/MedsurgPurchasing', 'configuration/relationTypes/nonGPOmedsurg', 'configuration/relationTypes/PharmaPurchasing', 'configuration/relationTypes/nonGPOpharma'],
                'graphStructure': 'hierarchy'
            }, {
                'uri': 'configuration/graphTypes/CompanyHierarchy',
                'label': 'Company Hierarchy',
                'limitMemberToOneGraphInstance': false,
                'type': 'logical',
                'layout': 'hierarchy',
                'allowCycles': false,
                'relationshipTypeURIs': ['configuration/relationTypes/Subsidiary', 'configuration/relationTypes/Managed', 'configuration/relationTypes/Owned', 'configuration/relationTypes/AffiliatedPurchasing', 'configuration/relationTypes/Leased'],
                'graphStructure': 'hierarchy'
            }, {
                'uri': 'configuration/graphTypes/FamilyGraph',
                'label': 'Family Graph',
                'limitMemberToOneGraphInstance': false,
                'type': 'logical',
                'layout': 'family',
                'allowCycles': true,
                'relationshipTypeURIs': ['configuration/relationTypes/Family'],
                'graphStructure': 'network'
            }],
            'survivorshipStrategies': [{
                'uri': 'configuration/survivorshipStrategies/CleanserWinsStrategy',
                'label': 'Reltio Cleanser or Nothing',
                'winnerSourceType': 'configuration/sources/ReltioCleanser'
            }, {
                'uri': 'configuration/survivorshipStrategies/LUD',
                'label': 'Recency'
            }, {
                'uri': 'configuration/survivorshipStrategies/Frequency',
                'label': 'Frequency'
            }, {
                'uri': 'configuration/survivorshipStrategies/Aggregation',
                'label': 'Aggregation'
            }, {
                'uri': 'configuration/survivorshipStrategies/SRC_SYS',
                'label': 'Source system'
            }, {
                'uri': 'configuration/survivorshipStrategies/OldestValue',
                'label': 'Oldest value'
            }, {
                'uri': 'configuration/survivorshipStrategies/MinValue',
                'label': 'Minimum value'
            }, {
                'uri': 'configuration/survivorshipStrategies/LUD',
                'label': 'Recency'
            }, {'uri': 'configuration/survivorshipStrategies/LUD', 'label': 'Recency'}],
            'sources': [{
                'uri': 'configuration/sources/ABV_MABI',
                'label': 'ABV_MABI',
                'abbreviation': 'ABV_MABI',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/Reltio',
                'label': 'Reltio',
                'abbreviation': 'Reltio',
                'icon': 'images/source/reltio.png'
            }, {
                'uri': 'configuration/sources/AMA',
                'label': 'AMA',
                'abbreviation': 'AMA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/AOA',
                'label': 'AOA',
                'abbreviation': 'AOA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/IMSDDD',
                'label': 'IMS DDD',
                'abbreviation': 'IMSDDD',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/LNKD',
                'label': 'Linkedin',
                'abbreviation': 'LNKD',
                'icon': 'images/source/linkedin.png'
            }, {
                'uri': 'configuration/sources/FB',
                'label': 'Facebook',
                'abbreviation': 'FB',
                'icon': 'images/source/facebook.png'
            }, {
                'uri': 'configuration/sources/IMSPLAN',
                'label': 'IMS PLAN',
                'abbreviation': 'IMSPLAN',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/IREP',
                'label': 'IREP',
                'abbreviation': 'IREP',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/MOCA',
                'label': 'MOCA',
                'abbreviation': 'MOCA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/AMS',
                'label': 'AMS',
                'description': 'AdvantageMS',
                'abbreviation': 'AMS',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/DEA',
                'label': 'DEA',
                'abbreviation': 'DEA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/HMS',
                'label': 'HMS',
                'description': 'Health Market Services',
                'abbreviation': 'HMS',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/DT_PA',
                'label': 'DT:PA',
                'abbreviation': 'DT_PA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/ReltioCleanser',
                'label': 'Reltio Data Cleanser',
                'priority': 100500,
                'abbreviation': 'ReltioCleanser',
                'icon': 'images/source/reltio.png'
            }, {
                'uri': 'configuration/sources/RPA',
                'label': 'RPA',
                'abbreviation': 'RPA',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/TWITTER',
                'label': 'Twitter',
                'abbreviation': 'TWITTER',
                'icon': 'images/source/twitter.png'
            }, {
                'uri': 'configuration/sources/Veeva',
                'label': 'Veeva',
                'abbreviation': 'Veeva',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/HCOS',
                'label': 'HCOS',
                'abbreviation': 'HCOS',
                'icon': 'images/source/source_s.png'
            }, {
                'uri': 'configuration/sources/AHA',
                'label': 'AHA',
                'description': 'AMERICAN HOSPITAL association',
                'abbreviation': 'AHA',
                'icon': 'images/source/source_s.png'
            }],
            'abstract': false,
            'relationTypes': [{
                'uri': 'configuration/relationTypes/Grandchild',
                'label': 'Grandchild',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Grandchild/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Grandchild/startObject/directionalContext',
                        'labelPattern': 'grandchild'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Grandchild/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Grandchild/endObject/directionalContext',
                        'labelPattern': 'grandchild'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Spouse',
                'label': 'Spouse',
                'description': 'Types of relationships between Individual and Individual - representing Spouse relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Spouse/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Spouse/startObject/directionalContext',
                        'labelPattern': 'wife',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Spouse/startObject/directionalContext',
                        'labelPattern': 'husband',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Spouse/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Spouse/endObject/directionalContext',
                        'labelPattern': 'wife',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Spouse/endObject/directionalContext',
                        'labelPattern': 'husband',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Family',
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Sibling',
                'label': 'Sibling',
                'description': 'Types of relationships between Individual and Individual - representing Sibling relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Sibling/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Sibling/startObject/directionalContext',
                        'labelPattern': 'sister',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Sibling/startObject/directionalContext',
                        'labelPattern': 'brother',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Sibling/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Sibling/endObject/directionalContext',
                        'labelPattern': 'sister',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Sibling/endObject/directionalContext',
                        'labelPattern': 'brother',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Family',
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Contact',
                'label': 'Contact',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Contact/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Contact/startObject/directionalContext',
                        'labelPattern': 'firm'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Contact/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Contact/endObject/directionalContext',
                        'labelPattern': 'contact'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/Contact/attributes/Desc',
                    'label': 'Desc',
                    'name': 'Desc',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/NursingHome',
                'label': 'NursingHome',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/NursingHome/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/NursingHome/startObject/directionalContext',
                        'labelPattern': 'nursinghome'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/NursingHome/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/NursingHome/endObject/directionalContext',
                        'labelPattern': 'nursinghome'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Attorney',
                'label': 'Attorney',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Attorney/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Attorney/startObject/directionalContext',
                        'labelPattern': 'client'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Attorney/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Attorney/endObject/directionalContext',
                        'labelPattern': 'attorney'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/nonGPOmedsurg',
                'label': 'nonGPOmedsurg',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/nonGPOmedsurg/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/nonGPOmedsurg/startObject/directionalContext',
                        'labelPattern': 'non-GPO medsurg'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/nonGPOmedsurg/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/nonGPOmedsurg/endObject/directionalContext',
                        'labelPattern': 'non-GPO medsurg'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Supplier',
                'label': 'Supplier',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Supplier/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Supplier/startObject/directionalContext',
                        'labelPattern': 'supplier'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Supplier/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Supplier/endObject/directionalContext',
                        'labelPattern': 'buyer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/AffiliatedPurchasing',
                'label': 'Affiliated Purchasing',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/AffiliatedPurchasing/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/AffiliatedPurchasing/startObject/directionalContext',
                        'labelPattern': 'affiliated purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/AffiliatedPurchasing/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/AffiliatedPurchasing/endObject/directionalContext',
                        'labelPattern': 'affiliated purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Partner',
                'label': 'Partner',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Partner/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Partner/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/ReferredBy',
                'label': 'ReferredBy',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/ReferredBy/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ReferredBy/startObject/directionalContext',
                        'labelPattern': 'referred by'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/ReferredBy/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ReferredBy/endObject/directionalContext',
                        'labelPattern': 'refers'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Allied',
                'label': 'Allied',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Allied/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Allied/startObject/directionalContext',
                        'labelPattern': 'allied'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Allied/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Allied/endObject/directionalContext',
                        'labelPattern': 'allied'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Accountant',
                'label': 'Accountant',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Accountant/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Accountant/startObject/directionalContext',
                        'labelPattern': 'client'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Accountant/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Accountant/endObject/directionalContext',
                        'labelPattern': 'accountant'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/PharmacyProvider',
                'label': 'PharmacyProvider',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/PharmacyProvider/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmacyProvider/startObject/directionalContext',
                        'labelPattern': 'pharmacy provider'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/PharmacyProvider/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmacyProvider/endObject/directionalContext',
                        'labelPattern': 'pharmacy provider'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/GPOtoHCO',
                'label': 'GPOtoHCO',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/GPOtoHCO/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/GPOtoHCO/startObject/directionalContext',
                        'labelPattern': 'buys for'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/GPO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/GPOtoHCO/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/GPOtoHCO/endObject/directionalContext',
                        'labelPattern': 'is client of'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/GPOtoHCO/attributes/ID',
                    'label': 'GPO',
                    'name': 'GPO',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/GPOtoHCO/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Managed',
                'label': 'Managed',
                'description': 'Types of relationships between Organization and Organization - representing Subsidiary of relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Managed/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Managed/startObject/directionalContext',
                        'labelPattern': 'manager'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Managed/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Managed/endObject/directionalContext',
                        'labelPattern': 'managed'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Attending',
                'label': 'Attending',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Attending/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Attending/startObject/directionalContext',
                        'labelPattern': 'attending'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Attending/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Attending/endObject/directionalContext',
                        'labelPattern': 'attending'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Attending/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Attending/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Attending/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Officer',
                'label': 'Officer',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Officer/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Officer/startObject/directionalContext',
                        'labelPattern': 'officer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Officer/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Officer/endObject/directionalContext',
                        'labelPattern': 'has officer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Employment',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Officer/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Officer/attributes/Summary',
                    'label': 'Summary',
                    'name': 'Summary',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Officer/attributes/IsCurrent',
                    'label': 'Is Current',
                    'name': 'IsCurrent',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Boardmember',
                'label': 'Boardmember',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Boardmember/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Boardmember/startObject/directionalContext',
                        'labelPattern': 'firm'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Boardmember/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Boardmember/endObject/directionalContext',
                        'labelPattern': 'boardmember'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HeadFinance',
                'label': 'Head of Finance',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HeadFinance/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadFinance/startObject/directionalContext',
                        'labelPattern': 'head of finance'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HeadFinance/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadFinance/endObject/directionalContext',
                        'labelPattern': 'head of finance'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HeadFinance/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadFinance/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadFinance/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HasAddress',
                'label': 'has address',
                'description': 'Types of relationships between Parties (Individuals and Organizations) and Location',
                'typeColor': '#663344',
                'survivorshipGroups': [{
                    'uri': 'configuration/relationTypes/HasAddress/survivorshipGroups/default',
                    'default': true,
                    'mapping': [{
                        'attribute': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'survivorshipStrategy': 'Aggregation'
                    }]
                }],
                'startObject': {
                    'uri': 'configuration/relationTypes/HasAddress/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasAddress/startObject/directionalContext',
                        'labelPattern': 'has address'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HasAddress/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasAddress/endObject/directionalContext',
                        'labelPattern': 'locates'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Location'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/HasAddress/attributes/AddressRank',
                    'label': 'AddressRank',
                    'name': 'AddressRank',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/BusinessCode',
                    'label': 'BusinessCode',
                    'name': 'BusinessCode',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                    'label': 'Address Type',
                    'name': 'AddressType',
                    'type': 'String',
                    'lookupCode': 'ADDR_TYPE',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/OfficeInformation',
                    'label': 'Office Information',
                    'name': 'OfficeInformation',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{BestTimes} (Appt required - {ApptRequired})',
                    'singleValue': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/relationTypes/HasAddress/attributes/OfficeInformation/attributes/BestTimes',
                        'label': 'BestTimes',
                        'name': 'BestTimes',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/OfficeInformation/attributes/ApptRequired',
                        'label': 'Appt Required',
                        'name': 'ApptRequired',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/OfficeInformation/attributes/OfficeNotes',
                        'label': 'Office Notes',
                        'name': 'OfficeNotes',
                        'description': '',
                        'type': 'Blob',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/DEA',
                    'label': 'DEA',
                    'name': 'DEA',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{BestTimes} (Appt required - {ApptRequired})',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/Number',
                        'label': 'Number',
                        'name': 'Number',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/ExpirationDate',
                        'label': 'Expiration Date',
                        'name': 'ExpirationDate',
                        'description': '',
                        'type': 'Date',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/Status',
                        'label': 'Status',
                        'name': 'Status',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/DrugSchedule',
                        'label': 'Drug Schedule',
                        'name': 'DrugSchedule',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/Status',
                    'label': 'Status',
                    'name': 'Status',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/Primary',
                    'label': 'Primary',
                    'name': 'Primary',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/CareOf',
                    'label': 'Care Of',
                    'name': 'CareOf',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/Active',
                    'label': 'Active',
                    'name': 'Active',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/Phone',
                    'label': 'Phone',
                    'name': 'Phone',
                    'type': 'Nested',
                    'hidden': false,
                    'important': false,
                    'dataLabelPattern': '{Number} {Type}',
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'},
                    'attributes': [{
                        'uri': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Type',
                        'label': 'Type',
                        'name': 'Type',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'values': ['Mobile', 'Home', 'Work', 'Fax', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Number',
                        'label': 'Number',
                        'name': 'Number',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Extension',
                        'label': 'Extension',
                        'name': 'Extension',
                        'description': '',
                        'type': 'String',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Rank',
                        'label': 'Rank',
                        'name': 'Rank',
                        'description': '',
                        'type': 'Int',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }, {
                        'uri': 'configuration/relationTypes/HasAddress/attributes/Phone/attributes/Active',
                        'label': 'Active',
                        'name': 'Active',
                        'description': '',
                        'type': 'Boolean',
                        'hidden': false,
                        'important': false,
                        'attributeOrdering': {'orderingStrategy': 'LUD'}
                    }]
                }, {
                    'uri': 'configuration/relationTypes/HasAddress/attributes/SourceCD',
                    'label': 'Source CD',
                    'name': 'SourceCD',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/nonGPOpharma',
                'label': 'nonGPOpharma',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/nonGPOpharma/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/nonGPOpharma/startObject/directionalContext',
                        'labelPattern': 'non-GPO pharma'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/nonGPOpharma/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/nonGPOpharma/endObject/directionalContext',
                        'labelPattern': 'non-GPO pharma'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/IDNtoGPO',
                'label': 'Has GPO',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/IDNtoGPO/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IDNtoGPO/startObject/directionalContext',
                        'labelPattern': 'is client of'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/IDN'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/IDNtoGPO/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IDNtoGPO/endObject/directionalContext',
                        'labelPattern': 'buys for'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/GPO'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/IDNtoGPO/attributes/ID',
                    'label': 'GPO',
                    'name': 'GPO',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/IDNtoGPO/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Employment',
                'label': 'Employment',
                'description': 'Types of relationships between Individuals and Organizations - representing Individual employment for some Organization',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Employment/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Employment/startObject/directionalContext',
                        'labelPattern': 'employee'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Employment/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Employment/endObject/directionalContext',
                        'labelPattern': 'employer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/Employment/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Employment/attributes/Summary',
                    'label': 'Summary',
                    'name': 'Summary',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Employment/attributes/IsCurrent',
                    'label': 'Is Current',
                    'name': 'IsCurrent',
                    'type': 'Boolean',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/MedsurgPurchasing',
                'label': 'MedsurgPurchasing',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/MedsurgPurchasing/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedsurgPurchasing/startObject/directionalContext',
                        'labelPattern': 'medsurg purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/MedsurgPurchasing/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedsurgPurchasing/endObject/directionalContext',
                        'labelPattern': 'medsurg purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Relative',
                'label': 'Relative',
                'typeColor': '#777777',
                'startObject': {
                    'uri': 'configuration/relationTypes/Relative/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Relative/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Family',
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Parent',
                'label': 'Parent',
                'description': 'Types of relationships between Individual and Individual - representing Parent relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Parent/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Parent/startObject/directionalContext',
                        'labelPattern': 'mother',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Parent/startObject/directionalContext',
                        'labelPattern': 'father',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }, {
                        'uri': 'configuration/relationTypes/Parent/startObject/directionalContext',
                        'labelPattern': 'parent'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Parent/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Parent/endObject/directionalContext',
                        'labelPattern': 'daughter',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Female'}]
                    }, {
                        'uri': 'configuration/relationTypes/Parent/endObject/directionalContext',
                        'labelPattern': 'son',
                        'rule': [{'attribute': 'Gender', 'type': 'condition', 'condition': '=', 'value': 'Male'}]
                    }, {
                        'uri': 'configuration/relationTypes/Parent/endObject/directionalContext',
                        'labelPattern': 'child'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Family',
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HeadofAdmin',
                'label': 'Head of Administration',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HeadofAdmin/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadofAdmin/startObject/directionalContext',
                        'labelPattern': 'head of admin'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HeadofAdmin/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadofAdmin/endObject/directionalContext',
                        'labelPattern': 'head of admin'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HeadofAdmin/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadofAdmin/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadofAdmin/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HeadMatMgmnt',
                'label': 'Head of Materials Management',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HeadMatMgmnt/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadMatMgmnt/startObject/directionalContext',
                        'labelPattern': 'head of mat mgmnt'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HeadMatMgmnt/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadMatMgmnt/endObject/directionalContext',
                        'labelPattern': 'head of mat mgmnt'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HeadMatMgmnt/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadMatMgmnt/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadMatMgmnt/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Admitting',
                'label': 'Admitting',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Admitting/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Admitting/startObject/directionalContext',
                        'labelPattern': 'admitting'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Admitting/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Admitting/endObject/directionalContext',
                        'labelPattern': 'admitting'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Admitting/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Admitting/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Admitting/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/DomesticPartner',
                'label': 'domestic partner',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/DomesticPartner/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/DomesticPartner/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/Family',
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Other',
                'label': 'Other',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Other/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Other/startObject/directionalContext',
                        'labelPattern': 'group practice'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Other/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Other/endObject/directionalContext',
                        'labelPattern': 'group practice'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Other/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Other/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Other/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/CoBusiness',
                'label': 'CoBusiness',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/CoBusiness/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/CoBusiness/startObject/directionalContext',
                        'labelPattern': 'co-business'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/CoBusiness/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/CoBusiness/endObject/directionalContext',
                        'labelPattern': 'co-business'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/MedDirector',
                'label': 'Medical Director',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/MedDirector/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedDirector/startObject/directionalContext',
                        'labelPattern': 'med director'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/MedDirector/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedDirector/endObject/directionalContext',
                        'labelPattern': 'med director'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/MedDirector/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/MedDirector/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/MedDirector/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HasHealthCareRole',
                'label': 'Has Health Care Role',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HasHealthCareRole/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasHealthCareRole/startObject/directionalContext',
                        'labelPattern': 'has health care role'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HasHealthCareRole/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasHealthCareRole/endObject/directionalContext',
                        'labelPattern': 'has health care affiliation'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/HasHealthCareRole/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasHealthCareRole/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasHealthCareRole/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Advisor',
                'label': 'Advisor',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Advisor/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Advisor/startObject/directionalContext',
                        'labelPattern': 'client'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Advisor/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Advisor/endObject/directionalContext',
                        'labelPattern': 'advisor'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/ParentChild',
                'label': 'ParentChild',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/ParentChild/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ParentChild/startObject/directionalContext',
                        'labelPattern': 'is parent to'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/ParentChild/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ParentChild/endObject/directionalContext',
                        'labelPattern': 'has parent'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/ParentChild/attributes/ID',
                    'label': 'IDN',
                    'name': 'IDN',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/ParentChild/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Contractor',
                'label': 'Contractor',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Contractor/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Contractor/startObject/directionalContext',
                        'labelPattern': 'contractor'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Contractor/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Contractor/endObject/directionalContext',
                        'labelPattern': 'customer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HeadNursing',
                'label': 'Head of Nursing',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HeadNursing/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadNursing/startObject/directionalContext',
                        'labelPattern': 'head of nursing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HeadNursing/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadNursing/endObject/directionalContext',
                        'labelPattern': 'head of nursing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HeadNursing/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadNursing/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadNursing/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HeadOperations',
                'label': 'Head of Operations',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HeadOperations/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadOperations/startObject/directionalContext',
                        'labelPattern': 'head of ops'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HeadOperations/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HeadOperations/endObject/directionalContext',
                        'labelPattern': 'head of ops'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HeadOperations/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadOperations/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HeadOperations/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Leased',
                'label': 'Leased',
                'description': 'Types of relationships between Organization and Organization - representing Subsidiary of relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Leased/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Leased/startObject/directionalContext',
                        'labelPattern': 'lessor'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Leased/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Leased/endObject/directionalContext',
                        'labelPattern': 'lessee'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/affiliatedwith',
                'label': 'Affiliated with',
                'description': 'Types of relationships between Individual and Organization - representing Individual employment for some Organization',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/affiliatedwith/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/affiliatedwith/startObject/directionalContext',
                        'labelPattern': 'affiliated with'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/affiliatedwith/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/affiliatedwith/endObject/directionalContext',
                        'labelPattern': 'affiliated with'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/affiliatedwith/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/affiliatedwith/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/affiliatedwith/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Academic',
                'label': 'Academic',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Academic/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Academic/startObject/directionalContext',
                        'labelPattern': 'academic'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Academic/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Academic/endObject/directionalContext',
                        'labelPattern': 'academic'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Assistant',
                'label': 'Assistant',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Assistant/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Assistant/startObject/directionalContext',
                        'labelPattern': 'assistant'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Assistant/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Assistant/endObject/directionalContext',
                        'labelPattern': 'has assistant'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/ProfAssocMember',
                'label': 'Prof Assoc Member',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/ProfAssocMember/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ProfAssocMember/startObject/directionalContext',
                        'labelPattern': 'is member of'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/ProfAssocMember/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ProfAssocMember/endObject/directionalContext',
                        'labelPattern': 'is assoc for'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/ProfessionalAssociation'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Business',
                'label': 'Business',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Business/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Business/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Family',
                'label': 'Family',
                'description': 'Relationship between Families',
                'typeColor': '#777777',
                'startObject': {
                    'uri': 'configuration/relationTypes/Family/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Family/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Owned',
                'label': 'Owned',
                'description': 'Types of relationships between Organization and Organization - representing Subsidiary of relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Owned/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Owned/startObject/directionalContext',
                        'labelPattern': 'owner'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Owned/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Owned/endObject/directionalContext',
                        'labelPattern': 'owned'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/IPAContract',
                'label': 'IPA Contract',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/IPAContract/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IPAContract/startObject/directionalContext',
                        'labelPattern': 'ipa contract'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/IPAContract/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IPAContract/endObject/directionalContext',
                        'labelPattern': 'ipa contract'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/MedsurgDistribution',
                'label': 'MedsurgDistribution',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/MedsurgDistribution/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedsurgDistribution/startObject/directionalContext',
                        'labelPattern': 'medsurg distribution'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/MedsurgDistribution/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/MedsurgDistribution/endObject/directionalContext',
                        'labelPattern': 'medsurg distribution'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Subsidiary',
                'label': 'Subsidiary of',
                'description': 'Types of relationships between Organization and Organization - representing Subsidiary of relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Subsidiary/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Subsidiary/startObject/directionalContext',
                        'labelPattern': 'parent'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Subsidiary/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Subsidiary/endObject/directionalContext',
                        'labelPattern': 'subsidiary'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Organization'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Affiliation',
                'label': 'Affiliation',
                'description': '',
                'typeColor': '#663344',
                'startObject': {
                    'uri': 'configuration/relationTypes/Affiliation/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Affiliation/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/Affiliation/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'values': ['Resident', 'Employee', 'Attending', 'RN'],
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Affiliation/attributes/Status',
                    'label': 'Status',
                    'name': 'Status',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/DirPharmacy',
                'label': 'Director of Pharmacy',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/DirPharmacy/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/DirPharmacy/startObject/directionalContext',
                        'labelPattern': 'dir of pharmacy'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/DirPharmacy/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/DirPharmacy/endObject/directionalContext',
                        'labelPattern': 'dir of pharmacy'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/DirPharmacy/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/DirPharmacy/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/DirPharmacy/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Consulting',
                'label': 'Consulting',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Consulting/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Consulting/startObject/directionalContext',
                        'labelPattern': 'consulting'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Consulting/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Consulting/endObject/directionalContext',
                        'labelPattern': 'consulting'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Consulting/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Consulting/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Consulting/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/PharmConsult',
                'label': 'Pharmacy Consultant',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/PharmConsult/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmConsult/startObject/directionalContext',
                        'labelPattern': 'pharmacy consultant'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/PharmConsult/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmConsult/endObject/directionalContext',
                        'labelPattern': 'pharmacy consultant'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/PharmConsult/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/PharmConsult/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/PharmConsult/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Treating',
                'label': 'Treating',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Treating/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Treating/startObject/directionalContext',
                        'labelPattern': 'treating'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Treating/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Treating/endObject/directionalContext',
                        'labelPattern': 'treating'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Treating/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Treating/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Treating/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/ACOMember',
                'label': 'ACO Member',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/ACOMember/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ACOMember/startObject/directionalContext',
                        'labelPattern': 'is member of'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Party'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/ACOMember/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/ACOMember/endObject/directionalContext',
                        'labelPattern': 'is ACO of'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/ACO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Influencer',
                'label': 'Influencer',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Influencer/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Influencer/startObject/directionalContext',
                        'labelPattern': 'influenced'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Influencer/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Influencer/endObject/directionalContext',
                        'labelPattern': 'influencer'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/IDNaffiliated',
                'label': 'IDN affiliated',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/IDNaffiliated/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IDNaffiliated/startObject/directionalContext',
                        'labelPattern': 'IDN affiliated'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/IDNaffiliated/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/IDNaffiliated/endObject/directionalContext',
                        'labelPattern': 'IDN affiliated'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/IDNaffiliated/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/IDNaffiliated/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/IDNaffiliated/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/GPOtoIDN',
                'label': 'GPOtoIDN',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/GPOtoIDN/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/GPOtoIDN/startObject/directionalContext',
                        'labelPattern': ''
                    }],
                    'objectTypeURI': 'configuration/entityTypes/GPO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/GPOtoIDN/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/GPOtoIDN/endObject/directionalContext',
                        'labelPattern': ''
                    }],
                    'objectTypeURI': 'configuration/entityTypes/IDN'
                },
                'implicit': false,
                'attributes': [{
                    'uri': 'configuration/relationTypes/GPOtoIDN/attributes/ID',
                    'label': 'GPO',
                    'name': 'GPO',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/GPOtoIDN/attributes/Type',
                    'label': 'Type',
                    'name': 'Type',
                    'description': '',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'faceted': true,
                    'searchable': true,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Staff',
                'label': 'Staff',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Staff/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Staff/startObject/directionalContext',
                        'labelPattern': 'staff'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Staff/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Staff/endObject/directionalContext',
                        'labelPattern': 'staff'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/Staff/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Staff/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/Staff/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/HasAdminRole',
                'label': 'Has Admin Role',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/HasAdminRole/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasAdminRole/startObject/directionalContext',
                        'labelPattern': 'has admin role'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCP'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/HasAdminRole/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/HasAdminRole/endObject/directionalContext',
                        'labelPattern': 'has admin affiliation'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'extendsTypeURI': 'configuration/relationTypes/affiliatedwith',
                'attributes': [{
                    'uri': 'configuration/relationTypes/HasAdminRole/attributes/Title',
                    'label': 'Title',
                    'name': 'Title',
                    'type': 'String',
                    'hidden': false,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAdminRole/attributes/PrefOrActive',
                    'label': 'PrefOrActive',
                    'name': 'PrefOrActive',
                    'type': 'String',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }, {
                    'uri': 'configuration/relationTypes/HasAdminRole/attributes/Rank',
                    'label': 'Rank',
                    'name': 'Rank',
                    'type': 'Int',
                    'hidden': true,
                    'important': false,
                    'attributeOrdering': {'orderType': 'ASC', 'orderingStrategy': 'LUD'}
                }],
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/PharmaDistribution',
                'label': 'PharmaDistribution',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/PharmaDistribution/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmaDistribution/startObject/directionalContext',
                        'labelPattern': 'pharma distribution'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/PharmaDistribution/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmaDistribution/endObject/directionalContext',
                        'labelPattern': 'pharma distribution'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/Friend',
                'label': 'Friend',
                'description': 'Types of relationships between Individual and Individual - representing Friend relationship type',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Friend/startObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Friend/endObject',
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'bidirectional'
            }, {
                'uri': 'configuration/relationTypes/Manager',
                'label': 'Manager',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/Manager/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Manager/startObject/directionalContext',
                        'labelPattern': 'manager'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/Manager/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/Manager/endObject/directionalContext',
                        'labelPattern': 'reportee'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/Individual'
                },
                'implicit': false,
                'direction': 'directed'
            }, {
                'uri': 'configuration/relationTypes/PharmaPurchasing',
                'label': 'PharmaPurchasing',
                'description': '',
                'typeColor': '#AA3A44',
                'startObject': {
                    'uri': 'configuration/relationTypes/PharmaPurchasing/startObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmaPurchasing/startObject/directionalContext',
                        'labelPattern': 'pharma purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'endObject': {
                    'uri': 'configuration/relationTypes/PharmaPurchasing/endObject',
                    'directionalContext': [{
                        'uri': 'configuration/relationTypes/PharmaPurchasing/endObject/directionalContext',
                        'labelPattern': 'pharma purchasing'
                    }],
                    'objectTypeURI': 'configuration/entityTypes/HCO'
                },
                'implicit': false,
                'direction': 'directed'
            }]
        }
    );
};
var getGroupedAttributes = function () {
    return {
        'Personal Details': [{
            'value': [{
                'type': 'configuration/entityTypes/HCP/attributes/FirstName',
                'ov': true,
                'value': 'Alexey',
                'uri': 'entities/0000hgf/attributes/FirstName/1n1lp3'
            }],
            'attrType': {
                'uri': 'configuration/entityTypes/HCP/attributes/FirstName',
                'label': 'First Name',
                'name': 'FirstName',
                'description': 'First Name',
                'type': 'String',
                'hidden': false,
                'important': false,
                'searchable': true,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            },
            'showLabel': true
        }],
        'Affiliated with': [{
            'value': [{
                'label': 'Pivo u Leshi',
                'relationshipLabel': 'Pivo u Leshi - ',
                'value': {
                    'Name': [{
                        'type': 'configuration/entityTypes/HCO/attributes/Name',
                        'ov': true,
                        'value': 'Pivo u Leshi',
                        'uri': 'entities/0000hgf/attributes/affiliatedwith/0001L73/Name/1nHvRh'
                    }]
                },
                'ov': true,
                'uri': 'entities/0000hgf/attributes/affiliatedwith/0001L73',
                'refEntity': {
                    'crosswalks': [{
                        'type': 'configuration/sources/Reltio',
                        'value': '0001SZV',
                        'createDate': '2015-05-27T08:43:35.233Z',
                        'updateDate': '2015-05-27T08:43:35.233Z',
                        'uri': 'entities/0000hgf/crosswalks/0001SZV.1nI8ET',
                        'attributeURIs': ['entities/0000hgf/attributes/affiliatedwith/0001L73/Name/1nHvRh', 'entities/0000hgf/attributes/affiliatedwith/0001L73']
                    }], 'type': 'configuration/entityTypes/HCO', 'objectURI': 'entities/0001SZV'
                },
                'refRelation': {
                    'crosswalks': [{
                        'type': 'configuration/sources/Reltio',
                        'value': '0001L73',
                        'createDate': '2015-05-27T08:59:30.596Z',
                        'updateDate': '2015-05-27T08:59:30.596Z',
                        'uri': 'entities/0000hgf/crosswalks/0001L73.1nJzJR'
                    }], 'objectURI': 'relations/0001L73'
                }
            }],
            'attrType': {
                'uri': 'configuration/entityTypes/HCP/attributes/affiliatedwith',
                'label': 'Affiliated with',
                'name': 'affiliatedwith',
                'type': 'Reference',
                'hidden': false,
                'important': false,
                'faceted': true,
                'searchable': true,
                'referencedAttributeURIs': ['configuration/entityTypes/HCO/attributes/Name', 'configuration/entityTypes/HCO/attributes/HealthSystemName'],
                'relationshipLabelPattern': '{Name} - {HealthSystemName}',
                'attributeOrdering': {'orderingStrategy': 'LUD'},
                'referencedEntityTypeURI': 'configuration/entityTypes/HCO',
                'relationshipTypeURI': 'configuration/relationTypes/HasHealthCareRole'
            },
            'showLabel': false
        }],
        'Other': [{
            'value': [{
                'type': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                'ov': true,
                'value': 'Unknown',
                'lookupCode': 'Unknown',
                'uri': 'entities/0000hgf/attributes/ProfDesignation/1n2Wht'
            }],
            'attrType': {
                'uri': 'configuration/entityTypes/HCP/attributes/ProfDesignation',
                'label': 'Prof Designation',
                'name': 'ProfDesignation',
                'description': 'String',
                'type': 'String',
                'lookupCode': 'PROF_DESG',
                'hidden': false,
                'important': false,
                'faceted': true,
                'searchable': true,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            },
            'showLabel': true
        }, {
            'value': [{
                'type': 'configuration/entityTypes/HCP/attributes/WebsiteURL',
                'ov': true,
                'value': 'www.google.com',
                'uri': 'entities/0000hgf/attributes/WebsiteURL/1nUv0T'
            }],
            'attrType': {
                'uri': 'configuration/entityTypes/HCP/attributes/WebsiteURL',
                'label': 'Website URL',
                'name': 'WebsiteURL',
                'type': 'URL',
                'hidden': false,
                'important': false,
                'attributeOrdering': {'orderingStrategy': 'LUD'}
            },
            'showLabel': true
        }]
    };
};


window.mocked = {
    getMobileConfiguration: getMobileConfiguration,
    getReferencedAttribute: getReferencedAttribute,
    getTestEntity: getTestEntity,
    getLocationEntity: getLocationEntity,
    getAttributes: getAttributes,
    getMetadata: getMetadata,
    getGroupedAttributes: getGroupedAttributes
};