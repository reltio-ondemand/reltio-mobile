/*
 * Copyright [2020] Reltio Inc., all rights reserved.
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var domEl;
beforeEach(function () {            // Reset the div with a new one.
    domEl = document.createElement('div');
    domEl.setAttribute('id', 'jasmine_content');
    var oldEl = document.getElementById('jasmine_content');
    if (oldEl) oldEl.parentNode.replaceChild(domEl, oldEl);
});

afterEach(function () {             // Make the test runner look pretty
    domEl.setAttribute('style', 'display:none;');
});

// Karma normally starts the tests right after all files specified in 'karma.config.js' have been loaded
// We only want the tests to start after Sencha Touch/ExtJS has bootstrapped the application.
// 1. We temporary override the '__karma__.loaded' function
// 2. When Ext is ready we call the '__karma__.loaded' function manually
var karmaLoadedFunction = window.__karma__.loaded;
window.__karma__.loaded = function () {};
Ext.Loader.setConfig({
    enabled: true,                  // Turn on Ext.Loader
    disableCaching: false           // Turn OFF cache BUSTING
});

// 'base' is set by Karma to be __dirname of karm.conf.js file
Ext.Loader.setPath({
    'Ext':  'base/touch/src',
    'MobileUI':   'base/app'
});
Ext.Loader.addClassPathMappings({
    "MobileUI": "base/app",
    "MobileUI.core.util.HashUtil": "base/app/core/util/HashUtils.js",
    "MobileUI.core.feedback.ZendeskProvider": "base/app/core/feedback/ZendeskProvider.js"
});
Ext.os.deviceType = 'test';

Ext.require('Ext.XTemplate');
Ext.require('MobileUI.components.editablelist.editors.NestedAttributeEditor');
Ext.require('MobileUI.controller.NewEditMode');
Ext.require('MobileUI.core.security.SecurityService')
Ext.application({
    name: 'MobileUI',
    controllers: [
        'Login',
        'ViewActivator',
        'ForgotPassword',
        'PasswordRestore',
        'Base',
        'Feedback',
        'NewPassword',
        'SearchFacet',
        'RelationAttributes',
        'CustomViewController',
        'EditRelationAttributes',
        'RelationComplexAttributes',
        'Search',
        'SearchTypeAhead',
        'Profile',
        'ComplexAttributes',
        'AttributesFacet',
        'PlacesFacet',
        'Relations',
        'Comments',
        'AddComments',
        'RelationsTree',
        'TwoHopsConnections',
        'dialogs.RelationshipType',
        'dialogs.CreateEntity',
        'dialogs.ChooseEntity',
        'dialogs.CreateRelation',
        'dialogs.NestedAttributeEditor',
        'dialogs.ReferenceAttributeEditor',
        'dialogs.ChooseAttributeType',
        'dialogs.GlobalFilter',
        'dialogs.SavedSearches',
        'dialogs.EditSavedSearch',
        'editors.EnumEditor',
        'editors.LookupEditor',
        'editors.StringEditor',
        'editors.NumberEditor',
        'editors.DateEditor',
        'editors.BoolEditor',
        'editors.BlobEditor',
        'dashboard.Dashboard'
    ],

    profiles: ['General'],
    onDependenciesLoaded: function(){
        console.log('test');
    },
    launch: function(){
        window.MobileUI = MobileUI;
    }

});
Ext.onReady(function () {
    console.info("Starting Tests ...");
    window.__karma__.loaded = karmaLoadedFunction;
    window.__karma__.loaded();
});
