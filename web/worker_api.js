/*
    The promise-based api usage example
    var apiPath;
    UI.getApiPath()
        .then(function (api) {
            apiPath = api;
            return UI.getUiConfiguration();
        })
        .then(function (config) {
            UI.alert(apiPath);
            UI.alert(config);
        });
 */
(function (root)
{
    // Use polyfill for setImmediate for performance gains
    var asap = typeof setImmediate === "function" ? setImmediate :
        function (fn)
        {
            setTimeout(fn, 1);
        };

    // Polyfill for Function.prototype.bind
    function bind(fn, thisArg)
    {
        return function ()
        {
            fn.apply(thisArg, arguments);
        }
    }

    var isArray = Array.isArray || function (value)
        {
            return Object.prototype.toString.call(value) === "[object Array]"
        };

    /**
     * Executor function
     * Function object with two arguments resolve and reject.
     * The first argument fulfills the promise, the second argument rejects it.
     * We can call these functions once our operation is completed.
     * @function executorFunction
     * @param resolve {Function}  fulfills the promise.
     * @param reject {Function} reject the promise.
     */
    /**
     * Creates the promise
     * @param fn (executorFunction) Executor function
     * @constructor
     */
    function Promise(fn)
    {
        if (typeof this !== "object")
        {
            throw new TypeError("Promises must be constructed via new");
        }
        if (typeof fn !== "function")
        {
            throw new TypeError("not a function");
        }
        this._state = null;
        this._value = null;
        this._deferreds = [];

        doResolve(fn, bind(resolve, this), bind(reject, this))
    }

    /**
     * @private
     * @param deferred {Promise}
     */
    function handle(deferred)
    {
        var me = this;
        if (this._state === null)
        {
            this._deferreds.push(deferred);
            return
        }
        asap(function () {
            var cb = me._state ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
                (me._state ? deferred.resolve : deferred.reject)(me._value);
                return;
            }
            var ret;
            try {
                ret = cb(me._value);
            }
            catch (e) {
                deferred.reject(e);
                return;
            }
            deferred.resolve(ret);
        })
    }

    /**
     * @private
     * @param newValue {*} resolve value
     */
    function resolve(newValue)
    {
        try
        { //Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === this)
            {
                throw new TypeError("A promise cannot be resolved with itself.");
            }
            if (newValue && (typeof newValue === "object" || typeof newValue === "function"))
            {
                var then = newValue.then;
                if (typeof then === "function")
                {
                    doResolve(bind(then, newValue), bind(resolve, this), bind(reject, this));
                    return;
                }
            }
            this._state = true;
            this._value = newValue;
            finale.call(this);
        } catch (e)
        {
            reject.call(this, e);
        }
    }

    /**
     * @private
     * @param newValue {*} reject value
     */
    function reject(newValue)
    {
        this._state = false;
        this._value = newValue;
        finale.call(this);
    }

    /**
     * @private
     */
    function finale()
    {
        for (var i = 0, len = this._deferreds.length; i < len; i++)
        {
            handle.call(this, this._deferreds[i]);
        }
        this._deferreds = null;
    }

    /**
     * @private
     * Handler object constructor
     * @param onFulfilled {Function}
     * @param onRejected {Function}
     * @param resolve {Function}
     * @param reject {Function}
     * @constructor
     */
    function Handler(onFulfilled, onRejected, resolve, reject)
    {
        this.onFulfilled = typeof onFulfilled === "function" ? onFulfilled : null;
        this.onRejected = typeof onRejected === "function" ? onRejected : null;
        this.resolve = resolve;
        this.reject = reject;
    }

    /**
     * @private
     * Take a potentially misbehaving resolver function and make sure
     * onFulfilled and onRejected are only called once.
     * Makes no guarantees about asynchrony.
     * @param fn {Function}
     * @param onFulfilled {Function}
     * @param onRejected {Function}
     */
    function doResolve(fn, onFulfilled, onRejected)
    {
        var done = false;
        try {
            fn(function (value) {
                if (done) {
                    return;
                }
                done = true;
                onFulfilled(value);
            }, function (reason) {
                if (done)
                {
                    return;
                }
                done = true;
                onRejected(reason);
            })
        } catch (ex) {
            if (done)
            {
                return;
            }
            done = true;
            onRejected(ex);
        }
    }

    /**
     * Appends a rejection handler callback to the promise,
     * and returns a new promise resolving to the return value of the callback if it is called,
     * or to its original fulfillment value if the promise is instead fulfilled.
     * @param onRejected {Function} on rejected function.
     */
    Promise.prototype["catch"] = function (onRejected)
    {
        return this.then(null, onRejected);
    };
    /**
     * Appends fulfillment and rejection handlers to the promise,
     * and returns a new promise resolving to the return value of the called handler,
     * or to its original settled value if the promise was not handled (i.e. if the relevant handler onFulfilled or onRejected is undefined).
     * @param onFulfilled {Function} on resolve function
     * @param onRejected {Function} on reject function
     * @returns {Promise}
     */
    Promise.prototype.then = function (onFulfilled, onRejected)
    {
        var me = this;
        return new Promise(function (resolve, reject) {
            handle.call(me, new Handler(onFulfilled, onRejected, resolve, reject));
        })
    };

    /**
     * Returns a promise that resolves when all of the promises in the iterable argument have resolved.
     * This is useful for aggregating results of multiple promises together.
     * @returns {Promise}
     */
    Promise.all = function ()
    {
        var args = Array.prototype.slice.call(arguments.length === 1 && isArray(arguments[0]) ? arguments[0] : arguments);

        return new Promise(function (resolve, reject)
        {
            if (args.length === 0)
            {
                return resolve([]);
            }
            var remaining = args.length;

            function res(i, val)
            {
                try
                {
                    if (val && (typeof val === "object" || typeof val === "function"))
                    {
                        var then = val.then;
                        if (typeof then === "function")
                        {
                            then.call(val, function (val)
                            {
                                res(i, val)
                            }, reject);
                            return;
                        }
                    }
                    args[i] = val;
                    if (--remaining === 0) {
                        resolve(args);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }

            for (var i = 0; i < args.length; i++)
            {
                res(i, args[i]);
            }
        });
    };
    /**
     * Returns a Promise object that is resolved with the given value.
     * If the value is a thenable (i.e. has a then method), the returned promise will "follow" that thenable, adopting its eventual state;
     * otherwise the returned promise will be fulfilled with the value.
     * Generally, if you want to know if a value is a promise or not
     * - Promise.resolve(value) it instead and work with the return value as a promise.
     * @param value {*} resolve value
     * @returns {Promise}
     */
    Promise.resolve = function (value)
    {
        if (value && typeof value === "object" && value.constructor === Promise)
        {
            return value;
        }

        return new Promise(function (resolve)
        {
            resolve(value);
        });
    };
    /**
     * Returns a Promise object that is rejected with the given reason.
     * @param value {*} reject reason
     * @returns {Promise}
     */
    Promise.reject = function (value)
    {
        return new Promise(function (resolve, reject)
        {
            reject(value);
        });
    };

    /**
     * Returns a promise that resolves or rejects as soon as one of the promises in the iterable resolves or rejects,
     * with the value or reason from that promise.
     * @param values {Array} arrays of promise
     * @returns {Promise}
     */
    Promise.race = function (values)
    {
        return new Promise(function (resolve, reject)
        {
            for (var i = 0, len = values.length; i < len; i++)
            {
                values[i].then(resolve, reject);
            }
        });
    };

    /**
     * Set the immediate function to execute callbacks
     * @param fn {function} Function to execute
     * @private
     */
    Promise._setImmediateFn = function _setImmediateFn(fn)
    {
        asap = fn;
    };

    if (typeof module !== "undefined" && module.exports)
    {
        module.exports = Promise;
    }
    else if (!root.Promise)
    {
        root.Promise = Promise;
    }

})(this);

/**
 * The UI global object, if you add the method to the UI, please update the com.reltio.framework.sandbox.API
 */
UI = function ()
{
    this.__funcOnAction = null;
    this.__contextOnAction = null;

    this.__funcOnEvent = null;
    this.__contextOnEvent = null;

    this.__requests = {};
    /**
     * Method which is allow to log smth
     * @param log {String|Object} log message
     */
    this.log = function (log)
    {
        this.sendMessage({action: "log", params: log});
    };
    /**
     * Method which is allow to change enabled state of control.
     * @param value {Boolean} is enabled
     */
    this.setEnabled = function (value)
    {
        this.sendMessage({action: "setEnabled", params: value});
    };
    /**
     * Method which is allow to change visibility state of control.
     * @param value {String} "visible" | "hidden" | "excluded"
     */
    this.setVisibility = function (value)
    {
        this.sendMessage({action: "setVisibility", params: value});
    };
    /**
     * Set widget's internal HTML
     * @param html {String?} HTML
     */
    this.setHtml = function(html) {
        this.sendMessage({action: "setHtml", params: {html: html}});
    };
    /**
     * Set widget child's internal HTML
     * @param id {String} widget child's id
     * @param html {String?} HTML
     */
    this.setChildHtml = function(id, html) {
        this.sendMessage({action: "setHtml", params: {id: id, html: html}});
    };
    /**
     * Set widget's height
     * @param height {Number?} height in pixels
     */
    this.setHeight = function(height) {
        this.sendMessage({action: "setHeight", params: height});
    };
    /**
     * Set widget's width
     * @param width {Number?} width in pixels
     */
    this.setWidth = function(width) {
        this.sendMessage({action: "setWidth", params: width});
    };
    /**
     * Set widget's tooltip
     * @param toolTip {String?} tooltip
     */
    this.setToolTip = function(toolTip) {
        this.sendMessage({action: "setToolTip", params: toolTip});
    };
    /**
     * @deprecated Use UI.onEvent(...) and listen for event "execute" instead
     *
     * The entry point of the script, when the all scripts are loaded then the on action will be invoked.
     * @param func {Function} func to call
     * @param context {Object} context object
     */
    this.onAction = function (func, context)
    {
        this.__funcOnAction = func;
        this.__contextOnAction = context;
    };
    /**
     * internal method which invoke the entry point
     */
    this.invokeOnAction = function ()
    {
        if (this.__funcOnAction)
        {
            this.__funcOnAction.call(this.__contextOnAction);
        }
    };
    /**
     * The entry point of the script, when a new event.
     * @param func {Function} func to call
     * @param context {Object} context object
     */
    this.onEvent = function (func, context)
    {
        this.__funcOnEvent = func;
        this.__contextOnEvent = context;
    };
    /**
     * internal method which invoke the entry point
     */
    this.invokeOnEvent = function (type, data)
    {
        if (this.__funcOnEvent)
        {
            this.__funcOnEvent.call(this.__contextOnEvent, type, data);
        }
    };
    /**
     * All the communication should be implemented through this method
     * @param message {Message}
     */
    this.sendMessage = function (message)
    {
        //noinspection JSUnresolvedFunction
        postMessage(JSON.stringify(message));
    };
    /**
     * This method is needed to process response messages from the Sandbox
     * @typedef Response
     * @type {object}
     * @property {string} name - action name
     * @property {string} id - an unique identifier
     * @property {object} result - a response result object
     * @param data {Message} message from the sandbox;
     */
    this.processResponse = function (data)
    {
        var name, request;
        if (data)
        {
            name = this.composeName(data.name, data.id);
            request = this.__requests[name];
            if (request)
            {
                if (request.callback)
                {
                    request.callback.call(request.context, data.result);
                }
                if (request.resolve)
                {
                    request.resolve(data.result);
                }
            }
            delete this.__requests[name];
        }
    };
    /**
     * Internal method which is needed to create unique names
     * @param name {String} request name
     * @param id {String} request id
     * @returns {string}
     */
    this.composeName = function (name, id)
    {
        return name + "." + id;
    };
    /**
     * Method which is allow to send request to UI side
     * @param action {String} action name
     * @param params {Object?} request parameters
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.request = function (action, params, callback, context)
    {
        var id = Math.floor(Math.random() * 100);
        var that = this;
        var promise = new Promise(function(resolve, reject) {
            that.__requests[that.composeName(action, id)] = {resolve: resolve, reject: reject, callback: callback, context: context};
        });
        params = params || {};
        params.name = action;
        params.id = id;
        this.sendMessage({action: "request", params: params});
        return promise;
    };
    /**
     * Method which is allow to get the entity uri
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getEntityUri = function (callback, context)
    {
        return this.request("getEntityUri", null, callback, context);
    };
    /**
     * Get current entity
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getEntity = function(callback, context) {
       return this.request("getEntity", null, callback, context);
    };
    /**
     * Method which is allow to get the api path url
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getApiPath = function (callback, context)
    {
       return this.request("getApiPath", null, callback, context);
    };
    /**
     * Method which is allow to get search query
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getSearchQuery = function (callback, context)
    {
       return this.request("getSearchQuery", null, callback, context);
    };
    /**
     * Method which is allow to get tenant name
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getTenant = function (callback, context)
    {
       return this.request("getTenant", null, callback, context);
    };
    /**
     * Method which is allow to get current perspective id
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getPerspective = function (callback, context)
    {
       return this.request("getPerspective", null, callback, context);
    };
    /**
     * Method which is allow to set current perspective
     * @param perspective {String} perspective id
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.setPerspective = function (perspective, callback, context)
    {
       return this.request("setPerspective", {perspective: perspective}, callback, context);
    };
    /**
     * Method which is allow to set current perspective
     * @param entityUri {String} entity URI
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.setEntityUri = function (entityUri, callback, context)
    {
       return this.request("setEntityUri", {entityUri: entityUri}, callback, context);
    };
    /**
     * Method which is allow to send api request
     * @param url {String} api url
     * @param method {String?} request method - GET, POST, PUT, DELETE
     * @param tenant {String?} tenant
     * @param headers {Object?} headers
     * @param data {Object|String?} request body
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.api = function (url, method, tenant, headers, data, callback, context)
    {
       return this.request("api", {url: url, method: method, headers: headers, data: data}, callback, context);
    };
    /**
     * Method which is allow to show alert box with text
     * @param text {String} alert text
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.alert = function (text, callback, context)
    {
       return this.request("alert", {text: text}, callback, context);
    };
    /**
     * Method which is allow to show confirm box with text
     * @param text {String} confirm text
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.confirm = function (text, callback, context)
    {
       return this.request("confirm", {text: text}, callback, context);
    };
    /**
     * Method which is allow to show prompt box
     * @param text {String} prompt text
     * @param defaultText {String?} prompt default value
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.prompt = function (text, defaultText, callback, context)
    {
       return this.request("prompt", {text: text, defaultText: defaultText || ""}, callback, context);
    };

    /**
     * Get metadata configuration
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getConfiguration = function(callback, context) {
       return this.request("getConfiguration", null, callback, context);
    };

    /**
     * Get UI configuration
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.getUiConfiguration = function(callback, context) {
       return this.request("getUiConfiguration", null, callback, context);
    };

    /**
     * Open search screen with some state
     * @param searchState {Object?} search state json
     * @param callback {Function?} callback function (deprecated)
     * @param context  {Object?} context (deprecated)
     * @return {Promise} promise object
     */
    this.openSearch = function(searchState, callback, context) {
       return this.request("openSearch", {searchState: searchState}, callback, context);
    };

    var __UI = this;
    this.Workflow =
    {
        /**
         * Method which is allow to send workflow request
         * @param url {String} workflow url
         * @param method {String?} request method - GET, POST, PUT, DELETE
         * @param tenant {String?} tenant
         * @param headers {Object?} headers
         * @param data {Object|String?} request body
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        request : function (url, method, tenant, headers, data, callback, context)
        {
          return __UI.request("workflow", {url: url, method: method, headers: headers, data: data, tenant: tenant}, callback, context);
        },

        /**
         * Start workflow process from search query
         * @param processDefinitionId {String} workflow type
         * @param searchString {String} search query
         * @param data {Object|String?} request body
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        startProcessFromQuery : function(processDefinitionId, searchString, data, callback, context)
        {
           return __UI.request("workflow/startProcessFromQuery", {processDefinitionId: processDefinitionId, searchString: searchString, data: data}, callback, context);
        },

        /**
         * Method which is allow to get the workflow path url
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        getWorkflowPath : function (callback, context)
        {
           return __UI.request("workflow/getWorkflowPath", null, callback, context);
        },

        /**
         * Method which is allow to check permission
         * @param permissionName {String} name of permission
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        checkPermission : function (permissionName, callback, context)
        {
            return __UI.request("workflow/checkPermission", {permission:permissionName}, callback, context);
        },

        /**
         * Method which is allow to check if a user can start process from query
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        canStartProcessFromQuery : function (callback, context)
        {
            return this.checkPermission("START_PROCESS_INSTANCE_SEARCH_QUERY", callback, context);
        },

        /**
         * Method which is allow to check if a user can start process
         * @param callback {Function?} callback function (deprecated)
         * @param context  {Object?} context (deprecated)
         * @return {Promise} promise object
         */
        canStartProcess : function (callback, context)
        {
            return this.checkPermission("START_PROCESS_INSTANCE", callback, context);
        }

    };

    return this;
}();

/**
 * this method needed for communication between the UI and sandbox
 * @param oEvent
 */
onmessage = function (oEvent)
{
    if (oEvent.data.action === "event")
    {
        UI.invokeOnEvent(oEvent.data.type || null, oEvent.data.data || null);
        if(oEvent.data.type === "execute") {
            UI.invokeOnAction();
        }
    }
    else if (oEvent.data.action === "response")
    {
        UI.processResponse(oEvent.data);
    }
};